# Changelog

## 1.0.0

Released at December 13, 2019.

### Added

### Changed

- Activados todos los productos
- Modificada plantilla de productos para acomodar todos los items
- Subir gratis no se realiza de forma directa; en lugar de ello se solicita el email del anunciante para subir el anuncio.
- No aplicar segunda pasada de búsqueda si se utilizan filtros.

### Deprecated

- Columnas product.credits_granted

### Removed

### Fixed

- Criterio filtrado edad/tarifa
- Modificados datos de tarifas de acuerdo a Excel
- Corregidas descripciones de tarifas, precios y porcentajes
- Placeholder para edad "Tu edad" en lugar de "Opcional".
- Mostrar el botón cerrar panel en experiencias.
- Corregido margen superior panel experiencias (mobile y tablet)
- Las promociones no referenciaban product_id y payment_id.
- La edad y la tarifa contabilizan como filtros

### Security

