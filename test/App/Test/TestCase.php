<?php

namespace App\Test;

use PHPUnit\Framework\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{

    protected $files = [];

    public function tearDown()
    {
        foreach ($this->files as $file) {
            @unlink($file);
        }
    }

    protected function createTempFile($filename)
    {
        $this->files[] = $path = sys_get_temp_dir() . \DIRECTORY_SEPARATOR . $filename;

        if (false === touch($path)) {
            $this->markTestSkipped(sprintf('Failed to create temporary test file "%s".', $path));
        }

        return $path;
    }
}