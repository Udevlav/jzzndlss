<?php

namespace App\Test\Locale;

use App\I18n\CorruptDictionaryException;
use App\I18n\DictionaryNotFoundException;

use App\Test\TestCase;

abstract class TranslationLoaderTest extends TestCase
{

    protected function expectCorruptDictionaryException()
    {
        $this->expectException(CorruptDictionaryException::class);
    }

    protected function expectDictionaryNotFoundException()
    {
        $this->expectException(DictionaryNotFoundException::class);
    }

}