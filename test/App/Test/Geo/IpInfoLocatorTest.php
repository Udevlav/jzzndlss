<?php

namespace App\Test\Geo;

use App\Geo\Locator\IpInfoLocator;

class IpInfoLocatorTest extends GeoLocatorTest
{

    public function testInstantiationWithAllowUrlFopenDisabledThrowsException()
    {
        $value = ini_get('allow_url_fopen');

        if (false === ini_set('allow_url_fopen', false)) {
            $this->markTestSkipped('Failed to set "allow_url_fopen" directive.');
        }

        $this->expectException(\RuntimeException::class);

        try {
            new IpInfoLocator();

        } catch (\RuntimeException $e) {
            ini_set('allow_url_fopen', $value);

            throw $e;
        }
    }

    public function testLookupWithInvalidIpThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);

        $geo = new IpInfoLocator();
        $geo->lookup('.127.0.0.1');
    }

    public function testLookupLocalhost()
    {
        $this->expectGeoLocationException();

        $geo = new IpInfoLocator();
        $geo->lookup('127.0.0.1');
    }

    public function testLookup()
    {
        $geo = new IpInfoLocator();

        $actual = $geo->lookup('81.184.162.45');

        $this->assertGeoLocation($actual);

        // ipinfo.io does not return country name, region code, postal code
        // nor timezone
        $this->assertNull($actual->getCountry());
        $this->assertNull($actual->getRegionCode());
        $this->assertNull($actual->getPostalCode());
        $this->assertNull($actual->getTimeZone());

        $this->assertEquals('81.184.162.45', $actual->getIp());
        $this->assertEquals('ES', $actual->getCountryCode());
        $this->assertEquals('Catalonia', $actual->getRegion());
        $this->assertEquals('Viladecans', $actual->getCity());
        $this->assertEquals(41.314, $actual->getLat());
        $this->assertEquals(2.0143, $actual->getLng());
    }
}