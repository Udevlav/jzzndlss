<?php

namespace App\Test\Geo;

use App\Geo\GeoLocationException;
use App\Geo\GeoLocation;
use App\Geo\LoopbackGeoLocation;
use App\Geo\UnknownGeoLocation;

use App\Test\TestCase;

abstract class GeoLocatorTest extends TestCase
{

    protected function expectGeoLocationException()
    {
        $this->expectException(GeoLocationException::class);
    }

    protected function assertGeoLocation(GeoLocation $actual)
    {
        $this->assertInstanceOf(GeoLocation::class, $actual);
        $this->assertNotNull($actual->getIp());
        $this->assertNotNull($actual->getLat());
        $this->assertNotNull($actual->getLng());
    }

    protected function assertLoopbackGeoLocation(GeoLocation $actual)
    {
        $this->assertGeoLocation($actual);

        $this->assertInstanceOf(LoopbackGeoLocation::class, $actual);
        $this->assertEquals(0.0, $actual->getLat());
        $this->assertEquals(0.0, $actual->getLng());
        $this->assertEquals('Loopback', $actual->getCity());
        $this->assertEquals('Loopback', $actual->getCountry());
        $this->assertEquals('Loopback', $actual->getRegion());
        $this->assertEquals('00', $actual->getRegionCode());
        $this->assertEquals('00', $actual->getCountryCode());
        $this->assertEquals('00000', $actual->getPostalCode());
    }

    protected function assertUnknownGeoLocation(GeoLocation $actual)
    {
        $this->assertGeoLocation($actual);

        $this->assertInstanceOf(UnknownGeoLocation::class, $actual);
        $this->assertEquals(0.0, $actual->getLat());
        $this->assertEquals(0.0, $actual->getLng());
        $this->assertEquals('Unknown', $actual->getCity());
        $this->assertEquals('Unknown', $actual->getCountry());
        $this->assertEquals('Unknown', $actual->getRegion());
        $this->assertEquals('00', $actual->getRegionCode());
        $this->assertEquals('00', $actual->getCountryCode());
        $this->assertEquals('00000', $actual->getPostalCode());
    }
}