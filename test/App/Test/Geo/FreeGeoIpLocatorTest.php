<?php

namespace App\Test\Geo;

use App\Geo\Locator\FreeGeoIpLocator;

class FreeGeoIpLocatorTest extends GeoLocatorTest
{

    public function testInstantiationWithAllowUrlFopenDisabledThrowsException()
    {
        $value = ini_get('allow_url_fopen');

        if (false === ini_set('allow_url_fopen', false)) {
            $this->markTestSkipped('Failed to set "allow_url_fopen" directive.');
        }

        $this->expectException(\RuntimeException::class);

        try {
            new FreeGeoIpLocator();

        } catch (\RuntimeException $e) {
            ini_set('allow_url_fopen', $value);

            throw $e;
        }
    }

    public function testLookupWithInvalidIpThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);

        $geo = new FreeGeoIpLocator();
        $geo->lookup('.127.0.0.1');
    }

    public function testLookupLocalhost()
    {
        $geo = new FreeGeoIpLocator();

        $actual = $geo->lookup('127.0.0.1');
    }

    public function testLookup()
    {
        $geo = new FreeGeoIpLocator();

        $actual = $geo->lookup('81.184.162.45');

        $this->assertGeoLocation($actual);
        $this->assertEquals('81.184.162.45', $actual->getIp());
        $this->assertEquals('ES', $actual->getCountryCode());
        $this->assertEquals('Spain', $actual->getCountry());
        $this->assertEquals('CT', $actual->getRegionCode());
        $this->assertEquals('Catalonia', $actual->getRegion());
        $this->assertEquals('Viladecans', $actual->getCity());
        $this->assertEquals('08840', $actual->getPostalCode());
        $this->assertEquals('Europe/Madrid', $actual->getTimeZone());
        $this->assertEquals(41.314, $actual->getLat());
        $this->assertEquals(2.0143, $actual->getLng());
    }
}