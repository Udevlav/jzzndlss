<?php

namespace App\Test\Geo;

use App\Geo\Locator\GeoLite2Locator;

class GeoLite2LocatorTest extends GeoLocatorTest
{

    static public function bundledDb(): string
    {
        return dirname(dirname(dirname(dirname(__DIR__)))) .'/var/geo/GeoLite2-City.mmdb';
    }
    

    public function testInstantiationWithNullFileThrowsException()
    {
        $this->expectException(\Error::class);
        new GeoLite2Locator(null);
    }

    public function testInstantiationWithInexistentFileThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        new GeoLite2Locator('/hope/this/does/not.exists');
    }

    public function testInstantiationWithUnreadableFileThrowsException()
    {
        $path = $this->createTempFile('test.mmdb');

        if (false === @chmod($path, 0300)) {
            $this->markTestSkipped(sprintf('Failed to change temporary test file "%s" permissions.', $path));

        } else {
            $this->expectException(\InvalidArgumentException::class);
            new GeoLite2Locator('/hope/this/does/not.exists');
        }
    }

    public function testLookupWithEmptyDbThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);

        new GeoLite2Locator($this->createTempFile('empty.mmdb'));
    }

    public function testLookupWithInvalidDbThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);

        $path = $this->createTempFile('invalid.mmdb');
        file_put_contents($path, "foobar\n");

        new GeoLite2Locator($path);
    }

    public function testLookupWithInvalidIpThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);

        $geo = new GeoLite2Locator(static::bundledDb());
        $geo->lookup('.127.0.0.1');
    }

    public function testLookupLocalhost()
    {
        $this->expectGeoLocationException();

        $geo = new GeoLite2Locator(static::bundledDb());
        $geo->lookup('127.0.0.1');
    }

    public function testLookup()
    {
        $geo = new GeoLite2Locator(static::bundledDb());

        $actual = $geo->lookup('81.184.162.45');

        $this->assertGeoLocation($actual);
        $this->assertNull($actual->getTimeZone());

        $this->assertEquals('81.184.162.45', $actual->getIp());
        $this->assertEquals('ES', $actual->getCountryCode());
        $this->assertEquals('España', $actual->getCountry());
        $this->assertEquals('B', $actual->getRegionCode());
        $this->assertEquals('Barcelona', $actual->getRegion());
        $this->assertEquals('Barcelona', $actual->getCity());
        $this->assertEquals('08038', $actual->getPostalCode());
        $this->assertEquals(41.3888, $actual->getLat());
        $this->assertEquals(2.1589999999999998, $actual->getLng());
    }
}