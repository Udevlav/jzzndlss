<?php

namespace App\Test\Geo;

use App\Config\Config;
use App\Geo\Locator\DelegatingGeoLocator;

use Phalcon\Di\FactoryDefault;

class DelegatingGeoLocatorTest extends GeoLocatorTest
{

    protected function getLocator(): DelegatingGeoLocator
    {
        $di = new FactoryDefault();
        $di->setShared('config', new Config([ 'geo' => [ 'geoip2' => [ 'db' => GeoLite2LocatorTest::bundledDb() ]]]));

        $locator = new DelegatingGeoLocator();
        $locator->setDI($di);

        return $locator;
    }

    public function testLookupLocalhost()
    {
        $this->assertLoopbackGeoLocation($this->getLocator()->lookup('127.0.0.1'));
    }
}