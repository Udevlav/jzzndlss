INSERT INTO `jz_product` (`id`, `label`, `description`, `credits_granted`, `dayspan`, `promotions`, `price`, `active`, `savings`)
VALUES
  (1,  '1 subida al día',    '1,50 EUR/subida',  24,  1,   1,  '1.50', 1, ''),
  (2,  '3 subidas al día',   '1,00 EUR/subida',  72,  1,   3,  '3.00', 1, '¡Ahorras un 33%!'),
  (3,  '10 subidas al día',  '0,50 EUR/subida', 240,  1,  10,  '5.00', 1, '¡Ahorras un 67%!'),
  (4,  '16 subidas al día',  '0,50 EUR/subida', 384,  1,  16,  '8.00', 1, '¡Ahorras un 67%!'),
  (5,  '20 subidas al día',  '0,50 EUR/subida', 480,  1,  20, '10.00', 1, '¡Ahorras un 67%!'),
  (6,  '24 subidas al día',  '0,50 EUR/subida', 576,  1,  24, '12.00', 1, '¡Ahorras un 67%!'),

  (7,  '1 subida al día',    '1,29 EUR/subida',  24,  7,   7,  '9.00', 1, '¡Ahorras un 14%!'),
  (8,  '3 subidas al día',   '0,86 EUR/subida',  72,  7,  21, '18.00', 1, '¡Ahorras un 43%!'),
  (9,  '10 subidas al día',  '0,40 EUR/subida', 240,  7,  70, '28.00', 1, '¡Ahorras un 73%!'),
  (10, '16 subidas al día',  '0,38 EUR/subida', 384,  7, 112, '42.00', 1, '¡Ahorras un 75%!'),
  (11, '20 subidas al día',  '0,36 EUR/subida', 480,  7, 140, '50.00', 1, '¡Ahorras un 76%!'),
  (12, '24 subidas al día',  '0,34 EUR/subida', 576,  7, 168, '57.00', 1, '¡Ahorras un 77%!'),

  (13, '1 subida al día',    '1,07 EUR/subida',  24, 15,  15, '16.00', 1, '¡Ahorras un 29%!'),
  (14, '3 subidas al día',   '0,71 EUR/subida',  72, 15,  45, '32.00', 1, '¡Ahorras un 53%!'),
  (15, '10 subidas al día',  '0,33 EUR/subida', 240, 15, 150, '49.00', 1, '¡Ahorras un 78%!'),
  (16, '16 subidas al día',  '0,30 EUR/subida', 384, 15, 240, '72.00', 1, '¡Ahorras un 80%!'),
  (17, '20 subidas al día',  '0,28 EUR/subida', 480, 15, 300, '85.00', 1, '¡Ahorras un 81%!'),
  (18, '24 subidas al día',  '0,27 EUR/subida', 576, 15, 360, '96.00', 1, '¡Ahorras un 82%!'),

  (19, '1 subida al día',    '0,97 EUR/subida',  24, 30,  30, '29.00', 1, '¡Ahorras un 36%!'),
  (20, '3 subidas al día',   '0,44 EUR/subida',  72, 30,  90, '40.00', 1, '¡Ahorras un 70%!'),
  (21, '10 subidas al día',  '0,19 EUR/subida', 240, 30, 300, '58.00', 1, '¡Ahorras un 87%!'),
  (22, '16 subidas al día',  '0,17 EUR/subida', 384, 30, 480, '81.00', 1, '¡Ahorras un 89%!'),
  (23, '20 subidas al día',  '0,15 EUR/subida', 480, 30, 600, '91.00', 1, '¡Ahorras un 90%!'),
  (24, '24 subidas al día',  '0,13 EUR/subida', 576, 30, 720, '96.00', 1, '¡Ahorras un 91%!')

;
