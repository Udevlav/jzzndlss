CREATE TABLE IF NOT EXISTS `jz_ad_stats` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `measured_at`       DATE NOT NULL
  `num_days_on_top`   INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_impressions`   INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_promotions`    INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_phone_clicks`  INT(10) UNSIGNED NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),

  KEY `idx_ad_id`  (`ad_id`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `jz_ad_stats` ADD CONSTRAINT `fk_ad_stats_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

