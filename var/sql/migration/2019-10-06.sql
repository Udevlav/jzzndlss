ALTER TABLE jz_ad ADD COLUMN `promoted_at` TIMESTAMP NULL;
ALTER TABLE jz_product ADD COLUMN `promotions` INT(4) UNSIGNED NOT NULL;
ALTER TABLE jz_promotion ADD COLUMN `timestamps` TEXT DEFAULT NULL;
ALTER TABLE jz_ad ADD INDEX `idx_promoted_at` (`promoted_at`);
