ALTER TABLE jz_payment ADD COLUMN `ad_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE jz_payment ADD COLUMN `request` VARCHAR(255) NOT NULL;
ALTER TABLE jz_promotion DROP COLUMN `autopromote`;
ALTER TABLE jz_promotion ADD COLUMN `product_id` INT(10) UNSIGNED DEFAULT NULL;
ALTER TABLE jz_promotion ADD COLUMN `payment_id` INT(10) UNSIGNED DEFAULT NULL;

ALTER TABLE jz_payment ADD KEY `idx_ad_id` (`ad_id`);
ALTER TABLE jz_promotion ADD KEY `idx_payment_id` (`payment_id`);

ALTER TABLE `jz_payment` ADD CONSTRAINT `fk_payment_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_promotion` ADD CONSTRAINT `fk_promotion_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_promotion` ADD CONSTRAINT `fk_promotion_product` FOREIGN KEY idx_product_id(`product_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_promotion` ADD CONSTRAINT `fk_promotion_payment` FOREIGN KEY idx_payment_id(`payment_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
