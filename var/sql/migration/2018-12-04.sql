ALTER TABLE jz_ad ADD COLUMN `hash` VARCHAR(255) NOT NULL;
ALTER TABLE jz_ad ADD KEY `idx_hash` (`hash`);
