CREATE TABLE IF NOT EXISTS `jz_ad` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `advertiser_id`     INT(10) UNSIGNED NOT NULL,
  `slug`              VARCHAR(255) NOT NULL,
  `hash`              VARCHAR(255) NOT NULL,
  `provider`          VARCHAR(64) NOT NULL,
  `category`          VARCHAR(255) NOT NULL,
  `poster`            VARCHAR(512) NOT NULL,
  `layout`            VARCHAR(32) NOT NULL,
  `title`             VARCHAR(127) NOT NULL,
  `text`              LONGTEXT NOT NULL,
  `name`              VARCHAR(127) NOT NULL,
  `age`               INT(4) UNSIGNED DEFAULT NULL,
  `email`             VARCHAR(127) NOT NULL,
  `phone`             VARCHAR(127) DEFAULT NULL,
  `twitter`           VARCHAR(127) DEFAULT NULL,
  `whatsapp`          TINYINT(1) DEFAULT 0,
  `agency`            VARCHAR(127) DEFAULT NULL,
  `independent`       TINYINT(1) DEFAULT 0,
  `lat`               DECIMAL(7,4) NOT NULL,
  `lng`               DECIMAL(7,4) NOT NULL,
  `postal_code`       VARCHAR(5) NOT NULL,
  `all_tags`          VARCHAR(1024) DEFAULT NULL,
  `country`           VARCHAR(127) NOT NULL,
  `city`              VARCHAR(127) NOT NULL,
  `zone`              VARCHAR(127) NOT NULL,
  `fee`               DECIMAL(7,2) NOT NULL,
  `fees`              VARCHAR(1024) NOT NULL,
  `cash`              TINYINT(1) DEFAULT 1,
  `card`              TINYINT(1) DEFAULT 1,
  `availability`      VARCHAR(1024) NOT NULL,
  `ethnicity`         VARCHAR(127) DEFAULT NULL,
  `origin`            VARCHAR(127) DEFAULT NULL,
  `occupation`        VARCHAR(127) DEFAULT NULL,
  `schooling`         VARCHAR(127) DEFAULT NULL,
  `languages`         VARCHAR(127) DEFAULT NULL,
  `hair_color`        VARCHAR(32) DEFAULT NULL,
  `bsize`             VARCHAR(32) DEFAULT NULL,
  `height`            INT(3) DEFAULT NULL,
  `weight`            INT(2) DEFAULT NULL,
  `score`             INT(4) UNSIGNED DEFAULT 0,
  `rating`            INT(4) UNSIGNED DEFAULT 0,
  `num_hits`          INT(4) UNSIGNED DEFAULT 0,
  `num_images`        INT(4) UNSIGNED DEFAULT 0,
  `num_videos`        INT(4) UNSIGNED DEFAULT 0,
  `num_experiences`   INT(4) UNSIGNED DEFAULT 0,
  `created_from`      VARCHAR(15) NOT NULL,
  `updated_from`      VARCHAR(15) NOT NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
  `updated_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
  `published_at`      TIMESTAMP NULL,
  `promoted_at`       TIMESTAMP NULL,
  `last_hit_at`       TIMESTAMP NULL,

  PRIMARY KEY (`id`),

  UNIQUE(`slug`),

  KEY `idx_advertiser_id`  (`advertiser_id`),
  KEY `idx_slug` (`slug`),
  KEY `idx_hash` (`hash`),
  KEY `idx_phone` (`phone`),
  KEY `idx_category` (`category`),
  KEY `idx_postal_code`  (`postal_code`),
  KEY `idx_name` (`name`),
  KEY `idx_email` (`email`),
  KEY `idx_fees` (`fees`),
  KEY `idx_num_hits` (`num_hits`),
  KEY `idx_promoted_at` (`promoted_at`),
  KEY `idx_published_at` (`published_at`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_ad_stats` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `measured_at`       DATE NOT NULL,
  `num_days_on_top`   INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_impressions`   INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_promotions`    INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_phone_clicks`  INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `num_visits`        INT(10) UNSIGNED NOT NULL DEFAULT 0,

  PRIMARY KEY (`id`),

  KEY `idx_ad_id`  (`ad_id`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_ad_tag` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `tag_id`            INT(10) UNSIGNED NOT NULL,

  PRIMARY KEY (`id`),

  KEY `idx_ad_id`  (`ad_id`),
  KEY `idx_tag_id`  (`tag_id`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_advertiser` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`              VARCHAR(127) NOT NULL,
  `age`               INT(4) UNSIGNED DEFAULT NULL,
  `email`             VARCHAR(127) NOT NULL,
  `phone`             VARCHAR(127) DEFAULT NULL,
  `fingerprints`      TEXT DEFAULT NULL,
  `credits`           INT(4) DEFAULT 0,
  `num_ads`           INT(10) UNSIGNED NOT NULL,
  `token`             VARCHAR(127) DEFAULT NULL,
  `token_valid_from`  TIMESTAMP NULL,
  `token_valid_to`    TIMESTAMP NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
  `updated_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),

  PRIMARY KEY (`id`),

  UNIQUE(`email`),

  KEY `idx_email` (`email`),
  KEY `idx_token` (`token`),
  KEY `idx_token_valid_from` (`token_valid_from`),
  KEY `idx_token_valid_to` (`token_valid_to`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_complaint` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `email`             VARCHAR(127) NOT NULL,
  `cause`             INT(4) UNSIGNED DEFAULT NULL,
  `text`              LONGTEXT NOT NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
  `updated_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),

  PRIMARY KEY (`id`),

  KEY `idx_ad_id`  (`ad_id`),
  KEY `idx_email` (`email`),
  KEY `idx_cause` (`cause`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_chat_message` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `advertiser_id`     INT(10) UNSIGNED NOT NULL,
  `advertiser_sid`    VARCHAR(127) NOT NULL,
  `peer_sid`          VARCHAR(127) NOT NULL,
  `direction`         INT(1) NOT NULL,
  `text`              VARCHAR(127) NOT NULL,
  `sent_at`           TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),

  PRIMARY KEY (`id`),

  KEY `idx_advertiser_id` (`advertiser_id`),
  KEY `idx_advertiser_sid` (`advertiser_sid`),
  KEY `idx_peer_sid` (`peer_sid`),
  KEY `idx_sent_at` (`sent_at`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_contact` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email`             VARCHAR(127) NOT NULL,
  `text`              LONGTEXT NOT NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
  `updated_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),

  PRIMARY KEY (`id`),

  KEY `idx_email` (`email`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_experience` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `text`              VARCHAR(1024) NOT NULL,
  `author`            VARCHAR(32) NOT NULL,
  `rating`            INT(4) NOT NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),

  PRIMARY KEY (`id`),

  KEY `idx_ad_id`  (`ad_id`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_hit` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `hit_at`            TIMESTAMP NOT NULL,
  `fingerprint`       VARCHAR(512) DEFAULT NULL,

  PRIMARY KEY (`id`),

  KEY `idx_ad_id`  (`ad_id`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_media` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NULL,
  `master_id`         INT(10) UNSIGNED NULL,
  `temp`              TINYINT(1) DEFAULT 1,
  `poster`            TINYINT(1) DEFAULT 0,
  `name`              VARCHAR(127) NOT NULL,
  `version`           VARCHAR(32) DEFAULT NULL,
  `path`              VARCHAR(127) NOT NULL,
  `type`              VARCHAR(127) NOT NULL,
  `size`              INT(4) DEFAULT NULL,
  `width`             INT(4) DEFAULT NULL,
  `height`            INT(4) DEFAULT NULL,
  `duration`          INT(4) DEFAULT NULL,
  `time_code`         VARCHAR(16) DEFAULT NULL,
  `container_type`    VARCHAR(32) DEFAULT NULL,
  `audio_desc`        VARCHAR(512) DEFAULT NULL,
  `audio_codec`       VARCHAR(32) DEFAULT NULL,
  `audio_profile`     VARCHAR(32) DEFAULT NULL,
  `audio_level`       VARCHAR(32) DEFAULT NULL,
  `audio_channels`    VARCHAR(32) DEFAULT NULL,
  `audio_sample_rate` VARCHAR(32) DEFAULT NULL,
  `audio_bitrate`     VARCHAR(32) DEFAULT NULL,
  `video_desc`        VARCHAR(512) DEFAULT NULL,
  `video_codec`       VARCHAR(32) DEFAULT NULL,
  `video_profile`     VARCHAR(32) DEFAULT NULL,
  `video_level`       VARCHAR(32) DEFAULT NULL,
  `video_bitrate`     VARCHAR(32) DEFAULT NULL,
  `video_dar`         VARCHAR(16) DEFAULT NULL,
  `video_par`         VARCHAR(16) DEFAULT NULL,
  `video_cs`          VARCHAR(16) DEFAULT NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
  `updated_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
  `uploader_ip`       VARCHAR(15) NOT NULL,

  PRIMARY KEY (`id`),

  UNIQUE(`path`),

  KEY `idx_ad_id`  (`ad_id`),
  KEY `idx_master_id` (`master_id`),
  KEY `idx_poster` (`poster`),
  KEY `idx_temp` (`temp`),
  KEY `idx_path` (`path`),
  KEY `idx_type` (`type`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_online_advertiser` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `advertiser_id`     INT(10) UNSIGNED NOT NULL,
  `advertiser_sid`    VARCHAR(127) NOT NULL,
  `last_activity_at`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),

  PRIMARY KEY (`id`),

  KEY `idx_advertiser_id`  (`advertiser_id`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_payment` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id`        INT(10) UNSIGNED NOT NULL,
  `advertiser_id`     INT(10) UNSIGNED NOT NULL,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `method`            VARCHAR(32) DEFAULT NULL,
  `amount`            DECIMAL(9,2) NOT NULL,
  `promo_code`        VARCHAR(8) DEFAULT NULL,
  `credits_granted`   INT(4) UNSIGNED NOT NULL,
  `request`           VARCHAR(255) NOT NULL,
  `result`            INT(1) UNSIGNED DEFAULT NULL,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
  `committed_at`      TIMESTAMP NULL,
  `data`              VARCHAR(2048) DEFAULT NULL,

  PRIMARY KEY (`id`),

  KEY `idx_product_id` (`product_id`),
  KEY `idx_advertiser_id` (`advertiser_id`),
  KEY `idx_ad_id` (`ad_id`),
  KEY `idx_method` (`method`),
  KEY `idx_result` (`result`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_product` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label`             VARCHAR(127) NOT NULL,
  `days`              VARCHAR(127) NOT NULL,
  `description`       VARCHAR(255) NOT NULL,
  `savings`           VARCHAR(255) NOT NULL,
  `credits_granted`   INT(4) UNSIGNED NOT NULL,
  `promotions`        INT(4) UNSIGNED NOT NULL,
  `dayspan`           INT(4) UNSIGNED NOT NULL,
  `price`             DECIMAL(5,2) NOT NULL,
  `active`            TINYINT(1) DEFAULT 1,

  PRIMARY KEY (`id`),

  KEY `idx_active`  (`active`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_promotion` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ad_id`             INT(10) UNSIGNED NOT NULL,
  `product_id`        INT(10) UNSIGNED DEFAULT NULL,
  `payment_id`        INT(10) UNSIGNED DEFAULT NULL,
  `timestamps`        VARCHAR(1024) DEFAULT NULL,
  `valid_from`        TIMESTAMP NULL,
  `valid_to`          TIMESTAMP NULL,
  `bonus`             INT(10) NULL,
  `credits`           INT(10) DEFAULT 0,

  PRIMARY KEY (`id`),

  KEY `idx_ad_id` (`ad_id`),
  KEY `idx_product_id` (`product_id`),
  KEY `idx_payment_id` (`payment_id`),
  KEY `idx_valid_from` (`valid_from`),
  KEY `idx_valid_to` (`valid_to`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `jz_tag` (
  `id`                INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gid`               INT(10) NOT NULL,
  `name`              VARCHAR(127) NOT NULL,
  `slug`              VARCHAR(127) NOT NULL,

  PRIMARY KEY (`id`),

  UNIQUE(`name`),

  KEY `idx_gid` (`gid`),
  KEY `idx_slug` (`slug`)

) ENGINE=innodb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `jz_ad` ADD CONSTRAINT `fk_ad_advertiser` FOREIGN KEY idx_advertiser_id(`advertiser_id`) REFERENCES `jz_advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_ad_stats` ADD CONSTRAINT `fk_ad_stats_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_ad_tag` ADD CONSTRAINT `fk_ad_tag_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_ad_tag` ADD CONSTRAINT `fk_ad_tag_tag` FOREIGN KEY idx_tag_id(`tag_id`) REFERENCES `jz_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_chat_message` ADD CONSTRAINT `fk_chat_message_advertiser` FOREIGN KEY idx_advertiser_id(`advertiser_id`) REFERENCES `jz_advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_complaint` ADD CONSTRAINT `fk_complaint_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_experience` ADD CONSTRAINT `fk_experience_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_hit` ADD CONSTRAINT `fk_hit_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_media` ADD CONSTRAINT `fk_media_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_media` ADD CONSTRAINT `fk_media_master` FOREIGN KEY idx_master_id(`master_id`) REFERENCES `jz_media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_online_advertiser` ADD CONSTRAINT `fk_online_advertiser` FOREIGN KEY idx_advertiser_id(`advertiser_id`) REFERENCES `jz_advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_payment` ADD CONSTRAINT `fk_product_advertiser` FOREIGN KEY idx_product_id(`product_id`) REFERENCES `jz_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_payment` ADD CONSTRAINT `fk_payment_advertiser` FOREIGN KEY idx_advertiser_id(`advertiser_id`) REFERENCES `jz_advertiser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_payment` ADD CONSTRAINT `fk_payment_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jz_promotion` ADD CONSTRAINT `fk_promotion_ad` FOREIGN KEY idx_ad_id(`ad_id`) REFERENCES `jz_ad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_promotion` ADD CONSTRAINT `fk_promotion_product` FOREIGN KEY idx_product_id(`product_id`) REFERENCES `jz_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `jz_promotion` ADD CONSTRAINT `fk_promotion_payment` FOREIGN KEY idx_payment_id(`payment_id`) REFERENCES `jz_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
