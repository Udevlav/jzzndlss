yum install jpegoptim
yum install pngquant

cd ~/src
git clone --recursive https://github.com/kornelski/pngquant.git ./pngquant
cd pngquant
./configure
make
make install
