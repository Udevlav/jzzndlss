/* global ga, google, Fingerprint2 */
(function(global, document, $) {

    function detectmob() {
        if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ){
            return true;
        }
        else {
            return false;
        }
    }

    var IS_MOBILE = detectmob();
    var IS_DESKTOP = ! IS_MOBILE;
    var categoryToSlug = {
        1: 'escort',
        2: 'gigolo',
        3: 'travesti'
    };
    var months = ('ene feb mar abr may jun jul ago sep oct nov dic').split(' ');
    var provinceCodeToPostalCode = {
        'Álava': '01',
        'Vitoria': '01',
        'Vitoria-Gasteiz': '01',
        'Albacete': '02',
        'Alicante': '03',
        'Almería': '04',
        'Ávila': '05',
        'Badajoz': '06',
        'Islas Baleares': '07',
        'Barcelona': '08',
        'Burgos': '09',
        'Cáceres': '10',
        'Cádiz': '11',
        'Castellón': '12',
        'Ciudad Real': '13',
        'Córdoba': '14',
        'A Coruña': '15',
        'Cuenca': '16',
        'Girona': '17',
        'Granada': '18',
        'Guadalajara': '19',
        'Gipuzkoa': '20',
        'Huelva': '21',
        'Huesca': '22',
        'Jaén': '23',
        'León': '24',
        'Lleida': '25',
        'La Rioja': '26',
        'Lugo': '27',
        'Madrid': '28',
        'Málaga': '29',
        'Murcia': '30',
        'Navarre': '31',
        'Ourense': '32',
        'Asturias': '33',
        'Palencia': '34',
        'Las Palmas': '35',
        'Pontevedra': '36',
        'Salamanca': '37',
        'Santa Cruz de Tenerife': '38',
        'Santa Cruz Tenerife': '38',
        'Tenerife': '38',
        'Cantabria': '39',
        'Segovia': '40',
        'Seville': '41',
        'Soria': '42',
        'Tarragona': '43',
        'Teruel': '44',
        'Toledo': '45',
        'Valencia': '46',
        'Valladolid': '47',
        'Bizkaia': '48',
        'Zamora': '49',
        'Zaragoza': '50',
        'Ceuta': '51',
        'Melilla': '52'
    };

    function clearValidationClasses() {
        $(this).removeClass('invalid').removeClass('valid');
    }

    function getCurrentLocation(callback) {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(callback);
        } else {
            callback(null);
        }
    }

    function requiredValidator() {
        $(this).removeClass('invalid').removeClass('valid');
    }

    function pad(str, len, ch) {
        while (str.len < len) {
            str = ch + str;
        }
        return str;
    }

    function formatDate(date) {
        return [
            pad(date.getDate(), 2, '0'),
            pad(1 + date.getMonth(), 2, '0'),
            date.getFullYear()
        ].join('/');
    }

    function formatTime(date) {
        if (! date) {
            date = new Date();
        }

        if (typeof date === 'string') {
            return date.split(' ').pop();
        }

        return date.getHours() + ':' + date.getMinutes();
    }

    function maybeCall(fn) {
        if (typeof fn === 'function') {
            fn.apply(fn, Array.prototype.slice.call(arguments, 1));
        }
    }

    function scrollTo(offset, time) {
        $('html, body').animate({
            scrollTop: Math.max(offset, 0) - 64
        }, time || 1000);
    }

    function sluggify(value) {
        return value
            .toString()
            .toLowerCase()
            .replace('á', 'a')
            .replace('é', 'e')
            .replace('í', 'i')
            .replace('ó', 'o')
            .replace('ú', 'u')
            .replace('ñ', 'n')
            .replace(/\s+/g, '-')     // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/--+/g, '-')     // Replace multiple - with single -
            .replace(/^-+/, '')       // Trim - from start of text
            .replace(/-+$/, '');      // Trim - from end of text
    }

    var eventQueue = [];

    function sendEvent(eventAction, eventLabel, eventValue) {
        if (ga) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'ad',
                eventAction: eventAction,
                eventLabel: eventLabel,
                eventValue: eventValue,
                hitCallback: processQueue
            });
        }
    }

    function processQueue() {
        var queued;

        if (eventQueue.length) {
            queued = eventQueue.shift();

            sendEvent(
                queued.eventAction,
                queued.eventLabel,
                queued.eventValue
            );
        }
    }

    function queueEvent(action, label, id) {
        eventQueue.push({
            eventAction: action,
            eventLabel: label,
            eventValue: id
        });
    }

    function sendImpression(id, label) {
        queueEvent('impression', label, id);
    }

    function sendPhoneClick(id, label) {
        sendEvent('phoneClick', label, id);
    }

    function sendTop(id, label) {
        queueEvent('top', label, id);
    }

    function sendPaymentStart(slug) {
        sendEvent('paymentStart', slug, slug);
    }

    function sendPaymentProceed(slug, productId) {
        sendEvent('paymentProceed', slug, productId);
    }

    function sendPaymentSuccess(slug, productId) {
        sendEvent('paymentSuccess', slug, productId);
    }

    function sendPaymentFailed(slug, productId) {
        sendEvent('paymentFailed', slug, productId);
    }

    function sendProductSelected(slug, productId) {
        sendEvent('productSelected', slug, productId);
    }

    function sendProductChanged(slug, productId, previousProductId) {
        sendEvent('productChanged', slug, productId, previousProductId);
    }

    function sendProductScheduled(slug, productId) {
        sendEvent('productScheduled', slug, productId);
    }


    var Capabilities = {
        FILE_READER_API_SUPPORT: !! (typeof global.FileReader === 'function'),
        GEOLOCATION_API_SUPPORT: !! ('geolocation' in global.navigator) && global.location.protocol === 'https:'
    };


    function CookieManager(scope) {
        this.scope = scope;
        this.cookiePolicyAccepted = Boolean(this.get(CookieManager.COOKIE_POLICY_ACCEPTED));
    }

    CookieManager.ONE_DAY_IN_MS = 24 * 3600000;
    CookieManager.COOKIE_POLICY_ACCEPTED = 'uoc';

    CookieManager.prototype.init = function() {
        var self = this;

        $('#adult-modal .modal-action').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.acceptCookiePolicy();
        });

        if (this.cookiePolicyAccepted) {
            this.acceptCookiePolicy();
        } else {
            $('#adult-modal').modal('open');
        }
    };

    CookieManager.prototype.acceptCookiePolicy = function() {
        this.cookiePolicyAccepted = true;
        this.set(CookieManager.COOKIE_POLICY_ACCEPTED, String(new Date()));

        $('.cookies').addClass('dn');
        $('#adult-modal').modal('close');
    };

    CookieManager.prototype.has = function(name) {
        return !! this.get(name);
    };

    CookieManager.prototype.remove = function(name) {
        this.set(name, '', -1);
    };

    CookieManager.prototype.clear = function() {
        var self = this;

        document.cookie.split(';').map(function(cookie) {
            self.remove(cookie.split('=')[0]);
        });
    };

    CookieManager.prototype.set = function(name, value, days, path, domain, secure) {
        var date, expires = '';

        if (days) {
            date = new Date();
            date.setTime(date.getTime() + (days * CookieManager.ONE_DAY_IN_MS));
            expires = '; expires=' + date.toGMTString();
        }

        document.cookie = name + '=' + value +
            (domain ? '; domain=' + domain : '') +
            expires + '; path=' + (path || '/') +
            (secure ? '; secure' : '');
    };

    CookieManager.prototype.get = function(name) {
        var i, l, cookie, cookies, cookieName, cookieValue, nameEq;

        nameEq = name + '=';
        cookies = document.cookie.split(';');

        for (i = 0, l = cookies.length; i < l; i++) {
            cookie = cookies[i];

            while (cookie.charAt(0) === ' ') {
                cookie = cookie.substring(1, cookie.length);
            }

            if (cookie.indexOf(nameEq) === 0) {
                cookieName = cookie.substring(0, nameEq);
                cookieValue = cookie.substring(nameEq.length, cookie.length);

                return cookieValue;
            }
        }

        return null;
    };

    CookieManager.prototype.all =  function() {
        var cookies = {};

        document.cookie.split(';').map(function(cookie) {
            var cookieName, cookieValue, index;

            index = cookie.indexOf('=');

            if (index === -1) {
                // Empty cookie name
                cookieName = '';
                cookieValue = cookie;
            } else {
                cookieName = cookie.substring(0, index);
                cookieValue = cookie.substring(index + 1);
            }

            cookies[cookieName] = cookieValue;
        });

        return cookies;
    };


    function Scroll(scope) {
        this.scope = scope;
        this.listening = false;
        this.listeners = [];
    }

    Scroll.prototype.init = function() {
        this.height = $('nav.menu').height();
    };

    Scroll.prototype.add = function(listener, once) {
        this.listeners.push({
            fn: listener,
            once: Boolean(once)
        });

        if (! this.listening) {
            this.start();
        }
    };

    Scroll.prototype.start = function() {
        var listeners = this.listeners;

        // Note we do not debounce the scroll event
        // as the fixed elements will overlap with 
        // page content
        $(document).scroll(function() {
            var 
                scrollTop = $(global).scrollTop(),
                indexes = [];

            listeners.map(function(listener, index) {
                try {
                    listener.fn(scrollTop);

                    if (listener.once) {
                        indexes.push(index);
                    }
                } catch (error) {
                    indexes.push(index);
                }
            });

            indexes.reverse().map(function(index) {
                listeners.splice(index, 1);
            });
        });

        $(document).trigger('scroll');
    };


    var Validator = {
        age: function(field) {
            var value = Number(field.getValue());

            if (value < 18) {
                field.errors.push('illegal');
            }

            if (value > 90) {
                field.errors.push('too_much');
            }
        },
        fee: function(field) {
            var value = Number(field.getValue());

            if (value < 0) {
                field.errors.push('invalid');
            }
        },
        gtz: function(field) {
            var value = Number(field.getValue());

            if (value <= 0) {
                field.errors.push('invalid');
            }
        },
        phone: function(field) {
            var value = String(field.getValue()).replace(/\s+/, '');

            if (value.length !== 9) {
                field.errors.push('invalid_format');
            }
        },
        required: function(field) {
            var value = field.getValue();

            if (! value) {
                field.errors.push('required');
            }
        },
        time: function(field) {
            var value = field.getValue();

            if (! (/^\d{1,2}:\d{2}$/).test(value)) {
                field.errors.push('invalid');
            }
        }
    };


    function Field(element) {
        this.element = $(element);
        this.errors = [];
        this.init();
    }

    Field.prototype.init = function() {
        var self = this;

        this.element.on('focus', function() {
            self.element.removeClass('invalid');
            self.element.addClass('focused');
            self.reset();
        });

        this.element.on('blur', function() {
            self.element.removeClass('focused');
            self.validate();
        });

        this.element.on('change', function() {
            self.validate();
        });
    };

    Field.prototype.getName = function() {
        return this.element.attr('name');
    };

    Field.prototype.getValue = function() {
        return this.element.val();
    };

    Field.prototype.clear = function() {
        if (this.element.attr('type') !== 'hidden') {
            this.element.val('');
        }
    };

    Field.prototype.reset = function() {
        this.errors.length = 0;
    };

    Field.prototype.validate = function() {
        var fn;

        if (this.element.hasClass('validate') ||
            this.element.hasClass('required')) {
            Validator.required(this);
        }

        fn = Validator[this.element.attr('validator')];

        if (typeof fn === 'function') {
            fn(this);
        }

        if (this.errors.length > 0) {
            if (this.element.attr('type') === 'hidden') {
                $('#' + this.element.attr('name') + '-error').removeClass('dn');
            } else {
                this.element.addClass('invalid');
            }
        }
    };


    function Form(element, callback, errback) {
        this.element = $(element);
        this.callback = callback;
        this.errback = errback;
        this.fields = [];
        this.init();
    }

    Form.prototype.init = function() {
        var self = this;

        $('.mapped-field', this.element).each(function() {
            self.fields.push(new Field(this));
        });

        $('.form-submit', this.element).on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.validate();

            if (self.isValid()) {
                self.submit();
            }
        });
    };

    Form.prototype.isValid = function() {
        var errors = 0;

        this.fields.map(function(field) {
            errors += field.errors.length;
        });

        return (errors === 0);
    };

    Form.prototype.validate = function() {
        this.fields.map(function(field) {
            field.validate();
        });
    };

    Form.prototype.clear = function() {
        this.fields.map(function(field) {
            field.clear();
        });
    };

    Form.prototype.reset = function() {
        this.fields.map(function(field) {
            field.reset();
        });
    };

    Form.prototype.getData = function() {
        var data = {};

        this.fields.map(function(field) {
            data[ field.getName() ] = field.getValue();
        });

        return data;
    };

    Form.prototype.submit = function(callback, errback) {
        var self = this;

        if (this.element.hasClass('form-async')) {
            $.ajax({
                url: this.element.attr('action'),
                method: this.element.attr('method'),
                data: this.getData(),
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                }
            }).then(function(data) {
                maybeCall(self.callback, data);
                maybeCall(callback, data);
            }).fail(function(error) {
                maybeCall(self.errback, error);
                maybeCall(errback, error);
            });
        } else {
            this.element.submit();
        }
    };


    function Filter(scope) {
        this.scope = scope;
        this.category = null;
        this.location = null;
        this.locationSlug = null;
        this.selectedTags = [];
        this.ageFilter = false;
        this.feeFilter = false;
        this.maxTags = 5;
    }

    Filter.prototype.init = function() {
        var
            self = this,
            currentCat = $('#category').val(),
            checkedTags = $('.filter-tag:checked');

        this.element = $('.overlay.overlay-filter');
        this.form = $('.filter-form');
        this.tags = $('.overlay-filter .filter-tag');

        $('#filter-tags').on('focus', function() {
            self.openFilters();
        });

        $('.filter-form input').on('keyup', function() {
            self.applyFilters();
        });

        $('.apply-filters').click(function() {
            self.closeFilters();
        });

        $('.remove-filters').on('click', function() {
            self.unsetTags();
            self.closeFilters();
        });

        $('#category').on('change', function() {
            self.category = categoryToSlug[category];
            $('#offset').val(0);
        });

        $('.filter-form .submit-btn').on('click', function(e) {
            self.applyFilters();
            self.form.submit();
        });

        this.form.on('keyup', function(e) {
            if (e.keyCode == 13) {
                self.applyFilters();
                self.form.submit();
            }
        });

        this.tags.on('click', function() {
            var i, l, item, node = $(this), slug = node.attr('data-slug');

            if (this.checked) {
                self.selectedTags.push({
                    id: this.value,
                    slug: slug
                });
            } else {
                for (i = 0, l = self.selectedTags.length; i < l; i++) {
                    item = self.selectedTags[i];

                    if (item.slug === slug) {
                        self.selectedTags.splice(i, 1);
                        break;
                    }
                }
            }

            if (self.selectedTags.length > self.maxTags) {
                item = self.selectedTags.shift();
                $('.filter-tag[value="' + item.id + '"]').prop('checked', false);
            }

            self.updateTags();
        });

        this.scope.maps.load(function() {
            var
                currentLocation,
                currentData = {},
                clearLocation = function() {
                    $('#offset').val(0);
                    $('#lat').val('');
                    $('#lng').val('');
                    $('#city').val('');
                    $('#province').val('');
                    $('#postalCode').val('');

                    self.location = null;
                    self.locationSlug = null;
                },
                updateLocation = function(item) {
                    if (! item.postalCode && (provinceCodeToPostalCode[item.provinceName] || provinceCodeToPostalCode[item.cityName])) {
                        item.postalCode = provinceCodeToPostalCode[item.provinceName] || provinceCodeToPostalCode[item.cityName];
                    }

                    $('#offset').val(0);
                    $('#lat').val(item.lat);
                    $('#lng').val(item.lng);
                    $('#city').val(item.cityName);
                    $('#province').val(item.provinceName);
                    $('#postalCode').val(item.postalCode);

                    self.locationSlug = sluggify(item.cityName || item.provinceName || '');
                };

            getCurrentLocation(function(geo) {
                if (! geo || ! geo.coords) {
                    return;
                }

                self.scope.maps.reverseLookup({
                    lat: geo.coords.latitude,
                    lng: geo.coords.longitude
                }, function(results) {
                    var data = {};

                    results.map(function(item) {
                        data[item.cityName] = item;
                    });

                    currentLocation = data;
                    currentData = data;

                    //updateLocation();
                });
            });

            $('#age_from').on('change', function() {
                $('#ageFrom').val(this.value);
                self.updateTags();
            });

            $('#age_to').on('change', function() {
                $('#ageTo').val(this.value);
                self.updateTags();
            });

            $('#fee_from').on('change', function() {
                $('#feeFrom').val(this.value);
                self.updateTags();
            });

            $('#fee_to').on('change', function() {
                $('#feeTo').val(this.value);
                self.updateTags();
            });

            $('#filter-location').on('change', function() {
                var option = this.selectedOptions.item(0);

                if (option && option.value) {
                    if (option.value === 'any') {
                        updateLocation({
                            provinceName: '',
                            postalCode: 'any'
                        });
                    } else {
                        updateLocation({
                            provinceName: option.label,
                            postalCode: option.value
                        });
                    }
                } else {
                    clearLocation();
                }
            }).change();

            /*
            self.location = $('#filter-location').autocomplete({
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                minLength: 3, // The minimum length of the input for the autocomplete to start. Default: 1.,
                data: { '': 'Utilizar mi ubicación actual' },
                onAutocomplete: function(val, item) {
                    if (! item) {
                        item = currentData[val];
                    }

                    if (! item) {
                        return;
                    }

                    updateLocation(item);
                }
            });
            */

            /*
            self.location.on('keyup', function() {
                var loc = this;
                var query = this.value;

                clearTimeout(timer);
                clearTimeout(clearTimer);
                clearTimeout(commitClearTimer);

                clearTimer = setTimeout(function() {
                    if (! loc.value) {
                        clearTimeout(commitClearTimer);
                        commitClearTimer = setTimeout(function() {
                            if (! loc.value) {
                                clearLocation();
                            }
                        }, 500);
                    }
                }, 500);

                if (cache.hasOwnProperty(query)) {
                    callback(cache[query]);
                } else {
                    timer = setTimeout(function() {
                        self.scope.maps.lookup(query + ' España', function(results) {
                            var data = {};

                            results.map(function(item) {
                                data[item.cityName] = item;
                            });

                            cache[query] = data;
                            callback(data);
                        });
                    }, 350);
                }
            });
            */
        });

        if (checkedTags.length) {
            checkedTags.each(function() {
                self.selectedTags.push({
                    id: this.value,
                    slug: $(this).attr('slug')
                });
            });
            this.updateTags();
        }

        if (currentCat) {
            this.category = categoryToSlug[currentCat];
        }
    };

    Filter.prototype.applyFilters = function() {
        var url = [];

        if (this.category) {
            url.push(this.category);
        }

        if (this.locationSlug) {
            url.push('provincia-' + this.locationSlug);
        }

        if (this.locationSlug) {
            for (var i = 0; i < Math.min(5, this.selectedTags.length); i ++) {
                url.push(this.selectedTags[i].slug);
            }
        }

        this.form.attr('action', '/' + url.join('/'));
    };

    Filter.prototype.updateTags = function() {
        var tags = this.selectedTags.map(function(tag) {
            return tag.id;
        }).join(',');

        $('#filter-tags').siblings('label').addClass('active');

        var numFilters = this.selectedTags.length;

        this.ageFilter = !! ($('#age_from').val() || $('#age_to').val());
        this.feeFilter = !! ($('#fee_from').val() || $('#fee_to').val());

        if (this.ageFilter) {
            numFilters ++;
        }

        if (this.feeFilter) {
            numFilters ++;
        }

        if (numFilters > 1) {
            $('#filter-tags').val(numFilters + ' filtros');
        } else if (numFilters === 1) {
            $('#filter-tags').val(numFilters + ' filtro');
        } else {
            $('#filter-tags').val('Ningún filtro');
        }

        if (this.selectedTags.length) {
            $('#tags').val(tags);
        } else {
            $('#tags').val('');
        }
    };

    Filter.prototype.unsetTags = function() {
        var tag;

        while (this.selectedTags.length) {
            tag = this.selectedTags.shift();
            $('.filter-tag[value="' + tag.id + '"]').prop('checked', false);
        }

        this.selectedTags = [];
        this.updateTags();
    };

    Filter.prototype.openFilters = function() {
        this.updateTags();

        $('.fixed-action-btn.fab-detail.direction-top').addClass('dn');
        $('body').addClass('no-scroll');
        $('.overlay-filter').addClass('open');
    };

    Filter.prototype.closeFilters = function() {
        this.updateTags();

        $('.overlay-filter').removeClass('open');
        $('body').removeClass('no-scroll');
        $('.fixed-action-btn.fab-detail.direction-top').removeClass('dn');
    };

    Filter.prototype.disable = function() {
        $('.menu-filter').hide();
    };


    function Fingerprint(scope) {
        this.scope = scope;
        this.fingerprint = null;
        this.components = null;
    }

    Fingerprint.prototype.getFingerprint = function(callback) {
        var self = this;

        if (! this.fingerprint) {
            maybeCall(callback, this.fingerprint);
        } else {
            new Fingerprint2().get(function(result, components) {
                self.fingerprint = result;
                self.components = components;

                maybeCall(callback, self.fingerprint, self.components);
            });
        }
    };


    function Location(scope) {
        this.scope = scope;
    }


    function Captcha(scope) {
        this.scope = scope;
    }

    Captcha.prototype.load = function() {
        var head, script, url;

        if (! this.loaded) {
            url = 'https://www.google.com/recaptcha/api.js?hl=es';

            script = document.createElement('script');
            script.setAttribute('type', 'text/javascript');
            script.setAttribute('async', 'true');
            script.setAttribute('src', url);

            head = document.getElementsByTagName('head')[0];
            head.insertBefore(script, head.firstElementChild);

            this.loaded = true;
        }
    };


    function Maps(scope, apiKey) {
        this.scope = scope;
        this.apiKey = apiKey;
        this.maps = {};
        this.loaded = false;
        this.geocoder = null;
    }

    Maps.prototype.load = function(callback, errback) {
        var
            head,
            script,
            timer,
            uuid,
            self = this,
            failed = false;

        if (this.loaded) {
            callback();
        } else {
            uuid = (new Date()).getTime();

            global[uuid] = function() {
                clearTimeout(timer);

                self.loaded = true;

                if (! failed) {
                    $(document).trigger('googlemaps.loaded');
                    maybeCall(callback);
                }
            };

            timer = setTimeout(function() {
                failed = true;
                self.loaded = true;
                maybeCall(errback);
            }, 10000);

            script = document.createElement('script');
            script.setAttribute('type', 'text/javascript');
            script.setAttribute('async', 'true');
            script.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&language=es&callback=' + uuid);

            head = document.getElementsByTagName('head')[0];
            head.insertBefore(script, head.firstElementChild);
        }
    };

    Maps.prototype.toAddress = function(result) {
        var address = {
            lat: result.geometry.location.lat(),
            lng: result.geometry.location.lng(),
            label: result['formatted_address'],
            types: result.types
        };

        result['address_components'].map(function(component) {
            if (component.types.indexOf('postal_code') > -1) {
                address.postalCode = component['short_name'] || component['long_name'];

            } else if (component.types.indexOf('country') > -1) {
                address.country = component['short_name'];
                address.countryName = component['long_name'];

            } else if (component.types.indexOf('administrative_area_level_1') > -1) {
                address.province = component['short_name'];
                address.provinceName = component['long_name'];

            } else if (component.types.indexOf('administrative_area_level_2') > -1) {
                address.city = component['short_name'];
                address.cityName = component['long_name'];

            }
        });

        if (! address.postalCode ||
            ! address.country) {
            // Maybe a click on the sea
            return null;
        }

        return address;
    };

    Maps.prototype.errorMessage = function(status) {
        if (status === google.maps.GeocoderStatus.OK) {
            return 'OK';
        }
        if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
            return 'Ningún resultado';
        }
        if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            return 'Demasiadas peticiones a la API de Google Maps';
        }
        if (status === google.maps.GeocoderStatus.REQUEST_DENIED) {
            return 'Petición denegada';
        }
        if (status === google.maps.GeocoderStatus.INVALID_REQUEST) {
            return 'Petición inválida (generalmente falta alguno de los componentes de la dirección en la consulta)';
        }
        if (status === google.maps.GeocoderStatus.UNKNOWN_ERROR) {
            return 'La petición no se pudo procesar debido a un error de servidor. Inténtalo de nuevo.';
        }
        return 'Desconocido';
    };

    Maps.prototype.lookup = function(query, callback, errback) {
        var self = this;

        if (! this.loaded) {
            this.load(function() {
                self.lookup(callback, errback);
            });
        } else {
            if (! this.geocoder) {
                this.geocoder = new google.maps.Geocoder();
            }

            query += ' España';

            this.geocoder.geocode({ address: query }, function(result, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    maybeCall(callback, result.map(self.toAddress));
                } else {
                    maybeCall(errback, self.errorMessage(status));
                }
            });
        }
    };

    Maps.prototype.reverseLookup = function(coords, callback, errback) {
        var self = this;

        if (! this.loaded) {
            this.load(function() {
                self.reverseLookup(callback, errback);
            });
        } else {
            if (! this.geocoder) {
                this.geocoder = new google.maps.Geocoder();
            }

            this.geocoder.geocode({ location: coords }, function(result, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    maybeCall(callback, result.map(self.toAddress));
                } else {
                    maybeCall(errback, self.errorMessage(status));
                }
            });
        }
    };

    Maps.prototype.computeHeight = function(mapContainerWidth) {
        if (window.innerHeight <= 768) {
            return 0.75 * window.innerHeight;
        }

        if (window.innerHeight <= 1024) {
            return 0.5 * window.innerHeight;
        }

        return 0.75 * mapContainerWidth;
    };

    Maps.prototype.create = function(container, options, callback, errback) {
        var mapContainer, map, marker, self = this;

        if (! this.loaded) {
            this.load(function() {
                self.create(container, options, callback, errback);
            });
        } else {
            mapContainer = $(container);
            mapContainer.css('height', this.computeHeight(mapContainer.width()));
            mapContainer.find('.gmap').css('height', '100%');

            try {
                map = new google.maps.Map(mapContainer.find('.gmap').get(0), $.extend({
                    mapTypeControl: false,
                    zoom: 12,
                    center: {
                        lat: 41.37624,
                        lng: 2.132249
                    }
                }, options));

                if (options.marker) {
                    map.addListener('click', function(e) {
                        if (marker) {
                            marker.setMap(null);
                        }

                        marker = self.placeMarker(map, e.latLng, options.marker);
                    });
                }

                maybeCall(callback, map);
            } catch (error) {
                maybeCall(errback, error);
            }
        }
    };
    
    Maps.prototype.placeCircle = function(map, latLng) {
        var self = this;

        if (map.currentCircle) {
            map.currentCircle.setMap(null);
        }

        map.currentCircle = new google.maps.Circle({
            map: map,
            center: latLng,
            radius: 1000,
            strokeColor: '#ff5a5f',
            strokeOpacity: 0.25,
            strokeWeight: 2,
            fillColor: '#ff5a5f',
            fillOpacity: 0.15
        });

        map.currentCircle.addListener('click', function(e) {
            self.placeMarker(map, e.latLng, {});
        });
    };

    Maps.prototype.doPlaceMarker = function(map, latLng, options) {
        var callback, self = this;

        if (map.currentMarker) {
            map.currentMarker.setMap(null);
        }

        map.currentMarker = new google.maps.Marker({
            map: map,
            position: latLng,
            animation: options.animation || google.maps.Animation.DROP,
            draggable: options.draggable || true
        });

        this.placeCircle(map, latLng);

        callback = function(result) {
            options.rlookup(result);
        };

        if (options.draggable && options.rlookup) {
            map.currentMarker.addListener('dragend', function(e) {
                map.setCenter(e.latLng);
                self.placeMarker(map, e.latLng, options);
            });
        }

        if (options.zoomToPoint) {
            map.setCenter(latLng);
            map.setZoom(15);
        }

        if (options.rlookup) {
            this.reverseLookup(latLng, callback);
        }

        return map.currentMarker;
    };

    Maps.prototype.placeMarker = function(map, latLng, options) {
        var callback, self = this;

        if (! options.rlookup) {
            return this.doPlaceMarker(map, latLng, options);
        }

        if (map.currentMarker) {
            map.currentMarker.setMap(null);
        }

        map.currentMarker = new google.maps.Marker({
            position: latLng,
            animation: options.animation || google.maps.Animation.DROP,
            draggable: options.draggable || true
        });

        callback = function(result) {
            options.rlookup(result);
        };

        map.currentMarker.setMap(map);

        this.reverseLookup(latLng, function(result) {
            callback(result);
        });

        return map.currentMarker;
    };


    function Menu(scope) {
        this.scope = scope;
    }

    Menu.prototype.init = function() {
        var offset, links, logo, self = this;
        var logoButton = $('.menu-logo');
        var moreButton = $('.menu-more');
        var filterButton = $('.menu-filter');
        var searchButton = $('.menu-search');
        var overlayClose = $('.overlay-close');

        this.element = $('nav.menu');

        filterButton.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.openFilters();

            filterButton.addClass('active');
            searchButton.addClass('dn');
            overlayClose.removeClass('dn');
        });

        searchButton.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.openSearch();

            searchButton.addClass('dn');
            overlayClose.removeClass('dn');
        });

        logoButton.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            if ($('.overlay-menu.open').length) {
                self.closeMenu();
            } else {
                self.openMenu();

                searchButton.addClass('dn');
                overlayClose.removeClass('dn');
            }
        });

        moreButton.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            if ($('.overlay-menu.open').length) {
                self.closeMenu();
            } else {
                self.openMenu();

                searchButton.addClass('dn');
                overlayClose.removeClass('dn');
            }
        });

        overlayClose.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.closeFilters();
            self.closeMenu();
            self.closeSearch();

            overlayClose.addClass('dn');
            searchButton.removeClass('dn');
        });

        this.element.find('.links .link').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            scrollTo($(this.getAttribute('href')).offset().top);
        });
        
        if (! $('body').hasClass('page-index')) {
            offset = $('#ad-poster').height() / 2;
            links = this.element.find('.links');
            logo = this.element.find('.logo,.menu-filter,.menu-publish');

            this.element.find('.menu-more').addClass('dn').hide();
            this.element.find('.menu-back').removeClass('dn');

            if ($('body').hasClass('page-detail')) {
                this.scope.scroll.add(function(scrollTop) {
                    if (scrollTop > offset) {
                        logo.addClass('dn');
                        links.removeClass('dn');
                    } else {
                        links.addClass('dn');
                        logo.removeClass('dn');
                    }
                });
            } else if (this.hasLinks()) {
                logo.addClass('dn');
                links.removeClass('dn');
            } else {
                links.addClass('dn');
                logo.removeClass('dn');
            }
        } else {
            this.element.find('.menu-back').addClass('dn').hide();
        }
    };

    Menu.prototype.hasLinks = function() {
        var body = $('body');

        return body.hasClass('page-detail') || body.hasClass('');
    };

    Menu.prototype.setCurrentSection = function(name) {
        this.element.find('.links .link').removeClass('active');
        this.element.find('.links .link[href="#' + name +'"]').addClass('active');
    };

    Menu.prototype.openMenu = function() {
        $('body').addClass('no-scroll');
        $('#menu').addClass('open');
    };

    Menu.prototype.closeMenu = function() {
        $('#menu').removeClass('open');
        $('body').removeClass('no-scroll');
    };

    Menu.prototype.openSearch = function() {
        $('body').addClass('no-scroll');
        $('#search').addClass('open');

        setTimeout(function() {
            $('#search').find('input').focus();
        }, 400);
    };

    Menu.prototype.closeSearch = function() {
        $('body').removeClass('no-scroll');
        $('#search').removeClass('open');
    };

    Menu.prototype.openFilters = function() {
        $('body').addClass('no-scroll');
        $('#filter').addClass('open');
        //$('.filter-form').mCustomScrollbar();
    };

    Menu.prototype.closeFilters = function() {
        $('body').removeClass('no-scroll');
        $('#filter').removeClass('open');
    };


    function Payment(scope) {
        this.scope = scope;
        this.slug = (window.location.pathname || '').split('/').pop();
        this.paymentData = {};
    }

    Payment.prototype.init = function() {
        var
            self = this,
            form = $('.payment-form'),
            maxHeight = 0;

        this.form = new Form(form);
        this.panels = $('.panels .panel');
        this.paymentData = {};

        form.on('submit', function() {
            sendPaymentProceed(self.slug, self.paymentData.productId);
        });

        form.find('.product-col').each(function(offset, col) {
            var h = $(this).height();

            if (h > maxHeight) {
                maxHeight = h;
            }
        });

        form.find('.product-col').css('height', maxHeight + 'px');

        form.find('.product-col').on('click', function() {
            var productId = $(this).find('input').val();

            if (self.paymentData.productId && self.paymentData.productId !== productId) {
                sendProductChanged(self.slug, productId, self.paymentData.productId);
            }

            sendProductSelected(self.slug, productId);

            form.find('.product-col').removeClass('active');
            $(this).addClass('active');

            self.paymentData.label = $(this).find('span.label').html();
            self.paymentData.price = $(this).find('span.credits').html() + ' &euro;';
            self.setProductId(productId);

            $('.step2').removeClass('dn');

            setTimeout(function() {
                scrollTo($('.step2').offset().top);
            }, 200);
        });

        form.find('.step2 input').on('change', function() {
            self.updatePaymentData();

            if (self.checkPaymentData()) {
                sendProductScheduled(self.slug, self.paymentData.productId);

                $('.step3').removeClass('dn');

                setTimeout(function() {
                    scrollTo($('.step3').offset().top);
                }, 200);
            }
        });

        $('#fullDay').on('change', function() {
            if (this.checked) {
                $('#startTime').val('00:00');
                $('#endTime').val('23:00');

                $('.step3').removeClass('dn');

                setTimeout(function() {
                    scrollTo($('.step3').offset().top);
                }, 200);
            } else {
                $('#startTime').val('');
                $('#endTime').val('');
            }
        });

        sendPaymentStart(this.slug);
    };

    Payment.prototype.checkPaymentData = function() {
        return !! (
            this.paymentData.productId &&
            this.paymentData.startDate && ((this.paymentData.startTime && this.paymentData.endTime) || this.paymentData.fullDay)
        );
    };

    Payment.prototype.updatePaymentData = function() {
        this.paymentData.productId = $('#productId').val();
        this.paymentData.startDate = $('#startDate').val();
        this.paymentData.startTime = $('#startTime').val();
        this.paymentData.endTime   = $('#endTime').val();
        this.paymentData.fullDay   = $('#fullDay').val();
    };

    Payment.prototype.setProductId = function(id) {
        $('#productId').val(id);
    };


    var MediaError = {
        ERR_ASPECT_RATIO_HORIZONTAL: 'la imagen es demasiado horizontal',
        ERR_ASPECT_RATIO_VERTICAL  : 'la imagen es demasiado vertical',
        ERR_DIMENSION              : 'las dimensiones mínimas de la imagen son 256x256 px',
        ERR_EXTENSION_BLACKLISTED  : 'la extensión :extension no está permitida',
        ERR_EXTENSION_WHITELISTED  : 'la extensión :extension no está permitida',
        ERR_MEDIATYPE_BLACKLISTED  : 'el tipo :mediaType no está permitido',
        ERR_MEDIATYPE_WHITELISTED  : 'el tipo :mediaType no está permitido',
        ERR_SIZE_TOO_SMALL         : ':size es demasiado pequeño (el mínimo es :minSize)',
        ERR_SIZE_TOO_BIG           : ':size es demasiado grande (el máximo es :maxSize)',
        ERR_TOO_MUCH_FILES         : 'has intentado subir demasiados ficheros, inténtalo de nuevo más tarde'
    };


    function Publisher(scope) {
        this.scope = scope;
        this.map = null;
        this.minFee = Infinity;
        this.maxFees = 10;
        this.lookupTimer = null;
        this.lookupTimeout = 1500;
        this.minLookupQueryLength = 5;
        this.chunkSize = 5 * 1024 * 1025;
        this.maxConcurrent = 5;
        this.maxUploadsPerRequest = 5;
        this.maxFiles = 50;
        this.maxFileSize = 250 * 1024 * 1024;
        this.minFileSize = 1;
        this.minFileWidth = 256;
        this.minFileHeight = 256;
        this.maxFileDar = 2.2;
        this.minFileDar = 0.45;
    }

    Publisher.prototype.init = function() {
        var
            self = this;

        $('.menu-publish').hide();

        this.element = $('.publish-form');
        this.scope.maps.create(this.element.find('.map'), {
            marker: {
                draggable: true,
                zoomToPoint: true,
                rlookup: function(address) {
                    self.setAddress(address[0]);
                }
            }
        }, function(map) {
            self.map = map;

            if (self.scope.ad) {
                self.setLocation(self.scope.ad.lat, self.scope.ad.lng);
            }

            $('#address-lookup')
                .on('focus', clearValidationClasses)
                .on('blur', requiredValidator)
                .on('blur', function() {
                    self.updateLocation();
                })
                .on('keyup', function() {
                    clearTimeout(self.lookupTimer);

                    self.lookupTimer = setTimeout(function() {
                        var query = $('#address-lookup').val();

                        if (! query || query.length < self.minLookupQueryLength) {
                            return;
                        }

                        self.scope.maps.lookup(query, function(results) {
                            var latLng = results[0];

                            map.setCenter(latLng);

                            self.scope.maps.placeMarker(map, latLng, {
                                draggable: true,
                                zoomToPoint: true,
                                rlookup: function(address) {
                                    self.setAddress(address[0]);
                                }
                            });
                        });
                    }, self.lookupTimeout);
                });
        });

        this.sectionOffsets = [];
        this.computeSectionOffsets();

        $(document).on('googlemaps.loaded', function() {
            self.computeSectionOffsets();
        });

        this.scope.scroll.add(function(scrollTop) {
            var i, l, item, found = false;

            for (i = 0, l = self.sectionOffsets.length; i < l; i ++) {
                item = self.sectionOffsets[i];

                if (scrollTop >= item.from &&
                    scrollTop < item.to) {
                    self.scope.menu.setCurrentSection(item.section);
                    found = true;
                    break;
                }
            }

            if (! found) {
                self.scope.menu.setCurrentSection(item.section);
            }
        });

        $('.fees .first input')
            .on('focus', clearValidationClasses)
            .on('blur', requiredValidator)
            .on('blur', function() {
                self.updateFees();
            })
            .on('change', function() {
                self.updateFees();
            });

        $('.fees .button').click(function() {
            var fee;

            if (fees.length > self.maxFees) {
                return;
            }

            fee = $($('.fees .tpl').text().replace(/\$OFFSET/g, 1 + fees.length)).appendTo($('.fees-container'));

            fee.find('.delete').click(function() {
                fee.remove();
                self.updateFees();
            });

            fee.find('input')
                .on('focus', self.clearValidationClasses)
                .on('blur', self.requiredValidator)
                .on('blur', function() {
                    self.updateFees();
                })
                .on('change', function() {
                    self.updateFees();
                });

            self.updateFees();
        });

        $('.availability input[type="time"]')
            .on('focus', clearValidationClasses)
            .on('blur', requiredValidator)
            .on('blur', function() {
                self.updateAvailability();
            });

        $('.availability input[type="checkbox"]').change(function() {
            var day = $(this).parent().parent();

            if (this.checked) {
                day.find('input[type="time"]').removeAttr('disabled');
            } else {
                day.find('input[type="time"]').val('').attr('disabled', true);
            }
        }).change();

        $('.availability input[type="radio"]').change(function() {
            var dayAll = $('.day.day_all');

            if ($('.availability input[type="radio"]:checked').val() === 'custom') {
                dayAll.find('input[type="checkbox"]').removeAttr('checked').attr('disabled', true).change();
                dayAll.addClass('dn');

                $('.day').not('.day_all').each(function(offset, node) {
                    var day = $(node);

                    day.find('input[type="checkbox"]').removeAttr('disabled').attr('checked', true).change();
                    day.removeClass('dn');
                });

            } else {
                $('.day').not('.day_all').each(function(offset, node) {
                    var day = $(node);

                    day.find('input[type="checkbox"]').removeAttr('checked').attr('disabled', true).change();
                    day.addClass('dn');
                });

                dayAll.find('input[type="checkbox"]').removeAttr('disabled').attr('checked', true).change();
                dayAll.removeClass('dn');
            }
        }).change();

        // For chunked uploads to work in Mozilla Firefox <7, the multipart
        // option has to be set to false. This is due to Gecko 2.0 (Firefox 4
        // etc.) adding blobs with an empty filename when building a multipart
        // upload request using the FormData interface - see Bugzilla entry
        // #649150 (Fixed in FF 7.0). Several server-side frameworks (inc PHP
        // and Django) cannot handle multipart file uploads with empty
        // filenames
        $('form .uploader').each(function(offset, container) {
            var
                attr,
                uploadContainer = $('.uploader-items', container),
                uploadTemplate = $($('.tpl-media', container).text()),
                maxChunkSize = parseInt(container.getAttribute('data-chunksize-max'), 10) || self.chunkSize,
                maxFiles = parseInt(container.getAttribute('data-files-max'), 10) || self.maxFiles,
                maxFileSize = parseInt(container.getAttribute('data-filesize-max'), 10) || self.maxFileSize,
                minFileSize = parseInt(container.getAttribute('data-filesize-min'), 10) || self.minFileSize,
                maxUploadsPerRequest = parseInt(container.getAttribute('data-uploads-per-request-max'), 10) || self.maxUploadsPerRequest,
                maxConcurrentRequests = parseInt(container.getAttribute('data-concurrent-requests-max'), 10) || self.maxConcurrent,
                mediaTypePreviewable = [
                    'image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/webp'
                ],
                mediaTypeBlacklist = (attr = container.getAttribute('data-mediatype-blacklist')) ? attr.split(/\s+/) : null,
                mediaTypeWhitelist = (attr = container.getAttribute('data-mediatype-whitelist')) ? attr.split(/\s+/) : null,
                extensionBlacklist = (attr = container.getAttribute('data-extension-blacklist')) ? attr.split(/\s+/) : null,
                extensionWhitelist = (attr = container.getAttribute('data-extension-whitelist')) ? attr.split(/\s+/) : null,
                selectable = (container.getAttribute('data-selectable') === 'true'),
                selection = $('#' + container.getAttribute('data-selection')),
                files = [],
                totalFiles = 0,

                formatFileSize = (function() {
                    var i, k = 1000, units = ['Bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

                    return function(bytes, decimals) {
                        if (! bytes) {
                            return '0 Bytes';
                        }

                        i = Math.floor(Math.log(bytes) / Math.log(k));

                        return parseFloat((bytes / Math.pow(k, i)).toFixed(decimals + 1 || 3)) + ' ' + units[i];
                    };
                }()),

                formatErrorLabel = function(file) {
                    return file.name + ' no es válido, ' + file.validationError
                        .replace(':dimension', file.width + '&times;' + file.height)
                        .replace(':extension', file.name.split('.').pop())
                        .replace(':maxFiles', '' + self.maxFiles)
                        .replace(':maxSize', formatFileSize(maxFileSize))
                        .replace(':minSize', formatFileSize(minFileSize))
                        .replace(':mediaType', file.type)
                        .replace(':size', formatFileSize(file.size));
                },

                isPreviewable = function(file) {
                    return Capabilities.FILE_READER_API_SUPPORT && mediaTypePreviewable.indexOf(file.type) > -1;
                },

                isValid = function(file) {
                    var ext = file.name.split('.').pop().toLowerCase();

                    file.validationError = null;

                    if (totalFiles >= maxFiles) {
                        file.validationError = MediaError.ERR_TOO_MUCH_FILES;

                    } else if (minFileSize && file.size < minFileSize) {
                        file.validationError = MediaError.ERR_SIZE_TOO_SMALL;

                    } else if (maxFileSize && file.size > maxFileSize) {
                        file.validationError = MediaError.ERR_SIZE_TOO_BIG;

                    } else if (mediaTypeBlacklist && mediaTypeBlacklist.indexOf(file.type) > -1) {
                        file.validationError = MediaError.ERR_MEDIATYPE_BLACKLISTED;

                    } else if (mediaTypeWhitelist && mediaTypeWhitelist.indexOf(file.type) === -1) {
                        file.validationError = MediaError.ERR_MEDIATYPE_WHITELISTED;

                    } else if (extensionBlacklist && extensionBlacklist.indexOf(ext) > -1) {
                        file.validationError = MediaError.ERR_EXTENSION_BLACKLISTED;

                    } else if (extensionWhitelist && extensionWhitelist.indexOf(ext) === -1) {
                        file.validationError = MediaError.ERR_EXTENSION_WHITELISTED;
                    }

                    return file.validationError;
                },

                isValidDimensions = function(file) {
                    var dar = file.realWidth / file.realHeight;

                    if (file.realWidth < self.minFileWidth ||
                        file.realHeight < self.minFileHeight) {
                        file.validationError = MediaError.ERR_DIMENSION;

                    } else if (dar > self.maxFileDar) {
                        file.validationError = MediaError.ERR_ASPECT_RATIO_HORIZONTAL;

                    } else if (dar < self.minFileDar) {
                        file.validationError = MediaError.ERR_ASPECT_RATIO_VERTICAL;
                    }

                    return file.validationError;
                },

                requestRemove = function(id) {
                    if (id) {
                        $.ajax('/descartar/' + id, {
                            method: 'DELETE',
                            dataType: 'json'
                        });
                    }
                },

                renderTemplate = function(data) {
                    var i, tpl, file, promiseList = [];

                    data.context = $();

                    for (i = 0; i < data.files.length; i ++) {
                        file = data.files[i];
                        file.offset = i;

                        tpl = uploadTemplate.clone();
                        tpl.find('.fname').html(file.name);
                        tpl.find('.ftype').html(file.type);
                        tpl.find('.fsize').html(formatFileSize(file.size));
                        tpl.find('.delete').on('click', function(e) {
                            requestRemove(tpl.find('input').val());

                            e.preventDefault();
                            e.stopPropagation();

                            tpl.remove();

                            files.splice(files.indexOf(file), 1);
                        });

                        if (selectable) {
                            (function(node){
                                node.on('click', function(e) {
                                    e.preventDefault();

                                    if (! node.hasClass('error')) {
                                        uploadContainer.find('.media-item').removeClass('selected');
                                        node.addClass('selected');

                                        if (selection) {
                                            selection.val(node.find('input[type=hidden]').val());
                                        }
                                    }
                                });
                            }(tpl));
                        }

                        if (isPreviewable(file)) {
                            (function(file, node) {
                                promiseList.push(new Promise(function(resolve, reject) {
                                    var reader = new FileReader();
                                    reader.onload = function(e) {
                                        node.find('.media-preview img')
                                            .on('load', function() {
                                                file.realWidth = this.naturalWidth;
                                                file.realHeight = this.naturalHeight;
                                                node.find('.fdar').html(file.realWidth + '&times;' + file.realHeight + 'px');

                                                if (! file.validationError) {
                                                    file.validationError = isValidDimensions(file);

                                                    if (file.validationError) {
                                                        node.addClass('error').find('.error').html(formatErrorLabel(file)).parent().removeClass('dn');
                                                    }
                                                }

                                                resolve();
                                            })
                                            .on('error', function(e) {
                                                reject(e);
                                            })
                                            .attr('src', e.target.result)
                                            .css('width', '100%')
                                            .css('max-width', '100%');
                                    };
                                    reader.readAsDataURL(file);
                                }));
                            }(file, tpl));
                        } else {
                            tpl.find('.media-preview img')
                                .attr('src', '/asset/img/wallaicon.png')
                                .css('width', '100%')
                                .css('max-width', '100%');

                            promiseList.push(Promise.resolve());
                        }

                        if ((file.validationError = isValid(file))) {
                            tpl.addClass('error').find('.error').html(formatErrorLabel(file)).parent().removeClass('dn');
                        } else {
                            files.push(file);
                            totalFiles ++;
                        }

                        file.node = tpl;
                        tpl.appendTo(uploadContainer);
                        data.context = data.context.add(tpl);
                    }

                    return Promise.all(promiseList);
                },

                selectMedia = function(node) {
                    var item = $(node);

                    uploadContainer.find('.media-item').removeClass('selected');
                    item.addClass('selected');

                    if (selection) {
                        selection.val(item.find('input[type=hidden]').val());
                    }
                },

                enableFileInput = function() {
                    $('input[type=file]', container)
                        .prop('disabled', false)
                        .parent()
                        .removeClass('disabled');
                },

                disableFileInput = function() {
                    $('input[type=file]', container)
                        .prop('disabled', true)
                        .parent()
                        .addClass('disabled');
                };

            $('input[type=file]', container).fileupload({
                type: 'POST',
                url: '/ingestar',
                dataType: 'json',
                multipart: true,

                autoUpload: false,
                formData: {},
                limitConcurrentUploads: maxUploadsPerRequest,
                limitMultiFileUploads: maxConcurrentRequests,
                limitMultiFileUploadSize: maxFileSize,
                maxChunkSize: maxChunkSize,
                paramName: 'media',

                // Called when files are added to the fileupload
                // widget (via file input selection, drag & drop
                // or add API call)
                add: function(e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    renderTemplate(data).then(function() {
                        var i, file;

                        for (i = data.files.length - 1; i >= 0; i --) {
                            file = data.files[i];

                            if (file.validationError) {
                                data.files.splice(i, 1);
                            }
                        }

                        if (data.files.length > 0) {
                            data.submit();
                        }
                    });
                },

                // Called on each upload start, equivalent to the
                // global ajaxStart event
                start: function(e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    $('.group-error', container).addClass('dn');

                    $(data.context)
                        .removeClass('idle')
                        .addClass('active');
                },

                // Called on an upload stop, equivalent to the
                // global ajaxStop event
                stop: function(e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    $(data.context)
                        .removeClass('active')
                        .addClass('idle');
                },

                // Called at the start of each file upload request
                send: function(e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    if (data.context && data.dataType && data.dataType.substr(0, 6) === 'iframe') {
                        // Iframe Transport does not support progress events.
                        // In lack of an indeterminate progress bar, we set
                        // the progress to 100% and show an animated bar
                        data.context.find('.media-progress')
                            .addClass(! $.support.transition && 'progress-animated')
                            .attr('aria-valuenow', 100)
                            .children()
                            .first()
                            .css('width', '100%');
                    }
                },

                // Called on upload progress events
                progress: function(e, data) {
                    var progress;

                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    progress = Math.floor(100 * data.loaded / data.total);

                    if (data.context) {
                        data.context.each(function() {
                            $(this)
                                .find('.media-progress')
                                .attr('aria-valuenow', progress)
                                .children()
                                .first()
                                .css('width', progress + '%');
                        });
                    }
                },

                // Called on a successful upload
                done: function(e, data) {
                    var response;
                    
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    response = data.response();

                    if (data.context) {
                        data.context
                            .removeClass('active')
                            .addClass('done')
                            .each(function(offset) {
                                var media = response.result.media[offset] || {
                                    id: null,
                                    error: 'Empty file upload result'
                                };

                                $(this).find('input[type=hidden]').val(media.id);
                                $(this).find('.media-progress')
                                    .attr('aria-valuenow', 100)
                                    .children()
                                    .first()
                                    .css('width', '100%');

                                $(this).on('click', function(e) {
                                    e.preventDefault();

                                    if (! $(e.target).hasClass('material-icons')) {
                                        selectMedia(this);
                                    }
                                });

                                if (! uploadContainer.find('.media-item.selected').length) {
                                    selectMedia(this);
                                }
                            });
                    }
                },

                // Called on a failed upload (abort or error)
                fail: function(e, data) {
                    var response;

                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    response = data.response();

                    if (data.context) {
                        data.context
                            .removeClass('active')
                            .addClass('failed')
                            .each(function(offset) {
                                var file;

                                if (data.errorThrown !== 'abort') {
                                    file = data.files[offset];
                                    file.error = file.error || data.errorThrown || data.i18n('unknownError');

                                    $(this).find('.error').html(file.error).parent().removeClass('dn');
                                }

                                $(this).addClass('failed');
                            });

                    } else if (data.errorThrown !== 'abort') {
                        renderTemplate(data.files).then(function() {
                            data.context.appendTo(uploadContainer).data('data', data);
                        });
                    }
                }
            });

            $('.media-item', container).each(function(offset, node) {
                var item = $(node);

                item.find('.delete').on('click', function(e) {
                    var val = item.find('input').val();

                    if (val) {
                        $.ajax('/descartar/' + val, {
                            method: 'DELETE',
                            dataType: 'json'
                        });
                    }

                    e.preventDefault();
                    item.remove();
                });

                if (selectable) {
                    item.on('click', function(e) {
                        e.preventDefault();

                        if (! $(e.target).hasClass('material-icons')) {
                            selectMedia(item);
                        }
                    });
                }
            });
        });

        if (this.scope.ad) {
            this.setFees();
            this.setAvailability();
            this.setSelects();
            this.setPoster();
        } else {
            this.updateFees();
            this.updateAvailability();
            this.updateLocation();
        }
    };

    Publisher.prototype.computeSectionOffsets = function() {
        var prevOffset, sectionOffsets = this.sectionOffsets = [];

        $('h2').each(function() {
            var
                section = $(this),
                offset = section.offset();

            sectionOffsets.push({
                section: section.attr('id'),
                from: prevOffset,
                to: offset.top
            });

            prevOffset = offset.top;
        });
    };

    Publisher.prototype.setAddress = function(data) {
        if (data === null) {
            $('#address-lookup').val('');
            $('#country').val('');
            $('#postalCode').val('');
            $('#city').val('');
            $('#zone').val('');
            $('#lat').val('');
            $('#lng').val('');
        } else {
            $('#address-lookup').val(data.postalCode + ' ' + (data.cityName || data.city));
            $('#country').val(data.country);
            $('#postalCode').val(data.postalCode);
            $('#city').val(data.city);
            $('#zone').val(data.label);
            $('#lat').val(data.lat);
            $('#lng').val(data.lng);
        }
    };

    Publisher.prototype.setAvailability = function() {
        if (! this.scope.ad.availability) {
            return;
        }

        if (typeof this.scope.ad.availability === 'string') {
            try {
                this.scope.ad.availability = JSON.parse(this.scope.ad.availability);
            } catch (e) {
                this.scope.ad.availability = [];
            }
        }

        this.scope.ad.availability.map(function(item) {
            if (parseInt(item.day, 10) === -1) {
                $('#availability_layout_all').prop('checked', true).change();
                $('#availability_day_all_enabled').prop('checked', true).change();
                $('#availability_day_all_from_time').val(item.from);
                $('#availability_day_all_to_time').val(item.to);
            } else {
                $('#availability_layout_custom').prop('checked', true).change();
                $('#availability_day_' + item.day + '_enabled').click();
                $('#availability_day_' + item.day + '_from_time').val(item.from);
                $('#availability_day_' + item.day + '_to_time').val(item.to);
            }
        });

        this.updateAvailability();
    };

    Publisher.prototype.setFees = function() {
        if (! this.scope.ad.fees) {
            return;
        }

        if (typeof this.scope.ad.fees === 'string') {
            try {
                this.scope.ad.fees = JSON.parse(this.scope.ad.fees);
            } catch (e) {
                this.scope.ad.fees = [];
            }
        }

        this.scope.ad.fees.map(function(fee, offset) {
            var group = $('.fees .fee-group');

            if (offset > 0) {
                $('.fees .button').click();
            }

            group.eq(offset).find('.s6 input').val(fee.desc);
            group.eq(offset).find('.s5 input').val(fee.fee || '');
        });

        this.updateFees();
    };

    Publisher.prototype.setLocation = function(lat, lng) {
        if (lat && lng) {
            this.scope.maps.placeMarker(this.map, new google.maps.LatLng(
                parseFloat(lat),
                parseFloat(lng)
            ), {
                zoomToPoint: true
            });
        }
    };

    Publisher.prototype.setPoster = function() {
        if (this.scope.ad.poster) {
            $('.media-item input[value="' + this.scope.ad.poster + '"]').parent().parent().click();
        }
    };

    Publisher.prototype.setSelect = function(id, val) {
        // IMPORTANT: Make this before materializing
        $('#' + id).val(val);
    };

    Publisher.prototype.setSelects = function() {
        this.setSelect('ethnicity', this.scope.ad.ethnicity);
        this.setSelect('origin', this.scope.ad.origin);
        this.setSelect('occupation', this.scope.ad.occupation);
        this.setSelect('schooling', this.scope.ad.schooling);
    };

    Publisher.prototype.updateAvailability = function() {
        var
            availability = [],
            availabilityLayout = $('.availability input[type="radio"]:checked').val();

        if (availabilityLayout === 'custom') {
            $('.availability .day').not('.day_all').each(function(offset, node) {
                var day = $(node);

                availability.push({
                    day: offset,
                    from: day.find('.input[type="time"]').eq(0).val(),
                    to: day.find('.input[type="time"]').eq(1).val()
                });
            });
        } else {
            availability.push({
                day: -1,
                from: $('#availability_day_all_from_time').val(),
                to: $('#availability_day_all_to_time').val()
            });
        }

        $('#availability').val(JSON.stringify(availability));
    };

    Publisher.prototype.updateFee = function(fee) {
        if (fee < this.minFee) {
            $('#fee').val(fee);
            this.minFee = fee;
        }
    };

    Publisher.prototype.updateFees = function() {
        var
            self = this,
            fees = [];

        $('.fees .fee-group').each(function(offset, node) {
            var fee = parseFloat($('.fee', node).val());

            self.updateFee(fee);

            fees.push({
                fee: fee,
                desc: $('.desc', node).val()
            });
        });

        $('#fees').val(JSON.stringify(fees));
    };

    Publisher.prototype.updateLocation = function() {
    };


    function List(scope) {
        this.scope = scope;
        this.type = 'p';
        this.offset = 1;
        this.limit = 32;
        this.loadThreshold = 55;
        this.toTopThreshold = 100;
        this.loading = false;
        this.done = false;
    }

    List.prototype.init = function() {
        var self = this;
        var slugs = [];

        this.container = $('.grid');
        this.template = $($('.item-template').eq(0).text());
        this.tagTemplate = $($('.tag-item-template').eq(0).text());

        this.scope.scroll.add(function(scrollTop) {
            var
                docHeight = $(document).height(),
                scrollPct = 100 * scrollTop / docHeight;

            if (scrollTop > self.toTopThreshold) {
                $('#totop').removeClass('dn');
            } else {
                $('#totop').addClass('dn');
            }

            if (scrollPct > self.loadThreshold) {
                self.loadMore();
            }
        });

        $('#totop').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            scrollTo(0);
        });

        $('.grid-item .card-image a.db').each(function(offset, item) {
            slugs.push($(item).attr('href'));
        });

        this.initGrid();
        this.initImpressions();

        if (navigator.cookieEnabled && document.cookie) {
            this.scope.cookies.set('cursor', slugs.join(','));
        }
    };

    List.prototype.initGrid = function(appendedItems) {
        var grid = this.container;

        if (! grid.data('masonry')) {
            grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                transitionDuration: '0.5s',
                // nicer reveal transition
                visibleStyle: {
                    transform: 'translateY(0)',
                    opacity: 1
                },
                hiddenStyle: {
                    transform: 'translateY(100px)',
                    opacity: 0
                }
            });
        }

        if (appendedItems) {
            grid.imagesLoaded(function() {
                grid.masonry('appended', appendedItems);
            });
        } else {
            grid.imagesLoaded(function() {
                grid.masonry('layout');
            });
        }
    };

    List.prototype.initImpressions = function() {
        $('.grid-item .card a.db').each(function(offset, item) {
            var label = item.getAttribute('href');
            var id = item.getAttribute('data-rel');

            if (id && label) {
                sendImpression(id, label);
                sendTop(id, label);
            }
        });

        processQueue();
    };

    List.prototype.renderItem = function(item, template) {
        var i, tag, tagNode;
        var node = this.template.clone();
        var separator = '&nbsp;&nbsp;·&nbsp;&nbsp;';
        var parts = [item.name];
        var showFee = item.fee > 0;
        var tagTitle = $('.chip').eq(0).attr('title');

        if (item.age > 0) {
            parts[0] += ', ' + item.age + ' años';
        }

        if (item.category) {
            parts.push(item.category);
        }

        node.find('a.link').prop('href', '/' + item.hash);
        node.find('.card-image img')
            .prop('src', item.poster)
            .prop('alt', item.name)
            .prop('title', item.title);

        node.find('.card-content .subj').html(parts.join(separator));
        node.find('.card-action .loc').html(item.city || item.zone || item.postalCode);
        node.find('.card-action .loc').prop('href', sluggify(item.category) + '/' + sluggify(item.city));

        if (showFee) {
            node.find('.card-content .currency').text(parseInt(item.fee, 10));
            node.find('.card-content .concrete_fee').removeClass('dn');
        } else {
             node.find('.card-content .generic_fee').removeClass('dn');
        }

        if (item.allTags) {
            for (i = 0; i < item.allTags.length; i ++) {
                tag = item.allTags[i];
                tagNode = this.tagTemplate.clone();
                tagNode.addClass('gid-' + item.gid);
                tagNode.attr('href', '/' + item.category + '/' + sluggify(item.city) + '/' + tag.slug);
                tagNode.attr('title', tagTitle);
                tagNode.find('span').html(tag.name);

                node.find('.all-tags').append(tagNode);
            }
        }

        if (item.layout === 'LANDSCAPE') {
            node.addClass(node.attr('data-layout-landscape'));
        } else {
            node.addClass(node.attr('data-layout-portrait'));
        }

        return node;
    };

    List.prototype.addItems = function(data, template) {
        var i, item, slug, cursor, cookie, nodes = $();

        cursor = this.scope.cookies.get('cursor');

        if (cursor) {
            cursor = cursor.split(',');
        } else {
            cursor = [];
        }

        for (i = 0; i < data.length; i++) {
            item = data[i];
            slug = '/' + item.hash;
            cursor.push(slug);

            sendImpression(item.id, slug);

            nodes = nodes.add(
                this.renderItem(item, template)
            );
        }

        if (cursor.length) {
            cookie = cursor.join(',');

            while (cookie.length > 4000) {
                cursor.pop();
                cookie = cursor.join(',');
            }

            this.scope.cookies.set('cursor', cookie);
        }

        processQueue();

        this.container.append(nodes);
        this.initGrid(nodes);
    };

    List.prototype.buildUrl = function() {
        var
            url = '/',
            qs = [];

        if (this.offset) {
            qs.push('offset=' + this.offset);
        }

        if (this.limit) {
            qs.push('limit=' + this.limit);
        }

        if (this.type) {
            qs.push('type=' + this.type);
        }

        if (qs.length) {
            url += '?' + qs.join('&');
        }

        // TODO Add rest of filters

        return url;
    };

    List.prototype.loadMore = function() {
        var self = this;

        if (this.loading || this.done) {
            return;
        }

        this.loading = true;

        $.ajax({
            method: 'GET',
            url: this.buildUrl(),
            dataType: 'json'
        }).done(function(data) {
            var hasPromoted = data.promoted && data.promoted.length;
            var hasRegular = data.regular && data.regular.length;

            if (hasPromoted || hasRegular) {
                if (hasPromoted) {
                    self.addItems(data.promoted, self.promotedTemplate);
                }
                if (hasRegular) {
                    self.addItems(data.regular, self.regularTemplate);
                }
            } else {
                // No more results on current page, so stop querying
                // and loading the server
                self.done = true;
            }
            self.offset ++;
            self.loading = false;
        });
    };


    function Detail(scope) {
        this.scope = scope;
    }

    Detail.prototype.init = function() {
        var
            self = this,
            fabTimeout,
            prevOffset = 0,
            grid = $('.grid'),
            stars = $('.stars'),
            star = stars.find('.star'),
            aside = $('.aside'),
            height = aside.height(),
            width  = aside.width(),
            offsetTop = aside.offset(),
            offsetBottom = $('#ad-owner').offset(),
            sectionOffsets = [],
            freePromotionOverlay = $('#free-promotion-panel');

        this.hit();
        this.chart();
        this.chat();

        this.asidePositionTop = {
            position: 'fixed',
            top: window.innerWidth < 1024 ? 75 : 180,
            left: offsetTop.left,
            height: height,
            width: width
        };
        
        this.asidePositionBottom = {
            position: 'absolute',
            top: offsetBottom.top - height - 50,
            left: offsetTop.left,
            height: height,
            width: width
        };

        // PULSE to fab
        if (document.body.offsetWidth >= 992) {
            this.scope.scroll.add(function(scrollTop) {
                if (scrollTop <= offsetTop.top) {
                    self.unfixAside();
                } else if (scrollTop >= offsetBottom.top - height) {
                    self.fixAsideBottom();
                } else {
                    self.fixAsideTop();
                }
            });
        } else {
            $('.fixed-action-button .button').addClass('pulse');
            setTimeout(function() {
                $('.fixed-action-button .button').removeClass('pulse');
            }, 3000);
        }

        $('a.call-btn').on('click', function(e) {
            var slug = global.location.pathname.split('/');
            var label = '/' + slug[1];
            var element = $('article');
            var id = element.attr('data-rel');

            sendPhoneClick(id, label);
        });

        $('.view-images').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            scrollTo($('#ad-images').offset().top);
        });
        
        $('.show-more').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            $(this).hide().next('.extra').show();
        });

        $('.summarizable').each(function() {
            var
                section = $(this),
                offset = section.offset();

            sectionOffsets.push({
                section: section.attr('id'),
                from: prevOffset,
                to: offset.top
            });
            
            prevOffset = offset.top;
        });

        this.experienceForm = new Form($('.experience-form'), function(data) {
            self.closeExperienceForm();
        }, function() {

        });

        $('#ad-experience-panel').find('.overlay-close').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.closeExperienceForm();
        });

        $('.new-experience').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $('#rating-error').addClass('dn');
            star.removeClass('on').addClass('off');
            self.experienceForm.reset();
            self.openExperienceForm();
        });

        star.on('click', function(e) {
            var offset = parseInt(e.target.parentNode.getAttribute('data-index'), 10);

            e.preventDefault();
            e.stopPropagation();

            star.slice(0, offset).removeClass('off').addClass('on');
            star.slice(offset).removeClass('on').addClass('off');

            $('#rating').val(offset);
            $('.actual-rating').removeClass('dn').html(offset);
            $('.max-rating').removeClass('dn');
        });

        this.scope.scroll.add(function(scrollTop) {
            var i, l, item, found = false;
            
            for (i = 0, l = sectionOffsets.length; i < l; i ++) {
                item = sectionOffsets[i];
                
                if (scrollTop >= item.from && 
                    scrollTop < item.to) {
                    self.scope.menu.setCurrentSection(item.section);
                    found = true;
                    break;
                }
            }
            
            if (! found) {
                self.scope.menu.setCurrentSection(item.section);
            }
        });

        if (window.innerWidth < 1024) {
            this.scope.scroll.add(function(scrollTop) {
                if (scrollTop > 75) {
                    $('nav.menu').addClass('scroll');
                } else {
                    $('nav.menu').removeClass('scroll');
                }

                clearTimeout(fabTimeout);
                fabTimeout = setTimeout(function() {
                    $('.fixed-action-btn.fab-detail').removeClass('active');
                }, 500);
            });

            $('.fixed-action-btn.fab-detail').on('click', function() {
                $(this).toggleClass('active');
            });
        }

        $('.totop').on('click', function() {
            scrollTo(0);
        });

        grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true,
            transitionDuration: '0.5s',
            // nicer reveal transition
            visibleStyle: {
                transform: 'translateY(0)',
                opacity: 1
            },
            hiddenStyle: {
                transform: 'translateY(100px)',
                opacity: 0
            }
        }).imagesLoaded(function() {
            grid.masonry('layout');
        });

        $('.free-promotion-btn').on('click', function(e) {
            e.preventDefault();
            $('body').addClass('no-scroll');
            freePromotionOverlay.addClass('open');
            freePromotionOverlay.find('.overlay-close').removeClass('dn');
            return false;
        });

        freePromotionOverlay.find('.overlay-close').on('click', function(e) {
            e.preventDefault();
            $('body').removeClass('no-scroll');
            freePromotionOverlay.removeClass('open');
            return false;
        });

        this.initCursor();
        this.initImpressions();
    };

    Detail.prototype.initImpressions = function() {
        $('.grid-item .card a.item').each(function(offset, item) {
            var label = item.getAttribute('href');
            var id = item.getAttribute('data-rel');

            if (id && label) {
                sendImpression(id, label);
                sendTop(id, label);
            }
        });

        processQueue();
    };

    Detail.prototype.initCursor = function() {
        var cookie = this.scope.cookies.get('cursor');
        var currentSlug = window.location.pathname;

        if (! cookie) {
            return;
        }

        cookie = cookie.split(',');

        if (! cookie.length) {
            return;
        }

        var i;
        var prev = null;
        var next = null;

        for (i = 0; i < cookie.length; i ++) {
            var slug = cookie[i];

            if (slug === currentSlug) {
                prev = cookie[i - 1] || null;
                next = cookie[i + 1] || null;
                break;
            }
        }

        if (prev) {
            $('.cursor-prev').attr('href', prev).removeClass('dn');
        }

        if (next) {
            $('.cursor-next').attr('href', next).removeClass('dn');
        }
    };

    Detail.prototype.closeExperienceForm = function() {
        $('#ad-experience-panel').removeClass('open');
        $('body').removeClass('no-scroll');
    };

    Detail.prototype.openExperienceForm = function() {
        $('body').addClass('no-scroll');
        $('#ad-experience-panel').addClass('open');
        $('#ad-experience-panel').find('.overlay-close').removeClass('dn');
    };

    Detail.prototype.fixAsideTop = function() {
        $('aside').css(this.asidePositionTop);
    };

    Detail.prototype.fixAsideBottom = function() {
        $('aside').css(this.asidePositionBottom);
    };

    Detail.prototype.unfixAside = function() {
        $('aside').css({
            position: 'relative',
            left: 'auto',
            top: 'auto',
            height: 'auto',
            width: 'auto'
        });
    };

    Detail.prototype.hit = function() {
        var slug = global.location.pathname.split('/');
        var label = '/' + slug[1];
        var element = $('article');
        var id = element.attr('data-rel');

        sendImpression(id, label);
        processQueue();

        $.ajax({
            url: '/' + slug[1] + '/hit',
            method: 'GET',
            xhrFields: {
                withCredentials: true
            }
        });
    };

    Detail.prototype.chart = function() {
        var self = this;
        var slug = global.location.pathname.split('/');

        $.ajax({
            url: '/' + slug[1] + '/stats',
            method: 'GET',
            xhrFields: {
                withCredentials: true
            }
        }).then(function(data) {
            var offset = 0;
            var labels = [];
            var values = [];
            var aggregate = [];
            var numImpressions = 0;
            var numPhoneClicks = 0;
            var numPromotions = 0;
            var numDaysOnTop = 0;
            var key, item, date, parts, sortedKeys = [];

            if (! data) {
                return;
            }

            var element = document.getElementById("stats-chart");
            element.setAttribute('width', element.offsetWidth);
            element.setAttribute('height', 180);
            element.style.marginTop = '-10px';

            for (key in data) {
                sortedKeys.push(key);
            }

            sortedKeys.sort();

            for (key = 0; key < sortedKeys.length; key ++) {
                date = sortedKeys[key];
                item = data[date];

                // Aggregate
                numImpressions += item.numImpressions;
                numPhoneClicks += item.numPhoneClicks;
                numPromotions += item.numPromotions;
                numDaysOnTop += item.numDaysOnTop;

                // Date
                parts = date.split('-');
                date = new Date(
                    parseInt(parts[0], 10),
                    parseInt(parts[1], 10) - 1,
                    parseInt(parts[2], 10)
                );

                labels.push(date);
                values.push(item.numImpressions);
                aggregate.push(numImpressions);
            }

            if (numImpressions) {
                $('#num-impressions').html(numImpressions.toLocaleString());
            } else {
                $('#num-impressions').parent().addClass('dn');
            }

            if (numPhoneClicks) {
                $('#num-phone-clicks').html(numPhoneClicks.toLocaleString());
            } else {
                $('#num-phone-clicks').parent().addClass('dn');
            }

            if (numPromotions) {
                $('#num-promotions').html(numPromotions.toLocaleString());
            } else {
                $('#num-promotions').parent().addClass('dn');
            }

            if (numDaysOnTop) {
                $('#num-days-on-top').html(numDaysOnTop.toLocaleString());
            } else {
                $('#num-days-on-top').parent().addClass('dn');
            }

            if (numImpressions && aggregate.length) {
                $('.ad-stats-figures').removeClass('dn');

                new Chart(element, {
                    type: "line",
                    data: {
                        labels: labels,
                        datasets:[{
                            label: "Impresiones",
                            data: aggregate,
                            fill: true,
                            borderColor:"rgb(75, 192, 192)",
                            backgroundColor:"rgba(75, 192, 192, 0.2)",
                            lineTension: 0.1
                        }]
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    autoSkip: false,
                                    callback: function(date) {
                                        return (0 === (offset ++) % 5) ?
                                            date.getDate() + ' ' + months[date.getMonth()] : '';
                                    }
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: {
                                    display: false
                                }
                            }]
                        }
                    }
                });
            }
        }).fail(function() {
            $('.ad-stats-figures').addClass('dn');
        });
    };

    Detail.prototype.chat = function() {
        var slug = global.location.pathname.split('/');

        $.ajax({
            url: '/' + slug[1] + '/chat',
            method: 'GET',
            xhrFields: {
                withCredentials: true
            }
        }).then(function(data) {
            if (data && data.online) {
                $('#ad-owner .chat-advertiser-connect').addClass('dn');
                $('#chat-peer-connect').removeClass('dn');
                $('.advertiser-is-offline').addClass('dn');
                $('.advertiser-is-online').removeClass('dn');
            } else {
                $('#ad-owner .chat-advertiser-connect').removeClass('dn');
                $('#chat-peer-connect').addClass('dn');
                $('.advertiser-is-online').addClass('dn');
                $('.advertiser-is-offline').removeClass('dn');
            }

            $('.chat-start').removeClass('dn');

        }).fail(function() {
            $('.chat-start').addClass('dn');
        });
    };


    function Manage(scope) {
        this.scope = scope;
        this.currentLink = null;
        this.collection = null;
        this.noResults = null;
        this.grid = null;
        this.creditsPerHour = 1.0;
    }

    Manage.prototype.init = function() {
        var
            self = this,
            timeout = null,
            stateTimeout = null;

        this.collection = $('.card');
        this.noResults = $('.no-results');
        this.grid = $('.grid');

        $('#filter-ads').keyup(function(e) {
            var query = this.value;

            e.preventDefault();
            e.stopPropagation();

            clearTimeout(timeout);
            timeout = setTimeout(function() {
                self.filterAds(query);
            }, 300);
        });

        $('#filter-state').change(function(e) {
            var state = this.value;

            e.preventDefault();
            e.stopPropagation();

            clearTimeout(stateTimeout);
            stateTimeout = setTimeout(function() {
                self.filterAdsByState(state);
            }, 50);
        });

        $('.manage-delete').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.currentLink = $(this);
            $('#manage-deletion').modal('open');

            return false;
        });

        $('.manage-promote-now').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.currentLink = $(this);
            $('#manage-promotion').modal('open');

            return false;
        });

        $('.manage-publish').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.currentLink = $(this);
            $('#manage-publication').modal('open');

            return false;
        });

        $('.manage-modal .accept').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            if (self.currentLink) {
                window.location.href = self.currentLink.attr('href');
            }

            return false;
        });

        $('.manage-modal .dismiss').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.currentLink = null;
            $('.modal').modal('close');

            return false;
        });

        this.initGrid();
    };

    Manage.prototype.initGrid = function(appendedItems) {
        var grid = this.grid;

        if (! grid.data('masonry')) {
            grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                transitionDuration: '0.5s',
                // nicer reveal transition
                visibleStyle: {
                    transform: 'translateY(0)',
                    opacity: 1
                },
                hiddenStyle: {
                    transform: 'translateY(100px)',
                    opacity: 0
                }
            });
        }

        if (appendedItems) {
            grid.imagesLoaded(function() {
                grid.masonry('appended', appendedItems);
            });
        } else {
            grid.imagesLoaded(function() {
                grid.masonry('layout');
            });
        }
    };

    Manage.prototype.filterAds = function(query) {
        this.noResults.addClass('dn');

        if (! query) {
            this.collection.removeClass('dn');
        } else {
            query = query.toLowerCase();

            this.collection.each(function(offset, node) {
                var text = (node.getAttribute('data-search') || '').toLowerCase();

                if (text.indexOf(query) > -1) {
                    $(node).removeClass('dn');
                } else {
                    $(node).addClass('dn');
                }
            });
        }

        if ($('.card.dn').length === this.collection.length) {
            this.noResults.removeClass('dn');
        }

        this.initGrid();
    };

    Manage.prototype.filterAdsByState = function(state) {
        var reciprocalState = {
            draft: 'published',
            published: 'draft'
        };

        this.noResults.addClass('dn');

        if (! state) {
            this.collection.parent().removeClass('dn');
        } else {
            state = state.toLowerCase();

            $('.card.' + reciprocalState[state]).parent().addClass('dn');
            $('.card.' + state).parent().removeClass('dn');
        }

        if ($('.card.dn').length === this.collection.length) {
            this.noResults.removeClass('dn');
        }

        this.initGrid();
    };


    function User(role, name, sessionId) {
        this.role = String(role);
        this.name = String(name);
        this.sessionId = String(sessionId);
    }


    function Chat(user, peer, isPeer) {
        this.user = user;
        this.peer = peer;
        this.isPeer = isPeer;
        this.opened = false;
        this.online = false;
        this.messages = [];
        this.init();
    }

    Chat.PEER_TO_ADVERTISER = 0;
    Chat.ADVERTISER_TO_PEER = 1;
    Chat.current = null;
    Chat.form = null;
    Chat.initialized = false;

    Chat.prototype.init = function() {
        var self = this;

        this.element = $('#chat-session');
        this.element.addClass('online');
        this.element.find('.chat-session-form textarea').val('');
        this.element.find('.chat-session-peer-name').html(this.peer.name);
        this.element.find('.chat-session-peer-time').html(formatTime());

        if (! Chat.initialized) {
            Chat.initialized = true;
            Chat.form = new Form(this.element.find('.chat-session-form form'), function(data) {
                Chat.form.clear();
                Chat.current.post(data);
            }, function(error) {
                Chat.form.clear();
                Chat.current.post($.extend(true, Chat.form.getData(), {
                    error: error
                }));
            });

            this.element.find('.chat-session-messages').mCustomScrollbar();
            this.element.find('.overlay-close').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                Chat.current.close();
            });
        }

        if (this.isPeer) {
            $('#peerSid').val(this.user.sessionId);
            $('#advertiserSid').val(this.peer.sessionId);
        } else {
            $('#peerSid').val(this.peer.sessionId);
            $('#advertiserSid').val(this.user.sessionId);
        }

        if (Chat.current !== this) {
            if (Chat.current) {
                Chat.current.opened = false;
            }

            Chat.current = this;

            this.element.find('.chat-session-messages .chat-scroll .col').html('');
        }
    };

    Chat.prototype.open = function() {
        $('.fixed-action-btn').addClass('dn');
        $('body').addClass('no-scroll');

        this.element.addClass('open');
        this.opened = true;

        $('.fixed-action-btn.fab-send').removeClass('dn');
    };

    Chat.prototype.close = function() {
        $('.fixed-action-btn.fab-send').addClass('dn');

        this.element.removeClass('open');
        this.opened = false;

        $('body').removeClass('no-scroll');
        $('.fixed-action-btn').not('.fab-send').removeClass('dn');
    };

    Chat.prototype.setOnline = function() {
        this.online = true;
        this.element.addClass('online');
    };

    Chat.prototype.setOffline = function() {
        this.online = false;
        this.element.removeClass('online');
    };

    Chat.prototype.post = function(message) {
        var i, l, node;

        if (message instanceof Array) {
            for (i = 0, l = message.length; i < l; i ++) {
                this.post(message[i]);
            }
        } else {
            if (! Chat.template) {
                Chat.template = $($('.chat-session-message-template').text());
            }

            node = Chat.template.clone();

            if (message.id) {
                if (this.isPeer) {
                    message.from = (Chat.PEER_TO_ADVERTISER === parseInt(message.direction, 10)) ?
                        this.user.name : this.peer.name;
                } else {
                    message.from = (Chat.PEER_TO_ADVERTISER === parseInt(message.direction, 10)) ?
                        this.peer.name : this.user.name;
                }
                message.sentAt = formatTime(message.sentAt);
            } else {
                message.from = this.user.name;
                message.sentAt = formatTime();
            }

            if (message.from === this.user.name) {
                node.addClass('u2p');
            } else {
                node.addClass('p2u');
            }

            node.find('.from').html(message.from || this.user.name);
            node.find('.time').html(message.sentAt);
            node.find('.text').html(message.text);

            if (message.error) {
                node.addClass('error');
            }

            this.element.find('.chat-session-messages .chat-scroll .col').append(node);

            this.messages.push(message);
            this.scrollToBottom();
        }
    };

    Chat.prototype.top = function() {
        return this.messages[this.messages.length - 1] || null;
    };

    Chat.prototype.scrollToBottom = function() {
        this.element.find('.chat-session-messages .chat-scroll').mCustomScrollbar('scrollTo', 'bottom');
    };


    function ChatManager(scope) {
        this.scope = scope;
        this.user = null;
        this.isPeer = false;
        this.online = false;
        this.chats = [];

        this.init();
    }

    ChatManager.prototype.init = function() {
        var self = this;

        this.element = $('#chat-sessions');
        this.advertiserConnectPanel = $('#chat-advertiser-connect-panel');
        this.peerConnectPanel = $('#chat-peer-connect-panel');

        $('.chat-advertiser-connect').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.openAdvertiserConnectPanel();
        });

        $('.chat-peer-connect').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            if (self.online && self.chats.length) {
                Chat.current.open();
            } else {
                self.openPeerConnectPanel();
            }
        });

        this.advertiserConnectPanel.find('.overlay-close').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.closeAdvertiserConnectPanel();
        });

        this.advertiserConnectPanel.find('input').on('focus', function() {
            $('.invalid-advertiser-email').addClass('dn');
        });

        this.peerConnectPanel.find('.overlay-close').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            self.closePeerConnectPanel();
        });

        this.advertiserConnectForm = new Form($('.online-advertiser-form'), function(data) {
            self.closeAdvertiserConnectPanel();
            self.startAdvertiserSession(data);
        }, function(xhr) {
            if (xhr.status === 400) {
                $('.invalid-advertiser-email').removeClass('dn');
            } else if (xhr.status >= 500) {
                // Display error
            }
        });

        this.peerConnectPanel.find('input').on('focus', function() {
            $('.invalid-peer-display-name').addClass('dn');
            $('.no-loopback-chat').addClass('dn');
        });

        this.peerConnectForm = new Form($('.online-peer-form'), function(data) {
            self.closePeerConnectPanel();
            self.startPeerSession(data);
        }, function() {
            if (xhr.status === 400) {
                $('.invalid-peer-display-name').removeClass('dn');
            } else if (xhr.status >= 500) {
                $('.no-loopback-chat').removeClass('dn');
            } else if (xhr.status >= 500) {
                // Display error
            }
        });
    };

    ChatManager.prototype.close = function() {
        this.element.removeClass('open');

        $('body').removeClass('no-scroll');
        $('.fixed-action-btn').removeClass('dn');
    };

    ChatManager.prototype.open = function() {
        this.element.find('.chat-sessions-user-name').html(this.user.name);

        $('.fixed-action-btn').addClass('dn');
        $('body').addClass('no-scroll');

        this.element.addClass('open');
    };

    ChatManager.prototype.closeAdvertiserConnectPanel = function() {
        this.advertiserConnectPanel.removeClass('open');

        $('body').removeClass('no-scroll');
        $('.fixed-action-btn').removeClass('dn');
    };

    ChatManager.prototype.openAdvertiserConnectPanel = function() {
        $('.invalid-advertiser-email').addClass('dn');
        $('.fixed-action-btn').addClass('dn');
        $('body').addClass('no-scroll');

        this.advertiserConnectPanel.addClass('open');
    };

    ChatManager.prototype.closePeerConnectPanel = function() {
        this.peerConnectPanel.removeClass('open');

        $('body').removeClass('no-scroll');
        $('.fixed-action-btn').removeClass('dn');
    };

    ChatManager.prototype.openPeerConnectPanel = function() {
        $('.invalid-peer-display-name').addClass('dn');
        $('.no-loopback-chat').addClass('dn');
        $('.fixed-action-btn').addClass('dn');
        $('body').addClass('no-scroll');

        this.peerConnectPanel.addClass('open');
    };

    ChatManager.prototype.startAdvertiserSession = function(data) {
        var i, l;

        this.online = data.online;
        this.user = data.user;
        this.isPeer = false;

        if (data.chats && data.chats.length) {
            for (i = 0, l = data.chats.length; i < l; i ++) {
                this.createNewChatSession(data.chats[i].peer);
            }
        }

        this.open();
        this.refresh();
    };

    ChatManager.prototype.startPeerSession = function(data) {
        var chat;

        this.online = data.online;
        this.user = data.user;
        this.peer = data.peer;
        this.isPeer = true;

        this.createNewChatSession(data.peer);

        chat = this.chats[0];
        chat.open();

        if (data.messages && data.messages.length) {
            chat.post(data.messages);
        }

        this.refresh();
    };

    ChatManager.prototype.createNewChatSessionEntry = function(peer) {

    };

    ChatManager.prototype.createNewChatSession = function(peer) {
        var chat;

        if (! this.isPeer) {
            this.createNewChatSessionEntry(peer);
        }

        chat = new Chat(this.user, peer, this.isPeer);
        chat.init();

        this.chats.push(chat);
    };



    ChatManager.prototype.getChatSession = function(sessionId) {
        var i, l;

        for (i = 0, l = this.chats.length; i < l; i ++) {
            if (this.chats[i].sessionId === sessionId) {
                return this.chats[i];
            }
        }

        return null;
    };

    ChatManager.prototype.refresh = function() {
        var self = this;

        if (! this.user) {
            return;
        }

        $.ajax({
            url: '/chat',
            method: 'GET',
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            }
        }).then(function(data) {
        }).fail(function(error) {
        });
    };


    function App(config) {
        this.scope = {};
        this.boot(config || {
            apiKey: $('.map').attr('data-api-key')
        });
    }

    App.prototype.get = function(name) {
        if (! this.scope.hasOwnProperty(name)) {
            throw new Error('Service ' + name + ' not found in app scope.');
        }

        return this.scope[name];
    };

    App.prototype.setAd = function(data) {
        this.scope.ad = data || {};
    };

    App.prototype.boot = function(config) {
        var self = this;

        this.scope.ad = {};
        this.scope.captcha = new Captcha(this.scope);
        this.scope.chat = new ChatManager(this.scope);
        this.scope.cookies = new CookieManager(this.scope);
        this.scope.detail = new Detail(this.scope);
        this.scope.filter = new Filter(this.scope);
        this.scope.fingerprint = new Fingerprint(this.scope);
        this.scope.list = new List(this.scope);
        this.scope.location = new Location(this.scope);
        this.scope.manage = new Manage(this.scope);
        this.scope.maps = new Maps(this.scope, config.apiKey);
        this.scope.menu = new Menu(this.scope);
        this.scope.payment = new Payment(this.scope);
        this.scope.publish = new Publisher(this.scope);
        this.scope.scroll = new Scroll(this.scope);

        $(document).ready(function() {
            $('select').formSelect();
            $('.fixed-action-btn').floatingActionButton();
            //$('.materialboxed').materialbox();
            $('.modal').modal();

            if (IS_DESKTOP) {
                $('.datepicker').datepicker({
                    autoClose: true,
                    format: 'yyyy-mm-dd',
                    firstDay: 1,
                    minDate: new Date(),
                    i18n: {
                        cancel: 'Cerrar',
                        'clear': 'Borrar',
                        done: 'Ok',
                        months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                        weekdaysShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                        weekdaysAbbrev: ['D','L','M','X','J','V','S'],
                    }
                });

                $('.timepicker').timepicker({
                    autoClose: true,
                    twelveHour: false,
                    i18n: {
                        cancel: 'Cerrar',
                        clear: 'Borrar',
                        done: 'Ok'
                    }
                });
            }
            $('.parallax').parallax();
            $('.tabs').tabs();
            $('.carousel.carousel-slider').carousel({
                fullWidth: true,
                indicators: true
            });

            baguetteBox.run('.materialboxed');

            self.run();
        });
    };

    App.prototype.run = function() {
        var body = $('body');

        this.scope.cookies.init();
        this.scope.menu.init();
        this.scope.scroll.init();

        if (body.hasClass('has-captcha')) {
            this.scope.captcha.load();

        }
        if (body.hasClass('has-filters')) {
        }

        if (body.hasClass('has-maps')) {
            this.scope.maps.load();
        }

        if (body.hasClass('page-index')) {
            this.scope.list.init();
        }

        if (body.hasClass('page-detail')) {
            this.scope.detail.init();
        }

        if (body.hasClass('page-publish')) {
            this.scope.filter.disable();
            this.scope.publish.init();
        } else {
            this.scope.filter.init();
        }

        if (body.hasClass('page-manage')) {
            this.scope.manage.init();
        }

        if (body.hasClass('page-payment')) {
            this.scope.payment.init();
        }
    };

    global.App = App;

}(window, window.document, jQuery));
