<?php

use Phalcon\Debug;
use Phalcon\Di\FactoryDefault;
use Phalcon\Logger;
use Phalcon\Mvc\Application;

error_reporting(\E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '1024M');

define('BASE_PATH', dirname(dirname(__DIR__)));
define('APP_PATH', BASE_PATH . '/src/App');

if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
    $debug = new Debug();
    $debug->listen();
}

try {

    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    /**
     * Read services. This must set the configuration as a service.
     */
    include APP_PATH . '/Resources/config/services.php';

    /**
     * Include Autoloader
     */
    include APP_PATH . '/Resources/config/loader.php';

} catch (\Throwable $t) {
    die(sprintf('Error booting app: %s<br/><pre>%s</pre>', $t->getMessage(), $t->getTraceAsString()));

} catch (\Exception $e) {
    die(sprintf('Exception booting app: %s<br/><pre>%s</pre>', $e->getMessage(), $e->getTraceAsString()));
}

/*
 * Logging data to an adapter i.e. File (file system) is always an expensive
 * operation in terms of performance. To combat that, you can take advantage
 * of logging transactions. Transactions store log data temporarily in memory
 * and later on write the data to the relevant adapter (File in this case)
 * in a single atomic operation.
 */
$di->get('logger')->begin();

try {

    /**
     * Handle the request
     */
    $application = new Application($di);

    echo $application->handle()->getContent();

} catch (\Throwable $t) {
    $message = $t->getMessage();

    if ($di->get('config')->app->debug) {
        $message .= '. '. $t->getTraceAsString();
    }

    $di->get('logger')->log(Logger::ERROR, $message);

    die(sprintf('<!DOCTYPE html><html><head></head><body><h1>%s</h1><pre>%s</pre></body></html>', $t->getMessage(), $t->getTraceAsString()));

} catch (\Exception $e) {
    $message = $e->getMessage();

    if ($di->get('config')->app->debug) {
        $message .= '. '. $e->getTraceAsString();
    }

    $di->get('logger')->log(Logger::ERROR, $message);

    die(sprintf('<!DOCTYPE html><html><head></head><body><h1>%s</h1><pre>%s</pre></body></html>', $e->getMessage(), $e->getTraceAsString()));

} finally {
    $di->get('logger')->commit();
}
