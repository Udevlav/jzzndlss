(function(window) {

    function User() {
        this.ip = null;
        this.email = null;
        this.phone = null;
        this.anonymous = true;
        this.fingerprint = null;
        this.tokens = [];
    }


    function State() {
        this.parameters = {};
        this.services = {};
    }


    function Service(scope, instanceOrFactory) {
        this.scope = scope;
        this.initialized = false;

        if (typeof instanceOrFactory === 'function') {
            this.instance = null;
            this.factory = instanceOrFactory;
        } else if (typeof instanceOrFactory === 'object') {
            this.instance = instanceOrFactory;
            this.factory = null;
        } else {
            throw new TypeError('Invalid service instance.');
        }
    }

    Service.prototype.getInstance = function() {
        if (! this.instance) {
            this.instance = this.factory();
        }

        if (! this.initialized) {
            this.initialized = true;

            if (typeof this.instance.init === 'function') {
                try {
                    this.instance.init(this.scope);
                } catch (e) {
                    throw new Error('Failed to initialize service ' + name + ': ' + e.message);
                }
            }
        }

        return this.instance;
    };


    function Scope() {
        this.parameters = {};
        this.services = {};
    }

    Scope.prototype.getParam = function(name) {
        if (! this.parameters.hasOwnProperty(name)) {
            throw new Error('No such parameter ' + name);
        }

        return this.parameters[name];
    };

    Scope.prototype.setParam = function(name) {
        this.parameters[name] = value;

        return this;
    };

    Scope.prototype.get = function(name) {
        if (! this.services.hasOwnProperty(name)) {
            throw new Error('No such service ' + name);
        }

        return this.services[name].getInstance();
    };

    Scope.prototype.set = function(name, instanceOrFactory) {
        this.services[name] = new Service(instanceOrFactory);

        return this;
    };


    function App(bootstrapper) {
        if (typeof bootstrapper !== 'function') {
            throw new TypeError('The App bootstrapper should be a function.');
        }

        this.state = new State();
        this.scope = new Scope();
        this.user = new User();

        bootstrapper.call(this);
    }

    App.prototype.run = function() {
        try {
            this.handleState();
        } catch (e) {
            throw new Error('Error handling state ' + state + ': ' + e.message);
        }
    };

    App.prototype.handleState = function() {

    };

}());