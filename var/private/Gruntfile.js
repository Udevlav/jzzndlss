module.exports = function(grunt) {

    /**
     * Project configuration
     */
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        /**
         * Compile SASS files to CSS.
         *
         * @see https://github.com/gruntjs/grunt-contrib-sass
         */
        sass: {
            compileSources: {
                options: {
                    style: "expanded"
                },
                files: {
                    "src/css/package.css": "src/sass/main.scss"
                }
            }
        },

        /**
         * Babel ES6 transpiler.
         *
         * @see https://github.com/babel/grunt-babel
         */
        babel: {
            options: {
                sourceMap: true,
                plugins: ["transform-es2015-modules-amd"],
                presets: ['es2015']
            },
            transpile: {
                files: [{
                    expand: true,
                    cwd: 'src/js/',
                    src: ['**/*.js'],
                    dest: 'dist/js/',
                    ext: '.src.js'
                }]
            }
        },

        /**
         * Runs JsHint.
         *
         * @see https://github.com/gruntjs/grunt-contrib-jshint
         */
        jshint: {
            options: {
                force: true,
                extensions: 'js',
                ignores: ['**/*.js.min', 'Gruntfile.js'],
                jshintrc: '<%= baseDir %>.jshintrc',
                reporter: require('jshint-stylish-ex')
            },
            grunt: ['Gruntfile.js'],
            checkSources: {
                files: {
                    src: ['Gruntfile.js', 'src/js/**/*.js']
                }
            }
        },

        /**
         * Concatenates files.
         *
         * @see https://github.com/gruntjs/grunt-contrib-concat
         */
        concat: {
            cssBundle: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> CSS Bundle (custom) ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    stripBanners: true,
                    separator: grunt.util.linefeed + grunt.util.linefeed
                },
                files: {
                    'dist/css/app-src.css': [
                        'src/css/package.css',
                        'src/css/package-extra.css'
                    ]
                }
            },
            cssVendor: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> CSS Vendors Bundle ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    stripBanners: true,
                    separator: grunt.util.linefeed + grunt.util.linefeed
                },
                files: {
                    'dist/css/app-vendor.min.css': [
                        'vendor/materialize/materialize.min.css',
                        'vendor/scrollbar/jquery.mCustomScrollbar.min.css',
                    ]
                }
            },
            css: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> CSS Bundle (custom + vendors)' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    stripBanners: true,
                    separator: grunt.util.linefeed + grunt.util.linefeed
                },
                files: {
                    'dist/css/app.css': [
                        'dist/css/app-vendor.min.css',
                        'dist/css/app-src.css',
                    ]
                }
            },
            cssMin: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> CSS Bundle (custom + vendors)' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    stripBanners: true
                },
                files: {
                    'dist/css/app.min.css': [
                        'dist/css/app-vendor.min.css',
                        'dist/css/app-src.min.css',
                    ]
                }
            },
            jqFileUpload: {
                options: {
                    separator: grunt.util.linefeed + grunt.util.linefeed,
                    stripBanners: true,
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> jq-file-upload ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n"use strict";\n',
                    process: function(src, filepath) {
                        return '// Source: ' + filepath + '\n' + src;
                    }
                },
                files: {
                    'vendor/jquery-file-upload/dist/jquery.fileupload.pack.js': [
                        'vendor/jquery-file-upload/vendor/jquery.ui.widget.js',
                        'vendor/jquery-file-upload/cors/jquery.iframe-transport.js',
                        'vendor/jquery-file-upload/cors/jquery.postmessage-transport.js',
                        'vendor/jquery-file-upload/cors/jquery.xdr-transport.js',
                        'vendor/jquery-file-upload/jquery.fileupload.js',
                        'vendor/jquery-file-upload/jquery.fileupload-process.js',
                        'vendor/jquery-file-upload/jquery.fileupload-validate.js',
                    ]
                }
            },
            jsBundle: {
                options: {
                    separator: grunt.util.linefeed + grunt.util.linefeed,
                    stripBanners: true,
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> App JS Bundle (custom) ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n"use strict";\n',
                    process: function(src, filepath) {
                        return '// Source: ' + filepath + '\n' +
                            // Replace all 'use strict' statements in the code with a single one at the  top
                            src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                    }
                },
                files: {
                    'dist/js/app-src.js': [
                        'src/js/cookies.js',
                        'src/js/common.js',
                        'src/js/index.js',
                        'src/js/detail.js',
                        'src/js/captcha.js',
                        'src/js/promote.js',
                        'src/js/publish.js',
                        'src/js/contact.js'
                    ]
                }
            },
            jsVendor: {
                options: {
                    stripBanners: true,
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> App JS Vendors Bundle ' +
                       '<%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    process: function(src, filepath) {
                        return '// Source: ' + filepath + '\n' + src;
                    }
                },
                files: {
                    'dist/js/app-vendor.min.js': [
                        'vendor/html5shiv/html5shiv.min.js',
                        'vendor/modernizr/modernizr.custom.min.js',
                        'vendor/fingerprintjs2/fingerprintjs2.min.js',
                        'vendor/jquery/jquery-3.1.1.min.js',
                        //'vendor/bootstrap/bootstrap.min.js',
                        'vendor/materialize/materialize.min.js',
                        'vendor/masonry/masonry.min.js',
                        'vendor/isotope/isotope.min.js',
                        'vendor/load-image/load-image.custom.min.js',
                        'vendor/jquery-file-upload/dist/jquery.fileupload.pack.min.js',
                        'vendor/chart/chart.bundle.min.js',
                        'vendor/scrollbar/jquery.mCustomScrollbar.concat.min.js',
                    ]
                }
            },
            js: {
                options: {
                    stripBanners: true,
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> App JS Bundle (custom + vendor) ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n"use strict";\n'
                },
                files: {
                    'dist/js/app.js': [
                        'dist/js/app-vendor.min.js',
                        'dist/js/app-src.js'
                    ]
                }
            },
            jsMin: {
                options: {
                    stripBanners: true,
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> App JS Bundle (custom + vendor) ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n"use strict";\n'
                },
                files: {
                    'dist/js/app.min.js': [
                        'dist/js/app-vendor.min.js',
                        'dist/js/app-src.min.js'
                    ]
                }
            }
        },

        /**
         * Minifies CSS.
         *
         * @see https://github.com/gruntjs/grunt-contrib-cssmin
         */
        cssmin: {
            cssBundle: {
                files: [{
                    expand: true,
                    cwd: 'dist/css',
                    src: ['app-src.css'],
                    dest: 'dist/css',
                    ext: '.min.css'
                }]
            }
        },

        /**
         * Minify files with UglifyJS.
         *
         * @see https://github.com/gruntjs/grunt-contrib-uglify
         * @see http://lisperator.net/uglifyjs/compress
         */
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: true,
                sourceMap: false,
                compress: {
                    sequences     : true,  // join consecutive statemets with the “comma operator”
                    properties    : true,  // optimize property access: a["foo"] → a.foo
                    dead_code     : true,  // discard unreachable code
                    drop_debugger : true,  // discard “debugger” statements
                    unsafe        : false, // some unsafe optimizations (see below)
                    conditionals  : true,  // optimize if-s and conditional expressions
                    comparisons   : true,  // optimize comparisons
                    evaluate      : true,  // evaluate constant expressions
                    booleans      : true,  // optimize boolean expressions
                    loops         : true,  // optimize loops
                    unused        : true,  // drop unused variables/functions
                    hoist_funs    : true,  // hoist function declarations
                    hoist_vars    : false, // hoist variable declarations
                    if_return     : true,  // optimize if-s followed by return/continue
                    join_vars     : true,  // join var declarations
                    cascade       : true,  // try to cascade `right` into `left` in sequences
                    side_effects  : true,  // drop side-effect-free statements
                    warnings      : true,  // warn about potentially dangerous optimizations/code
                    global_defs   : {}     // global definitions
                }
            },
            jqFileUpload: {
                src: 'vendor/jquery-file-upload/dist/jquery.fileupload.pack.js',
                dest: 'vendor/jquery-file-upload/dist/jquery.fileupload.pack.min.js'
            },
            jsBundle: {
                src: 'dist/js/app-src.js',
                dest: 'dist/js/app-src.min.js'
            }
        },

        /**
         * String replacement.
         *
         * @see https://github.com/eruizdechavez/grunt-string-replace
         */
        'string-replace': {
        },

        /**
         * Copies files.
         *
         * @see https://github.com/gruntjs/grunt-contrib-copy
         */
        copy: {
            css: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['app.css'],
                    dest: '../public/asset/css/'
                }]
            },
            cssMin: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['app.min.css'],
                    dest: '../public/asset/css/'
                }]
            },
            js: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['app.js'],
                    dest: '../public/asset/js/'
                }]
            },
            jsBundle: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['bundle.js'],
                    dest: '../public/asset/js/'
                }]
            },
            jsMin: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['app.min.js'],
                    dest: '../public/asset/js/'
                }]
            },
            jsVendor: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['vendor.min.js'],
                    dest: '../public/asset/js/'
                }]
            },
            serviceWorker: {
                files: [{
                    expand: false,
                    cwd: 'src/js',
                    src: ['sw.js'],
                    dest: '../public/asset/js/'
                }]
            }
        },

        /**
         * @see https://github.com/gruntjs/grunt-contrib-watch
         */
        watch: {
            options: {
                dateFormat: function(time) {
                    grunt.log.writeln('Completed in ' + time + 'ms at ' + (new Date()).toString());
                    grunt.log.writeln('Waiting for more changes ...');
                }
            },
            dev: {
                files: [
                    'src/js/**/*.js',
                    'src/sass/**/*.less',
                ],
                interval: 2500,
                tasks: ['dev'],
                options: {
                    spawn: false
                },
            }
        }
    });

    // Load grunt plugins that are used to provide tasks
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s)
    grunt.registerTask('css', [
        'sass:compileSources',
        'concat:cssBundle',
        'cssmin:cssBundle',
        'concat:cssVendor',
        'concat:css',
        'concat:cssMin',
        'copy:css',
        'copy:cssMin'
    ]);

    grunt.registerTask('js', [
        //'babel:transpile',
        'jshint:checkSources',
        'concat:jsBundle',
        'uglify:jsBundle',
        'concat:jqFileUpload',
        'uglify:jqFileUpload',
        'concat:jsVendor',
        'concat:js',
        'concat:jsMin',
        'copy:js',
        'copy:jsMin',
        'copy:serviceWorker'
    ]);

    grunt.registerTask('default', [
        'css',
        'js'
    ]);

    grunt.registerTask('dev', [
        'css',
        'js',
        'copy:jsBundle',
        'copy:jsVendor'
    ]);
};