{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}

{% block content %}
    <div class="container">
        <div class="row">
            <div class="col s12 m8 static_content mt75">
                <h1 id="privacy_policy" class='x24 dark bold mt100 mb25'>{{ helper.trans('app.label.privacy') }}</h1>
                <div class="x14 grey">
                    {{ helper.trans('app.text.privacy') }}
                </div>
                <h1 id="cookie_policy" class='x24 dark bold mt25 mb25'>{{ helper.trans('app.label.privacy') }}</h1>
                <div class="x14 grey">
                    {{ helper.trans('app.text.cookies') }}
                </div>
            </div>
        </div>
    </div>
{% endblock %}