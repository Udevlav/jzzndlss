{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}

{% block content %}
    <div class="container">
        <div class="row">
            <div class="col s12 m8 offset-m2 mt75">
                <h1 class='x24 dark bold mt100 mb25'>{{ helper.trans('app.label.tos') }}</h1>
                <div class="x14 grey">
                    {{ helper.trans('app.text.tos') }}
                </div>
            </div>
        </div>
    </div>
{% endblock %}