{% extends 'layout.volt' %}

{% block title%}{{ title }}{% endblock %}

{% block content %}
    <section class="error">
        <div class="container">
            <h1 class="x40 mt100 mb15">{{ helper.trans('error.title.rate_limit_exceeded') }}</h1>
            <p class="x24 grey">{{ helper.trans('error.label.rate_limit_exceeded') }}</p>
            <a class="db x16 light mt15 mb50" href="{{ helper.link('@index') }}">{{ helper.trans('app.label.back_to_home') }}</a>
        </div>
    </section>
    <section class="trace bglight pt25 pb25">
        <div class="container">
            {% if config.app.debug %}
            {% endif %}
        </div>
    </section>
{% endblock %}
