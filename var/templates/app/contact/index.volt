{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-contact has-captcha{% endblock %}

{% block content %}
    <article class="container">
        <form class="contact-form pt25 mb0" action="{{ helper.link('@contact') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" id="_csrf" name="_csrf" value="{{ form.getCsrfToken() }}" />

            <fieldset>
                <div class="row">
                    <div class="col s12">
                        <h1 class="x40 dark bold mt50 mb25 ml40">{{ helper.trans('contact.title.contact') }}</h1>
                    </div>
                </div>
                <div class="row">
                    {{ partial('theme/form/email', ['entity': entity, 'field':form.get('email')]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/textarea', ['entity': entity, 'field':form.get('text')]) }}
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="g-recaptcha ml40" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
                    </div>
                </div>
                <div class="row mb50">
                    <div class="col s12">
                        <p class="x12 grey mb10 ml40">{{ helper.trans('contact.help.disclaimer') }}</p>

                        <button class="button x18 big red submit ml40">
                            {{ helper.trans('contact.link.contact') }}
                        </button>
                    </div>
                </div>
            </fieldset>
        </form>
    </article>
{% endblock %}