<section>
    <div class="container">
        <div class="row">
            {#
            <div class="col s3">
                <h1 class="x40 mt100">{{ advertiser.credits }}</h1>
                <p class="num-credits x24 grey">{{ helper.trans('advertiser.label.credits') }}</p>
            </div>
            #}
            <div class="col s3">
                <h2 class="x40 mt100">{{ ads | length }}</h2>
                <p class="num-ads x24 grey">{{ helper.trans('advertiser.label.ads') }}</p>
            </div>
        </div>
    </div>
</section>