<section class="bglight pt25 pb25">
    <form class="container" action="#">
        <div class="row">
            <div class="input-field col s6">
                <input autocomplete="off" type="text" id="filter-ads" name="filter_ads" placeholder="{{ helper.trans('advertiser.label.filter') }}" />
                <label for="filter-ads">{{ helper.trans('manage.label.filter') }}</label>
            </div>
            <div class="input-field col s6">
                <select id="filter-state">
                    <option value="" selected="selected">{{ helper.trans('manage.label.any') }}</option>
                    <option value="published">{{ helper.trans('manage.label.published') }}</option>
                    <option value="draft">{{ helper.trans('manage.label.draft') }}</option>
                </select>
                <label for="filter-state">{{ helper.trans('manage.label.state') }}</label>
            </div>
        </div>
    </form>
</section>
