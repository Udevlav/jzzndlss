<section class="bglight pt25 pb25">
    <div class="container">
        <p class="no-results x36 alc lighter pt25 pb100 dn">{{ helper.trans('manage.label.noResults') }}</p>
        <div class="row grid">
            {% if ads|length < 10 %}
                {% set num_cols_landscape = 12 %}
                {% set num_cols_portrait = 6 %}
                <div class="grid-sizer col s6"></div>
            {% else %}
                {% set num_cols_landscape = 6 %}
                {% set num_cols_portrait = 6 %}
                <div class="grid-sizer col s6"></div>
            {% endif %}

            {% for ad in ads %}
                <div class="col s12 m{% if ad.isPortrait() %}{{ num_cols_portrait }}{% else %}{{ num_cols_landscape }}{% endif %} mb50 grid-item">
                    <div class="card{% if ad.promoted %} promoted{% endif %}{% if ad.publishedAt %} published{% else %} draft{% endif %}" data-search="{{ ad.name }} {{ ad.city }} {{ ad.title }}">
                        {% if ad.promoted %}
                            <div class="dbadge">{{ helper.trans('app.label.promoted') }}</div>
                        {% endif %}

                        <div class="card-image">
                            <img src="{{ helper.poster(ad) }}">
                            {% if ad.publishedAt %}
                                <a href="{{ helper.link('@promote', advertiser.token, ad.slug) }}" class="btn-floating halfway-fab waves-effect waves-light red manage-promote" title="{{ helper.trans('app.link.promote_ad') }}" aria-label="{{ helper.trans('app.link.promote_ad') }}">
                                    <i class="material-icons">trending_up</i>
                                </a>
                            {% else %}
                                <a href="{{ helper.link('@confirm_publication', advertiser.token, ad.slug) }}" class="btn-floating halfway-fab waves-effect waves-light red manage-publish" title="{{ helper.trans('app.link.confirm_publication') }}" aria-label="{{ helper.trans('app.link.confirm_publication') }}">
                                    <i class="material-icons">check</i>
                                </a>
                            {% endif %}
                        </div>
                        <div class="card-content">
                            <p class="x16 lred bold upper mb5">{{ ad.name }}{% if ad.age > 0 %}, {{ helper.trans('ad.label.aged', ad.age) }}{% endif %}</p>
                            <p class="x20 lh25 dark bold mb15">{{ helper.substr(ad.title, 40) }}</p>
                            <p class="x16 lh22 mb15">
                                <span class="grey">{{ helper.trans('app.label.in') }}&nbsp;{{ ad.city }}, {% if ad.publishedAt %}{{ helper.timeago(ad.publishedAt) }}{% else %}{{ helper.timeago(ad.createdAt) }}{% endif %}</span>
                            </p>
                            <p class="x14 lh22 light">
                                <a href="{{ helper.link('@ad_seo', ad.hash) }}" class="chip dib mr15" title="{{ ad.getSeoLabel() }}">
                                    <span class="vm light pdl5">Enlace permanente </span>
                                    <i class="material-icons vm">insert_link</i>
                                </a>
                                <span class="dib mr15">
                                    <i class="material-icons lighter vm">insert_photo</i>
                                    <span class="vm light pdl5">&times; {{ helper.trans('ad.label.num_images', ad.numImages) }}</span>
                                </span>
                                {% if ad.numVideos %}
                                    <span class="dib mr15">
                                        <i class="material-icons lighter vm">ondemand_video</i>
                                        <span class="vm light pdl5">&times; {{ helper.trans('ad.label.num_videos', ad.numVideos) }}</span>
                                    </span>
                                {% endif %}
                            </p>
                            {{ partial('theme/list/tagsall', ['item': ad]) }}
                        </div>
                        <div class="card-action">
                            {% if ad.publishedAt %}
                                <a class="button fill white" href="{{ helper.link('@ad_seo', ad.hash) }}" title="{{ helper.trans('app.link.view_ad') }}">
                                    <span class="material-icons grey x16 dib vm">remove_red_eye</span>
                                    <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.view_ad') }}</span>
                                </a>
                                <a class="button fill white" href="{{ helper.link('@edit', advertiser.token, ad.slug) }}" title="{{ helper.trans('app.link.edit_ad') }}">
                                    <span class="material-icons grey x16 dib vm">mode_edit</span>
                                    <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.edit_ad') }}</span>
                                </a>
                                <a class="manage-delete button fill white" href="{{ helper.link('@delete', advertiser.token, ad.slug) }}" title="{{ helper.trans('app.link.delete_ad') }}">
                                    <span class="material-icons grey x16 dib vm">delete</span>
                                    <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.delete_ad') }}</span>
                                </a>
                                {% if not ad.promoted %}
                                    <a class="manage-promote-now button fill white" href="{{ helper.link('@promote_now', advertiser.token, ad.slug) }}" title="{{ helper.trans('app.link.promote_now') }}">
                                        <span class="material-icons grey x16 dib vm">trending_up</span>
                                        <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.promote_now') }}</span>
                                    </a>
                                {% endif %}
                                <a class="manage-promote-later button fill white" href="{{ helper.link('@promote', advertiser.token, ad.slug) }}" title="{{ helper.trans('app.link.promote_auto') }}">
                                    <span class="material-icons grey x16 dib vm">trending_up</span>
                                    <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.promote_auto') }}</span>
                                </a>
                            {% else %}
                                <a class="manage-publish button fill white" href="{{ helper.link('@confirm_publication', advertiser.token, ad.slug) }}" title="{{ helper.trans('app.link.confirm_publication') }}">
                                    <span class="material-icons grey x16 vm">check</span>
                                    <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.confirm_publication') }}</span>
                                </a>
                                <a class="manage-delete button fill white" href="{{ helper.link('@delete', advertiser.token, ad.slug) }}" title="{{ helper.trans('app.link.delete_ad') }}">
                                    <span class="material-icons grey x16 dib vm">delete</span>
                                    <span class="grey x14 dib vm hide-on-small-and-down">{{ helper.trans('app.link.delete_ad') }}</span>
                                </a>
                            {% endif %}
                        </div>
                    </div>
                </div>

            {% endfor %}
        </div>
    </div>
</section>
