{% extends 'layout.volt' %}

{% block bodyClass %}page-feedback page-request-token has-captcha{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/out-of-time.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ helper.trans('advertiser.title.request_token') }}</h1>
                    <p class="x24 grey">{{ helper.trans('advertiser.label.request_token') }}</p>
                </div>
            </div>
            <form class="row contact-form mt25 mb0" action="{{ helper.link('@manage', advertiser.token) }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" id="_csrf" name="_csrf" value="{{ form.getCsrfToken() }}" />

                {#
                <div class="col s12 m8 offset-m2">
                    {{ partial('theme/form/checkbox', ['entity': advertiser, 'field':form.get('notifyBySms'), 'cols': 12]) }}
                </div>
                 #}

                <div class="col s12 mt25">
                    <div class="ctr dib">
                        <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
                    </div>
                </div>

                <button class="button big fill white db x16 light mt25 mb50 submit">
                    {{ helper.trans('app.link.generate_token') }}
                </button>
            </form>
        </div>
    </section>
    <section class="trace bglight pt25 pb25">
        <div class="container">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <p class="x16 grey alc">{{ helper.trans('ad.text.already_published') }}</p>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
