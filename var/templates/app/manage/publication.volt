<div id="manage-publication" class="modal manage-modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s12">
                <p class="x16 bold dark">{{ helper.trans('manage.title.publication') }}</p>
                <p class="x16 grey">{{ helper.trans('manage.label.publish_confirmation') }}</p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="dismiss modal-action modal-close waves-effect waves-green btn-flat">{{ helper.trans('app.link.dismiss') }}</a>
        <a href="#!" class="accept modal-action modal-close waves-effect waves-green btn-flat">{{ helper.trans('app.link.accept') }}</a>
    </div>
</div>