<form class="promote-form mt100" method="POST" action="#">
    <span class="dn" id="auto-credits-tpl">{{ helper.trans('promote.label.auto_credits') }}</span>
    <span class="dn" id="required-credits-tpl">{{ helper.trans('promote.label.required_credits') }}</span>
    <span class="dn" id="not-enough-credits-tpl">{{ helper.trans('promote.label.not_enough_credits') }}</span>

    <input type="hidden" id="_csrf" name="_csrf" value="{{ form.getCsrfToken() }}" />
    <input type="hidden" id="days" name="days" value="" />

    <div class="container pt25">
        <div class="row">
            <div class="col s12 m8 offset-m2">
                <h3 class="x24 mt25 dark bold mb15">{{ helper.trans('promote.title.when') }}</h3>
            </div>
            <div class="input-field col s6 m4 offset-m2">
                <input class="with-gap" type="radio" id="when_now" name="when" value="now" />
                <label class="x16" for="when_now">{{ helper.trans('promote.label.now') }}</label>
            </div>
            <div class="input-field col s6 m4">
                <input class="with-gap" type="radio" id="when_later" name="when" value="later" />
                <label class="x16" for="when_later">{{ helper.trans('promote.label.later') }}</label>
            </div>
        </div>
        <div class="row when-now dn">
            <div class="col s12 m8 offset-m2">
                <h3 class="x24 mt25 dark bold mb15">{{ helper.trans('promote.title.hours') }}</h3>
            </div>
            <div class="input-field col s12 m8 offset-m2">
                <input class="update-credits with-gap" type="number" id="hours" name="hours" />
                <label for="hours">{{ helper.trans('promote.label.hours') }}</label>
            </div>
        </div>
        <div class="row when-later dn">
            <div class="col s12 m8 offset-m2">
                <h3 class="x24 mt25 dark bold mb15">{{ helper.trans('promote.title.range') }}</h3>
            </div>
            <div class="input-field col s6 m4 offset-m2">
                <input class="update-credits with-gap" type="time" id="start_time" name="start_time" />
                <label class="active" for="start_time">{{ helper.trans('promote.label.start_time') }}</label>
            </div>
            <div class="input-field col s6 m4">
                <input class="update-credits with-gap" type="time" id="end_time" name="end_time" />
                <label class="active" for="end_time">{{ helper.trans('promote.label.end_time') }}</label>
            </div>
        </div>
        <div class="row when-later dn">
            <div class="col s12 m8 offset-m2">
                <h3 class="x24 mt25 dark bold mb15">{{ helper.trans('promote.title.auto') }}</h3>
            </div>
            <div class="input-field col s12 m8 offset-m2">
                <input class="update-credits with-gap" type="checkbox" id="auto" name="auto" />
                <label class="x16" for="auto">{{ helper.trans('promote.label.auto') }}</label>
            </div>
        </div>
        <div class="row when-later-auto dn">
            <div class="col s12 m8 offset-m2">
                <h3 class="x24 mt25 dark bold mb15">{{ helper.trans('promote.title.days') }}</h3>
            </div>
            <div class="input-field col s6 m4 offset-m2">
                <input class="promotion-day" type="checkbox" id="day_monday" value="1" />
                <label for="day_monday">{{ helper.ucfirst(helper.trans('day.monday')) }}</label>
            </div>
            <div class="input-field col s6 m4">
                <input class="promotion-day" type="checkbox" id="day_friday" value="5" />
                <label for="day_friday">{{ helper.ucfirst(helper.trans('day.friday')) }}</label>
            </div>
            <div class="input-field col s6 m4 offset-m2">
                <input class="promotion-day" type="checkbox" id="day_tuesday" value="2" />
                <label for="day_tuesday">{{ helper.ucfirst(helper.trans('day.tuesday')) }}</label>
            </div>
            <div class="input-field col s6 m4">
                <input class="promotion-day" type="checkbox" id="day_saturday" value="6" />
                <label for="day_saturday">{{ helper.ucfirst(helper.trans('day.saturday')) }}</label>
            </div>
            <div class="input-field col s6 m4 offset-m2">
                <input class="promotion-day" type="checkbox" id="day_wednesday" value="3" />
                <label for="day_wednesday">{{ helper.ucfirst(helper.trans('day.wednesday')) }}</label>
            </div>
            <div class="input-field col s6 m4">
                <input class="promotion-day" type="checkbox" id="day_sunday" value="0" />
                <label for="day_sunday">{{ helper.ucfirst(helper.trans('day.sunday')) }}</label>
            </div>
            <div class="input-field col s6 m4 offset-m2">
                <input class="promotion-day" type="checkbox" id="day_thursday" value="4" />
                <label for="day_thursday">{{ helper.ucfirst(helper.trans('day.thursday')) }}</label>
            </div>
        </div>
        <div class="row credits-info dn">
            <div class="col s12 m8 offset-m2">
                <p class="x16 grey message"></p>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m8 offset-m2">
                <button class="button x18 big red cancel mt25">
                    {{ helper.trans('promote.link.cancel') }}
                </button>

                <button class="button x18 big red submit mt25">
                    {{ helper.trans('promote.link.commit') }}
                </button>
            </div>
        </div>
    </div>
</form>