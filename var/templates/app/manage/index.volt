{% extends 'layout.volt' %}

{% block bodyClass %}page-manage{% endblock %}

{% block extra %}
    {% include 'manage/publication.volt' %}
    {% include 'manage/promotion.volt' %}
    {% include 'manage/deletion.volt' %}
{% endblock %}

{% block content %}
    <article>
        {{ partial('manage/header', ['advertiser': advertiser, 'ads': ads]) }}
        {% if ads|length %}
            {{ partial('manage/filter') }}
        {% endif %}
        {{ partial('manage/list', ['advertiser': advertiser, 'ads': ads]) }}
    </article>
{% endblock %}
