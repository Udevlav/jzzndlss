{% extends 'layout.volt' %}

{% block bodyClass %}page-feedback page-token-generated{% endblock %}

{% block content %}
    <article>
        <section>
            <div class="container alc">
                <div class="row">
                    <div class="col s12 m8 offset-m2">
                        <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/checked.png') }}" width="128" />
                        <h1 class="x40 mb15">{{ helper.trans('manage.title.token_generated') }}</h1>
                        <p class="x24 grey">{{ helper.trans('manage.label.token_sent') }}</p>

                        <a class="button white fill db x16 light mt25 mb50" href="{{ helper.link('@index') }}">{{ helper.trans('app.label.back_to_home') }}</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="bglight pt25 pb25">
            <div class="container">
                <div class="row">
                    <div class="col s12 m8 offset-m2">
                        <p class="x16 grey">{{ helper.trans('manage.text.token_generated') }}</p>
                    </div>
                </div>
            </div>
        </section>
    </article>
{% endblock %}
