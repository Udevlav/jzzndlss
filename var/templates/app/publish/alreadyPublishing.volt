{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-already-publishing{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/sandclock.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ helper.trans('ad.title.already_publishing') }}</h1>
                    <p class="x24 grey">{{ helper.trans('ad.label.already_publishing') }}</p>

                    <a class="button white db x16 light mt25 mb50" href="{{ helper.link('@index') }}">{{ helper.trans('app.label.back_to_home') }}</a>
                </div>
            </div>
        </div>
    </section>
    <section class="trace bglight pt25 pb25">
        <div class="container">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <p class="x16 grey">{{ helper.trans('ad.text.already_publishing') }}</p>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
