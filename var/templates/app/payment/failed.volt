{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-payment-failed{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/warning.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ helper.trans('payment.title.failed') }}</h1>
                    <p class="x24 grey">{{ helper.trans('payment.label.failed') }}</p>

                    <a class="button fill white db x16 light mt25 mb50" href="{{ helper.link('@index') }}">{{ helper.trans('app.label.back_to_home') }}</a>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
