{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-payment has-captcha{% endblock %}

{% block content %}
    <form class="payment-form big pt25 mb0" action="{{ helper.link('@payment') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="_csrf" name="_csrf" value="{{ form.getCsrfToken() }}" />
        <input type="hidden" id="advertiserId" name="advertiserId" value="{% if advertiser is defined %}{{ advertiser.id }}{% endif %}" />
        <input type="hidden" id="productId" name="productId" value="{% if product is defined %}{{ product.id }}{% endif %}" />

        <div class="container">
            {{ flash.output() }}
        </div>

        {% set paymentCarouselLayout = 's12 m8 l6 offset-m2 offset-l3' %}

        <div class="payment-panels panels" data-indicators="true">
            <div class="panel select-product open" id="step-0">
                <div class="container">
                    <div class="row">
                        <div class="col s12 hgroup alc">
                            <h1 class="x32 dark bold alc mt25 mb15">{{ helper.trans('payment.title.select_product') }}</h1>
                            <p class="x18 grey alc mb25">{{ helper.trans('payment.text.select_product') }}</p>
                        </div>
                    </div>
                    <div class="row all">
                        <div class="col s12 l10 offset-l1">
                            <div class="row">
                                <div class="carousel-item-scroll">

                                    {% for product in products %}
                                        <div class="col s12 m4 mt25 product-col">
                                            <input type="hidden" id="product_{{ product.id }}" name="productId" value="{{ product.id }}" />
                                            <div class="card mt25 mb25">
                                                <div class="card-content alc">
                                                    <img src="{{ helper.asset('img/promote.png') }}" width="48" />
                                                    <p class="grey mb0 alc">
                                                        <span class="label db x16 bold lred upper mt10 mb10">{{ product.label }}</span>
                                                        <span class="currency db x32 dark bold mb25">
                                                            <span class="x14 lgrey">por</span><sup></sup>
                                                            <span class="credits">{{ product.price }}</span><sup></sup>
                                                        </span>
                                                    </p>
                                                    <span class="dib mb15 badge green" data-badge-caption="{{ product.savings }}"></span>
                                                    <span class="desc db x16 lgrey lh20 mb25">{{ product.description }}</span>
                                                    <span class="button fill white x16">
                                                        <i class="material-icons x16 light vm mr5">done</i>
                                                        <span class="vm">{{ helper.trans('payment.link.select_coupon') }}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    {% endfor %}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel payment-data dn" id="step-1">
                <div class="container">
                    <div class="row">
                        <div class="col {{ paymentCarouselLayout }}">
                            <h2 class="x32 dark bold alc mt50 mb10">{{ helper.trans('payment.title.specify_payment') }}</h2>
                            <p class="x18 grey alc mb25">{{ helper.trans('payment.text.specify_payment') }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col {{ paymentCarouselLayout }}">
                            <div class="row">
                                {{ partial('theme/form/input', ['entity': payment, 'field': form.get('email'), 'cols': 12]) }}
                                {{ partial('theme/form/input', ['entity': payment, 'field': form.get('promoCode'), 'cols': 12]) }}
                            </div>
                            <div class="row alc">
                                <div class="col s6">
                                    <label for="method_card">
                                        <input type="radio" value="card" id="method_card" name="method" class="with-gap" />
                                        <span>{{ helper.trans('payment.label.method_card') }}</span>
                                    </label>
                                </div>
                                <div class="col s6">
                                    <label for="method_paypal">
                                        <input type="radio" value="sms" id="method_paypal" name="method" class="with-gap" />
                                        <span>{{ helper.trans('payment.label.method_sms') }}</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel verify-payment dn" id="step-2">
                <div class="container">
                    <div class="row">
                        <div class="col {{ paymentCarouselLayout }}">
                            <h2 class="x32 dark bold alc mt50 mb25">{{ helper.trans('payment.title.verify_payment') }}</h2>

                            <div class="line"></div>

                            <div class="product mb25 mt15 all">
                                <h3 class="x16 bold lred mt15 mb10 upper">{{ helper.trans('payment.label.selected_promotion') }}</h3>
                                <p id="selected-promotion-price" class="x24 lh32 dark bold mb0"></p>
                                <p id="selected-promotion-desc" class="x16 light"></p>
                            </div>

                            <div class="line"></div>

                            <div class="product mb25 mt15 all">
                                <h3 class="x16 bold lred mt15 mb10 upper">{{ helper.trans('payment.label.method') }}</h3>
                                <p id="selected-payment-method" class="x16 grey mb0"></p>
                            </div>

                            <div class="line"></div>

                            <div class="product mb25 mt15 all">
                                <h3 class="x16 bold lred mt15 mb10 upper">{{ helper.trans('payment.label.your_data') }}</h3>
                                <p id="selected-email" class="x18 lh24 dark bold mb0"></p>
                                <p class="x16 light">{{ helper.trans('payment.text.about_email') }}</p>
                            </div>

                            <div class="line"></div>

                            <button class="button red big x18 waves-effect form-submit mt15">{{ helper.trans('payment.link.pay') }}</button>
                            <a href="{{ helper.link('@tos') }}" class="db x14 light mt25">{{ helper.trans('payment.link.policy') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
{% endblock %}