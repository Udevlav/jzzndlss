{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-payment-already-committed{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/warning.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ helper.trans('payment.title.already_commited') }}</h1>
                    <p class="x24 grey">
                        {% if payment.result %}
                            {{ helper.trans('payment.label.already_succeed') }}
                        {% else %}
                            {{ helper.trans('payment.label.already_commited_failed') }}
                        {% endif %}
                    </p>

                    <a class="button white fill db x16 light mt25 mb50" href="{{ helper.link('@index') }}">{{ helper.trans('app.label.back_to_home') }}</a>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
