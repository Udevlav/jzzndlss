<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8" />
    </head>
    <body>
        <form action="{{ form.getAction() }}" method="POST">
            {% for field in form %}
                <input type="hidden" name="Ds_{{ helper.ucfirst(field.getName()) }}" value="{{ field.getValue() }}" />
            {% endfor %}
        </form>
        <script type="text/javascript">
            document.forms[0].submit();
        </script>
    </body>
</html>