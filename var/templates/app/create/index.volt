{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-publish has-captcha{% endblock %}

{% block links %}
    {{ partial('theme/form/links') }}
{% endblock %}

{% block content %}
    <article>
        <section>
            <div class="container mt75">
                <div class="row pt25 pb25">
                    <div class="col s12">

                        <h1 class="x40 dark bold">{{ helper.trans('ad.title.publish') }}</h1>
                        <p class="x18 mt15 grey">{{ helper.trans('ad.label.publish') }}</p>

                    </div>
                </div>
            </div>
        </section>
        <section>
            {{ partial('theme/item/form', ['action': helper.link('@publish'), 'method': 'POST', 'enctype': 'multipart/form-data']) }}
        </section>
    </article>
{% endblock %}

{% block inlineAssets %}
    {{ super() }}
    <script type="text/javascript">
        (function(global) {
            if (global.app) {
                global.app.setAd({{ entity.toArray()|json_encode }});
            }
        }(this));
    </script>
{% endblock %}
