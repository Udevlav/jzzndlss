<!DOCTYPE html>
<html{% block amp %}{% endblock %}>
    <head>
        {% include 'layout/meta.volt' %}

        <title>{% block title %}{% endblock %}</title>

        {% block seo %}
        {% endblock %}

        {% block favicon %}
            {% include 'layout/favicon.volt' %}
        {% endblock %}

        {% block fonts %}
            {% include 'layout/fonts.volt' %}
        {% endblock %}

        {% block manifest %}
            {% include 'layout/manifest.volt' %}
        {% endblock %}

        {% block assets %}
            {% include 'layout/assets.volt' %}
        {% endblock %}

        {% block analytics %}
            {% include 'layout/analytics.volt' %}
            {% include 'layout/verification.volt' %}
        {% endblock %}
    </head>
    <body class="{% block bodyClass %}{% endblock %}">
        <nav class="menu">
            <div class="container rel">
                <div class="row">
                    <div class="col s12">
                        {% block logo %}
                            {% include 'layout/navlogo.volt' %}
                            {% include 'layout/navpublish.volt' %}
                            {% include 'layout/navmore.volt' %}
                        {% endblock %}

                        {% block links %}
                            <div class="links dn"></div>
                        {% endblock %}
                    </div>
                </div>
                {% include 'layout/navform.volt' %}
            </div>
        </nav>

        {% block overlays %}
            {% include 'layout/menu.volt' %}
            {% include 'layout/filter.volt' %}
        {% endblock %}

        {% block header %}
            {% include 'layout/header.volt' %}
        {% endblock %}

        <main>
            {% block breadcrumbs %}
            {% endblock %}

            {% block flash %}
                {% include 'layout/flash.volt' %}
            {% endblock %}

            {% block content %}
            {% endblock %}

            {% block debug %}
                {% include 'layout/debug.volt' %}
            {% endblock %}
        </main>

        {% block footer %}
            {% include 'layout/footer.volt' %}
        {% endblock %}

        {% block fab %}
            {% include 'layout/totop.volt' %}
        {% endblock %}

        {% block adult %}
            {% include 'layout/modal.volt' %}
        {% endblock %}

        {% block cookies %}
            {% include 'layout/cookies.volt' %}
        {% endblock %}

        {% block extra %}
        {% endblock %}

        {% block inlineAssets %}
            {% if config.app.env == 'dev' %}
                <script type="text/javascript" src="{{ config.asset('js/vendor.min.js') }}"></script>
                <script type="text/javascript" src="{{ config.asset('js/materialize.js') }}"></script>
                <script type="text/javascript" src="{{ config.asset('js/app.js') }}"></script>
            {% else %}
                <script type="text/javascript" src="{{ config.asset('js/app.min.js') }}"></script>
            {% endif %}
            <script type="text/javascript">
                (function(global) {
                    global.app = new App({
                        appName: '{{ config.app.name }}',
                        appVersion: '{{ config.app.version }}',
                        apiKey: '{{ config.map.apiKey }}',
                        locale: '{{ config.app.locale }}'
                    });
                }(this));
            </script>
        {% endblock %}
    </body>
</html>
