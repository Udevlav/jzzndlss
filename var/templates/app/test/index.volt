<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="/asset/css/materialize.css" />
        <link rel="stylesheet" type="text/css" href="/asset/css/app.css" />
        <link type="text/css" rel="stylesheet" href="/asset/css/App.css" />
        <script type="text/javascript" src="/asset/js/vendor.min.js"></script>
        <script type="text/javascript" src="/asset/js/materialize.js"></script>
        <script type="text/javascript" src="/asset/js/App.js"></script>
    </head>
    <body class="page-detail">
        <nav class="menu">
            <a class="menu-action menu-left menu-more" href="#filter">
                <i class="material-icons">more_vert</i>
            </a>
            <a class="menu-action menu-left menu-back" href="#filter">
                <i class="material-icons">navigate_before</i>
            </a>
            <a class="menu-action menu-right menu-search" href="#search">
                <i class="material-icons">search</i>
            </a>
            <div class="container">
                <div class="row">
                    <div class="col s12">
                        <div class="logo alc">
                            <a class="logo x16 grey" href="/">
                                {{ partial('svg/wifi') }}
                            </a>
                        </div>
                        <div class="links dn">
                            <a class="link active" href="#ad-poster">Overview</a>
                            <span class="separator"> · </span>
                            <a class="link" href="#ad-images">Fotos</a>
                            <span class="separator"> · </span>
                            <a class="link" href="#ad-videos">Videos</a>
                            <span class="separator"> · </span>
                            <a class="link" href="#ad-location">Location</a>
                            <span class="separator"> · </span>
                            <a class="link" href="#ad-experiences">Experiencias</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <div id="ad-poster" class="ad-carousel">
            <div class="carousel-item" href="#media-1!">
                <img class="responsive-img materialboxed" src="https://www.placehold.it/1220x820" data-caption="Radamel, 32 años" />
            </div>
            <div class="user-is-online">
                <a class="button white">
                    {{ partial('svg/wifi') }}
                    <span>Online</span>
                </a>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col s12 l8">
                    <article class="page-detail">

                        <section id="ad-summary">
                            <div class="row ad-summary mt25 mb25">
                                <div class="col s12">
                                    <h2 class="mb10">Berenice, 32 años</h2>
                                    <h1 class="mb10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat nulla ipsum, a vulputate tortor blandit vitae</h1>
                                    <p class="ad-zone">Sants, Barcelona</p>
                                </div>
                            </div>

                            <div class="row ad-tags">
                                <div class="col s12 light">
                                    <i class="material-icons vm">done</i>
                                    <span class="x12 vm">Publicado hace 20 días</span>
                                </div>
                            </div>
                        </section>

                        <section id="ad-text">
                            <div class="row mt25">
                                <div class="col s12">
                                    <p class="x16 mb15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat nulla ipsum, a vulputate tortor blandit vitae. Nullam at ultrices nunc. Suspendisse pretium placerat sapien, eu fermentum mi gravida in. Integer ornare, ipsum eu viverra hendrerit, nulla diam eleifend metus, at porttitor sapien diam sit amet erat. Duis sed accumsan lacus, at ornare ipsum. Sed imperdiet volutpat gravida. Curabitur eget massa massa. Sed sed egestas odio. Nunc scelerisque arcu sollicitudin, ultrices neque eget, pretium mi. Nunc molestie eros risus. Nunc nec ultricies augue. Quisque vitae sem id metus porttitor semper faucibus ut urna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tortor libero, sagittis sed laoreet nec, sagittis vel orci. Nam a est ligula.</p>
                                    <p class="x16 mb15">No olvides decir que viste mi anuncio en tal y cual.</p>
                                </div>
                            </div>
                            <div class="line"></div>
                        </section>

                        <section id="ad-images" class="summarizable">
                            <div class="row mt25">
                                <div class="col s12">
                                    <h3>Fotos</h3>
                                </div>
                                {% for i in [0,1,2,3,4,5,6,7] %}
                                    <div class="col s4 m3">
                                        <div class="round-frame mt15">
                                            <img class="responsive-img materialboxed" src="https://www.placehold.it/220x160" data-caption="Radamel, 32 años" />
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                            <div class="line"></div>
                        </section>

                        <section id="ad-videos" class="summarizable">
                            <div class="row mt25">
                                <div class="col s12">
                                    <h3 class="mb15">Videos</h3>
                                </div>
                                {% for i in [0,1,2] %}
                                    <div class="col s6 m4 l3">
                                        <div class="frame">
                                            <img class="responsive-img materialboxed" src="https://www.placehold.it/220x160" data-caption="Radamel, 32 años" />
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                            <div class="line"></div>
                        </section>

                        <section id="ad-services" class="summarizable">
                            <div class="row mt25">
                                <div class="col s12">
                                    <h3 class="mb15">Mis servicios</h3>
                                </div>
                                <div class="col s6">
                                    <p class="x16 mb15">
                                        {{ partial('svg/wifi') }}
                                        <span>Servicio y tal</span>
                                    </p>
                                    <p class="x16 mb15">
                                        {{ partial('svg/wifi') }}
                                        <span>Servicio y tal</span>
                                    </p>
                                    <p class="x16 mb15">
                                        {{ partial('svg/wifi') }}
                                        <span>Servicio y tal</span>
                                    </p>
                                </div>
                                <div class="col s6">
                                    <p class="x16 mb15">
                                        {{ partial('svg/wifi') }}
                                        <span>Servicio y tal</span>
                                    </p>
                                    <p class="x16 mb15">
                                        {{ partial('svg/wifi') }}
                                        <span>Servicio y tal</span>
                                    </p>
                                    <p class="x16 mb15">
                                        {{ partial('svg/wifi') }}
                                        <span>Servicio y tal</span>
                                    </p>
                                </div>
                                <div class="col s6">
                                    <a class="x14 show-more">Ver las 18 categorías</a>
                                </div>
                            </div>
                            <div class="row dn extra">
                                <div class="col s6">
                                </div>
                                <div class="col s6">
                                </div>
                            </div>
                            <div class="line"></div>
                        </section>

                        <section id="ad-location" class="summarizable">
                            <div class="row mt25">
                                <div class="col s12">
                                    <h3 class="mb15">Localización</h3>
                                </div>
                                <div class="col s12">
                                    <p class="x16 mb15">Sants, Barcelona</p>
                                </div>
                                <div class="col s12">
                                    <div class="wrap rel">
                                        <img src="https://www.placehold.it/545x320">
                                        <div class="see-on-maps">
                                            <a class="button white">
                                                {{ partial('svg/wifi') }}
                                                <span>Abrir en mapas</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        
                        <section id="ad-fees-timetable" class="sm-show">
                            <div class="row mt25">
                                <div class="col s6 vt">
                                    <h3 class="x14 bold mb10">Tarifas</h3>
                                    <p>
                                        <span class="x18 dark currency">
                                            <span>100</span>
                                            <sup class="light">/30 minutos</sup>
                                        </span>
                                    </p>
                                    <p>
                                        <span class="x18 dark currency">
                                            <span>140</span>
                                            <sup class="light">/45 minutos</sup>
                                        </span>
                                    </p>
                                    <p>
                                        <span class="x18 dark currency">
                                            <span>180</span>
                                            <sup class="light">/1 hora</sup>
                                        </span>
                                    </p>
                                </div>
                                <div class="col s6 vt">
                                    <h3 class="x14 bold mb10">Horarios</h3>
                                    <p>
                                        <span class="x18 dark">
                                            <span>08:00 - 14:00</span>
                                            <sup class="light">/lu-vi</sup>
                                        </span>
                                    </p>
                                    <p>
                                        <span class="x18 dark">
                                            <span>00:00 - 06:00</span>
                                            <sup class="light">/sa, do</sup>
                                        </span>
                                    </p>
                                </div>
                            </div>
                            <div class="line"></div>
                        </section>

                        <section id="ad-experiences" class="summarizable">
                            <div class="row mt25">
                                <div class="col s6 vm">
                                    <h3>
                                        <span class="mr10">122 Experiencias</span>
                                        <span class="x14 light">
                                            {{ partial('svg/rating') }}
                                        </span>
                                    </h3>
                                </div>
                                <div class="col s6 alr vm">
                                    <a id="new-experience" class="button white">Añadir experiencia</a>
                                </div>
                                {% for i in [0,1,2,3,4] %}
                                    <div class="col s12">
                                        <div class="line"></div>
                                        <p class="x16 bold">Nombre</p>
                                        <p class="x14 mb15">Septiembre 2018</p>
                                    </div>
                                    <div class="col s12">
                                        <p class="x12 mb15 light">
                                            {{ partial('svg/rating') }}
                                        </p>
                                    </div>
                                    <div class="col s12">
                                        <p class="x16">We had a wonderful stay at Anna’s Trullo. Anna was incredibly helpful and responsive. The pleasant surprise for us was the setting— sometimes rural looking Airbnbs can turn out to be in less than appealing settings (eg on a busy road) since that is harder to tell from the posting...</p>
                                    </div>
                                {% endfor %}
                                <div class="col s12">
                                    <a class="x14 show-more">
                                        <div class="line"></div>
                                        <span>Ver las 18 experiencias de los usuarios</span>
                                    </a>
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="row dn extra">
                                <div class="col s6">
                                </div>
                                <div class="col s6">
                                </div>
                            </div>
                        </section>

                    </article>
                </div>
                
                <div class="col s12 l4">
                    <aside class="aside sm-hidden">
                        <div class="bbox">
                            <div class="ad-fees">
                                <span class="x14 db">Desde</span>
                                <span class="x20">
                                    <span class="bold">368&euro;</span>
                                    <sup>/hora</sup>
                                </span>
                                <div class="x11 light">
                                    {{ partial('svg/rating') }}
                                    <span class="x12 grey">122</span>
                                </div>
                            </div>
                            <div class="ad-fees">
                                <div class="line"></div>
                                <h3 class="x14 bold mb10">Tarifas</h3>
                                <p>
                                    <span class="x18 dark currency">
                                        <span>100</span>
                                        <sup class="light">/30 minutos</sup>
                                    </span>
                                </p>
                                <p>
                                    <span class="x18 dark currency">
                                        <span>140</span>
                                        <sup class="light">/45 minutos</sup>
                                    </span>
                                </p>
                                <p>
                                    <span class="x18 dark currency">
                                        <span>180</span>
                                        <sup class="light">/1 hora</sup>
                                    </span>
                                </p>
                            </div>
                            <div class="ad-timetable">
                                <div class="line"></div>
                                <h3 class="x14 bold mb10">Horarios</h3>
                                <p>
                                    <span class="x18 dark">
                                        <span>08:00 - 14:00</span>
                                        <sup class="light">/lu-vi</sup>
                                    </span>
                                </p>
                                <p>
                                    <span class="x18 dark">
                                        <span>00:00 - 06:00</span>
                                        <sup class="light">/sa, do</sup>
                                    </span>
                                </p>
                            </div>
                            <div class="call-actions">
                                <div class="line"></div>
                                <a class="chat-call button big red x18 w100" href="tel:123456789">Llama</a>
                            </div>
                            <div class="chat-actions">
                                <div class="line"></div>
                                <p class="x16 bold">Berenice está en línea</p>
                                <p class="x14 mb15">Si quieres puedes iniciar un nuevo chat ahora</p>
                                <a class="chat-connect-peer button big red x18 w100">Iniciar chat</a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        
        <div id="ad-related">
            <div class="container">
                <div class="row mb100">
                    <div class="col s12">
                        <h3 class="x14 bold mb15">Anuncios relacionados</h3>
                    </div>
                    {% for i in [0,1,2] %}
                        <div class="col s4">
                            <img class="responsive-img" src="https://www.placehold.it/220x160" />
                            <p class="x16 bold mt10">Aya, 24 años</p>
                            <p class="x12 mt10">
                                <span class="x12">Desde</span>
                                <span class="x16">
                                    <span class="bold">368&euro;</span>
                                    <sup>/hora</sup>
                                </span>
                            </p>
                            <div class="x11 light">
                                {{ partial('svg/rating') }}
                                <span class="x12 grey">122</span>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
        
        <aside class="fix sm-show">
            <div class="container">
                <div class="row">
                    <div class="col s6">
                        <span class="x14">Desde</span>
                        <span class="x20">
                            <span class="bold">368&euro;</span>
                            <sup>/hora</sup>
                        </span>
                        <div class="x11 light">
                            {{ partial('svg/rating') }}
                            <span class="x12 grey">122</span>
                        </div>
                    </div>
                    <div class="col s6 alr">
                        <div class="fixed-action-btn">
                            <a class="btn-floating btn-large red">
                                <i class="large material-icons">phone</i>
                            </a>
                            <ul>
                                <li>
                                    <a class="btn-floating red">
                                        <i class="material-icons">insert_comment</i>
                                    </a>
                                </li>
                                <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
                                <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
                                <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        
        {{ partial('chat/chat') }}

        <footer></footer>
    </body>
</html>
