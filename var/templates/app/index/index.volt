{% extends 'layout.volt' %}

{% block title %}{{ helper.trans('app.title.index', config.app.name) }}{% endblock %}
{% block bodyClass %}page-index has-filters{% endblock %}

{% block seo %}
    {{ partial('layout/seoIndex', ['config': config, 'filters': filters]) }}
{% endblock %}

{% block content %}
    <article>
        <section class="promoted pb25 mt25">
            <div class="container">

                {{ partial('theme/list/header', ['results': results, 'filters': filters]) }}

                <div class="row grid">
                    <div class="grid-sizer col s6 m6 l3"></div>
                    {% if results.isEmpty() %}
                        {{ partial('theme/list/empty') }}
                    {% else %}
                        {% if results.hasPromoted() %}
                            {{ partial('theme/list/items', ['items': results.promoted, 'kind': 'promoted', 'landscape': 's12 m12 l6', 'portrait': 's12 m6 l3', 'size': 's6 m6 l3']) }}
                        {% endif %}
                        {% if results.hasRegular() %}
                            {{ partial('theme/list/items', ['items': results.regular, 'kind': 'regular', 'landscape': 's12 m12 l6', 'portrait': 's6 m6 l3', 'size': 's6 m6 l3']) }}
                        {% endif %}
                    {% endif %}
                </div>
            </div>
        </section>
    </article>
{% endblock %}
