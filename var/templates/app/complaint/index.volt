{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-complaint has-captcha{% endblock %}

{% block content %}
    <article>
        <form class="row contact-form mb0" action="{{ helper.link('@complaint', ad.slug) }}" method="POST" enctype="multipart/form-data">
            <section>
                <div class="container">

                    <input type="hidden" id="_csrf" name="_csrf" value="{{ form.getCsrfToken() }}" />
                    <input type="hidden" id="adId" name="adId" value="{{ entity.adId }}" />

                    {% if errors is defined %}
                        <fieldset>
                            <div class="row">
                                <div class="col s12">
                                    <div class="form-errors">
                                        <ul>
                                            {% for message in errors %}
                                                <li>{{ message }}</li>
                                            {% endfor %}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    {% endif %}

                    <fieldset>
                        <div class="row">
                            <div class="col s12">

                                <h1 class="x40 dark bold mt100 mb25">{{ helper.trans('complaint.title.index') }}</h1>

                            </div>
                        </div>
                        <div class="row">
                            {{ partial('theme/form/email', ['entity': entity, 'field':form.get('email')]) }}
                        </div>
                        <div class="row">
                            {{ partial('theme/form/select', ['entity': entity, 'field':form.get('cause')]) }}
                        </div>
                        <div class="row">
                            {{ partial('theme/form/textarea', ['entity': entity, 'field':form.get('text')]) }}
                        </div>
                    </fieldset>
                </div>
            </section>
            <section class="bglight pb25">
                <div class="container">
                    <fieldset>
                        <div class="row mt50">
                            <div class="col s12">
                                <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="row mb50">
                            <div class="col s12">
                                <button class="button x18 big red submit">
                                    {{ helper.trans('app.link.send_complaint') }}
                                </button>
                            </div>
                        </div>
                    </fieldset>

                </div>
            </section>
        </form>
    </article>
{% endblock %}