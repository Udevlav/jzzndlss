<span class="rating">
    {% if rating is defined %}
        {% set emptyStars = 5 - rating %}
    {% else %}
        {% set rating = 0 %}
        {% set emptyStars = 5 %}
    {% endif %}
    {% for i in 1 .. rating %}
        {{ partial('svg/star') }}
    {% endfor %}
    {% for i in 1 .. emptyStars %}
        {{ partial('svg/star_empty') }}
    {% endfor %}
</span>
