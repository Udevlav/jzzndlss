{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-blocked has-captcha{% endblock %}

{% block content %}
    <article>
        <form method="POST" action="{{ helper.link('@unblock') }}">
            <section>
                <div class="container">
                    <h1 class="x40 mt100 mb15">{{ helper.trans('app.title.blocked') }}</h1>
                    <p class="x24 grey">{{ helper.trans('app.label.blocked') }}</p>
                </div>
            </section>
            <section class="bglight pt25 pb25">
                <div class="container">

                    <fieldset>
                        <div class="row mt50">
                            <div class="col s12">
                                <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="row mb50">
                            <div class="col s12">
                                <button class="btn btn-primary submit">
                                    {{ helper.trans('contact.link.contact') }}
                                </button>

                                <p class="x12 grey mt15">{{ helper.trans('app.text.blocked') }}</p>
                            </div>
                        </div>
                    </fieldset>

                </div>
            </section>
        </form>
    </article>
{% endblock %}