{% extends 'layout.volt' %}

{% block title%}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-error{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/error.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ code }}</h1>
                    <p class="x24 grey">{{ helper.trans('app.error.'~ code) }}</p>

                    <a class="button fill white db x16 light mt50 mb50" href="{{ helper.link('@index') }}">
                        {{ helper.trans('app.label.back_to_home') }}
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="trace bglight pt25 pb25">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    {% if config.app.debug %}
                        {% if throwable is defined %}
                            {{ partial('layout/throwable', ['t': throwable ]) }}
                        {% endif %}
                    {% endif %}
                    <p class="x16 grey alc">{{ helper.trans('app.label.thats_all_we_know') }}</p>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
