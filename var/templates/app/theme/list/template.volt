<script type="text/x-html-template" class="item-template {{ kind }}">
    <div class="col grid-item mb25" data-layout-landscape="{{ landscape }}" data-layout-portrait="{{ portrait }}">
        <div class="card">
            <div class="card-image">
                <a class="link db">
                    <img>
                </a>
            </div>
            <div class="card-content">
                <a class="link db">
                    <h2 class="subj x16 lh22 lred bold upper mb0"></h2>
                    <p class="x32 mb10">
                    <span class="concrete_fee dn">
                        <span class="dark currency bold"></span>
                    </span>
                        <span class="generic_fee dn">
                        <span class="dark">{{ helper.trans('ad.label.unknown_price') }}</span>
                    </span>
                    </p>
                </a>
                <div class="all-tags"></div>
            </div>
            <div class="card-action">
                <a class="loc x14 lh20 light"></a>
            </div>
        </div>
    </div>
</script>
