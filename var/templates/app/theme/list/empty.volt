<section class="empty">
    <div class="container alc">
        <div class="row">
            <div class="col s12">
                <img class="mt100 mb25" src="{{ helper.asset('img/under-construction.png') }}" width="128" />
                <h1 class="x40 mb15">{{ helper.trans('ad.title.no_results') }}</h1>
                <p class="x24 grey">{{ helper.trans('ad.label.no_results') }}</p>

                <a class="button fill white db x16 light mt50 mb50" href="{{ helper.link('@publish') }}">
                    <span class="vm">{{ helper.trans('app.link.publish') }}</span>
                </a>
            </div>
        </div>
    </div>
</section>
