<div class="col {% if item.layout === 'LANDSCAPE' %}{{ landscape }}{% else %}{{ portrait }}{% endif %} grid-item grid-item-{{ item.layout|lower }}">
    <div class="card {{ kind }} mb25">
        {% if kind === 'promoted' %}
            <div class="dbadge">{{ helper.trans('app.label.promoted') }}</div>
        {% endif %}
        <div class="card-image">
            <a class="db" href="{{ helper.link('@ad_seo', item.hash) }}" data-rel="{{ item.id }}">
                {% if item.isLandscape() %}
                    {% if kind === 'promoted' %}
                        <img src="{{ helper.flavour(item.poster, 'md') }}" alt="{{ item.getCaption(false) }}" title="{{ item.getCaption(false) }}">
                    {% else %}
                        <img src="{{ helper.flavour(item.poster, 'sm') }}" alt="{{ item.getCaption(false) }}" title="{{ item.getCaption(false) }}">
                    {% endif %}
                {% else %}
                    {% if kind === 'promoted' %}
                        <img src="{{ helper.flavour(item.poster, 'sm') }}" alt="{{ item.getCaption(false) }}" title="{{ item.getCaption(false) }}">
                    {% else %}
                        <img src="{{ helper.flavour(item.poster, 'xs') }}" alt="{{ item.getCaption(false) }}" title="{{ item.getCaption(false) }}">
                    {% endif %}
                {% endif %}
            </a>
        </div>
        <div class="card-content">
            <a class="db" href="{{ helper.link('@ad_seo', item.hash)}}">
                <h2 class="subj x16 lh22 lred bold upper mb0">
                    <span class="dib card-name">{{ item.name }}</span>
                    {% if item.age > 0 %}
                        <span class="dib card-age">, {{ helper.trans('ad.label.aged', item.age) }}</span>
                    {% endif %}
                    <span class="dib card-category">
                        <span class="dib">&nbsp;&nbsp;·&nbsp;&nbsp;</span>
                        <span>{{ helper.category(item.category) }}</span>
                    </span>
                    {% if item.numExperiences %}
                        <span class="dib card-rating">
                            <span class="dib">&nbsp;&nbsp;·&nbsp;&nbsp;</span>
                            <span class="dib x11 light">
                            {{ partial('svg/rating', ['rating': item.rating]) }}
                                <span class="x12 grey bold">{{ item.numExperiences }}</span>
                            </span>
                        </span>
                    {% endif %}
                </h2>
                {% if item.fee > 0 %}
                    <p class="x32 mb10 ad-currency">
                        <span class="dark currency bold">{{ helper.intval(item.fee) }}</span>
                    </p>
                {% endif %}
            </a>
            {{ partial('theme/list/tagsall', ['item': item]) }}
        </div>
        <div class="card-action">
            <a class="loc x14 lh20 light" title="{{ helper.trans('ad.link.filter_by_category') }}" href="{{ helper.link('@filter_by_category_and_location', item.category, helper.provinceSlug(item.postalCode)) }}">{{ item.city }}</a>
        </div>
    </div>
</div>
