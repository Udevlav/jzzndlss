{% if not results.isEmpty() %}
    <div class="row">
        <div class="col s12">
            <h1 class="x14 light upper bold">
                {% if filters.getData().province %}
                    {{ helper.trans('app.label.found_results', filters.getData().province) }}
                {% else %}
                    {{ helper.trans('app.label.results_near_you') }}
                {% endif %}
            </h1>
        </div>
    </div>
{% endif %}