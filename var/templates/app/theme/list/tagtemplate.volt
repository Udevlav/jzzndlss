<script type="text/x-html-template" class="tag-item-template {{ kind }}">
    <a class="chip dib" href="">
        <span class="dib lh32 vt"></span>
        <i class="dib lh32 vt material-icons">insert_link</i>
    </a>
</script>
