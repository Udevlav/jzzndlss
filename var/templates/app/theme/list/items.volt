{{ partial('theme/list/template', ['landscape': landscape, 'portrait': portrait, 'kind': kind]) }}
{{ partial('theme/list/tagtemplate', ['landscape': landscape, 'portrait': portrait, 'kind': kind]) }}

{% for item in items %}
    {{ partial('theme/list/item', ['item': item, 'landscape': landscape, 'portrait': portrait, 'kind': kind]) }}
{% endfor %}