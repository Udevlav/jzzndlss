<div class="all-tags">
    {% if helper.isArray(item.allTags) %}
        {% for t in item.allTags %}
            <a class="chip dib gid-{{ t.gid }}" href="{{ helper.link('@filter_by_category_and_location_and_tags_1', item.category, helper.provinceSlug(item.postalCode), t.slug) }}" title="{{ helper.trans('ad.link.filter_by_tag') }}">
                <span class="dib lh32 vt">{{ t.name }}</span>
                <i class="dib lh32 vt material-icons">insert_link</i>
            </a>
        {% endfor %}
    {% endif %}
</div>