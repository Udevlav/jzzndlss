{% extends 'layout.volt' %}

{% block title%}{{ title }}{% endblock %}

{% block content %}
    <div class="container">
        <div class="row">
            <div class="col s6 offset-s3 pt100 pb100">
                <div class="error">
                    <h1 class="x16 dark bold">{{ code }}</h1>
                    <p class="x14 grey">{{ message }}

                    {% if config.app.debug %}
                        {% if throwable is defined %}
                            {{ partial('layout/throwable', ['t': throwable ]) }}
                        {% endif %}
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
{% endblock %}
