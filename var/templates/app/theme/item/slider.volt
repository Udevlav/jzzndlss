<div class="carousel carousel-slider fw center" data-indicators="true">
    {% for media in images %}
        {% if media.masterId and media.version === size %}
            {% if (entity.layout === 'LANDSCAPE' and media.isLandscape()) or (entity.layout !== 'LANDSCAPE' and media.isPortrait()) %}

                <a class="carousel-item" href="#media-{{ media.id }}!">
                    <img src="{{ media.path }}" title="{{ entity.getSeoLabel() }}" alt="{{ entity.getSeoLabel() }}" />
                </a>

            {% endif %}
        {% endif %}
    {% endfor %}
</div>
{% if entity.layout !== 'PORTRAIT_LORES' %}
    <div class="view-images">
        <a class="button fill white">
            <i class="material-icons x16 light vm mr5">photo_camera</i>
            <span class="vm">{{ helper.trans('ad.label.view_images') }}</span>
        </a>
    </div>
{% endif %}
<div class="advertiser-is-online dn">
    <a class="button fill white dn">
        <i class="material-icons x16 light vm mr5">wifi</i>
        <span class="vm">{{ helper.trans('ad.label.online') }}</span>
    </a>
</div>
<div class="advertiser-is-offline dn">
    <a class="button fill white dn">
        <i class="material-icons x16 light vm mr5">signal_wifi_off</i>
        <span class="vm">{{ helper.trans('ad.label.offline') }}</span>
    </a>
</div>