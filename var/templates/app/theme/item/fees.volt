<div class="col s12 m6 vt detail-fees">
    <h3 class="x14 bold mb15">{{ helper.trans('ad.label.fees') }}</h3>
    {% if entity.fees|length %}
        {% for fee in entity.fees %}
            <p class="mb5">
                <span class="x24 dark currency">{{ fee.fee }}</span>
                <span class="x24 dark">
                    <sup class="light">&nbsp;{{ fee.desc }}</sup>
                </span>
            </p>
        {% endfor %}
    {% else %}
        <p class="mb5">
            {% if entity.fee > 0 %}
                <span class="x24 dark currency">{{ entity.fee }}</span>
                {% if entity.fees|length %}
                <span class="x24 dark">
                    <sup class="light">&nbsp;{{ helper.trans('ad.label.one_hour') }}</sup>
                </span>
                {% endif %}
            {% else %}
                <span class="dark">{{ helper.trans('ad.label.unknown_price') }}</span>
            {% endif %}
        </p>
    {% endif %}

    <div class="row mt25">
        {{ partial('theme/item/payment', ['entity': entity]) }}
    </div>
</div>
