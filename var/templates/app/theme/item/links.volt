<div class="links dn">
    <a class="link active" href="#ad-poster">{{ helper.trans('ad.label.overview') }}</a>

    <span class="separator"> · </span>
    <a class="link" href="#ad-images">{{ helper.trans('ad.label.images') }}</a>

    {% if videos|length %}
        <span class="separator"> · </span>
        <a class="link" href="#ad-videos">{{ helper.trans('ad.label.videos') }}</a>
    {% endif %}

    <span class="separator"> · </span>
    <a class="link" href="#ad-location">{{ helper.trans('ad.label.location') }}</a>

    <span class="separator"> · </span>
    <a class="link" href="#ad-experiences">{{ helper.trans('ad.label.experiences') }}</a>
</div>


