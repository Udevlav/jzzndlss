<form class="experience-form form-async mt100 mb0" action="{{ helper.link('@experiences', entity.slug) }}" method="POST">
    <input type="hidden" id="_csrf" name="_csrf" value="{{ experienceForm.getCsrfToken() }}" />
    <input type="hidden" id="adId" name="adId" value="{{ entity.id }}"  />
    <input type="hidden" id="rating" name="rating" value="0" class="required mapped-field" data-validator="gtz" />

    <div class="container">
        <div class="row">
            <div class="col s10 offset-s1">
                <h2 class="x20 upper lred bold mb15">{{ helper.trans('experience.title.new') }}</h2>
                <div class="input-rating">
                    <span class="dib vm mr10 stars">
                        <span class="star off" data-index="1">
                            <i class="material-icons lighter">star_border</i>
                            <i class="material-icons light">star</i>
                        </span>
                        <span class="star off" data-index="2">
                            <i class="material-icons lighter">star_border</i>
                            <i class="material-icons light">star</i>
                        </span>
                        <span class="star off" data-index="3">
                            <i class="material-icons lighter">star_border</i>
                            <i class="material-icons light">star</i>
                        </span>
                        <span class="star off" data-index="4">
                            <i class="material-icons lighter">star_border</i>
                            <i class="material-icons light">star</i>
                        </span>
                        <span class="star off" data-index="5">
                            <i class="material-icons lighter">star_border</i>
                            <i class="material-icons light">star</i>
                        </span>
                    </span>
                    <span class="dib vm x36 dark actual-rating dn"></span>
                    <span class="dib vm x24 light max-rating dn">/5</span>
                    <span id="rating-error" class="dn">{{ helper.trans('experience.error.rating_required') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s10 offset-s1">
                <input autocomplete="off" type="text" id="author" name="author" class="validate mapped-field" required="required" aria-required="true" placeholder="{{ helper.trans(experienceForm.get('author').getAttribute('placeholder')) }}" minlength="3" maxlength="32" />
                <label class="active" data-error="{{ helper.trans('validation.required') }}" for="author">{{ helper.trans(experienceForm.get('author').getLabel()) }}</label>
            </div>
            <div class="input-field col s10 offset-s1">
                <textarea id="text" name="text" class="materialize-textarea validate mapped-field" required="required" placeholder="{{ helper.trans(experienceForm.get('text').getAttribute('placeholder')) }}" data-length="{{ experienceForm.get('text').getAttribute('minLength') }}" minlength="50" maxlength="1024"></textarea>
                <label data-error="{{ helper.trans('validation.required') }}" for="text">{{ helper.trans(experienceForm.get('text').getLabel()) }}</label>
            </div>
            <div class="col s10 offset-s1">
                <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
            </div>
        </div>
        <div class="row">
            <div class="col s10 offset-s1">
                <button class="button x18 big red submit form-submit">
                    {{ helper.trans('experience.link.submit') }}
                </button>
            </div>
        </div>
    </div>

    <a class="overlay-close" href="#" role="button">
        <i class="material-icons">close</i>
    </a>
</form>