{% if experiences|length %}
    <div class="row mt25">
        <div class="col s12">
            <a id="add-experience" class="button fill white new-experience add-experience x16">
                <i class="material-icons x16 light vm mr5">add</i>
                <span class="vm">{{ helper.trans('ad.link.add_experience') }}</span>
            </a>
            <h3 class="mt5">
                {% if entity.numExperiences > 1 %}
                    <span class="dib mb5 mr10">{{ helper.trans('ad.label.num_experiences', entity.numExperiences) }}</span>
                {% else %}
                    <span class="dib mb5 mr10">{{ helper.trans('ad.label.one_experience') }}</span>
                {% endif %}
                <span class="dib x14 light">
                    {{ partial('svg/rating', [ 'rating': entity.rating ]) }}
                </span>
            </h3>
        </div>
        {% for experience in experiences %}
            <div class="col s12">
                <div class="row ad-experience">
                    <i class="comments-icon material-icons x24 light vt">comments</i>
                    <div class="col s12">
                        <div class="line pl48 mt15"></div>
                        <p class="x16 pl48 bold">{{ experience.author }}</p>
                        <p class="x14 pl48 mb15">{{ helper.date('F Y', experience.createdAt) }}</p>
                    </div>
                    <div class="col s12">
                        <p class="x12 pl48 mb15 light">
                            {{ partial('svg/rating', [ 'rating': experience.rating ]) }}
                        </p>
                    </div>
                    <div class="col s12">
                        <p class="x18 pl48 grey">{{ experience.text }}</p>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
{% else %}
    <div class="row mt25">
        <div class="col s12">
            <h3 class="mb15">{{ helper.trans('ad.label.experiences') }}</h3>
            <p class="x16 mb15">{{ helper.trans('ad.label.no_experiences_yet') }}</p>
            <a id="add-experience" class="button fill white new-experience x16">
                <i class="material-icons x16 light vm mr5">star_half</i>
                <span class="vm">{{ helper.trans('ad.link.add_experience') }}</span>
            </a>
        </div>
    </div>
{% endif %}
