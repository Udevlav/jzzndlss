<form class="row publish-form mb0" action="{{ action }}" method="{{ method }}" enctype="{{ enctype }}">
    <section>
        <div class="container">
            <input type="hidden" id="_csrf"      name="_csrf"        value="{{ form.getCsrfToken() }}" />
            <input type="hidden" id="fee"        name="fee"          value="{{ entity.fee }}"  />
            <input type="hidden" id="country"    name="country"      value="{{ entity.country }}"  />
            <input type="hidden" id="postalCode" name="postalCode"   value="{{ entity.postalCode }}"  />
            <input type="hidden" id="city"       name="city"         value="{{ entity.city }}"  />
            <input type="hidden" id="zone"       name="zone"         value="{{ entity.zone }}"  />
            <input type="hidden" id="lat"        name="lat"          value="{{ entity.lat }}"  />
            <input type="hidden" id="lng"        name="lng"          value="{{ entity.lng }}"  />

            {{ flash.output() }}

            <fieldset>
                <div class="row">
                    <div class="col s12">
                        <h2 id="publish-what" class="x20 upper lred bold mb15">{{ helper.trans('ad.title.what') }}</h2>
                    </div>
                </div>
                <div class="row">
                    {{ partial('theme/form/select', ['entity': entity, 'field': form.get('category')]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/input', ['entity': entity, 'field':form.get('title')]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/textarea', ['entity': entity, 'field':form.get('text')]) }}
                </div>
            </fieldset>

            <fieldset>
                <div class="row">
                    <div class="col s12">
                        <h2 id="publish-about-you" class="x20 upper lred bold mb15">{{ helper.trans('ad.title.you') }}</h2>
                    </div>
                </div>
                <div class="row">
                    {{ partial('theme/form/input', ['entity': entity, 'field':form.get('name'), 'cols': 6]) }}
                    {{ partial('theme/form/input', ['entity': entity, 'field':form.get('age'), 'cols': 6]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/select', ['entity': entity, 'field': form.get('ethnicity'), 'cols': 6 ]) }}
                    {{ partial('theme/form/select', ['entity': entity, 'field': form.get('origin'), 'cols': 6 ]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/tel', ['entity': entity, 'field':form.get('phone'), 'cols': 6]) }}
                    {{ partial('theme/form/email', ['entity': entity, 'field':form.get('email'), 'cols': 6]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/input', ['entity': entity, 'field':form.get('twitter'), 'cols': 6]) }}
                </div>
                <div class="row">
                    {{ partial('theme/form/checkbox', ['entity': entity, 'field':form.get('whatsapp'), 'cols': 6]) }}
                    {{ partial('theme/form/checkbox', ['entity': entity, 'field':form.get('independent'), 'cols': 6 ]) }}
                </div>
            </fieldset>

            <fieldset>
                <div class="row pt25 pb15">
                    <div class="col s12">
                        <h2 id="publish-where" class="x20 upper lred bold mb15">{{ helper.trans('ad.title.where') }}</h2>
                        <p class="x16 grey">{{ helper.trans('ad.help.where') }}</p>
                        {% set postalCodeField = form.get('postalCode') %}
                        <div class="group-error{% if not postalCodeField.hasMessages() %} dn{% endif %}">
                            <p>{{ helper.trans('ad.error.where') }}</p>
                        </div>
                    </div>
                </div>
                <div class="row mb0">
                    <div class="input-field col s12">
                        <input type="text" id="address-lookup" name="address" class="form-control" placeholder="{{ helper.trans('ad.placeholder.where') }}" value="{{ entity.zone }}" tabindex="{{ helper.nextTabIndex() }}" />
                        <label class="active" for="address-lookup">{{ helper.trans('ad.label.location') }}</label>
                    </div>
                </div>
            </fieldset>

            <fieldset class="map-wrapper">
                <div class="row">
                    <div class="col s12 map" data-api-key="{{ config.map.apiKey }}" data-center-lat="{{ config.geo.center.lat }}" data-center-lng="{{ config.geo.center.lng }}">
                        <div class="gmap" tabindex="{{ helper.nextTabIndex() }}"></div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="row fees pt25 pb15">
                    <div class="col s12">
                        <h2 id="publish-fees" class="x20 upper lred bold mb15">{{ helper.trans('ad.label.fees') }}</h2>
                        <p class="x16 grey mb15">{{ helper.trans('ad.help.fees') }}</p>
                        <div class="group-error dn">
                            <p>{{ helper.trans('ad.error.no_blank_fees') }}</p>
                        </div>
                    </div>
                    {{ partial('theme/form/checkbox', ['entity': entity, 'field':form.get('cash'), 'cols': 6 ]) }}
                    {{ partial('theme/form/checkbox', ['entity': entity, 'field':form.get('card'), 'cols': 6 ]) }}
                </div>
                {{ partial('theme/form/fees', ['entity': entity, 'field':form.get('fees')]) }}
            </fieldset>

            <fieldset>
                {{ partial('theme/form/availability', ['entity': entity, 'field':form.get('availability')]) }}
            </fieldset>

            <fieldset>
                {{ partial('theme/form/uploader', ['entity': entity, 'field':form.get('images'), 'medias': images, 'type': 'image']) }}
            </fieldset>

            <fieldset>
                {{ partial('theme/form/uploader', ['entity': entity, 'field':form.get('videos'), 'medias': videos, 'type': 'video']) }}
            </fieldset>

            <fieldset>
                <div class="row">
                    <div class="col s12 rel">
                        <h2 id="publish-gid1" class="x20 upper lred bold mt25 mb25">{{ helper.trans('gid.label.gid1') }}</h2>
                    </div>
                    {{ partial('theme/form/checkboxes', ['entity': entity, 'field': form.get('tags1') ]) }}
                </div>
            </fieldset>

            <fieldset>
                <div class="row mt25">
                    <div class="col s12 rel">
                        <h2 id="publish-gid2" class="x20 upper lred bold mt25 mb25">{{ helper.trans('gid.label.gid2') }}</h2>
                    </div>
                    {{ partial('theme/form/checkboxes', ['entity': entity, 'field': form.get('tags2') ]) }}
                </div>
            </fieldset>

            <fieldset>
                <div class="row">
                    <div class="col s12 rel">
                        <h2 id="publish-gid3" class="x20 upper lred bold mt25 mb25">{{ helper.trans('gid.label.gid3') }}</h2>
                    </div>
                    {{ partial('theme/form/checkboxes', ['entity': entity, 'field': form.get('tags3') ]) }}
                </div>
            </fieldset>
            
            <fieldset>
                <div class="row">
                    <div class="col s12 rel">
                        <h2 id="publish-gid4" class="x20 upper lred bold mt25 mb25">{{ helper.trans('gid.label.gid4') }}</h2>
                    </div>
                    {{ partial('theme/form/checkboxes', ['entity': entity, 'field': form.get('tags4') ]) }}
                </div>
            </fieldset>

            <fieldset>
                <div class="row">
                    <div class="col s12 rel">
                        <h2 id="publish-gid5" class="x20 upper lred bold mt25 mb25">{{ helper.trans('gid.label.gid5') }}</h2>
                    </div>
                    {{ partial('theme/form/checkboxes', ['entity': entity, 'field': form.get('tags5') ]) }}
                </div>
            </fieldset>

        </div>
    </section>
    <section class="bglight pb25">
        <div class="container">
            <fieldset>
                <div class="row mt50">
                    <div class="col s12">
                        <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <div class="row">
                    {{ partial('theme/form/checkbox', ['entity': entity, 'field': form.get('sms'), 'cols': 12 ]) }}
                </div>
                <div class="row">
                    <div class="col s12">
                        <button class="button x18 big red submit">
                            {{ helper.trans('ad.link.publish') }}
                        </button>
                    </div>
                </div>
            </fieldset>
        </div>
    </section>
</form>
