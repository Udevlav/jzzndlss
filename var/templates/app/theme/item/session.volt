<script type="text/x-html-template" class="chat-session-message-template">
    {% include 'theme/item/message.volt' %}
</script>

<div class="chat-session">
    <div class="chat-session-header">
        <div class="chat-session-peer">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 l8 offset-m1 offset-l2">
                        <p class="chat-session-peer-name x16 bold dark mt15"></p>
                        <p class="chat-session-peer-time x14 light"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="chat-session-messages">
        <div class="chat-scroll">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 l8 offset-m1 offset-l2 mt50">

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="chat-session-form">
        <form class="form-async" method="POST" action="{{ helper.link('@chat_post') }}">
            <input type="hidden" id="peerSid" name="peerSid" value="" class="required mapped-field" />
            <input type="hidden" id="advertiserSid" name="advertiserSid" value="" class="required mapped-field" />
            <div class="container rel">
                <textarea id="text" name="text" class="validate mapped-field" required="required" placeholder="{{ helper.trans(experienceForm.get('text').getAttribute('placeholder')) }}" data-length="{{ experienceForm.get('text').getAttribute('minLength') }}" minlength="50" maxlength="1024"></textarea>
            </div>
            <div class="fixed-action-btn fab-send">
                <a role="button" class="btn-floating btn-large red form-submit" title="{{ helper.trans('chat.link.send') }}" aria-label="{{ helper.trans('chat.link.send') }}">
                    <i class="large material-icons">send</i>
                </a>
            </div>
        </form>
    </div>

    <a class="overlay-close" href="#" role="button">
        <i class="material-icons">close</i>
    </a>
</div>