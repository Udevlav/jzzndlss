<img class="hero-img responsive-img" src="{{ helper.poster(entity, 'md') }}" title="{{ entity.getSeoLabel() }}" alt="{{ entity.getSeoLabel() }}" />
{% if entity.layout !== 'PORTRAIT_LORES' %}
    <div class="view-images">
        <a class="button fill white">
            <i class="material-icons x16 light vm mr5">photo_camera</i>
            <span class="vm">{{ helper.trans('ad.label.view_images') }}</span>
        </a>
    </div>
{% endif %}
<div class="advertiser-is-online dn">
    <a class="button fill white dn">
        <i class="material-icons x16 light vm mr5">wifi</i>
        <span class="vm">{{ helper.trans('ad.label.online') }}</span>
    </a>
</div>
<div class="advertiser-is-offline dn">
    <a class="button fill white dn">
        <i class="material-icons x16 light vm mr5">signal_wifi_off</i>
        <span class="vm">{{ helper.trans('ad.label.offline') }}</span>
    </a>
</div>