<div class="line mt15"></div>
<div class="container">
    <div class="row mt25 mb25">
        <div class="col s12">
            <p class="x14 light mb15">{{ helper.trans('ad.label.disclaimer') }}</p>
            <a class="button fill white" href="{{ helper.link('@complaint', entity.slug) }}">
                <i class="material-icons x16 grey vm mr5">flag</i>
                <span class="vm">{{ helper.trans('app.link.complaint') }}</span>
            </a>
        </div>
    </div>
</div>
