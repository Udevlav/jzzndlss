<div class="row mt25">
    <div class="col s12">
        <h2 class="upper mb10">
            <span>{{ entity.name }}{% if entity.age > 0 %}, {{ helper.trans('ad.label.aged', entity.age) }}{% endif %}</span>
            <span class="dib">&nbsp;&nbsp;·&nbsp;&nbsp;</span>
            <span>{{ helper.category(entity.category) }}</span>
        </h2>
        <h1 class="mb10">{{ entity.title }}</h1>
        <p class="light x16">{{ entity.city }}</p>
    </div>
</div>

<div class="row mt25">
    <div class="col s12 light">
        <i class="material-icons vm">rss_feed</i>
        <span class="x14 vm">
            <span>{{ helper.date('F Y', entity.publishedAt) }}</span>
            <span class="separator"> · </span>
            <span>{{ helper.trans('ad.label.visits', entity.numHits) }}</span>
        </span>
    </div>
</div>