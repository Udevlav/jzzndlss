<div class="chat-list-item">
    <div class="chat-list-item-avatar">
    </div>
    <div class="chat-list-item-peer">
        <p class="chat-list-item-peer-name"></p>
        <p class="chat-list-item-peer-peek"></p>
        <p class="chat-list-item-peer-time"></p>
    </div>
</div>
