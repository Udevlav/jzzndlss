<form action="{{ helper.link('@promote_now', token, entity.slug) }}" method="POST" class="container mt100">
    <div class="row">
        <div class="col s10 offset-s1">
            <h2 class="x20 upper lred bold mb15">{{ helper.trans('ad.title.free_promotion') }}</h2>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s10 offset-s1">
            <input autocomplete="off" type="email" id="author_email" name="author_email" class="validate mapped-field" required="required" aria-required="true" placeholder="{{ helper.trans('ad.placeholder.free_promotion') }}" />
            <label class="active" data-error="{{ helper.trans('validation.required') }}" for="author">{{ helper.trans('ad.label.email') }}</label>
        </div>
        <div class="col s10 offset-s1">
            <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
        </div>
    </div>
    <div class="row">
        <div class="col s10 offset-s1">
            <button class="button x18 big red submit form-submit">
                {{ helper.trans('ad.link.free_promotion') }}
            </button>
        </div>
    </div>
</form>

<a class="overlay-close" href="#" role="button">
    <i class="material-icons">close</i>
</a>
