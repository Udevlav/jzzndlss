<form class="online-peer-form form-async mt75 mb0" action="{{ helper.link('@chat_peer', entity.slug) }}" method="POST">
    <div class="container">
        <div class="row">
            <div class="col s10 l6 offset-s1 offset-l3">
                <input type="hidden" id="_csrf" name="_csrf" value="{{ onlinePeerForm.getCsrfToken() }}" />
                <input type="text" id="displayName" name="displayName" class="validate search-field mapped-field" placeholder="{{ helper.trans('chat.placeholder.display_name') }}" />
                <label class="form-error invalid-peer-display-name dn">{{ helper.trans('chat.error.display_name') }}</label>
                <label class="form-error no-loopback-chat dn">{{ helper.trans('chat.error.loopback') }}</label>
            </div>
            <div class="col s10 offset-s1 mt25">
                <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
            </div>
            <div class="col s10 offset-s1 mt25">
                <button class="button x18 big red submit form-submit">{{ helper.trans('chat.link.connect') }}</button>
            </div>
        </div>
    </div>

    <a class="overlay-close" href="#" role="button">
        <i class="material-icons">close</i>
    </a>
</form>