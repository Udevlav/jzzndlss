<div class="row mt25">
    <div class="col s12">
        <h3 class="mb15">{{ helper.trans(label) }}</h3>
    </div>
    {% for item in tags %}
        <div class="col s6 l4{% if loop.index > 6 %}extra dn{% endif %}">
            <p class="x16 mb10">
                <i class="material-icons x16 light vm mr5">done</i>
                <a href="{{ helper.link('@filter_by_category_and_location_and_tags_1', helper.category(entity.category), helper.slug(entity.city), item.slug) }}" title="{{ helper.trans('app.link.by_category_and_location_and_tag', helper.superCategory(entity.category), entity.city, item.name) }}" class="vm {% if loop.index > 10 %}extra dn{% endif %}">{{ item.name }}</a>
            </p>
        </div>
    {% endfor %}
    {% if tags|length > 6 %}
        <div class="col s12">
            <a class="x14 show-more">{{ helper.trans('ad.link.show_all_tags') }}</a>
        </div>
    {% endif %}
</div>
<div class="line mt15"></div>
