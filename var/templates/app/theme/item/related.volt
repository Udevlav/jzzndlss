{% if related.count() > 0 %}
    <div class="line mt15"></div>
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h3 class="mt15 mb15">{{ helper.trans('ad.label.related') }}</h3>
            </div>
        </div>
        <div class="row grid">
            <div class="grid-sizer col s12 m6 l4"></div>
            {% for relatedAd in related.get(10) %}
                {% if relatedAd.id != entity.id %}
                    <div class="grid-item col s12 m6 l4 pb50">
                        <div class="item mb45">
                            <div class="card">
                                <div class="card-image">
                                    <a class="item" href="{{ helper.link('@ad_seo', relatedAd.hash) }}" data-rel="{{ relatedAd.id }}">
                                        <img class="responsive-img" src="{{ helper.flavour(relatedAd.poster, 'xs') }}" alt="{{ relatedAd.getCaption(false) }}" title="{{ relatedAd.getCaption(false) }}" />
                                    </a>
                                </div>
                                <div class="card-content">
                                    <a class="item" href="/{{ relatedAd.slug }}">
                                        <h4 class="x14 lred bold upper mb10">
                                            <span>{{ relatedAd.name }}{% if relatedAd.age > 0 %}, {{ helper.trans('ad.label.aged', relatedAd.age) }}{% endif %}</span>
                                        </h4>
                                    </a>
                                    <a class="link" href="{{ helper.link('@filter_by_category_and_location', relatedAd.category, helper.provinceSlug(relatedAd.postalCode)) }}">{{ relatedAd.city }}</a>
                                </div>
                                {% if relatedAd.numExperiences %}
                                    <div class="card-action">
                                        <p class="x11 light">
                                            {{ partial('svg/rating', ['rating': relatedAd.rating]) }}
                                            <span class="x12 grey">{{ relatedAd.numExperiences }}</span>
                                        </p>
                                    </div>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                {% endif %}
            {% endfor %}
        </div>
    </div>
{% endif %}
