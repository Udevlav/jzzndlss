<div class="row mt25" tabindex="{{ helper.nextTabIndex() }}">
    <div class="col s12">
        <h3 class="mb15">{{ helper.trans('ad.label.location') }}</h3>
    </div>
    <div class="col s12">
        <a class="db x16 mb15" href="{{ helper.link('@filter_by_category_and_location', helper.category(entity.category), helper.slug(entity.city)) }}" title="{{ helper.trans('app.link.by_category_and_location', helper.superCategory(entity.category), entity.city) }}">{{ entity.postalCode }} {{ entity.city }}</a>
    </div>
    <div class="col s12">
        <div class="rel w100">
            <a class="db alc gmap" href="//maps.google.es/maps?q={{ entity.lat }},{{ entity.lng }}&amp;z=16" target="_blank">
                {#<img class="responsive-img" src="{{ helper.mapCircle(entity, apiKey) }}">#}
                <img class="responsive-img" src="//maps.googleapis.com/maps/api/staticmap?zoom={% if entity.zone %}12{% else %}10{% endif %}&center={{ entity.lat }},{{ entity.lng }}&amp;zoom=13&amp;size=1200x300&amp;maptype=roadmap&#10;&amp;key={{ apiKey }}">
            </a>
            <div class="see-on-maps">
                <a class="button fill white" href="//maps.google.es/maps?q={{ entity.lat }},{{ entity.lng }}&amp;z=16">
                    <i class="material-icons light x16 vm mr5">room</i>
                    <span>{{ helper.trans('ad.link.see_on_maps') }}</span>
                </a>
            </div>
        </div>
    </div>
</div>

