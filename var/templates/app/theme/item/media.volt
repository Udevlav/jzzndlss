<div class="media-item col {% if media.poster %}s6 l4 xl4{% else %} s6 m4 l3 xl3{% endif %}">
    <div class="card">
        <div class="card-image">
            <img src="{{ helper.media(media.path) }}" title="{{ media.name }}" alt="{{ media.name }}" />
        </div>

        <div class="card-content">
            <p class="x14">
                <span class="dark bold">{{ media.name }}</span>
                <span class="grey fdar">{{ media.width }}&times;{{ media.height }}</span>
                {% if media.duration %}
                    <span class="grey duration">{{ media.duration}} min</span>
                {% endif %}
            </p>
            <p class="x12">
                <span class="grey">{{ media.type }}</span>
                <span class="light">{{ media.getBytes() }}</span>
            </p>
        </div>
    </div>
</div>