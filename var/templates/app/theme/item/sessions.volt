<script type="text/x-html-template" class="chat-session-template">
    {% include 'theme/item/chatitem.volt' %}
</script>

<div class="chat-sessions">
    <div class="chat-sessions-header">
        <div class="chat-session-peer">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 l8 offset-m1 offset-l2">
                        <p class="chat-sessions-user-name x16 bold dark mt15">Bayek de Siwa</p>
                        <p class="chat-sessions-user-time x14 light">
                            <i class="material-icons tred x12 mr5 vm">lens</i>
                            <span class="vm">{{ helper.trans('chat.label.online') }}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="chat-sessions-content">
        <div class="chat-scroll">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 l8 offset-m1 offset-l2 mt50">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <a class="overlay-close" href="#" role="button">
        <i class="material-icons">close</i>
    </a>
</div>
