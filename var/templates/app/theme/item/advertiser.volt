<form class="online-advertiser-form form-async mt75 mb0" action="{{ helper.link('@chat_on', entity.slug) }}" method="POST">
    <div class="container">
        <div class="row">
            <div class="col s10 l6 offset-s1 offset-l3">
                <input type="hidden" id="_csrf" name="_csrf" value="{{ onlineAdvertiserForm.getCsrfToken() }}" />
                <input type="text" id="email" name="email" class="validate search-field mapped-field" placeholder="{{ helper.trans('chat.placeholder.email') }}" />
                <p class="x14 light mt25">{{ helper.trans('chat.help.email') }}</p>
                <label class="form-error invalid-advertiser-email dn">{{ helper.trans('chat.error.email') }}</label>
            </div>
            <div class="col s10 l6 offset-s1 offset-l3">
                <div class="g-recaptcha" data-sitekey="{{ config.recaptcha.siteKey }}"></div>
            </div>
            <div class="col s10 l6 offset-s1 offset-l3">
                <button class="button x18 big red submit form-submit">{{ helper.trans('chat.link.connect') }}</button>
            </div>
        </div>
    </div>

    <a class="overlay-close" href="#" role="button">
        <i class="material-icons">close</i>
    </a>
</form>