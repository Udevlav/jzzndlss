<div class="col s12 m6 vt detail-availability">
    <h3 class="x14 bold mb15">{{ helper.trans('ad.label.availability') }}</h3>
    {% if entity.availability | length %}
        {% for availability in entity.availability  %}
            <p>
                {% if availability.timerange is defined %}
                    <span class="x18 dark">
                        <span>{{ helper.timerange(availability.timerange) }}</span>
                        <sup class="light">/{{ helper.trans(helper.dayname(availability.day)) }}</sup>
                    </span>
                {% else %}
                    <span class="x18 dark">
                        <span>{{ availability.from }} - {{ availability.to }}</span>
                        <sup class="light">/{{ helper.trans(helper.dayname(availability.day)) }}</sup>
                    </span>
                {% endif %}
            </p>
        {% endfor %}
    {% else %}
        <p class="">
            <span class="x24 dark">{{ helper.trans('ad.label.full_availability_timerange') }}</span>
            <span class="x24 dark">
                <sup class="light">&nbsp;{{ helper.trans('ad.label.full_availability_dayrange') }}</sup>
            </span>
        </p>
    {% endif %}
</div>