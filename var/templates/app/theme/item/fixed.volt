<div class="container">
    <div class="row">
        <div class="col s12 m6 hide-on-small-only">
            {% if entity.phone and entity.phone !== '000000000' %}
                <a class="db mt15 x32 dark bold call-btn" href="tel:+034{{ entity.phone }}">{{ helper.formatPhone(entity.phone) }}</a>
            {% endif %}
        </div>
        <div class="col s12 m6">
            <p class="db bold lred upper x16">
                <span>{{ entity.name }}</span>
                {% if entity.phone and entity.phone !== '000000000' %}
                    <span class="hide-on-med-and-up"> · </span>
                    <a class="hide-on-med-and-up call-btn" href="tel:+034{{ entity.phone }}">{{ helper.formatPhone(entity.phone) }}</a>
                {% endif %}
            </p>
            {% if entity.fee > 0 %}
                {% if entity.fees | length %}
                    <span class="x18">{{ helper.trans('ad.label.fee_from') }}</span>
                {% endif %}
                <span class="x20 dark currency">{{ helper.intval(entity.fee) }}</span>
                {% if entity.fees | length %}
                <span class="x20 dark">
                    <sup class="light">&nbsp;{{ helper.trans('ad.label.one_hour') }}</sup>
                </span>
                {% endif %}
            {% else %}
                <span class="x20 dark">{{ helper.trans('ad.label.unknown_price') }}</span>
            {% endif %}
            {% if entity.numExperiences %}
                <div class="x10 light">
                    {{ partial('svg/rating', ['rating': entity.rating]) }}
                    <span class="x14 grey">{{ entity.numExperiences }}</span>
                </div>
            {% else %}
                <p class="x14 grey">{{ helper.trans('ad.label.not_yet_rated') }}</p>
            {% endif %}
        </div>
    </div>
</div>
