<div class="row mt25 materialboxed">
    <div class="col s12">
        <h3 class="mb10">{{ helper.trans('ad.label.num_images', entity.numImages) }}</h3>
    </div>
    {% for media in images %}
        {% if ! media.masterId %}
            {% if media.isPortrait() %}

                <div class="col s12 m6 mt15">
                    <a class="db"
                        href="{{ helper.flavour(media.path, 'md') }}"
                        data-caption="{{ entity.getCaption() }}"
                        title="{{ entity.getSeoLabel() }}"
                        data-at-{{ config.media.thumbnail.md }}="{{ helper.flavour(media.path, 'md') }}"
                        {#
                            data-at-{{ config.media.thumbnail.lg }}="{{ helper.flavour(media.path, 'lg') }}"
                            data-at-{{ config.media.thumbnail.xl }}="{{ helper.flavour(media.path, 'xl') }}"
                            #}
                        >
                            <img class="responsive-img"
                                 data-caption="{{ entity.getCaption() }}"
                                 title="{{ entity.getSeoLabel() }}"
                                 alt="{{ entity.getSeoLabel() }}"
                                 src="{{ helper.flavour(media.path, 'sm') }}"
                            />
                        </a>
                    </div>

                {% endif %}
            {% endif %}
        {% endfor %}
        {% for media in images %}
            {% if ! media.masterId %}
                {% if media.isLandscape() %}

                    <div class="col s12 mt15">
                        <a class="db"
                            href="{{ helper.flavour(media.path, 'md') }}"
                            data-caption="{{ entity.getCaption() }}"
                            title="{{ entity.getSeoLabel() }}"
                            data-at-{{ config.media.thumbnail.md }}="{{ helper.flavour(media.path, 'md') }}"
                            {#
                            data-at-{{ config.media.thumbnail.lg }}="{{ helper.flavour(media.path, 'lg') }}"
                            data-at-{{ config.media.thumbnail.xl }}="{{ helper.flavour(media.path, 'xl') }}"
                            #}
                    >
                        <img class="responsive-img materialboxed"
                             data-caption="{{ entity.getCaption() }}"
                             title="{{ entity.getSeoLabel() }}"
                             alt="{{ entity.getSeoLabel() }}"
                             src="{{ helper.flavour(media.path, 'md') }}"
                        />
                    </a>
                </div>

            {% endif %}
        {% endif %}
    {% endfor %}
</div>
<div class="line mt15"></div>