{% if entity.card or entity.cash %}
<div class="col s12">
{% if entity.card %}
    <p class="x16 mb10">
        <i class="material-icons x16 light vm mr5">credit_card</i>
        <span>{{ helper.trans('ad.label.pay_by_card') }}</span>
    </p>
{% endif %}
{% if entity.cash %}
    <p class="x16 mb10">
        <i class="material-icons x16 light vm mr5">attach_money</i>
        <span>{{ helper.trans('ad.label.pay_by_cash') }}</span>
    </p>
{% endif %}
    </div>
{% endif %}
