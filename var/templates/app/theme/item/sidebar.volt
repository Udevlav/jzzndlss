<div class="bounding-box">
    <div class="ad-fees">
        {% if entity.fee > 0 %}
            {% if entity.fees | length %}
                <span class="x16 db">{{ helper.trans('ad.label.fee_from') }}</span>
            {% endif %}
            <span class="x20 dark bold currency">{{ helper.intval(entity.fee) }}</span>
            {% if entity.fees | length %}
                <span class="x20 dark">
                    <sup class="light">&nbsp;{{ helper.trans('ad.label.one_hour') }}</sup>
                </span>
            {% endif %}
        {% else %}
            <span class="x20 dark">
                <span>{{ helper.trans('ad.label.unknown_price') }}</span>
            </span>
        {% endif %}
        {% if entity.numExperiences %}
            <div class="x10 light">
                {{ partial('svg/rating', ['rating': entity.rating]) }}
                <span class="x14 grey">{{ entity.numExperiences }}</span>
            </div>
        {% else %}
            <p class="x14 light mt10">{{ helper.trans('ad.label.not_yet_rated') }}</p>
        {% endif %}
    </div>

    {% if entity.fees|length > 1 %}
        <div class="ad-fees">
            <div class="line mt15"></div>
            <h3 class="x16 bold mb10 upper">{{ helper.trans('ad.label.fees') }}</h3>
            {% for fee in entity.fees %}
                <p class="{% if loop.index > 3 %}extra dn{% endif %}">
                    <span class="x20 dark currency">{{ helper.intval(fee.fee) }}</span>
                    <span class="x20 dark">
                        <sup class="light">&nbsp;{{ fee.desc }}</sup>
                    </span>
                </p>
            {% endfor %}

            {% if entity.cash and entity.card %}
                <p class="x14 light mt15">{{ helper.trans('ad.label.pay_by_cash_and_card') }}</p>
            {% elseif entity.cash %}
                <p class="x14 light mt15">{{ helper.trans('ad.label.pay_by_cash') }}</p>
            {% elseif entity.card %}
                <p class="x14 light mt15">{{ helper.trans('ad.label.pay_by_card') }}</p>
            {% endif %}
        </div>
    {% endif %}

    <div class="ad-timetable">
        <div class="line mt15"></div>
        <h3 class="x16 bold mb10 upper">{{ helper.trans('ad.label.availability') }}</h3>
        {% if entity.availability | length %}
            {% for availability in entity.availability  %}
                <p class="{% if loop.index > 3 %}extra dn{% endif %}">
                    {% if availability.timerange is defined %}
                        <span class="x20 dark">
                            <span>{{ helper.timerange(availability.timerange) }}</span>
                        </span>
                        <span class="x18 dark">
                            <sup class="light">&nbsp;{{ helper.trans(helper.dayname(availability.day)) }}</sup>
                        </span>
                    {% else %}
                        <span class="x20 dark">
                            <span>{{ availability.from }} - {{ availability.to }}</span>
                        </span>
                        <span class="x20 dark">
                            <sup class="light">&nbsp;{{ helper.trans(helper.dayname(availability.day)) }}</sup>
                        </span>
                    {% endif %}
                </p>
            {% endfor %}
        {% else %}
            <p class="">
                <span class="x20 dark">{{ helper.trans('ad.label.full_availability_timerange') }}</span>
                <span class="x20 dark">
                    <sup class="light">&nbsp;{{ helper.trans('ad.label.full_availability_dayrange') }}</sup>
                </span>
            </p>
        {% endif %}
    </div>

    {% if entity.phone and entity.phone != '000000000' %}
        <div class="call-actions">
            <div class="line mt15"></div>
            <a class="button chat-call call-btn x20 dark bold w100 mt15" title="{{ helper.trans('ad.link.call') }}" href="tel:{{ entity.phone }}">
                <i class="material-icons x20 dib ml5 fg-white">phone</i>
                <span class="dib">{{ helper.formatPhone(entity.phone) }}</span>
            </a>
            {% if entity.whatsapp %}
                <a class="button chat-call call-btn x20 dark bold w100 mt15" title="{{ helper.trans('ad.link.call') }}" href="whatsapp:{{ entity.phone }}">
                    <img class="dib ml5" width="24" title="{{ helper.trans('ad.label.allow_whatsapp') }}" src="{{ helper.asset('img/whatsapp.png') }}" />
                    <span class="dib">Whatsapp</span>
                </a>
            {% endif %}
        </div>
    {% endif %}

    <div class="chat-actions dn">
        <div class="line mt15"></div>
        <a class="chat-connect-peer button big red x18 w100">{{ helper.trans('ad.link.chat') }}</a>
        <p class="x12 dark bold mt15 alc">{{ helper.trans('ad.label.now_online', entity.name) }}</p>
    </div>
</div>
