<div class="row mt25">
    <div class="col s12">
        <h3 class="mb10">{{ helper.trans('ad.label.num_videos', entity.numVideos) }}</h3>
    </div>
    {% for media in videos %}
        {% if media.version === 'x264_480p' %}

            <div class="col s12 m6 mt15">
                <video controls class="responsive-video" poster="{{ helper.flavour(entity.poster, 'sm') }}" preload="none" src="{{ media.path }}" alt="{{ entity.getSeoLabel() }}" />
            </div>

        {% endif %}
    {% endfor %}
</div>
<div class="line mt15"></div>