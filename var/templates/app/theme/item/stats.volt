<div class="line mt15"></div>
<div class="container ad-stats">
    <div class="row mt25">
        <div class="col s12">
            <h3 class="mb10">Subir anuncio</h3>
        </div>
    </div>
    <div class="row mb25">
        <div class="col s12 l6 mt15">
            <div class="row">
                <div class="col s12 vb">
                    <p>
                        {% if not entity.promoted %}
                            {# {{ helper.link('@promote_now', token, entity.slug) }} #}
                            <a class="free-promotion-btn button fill white">
                                <span class="material-icons grey x16 dib vm">trending_up</span>
                                <span class="grey x16 dib vm">{{ helper.trans('app.link.promote_now') }}</span>
                            </a>
                        {% endif %}

                        <a class="button fill white" href="{{ helper.link('@promote', token, entity.slug) }}">
                            <span class="material-icons grey x16 dib vm">trending_up</span>
                            <span class="grey x16 dib vm">{{ helper.trans('app.link.promote_auto') }}</span>
                        </a>

                        {#
                        <a class="button fill dn white x16 chat-advertiser-connect" href="{{ helper.link('@chat_on', entity.slug) }}">
                            <i class="material-icons x16 light vm mr5">wifi</i>
                            <span class="vm">{{ helper.trans('ad.link.advertiser_online') }}</span>
                        </a>
                        #}
                    </p>

                </div>
                <div class="col mt25 s12 vb">
                    <p class="x14 light mb15">{{ helper.trans('ad.label.owner_section') }}&nbsp;<a href="{{ helper.link('@send_link', entity.slug) }}">{{ helper.trans('ad.link.send_link') }}</a></p>
                </div>
            </div>
        </div>
        <div class="col s12 l6">
            <div class="row ad-stats-figures dn">
                <div class="col s12">
                    <h3 class="mb10">Estadísticas</h3>
                </div>
                <div class="col s4 mt15 vt">
                    <p class="x20 bold dgrey" id="num-impressions"></p>
                    <p class="x13 lgrey mb0">Impresiones</p>
                </div>
                <div class="col s4 mt15 vt">
                    <p class="x20 bold dgrey" id="num-days-on-top"></p>
                    <p class="x13 lgrey mb0">Días en el top</p>
                </div>
                <!--
                <div class="col s3 mt15 vt">
                    <p class="x20 bold dgrey" id="num-promotions"></p>
                    <p class="x13 lgrey">Subidas</p>
                </div>
                -->
                <div class="col s4 mt15 vt">
                    <p class="x20 bold dgrey" id="num-phone-clicks"></p>
                    <p class="x13 lgrey mb0">Clicks en el teléfono</p>
                </div>
                <div class="col s12 vt">
                    <canvas id="stats-chart" class="w100"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
