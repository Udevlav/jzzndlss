<div class="row mt25">
    <div class="col s12">
        <p class="x18 grey">{{ entity.text }}</p>
        <p class="x18 grey mt15">{{ helper.trans('ad.label.seen_on_app') }}</p>
    </div>
</div>
<div class="line mt15"></div>