<div class="carousel carousel-slider fw center portrait" data-indicators="true">
    {% for media in images %}
        {% if media.version === size %}
            {% if media.isPortrait() %}

                <div class="carousel-item" href="#media-{{ media.id }}!">
                    <img src="{{ media.path }}" title="{{ entity.name }}" alt="{{ entity.name }}" />
                </div>

            {% endif %}
        {% endif %}
    {% endfor %}
</div>
<div class="view-images">
    <a class="button white">
        <i class="material-icons x16 light vm mr5">photo_camera</i>
        <span class="vm">{{ helper.trans('ad.label.view_images') }}</span>
    </a>
</div>
<div class="advertiser-is-online dn">
    <a class="button white">
        <i class="material-icons x16 light vm mr5">wifi</i>
        <span class="vm">{{ helper.trans('ad.label.online') }}</span>
    </a>
</div>
<div class="advertiser-is-offline dn">
    <a class="button white">
        <i class="material-icons x16 light vm mr5">signal_wifi_off</i>
        <span class="vm">{{ helper.trans('ad.label.offline') }}</span>
    </a>
</div>