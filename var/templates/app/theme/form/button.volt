<button {% if type != 'submit' %}type="{{ type }}"{% endif %} id="{{ name }}" class="waves-effect waves-light btn"{% for attribute in attributes %} {{ attribute['name'] }}="{{ attribute['value'] }}"{% endfor %} tabindex="{{ helper.nextTabIndex() }}">
    {{ helper.trans(label) }}
</button>
