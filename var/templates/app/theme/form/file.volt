<div class="input-field{% if field.hasMessages() %} invalid{% endif %}">
    <label for="{{ field.getName() }}_uploader">{{ helper.trans(field.getLabel()) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>

    {% if field.hasMessages() %}
        {% for message in field.getMessages() %}
            <label for="{{ field.getName() }}" class="error-label">
                {{ message }}
            </label>
        {% endfor %}
    {% endif %}

    <input type="file" id="{{ field.getName() }}_uploader" name="{{ field.getName() }}_uploader" class="form-control-file"{% if field.getAttribute('required') %} required="required"{% endif %}{% if field.getAttribute('placeholder') %} placeholder="{{ helper.trans(field.getAttribute('placeholder')) }}"{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
</div>
