<div class="input-field col s{% if cols is defined %}{{ cols }}{% else %}12{% endif %} l{% if cols is defined %}{{ cols }}{% else %}12{% endif %}">
    {% if field.getAttribute('icon') %}
        <i class="material-icons prefix lighter">{{ field.getAttribute('icon') }}</i>
    {% endif %}
    <input autocomplete="off" type="tel" id="{{ field.getName() }}" name="{{ field.getName() }}" value="{{ field.getValue() }}" class="{% if field.getAttribute('required') %} validate{% endif %}{% if field.hasMessages() %} invalid{% endif %}"{% if field.getAttribute('required') %} required="required"{% endif %}{% if field.getAttribute('placeholder') %} placeholder="{{ helper.trans(field.getAttribute('placeholder')) }}"{% endif %}{% if field.getAttribute('minLength') %} data-length="{{ field.getAttribute('minLength') }}"{% endif %}{% if field.hasMessages() %} data-error="{% for msg in field.getMessages() %}{{ helper.trans(msg) }} {% endfor %}"{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
    <label data-error="{{ helper.trans('validation.error') }}"{% if field.getAttribute('placeholder') %} class="active"{% endif %} for="{{ field.getName() }}">{{ helper.trans(field.getLabel()) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>
</div>
