<div class="availability">
    <div class="row pt25 pb15">
        <div class="col s12">
            <h2 id="publish-availability" class="x20 upper lred bold mb15">{{ helper.trans(field.getLabel()) }}</h2>
            <div class="group-error dn">
                <p>No deben quedar horarios en blanco</p>
            </div>
        </div>

        <input type="hidden" id="{{ field.getName() }}" name="{{ field.getName() }}" value="[]" />

        <div class="input-field col s6">
            <input class="with-gap" type="radio" id="{{ field.getName() }}_layout_all" name="{{ field.getName() }}_layout" value="all" tabindex="{{ helper.nextTabIndex() }}" />
            <label for="{{ field.getName() }}_layout_all">
                {{ helper.trans('ad.label.plain_timetable') }}
            </label>
        </div>

        <div class="input-field col s6">
            <input class="with-gap" type="radio" id="{{ field.getName() }}_layout_custom" name="{{ field.getName() }}_layout" value="custom" tabindex="{{ helper.nextTabIndex() }}" />
            <label for="{{ field.getName() }}_layout_custom">
                {{ helper.trans('ad.label.custom_timetable') }}
            </label>
        </div>
    </div>

    <div class="row">
        <div class="col s12">
            <div class="day day_all">
                <div class="input-field col s4">
                    <input type="checkbox" id="{{ field.getName() }}_day_all_enabled" value="-1"{% if field.getAttribute('help') %} aria-describedBy="{{ field.getName() }}-help"{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
                    <label for="{{ field.getName() }}_day_all_enabled">{{ helper.trans('ad.label.all_days') }}</label>
                </div>
                <div class="input-field col s4">
                    <input type="time" id="{{ field.getName() }}_day_all_from_time" placeholder="{{ helper.trans('ad.placeholder.from') }}" tabindex="{{ helper.nextTabIndex() }}" />
                    <label class="active" data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_day_all_from_time">{{ helper.trans(field.getAttribute('fromLabel')) }}</label>
                </div>
                <div class="input-field col s4">
                    <input type="time" id="{{ field.getName() }}_day_all_to_time" placeholder="{{ helper.trans('ad.placeholder.to') }}" tabindex="{{ helper.nextTabIndex() }}" />
                    <label class="active" data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_day_all_to_time">{{ helper.trans(field.getAttribute('toLabel')) }}</label>
                </div>
            </div>

            {% for day in [
                helper.trans('app.label.mon'),
                helper.trans('app.label.tue'),
                helper.trans('app.label.wed'),
                helper.trans('app.label.thu'),
                helper.trans('app.label.fri'),
                helper.trans('app.label.sat'),
                helper.trans('app.label.sun')
            ] %}
                {% set dayNo = loop.index % 7 %}

                <div class="day day_{{ dayNo }}">
                    <div class="input-field col s4">
                        <input type="checkbox" id="{{ field.getName() }}_day_{{ dayNo }}_enabled" value="{{ dayNo }}" tabindex="{{ helper.nextTabIndex() }}" />
                        <label for="{{ field.getName() }}_day_{{ dayNo }}_enabled">{{ day }}</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="time" id="{{ field.getName() }}_day_{{ dayNo }}_from_time" placeholder="{{ helper.trans('ad.placeholder.from') }}" tabindex="{{ helper.nextTabIndex() }}" />
                        <label class="active" data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_day_{{ dayNo }}_from_time">{{ helper.trans(field.getAttribute('fromLabel')) }}</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="time" id="{{ field.getName() }}_day_{{ dayNo }}_to_time" placeholder="{{ helper.trans('ad.placeholder.to') }}" tabindex="{{ helper.nextTabIndex() }}" />
                        <label class="active" data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_day_{{ dayNo }}_to_time">{{ helper.trans(field.getAttribute('toLabel')) }}</label>
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>
</div>
