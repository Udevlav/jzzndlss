<div class="input-field col s{% if cols is defined %}{{ cols }}{% else %}12{% endif %} l{% if cols is defined %}{{ cols }}{% else %}12{% endif %}">
    {% if field.getAttribute('icon') %}
        <i class="material-icons prefix lighter">{{ field.getAttribute('icon') }}</i>
    {% endif %}
    <input type="email" id="{{ field.getName() }}" name="{{ field.getName() }}" value="{{ field.getValue() }}" class="{% if field.getAttribute('required') %}validate{% endif %}{% if field.hasMessages() %} invalid{% endif %}"{% if field.getAttribute('required') %} required="required"{% endif %}{% if field.getAttribute('placeholder') %} placeholder="{{ helper.trans(field.getAttribute('placeholder')) }}"{% endif %}{% for name, value in field.getAttributes() %} {{ name }}="{{ value }}"{% endfor %}{% if field.hasMessages() %} data-error="{% for msg in field.getMessages() %}{{ msg }} {% endfor %}"{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
    <label data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}">{{ helper.trans(field.getLabel()) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>
</div>
