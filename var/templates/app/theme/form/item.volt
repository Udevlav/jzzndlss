<div class="media-item col s12 m6 l4 xl3 idle{% if (media is defined) and media.poster %} selected{% endif %}">
    <div class="card">
        <input type="hidden" name="{{ field.getName() }}[]"{% if media is defined %} value="{{ media.id }}"{% endif %} />

        <a class="delete">
            <i class="material-icons">close</i>
        </a>

        <div class="card-error dn">
            <p class="error"></span>
        </div>

        <div class="card-image media-preview">
            <img{% if media is defined %} src="{{ media.path }}" {% endif %}/>
            <span class="mark badge red x14 bold upper">Poster</span>
        </div>

        <div class="card-content media-label">
            <div class="progress media-progress">
                <div class="determinate progress-bar" role="progressbar" aria-valuenow="{% if media is defined %}100{% else %}0{% endif %}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>

            <p class="x14">
                <span class="dark bold fname db ellipsed">{% if media is defined %}{{ media.name }}{% endif %}</span>
                <span class="grey fdar db ellipsed"></span>
            </p>
            <p class="x12">
                <span class="grey ftype db ellipsed">{% if media is defined %}{{ media.type }}{% endif %}</span>
                <span class="light fsize db ellipsed">{% if media is defined %}{{ media.getBytes() }}{% endif %}</span>
            </p>
        </div>
    </div>
</div>