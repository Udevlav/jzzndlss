<div class="input-field col s{% if cols is defined %}{{ cols }}{% else %}12{% endif %} l{% if cols is defined %}{{ cols }}{% else %}12{% endif %}">
    <select id="{{ field.getName() }}" name="{{ field.getName() }}" {% if field.getAttribute('multiple') %} multiple="multiple"{% endif %}{% if field.getAttribute('required') %} required="required"{% endif %}{% if field.hasMessages() %} class="invalid" data-error="{% for msg in field.getMessages() %}{{ helper.trans(msg) }} {% endfor %}"{% endif %} tabindex="{{ helper.nextTabIndex() }}">
        {% if field.getAttribute('emptyText') %}
            <option value="{{ field.getAttribute('emptyValue') }}">{{ helper.trans(field.getAttribute('emptyText')) }}</option>
        {% endif %}
        {% for value, label in field.getOptions() %}
            <option value="{{ value }}"{% if value === field.getValue() %} selected="selected"{% endif %}>{{ helper.trans(label) }}</option>
        {% endfor %}
    </select>
    <label for="{{ field.getName() }}">{{ helper.trans(field.getLabel()) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>
</div>
