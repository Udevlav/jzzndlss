<div class="input col s12">
    <input class="{% if field.getAttribute('gap') %} with-gap{% endif %}{% if field.hasMessages() %} invalid{% endif %}" type="radio" id="{{ field.getName() }}" name="{{ field.getName() }}" value="{{ field.getAttribute('value') }}"{% if field.getAttribute('required') %} required="required"{% endif %}{% if field.getValue() %} checked="checked"{% endif %}{% if field.hasMessages() %}{% for msg in field.getMessages() %} data-error="{{ msg }}" {% endfor %}{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
    <label for="{{ field.getName() }}">{{ helper.trans(field.getLabel()) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>
</div>
