<div class="row mt25 {{ type }}">
    <div class="col s12 pt25 pb15">
        <h2 id="publish-{{ type }}" class="x20 upper lred bold mb15">{{ helper.trans(field.getLabel()) }}</h2>
    </div>
    <div class="col s12 uploader {{ field.getName() }}"{% if field.getAttribute('selectable') %} data-selectable="true" data-selection="poster"{% endif %}{% if field.getAttribute('extensionWhitelist') %} data-extension-whitelist="{% for item in field.getAttribute('extensionWhitelist') %}{% if loop.index0 %} {% endif %}{{ item }}{% endfor %}"{% endif %}{% if field.getAttribute('extensionBlacklist') %} data-extension-blacklist="{% for item in field.getAttribute('extensionBlacklist') %}{% if loop.index0 %} {% endif %}{{ item }}{% endfor %}"{% endif %}{% if field.getAttribute('mediaTypeWhitelist') %} data-mediatype-whitelist="{% for item in field.getAttribute('mediaTypeWhitelist') %}{% if loop.index0 %} {% endif %}{{ item }}{% endfor %}"{% endif %}{% if field.getAttribute('mediaTypeBlacklist') %} data-mediatype-blacklist="{% for item in field.getAttribute('mediaTypeBlacklist') %}{% if loop.index0 %} {% endif %}{{ item }}{% endfor %}"{% endif %}{% if field.getAttribute('minFileSize') %} data-filesize-min="{{ field.getAttribute('minFileSize') }}"{% endif %}{% if field.getAttribute('maxFileSize') %} data-filesize-max="{{ field.getAttribute('maxFileSize') }}"{% endif %}{% if field.getAttribute('minFiles') %} data-files-min="{{ field.getAttribute('minFiles') }}"{% endif %}{% if field.getAttribute('maxFiles') %} data-files-max="{{ field.getAttribute('maxFiles') }}"{% endif %}>
        {% if field.getAttribute('selectable') %}
            <input type="hidden" id="poster" name="poster"{% if medias is defined %}{% for media in medias %}{% if media.poster %} value="{{ media.id }}"{% endif %}{% endfor %}{% endif %} />
        {% endif %}

        {% if field.getAttribute('description') %}
            <p class="x16 grey" id="{{ field.getName() }}-desc">{{ helper.trans(field.getAttribute('description')) }}</p>
        {% endif %}

        {% if field.getAttribute('help') %}
            <p class="x14 light" id="{{ field.getName() }}-help">{{ helper.trans(field.getAttribute('help')) }}</p>
        {% endif %}

        <script type="text/x-html-template" class="tpl tpl-media">
            {{ partial('theme/form/item', ['field': field]) }}
        </script>

        <div class="group-error min-items dn">
            <p>{{ helper.trans(field.getAttribute('labelMinItems')) }}</p>
        </div>

        {% for message in field.getMessages() %}
            <div class="group-error">
                <p>{{ helper.trans(message) }}</p>
            </div>
        {% endfor %}

        {% if field.getAttribute('selectable') %}
            <div class="group-error selectable-required dn">
                <p>{{ helper.trans(field.getAttribute('labelSelectableRequired')) }}</p>
            </div>
        {% endif %}

        <div class="media row">
            <div class="col s12">
                <div class="uploader-items row">
                    {% if medias is defined %}
                        {% for media in medias %}
                            {% if media.isImage() %}
                                {{ partial('theme/form/item', ['field': field, 'media': media]) }}
                            {% elseif media.isVideo() %}
                                {{ partial('theme/form/item', ['field': field, 'media': media]) }}
                            {% endif %}
                        {% endfor %}
                    {% endif %}
                </div>
            </div>
        </div>

        <div class="btn-bar">
            <button type="button" class="button x16 big red">
                <span>{{ helper.trans(field.getAttribute('labelAdd')) }}</span>
                <input type="file" {% if type == 'video' %}accept="video/*" {% else %}accept="image/*" {% endif %} id="{{ field.getName() }}_uploader" multiple class="form-control-file"{% if field.getAttribute('required') %} required="required"{% endif %}{% if field.getAttribute('help') %} aria-describedBy="{{ field.getName() }}-help"{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
            </button>
        </div>
    </div>
</div>
