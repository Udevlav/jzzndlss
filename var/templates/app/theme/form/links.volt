<div class="links dn">
    <div class="scroll">
        <a class="link active" href="#publish-what">{{ helper.trans('ad.title.what') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-about-you">{{ helper.trans('ad.title.you') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-where">{{ helper.trans('ad.title.where') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-fees">{{ helper.trans('ad.title.fees') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-availability">{{ helper.trans('ad.title.availability') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-image">{{ helper.trans('ad.title.image') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-video">{{ helper.trans('ad.title.video') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-gid1">{{ helper.trans('gid.label.gid1') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-gid2">{{ helper.trans('gid.label.gid2') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-gid3">{{ helper.trans('gid.label.gid3') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-gid4">{{ helper.trans('gid.label.gid4') }}</a>
        <span class="separator"> · </span>
        <a class="link" href="#publish-gid5">{{ helper.trans('gid.label.gid5') }}</a>
    </div>
</div>