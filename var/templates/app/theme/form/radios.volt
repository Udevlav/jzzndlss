{% for value, label in field.getOptions() %}
    <div class="input col s6 m4 l3 xl3">
        <input class="{% if field.getAttribute('gap') %} with-gap{% endif %}{% if field.hasMessages() %} invalid{% endif %}" type="radio" id="{{ field.getName() }}-{{ value }}" name="{{ field.getName() }}-{{ value }}" value="{{ value }}"{% if value == field.getValue() %} checked="checked"{% endif %}{% if help %} aria-describedBy="{{ field.getName() }}-{{ value }}-help"{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
        <label for="{{ field.getName() }}-{{ value }}">{{ helper.trans(label) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>
    </div>
{% endfor %}
