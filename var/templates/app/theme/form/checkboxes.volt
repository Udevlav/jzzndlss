<div>
    {% for value, label in field.getOptions() %}
        <div class="input col s6 m4 l4 xl3 mb10">
            <input class="filled-in {% if field.getAttribute('fill') %} filled-in{% endif %}{% if field.hasMessages() %} invalid{% endif %}" type="checkbox" id="{{ field.getName() }}-{{ value }}" name="{{ field.getName() }}[]" value="{{ value }}"{% if value === field.getValue() %} checked="checked"{% else %}{% if (tags is defined) and helper.inArray(value, tags) %} checked="checked"{% endif %}{% endif %} tabindex="{{ helper.nextTabIndex() }}" />
            <label for="{{ field.getName() }}-{{ value }}">{{ helper.trans(label) }} {% if field.getAttribute('required') %}{{ helper.trans('app.label.required') }}{% endif %}</label>
        </div>
    {% endfor %}
</div>
