<div class="row fees mt25">
    <input type="hidden" id="{{ field.getName() }}" name="{{ field.getName() }}" value="[]" />

    <script type="text/x-html-template" class="tpl tpl-fee">
        <div class="fee-group">
            <div class="input-field col s6">
                <input type="text" id="{{ field.getName() }}_$OFFSET_name" placeholder="{{ helper.trans('ad.placeholder.description') }}" class="validate desc" tabindex="{{ helper.nextTabIndex() }}" />
                <label class="active" data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_$OFFSET_fee">
                    {{ helper.trans('ad.label.description') }}
                </label>
            </div>
            <div class="input-field col s5">
                <input type="text" id="{{ field.getName() }}_$OFFSET_fee" placeholder="{{ helper.trans('ad.placeholder.price') }}" class="validate fee" tabindex="{{ helper.nextTabIndex() }}">
                <label class="active" data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_$OFFSET_fee">
                    {{ helper.trans('ad.label.price') }}
                </label>
            </div>
            <div class="col s1">
                <i class="material-icons delete pointer mt25">close</i>
            </div>
        </div>
    </script>

    <div class="fees-container">
        <div class="fee-group first">
            <div class="input-field col s6">
                <input type="text" id="{{ field.getName() }}_0_name" class="desc" value="{{ helper.trans('ad.label.one_hour') }}" readonly="true" tabindex="{{ helper.nextTabIndex() }}" />
                <label for="{{ field.getName() }}_0_name">
                    {{ helper.trans('ad.label.description') }}
                </label>
            </div>
            <div class="input-field col s5">
                <input type="text" id="{{ field.getName() }}_0_fee" class="validate fee" placeholder="{{ helper.trans('ad.placeholder.price') }}" tabindex="{{ helper.nextTabIndex() }}">
                <label data-error="{{ helper.trans('validation.required') }}" for="{{ field.getName() }}_0_fee">
                    {{ helper.trans('ad.label.price') }}
                </label>
            </div>
            <div class="col s1">&nbsp;</div>
        </div>
    </div>

    <div class="col s12 mgtop20">
        <button type="button" class="button x16 big red" tabindex="{{ helper.nextTabIndex() }}">
            {{ helper.trans('ad.link.add_fee') }}
        </button>
    </div>
</div>
