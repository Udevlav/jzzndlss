{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-already-promoted{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/warning.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ helper.trans('promote.title.already_promoted') }}</h1>
                    <p class="x24 grey">{{ helper.trans('promote.label.already_promoted', promotion.validTo) }}</p>

                    <a class="button red db x16 light mt25 mb50" href="{{ helper.link('@promote', advertiser.token, ad.slug) }}">{{ helper.trans('promote.link.auto') }}</a>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
