{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-payment has-captcha{% endblock %}

{% block content %}
    <form class="payment-form big pt25 mb0" action="{{ helper.link('@payment', advertiser.token, ad.slug) }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" id="_csrf" name="_csrf" value="{{ form.getCsrfToken() }}" />
        <input type="hidden" id="productId" name="productId" value="{% if product is defined %}{{ product.id }}{% endif %}" />
        <input type="hidden" id="when" name="when" value="auto" />

        <div class="container">
            {{ flash.output() }}
        </div>

        <section class="error">
            <div class="container alc">
                <div class="row">
                    <div class="col s12 m8 offset-m2">
                        <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/promote.png') }}" width="128" />
                        <h1 class="x40 mb15">{{ helper.trans('payment.title.promote_auto') }}</h1>
                        <p class="x24 grey alc">{{ helper.trans('payment.text.promote_auto') }}</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="bglight pt25 pb100">
            <div class="container">
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row mb0">
                                    {% if ad.publishedAt %}
                                    <div class="col s12 m2">
                                        <img class="responsive-img" src="{{ helper.poster(ad, 'xs') }}" title="{{ ad.title }}" />
                                    </div>
                                    {% endif %}
                                    <div class="col {% if ad.publishedAt %}s10{% else %}s12{% endif %}">
                                        <p class="x16 lred bold upper mb5">{{ ad.name }}{% if ad.age > 0 %}, {{ helper.trans('ad.label.aged', ad.age) }}{% endif %}</p>
                                        <p class="x20 lh25 dark bold mb15">{{ helper.substr(ad.title, 60) }}</p>
                                        <p class="x16 lgrey">{{ ad.city }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row all">
                    <div class="col s12 l10 offset-l1">
                        <div class="row step1">
                            <div class="col s12">
                                <h2 class="x16 bold lred upper alc mt25 mb15">{{ helper.trans('payment.title.select_product') }}</h2>
                            </div>

                            {% for product in products %}
                                {% if product.dayspan == 1 %}
                                    <div class="col s12 m6 l4 mt15 product-col product-small">
                                    {# <div class="col s12 m6 l4 mt15 product-col"> #}
                                        <input type="hidden" id="product_{{ product.id }}" value="{{ product.id }}" />
                                        <div class="card">
                                            <div class="card-content alc">
                                                <img src="{{ helper.asset('img/promote.png') }}" width="48" />
                                                <p class="grey mb0 alc">
                                                    <span class="label db x16 bold lred upper mt10">{{ product.label }}</span>
                                                    <span class="currency db x32 dark bold mb5">
                                                    <span class="x14 lgrey">por</span><sup></sup>
                                                    <span class="credits">{{ product.price }}</span><sup></sup>
                                                </span>
                                                </p>
                                                {% if product.savings %}
                                                    <span class="dib mb15 badge green" data-badge-caption="{{ product.savings }}"></span>
                                                {% else %}
                                                    <span class="dib mb15">&nbsp;</span>
                                                {% endif %}
                                                <span class="product-desc desc db x16 lgrey lh20 mb5">{{ product.description }}</span>
                                                <span class="button fill white x16">
                                                <i class="material-icons x16 light vm mr5">done</i>
                                                <span class="vm sec-dn">{{ helper.trans('payment.link.select_coupon') }}</span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                {% endif %}
                            {% endfor %}

                            {% set currentProductDaySpan = 1 %}
                            {% for product in products %}
                                {% if product.dayspan != currentProductDaySpan %}
                                    <div class="col alc s12 mt15 product-title">
                                        <span class="alc db x16 bold lred upper">
                                            {% if product.dayspan == 7 %}
                                                {{ helper.trans('product.label.weekly') }}
                                            {% elseif product.dayspan == 15 %}
                                                {{ helper.trans('product.label.biweekly') }}
                                            {% elseif product.dayspan == 30 %}
                                                {{ helper.trans('product.label.monthly') }}
                                            {% endif %}
                                        </span>
                                    </div>
                                {% endif %}
                                {% set currentProductDaySpan = product.dayspan %}
                                {% if product.dayspan > 1 %}
                                    <div class="col s12 m6 l4 mt15 product-col product-small">
                                    {#<div class="col s6 m4 l3 mt15 product-col product-small">#}
                                        <input type="hidden" id="product_{{ product.id }}" value="{{ product.id }}" />
                                        <div class="card">
                                            <div class="card-content alc">
                                                <img src="{{ helper.asset('img/promote.png') }}" width="48" />
                                                <p class="grey mb0 alc">
                                                    <span class="label db x16 bold lred upper mt10">{{ product.label }}</span>
                                                    <span class="x14 lgrey">Durante {{ product.dayspan }} día{% if product.dayspan > 1 %}s{% endif %}</span>
                                                    <span class="currency db x32 dark bold mb5">
                                                    <span class="x14 lgrey">por</span><sup></sup>
                                                    <span class="credits">{{ product.price }}</span><sup></sup>
                                                </span>
                                                </p>
                                                {% if product.savings %}
                                                    <span class="dib mb15 badge green" data-badge-caption="{{ product.savings }}"></span>
                                                {% else %}
                                                    <span class="dib mb15">&nbsp;</span>
                                                {% endif %}
                                                <span class="product-desc desc db x16 lgrey lh20 mb5">
                                                    <span class="db">{{ product.description }}</span>
                                                    <span class="db sec-dn">{{ helper.trans('product.label.num_promotions', product.promotions) }}</span>
                                                </span>
                                                <span class="button fill white x16">
                                                    <i class="material-icons x16 light vm mr5">done</i>
                                                    <span class="vm sec-dn">{{ helper.trans('payment.link.select_coupon') }}</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                {% endif %}
                            {% endfor %}
                        </div>
                    </div>
                </div>

                <div class="row step2 dn">
                    <div class="col s12 hgroup alc">
                        <h2 class="x16 bold lred upper alc mt25 mb15">{{ helper.trans('payment.title.select_schedule') }}</h2>
                    </div>
                </div>

                <div class="row step2 dn">
                    {{ partial('theme/form/datepicker', ['entity': request, 'field': form.get('startDate'), 'size': 's12 l6']) }}
                    {{ partial('theme/form/timepicker', ['entity': request, 'field': form.get('startTime'), 'size': 's6 l3']) }}
                    {{ partial('theme/form/timepicker', ['entity': request, 'field': form.get('endTime'), 'size': 's6 l3']) }}
                    <div class="col s6 hide-on-med-and-down">&nbsp;</div>
                    {{ partial('theme/form/checkbox',   ['entity': request, 'field': form.get('fullDay'), 'size': 's12 l6 offset-l6']) }}
                </div>

                <div class="row step3 dn">
                    {#
                    <div class="col s12 hgroup alc">
                        <h2 class="x16 bold lred upper alc mt25 mb15">{{ helper.trans('payment.title.select_payment') }}</h2>
                    </div>
                    #}
                    <div class="col s12 alc">
                        <button class="button red big x18 waves-effect form-submit mt15">{{ helper.trans('payment.link.pay') }}</button>
                        <a href="{{ helper.link('@tos') }}" class="db x14 light mt25">{{ helper.trans('payment.link.policy') }}</a>
                    </div>
                </div>

            </div>
        </section>
    </form>
{% endblock %}
