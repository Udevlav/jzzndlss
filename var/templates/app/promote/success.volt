{% extends 'layout.volt' %}

{% block title %}{{ title }}{% endblock %}
{% block bodyClass %}page-feedback page-promoted{% endblock %}

{% block content %}
    <section class="error">
        <div class="container alc">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <img class="feedback-icon mt100 mb25" src="{{ helper.asset('img/rock.png') }}" width="128" />
                    <h1 class="x40 mb15">{{ helper.trans('advertiser.title.promoted') }}</h1>
                    <p class="x24 grey">{{ helper.trans('advertiser.label.promoted') }}</p>
                    <p class="x24 grey mt25">{{ helper.trans('advertiser.label.promotions_list') }}</p>
                    <ul class="collection mt25">
                        {% for promotion in promotions %}
                            <li class="collection-item all grey">{{ helper.trans('advertiser.label.promotion_date', helper.localDate(promotion.validFrom), helper.localDate(promotion.validTo)) }}</li>
                        {% endfor %}
                    </ul>

                    <a class="button white db x16 light mt25 mb50" href="{{ helper.link('@index') }}">{{ helper.trans('app.label.back_to_home') }}</a>
                </div>
            </div>
        </div>
    </section>
    <section class="trace bglight pt25 pb25">
        <div class="container">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <p class="x16 grey alc">{{ helper.trans('advertiser.text.promoted') }}</p>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
