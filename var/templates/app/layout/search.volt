<div class="overlay top-down overlay-search" id="search">
    <div class="container">
        <div class="row">
            <div class="col s10 l6 offset-s1 offset-l3">
                <form class="search-form" method="get" action="{{ helper.link('@search') }}" role="search">
                    <input type="search" name="q" class="search-field" placeholder="{{ helper.trans('app.placeholder.search') }}" />
                    <input type="submit" value="{{ helper.trans('app.label.search') }}" class="search-submit" />

                    <p class="x16 light mt25">{{ helper.trans('app.label.search_help') }}</p>
                </form>
            </div>

        </div>
    </div>
</div>