{% if filters is defined %}
    {% set filterData = filters.getData() %}
    {% set filterForm = filters.getForm() %}

    <div class="overlay top-down overlay-filter" id="filter">
        <form>
            <div class="filter-wrapper bglight">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s4">
                                    <a class="x14 lred bold upper active" href="#filter-1">Sobre mí</a>
                                </li>
                                <li class="tab col s4">
                                    <a class="x14 lred bold upper" href="#filter-2">Servicios</a>
                                </li>
                                <li class="tab col s4">
                                    <a class="x14 lred bold upper" href="#filter-3">La Alcoba</a>
                                </li>
                            </ul>
                        </div>
                        <div id="filter-1" class="col s12 m6 l8 offset-m3 offset-l2">
                            <div class="row pt25">
                                <div class="col s12">
                                    <p class="x14 lred bold upper mt25 mb15">{{ helper.trans('gid.label.age') }}</p>
                                </div>
                                <div class="input-field col s6">
                                    <input autocomplete="off" type="text" id="age_from" name="ageFrom" value="{{ filterData.ageFrom }}" />
                                    <label for="age_from">{{ helper.trans('app.filter.from') }}</label>
                                </div>
                                <div class="input-field col s6">
                                    <input autocomplete="off" type="text" id="age_to" name="ageTo" value="{{ filterData.ageTo }}" />
                                    <label for="age_to">{{ helper.trans('app.filter.to') }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <p class="x14 lred bold upper mt25 mb15">{{ helper.trans('gid.label.fee') }}</p>
                                </div>
                                <div class="input-field col s6">
                                    <input autocomplete="off" type="text" id="fee_from" name="feeFrom" value="{{ filterData.feeFrom }}" />
                                    <label for="fee_from">{{ helper.trans('app.filter.from') }}</label>
                                </div>
                                <div class="input-field col s6">
                                    <input autocomplete="off" type="text" id="fee_to" name="feeTo" value="{{ filterData.feeTo }}" />
                                    <label for="fee_to">{{ helper.trans('app.filter.to') }}</label>
                                </div>
                            </div>
                            <div class="row pt25">
                                {% for item in filters.getTags(1) %}
                                    <div class="input-field col s6 m4 l3">
                                        <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" {% if filters.hasTag(item.id) %}checked="checked"{% endif %} />
                                        <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>

                        <div id="filter-2" class="col s12">
                            <div class="row pt25">
                                {% for item in filters.getTags(2) %}
                                    <div class="input-field col s6 m4 l3">
                                        <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" {% if filters.hasTag(item.id) %}checked="checked"{% endif %} />
                                        <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>

                        <div id="filter-3" class="col s12">
                            <div class="row pt25">
                                {% for item in filters.getTags(3) %}
                                    <div class="input-field col s6 m4 l3">
                                        <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" {% if filters.hasTag(item.id) %}checked="checked"{% endif %} />
                                        <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                    {#

                                        <div class="row">
                                            <div class="col s12">
                                                <p class="x14 lred bold upper mt25 mb15">{{ helper.trans('gid.label.gid4') }}</p>
                                            </div>
                                            {% for item in filters.getTags(4) %}
                                                <div class="input-field col s6 m4 l3">
                                                    <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" {% if filters.hasTag(item.id) %}checked="checked"{% endif %} />
                                                    <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                                </div>
                                            {% endfor %}
                                        </div>

                                        <div class="row">
                                            <div class="col s12">
                                                <p class="x14 lred bold upper mt25 mb15">{{ helper.trans('gid.label.gid5') }}</p>
                                            </div>
                                            {% for item in filters.getTags(5) %}
                                                <div class="input-field col s6 m4 l3">
                                                    <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" {% if filters.hasTag(item.id) %}checked="checked"{% endif %} />
                                                    <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                                </div>
                                            {% endfor %}
                                        </div>
                                        #}

                </div>
            </div>
            <p class="mb25">&nbsp;</p>
        </form>

        <div class="filter-actions">
            <div class="container alr">
                <button type="button" class="button fill red big x16 remove-filters" aria-label="{{ helper.trans('app.link.close') }}">
                    <span class="upper">{{ helper.trans('app.link.remove_filters') }}</span>
                </button>
                <button type="button" class="button fill big x16 red apply-filters" aria-label="{{ helper.trans('app.link.apply_filter') }}">
                    <span class="upper">{{ helper.trans('app.link.apply_filter') }}</span>
                </button>
            </div>
        </div>
    </div>
{% endif %}
