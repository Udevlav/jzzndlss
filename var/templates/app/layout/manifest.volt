<!-- Startup configuration -->
<link rel="manifest" href="/manifest.json">

<!-- Fallback application metadata for legacy browsers -->
<meta name="application-name" content="{{ config.app.name }}">

{% for icon in config.app.icons %}
    <link rel="{{ icon.rel }}" sizes="{{ icon.sizes }}" href="{{ icon.href }}">

{% endfor %}
