<a class="menu-action menu-right menu-more menu-action-btn" href="#menu" role="button" aria-label="{{ helper.trans('app.label.menu') }}" title="{{ helper.trans('app.label.menu') }}">
    <i class="material-icons">menu</i>
</a>
