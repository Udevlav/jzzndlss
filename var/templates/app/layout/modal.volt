<div id="adult-modal" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s12">
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p class="x16 bold dark">{{ helper.trans('app.title.adult_content') }}</p>
                <p class="x16 grey">{{ helper.trans('app.label.adult_content') }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p class="x16 bold dark">{{ helper.trans('app.title.cookie_usage') }}</p>
                <p class="x16 grey">{{ helper.trans('app.label.cookie_usage') }}</p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="{{ helper.link('@privacy') }}#cookies" class="modal-action modal-close waves-effect waves-green btn-flat">{{ helper.trans('app.link.modify_cookies') }}</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">{{ helper.trans('app.link.accept') }}</a>
    </div>
</div>