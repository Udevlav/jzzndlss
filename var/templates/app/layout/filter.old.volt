{% if filters is defined %}
    {% set filterData = filters.getData() %}
    {% set filterForm = filters.getForm() %}

    <div class="overlay top-down overlay-filter" id="filter">
        <form class="filter-form mb0" action="{{ helper.link('@index') }}" method="post">
            <input type="hidden" id="_csrf" name="_csrf" value="{{ filterForm.getCsrfToken() }}" />
            <input type="hidden" id="offset" name="offset" value="{{ helper.fallback(filterData.offset, 0) }}"  />
            <input type="hidden" id="limit" name="limit" value="{{ helper.fallback(filterData.limit, 10) }}"  />
            <input type="hidden" id="category" name="category" value="{{ filterData.category }}"  />
            <input type="hidden" id="lat" name="lat" value="{{ filterData.lat }}"  />
            <input type="hidden" id="lng" name="lng" value="{{ filterData.lng }}"  />
            <input type="hidden" id="city" name="city" value="{{ filterData.city }}"  />
            <input type="hidden" id="province" name="province" value="{{ filterData.province }}"  />
            <input type="hidden" id="postalCode" name="postalCode" value="{{ filterData.postalCode }}"  />

            <div class="carousel carousel-slider initialized" data-indicators="true">

                <div class="carousel-item" href="#filter-category!">
                    <div class="container">
                        <div class="row">
                            <div class="col s8 l6 offset-s2 offset-l3">
                                <ul class="menu">
                                    <li>
                                        <p class="x14 light">{{ helper.trans('app.link.select_category') }}</p>
                                    </li>
                                    <li role="separator" class="separator"></li>
                                    <li>
                                        <a class="db choice filter-category" data-value="-1">
                                            {{ helper.trans('category.any') }}
                                        </a>
                                    </li>
                                    <li role="separator" class="separator"></li>
                                    <li>
                                        <a class="db choice filter-category{% if filterData.category == 1 %} active{% endif %}" data-value="1" data-slug="chica">
                                            {{ helper.trans('category.cat1') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a class="db choice filter-category{% if filterData.category == 2 %} active{% endif %}" data-value="2" data-slug="chico">
                                            {{ helper.trans('category.cat2') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a class="db choice filter-category{% if filterData.category == 3 %} active{% endif %}" data-value="3" data-slug="travesti">
                                            {{ helper.trans('category.cat3') }}
                                        </a>
                                    </li>
                                    <li role="separator" class="separator"></li>
                                    <li>
                                        <a class="db choice filter-category{% if filterData.category == 4 %} active{% endif %}" data-value="4" data-slug="castings-y-plazas">
                                            {{ helper.trans('category.cat4') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a class="db choice filter-category{% if filterData.category == 5 %} active{% endif %}" data-value="5" data-slug="alquiler-habitaciones">
                                            {{ helper.trans('category.cat5') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a class="db choice filter-category{% if filterData.category == 6 %} active{% endif %}" data-value="6" data-slug="fotografos-profesionales">
                                            {{ helper.trans('category.cat6') }}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" href="#filter-location!">
                    <div class="container">
                        <div class="row">
                            <div class="col s9 l6 offset-s1 offset-l3">
                                <ul class="menu">
                                    <li>
                                        <p class="x14 light">{{ helper.trans('app.link.select_location') }}</p>
                                    </li>
                                    <li role="separator" class="separator"></li>
                                </ul>
                                <div class="input-field">
                                    <input autocomplete="off" type="text" id="filter-location" class="autocomplete" data-api-key="{{ config.map.apiKey }}" placeholder="{{ helper.trans('app.placeholder.location') }}" />
                                    <label for="filter-location dn">{{ helper.trans('app.label.filter_location') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" href="#filter-extra!">
                    <section class="tab-header">
                        <div class="container">
                            <div class="row mb0">
                                <div class="col s12">
                                    <ul class="menu">
                                        <li>
                                            <p class="x14 light">{{ helper.trans('app.link.select_extra') }}</p>
                                        </li>
                                        <li role="separator" class="separator"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb0 bb">
                                <div class="col s12">
                                    <ul class="tabs initialized">
                                        <li class="tab">
                                            <a class="active" href="#filter_tab_1">{{ helper.trans('gid.label.gid0') }}</a>
                                        </li>
                                        <li class="tab">
                                            <a href="#filter_tab_2">{{ helper.trans('gid.label.gid1') }}</a>
                                        </li>
                                        <li class="tab">
                                            <a href="#filter_tab_3">{{ helper.trans('gid.label.gid2') }}</a>
                                        </li>
                                        <li class="tab">
                                            <a href="#filter_tab_4">{{ helper.trans('gid.label.gid3') }}</a>
                                        </li>
                                        <li class="tab">
                                            <a href="#filter_tab_5">{{ helper.trans('gid.label.gid4') }}</a>
                                        </li>
                                        <li class="tab">
                                            <a href="#filter_tab_6">{{ helper.trans('gid.label.gid5') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="tab-content">
                        <div class="container">
                            <div class="row">
                                <div id="filter_tab_1" class="tab col s12">
                                    <div class="row mt25">
                                        <div class="input-field col s6">
                                            <input autocomplete="off" type="text" id="age_from" name="ageFrom" value="{{ filterData.ageFrom }}" />
                                            <label for="age_from">{{ helper.trans('app.filter.age_from') }}</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input autocomplete="off" type="text" id="age_to" name="ageTo" value="{{ filterData.ageTo }}" />
                                            <label for="age_to">{{ helper.trans('app.filter.age_to') }}</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input autocomplete="off" type="text" id="fee_from" name="feeFrom" value="{{ filterData.feeFrom }}" />
                                            <label for="fee_from">{{ helper.trans('app.filter.fee_from') }}</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input autocomplete="off" type="text" id="fee_to" name="feeTo" value="{{ filterData.feeTo }}" />
                                            <label for="fee_to">{{ helper.trans('app.filter.fee_to') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="filter_tab_2" class="tab col s12 scrollable">
                                    <div class="row">
                                        {% for item in filters.getTags(1) %}
                                            <div class="input-field col s6">
                                                <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" />
                                                <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                                <div id="filter_tab_3" class="tab col s12 scrollable">
                                    <div class="row">
                                        {% for item in filters.getTags(2) %}
                                            <div class="input-field col s6">
                                                <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" />
                                                <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                                <div id="filter_tab_4" class="tab col s12 scrollable">
                                    <div class="row">
                                        {% for item in filters.getTags(3) %}
                                            <div class="input-field col s6">
                                                <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" />
                                                <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                                <div id="filter_tab_5" class="tab col s12 scrollable">
                                    <div class="row">
                                        {% for item in filters.getTags(4) %}
                                            <div class="input-field col s6">
                                                <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" />
                                                <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                                <div id="filter_tab_6" class="tab col s12 scrollable">
                                    <div class="row">
                                        {% for item in filters.getTags(5) %}
                                            <div class="input-field col s6">
                                                <input type="checkbox" class="filter-tag" id="filter_tag_{{ item.id }}" name="tags1[]" value="{{ item.id }}" data-slug="{{ item.slug }}" />
                                                <label for="filter_tag_{{ item.id }}">{{ item.name }}</label>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>

            <div class="filter-actions">
                <div class="container alr">
                    <button type="button" class="button white fill big upper x16 red apply-filters" aria-label="{{ helper.trans('app.link.apply_filter') }}">
                        <span>{{ helper.trans('app.link.apply_filter') }}</span>
                    </button>
                </div>
            </div>
        </form>

        <a class="overlay-close" role="button" aria-label="{{ helper.trans('app.label.close') }}" title="{{ helper.trans('app.label.close') }}">
            <i class="material-icons">close</i>
        </a>
    </div>
{% endif %}
