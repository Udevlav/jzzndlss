<div class="disclaimer cookies dn">
    <div class="container">
        <div class="row">
            <div class="col s12 rel mt25">
                <p class="x14 grey">{{ helper.trans('cookies.text.disclaimer') }}</p>
                <a role="button" class="close close-cookies" aria-label="{{ helper.trans('app.label.close') }}" title="{{ helper.trans('app.label.close') }}">
                    <i class="material-icons">close</i>
                </a>
            </div>
        </div>
    </div>
</div>