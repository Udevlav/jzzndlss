<a class="menu-action menu-right menu-search" href="#search" role="button" aria-label="{{ helper.trans('app.label.search') }}" title="{{ helper.trans('app.label.search') }}">
    <i class="material-icons">search</i>
</a>

<a class="menu-action menu-right dn overlay-close" role="button" aria-label="{{ helper.trans('app.label.close') }}" title="{{ helper.trans('app.label.close') }}">
    <i class="material-icons">close</i>
</a>
