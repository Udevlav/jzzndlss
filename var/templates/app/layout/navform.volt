{% if filters is defined %}
    {% set filterData = filters.getData() %}
    {% set filterForm = filters.getForm() %}

    <!-- FILTER_DATA
    {{ filterData | json_encode }}
    -->

    <form class="filter-form mb25" action="{{ helper.link('@index') }}" method="post">
        <input type="hidden" id="_csrf" name="_csrf" value="{{ filterForm.getCsrfToken() }}"/>
        <input type="hidden" id="offset" name="offset" value="{{ helper.fallback(filterData.offset, 0) }}" />
        <input type="hidden" id="limit" name="limit" value="{{ helper.fallback(filterData.limit, 10) }}" />
        <input type="hidden" id="lat" name="lat" value="{{ filterData.lat }}" />
        <input type="hidden" id="lng" name="lng" value="{{ filterData.lng }}" />
        <input type="hidden" id="city" name="city" value="{{ filterData.city }}" />
        <input type="hidden" id="province" name="province" value="{{ filterData.province }}" />
        <input type="hidden" id="postalCode" name="postalCode" value="{{ filterData.postalCode }}" />
        <input type="hidden" id="ageFrom" name="ageFrom" value="{{ filterData.ageFrom }}" />
        <input type="hidden" id="ageTo" name="ageTo" value="{{ filterData.ageTo }}" />
        <input type="hidden" id="feeFrom" name="feeFrom" value="{{ filterData.feeFrom }}" />
        <input type="hidden" id="feeTo" name="feeTo" value="{{ filterData.feeTo }}" />
        <input type="hidden" id="tags" name="tags" value="" />

        <div class="row relative">
            <div class="input-field col s6 m2 col-filter-category">
                <select id="category" name="category" class="nav-select">
                    <option value="1"{% if filterData.category == 1 %} selected="selected"{% endif %}>{{ helper.trans('category.cat1') }}</option>
                    <option value="2"{% if filterData.category == 2 %} selected="selected"{% endif %}>{{ helper.trans('category.cat2') }}</option>
                    <option value="3"{% if filterData.category == 3 %} selected="selected"{% endif %}>{{ helper.trans('category.cat3') }}</option>
                </select>
                <label for="category" class="active">{{ helper.trans('app.filter.category') }}</label>
            </div>
            <div class="input-field col s6 m3 col-filter-location">
                <select id="filter-location" class="nav-select">
                    <option value="any"{% if ((filterData.province == 'any') or (filterData.postalCode == '00')) or ((not filterData.province) and (not filterData.postalCode)) %} selected="selected"{% endif %}></option>
                    {% for provinceCode, provinceName in filters.getProvinces() | sort %}
                        <option data-slug="{{ helper.slug(provinceName) }}" value="{{ provinceCode }}"{% if provinceName == filterData.province or provinceCode == filterData.postalCode or provinceCode == (filterData.postalCode | slice(0, 2)) %} selected="selected"{% endif %}>{{ provinceName }}</option>
                    {% endfor %}
                </select>
                <label for="filter-location">{{ helper.trans('app.label.filter_location') }}</label>
                {#
                <input autocomplete="off" type="text" id="filter-location" class="autocomplete {% if filterData.postalCode %}active{% endif %}" data-api-key="{{ config.map.apiKey }}" value="{% if filterData.city %}{{ helper.ucfirst(filterData.city) }}{% else %}{% if filterData.province %}{{ helper.ucfirst(filterData.province) }}{% else %}{{ filterData.postalCode }}{% endif %}{% endif %}" />
                <label for="filter-location">{{ helper.trans('app.label.filter_location') }}</label>
                #}
            </div>
            <div class="input-field col s6 m3 col-filter-search">
                <input type="text" id="q" name="q" value="{{ filterData.q }}" class="{% if filterData.q %}active{% endif %}" />
                <label for="q">{{ helper.trans('app.label.filter_search') }}</label>
            </div>
            <div class="col s6 m4 col-filter-tags">
                <div class="row">
                    <div class="input-field col s8">
                        <input autocomplete="off" type="text" id="filter-tags" />
                        <label for="filter-tags">{{ helper.trans('app.label.filter_tags') }}</label>
                    </div>
                    <div class="input-field col s4">
                        <button class="button red submit-btn" type="button" title="{{ helper.trans('app.label.filter_search') }}">
                            <i class="material-icons">search</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
{% endif %}
