<div class="carousel carousel-slider center" data-indicators="true">
    {% for item in items %}
        <div class="carousel-item" href="#carousel-item-{{ loop.index }}">
            <div class="parallax-container">
                <div class="parallax">
                    <img src="{{ item.path }}" alt="{{ item.name }}" title="{{ item.name }}" />
                </div>
            </div>
        </div>
    {% endfor %}
</div>