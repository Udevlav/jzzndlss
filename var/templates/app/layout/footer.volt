<footer>

    <div class="line"></div>

    <section class="l0 pt25">
        <div class="container">
            <div class="row">

                <div class="col m4 hide-on-small-and-down">
                    <a href="{{ helper.link('@index') }}" class="db all foot-logo" title="{{ helper.trans('app.link.index') }}">
                        <img class="responsive-img" src="/asset/img/wallalogo1.png" title="{{ config.app.name }}" alt="{{ config.app.name }} Logo" />
                    </a>
                </div>

                <div class="col s6 m4">
                    <h5 class="x14 mt10 dark bold upper">{{ helper.trans('app.label.about') }}</h5>
                    <ul class="marker-list">
                        <li>
                            <a class="db normal mb5 x18" href="{{ helper.link('@whoweare') }}" title="{{ helper.trans('app.label.whoweare') }}">{{ helper.trans('app.link.whoweare') }}</a>
                        </li>
                        <li>
                            <a class="db normal mb5 x18" href="{{ helper.link('@tos') }}" title="{{ helper.trans('app.label.tos') }}">{{ helper.trans('app.link.tos') }}</a>
                        </li>
                        <li>
                            <a class="db normal mb5 x18" href="{{ helper.link('@privacy') }}" title="{{ helper.trans('app.label.privacy') }}">{{ helper.trans('app.link.privacy') }}</a>
                        </li>
                    </ul>
                </div>

                <div class="col s6 m4">
                    <h5 class="x14 mt10 dark bold upper">{{ helper.trans('app.label.links') }}</h5>
                    <ul class="marker-list">
                        <li>
                            <a class="db normal mb5 x18" href="{{ helper.link('@publish') }}" title="{{ helper.trans('app.label.publish') }}">{{ helper.trans('app.link.publish') }}</a>
                        </li>
                        <li>
                            <a class="db normal mb5 x18" href="{{ helper.link('@contact') }}" title="{{ helper.trans('app.label.contact') }}">{{ helper.trans('app.link.contact') }}</a>
                        </li>
                        <!--
                        <li>
                            <select class="locale_selector">
                                {% for locale in config.i18n.locales %}
                                    {% if locale == config.app.locale %}
                                        <option selected="selected" value="{{ locale }}">{{ locale }}</option>
                                    {% else %}
                                        <option value="{{ locale }}">{{ locale }}</option>
                                    {% endif %}
                                {% endfor %}
                            </select>
                        </li>
                         -->
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="l1">
        <div class="container">
            <div class="line"></div>
            <div class="row">

                <div class="col s12">
                    <p class="x14 light">{{ helper.trans('app.label.copy') }} {{ config.app.name }} {{ config.app.version }}</p>
                </div>

            </div>
        </div>
    </section>

</footer>
