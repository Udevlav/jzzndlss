<meta name="content-language" content="{{ config.app.lang }}">
<meta name="robots" content="all">
<meta name="revisit-after" content="1 day">
<meta name="abstract" content="{{ helper.trans(helper.superCategory(entity.category)) }}">
<meta name="title" content="{{ helper.seoTitle(entity, config) }}">
<meta name="score" content="{{ entity.score }}">
<meta name="description" content="{{ entity.title }}. {{ helper.trans('app.cta.head', config.app.name) }}">
<meta name="copyright" content="{{ config.app.name }}">
<meta name="geo.country" content="{{ config.app.region|upper }}">
<meta name="geo.regioncode" content="ES-{{ entity.postalCode }}">
<meta name="geo.placename" content="{{ entity.city }}">
<meta name="geo.position" content="{{ entity.lat }};{{ entity.lng }}">

<link rel="canonical" href="{{ config.baseUrl() }}{{ entity.hash }}">
<link rel="alternate" hreflang="es" href="{{ config.baseUrl() }}{{ entity.slug }}">
