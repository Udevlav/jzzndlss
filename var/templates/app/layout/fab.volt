<div class="fixed-action-btn fab-detail">
    <a role="button" class="btn-floating btn-large red call-btn" title="{{ helper.trans('ad.label.call', entity.name) }}" aria-label="{{ helper.trans('ad.label.call', entity.name) }}">
        <i class="large material-icons">phone</i>
    </a>
    <ul>
        <li>
            <a role="button" class="btn-floating red totop" title="{{ helper.trans('app.link.totop') }}" aria-label="{{ helper.trans('app.link.totop') }}">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        </li>
        <li id="chat-peer-connect" class="dn">
            <a role="button" class="btn-floating red chat-peer-connect" title="{{ helper.trans('ad.label.chat_with', entity.name) }}" aria-label="{{ helper.trans('ad.label.chat_with', entity.name) }}">
                <i class="material-icons">insert_comment</i>
            </a>
        </li>
        <li>
            <a role="button" class="btn-floating blue new-experience" title="{{ helper.trans('ad.link.add_experience') }}" aria-label="{{ helper.trans('ad.link.add_experience') }}">
                <i class="material-icons">star_half</i>
            </a>
        </li>
        <li>
            <a role="button" class="btn-floating green" href="tel:{{ entity.phone }}" title="{{ helper.trans('ad.label.call', entity.name) }}" aria-label="{{ helper.trans('ad.label.call', entity.name) }}">
                <i class="material-icons">phone</i>
            </a>
        </li>
    </ul>
</div>
