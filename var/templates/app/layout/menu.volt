<div class="overlay top-down overlay-menu" id="menu">
    <div class="container">
        <div class="row">
            <div class="col s8 l6 offset-s2 offset-l3">
                <ul class="menu">
                    <li>
                        <a class="db transitioned ellipsed" href="{{ helper.link('@index') }}" title="{{ helper.trans('app.label.index') }}">
                            {{ helper.trans('app.link.index') }}
                        </a>
                    </li>
                    <li role="separator" class="mt15"></li>
                    <li>
                        <a class="db transitioned ellipsed" href="{{ helper.link('@publish') }}" title="{{ helper.trans('app.label.publish') }}">
                            {{ helper.trans('app.link.publish') }}
                        </a>
                    </li>
                    <li role="separator" class="mt15"></li>
                    <li>
                        <a class="db transitioned ellipsed" href="{{ helper.link('@contact') }}" title="{{ helper.trans('app.label.contact') }}">
                            {{ helper.trans('app.link.contact') }}
                        </a>
                    </li>
                    <li>
                        <a class="db transitioned ellipsed" href="{{ helper.link('@whoweare') }}" title="{{ helper.trans('app.label.whoweare') }}">
                            {{ helper.trans('app.link.whoweare') }}
                        </a>
                    </li>
                    <li>
                        <a class="db transitioned ellipsed" href="{{ helper.link('@tos') }}" title="{{ helper.trans('app.label.tos') }}">
                            {{ helper.trans('app.link.tos') }}
                        </a>
                    </li>
                    <li>
                        <a class="db transitioned ellipsed" href="{{ helper.link('@privacy') }}" title="{{ helper.trans('app.label.privacy') }}">
                            {{ helper.trans('app.link.privacy') }}
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>
