<a class="menu-action menu-right menu-publish menu-action-btn" href="{{ helper.link('@publish') }}" role="button" aria-label="{{ helper.trans('app.label.publish') }}" title="{{ helper.trans('app.label.publish') }}">
    <i class="material-icons">edit</i>
    <span class="badge green{# hide-on-small-and-down #}" data-badge-caption="{{ helper.trans('app.label.publish') }}"></span>
</a>
