{% if t is defined %}
    <div class="throwable">
        <h3 class="x16 lh22 dark bold">{{ t.getMessage() }} ({{ t.getCode() }})</h3>
        <p class="x14 lh20 mb10 grey">in {{ t.getFile() }} at {{ t.getLine() }}</p>
        <pre class="trace x12">
{{ t.getTraceAsString() }}
        </pre>
    </div>

    <div class="caused-by mb50">
        {% if t.getPrevious() %}
            <p class="x12 lh20 mb10 light">Caused by</p>
            {{ partial('layout/throwable', ['t': t.getPrevious() ]) }}
        {% endif %}
    </div>
{% endif %}
