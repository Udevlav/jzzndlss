<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato&amp;subset=latin,latin-ext">

{% if config.app.env == 'dev' %}
    <!-- App assets (development): packed CSS and JS -->
    <link rel="stylesheet" type="text/css" href="{{ config.asset('css/materialize.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ config.asset('css/app.css') }}" />

{% else %}
    <!-- App assets -->
    <link rel="stylesheet" type="text/css" href="{{ config.asset('css/app.min.css') }}" />

{% endif %}
