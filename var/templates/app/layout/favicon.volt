{# See http://realfavicongenerator.net/ #}

{# Chrome, Firefox OS, Opera and Vivaldi #}
<meta name="theme-color" content="#42c4dd">

{# Windows Phone #}
<meta name="msapplication-navbutton-color" content="#42c4dd">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="{{ config.asset('favicon/mstile-150x150.png') }}">

{# iOS Safari #}
<meta name="apple-mobile-web-app-status-bar-style" content="#42c4dd">

{# iOS #}
<link rel="apple-touch-icon" sizes="57x57"   href="{{ config.asset('favicon/apple-touch-icon-57x57-precomposed.png') }}">
<link rel="apple-touch-icon" sizes="60x60"   href="{{ config.asset('favicon/apple-touch-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72"   href="{{ config.asset('favicon/apple-touch-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76"   href="{{ config.asset('favicon/apple-touch-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ config.asset('favicon/apple-touch-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ config.asset('favicon/apple-touch-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ config.asset('favicon/apple-touch-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ config.asset('favicon/apple-touch-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ config.asset('favicon/apple-touch-icon-180x180.png') }}">

{# Standard #}
<link rel="icon" type="image/png" sizes="16x16" href="{{ config.asset('favicon/favicon-16x16.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ config.asset('favicon/favicon-32x32.png') }}">

<link rel="mask-icon" href="{{ config.asset('favicon/safari-pinned-tab.svg') }}" color="#5bbad5">