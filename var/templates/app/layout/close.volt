<a role="button" class="overlay-close" aria-label="{{ helper.trans('app.label.close') }}" title="{{ helper.trans('app.label.close') }}">
    <i class="material-icons">close</i>
</a>