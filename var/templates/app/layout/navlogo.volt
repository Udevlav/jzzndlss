<a href="{{ helper.link('@index') }}" class="db logo menu-logo">
    <img src="/asset/img/wallaicon.png" title="{{ config.app.name }}" alt="{{ config.app.name }} Logo" />
    <span class="lemma dib lred upper x18 bold">{{ config.app.name }}</span>
</a>
