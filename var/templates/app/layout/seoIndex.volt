<meta name="content-language" content="{{ config.app.lang }}">
<meta name="robots" content="all">
<meta name="revisit-after" content="1 day">
{% if filters.getData().province %}
    <meta name="abstract" content="{{ helper.trans('app.meta.abstractWithZone', filters.getData().province) }}">
{% else %}
    <meta name="abstract" content="{{ helper.trans('app.meta.abstract') }}">
{% endif %}
<meta name="title" content="{{ helper.trans('app.meta.title', config.app.name) }}">
<meta name="description" content="{{ helper.trans('app.meta.description', config.app.name) }}">
<meta name="copyright" content="{{ config.app.name }}">
<meta name="geo.country" content="{{ config.app.region|upper }}">

{% if filters is defined %}
    {% set filterData = filters.getData() %}

    {% if filterData.postalCode %}
        <meta name="geo.regioncode" content="ES-{{ filterData.postalCode }}">
    {% endif %}
    {% if filterData.city %}
        <meta name="geo.placename" content="{{ filterData.city}}">
    {% endif %}
{% endif %}

<link rel="canonical" href="{{ config.baseUrl() }}/">
<link rel="alternate" hreflang="es" href="{{ config.baseUrl() }}/?hl=es">