<div class="disclaimer legal">
    <div class="container">
        <div class="row">
            <div class="col s12 m8 offset-m2 rel mt25">
                <p class="x14 grey">{{ helper.trans('legal.label.province') }}</p>
                <p class="x14 grey">{{ helper.trans('legal.label.adult_content') }}</p>
                <p class="x14 grey">{{ helper.trans('app.link.legal_age') }}</p>
            </div>
            <div class="col s12 m8 offset-m2 rel mt15">
                <p class="x12 lgrey">{{ helper.trans('legal.text.cookies') }}</p>
            </div>
        </div>
    </div>
</div>