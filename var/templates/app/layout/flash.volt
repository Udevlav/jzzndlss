{% if flash.hasMessages() %}
    <div class="flash mt50">
        <div class="container">
            <div class="row">
                <div class="col s12 mt25">
                    {{ flash.output() }}
                </div>
            </div>
        </div>
    </div>
{% endif %}