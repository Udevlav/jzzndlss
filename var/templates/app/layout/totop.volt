<div class="fixed-action-btn">
    <a href="#" id="totop" role="button" aria-hidden="true" aria-label="{{ helper.trans('app.link.totop') }}" title="{{ helper.trans('app.link.totop') }}" class="btn-floating btn-large red dn">
        <i class="material-icons">keyboard_arrow_up</i>
    </a>
</div>