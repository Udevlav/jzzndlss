{% extends 'layout.volt' %}

{% block title %}{{ entity.title }} | {{ config.app.name }}{% endblock %}
{% block bodyClass %}page-detail has-captcha{% endblock %}

{% block seo %}
    {{ partial('layout/seo', ['config': config, 'entity': entity]) }}
{% endblock %}

{% block fab %}
    {{ partial('layout/fab', ['entity': entity]) }}
{% endblock %}

{% block links %}
    {{ partial('theme/item/links', ['entity': entity, 'images': images, 'videos': videos]) }}
{% endblock %}

{% block content %}
    <section class="jumbo portrait" id="ad-poster">
        {{ partial('theme/item/portrait', ['entity': entity, 'images': images, 'size': 'md']) }}
    </section>

    <div class="container">
        <div class="row">
            <div class="col s12 l8">
                <article>

                    <section id="ad-head">
                        {{ partial('theme/item/head', ['entity': entity]) }}
                    </section>

                    <section id="ad-text">
                        {{ partial('theme/item/text', ['entity': entity]) }}
                    </section>

                    <section id="ad-images" class="summarizable">
                        {{ partial('theme/item/images', ['entity': entity, 'images': images]) }}
                    </section>

                    {% if entity.numVideos %}
                        <section id="ad-videos" class="summarizable">
                            {{ partial('theme/item/videos', ['entity': entity, 'videos': videos]) }}
                        </section>
                    {% endif %}

                    {% if tags1|length %}
                        <section id="ad-tags-1" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags1, 'label': 'gid.label.gid1']) }}
                        </section>
                    {% endif %}

                    {% if tags2|length %}
                        <section id="ad-tags-2" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags2, 'label': 'gid.label.gid2']) }}
                        </section>
                    {% endif %}

                    {% if tags3|length %}
                        <section id="ad-tags-3" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags3, 'label': 'gid.label.gid3']) }}
                        </section>
                    {% endif %}

                    {% if tags4|length %}
                        <section id="ad-tags-4" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags4, 'label': 'gid.label.gid4']) }}
                        </section>
                    {% endif %}

                    {% if tags5|length %}
                        <section id="ad-tags-5" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags5, 'label': 'gid.label.gid5']) }}
                        </section>
                    {% endif %}

                    <section id="ad-location" class="summarizable">
                        {{ partial('theme/item/map', ['entity': entity, 'apiKey': config.map.apiKey]) }}
                    </section>

                    <section id="ad-fees-availability" class="sm-only">
                        <div class="row mt25">
                            {{ partial('theme/item/fees', ['entity': entity]) }}
                            {{ partial('theme/item/availability', ['entity': entity]) }}
                        </div>
                        <div class="line"></div>
                    </section>

                    <section id="ad-experiences" class="summarizable">
                        {{ partial('theme/item/experiences', ['entity': entity, 'experiences': experiences]) }}
                    </section>

                </article>
            </div>
            <div class="col s12 l4">
                <aside class="aside sm-hidden">
                    {{ partial('theme/item/sidebar', ['entity': entity]) }}
                </aside>
            </div>
        </div>
    </div>

    <div id="ad-owner">
        {{ partial('theme/item/owner', ['entity': entity]) }}
    </div>

    <div id="ad-related">
        {{ partial('theme/item/related', ['related': related]) }}
    </div>

    <div id="ad-disclaimer">
        {{ partial('theme/item/disclaimer', ['entity': entity]) }}
    </div>

    <div id="ad-experience-panel" class="overlay right-to-left">
        {% include 'theme/item/experience.volt' %}
    </div>

    <div id="chat-advertiser-connect-panel" class="overlay right-to-left">
        {% include 'theme/item/advertiser.volt' %}
    </div>

    <div id="chat-peer-connect-panel" class="overlay right-to-left">
        {% include 'theme/item/peer.volt' %}
    </div>

    <div id="chat-sessions" class="overlay right-to-left">
        {% include 'theme/item/sessions.volt' %}
    </div>

    <div id="chat-session" class="overlay right-to-left">
        {% include 'theme/item/session.volt' %}
    </div>

    <aside class="fix sm-only">
        {{ partial('theme/item/fixed', ['entity': entity]) }}
    </aside>

{% endblock %}
