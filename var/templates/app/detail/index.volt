{% extends 'layout.volt' %}

{% block title %}{{ helper.seoTitle(entity, config) }}{% endblock %}
{% block bodyClass %}page-detail has-captcha layout-{{ entity.layout | lower }}{% endblock %}

{% block seo %}
    {{ partial('layout/seo', ['config': config, 'entity': entity]) }}
{% endblock %}

{% block fab %}
    {{ partial('layout/fab', ['entity': entity]) }}
{% endblock %}

{% block links %}
    {{ partial('theme/item/links', ['entity': entity, 'images': images, 'videos': videos]) }}
{% endblock %}

{% block content %}
    {% if entity.layout === 'LANDSCAPE' or entity.layout === 'PORTRAIT' %}
        <section class="jumbo" id="ad-poster">
            {{ partial('theme/item/hero', ['entity': entity, 'images': images, 'size': 'md']) }}
        </section>
    {% endif %}

    <div class="container">
        <div class="row">
            <div class="col s12 l8">
                <article data-rel="{{ entity.id }}">

                    {% if entity.layout === 'PORTRAIT_LORES' %}
                        <section id="ad-poster">
                            {{ partial('theme/item/hero', ['entity': entity, 'images': images, 'size': 'md']) }}
                        </section>
                    {% endif %}

                    <section id="ad-head">
                        {{ partial('theme/item/head', ['entity': entity]) }}
                    </section>

                    <section id="ad-text" class="hide-on-small-and-down">
                        {{ partial('theme/item/text', ['entity': entity]) }}
                    </section>

                    <section id="ad-images" class="summarizable">
                        {{ partial('theme/item/images', ['entity': entity, 'images': images]) }}
                    </section>

                    {% if entity.numVideos %}
                        <section id="ad-videos" class="summarizable">
                            {{ partial('theme/item/videos', ['entity': entity, 'videos': videos]) }}
                        </section>
                    {% endif %}

                    <section id="ad-text" class="hide-on-med-and-up">
                        {{ partial('theme/item/text', ['entity': entity]) }}
                    </section>

                    {% if tags1|length %}
                        <section id="ad-tags-1" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags1, 'label': 'gid.label.gid1']) }}
                        </section>
                    {% endif %}

                    {% if tags2|length %}
                        <section id="ad-tags-2" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags2, 'label': 'gid.label.gid2']) }}
                        </section>
                    {% endif %}

                    {% if tags3|length %}
                        <section id="ad-tags-3" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags3, 'label': 'gid.label.gid3']) }}
                        </section>
                    {% endif %}

                    {% if tags4|length %}
                        <section id="ad-tags-4" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags4, 'label': 'gid.label.gid4']) }}
                        </section>
                    {% endif %}

                    {% if tags5|length %}
                        <section id="ad-tags-5" class="summarizable">
                            {{ partial('theme/item/tags', ['entity': entity, 'tags': tags5, 'label': 'gid.label.gid5']) }}
                        </section>
                    {% endif %}

                    <section id="ad-location" class="summarizable">
                        {{ partial('theme/item/map', ['entity': entity, 'apiKey': config.map.apiKey]) }}
                    </section>

                    <section id="ad-fees-availability" class="sm-only">
                        <div class="row mt25">
                            {{ partial('theme/item/fees', ['entity': entity]) }}
                            {{ partial('theme/item/availability', ['entity': entity]) }}
                        </div>
                        <div class="line"></div>
                    </section>


                    <section id="ad-experiences" class="summarizable">
                        {{ partial('theme/item/experiences', ['entity': entity, 'experiences': experiences]) }}
                    </section>

                </article>
            </div>
            <div class="col s12 l4">
                <aside class="aside sm-hidden">
                    {{ partial('theme/item/sidebar', ['entity': entity]) }}
                </aside>
            </div>
        </div>
    </div>

    <div id="ad-owner">
        {{ partial('theme/item/stats', ['entity': entity, 'token': token]) }}
        {{ partial('theme/item/owner', ['entity': entity, 'token': token]) }}
    </div>

    <div id="ad-related">
        {{ partial('theme/item/related', ['entity': entity, 'related': related]) }}
    </div>

    <div id="ad-disclaimer">
        {{ partial('theme/item/disclaimer', ['entity': entity]) }}
    </div>

    <div id="ad-experience-panel" class="overlay right-to-left">
        {% include 'theme/item/experience.volt' %}
    </div>

    <div id="chat-advertiser-connect-panel" class="overlay right-to-left">
        {% include 'theme/item/advertiser.volt' %}
    </div>

    <div id="chat-peer-connect-panel" class="overlay right-to-left">
        {% include 'theme/item/peer.volt' %}
    </div>

    <div id="chat-sessions" class="overlay right-to-left">
        {% include 'theme/item/sessions.volt' %}
    </div>

    <div id="chat-session" class="overlay right-to-left">
        {% include 'theme/item/session.volt' %}
    </div>

    <div id="free-promotion-panel" class="overlay right-to-left">
        {% include 'theme/item/promotion.volt' %}
    </div>

    <aside class="fix sm-only">
        {{ partial('theme/item/fixed', ['entity': entity]) }}
    </aside>

    {{ partial('theme/item/cursor') }}

{% endblock %}
