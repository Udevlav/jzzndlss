{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.contact_request') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.contact_request') }}{% endblock %}
{% block content %}
    <p>Copia de la solicitud de contacto recibida:</p>
    <p>Email de contacto: {{ entity.email }}</p>
    <pre>{{ entity.text }}</pre>
{% endblock %}
