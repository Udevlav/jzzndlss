{% extends '../mail/mail.volt' %}

{% block subject %}Foobar{% endblock %}
{% block abstract %}Foobar{% endblock %}
{% block content %}
    <p>Foo</p>

    {{ partial('../mail/cta', ['label': 'foobar', 'href': helper.abslink('@index') ]) }}

    <p>Bar</p>

    {{ partial('../mail/cta', ['label': 'bazqux', 'href': helper.abslink('@index') ]) }}

    <p>Baz</p>
{% endblock %}
