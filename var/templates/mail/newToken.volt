{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.token_generated') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.token_generated') }}{% endblock %}
{% block content %}
    <p>{{ helper.trans('mail.label.salutation') }}, {{ advertiser.name }}</p>
    <p>{{ helper.trans('mail.label.token_generated') }}</p>

    {{ partial('../mail/cta', ['label': 'ad.link.manage', 'href': helper.abslink('@manage', advertiser.token) ]) }}

    <p>{{ helper.trans('mail.label.farewell') }}</p>
{% endblock %}
