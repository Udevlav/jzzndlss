{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.ad_created') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.ad_created') }}{% endblock %}
{% block content %}
    <p>{{ helper.trans('mail.label.salutation') }}, {{ advertiser.name }}</p>
    <p>{{ helper.trans('mail.label.ad_created') }}</p>

    {{ partial('../mail/cta', ['label': 'ad.link.confirm_publish', 'href': helper.abslink('@confirm_publication', advertiser.token, entity.slug) ]) }}

    <p>{{ helper.trans('mail.label.ad_created_disclaimer') }}</p>
    <p>{{ helper.trans('mail.label.ad_created_links') }}</p>

    {{ partial('../mail/cta', ['label': 'ad.link.manage', 'href': helper.abslink('@manage', advertiser.token) ]) }}

    <p>{{ helper.trans('mail.label.farewell') }}</p>
{% endblock %}
