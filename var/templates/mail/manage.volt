{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.token_generated') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.token_generated') }}{% endblock %}
{% block content %}
    <p>{{ helper.trans('mail.label.salutation') }}, {{ entity.name }}</p>
    <p>{{ helper.trans('mail.label.manage_links') }}</p>

    {{ partial('../mail/cta', ['label': 'ad.link.manage', 'href': helper.abslink('@manage', entity.token) ]) }}

    <p>{{ helper.trans('mail.label.farewell') }}</p>
{% endblock %}
