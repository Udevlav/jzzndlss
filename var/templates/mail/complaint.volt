{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.complaint_request') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.complaint_request') }}{% endblock %}
{% block content %}
    <p>{{ helper.trans('mail.label.salutation') }},</p>
    <p>{{ helper.trans('mail.label.complaint_request') }}</p>
    <p>{{ helper.trans('mail.label.farewell') }}</p>
{% endblock %}
