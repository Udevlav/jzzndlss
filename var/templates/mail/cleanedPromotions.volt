{% extends '../mail/mail.volt' %}

{% block subject %}Clean report{% endblock %}
{% block abstract %}Clean report{% endblock %}
{% block content %}
    <h1>{{ total }} Promotions cleaned</h1>
    <p class="ac">
        <strong>Clean Report</strong>
    </p>
{% if errors > 0 %}
    <p class="ac">{{ errors }} records could not be removed, and will be retried on next execution. TO manually remove this IDs, please run:</p>
    <code>
        <pre>
            DELETE FROM `jz_promotion` WHERE `id` IN ({{ failedIds | join(', ') }})
        </pre>
    </code>
{% endif %}
    <p class="ac">
        <small>
            This message was automatically generated at {{ helper.date('c') }}.
            Runner <code>gc:promotions</code> started at {{ startedAt }} and ended at {{ endedAt }}.
            Total running time was {{ millitime }} ms, memory usage was {{ diffMemory }}, peak memory usage was {{ peakMemory }}). Available disk space is {{ diskSpace }} ({{ relSpace }}% of total space).
        </small>
    </p>
{% endblock %}
