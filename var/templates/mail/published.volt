{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.ad_published') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.ad_published') }}{% endblock %}
{% block content %}
    <p>{{ helper.trans('mail.label.salutation') }}, {{ entity.name }}</p>
    <p>{{ helper.trans('mail.label.ad_published') }}</p>
    <p>{{ helper.trans('mail.label.ad_created_links') }}</p>

    {{ partial('../mail/cta', ['label': 'ad.link.manage', 'href': helper.abslink('@manage', advertiser.token) ]) }}

    <p>{{ helper.trans('mail.label.farewell') }}</p>
{% endblock %}
