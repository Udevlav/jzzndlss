{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.trans('mail.subject.contact_request') }}{% endblock %}
{% block abstract %}{{ helper.trans('mail.abstract.contact_request') }}{% endblock %}
{% block content %}
    <p>{{ helper.trans('mail.label.salutation') }},</p>
    <p>{{ helper.trans('mail.label.contact_request') }}</p>
    <p>{{ helper.trans('mail.label.farewell') }}</p>
{% endblock %}
