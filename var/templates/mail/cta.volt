<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary ac">
    <tbody>
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="ac">
                            <a role="button" href="{{ href }}" target="_blank">
                                {{ helper.trans(label) }}
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>