{% extends '../mail/mail.volt' %}

{% block subject %}{{ helper.ucfirst(scrapper.getId()) }} Scrapping Session Report{% endblock %}
{% block abstract %}{{ helper.ucfirst(scrapper.getId()) }} Scrapping Session Report{% endblock %}
{% block content %}
    <p>Session Details</p>

    <ul>
        <li>Provider: <strong>{{ scrapper.getId() }}</strong></li>
        <li>Start time: <strong>{{ stats.getStartTimeHumanReadable() }}</strong></li>
        <li>Total running time: <strong>{{ stats.getTimeToCompleteMins() }} min</strong></li>
    </ul>

    <p>Session Stats</p>

    <ul>
        <li>Processed: <strong>{{ stats.getTotal() }}</strong></li>
        <li>Skipped: <strong>{{ stats.getSkipped() }}/{{ stats.getTotal() }} ({{ stats.getSkippedPct() }} %)</strong></li>
        <li>Scrapped: <strong>{{ stats.getScrapped() }}/{{ stats.getTotal() }} ({{ stats.getScrappedPct() }} %)</strong></li>
        <li>Persisted: <strong>{{ stats.getPersisted() }}/{{ stats.getScrapped() }} ({{ stats.getPersistedPct() }} %)</strong></li>
        <li>Published: <strong>{{ stats.getPublished() }}/{{ stats.getPersisted() }} ({{ stats.getPublishedPct() }} %)</strong></li>
    </ul>

    <p>Session Errors</p>

    <ul>
        <li>Persist Failed: <strong>{{ stats.getPersistFailed() }}/{{ stats.getScrapped() }} ({{ stats.getPersistFailedPct() }} %)</strong></li>
        <li>Publish Failed: <strong>{{ stats.getPublicationFailed() }}/{{ stats.getPersisted() }} ({{ stats.getPublicationFailedPct() }} %)</strong></li>
    </ul>

    <p>Session Logs</p>

    <ul>
        <li>A copy of the full session log is stored in <code>/tmp/.scraplog</code></li>
        <li>A copy of each ad publication log is stored in <code>/tmp/.scraplog.{{ scrapper.getId() }}.{id}</code>, where <code>{id}</code> is the ad identifier.</li>
    </ul>

    {{ partial('../mail/cta', ['label': 'Manage scrapped ads', 'href': helper.abslink('@manage_scrapped') ~ '?provider=' ~ scrapper.getId() ]) }}
{% endblock %}
