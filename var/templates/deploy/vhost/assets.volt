# ----------------------------------------------------------------------
# {{ config.app.name }} virtual host
# ----------------------------------------------------------------------

events {
}

http {
    # Default set of files and their content types
    include      mime.types;

    # Prompt user for download for any undeclared file format
    default_type application/octet-stream;

    # Optimization when serving static files
    sendfile            on;
    sendfile_max_chunk  1m;

    server {
        # No need for root privileges
        listen      8080;
        server_name {{ config.cdn.authority }}

        # Logging
        error_log  {{ config.layout.logDir }}/cdn-error.log;
        access_log {{ config.layout.logDir }}/cdn-access.log;

        # Enable compression
        gzip on;
        gzip_types application/javascript text/css image/* audio/* video/*;
        gunzip on;

        location / {
            autoindex off;

            # Assets path
            root {{ config.layout.mediaDir }}
        }

        # Asset Caching
        location ~* \.(css|js)$ {
            expires 7d;
        }

        # Media Caching
        location ~* \.(jpg|jpeg|png|gif|ico)$ {
            expires 30d;
        }
    }
}

# ----------------------------------------------------------------------
# end-of-virtualhost