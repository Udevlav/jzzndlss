# ----------------------------------------------------------------------
# {{ config.app.name }} hosts
# ----------------------------------------------------------------------
#
{% if config.cdn.enabled %}
127.0.0.1 {{ config.cdn.authority }}

{% endif %}
127.0.0.1 {{ config.server.documentation }}

127.0.0.1 {{ config.server.authority }}

{% for alias in config.server.aliases %}
127.0.0.1 {{ alias }}

{% endfor %}
# ----------------------------------------------------------------------
end-of-hosts