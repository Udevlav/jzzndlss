# ----------------------------------------------------------------------
# {{ config.app.name }} virtual host
# ----------------------------------------------------------------------

Define APP_PATH {{ config.layout.baseDir }}

Define APP_NAME "{{ config.app.name }}"

Define APP_ENV {{ config.app.env }}


<IfModule mod_mime.c>
    AddType application/vnd.ms-fontobject    .eot
    AddType application/x-font-opentype      .otf
    AddType image/svg+xml                    .svg
    AddType application/x-font-ttf           .ttf
    AddType application/font-woff            .woff
    AddType application/font-woff2           .woff2
</IfModule>

<IfModule mod_expires.c>
    ExpiresActive on
    ExpiresByType image/jpg "access plus 60 days"
    ExpiresByType image/png "access plus 60 days"
    ExpiresByType image/gif "access plus 60 days"
    ExpiresByType image/jpeg "access plus 60 days"
    ExpiresByType text/css "access plus 1 days"
    ExpiresByType image/x-icon "access plus 1 month"
    ExpiresByType application/pdf "access plus 1 month"
    ExpiresByType audio/x-wav "access plus 1 month"
    ExpiresByType audio/mpeg "access plus 1 month"
    ExpiresByType video/mpeg "access plus 1 month"
    ExpiresByType video/mp4 "access plus 1 month"
    ExpiresByType video/quicktime "access plus 1 month"
    ExpiresByType video/x-ms-wmv "access plus 1 month"
    ExpiresByType application/x-shockwave-flash "access 1 month"
    ExpiresByType text/javascript "access plus 1 week"
    ExpiresByType application/x-javascript "access plus 1 week"
    ExpiresByType application/javascript "access plus 1 week"
</IfModule>

{% if https or config.server.protocol == 'https' %}
#
# WWW over HTTPS
#
<IfModule mod_ssl.c>
    <VirtualHost *:443>
        DocumentRoot {{ config.layout.publicDir }}


        ServerName {{ config.server.authority }}

    {% for alias in config.server.aliases %}
        ServerAlias {{ alias }}

    {% endfor %}

        ServerAdmin {{ config.server.admin }}

        ServerSignature Off

        SSLEngine on
        SSLCertificateFile {{ config.layout.certDir }}{{ config.server.sslCert }}

        SSLCertificateKeyFile {{ config.layout.certDir }}{{ config.server.sslKey }}


        # Some extras, copied from Apache's default SSL conf virtualhost
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
        SSLOptions +StdEnvVars
        </FilesMatch>

        BrowserMatch "MSIE [2-6]" \
            nokeepalive ssl-unclean-shutdown \
            downgrade-1.0 force-response-1.0

        # MSIE 7 and newer should be able to use keepalive
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

        ErrorLog {{ config.layout.logDir }}https-error.log
        CustomLog {{ config.layout.logDir }}https-access.log common

        DirectoryIndex index.php

        <Directory {{ config.layout.publicDir }}>
            AllowOverride All
            Require all granted

            <IfModule mod_rewrite.c>
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ /index.php [QSA,L]
            </IfModule>
        </Directory>
    </VirtualHost>
</IfModule>
{% endif %}

#
# WWW over HTTP
#
<VirtualHost *:80>
    DocumentRoot {{ config.layout.publicDir }}


    ServerName {{ config.server.authority }}

{% for alias in config.server.aliases %}
    ServerAlias {{ alias }}

{% endfor %}

    ServerAdmin {{ config.server.admin }}

    ServerSignature Off

    ErrorLog {{ config.layout.logDir }}http-error.log
    CustomLog {{ config.layout.logDir }}http-access.log common

    KeepAlive            On
    MaxKeepAliveRequests 200
    KeepAliveTimeout     5

    DirectoryIndex index.php

    <Directory {{ config.layout.publicDir }}>
        AllowOverride All
        Require all granted

        <IfModule mod_rewrite.c>
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ /index.php [QSA,L]
        </IfModule>
    </Directory>
</VirtualHost>

#
# Documentation over HTTP
#
<VirtualHost *:80>
    DocumentRoot {{ config.layout.docDir }}build/html


    ServerName {{ config.server.documentation }}

    ServerAdmin {{ config.server.admin }}

    ServerSignature Off

    ErrorLog {{ config.layout.logDir }}doc-error.log
    CustomLog {{ config.layout.logDir }}doc-access.log common

    KeepAlive            On
    MaxKeepAliveRequests 200
    KeepAliveTimeout     5

    DirectoryIndex index.html

    <Directory {{ config.layout.docDir }}build/html>
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

# ----------------------------------------------------------------------
# end-of-virtualhost