<?xml version="1.0" encoding="UTF-8"?>

{%- if sitemap.isIndex() -%}

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {%- for sitemap in sitemap.getSitemapIndex().getIterator() -%}

        <sitemap>
            <loc>{{ config.baseUrl() }}{{ sitemap.getLoc() }}</loc>
            {%- if sitemap.getLastMod() -%}
                <lastmod>{{ sitemap.getLastMod() }}</lastmod>
            {%- endif -%}
        </sitemap>

    {%- endfor -%}
</sitemapindex>

{%- else -%}

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
    {%- for url in sitemap.getUrlSet().getIterator() -%}

        <url>
            <loc>{{ url.getLoc() }}</loc>
            {%- if url.getLastMod() -%}
                <lastmod>{{ url.getLastMod() }}</lastmod>
            {%- endif -%}
            {%- if url.getChangeFreq() -%}
                <changefreq>{{ url.getLoc() }}</changefreq>
            {%- endif -%}
            {%- if url.getPriority() -%}
                <priority>{{ url.getPriority() }}</priority>
            {%- endif -%}
        </url>

    {%- endfor -%}
</urlset>

{%- endif -%}