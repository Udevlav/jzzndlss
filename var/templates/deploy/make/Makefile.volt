SHELL = /bin/bash

DB_HOST={{ config.db.host }}

DB_PORT={{ config.db.port }}

DB_NAME={{ config.db.dbname }}

DB_USER={{ config.db.username }}

DB_PASS={{ config.db.password }}

export

.PHONY: help
help: banner
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "app targets ----------------------------------------------------------------"
	@echo ""
	@echo "  assets      Generates and install the app assets (JS, CSS, fonts, etc.)"
	@echo "  cache       Clears the app cache"
	@echo "  clean       Cleans logs and temporary upload resources"
	@echo "  run         Runs a command using the app CLI (bin/cli)"
	@echo ""
	@echo "log targets ----------------------------------------------------------------"
	@echo ""
	@echo "  accesslog   Shows the last lines of the server access log"
	@echo "  applog      Shows the last lines of the app log"
	@echo "  dblog       Shows the last lines of the DB log"
	@echo "  errorlog    Shows the last lines of the server error log"
	@echo "  maillog     Shows the last lines of the mail log"
	@echo "  smslog      Shows the last lines of the SMS log"
	@echo ""
	@echo ""

.PHONY: banner
banner:
	@echo ""
	@echo ".......................,,::==+++?++I?II7II?++=~::~.............................."
	@echo ".......................,,::~=++???I7II7III+I?~=~~~,,............................"
	@echo "......................,,:~=+??I7777I777I7IIIIII==~~,,,.........................."
	@echo ".....................,,:~~~+?IIIIII7I7777777I?++==::,,,........................."
	@echo "...................,,:~~==?II777777?777I777777IIII+~:,,,........................"
	@echo "..................,,:~~==?II777777I+777?I77I7777I7?=:~:~,......................."
	@echo ".................,,:::~:=====+?+I??+II?==???II=++~:,,,,,........................"
	@echo "................,,.,..........,::~~~+?==~~~::,,................................."
	@echo ".................,.,..............,.,:~,.:,....................................."
	@echo ".................,,:==++:...........:,................:::,......................"
	@echo "..................:~+?+~~::...........:,.................,,....................."
	@echo ".................,,.,:~=?~==........:~?=,..........:??++:,......................"
	@echo ".......................,....:,......=?7?=,.......,:...:........................."
	@echo "..........................,...,....,=?7I=,.....,~..,............................"
	@echo "...................~:.....=+:...+~:=?I77+:,,,?~...I?:..........................."
	@echo "....................:,..,=?~=~==?=:+I777+:,:~?++?~=++~,,,......................."
	@echo "...............::.,....,..,,,:~?==:+I77I=:,:~+??.,~:.,,,........................"
	@echo "...............,~===~=:~==+?II?+=~~=I77I=:::~=:=???=+=~.,:,:::.................."
	@echo ".............,,,::=~++??++++I?+=~~=+I77I+=~~:::~I?7?+?+?===~:,.................."
	@echo ".............,:~~~~==?=+=~?I7?=~==+?777I?=~~~:::+I???=++++=~:::,,..............."
	@echo ".............,:~~=+?I+=?II77?+=~:~+I7777?=~::,::=+????++=~=~~~::,..............."
	@echo ".............,::~=++????I777I?,++==?7II?=~::=+.:+?7I??+??+=~~::,,..............."
	@echo ".............,,,,:~~=++??III?==:,,..,::......,~,+?II?I+==~~:,,,,................"
	@echo ".................,:===???I?I=:.................,~+??I+===~:,...................."
	@echo "...................,,~==?+==+...................==+?==~~:,,....................."
	@echo "....................,,~=++:+?...................:+~+~~~,........................"
	@echo "......................,:~~++?,..................?++=::,........................."
	@echo "......................:::=++??II?=:........:=+=++++~::,........................."
	@echo ".....................,,,==++??????I?=::~++++?++=+=~=~:,........................."
	@echo "....................,:,:~===++++++?7I?I77??++?+=+~=~::,,........................"
	@echo "..................,..,,:===~~:=??77I7+:+II?II=~+++~~~~,........................."
	@echo "....................,.:~===+++=~.:,:===.,+~:=?++:~=~~:,,........................"
	@echo ".....................,,=~~~==:..............,.:~=~::,.:,........................"
	@echo ".....................,::~~:,.....................,,:,..........................."
	@echo ".....................,,:,.......,,.............................................."
	@echo ".....,...............,........+=+?I77+~~I77??~I~,..............................."
	@echo ".........................,::,~==+~???II7I~=,,~~:..................,............."
	@echo "......................,...,.........~~:,.,......................................"
	@echo "................................................................................"
	@echo "................................................................................"
	@echo "................................................................................"

.PHONY: cache
cache: banner
	@echo "[{{ config.app.name }}] Clearing cache ..."
	@echo ""
	rm -rf {{ config.layout.cacheDir }}*
	touch {{ config.layout.mediaDir }}.gitkeep
	chmod -R 777 {{ config.layout.cacheDir }}

	mkdir -p {{ config.layout.tplCacheDir }}

	touch {{ config.layout.tplCacheDir }}.gitkeep

	chmod -R 777 {{ config.layout.tplCacheDir }}

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: clean
clean: cache
	@echo "[{{ config.app.name }}] Cleaning logs and temporary uploads (requires sudo) ..."
	@echo ""

	chmod -R 777 {{ config.layout.mediaDir }}

	rm -rf {{ config.layout.mediaDir }}*


	@echo ""
	@echo "[{{ config.app.name }}] Cleaning logs (requires sudo) ..."
	@echo ""

	sudo rm {{ config.layout.logDir }}*.log
	touch {{ config.layout.logDir }}{{ config.server.protocol }}-error.log
	touch {{ config.layout.logDir }}{{ config.server.protocol }}-access.log
	touch {{ config.layout.logDir }}{{ config.logging.logName }}

{% if config.db.log %}
	touch {{ config.layout.logDir }}{{ config.db.logName }}

{% endif %}

{% if config.mailer.log %}
	touch {{ config.layout.logDir }}{{ config.mailer.logName }}

{% endif %}

{% if config.sms.log %}
	touch {{ config.layout.logDir }}{{ config.sms.logName }}

{% endif %}
	sudo chmod -R 777 {{ config.layout.logDir }}

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: run
run: banner
	@echo "[{{ config.app.name }}] Running app CLI ..."
	@echo ""
	php bin/cli ${ARGS}
		
	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: accesslog
accesslog: banner
	@echo "[{{ config.app.name }}] Last entries in access log ..."
	@echo ""
	tail -n 100 {{ config.layout.logDir }}{{ config.server.protocol }}-access.log

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: errorlog
errorlog: banner
	@echo "[{{ config.app.name }}] Last entries in error log ..."
	@echo ""
	tail -n 100 {{ config.layout.logDir }}{{ config.server.protocol }}-error.log
	
	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: applog
applog: banner
	@echo "[{{ config.app.name }}] Last entries in App log ..."
	@echo ""
	tail -n 100 {{ config.layout.logDir }}{{ config.logging.logName }}

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: dblog
dblog: banner
	@echo "[{{ config.app.name }}] Last entries in DB log ..."
	@echo ""
	tail -n 100 {{ config.layout.logDir }}{{ config.db.logName }}

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: maillog
maillog: banner
	@echo "[{{ config.app.name }}] Last entries in Mail log ..."
	@echo ""
	tail -n 100 {{ config.layout.logDir }}{{ config.mailer.logName }}

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: smslog
smslog: banner
	@echo "[{{ config.app.name }}] Last entries in SMS log ..."
	@echo ""
	tail -n 100 {{ config.layout.logDir }}{{ config.sms.logName }}

	@echo ""
	@echo "[{{ config.app.name }}] Done"

.PHONY: assets
assets: cache
	@echo "[{{ config.app.name }}] Generating and publishing assets ..."
	@echo ""
{% if config.app.env == 'dev' %}
	grunt dev -v --base {{ config.layout.varDir }}private --gruntfile {{ config.layout.varDir }}private/Gruntfile.js
{% else %}
	grunt default -v --base {{ config.layout.varDir }}private --gruntfile {{ config.layout.varDir }}private/Gruntfile.js
{% endif %}
	@echo ""
	@echo "[{{ config.app.name }}] Done"
