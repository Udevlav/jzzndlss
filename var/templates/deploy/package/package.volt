{
    "name": "{{ config.app.name }}",
    "description": "{{ config.app.description }}",
    "version": "{{ config.app.version }}",
    "private": true,
    "directories": {
        "bin": "{{ config.app.binDir }}",
        "doc": "{{ config.app.docDir }}"
    },
    "devDependencies": {
        "babel-preset-es2015": "^6.22.0",
        "grunt": "~0.4.5",
        "grunt-babel": "^6.0.0",
        "grunt-contrib-concat": "^1.0.1",
        "grunt-contrib-copy": "^1.0.0",
        "grunt-contrib-cssmin": "^2.0.0",
        "grunt-contrib-htmlmin": "^2.3.0",
        "grunt-contrib-jshint": "^0.10.0",
        "grunt-contrib-less": "^1.4.1",
        "grunt-contrib-nodeunit": "~0.4.1",
        "grunt-contrib-uglify": "^0.5.1",
        "grunt-contrib-watch": "^1.0.0",
        "grunt-es6-transpiler": "^1.0.2",
        "grunt-string-replace": "^1.3.1",
        "jshint-stylish-ex": "^0.2.0"
    }
}