<?php

namespace App\Session;

use Phalcon\Flash\Session;

/**
 * Custom flash implementation.
 */
class Flash extends Session
{

    public function hasMessages(): bool
    {
        foreach (['success', 'notice', 'warning', 'error'] as $type) {
            if ($this->has($type)) {
                return true;
            }
        }

        return false;
    }
}