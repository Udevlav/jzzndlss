<?php

namespace App\Session;

use App\Geo\GeoLocation;

use App\Model\Advertiser;

class User
{

    /**
     * @var Advertiser
     */
    protected $advertiser;

    /**
     * @var GeoLocation
     */
    protected $geolocation;

    /**
     * @return Advertiser | null
     */
    public function getAdvertiser()
    {
        return $this->advertiser;
    }

    /**
     * @param Advertiser $advertiser
     *
     * @return User
     */
    public function setAdvertiser(Advertiser $advertiser): User
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * @return GeoLocation
     */
    public function getGeoLocation(): GeoLocation
    {
        return $this->geolocation;
    }

    /**
     * @param GeoLocation $geolocation
     *
     * @return User
     */
    public function setGeoLocation(GeoLocation $geolocation): User
    {
        $this->geolocation = $geolocation;

        return $this;
    }
}