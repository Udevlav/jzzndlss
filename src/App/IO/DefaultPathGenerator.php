<?php

namespace App\IO;

use App\Model\Ad;
use App\Model\Media;

/**
 * A media path generation strategy which uses a simple naming strategy, based
 * on the ad resource identifier and the ad identifier. Temporary paths are
 * generated according to the base implementation. Public paths for a resource
 * which belongs to an ad are generated using the ad identifier as <basedir>
 * and the resource basename as <basename>.
 */
class DefaultPathGenerator extends BasePathGenerator
{
    /**
     * @inheritdoc
     */
    protected function getTempname(string $path): string
    {
        return $this->replaceExtension(basename($path), 'tmp');
    }

    /**
     * @inheritdoc
     */
    protected function getBasedir(Ad $ad): string
    {
        return $ad->id;
    }

    /**
     * @inheritdoc
     */
    protected function getBasename(Media $media): string
    {
        return $this->replaceExtension(basename($media->path), $this->getExtension($media));
    }

    /**
     * @inheritdoc
     */
    protected function getExtension(Media $media): string
    {
        switch (strtolower($media->type)) {
            case 'image/jpeg':
                return 'jpg';

            case 'image/png':
                return 'png';

            case 'image/gif':
                return 'gif';

            case 'video/mp4':
                return 'mp4';
        }

        if ($media->isImage()) {
            // Default for images
            return 'jpg';
        }

        if ($media->isVideo()) {
            // Default for images
            return 'mp4';
        }

        return 'bin';
    }

    /**
     * @param string $path
     * @param string $ext
     *
     * @return string
     */
    protected function replaceExtension(string $path, string $ext): string
    {
        $extension = explode('.', $path);
        $extension = array_pop($extension);

        $pattern = sprintf('/%s$/', $extension);

        return preg_replace($pattern, $ext, $path);
    }
}