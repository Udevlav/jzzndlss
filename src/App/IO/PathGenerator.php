<?php

namespace App\IO;

use App\Model\Ad;
use App\Model\Media;

/**
 * Abstraction of a media path generation strategy.
 */
interface PathGenerator
{

    /**
     * Generates the public path for the given ad.
     *
     * @param Ad    $ad
     *
     * @return string
     */
    public function generateAdPath(Ad $ad): string;

    /**
     * Generates a temporary path for the given basename.
     *
     * @param string $basename
     *
     * @return string
     */
    public function generateTempPath(string $basename): string;

    /**
     * Generates the public path for the given media.
     *
     * @param Ad    $ad
     * @param Media $media
     *
     * @return string
     */
    public function generatePublicPath(Ad $ad, Media $media): string;

}