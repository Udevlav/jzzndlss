<?php

namespace App\IO;

use App\Config\Config;

use App\Model\Ad;
use App\Model\Media;
use Phalcon\Mvc\User\Component;

/**
 * Base path generator implementation. The default path for a temporay
 * resources with basename <basename> is
 *
 *    <config.layout.uploadDir> <tempname>
 *
 * where <tempname> is the value returned by the getTempname() method,
 * usually with a temporary extension. The default path for a public resource,
 * which belongs to an ad, is
 *
 *    <config.layout.mediaDir> <basedir> <basename>
 *
 * where <basedir> is the value returned by the getBasedir() method, which
 * depends on the ad; and <basename> is the value returned by the getBasename()
 * method, which depends on the resource, and replaces the temporary extension
 * by the extension corresponding to the resource media type.
 *
 * Sub-classes may override this behaviour.
 */
abstract class BasePathGenerator extends Component implements PathGenerator
{

    /**
     * @inheritdoc
     */
    public function generateAdPath(Ad $ad): string
    {
        return sprintf('%s%s',
            $this->getMediaDir(),
            $this->getBaseDir($ad)
        );
    }

    /**
     * @inheritdoc
     */
    public function generateTempPath(string $path): string
    {
        return sprintf('%s%s',
            $this->getUploadDir(),
            $this->getTempname($path)
        );
    }

    /**
     * @inheritdoc
     */
    public function generatePublicPath(Ad $ad, Media $media): string
    {
        return sprintf('%s%s/%s',
            $this->getMediaDir(),
            $this->getBaseDir($ad),
            $this->getBasename($media)
        );
    }


    /**
     * @param string $path
     *
     * @return string
     */
    abstract protected function getTempname(string $path): string;

    /**
     * @param Ad $ad
     *
     * @return string
     */
    abstract protected function getBasedir(Ad $ad): string;

    /**
     * @param Media $media
     *
     * @return string
     */
    abstract protected function getBasename(Media $media): string;

    /**
     * @param Media $media
     *
     * @return string
     */
    abstract protected function getExtension(Media $media): string;


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return string
     */
    protected function getPublicDir(): string
    {
        return $this->getConfig()->layout->publicDir;
    }

    /**
     * @return string
     */
    protected function getMediaDir(): string
    {
        return $this->getConfig()->layout->mediaDir;
    }

    /**
     * @return string
     */
    protected function getUploadDir(): string
    {
        return $this->getConfig()->layout->uploadDir;
    }
}
