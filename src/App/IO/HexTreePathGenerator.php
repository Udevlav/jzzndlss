<?php

namespace App\IO;

use App\Model\Ad;

/**
 * A media path generation strategy which uses a 32-bit unsigned integer
 * representation of the ad identifier, encoded in hexadecimal, as base
 * path <basedir> for public resources. This allows for up to
 *
 *    2 ^ 32 = 0xffffffff = 3.99 Gi = 4.294.967.295 ads
 *
 * to be stored in the <config.layout.mediaDir> directory. The hex number
 * is splitted in segments of 1 byte (each two hex digits), so each sub-
 * directory in the hierarchy will have up to
 *
 *    2 ^ 8 = 256 sub-directories
 *
 * This figures seem reasonably large now.
 */
class HexTreePathGenerator extends DefaultPathGenerator
{
    /**
     * @inheritdoc
     */
    public function getBasedir(Ad $ad): string
    {
        return implode('/', str_split(sprintf('%08x', $ad->id), 2));
    }
}