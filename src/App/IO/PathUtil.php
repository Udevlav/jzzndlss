<?php

namespace App\IO;

final class PathUtil
{

    static public function format(string $path): string
    {
        if (preg_match('/(.*)([0-9a-f]{64})(.*)/', $path, $match)) {
            return $match[1] . substr($match[2], 0, 5) .'...'. substr($match[2], -5) . $match[3];
        }

        return $path;
    }

}