<?php

namespace App\Sitemap;

class UrlSet
{

    private $urls;


    public function __construct()
    {
        $this->urls = [];
    }


    public function getUrls()
    {
        return $this->urls;
    }

    public function addUrl(Url $url): UrlSet
    {
        $this->urls[] = $url;

        return $this;
    }


    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->urls);
    }
}