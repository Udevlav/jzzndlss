<?php

namespace App\Sitemap;

class SitemapBuilder
{

    private $sitemap;
    private $currentSitemap;


    public function __construct(bool $index = false)
    {
        $this->sitemap = new Sitemap($index);
        $this->currentSitemap = $this->sitemap;
    }


    public function addUrl(string $loc, string $lastMod = null, string $changeFreq = null, string $priority = null): SitemapBuilder
    {
        $url = new Url();
        $url
            ->setLoc($loc)
            ->setLastMod($lastMod)
            ->setChangeFreq($changeFreq)
            ->setPriority($priority);

        $this->currentSitemap->getUrlSet()->addUrl($url);

        return $this;
    }

    public function addSitemap(string $loc, string $lastMod = null, bool $index = false): SitemapBuilder
    {
        $sitemap = new Sitemap($index);
        $sitemap
            ->setLoc($loc)
            ->setLastMod($lastMod);

        $this->currentSitemap->addSitemap($sitemap);
        $this->currentSitemap = $sitemap;

        return $this;
    }

    public function endSitemap(): SitemapBuilder
    {
        if ($this->currentSitemap->isRoot()) {
            throw new \LogicException('No current sitemap to end; use build to get the root sitemap.');
        }

        $this->currentSitemap = $this->currentSitemap->getParent();

        return $this;
    }

    public function build(): Sitemap
    {
        if ($this->currentSitemap !== $this->sitemap) {
            throw new \LogicException('You should end the current sitemap before building the root sitemap.');
        }

        return $this->sitemap;
    }
}