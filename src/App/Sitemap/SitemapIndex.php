<?php

namespace App\Sitemap;

class SitemapIndex
{

    private $sitemaps;


    public function __construct()
    {
        $this->sitemaps = [];
    }


    public function getSitemaps()
    {
        return $this->sitemaps;
    }

    public function addSitemap(Sitemap $sitemap): SitemapIndex
    {
        $this->sitemaps[] = $sitemap;

        return $this;
    }


    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->sitemaps);
    }
}