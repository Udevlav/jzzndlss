<?php

namespace App\Sitemap;

class Sitemap
{

    private $loc;
    private $lastMod;
    private $parent;
    private $urlSet;
    private $sitemapIndex;


    public function __construct(bool $index = false)
    {
        if ($index) {
            $this->sitemapIndex = new SitemapIndex();
        } else {
            $this->urlSet = new UrlSet();
        }
    }


    public function getLoc()
    {
        return $this->loc;
    }

    public function getLastMod()
    {
        return $this->lastMod;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getUrlSet()
    {
        return $this->urlSet;
    }

    public function getSitemapIndex()
    {
        return $this->sitemapIndex;
    }


    public function setLoc($loc): Sitemap
    {
        $this->loc = $loc;

        return $this;
    }

    public function setLastMod($lastMod): Sitemap
    {
        $this->lastMod = $lastMod;

        return $this;
    }

    public function setParent(Sitemap $parent = null): Sitemap
    {
        $this->parent = $parent;

        return $this;
    }


    public function isRoot(): bool
    {
        return null === $this->parent;
    }

    public function isPlain(): bool
    {
        return null === $this->sitemapIndex;
    }

    public function isIndex(): bool
    {
        return ! $this->isPlain();
    }


    public function addUrl(Url $url): Sitemap
    {
        if ($this->isIndex()) {
            throw new \LogicException();
        }

        $this->urlSet->addUrl($url);

        return $this;
    }

    public function addSitemap(Sitemap $sitemap): Sitemap
    {
        if ($this->isPlain()) {
            throw new \LogicException();
        }

        $sitemap->setParent($this);

        $this->sitemapIndex->addSitemap($sitemap);

        return $this;
    }
}