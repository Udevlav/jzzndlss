<?php

namespace App\Sitemap;

class Url
{

    private $loc;
    private $lastMod;
    private $changeFreq;
    private $priority;


    public function getLoc()
    {
        return $this->loc;
    }

    public function getLastMod()
    {
        return $this->lastMod;
    }

    public function getChangeFreq()
    {
        return $this->changeFreq;
    }

    public function getPriority()
    {
        return $this->priority;
    }


    public function setLoc($loc): Url
    {
        $this->loc = $loc;

        return $this;
    }

    public function setLastMod($lastMod): Url
    {
        $this->lastMod = $lastMod;

        return $this;
    }

    public function setChangeFreq($freq): Url
    {
        $this->changeFreq = $freq;

        return $this;
    }

    public function setPriority($priority): Url
    {
        $this->priority = $priority;

        return $this;
    }
}