<?php

namespace App\Promotion;

use InvalidArgumentException;

final class Promotions
{

    static public function assertDows(array $dows)
    {
        if (0 === count($dows)) {
            throw new InvalidArgumentException('Empty days of the week.');
        }

        foreach ($dows as $dow) {
            if (! is_numeric($dow) || $dow < 0 || $dow > 6) {
                throw new InvalidArgumentException('Invalid day of the week '. $dow);
            }
        }
    }

    static public function assertTime(string $time)
    {
        if (! preg_match('/^\d{2}:\d{2}$/', $time)) {
            throw new InvalidArgumentException('Invalid time string '. $time);
        }
    }

    static public function assertWhen(string $when)
    {
        if (! in_array($when, ['now', 'auto'])) {
            throw new InvalidArgumentException('Invalid when value '. $when);
        }
    }
}