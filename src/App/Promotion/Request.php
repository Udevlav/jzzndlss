<?php

namespace App\Promotion;

/**
 * This is a Promotion data-transfer object.
 */
class Request
{

    public $when;
    public $startDate;
    public $startTime;
    public $endTime;
    public $fullDay;
    public $productId;
}