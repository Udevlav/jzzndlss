<?php

namespace App\Promotion;

use App\Model\Product;
use App\Model\Promotion;

use Phalcon\Mvc\User\Component;

use DateInterval;
use DateTime;
use stdClass;

use Exception;
use InvalidArgumentException;

/**
 * This class takes care of computing the appropriate promotions
 * for an ad, according to a purchased product.
 */
class Promoter extends Component
{

    /**
     * The minimum allowed udpate period, in seconds.
     *
     * Important note:
     *
     *   No matter what you set in the configuration file; the minimum
     *   update period will never fall below this minimum value.
     *
     * @const int
     */
    const MIN_UPDATE_PERIOD = 120; // 2 minutes

    /**
     * @deprecated to be removed in next release
     *
     * @var float
     */
    private $creditsPerHour;

    /**
     * No matter what product buys the customer, this is the maximum
     * allowed update period.
     *
     * @var int
     */
    private $minUpdatePeriod;

    /**
     * Promoter constructor.
     *
     * @param float $creditsPerHour
     * @param int $minUpdatePeriod
     */
    public function __construct(float $creditsPerHour, int $minUpdatePeriod)
    {
        $this->creditsPerHour = $creditsPerHour;
        $this->minUpdatePeriod = max(self::MIN_UPDATE_PERIOD, $creditsPerHour);
    }

    /**
     * @param $request
     * @param Product|null $product
     * @return array
     *
     * @throws Exception
     */
    public function computePromotions($request, Product $product = null): array
    {
        Promotions::assertWhen($request->when);
        Promotions::assertTime($request->startTime);
        Promotions::assertTime($request->endTime);

        $promotions = [];

        if ($request->when === 'auto') {
            if (null === $product) {
                throw new InvalidArgumentException('Product should not be null');
            }

            // Paid promotion; promote according to the product
            $promotions = $this->computeAuto($request, $product);

        } else if ($request->when === 'now') {
            // Freemium promotion; promote once (duration is 24h)
            $promotion = new Promotion();
            $promotion->productId = $product->id;
            $promotion->validFrom = time();
            $promotion->validTo = $promotion->validFrom + 24 * 3600;
            $promotion->credits = $product ? $product->creditsGranted : 0;

            $promotions[] = $promotion;

        } else {
            throw new InvalidArgumentException('Invalid promotion when');
        }

        return $promotions;
    }


    /**
     * Computes the promotion schedule, according to a product. Basically,
     * the algorithmis is as follows:
     *
     *   1. Assign s <- promotion start date time
     *   2. Assign e <- promotion end date time (adding product.dayspan days to s)
     *   3. Let d = s
     *   4. Let p be an empty list of promotions
     *   5. While (d < e):
     *
     *      5.1. Create a new promotion instance, valid for the date
     *           d, with a start time and an end time as specified
     *           by the customer (in the request)
     *
     *      5.2. Compute and save the timestamps for the precise times
     *           the ad should be promoted, accoring to the prodcut
     *           promotions field (the minimum update time should not
     *           exceed in any case the minimum allowed)
     *
     *      5.3. Add the new promotion instance to the list of promotions
     *
     *      5.4. d = d + 1 day
     *
     *   6. Return promotions
     *
     * @param Request | stdClass $request
     * @param Product            $product
     *
     * @return Promotion[]
     *
     * @throws \Exception
     */
    private function computeAuto($request, Product $product): array
    {
        // 1. Start date
        $startDate = new DateTime();
        $startDate->setTimestamp(strtotime($request->startDate .' '. $request->startTime));

        // 2. End date = Start date + product.dayspan
        $endDate = new DateTime();
        $endDate->setTimestamp($startDate->getTimestamp());
        $endDate->add($this->dateInterval(sprintf('P%sD', $product->dayspan)));

        // 3. Let d = startDate
        $date = new DateTime();
        $date->setTimestamp($startDate->getTimestamp());

        // 4. Let promotions be an empty list
        $promotions = [];

        // 5. While d < e
        while ($date->getTimestamp() < $endDate->getTimestamp()) {
            // Ensure a < b
            list ($a, $b) = $this->computeValidity($date->format('Y-m-d'), $request);

            // 5.1
            $promotion = new Promotion();
            $promotion->productId = $product->id;
            $promotion->validFrom = $a;
            $promotion->validTo = $b;
            $promotion->credits = 0;

            // 5.2
            $promotion->timestamps = $this->computeTimestamps($product, $a, $b);

            // 5.3
            $promotions[] = $promotion;

            // 5.4. Next date
            $date->add($this->dateInterval('P1D'));
        }

        // 6
        return $promotions;
    }

    /**
     * @param Product $product
     * @param int $t1
     * @param int $t2
     *
     * @return int[]
     */
    private function computeTimestamps(Product $product, int $t1, int $t2): array
    {
        if (empty($product->promotions)) {
            // Assume just one promotion
            $product->promotions = 1;
        }

        $delta = abs($t2 - $t1);
        $updatePeriod = (int) max($this->minUpdatePeriod, $delta / $product->promotions);

        $t = $t1;
        $timestamps = [];

        while ($t < $t2) {
            $timestamps[] = $t;
            $t += $updatePeriod;
        }

        return $timestamps;
    }

    /**
     * @param string $date
     * @param $request
     *
     * @return int[]
     */
    private function computeValidity(string $date, $request): array
    {
        $a = strtotime($date .' '. $request->startTime);
        $b = strtotime($date .' '. $request->endTime);

        if ($b <= $a) {
            $b += 86400;
        }

        return [$a, $b];
    }

    /**
     * @param string $duration
     *
     * @return DateInterval
     */
    private function dateInterval(string $duration): DateInterval
    {
        try {
            return new DateInterval($duration);
        } catch (Exception $e) {
            throw new InvalidArgumentException('Invalid duration '. $duration);
        }
    }
}
