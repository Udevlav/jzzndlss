<?php

namespace App\Scrap;

use App\Config\Config;
use Goutte\Client;
use Phalcon\Mvc\User\Component;
use Symfony\Component\DomCrawler\Crawler;

use Throwable;
use stdClass;

abstract class Scrapper extends Component
{

    /**
     * @var string
     */
    protected $id = '';

    /**
     * @var string
     */
    protected $baseUrl = '';

    /**
     * @var bool
     */
    protected $useCache = false;

    /**
     * @var bool
     */
    protected $fromCache = false;

    /**
     * @var bool
     */
    protected $allowDownload = true;

    /**
     * @var \Goutte\Client
     */
    protected $client;

    /**
     * The current scrapping session
     *
     * @var ScrappingSession
     */
    protected $session;

    /**
     * The current scrapping target
     *
     * @var ScrappingTarget | null
     */
    protected $currentTarget;


    public function __construct(bool $useCache = true, bool $allowDownload = true)
    {
        $this->client = new Client();
        $this->session = new ScrappingSession();
        $this->useCache = $useCache;
        $this->allowDownload = $allowDownload;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        if (empty($this->id)) {
            $cn = explode('\\', static::class);
            $this->id = str_replace('scrapper', '', strtolower(array_pop($cn)));
        }

        return $this->id;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return rtrim($this->baseUrl, '/');
    }

    /**
     * Returns whether the schedule comes from cached data.
     *
     * @return bool
     */
    public function isFromCache(): bool
    {
        return $this->fromCache;
    }

    /**
     * Returns whether downloads are allowed.
     *
     * @return bool
     */
    public function allowDownload(): bool
    {
        return $this->allowDownload;
    }


    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return ScrappingSession
     */
    public function getSession(): ScrappingSession
    {
        return $this->session;
    }


    /**
     * @return string
     */
    public function cachePath(): string
    {
        $path = sprintf('%s/.%s.%s.cache',
            sys_get_temp_dir(),
            $this->getConfig()->app->id,
            $this->getId()
        );

        return $path;
    }

    /**
     * @param ScrappingTarget $target
     *
     * @return string
     */
    public function targetPath(ScrappingTarget $target): string
    {
        $path = sprintf('%s/%s', $this->cachePath(), md5($target->getUrl()));

        return $path;
    }

    /**
     * Schedules a list of URLs that should be scrapped. The way this links
     * are generated is implementation-dependent.
     *
     * @return void
     *
     * @throws ScrappingException on a general web-scrapping error
     */
    public function schedule()
    {
        if (! $this->useCache || ! $this->loadCache()) {
            $this->doSchedule();
            $this->saveCache();
        }
    }

    /**
     * Processes an URL, and returns the scrapped data.
     *
     * @param ScrappingTarget $target
     *
     * @return void
     *
     * @throws NotFoundException if the given URL cannot be fetched
     * @throws ScrappingException on a general web-scrapping error
     */
    public function scrap(ScrappingTarget $target)
    {
        if ($this->shouldSkip($target)) {
            return;
        }

        if (! $this->useCache || ! $this->loadTarget($target)) {
            $crawler = $this->client->request('GET', $target->getUrl());

            $this->currentTarget = $target;

            try {
                $target->setData($this->doScrap($target, $crawler));
            } catch (Throwable $t) {
                throw new ScrappingException($t->getMessage(), $t->getCode(), $t);
            } finally {
                $this->currentTarget = null;
            }
        }

        if ($this->useCache && ! $target->isFromCache()) {
            $this->saveTarget($target);
        }
    }

    /**
     * Flushes the cache contents, skipping targets which have been
     * successfully scrapped.
     *
     * If this instance does not use a cache, this is a no-op.
     *
     * @return void
     */
    public function flushCache()
    {
        if ($this->useCache) {
            $this->saveCache(true);
        }
    }

    /**
     * Removes the cache contents.
     *
     * If this instance does not use a cache, this is a no-op.
     *
     * @return void
     */
    public function emptyCache()
    {
        if ($this->useCache) {
            $this->clearCache(true);
        }
    }


    /**
     * @return void
     *
     * @throws ScrappingException on a general web-scrapping error
     */
    abstract protected function doSchedule();

    /**
     * @param ScrappingTarget $target
     * @param Crawler $crawler
     *
     * @return mixed the scrapped data
     */
    abstract protected function doScrap(ScrappingTarget $target, Crawler $crawler);


    /**
     * @param ScrappingTarget $target
     *
     * @return bool
     */
    protected function shouldSkip(ScrappingTarget $target): bool
    {
        return false;
    }

    /**
     * Downloads the resource at the given URL and returns the path of the
     * downloaded file, in the local filesystem.
     *
     * @param string $src
     *
     * @return string
     */
    protected function download(string $src): string
    {
        $path = sprintf('%s/downloads/%s', $this->cachePath(), basename($src));

        if (false !== ($offset = strpos($path, '?'))) {
            $path = substr($path, 0, $offset);
        }

        if (preg_match('/\.jpeg$/', $path)) {
            $path = preg_replace('/\.jpeg$/', '.jpg', $path);
        }

        if (file_exists($path)) {
            return $path;
        }

        if (false === ($data = @file_get_contents($src))) {
            throw new ScrappingException(sprintf('Failed to download "%s".', $src));
        }

        if (false === @file_put_contents($path, $data)) {
            throw new ScrappingException(sprintf('Failed to write downloaded data to "%s".', $path));
        }

        $this->currentTarget->addDownload($src, $path, ScrappingTargetStatus::PERSISTED);

        return $path;
    }


    protected function loadCache(): bool
    {
        $path = sprintf('%s/index', $this->cachePath());

        if (! file_exists($path) || ! is_readable($path)) {
            return false;
        }

        if (false === ($data = file_get_contents($path))) {
            return false;
        }

        try {
            $data = json_decode($data, true);
        } catch (Throwable $t) {
            return false;
        }

        if (empty($data)) {
            return false;
        }

        if (time() - $data['timestamp'] > 43200) {
            // Data is no longer valid, more than half
            // a day old
            return false;
        }

        $urls = array_keys($data['targets']);
        shuffle($urls);

        foreach ($urls as $url) {
            if (empty($url)) {
                continue;
            }

            $this->session->add($url, $data['targets'][$url]);
        }

        $this->fromCache = true;

        return true;
    }

    protected function loadTarget(ScrappingTarget $target): bool
    {
        $path = $this->targetPath($target);

        if (! file_exists($path) || ! is_readable($path)) {
            return false;
        }

        if (false === ($data = file_get_contents($path))) {
            return false;
        }

        try {
            $object = unserialize($data);
        } catch (Throwable $t) {
            return false;
        }

        $object->data->experiences = $object->experiences;
        $object->data->images = $object->images;
        $object->data->videos = $object->videos;
        $object->data->tags = $object->tags;

        $target->setMeta($object->meta);
        $target->setData($object->data, true);

        return true;
    }

    protected function saveCache(bool $skipScrapped = false): bool
    {
        $path = $this->cachePath();

        if (! is_writable(dirname($path))) {
            return false;
        }

        if (! is_dir($path)) {
            if (false === mkdir($path)) {
                return false;
            }

            if (false === mkdir($path .'/downloads')) {
                return false;
            }
        }

        return $this->saveCacheIndex($skipScrapped);
    }

    protected function saveCacheIndex(bool $skipScrapped = false): bool
    {
        $path = sprintf('%s/index', $this->cachePath());
        $data = [];

        foreach ($this->session->getTargets() as $target) {
            if (! $skipScrapped || ! $target->isScrapped()) {
                $data[$target->getUrl()] = $target->getMeta();
            }
        }

        $cache = new stdClass();
        $cache->timestamp = time();
        $cache->targets = $data;

        try {
            $json = @json_encode($cache);
        } catch (Throwable $t) {
            return false;
        }

        return @file_put_contents($path, $json);
    }

    protected function saveTarget(ScrappingTarget $target): bool
    {
        $path = $this->targetPath($target);

        try {
            $data = serialize($target->toObject());
        } catch (Throwable $t) {
            return false;
        }

        return @file_put_contents($path, $data);

    }

    protected function clearCache(): bool
    {
        $path = $this->cachePath();

        if (false === $this->removeDir($path)) {
            throw new ScrappingException(sprintf('Failed to remove dir "%s"', $path));
        }

        return true;
    }

    protected function removeDir(string $path): bool
    {
        foreach (glob($path . '/*') as $file) {
            is_dir($file) ? $this->removeDir($file) : unlink($file);
        }

        return rmdir($path);
    }
}
