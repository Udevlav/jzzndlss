<?php

namespace App\Scrap;

use RuntimeException;

class ContinueScrappingException extends RuntimeException
{
}