<?php

namespace App\Scrap;

class NotFoundException extends ScrappingException
{

    static public function url(string $url): NotFoundException
    {
        return new static(sprintf('%s not found.', $url));
    }
}