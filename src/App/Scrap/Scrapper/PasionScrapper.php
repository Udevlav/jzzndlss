<?php

namespace App\Scrap\Scrapper;

use App\Model\Ad;
use App\Model\Media;
use App\Model\Province;
use App\Scrap\NameMatcher;
use App\Scrap\ScrappingTarget;
use App\Scrap\ScrapUtil;

use Symfony\Component\DomCrawler\Crawler;

use stdClass;

class PasionScrapper extends AdScrapper
{

    /**
     * @inheritdoc
     */
    protected $baseUrl = 'http://www.pasion.com';

    /**
     * @inheritdoc
     */
    protected $categories = [
        'escort' => [
            '/contactos-mujeres-en-%s',
            '/contactos-lesbianas-en-%s',
            '/contactos-parejas-liberales-en-%s',
            '/lineas-eroticas-en-%s',
            '/webcam-sexcam-eroticas-en-%s',
            '/masajes-eroticos-en-%s',
        ],
        'gigolo' => [
        /*
            '/contactos-hombres-en-%s',
            '/contactos-gays-en-%s',
         */
        ],
        'travesti' => [
        /*
            '/transexuales-travestis-en-%s',
         */
        ],
    ];

    protected function shuffle(array $array): array {
        $new = [];
        $keys = array_keys($array);

        shuffle($keys);

        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }

        return $new;
    }

    protected function doSchedule()
    {
        foreach ($this->shuffle(Province::$postalCodeToSlug) as $provinceCode => $slug) {
            foreach ($this->categories as $category => $formats) {
                foreach ($formats as $format) {
                    $baseURL = $this->resolveUrl(sprintf($format, $slug));

                    /** @var Crawler $crawler */
                    $crawler = $this->client->request('GET', $baseURL);

                    $crawler->filter('#cuerpo .x1')->each(function(Crawler $node) use ($baseURL, $category, $provinceCode) {
                        $this->doScheduleTarget($node, $baseURL, $category, $provinceCode);
                    });
                }
            }
        }
    }

    protected function doScheduleTarget(Crawler $crawler, $baseURL, $category, $provinceCode)
    {
        $age = 0;
        $name = null;
        $phone = null;
        $title = null;
        $text = null;

        $node = $crawler->filter('.x7 a');

        if ($node->count() === 0) {
            $node = $crawler->filter('.x9 a');
        }

        $targetURL = $this->resolveUrl($node->attr('href'));
        $title = $node->text();

        $text = $crawler->filter('.tx')->text();

        if (preg_match('/(\s*edad (\d+) años)$/i', $text, $match)) {
            $age = intval($match[2]);
            $text = str_replace($match[1], '', $text);

            if (preg_match('/(\.\s*(\d{9})\.?)$/', $text, $match)) {
                $phone = intval($match[2]);
                $text = str_replace($match[1], '', $text);
            }
        }

        if (empty($phone) && preg_match('/(\d{9})/', $text, $match)) {
            $phone = intval($match[1]);
        }

        $this->session->add($targetURL, [
            'promoted' => false,
            'age' => $age,
            'name' => $name,
            'title' => $title,
            'text' => $text,
            'phone' => $phone,
            'category' => $category,
            'provinceCode' => $provinceCode,
        ]);
    }

    protected function createAd(ScrappingTarget $target): Ad
    {
        $data = parent::createAd($target);
        $data->provider = 'pasion';

        // Set all available data from meta
        $meta = $target->getMeta();

        if (isset($meta->age)) {
            $data->age = $meta->age;
        }
        if (isset($meta->name)) {
            $data->name = $meta->name;
        }
        if (isset($meta->title)) {
            $data->title = ScrapUtil::capitalize($meta->title);
        }
        if (isset($meta->text)) {
            $data->text = $meta->text;
        }
        if (isset($meta->phone)) {
            $data->phone = $meta->phone;
        }
        if (isset($meta->category)) {
            $data->category = $meta->category;
        }
        if (isset($meta->provinceCode)) {
            $data->postalCode = $meta->provinceCode;
        }
        $data->twitter = null;
        $data->whatsapp = true;

        return $data;
    }

    protected function normalize(Ad $ad)
    {
        if (empty($ad->city) && ! empty($ad->postalCode)) {
            $ad->city = Province::name($ad->postalCode);
        }

        if (empty($ad->phone)) {
            $ad->phone = '000000000';
        }

        $ad->title = ScrapUtil::cleanUtf8($ad->title);
        $ad->text = ScrapUtil::cleanUtf8($ad->text);

        parent::normalize($ad);
    }

    protected function doScrapExtra(Crawler $crawler, Ad $data)
    {
        $data->numHits = intval($crawler->filter('.pagAnuStatsAnu .dato strong')->text());
    }

    protected function doScrapTitleAndText(Crawler $crawler, Ad $data)
    {
        try {
            $data->title = str_replace('PASION.COM - ', '', trim($crawler->filter('title')->text()));

            if (preg_match('/<meta name="description" content="([^"]+)"/', $crawler->html(), $match)) {
                $data->text = trim($match[1]);
            }
        } catch (\Throwable $t) {
            $data->title = ScrapUtil::cleanText($data->title);
            $data->text = ScrapUtil::cleanText($data->text);
        }

        $tokens = preg_split('/\s+/', $data->title);

        while (count($tokens) && (empty($data->name) || strlen($data->name) < 3)) {
            $token = array_shift($tokens);
            $data->name = ucfirst(strtolower($token));
        }

        if (! $this->matcher->valid($data->name)) {
            if (! empty($match = $this->matcher->match($data->title))) {
                $data->name = $match;
            } else if (! empty($match = $this->matcher->match($data->text))) {
                $data->name = $match;
            }
        }
    }

    protected function doScrapPhone(Crawler $crawler, Ad $data)
    {
        if (empty($data->phone) || $data->phone === '000000000') {
            if (preg_match('/((?:\d{3})(?:[\s\.-]*)(?:\d{3})(?:[\s\.-]*)(?:\d{3}))/', $data->title, $match) ||
                preg_match('/((?:\d{3})(?:[\s\.-]*)(?:\d{3})(?:[\s\.-]*)(?:\d{3}))/', $data->text, $match)) {
                $data->phone = trim(preg_replace('/(\s|\.|-)+/', '', $match[1]));
            }
        }

        if (empty($data->phone) || $data->phone === '000000000') {
            $href = $crawler->filter('#pagAnuContactAnuBox .pagAnuContactAnu a')->attr('href');

            if (preg_match('/javascript:od\(\'([^\']+)\'\)/', $href, $match)) {
                $crawler = $this->client->request('GET', 'http://www.pasion.com/datos-contacto/?id='. $match[1]);
                $js = $crawler->html();

                if (preg_match('/document\.write\(\'([^\']+)\'\)/', $js, $m)) {
                    $unicode = rawurldecode(str_replace('%u00', '%', $m[1]));

                    if (preg_match('/>([\d]{9})</', $unicode, $n)) {
                        $data->phone = $n[1];
                    }
                }
            }
        }
    }

    protected function doScrapPersonalData(Crawler $crawler, Ad $data)
    {
        $data->ethnicity = null;
        $data->origin = null;
        $data->occupation = null;
        $data->schooling = null;
        $data->languages = null;
        $data->hairColor = null;
        $data->bsize = null;
        $data->height = null;
        $data->weight = null;
    }

    protected function doScrapLocation(Crawler $crawler, Ad $data)
    {
        $data->city = '';
        $data->zone = '';
    }

    protected function doScrapFees(Crawler $crawler, Ad $data)
    {
        $data->fee = -1;
        $data->fees = [];

        if (preg_match('/((?:[0-9]*[.,])?[0-9]+)\s*eur(os?)*/i', $data->title, $match) ||
            preg_match('/((?:[0-9]*[.,])?[0-9]+)\s*eur(os?)*/i', $data->text, $match) ||
            preg_match('/((?:[0-9]*[.,])?[0-9]+)\p{Sc}/u', $data->title, $match) ||
            preg_match('/((?:[0-9]*[.,])?[0-9]+)\p{Sc}/u', $data->text, $match)) {
            $fee = new stdClass();
            $fee->desc = '1h';
            $fee->fee = floatval($match[1]);

            $data->fees[] = $fee;
            $data->fee = $fee->fee;
        }
    }

    protected function doScrapAvailability(Crawler $crawler, Ad $data)
    {
        $data->availability = [];

        if (preg_match('/24\s*h/i', $data->title) ||
            preg_match('/24\s*h/i', $data->text) ||
            preg_match('/todos los días/i', $data->title) ||
            preg_match('/todos los días/i', $data->text)) {
            $entry = new stdClass();
            $entry->day = -1;
            $entry->timerange = ['00:00','00:00'];
        }
    }

    protected function doScrapImages(Crawler $crawler, Ad $data)
    {
        $data->images = [];
        $data->numImages = 0;

        $html = $crawler->html();
        $offset = 0;

        if (preg_match_all('/f = (.*);eval/m', $html, $match)) {
            foreach ($match[1] as $src) {
                $src = str_replace('"+"', '', $src);
                $src = explode('"+j+"', $src);
                $src = array_shift($src);
                $src = str_replace('"', '', str_replace(';i=', '', $src)) .'jpg';

                $image = $this->doScrapImage($src, 0 === $offset);

                if (null !== $image) {
                    $data->images[] = $image;
                    $offset ++;
                }
            }
        }

        $data->numImages = count($data->images);
    }

    protected function doScrapImage(string $src, bool $poster = false)
    {
        if ('jpg' !== substr($src, -3) &&
            'jpeg' !== substr($src, -4)) {
            return null;
        }

        $path = $this->download($src);

        $media = new Media();
        $media->temp = false;
        $media->poster = $poster;
        $media->name = basename($path);
        $media->version = 'md';
        $media->path = $this->relativize($path);
        $media->type = 'image/jpeg';
        $media->size = @filesize($path);
        $media->width = 0;
        $media->height = 0;
        $media->uploaderIp = '127.0.0.1';

        return $media;
    }

    protected function doScrapVideos(Crawler $crawler, Ad $data)
    {
        $data->videos = [];
        $data->numVideos = 0;
    }

    protected function relativize(string $path): string
    {
        return str_replace($this->getConfig()->layout->mediaDir, '/', $path);
    }
}