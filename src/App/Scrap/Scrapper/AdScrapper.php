<?php

namespace App\Scrap\Scrapper;

use App\Geo\GeoCoder;
use App\Model\Ad;
use App\Model\Layout;
use App\Model\Province;
use App\Scrap\Scrapper;
use App\Scrap\ScrappingException;
use App\Scrap\ScrappingTarget;
use App\Scrap\Util\NameMatcher;

use Symfony\Component\DomCrawler\Crawler;

abstract class AdScrapper extends Scrapper
{

    protected $matcher;

    public function __construct(bool $useCache = true, bool $allowDownload = true)
    {
        parent::__construct($useCache, $allowDownload);

        $this->matcher = new NameMatcher();
    }

    /**
     * Scheduling by category.
     *
     * Scraps each category page and looks for ads to be added as
     * {@link ScrappingTargets} to the session. Actual target scrapping
     * is delegated to the {@link ::doScrapCategoryTargets} method.
     *
     * @return void
     */
    protected function doScheduleByCategory()
    {
    }

    /**
     * Scheduling by province.
     *
     * Scraps each province page and looks for ads to be added as
     * {@link ScrappingTargets} to the session. Actual target scrapping
     * is delegated to the {@link ::doScrapProvinceTargets} method.
     *
     * @return void
     */
    protected function doScheduleByProvince()
    {
        $offset = 0;

        foreach (Province::$postalCodeToSlug as $provinceCode => $slug) {
            $path = $this->doGenerateProvincePath($provinceCode, $slug);

            /** @var Crawler $crawler */
            $crawler = $this->client->request('GET', $this->resolveUrl($path));

            $this->doScrapProvinceTargets($provinceCode, $crawler);

            $offset ++;

            if ($offset > 10) {
                break;
            }
        }
    }

    /**
     * Generates the path for the given category.
     *
     * @param $category
     *
     * @return string
     */
    protected function doGenerateCategoryPath($category): string
    {
        return '/'. $category;
    }

    /**
     * Generates the path for the given province.
     *
     * @param string $provinceCode
     * @param string $slug
     *
     * @return string
     */
    protected function doGenerateProvincePath($provinceCode, string $slug): string
    {
        return '/'. $slug;
    }

    /**
     * Scraps a category page and adds all found targets to the session.
     *
     * @param $category
     * @param Crawler $crawler
     *
     * @return void
     */
    protected function doScrapCategoryTargets($category, Crawler $crawler)
    {
    }

    /**
     * Scraps a province page and adds all found targets to the session.
     *
     * @param $provinceCode
     * @param Crawler $crawler
     *
     * @return void
     */
    protected function doScrapProvinceTargets($provinceCode, Crawler $crawler)
    {
    }

    /**
     * @inheritdoc
     */
    protected function doScrap(ScrappingTarget $target, Crawler $crawler)
    {
        $data = $this->createAd($target);

        $this->doScrapCategory($crawler, $data);
        $this->doScrapTitleAndText($crawler, $data);
        $this->doScrapPhone($crawler, $data);
        $this->doScrapPersonalData($crawler, $data);
        $this->doScrapAgency($crawler, $data);
        $this->doScrapLocation($crawler, $data);
        $this->doScrapFees($crawler, $data);
        $this->doScrapAvailability($crawler, $data);
        $this->doScrapImages($crawler, $data);
        $this->doScrapVideos($crawler, $data);
        $this->doScrapTags($crawler, $data);
        $this->doScrapExtra($crawler, $data);

        // Normalize scrapped data and return; if data cannot be normalized
        // a ScrappingException is thrown
        $this->normalize($data);

        return $data;
    }

    /**
     * Creates a new {@link Ad} entity and sets the default fields.
     *
     * @param ScrappingTarget $target
     *
     * @return Ad
     */
    protected function createAd(ScrappingTarget $target): Ad
    {
        // Set defaults
        $data = new Ad();
        $data->country = 'ES';
        $data->cash = true;
        $data->card = true;
        $data->layout = Layout::PORTRAIT;
        $data->lastHitAt = date('Y-m-d H:i:s');
        $data->numExperiences = 0;
        $data->numHits = 0;
        $data->numImages = 0;
        $data->numVideos = 0;
        $data->fee = -1;
        $data->fees = [];
        $data->availability = [];
        $data->independent = true;

        // By default, all communications are redirected
        // to the scrap mailbox
        $data->email = $this->getConfig()->mailbox->scrap->from;

        // Check target metadata
        $meta = $target->getMeta();

        if (isset($meta->provinceCode)) {
            $data->postalCode = $meta->provinceCode;
            $data->city = Province::name($data->postalCode) ?? null;
        }

        return $data;
    }

    /**
     * Scraps the following fields (category):
     *
     *    category
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapCategory(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (ad title and text):
     *
     *    title
     *    text
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapTitleAndText(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (phone):
     *
     *    phone
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapPhone(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (personal data):
     *
     *    name
     *    age
     *    twitter
     *    whatsapp
     *    ethnicity
     *    origin
     *    occupation
     *    schooling
     *    languages
     *    hairColor
     *    bsize
     *    height
     *    weight
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapPersonalData(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (location):
     *
     *    agency
     *    independent
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapAgency(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (location):
     *
     *    lat
     *    lng
     *    country
     *    postalCode
     *    city
     *    zone
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapLocation(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (fees and payment methods):
     *
     *    feees
     *    fee
     *    card
     *    cash
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapFees(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields:
     *
     *    availability
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapAvailability(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (images):
     *
     *    layout
     *    poster
     *    images
     *    numImages
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapImages(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (videos):
     *
     *    videos
     *    numVideos
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapVideos(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps the following fields (tags):
     *
     *    tags
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapTags(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Scraps extra data.
     *
     * @param Crawler $crawler
     * @param Ad      $data
     *
     * @return void
     */
    protected function doScrapExtra(Crawler $crawler, Ad $data)
    {
    }

    /**
     * Normalizes the given {@link Ad} entity, which is supposed to be
     * scrapped from a given page.
     *
     * The base implementation just populates the latitude and longitude
     * attributes if necessary, from the postal code, city or zone. This
     * may require a query to a third-party geocoding API. Additionally,
     * ad model fields are serialized and a slug is generated.
     *
     * @param Ad $ad
     *
     * @return void
     *
     * @throws ScrappingException
     */
    protected function normalize(Ad $ad)
    {
        if (empty($ad->name)) {
            if (null !== ($name = $this->matcher->match($ad->title))) {
                $ad->name = $name;
            }

            if (null !== ($name = $this->matcher->match($ad->text))) {
                $ad->name = $name;
            }
        }

        // Entity is completely useless without a name or a phone
        if (empty($ad->name) || empty($ad->phone) || strlen($ad->phone) < 9) {
            throw new ScrappingException('Failed to scrap ad: could not scrap name nor phone.');
        }

        if (empty($ad->city) && ! empty($ad->zone)) {
            if (preg_match('/\d{5} ([a-zA-Z0-9\s]+),/', $ad->zone, $match)) {
                $ad->city = $match[1];
            }
        }

        if (empty($ad->zone) && ! empty($ad->city)) {
            $ad->zone = $ad->city;
        }

        if (empty($ad->age)) {
            $ad->age = 0;
        }

        if (empty($ad->lat) || empty($ad->lng)) {
            $this->geocode($ad);
        }

        // Replace any occurrence of sites with Wallalumi
        if (! empty($ad->title)) {
            $ad->title = $this->replaceProviderNames($ad->title);
        }

        if (! empty($ad->text)) {
            $ad->text = $this->replaceProviderNames($ad->text);
        }
    }

    /**
     * @param string $text
     *
     * @return string
     */
    protected function replaceProviderNames(string $text): string
    {
        return str_replace([
            'forosx',
            'nuevoloquo',
            'nuevo loquo',
            'slumi',
            'Forosx',
            'Nuevoloquo',
            'Nuevo loquo',
            'Slumi',
        ], 'Wallalumi', $text);
    }

    /**
     * Resolves the given path against the base URL.
     *
     * @param string $path
     *
     * @return string the absolute URL
     */
    protected function resolveUrl(string $path): string
    {
        return sprintf('%s%s', $this->baseUrl, $path);
    }

    /**
     * Relativizes the given URL against the base URL.
     *
     * @param string $url
     *
     * @return string the relative URL
     */
    protected function relativizeUrl(string $url): string
    {
        return str_replace($this->baseUrl, '', $url);
    }

    /**
     * Relativizes the given path against the upload path.
     *
     * @param string $path
     *
     * @return string the absolute URL
     */
    protected function relativizePath(string $path): string
    {
        return str_replace($this->getConfig()->layout->publicDir, '/', $path);
    }

    /**
     * Queries a geocoding API and sets the ad latitude and longitude
     * attributes.
     *
     * @param Ad $ad
     *
     * @return void
     */
    protected function geocode(Ad $ad)
    {
        $address = ! empty($ad->zone) ? $ad->zone : $ad->city;
        $address = preg_replace('/(\s+capital)$/i', '', $address);

        $latLng = $this->getGeoCoder()->lookup($address .', España');

        $ad->lat = $latLng->getLat();
        $ad->lng = $latLng->getLng();
    }

    /**
     * Generates the temporary path for a the given URL.
     *
     * @param string $url
     *
     * @return string
     */
    protected function doGenerateTempPath(string $url): string
    {
        $config = $this->getConfig();

        $ext = explode('.', $url);
        $ext = array_pop($ext);
        $hash = 's-'. $this->di->get('hasher')->hash($url);
        $path = sprintf('%s%s.%s', $config->layout->uploadDir, $hash, $ext);

        return $path;
    }

    /**
     * @return GeoCoder
     */
    protected function getGeoCoder(): GeoCoder
    {
        return $this->di->get('geocoder');
    }
}
