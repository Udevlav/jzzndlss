<?php

namespace App\Scrap\Scrapper;

use App\Model\Ad;
use App\Model\Media;
use App\Model\Province;
use App\Model\Tag;
use App\Scrap\ScrappingTarget;
use App\Scrap\ScrapUtil;

use Symfony\Component\DomCrawler\Crawler;

use stdClass;

class SlumiScrapper extends AdScrapper
{

    protected $dayNameToDayNumber = [
        'lunes'     => 1,
        'martes'    => 2,
        'miercoles' => 3,
        'miércoles' => 3,
        'jueves'    => 4,
        'viernes'   => 5,
        'sabado'    => 6,
        'sábado'    => 6,
        'domingo'   => 0,
    ];

    /**
     * @inheritdoc
     */
    protected $baseUrl = 'https://www.slumi.com';

    /**
     * @inheritdoc
     */
    protected function createAd(ScrappingTarget $target): Ad
    {
        $data = parent::createAd($target);
        $data->provider = 'slumi';
        $data->postalCode = $target->getMeta()->provinceCode;

        return $data;
    }

    /**
     * @inheritdoc
     */
    protected function normalize(Ad $ad)
    {
        if (empty($ad->city)) {
            $ad->city = Province::name($ad->postalCode);
        }

        parent::normalize($ad);
    }

    /**
     * @inheritdoc
     */
    protected function doSchedule()
    {
        $this->doScheduleByProvince();
    }

    /**
     * @inheritdoc
     */
    protected function doGenerateProvincePath($provinceCode, string $slug): string
    {
        return sprintf('/escorts/provincia-%s', $slug);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapProvinceTargets($provinceCode, Crawler $crawler)
    {
        $crawler->filter('.anuncio.top_on > div > a:first-child')->each(function(Crawler $node) use ($provinceCode) {
            $this->session->add($this->baseUrl . $node->attr('href'), [
                'promoted' => true,
                'provinceCode' => $provinceCode,
            ]);
        });

        $crawler->filter('.anuncio.top_off > div > a:first-child')->each(function(Crawler $node) use ($provinceCode) {
            $this->session->add($this->baseUrl . $node->attr('href'), [
                'promoted' => false,
                'provinceCode' => $provinceCode,
            ]);
        });
    }


    /**
     * @inheritdoc
     */
    protected function doScrapCategory(Crawler $crawler, Ad $data)
    {
        $data->category = 'escort';
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTitleAndText(Crawler $crawler, Ad $data)
    {
        $data->title = ScrapUtil::cleanText($crawler->filter('h1')->text());
        $data->text = ScrapUtil::cleanText($crawler->filter('#anuncio_texto')->text());

        if (preg_match('/^([a-z\s]+)\s+(\d{9}),\s+(.*)\.?/i', $data->title, $match)) {
            $data->title = $match[1] .', '. $match[3];
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPhone(Crawler $crawler, Ad $data)
    {
        $data->phone = str_replace('tel:', '', $crawler->filter('#anuncio_telefono a')->attr('href'));
        $data->twitter = null;
        $data->whatsapp = true;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPersonalData(Crawler $crawler, Ad $data)
    {
        $data->name = ScrapUtil::capitalize(ScrapUtil::cleanText($crawler->filter('#anuncio_nombre')->text()));
        $data->age = intval($crawler->filter('#anuncio_edad')->text());
        $data->ethnicity = null;
        $data->origin = null;
        $data->occupation = null;
        $data->schooling = null;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAgency(Crawler $crawler, Ad $data)
    {
    }

    /**
     * @inheritdoc
     */
    protected function doScrapLocation(Crawler $crawler, Ad $data)
    {
        $postalCode = Province::postalCode(ScrapUtil::cleanText($crawler->filter('#anuncio_provincia')->text()));

        if (! empty($postalCode)) {
            $data->postalCode = $postalCode;
        }

        $data->city = ScrapUtil::cleanText($crawler->filter('#anuncio_poblacion')->text());

        $nodes = $crawler->filter('.pie_mapa .localizacion');

        if ($nodes->count()) {
            $data->zone = $nodes->first()->text();
        }

        $nodes = $crawler->filter('#anuncio_zona');

        if ($nodes->count()) {
            $data->zone = $nodes->text();
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapFees(Crawler $crawler, Ad $data)
    {
        $data->fee = floatval($crawler->filter('#anuncio_tarifa')->text());
        $data->fees = [];

        $crawler->filter('div#anuncio_disponibilidad .bloque_tarifas table tr')->each(function(Crawler $row) use ($data) {
            $fee = new stdClass();

            $row->filter('td')->each(function(Crawler $cell, $i) use ($fee) {
                if (0 === $i) {
                    $fee->desc = ScrapUtil::cleanText($cell->text());
                } else {
                    $fee->fee = floatval($cell->text());
                }
            });

            $data->fees[] = $fee;
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAvailability(Crawler $crawler, Ad $data)
    {
        $data->availability = [];

        $crawler->filter('div#anuncio_disponibilidad .bloque_horario table tr')->each(function(Crawler $row) use ($data) {
            $entry = new stdClass();

            $row->filter('td')->each(function(Crawler $cell, $i) use ($entry) {
                $text = strtolower($cell->text());

                if (0 === $i) {
                    $entry->day = $this->dayNameToDayNumber[$text] ?? -1;
                } else if ($text === '24 horas') {
                    $entry->timerange = ['00:00','00:00'];
                } else {
                    $entry->timerange = preg_split('/\s*,\s*/', $text, 2);
                }
            });

            $data->availability[] = $entry;
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImages(Crawler $crawler, Ad $data)
    {
        $data->images = [];
        $data->numImages = 0;

        $poster = $crawler->filter('#anuncio_imagen_portada')->attr('src');
        $posterName = basename($poster);

        $crawler->filter('div#anuncio_fotos img')->each(function(Crawler $node) use ($data, $posterName) {
            $basename = basename($node->attr('src'));

            $data->images[] = $this->doScrapImage($node, $basename === $posterName);
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImage(Crawler $node, bool $poster = false)
    {
        $src = $node->attr('src');

        if ('jpg' !== substr($src, -3) &&
            'jpeg' !== substr($src, -4)) {
            return null;
        }

        $path = $this->download($src);

        $media = new Media();
        $media->temp = false;
        $media->poster = $poster;
        $media->name = basename($src);
        $media->version = null;
        $media->path = $this->relativizePath($path);
        $media->type = 'image/jpeg';
        $media->size = @filesize($path);
        $media->width = $node->attr('width');
        $media->height = $node->attr('height');
        $media->uploaderIp = '127.0.0.1';

        return $media;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapVideos(Crawler $crawler, Ad $data)
    {
        $data->videos = [];
        $data->numVideos = 0;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTags(Crawler $crawler, Ad $data)
    {
        $data->tags = [];

        $crawler->filter('div.bloque_categorias_1 .anuncio_categoria')->each(function($node) use ($data) {
            $data->tags[] = $this->doScrapTag($node);
        });

        $crawler->filter('div.bloque_categorias_2 .anuncio_categoria')->each(function($node) use ($data) {
            $data->tags[] = $this->doScrapTag($node);
        });

        $crawler->filter('div.bloque_categorias_3 .anuncio_categoria')->each(function($node) use ($data) {
            $data->tags[] = $this->doScrapTag($node);
        });

        $crawler->filter('div.bloque_categorias_4 .anuncio_categoria')->each(function($node) use ($data) {
            $data->tags[] = $this->doScrapTag($node);
        });

        return $data;
    }

    private function doScrapTag(Crawler $node)
    {
        $tag = new Tag();
        $tag->slug = array_pop(explode('/', $node->attr('href')));
        $tag->name = $node->text();

        return $tag;
    }

}