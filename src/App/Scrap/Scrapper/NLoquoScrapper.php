<?php

namespace App\Scrap\Scrapper;

use App\Model\Ad;
use App\Model\Media;
use App\Model\Province;
use App\Scrap\ContinueScrappingException;
use App\Scrap\ScrappingTarget;
use App\Scrap\ScrapUtil;

use App\Scrap\Util\PriceMatcher;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

use Symfony\Component\DomCrawler\Crawler;

use Throwable;

use thiagoalessio\TesseractOCR\TesseractOCR;

use RuntimeException;

class NLoquoScrapper extends AdScrapper
{

    /**
     * Height of the cropped area (image is cropped vertically
     * from 0 to CROP_HEIGHT and from H - CROP_HEIGHT to H.)
     * Default value is 3.75%.
     *
     * @var int
     */
    const CROP_HEIGHT = 0.0375;
    const ENABLE_OCR = true;


    static private $categories = [
        'escort' => 'escort',
        'gigolo' => 'chico',
        'masaje-erotico' => 'escort',
        'otros' => 'escort',
        'travesti' => 'travesti',
    ];

    /**
     * @inheritdoc
     */
    protected $baseUrl = 'https://www.nuevoloquo.com';

    /**
     * @inheritdoc
     */
    protected function shouldSkip(ScrappingTarget $target): bool
    {
        $category = $this->getCategoryFromUrl($target);

        return $category !== 'escort';
    }

    /**
     * @inheritdoc
     */
    protected function createAd(ScrappingTarget $target): Ad
    {
        $meta = $target->getMeta();

        $data = parent::createAd($target);
        $data->provider = 'nloquo';
        $data->category = $this->getCategoryFromUrl($target);

        if (isset($meta->provinceCode)) {
            $data->postalCode = $meta->provinceCode;
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    protected function normalize(Ad $ad)
    {
        if (! empty($ad->postalCode)) {
            if (empty($ad->city)) {
                $ad->city = Province::name($ad->postalCode);
            }

            if (empty($ad->zone)) {
                $ad->zone = Province::name($ad->postalCode);
            }
        }

        parent::normalize($ad);
    }

    protected function getCategoryFromUrl(ScrappingTarget $target): string
    {
        $path = explode('/', $this->relativizeUrl($target->getUrl()));
        array_shift($path);

        return static::$categories[array_shift($path)];
    }

    /**
     * @inheritdoc
     */
    protected function doSchedule()
    {
        $this->doScheduleByProvince();
    }

    /**
     * @inheritdoc
     */
    protected function doGenerateProvincePath($provinceCode, string $slug): string
    {
        return sprintf('/anuncios-eroticos/%s', $slug);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapProvinceTargets($provinceCode, Crawler $crawler)
    {
        try {
            $crawler->filter('.tableGirlInfo')->each(function($node) use ($provinceCode) {
                $url = $node->filter('h3 a')->attr('href');

                $this->session->add($this->baseUrl . $url, [
                    'promoted' => (false !== strpos($node->attr('class'), 'highlighted')),
                    'provinceCode' => $provinceCode,
                ]);
            });
        } catch (Throwable $t) {
            try {
                $crawler->filter('.entry a')->each(function($node) use ($provinceCode) {
                    $url = $node->attr('href');

                    $this->session->add($this->baseUrl . $url, [
                        'promoted' => (false !== strpos($node->filter('.tableGirlInfo')->attr('class'), 'highlighted')),
                        'provinceCode' => $provinceCode,
                    ]);
                });
            } catch (Throwable $t) {
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapCategory(Crawler $crawler, Ad $data)
    {
        // Noop - obtained from the target URL
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTitleAndText(Crawler $crawler, Ad $data)
    {
        try {
            $data->title = ucfirst(strtolower(str_replace('Nuevo Loquo - ', '', trim($crawler->filter('title')->text()))));

            if (empty($data->title)) {
                if (preg_match('/<meta name="description" content="([^"]+)"/', $crawler->html(), $match)) {
                    $data->title = trim($match[1]);
                } else {
                    $data->title = trim($crawler->filter('meta[name="description"]')->text());
                }
            }
        } catch (Throwable $t) {
            $data->title = ScrapUtil::cleanText($crawler->filter('h1')->text());
            $data->title = preg_replace('/nuevoloquo/i', 'Wallalumi', $data->title);
            $data->title = ScrapUtil::capitalize($data->title);

            try {
                $data->text = ScrapUtil::cleanUtf8(ScrapUtil::cleanText($crawler->filter('#descriptionContainer')->text()));
            } catch (Throwable $t) {
                $data->text = $data->title;
            }
        }

        $data->text = $crawler->filter('#descriptionContainer')->text();

        if (empty($data->text)) {
            $data->text = $data->title;
        }

        if (preg_match('/(\s*\(*\s*(dime|com.entame) que me viste en nuevoloquo\s*\)*\s*)/i', $data->text, $match)) {
            $data->text = trim(str_replace($match[1], '', $data->text));
        } else {
            $data->text = preg_replace('/nuevoloquo/i', 'Wallalumi', $data->text);
        }

        $tokens = preg_split('/\s+/', $data->title);

        while (count($tokens) && (empty($data->name) || strlen($data->name) < 3)) {
            $token = array_shift($tokens);
            $data->name = ucfirst(strtolower($token));
        }

        if (! $this->matcher->valid($data->name)) {
            if (! empty($match = $this->matcher->match($data->title))) {
                $data->name = $match;
            } else if (! empty($match = $this->matcher->match($data->text))) {
                $data->name = $match;
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPhone(Crawler $crawler, Ad $data)
    {
        // Try to scrap it from the title, then from the text
        $phone = null;

        if (preg_match('/\s*((?:6|7|8|9)\d{2}[:.-_\s]*\d{3}[:.-_\s]*\d{3})\s*/', $data->title, $match)) {
            $phone = (int) $match[1];
        } else {
            if (preg_match('/\s*((?:6|7|8|9)\d{2}[:.-_\s]*\d{3}[:.-_\s]*\d{3})\s*/', $data->text, $match)) {
                $phone = (int) $match[1];
            }
        }

        if (is_numeric($phone)) {
            $data->phone = $phone;
        } else {
            $data->phone = '000000000';
        }

        if ('000000000' === $data->phone) {
            $data->phone = $crawler->filter('.adPhone .phoneImg')->attr('src');

            if (self::ENABLE_OCR) {
                $src = $data->phone;

                if (0 !== strpos($src, 'http')) {
                    $src = 'https://www.nuevoloquo.com'. $src;
                }

                try {
                    $path = parent::download($src);
                    $newPath = str_replace('.jpg', '.png', $path);

                    rename($path, $newPath);

                    $ocr = new TesseractOCR($newPath);
                    $phone = $ocr
                        ->psm(13)
                        /*
                         * Avoid "Fatal error: Method thiagoalessio\TesseractOCR\Command::__toString()
                         * must not throw an exception, caught Exception: oem option is only available
                         * on Tesseract 3.05 or later".
                         */
                        // ->oem(3)
                        ->run();

                    if ($phone) {
                        $data->phone = preg_replace('/[^\d]/', '', $phone);
                    }

                    // The OCR creates two files named ocrXXXX and ocrXXXX.txt
                    // in the temp directory; delete both once we got the phone
                    //unlink($ocr->command->getOutputFile());
                    //unlink($ocr->command->getOutputFile(true));
                    exec('rm -fv /tmp/ocr*');

                    $this->currentTarget->setOcr(true);
                } catch (Throwable $t) {
                    throw new RuntimeException($t->getCode());
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPersonalData(Crawler $crawler, Ad $data)
    {
        // Name
        $words = preg_split('/\s+/', $crawler->filter('h1')->text());

        // Age - Try to scrap it from the title, then from the text
        $age = null;

        if (preg_match('/(\d{2})\s+(años|añitos)/', $data->title, $match)) {
            $age = (int) $match[1];
        } else {
            if (preg_match('/(\d{2})\s+(años|añitos)/', $data->text, $match)) {
                $age = (int) $match[1];
            }
        }

        if (is_numeric($age)) {
            $data->age = max(18, min(90, $age));
        } else {
            $data->age = 0;
        }

        // Not available fields
        $data->whatsapp = true;
        $data->twitter = null;
        $data->ethnicity = null;
        $data->origin = null;
        $data->occupation = null;
        $data->schooling = null;
        $data->languages = null;
        $data->hairColor = null;
        $data->bsize = null;
        $data->weight = null;
        $data->height = null;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAgency(Crawler $crawler, Ad $data)
    {
        $data->agency = null;
        $data->independent = empty($data->agency);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapLocation(Crawler $crawler, Ad $data)
    {
        $crawler->filter('.breadcrumb a span')->each(function($node, $offset) use ($data) {
            if (3 === $offset) {
                $data->city = preg_replace('/^contactos /i', '', preg_replace('/putas /i', '', ScrapUtil::cleanText($node->text())));
                $data->zone = preg_replace('/^contactos /i', '', preg_replace('/putas /i', '', ScrapUtil::cleanText($node->text())));
            }
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapFees(Crawler $crawler, Ad $data)
    {
        $text = $crawler->filter('#descriptionContainer')->text();

        // FIXME Incorporar código de David ~/Descargas/tarifas/test.php

        if (preg_match('/(?:(\d+)\s*(euros?|eur|eu))/i', $text, $match)) {
            $data->fee = floatval($match[1]);
            $data->fees = [];
        } else {
            // Try price matcher
            $matcher = new PriceMatcher();
            $data->fee = $matcher->match($text);

            // No hay manera de determinar el precio en Nuevo Loquo.
            // El anuncio simplemente no lo pone (e.g.:
            // https://www.nuevoloquo.com/escort/barcelona/sabrina-cubanas-con-mis-tetorros-fiestera-hazte-un-raya-en-michochito-salidas/278223/
            //$data->fee = 0;
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAvailability(Crawler $crawler, Ad $data)
    {
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImages(Crawler $crawler, Ad $data)
    {
        $data->images = [];
        $data->numImages = 0;

        $crawler->filter('#adImages img')->each(function($node, $offset) use ($data) {
            $image = $this->doScrapImage($node, 0 === $offset);

            if (null !== $image) {
                $data->images[] = $image;
            }
        });

        $data->numImages = count($data->images);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImage(Crawler $node, bool $poster = false)
    {
        $src = $node->attr('data-retina');

        if ('jpg' !== substr($src, -3) &&
            'jpeg' !== substr($src, -4)) {
            return null;
        }

        try {
            $path = $this->download($src);
        } catch (ContinueScrappingException $e) {
            return null;
        }

        $media = new Media();
        $media->temp = false;
        $media->poster = $poster;
        $media->name = basename($path);
        $media->version = null;
        $media->path = $this->relativizePath($path);
        $media->type = 'image/jpeg';
        $media->size = @filesize($path);
        $media->uploaderIp = '127.0.0.1';

        return $media;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapVideos(Crawler $crawler, Ad $data)
    {
        $data->videos = [];
        $data->numVideos = 0;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTags(Crawler $crawler, Ad $data)
    {
        $data->tags = [];
    }

    /**
     * @inheritdoc
     */
    protected function download(string $url): string
    {
        $path = parent::download($url);

        // Looks like NLoquo watermark is placed at top or bottom
        // of the image, in an horizontal strip which is approx.
        // 3.75% the height of the image -- we don't known whether
        // the watermark is on top or on bottom, so we crop both
        // areas
        $image = $this->getImagine()->open($path);

        $height = $image->getSize()->getHeight();
        $width = $image->getSize()->getWidth();

        $cropHeight = 42;//ceil(self::CROP_HEIGHT * $height);

        if ($height <= $cropHeight) {
            // Either a download error, or invalid image size
            throw new ContinueScrappingException(sprintf('Image is too small (height is %d px).', $height));
        }

        $image
            ->crop(new Point(0, $cropHeight), new Box($width, $height - 2 * $cropHeight))
            ->save($path);

        return $path;
    }

    protected function getImagine(): Imagine
    {
        return $this->di->get('imagine');
    }
}
