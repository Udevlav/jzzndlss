<?php

namespace App\Scrap\Scrapper;

use App\Model\Ad;
use App\Model\Media;
use App\Model\Province;
use App\Scrap\ScrappingTarget;
use App\Scrap\ScrapUtil;

use Symfony\Component\DomCrawler\Crawler;

class SustitutasScrapper extends AdScrapper
{

    protected $dayNameToDayNumber = [
        'lunes'     => 1,
        'martes'    => 2,
        'miercoles' => 3,
        'miércoles' => 3,
        'jueves'    => 4,
        'viernes'   => 5,
        'sabado'    => 6,
        'sábado'    => 6,
        'domingo'   => 0,
    ];

    /**
     * @inheritdoc
     */
    protected $baseUrl = 'https://www.sustitutas.es';

    /**
     * @inheritdoc
     */
    protected function createAd(ScrappingTarget $target): Ad
    {
        $data = parent::createAd($target);
        $data->provider = 'sustitutas';
        $data->postalCode = $target->getMeta()->provinceCode;

        return $data;
    }

    /**
     * @inheritdoc
     */
    protected function normalize(Ad $ad)
    {
        if (empty($ad->city)) {
            $ad->city = Province::name($ad->postalCode);
        }

        parent::normalize($ad);
    }

    /**
     * @inheritdoc
     */
    protected function doSchedule()
    {
        $this->doScheduleByProvince();
    }

    /**
     * @inheritdoc
     */
    protected function doGenerateProvincePath($provinceCode, string $slug): string
    {
        return sprintf('/escorts-y-putas-en-%s', $slug);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapProvinceTargets($provinceCode, Crawler $crawler)
    {
        $crawler->filter('.pbox > a')->each(function(Crawler $node) use ($provinceCode) {
            $this->session->add($node->attr('href'), [
                'promoted' => false,
                'provinceCode' => $provinceCode,
            ]);
        });
    }


    /**
     * @inheritdoc
     */
    protected function doScrap(ScrappingTarget $target, Crawler $crawler)
    {
        $data = parent::doScrap($target, $crawler);

        $id = explode('-', $target->getUrl());
        $id = array_pop($id);

        /* * @var Crawler $ajaxCrawler */
        $ajaxCrawler = $this->client->request('GET', sprintf('%s/site/buscarperfil?id=%d', $this->baseUrl, $id));

        if (empty($data->age)) {
            $data->age = intval($ajaxCrawler->filter('.first .last .info-line:first-child .info-value')->text());
        }

        if (empty($data->origin)) {
            $data->origin = trim($ajaxCrawler->filter('.first .last .info-line:nth-child(3) .info-value')->text());
        }

        if (empty($data->languages)) {
            $data->languages = trim($ajaxCrawler->filter('.first .last .info-line:nth-child(4) .info-value')->text());
        }

        if (empty($data->hairColor)) {
            $data->hairColor = trim($ajaxCrawler->filter('.first .last .info-line:nth-child(5) .info-value')->text());
        }

        if (empty($data->bsize)) {
            $data->bsize = intval(trim($ajaxCrawler->filter('.first .last .info-line:nth-child(8) .info-value')->text()));
        }

        if (empty($data->height)) {
            $data->height = intval(trim($ajaxCrawler->filter('.first .last .info-line:nth-child(6) .info-value')->text()));
        }

        if (empty($data->weight)) {
            $data->weight = intval(trim($ajaxCrawler->filter('.first .last .info-line:nth-child(7) .info-value')->text()));
        }

        if (empty($data->agency)) {
            $data->agency = trim((string) $ajaxCrawler->filter('.first .first .info-line:first-child .info-value')->text());
            $data->independent = empty($data->agency);
        }

        if (empty($data->zone)) {
            $data->zone = trim($ajaxCrawler->filter('.first .first .info-line:nth-child(3) .info-value')->text());
        }

        if (empty($data->city)) {
            $data->city = trim($ajaxCrawler->filter('.first .first .info-line:nth-child(2) .info-value')->text());

            if (0 === strpos(strtolower($data->city), 'escorts ')) {
                $data->city = substr($data->city, 8);
            }
        }

        $data->cash = false;
        $data->card = false;

        foreach(preg_split('/\s*,\s*/', trim($ajaxCrawler->filter('.last .last .info-line:last-child .info-value')->text())) as $paymentMethod) {
            if ('efectivo' === strtolower($paymentMethod)) {
                $data->cash = true;
            }
            if (false !== stripos($paymentMethod, 'tarjeta')) {
                $data->card = true;
            }
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapCategory(Crawler $crawler, Ad $data)
    {
        $data->category = 'escort';
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTitleAndText(Crawler $crawler, Ad $data)
    {
        $data->text = trim((string) $crawler->filter('.container .profile-description')->text());
        $data->title = trim($crawler->filter('title')->text());

        if (preg_match('/^(\d{9}\s*)-\s*(.*)\s*-\s*([\w\s]+)$/', $data->title, $match)) {
            $data->phone = trim($match[1]);
            $data->title = trim($match[2]);
            $data->city = trim($match[3]);
        }

        $data->title = ScrapUtil::cleanUtf8($data->title);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPhone(Crawler $crawler, Ad $data)
    {
        $data->phone = str_replace('tel:', '', $crawler->filter('#profile-heading .last .line30:first-child .title1 a')->attr('href'));
        $data->whatsapp = (bool) count($crawler->filter('#profile-heading .phone-hint'));
        $data->twitter = null;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPersonalData(Crawler $crawler, Ad $data)
    {
        $data->name = $crawler->filter('#profile-heading header.title1 span:first-child')->text();
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAgency(Crawler $crawler, Ad $data)
    {
    }

    /**
     * @inheritdoc
     */
    protected function doScrapLocation(Crawler $crawler, Ad $data)
    {
        $data->lat = $crawler->filter('#zona')->attr('value');
        $data->lng = $crawler->filter('#ciudad')->attr('value');
    }

    /**
     * @inheritdoc
     */
    protected function doScrapFees(Crawler $crawler, Ad $data)
    {
        $data->fee = -1.0;
        $data->fees = [];

        $crawler->filter('.last .last .info-block .info-line:not(.full)')->each(function(Crawler $row) use ($data) {
            $fee = new \stdClass();
            $fee->desc = trim($row->filter('.info-label')->text());
            $fee->fee = trim($row->filter('.info-value')->text());
            $fee->fee = (float) intval($fee->fee);

            $data->fees[] = $fee;

            if ('1 hora' === strtolower($fee->desc)) {
                $data->fee = $fee->fee;
            }
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAvailability(Crawler $crawler, Ad $data)
    {
        $data->availability = [];

        foreach(preg_split('/<br\/?>/', trim($crawler->filter('.last .first .info-line:nth-child(3) .info-value')->html())) as $line) {
            $parts = explode(' ', trim($line), 2);

            if (2 !== count($parts)) {
                continue;
            }

            list ($dayName, $timerange) = $parts;

            $entry = new \stdClass();
            $entry->day = $this->dayNameToDayNumber[strtolower($dayName)] ?? -1;

            if ('24h' === $timerange) {
                $entry->from = '00:00';
                $entry->to = '00:00';
            } else {
                $timerange = preg_split('/\s*-\s*/', $timerange, 2);
                $entry->from = trim($timerange[0]);
                $entry->to = trim($timerange[1]);
            }

            $data->availability[] = $entry;
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImages(Crawler $crawler, Ad $data)
    {
        $data->images = [];
        $data->numImages = 0;

        $crawler->filter('#images-container .image-box img')->each(function($node, $offset) use ($data) {
            $image = $this->doScrapImage($node, 0 === $offset);

            if (null !== $image) {
                $data->images[] = $image;
            }
        });

        $data->numImages = count($data->images);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImage(Crawler $node, bool $poster = false)
    {
        $src = $node->attr('data-original');

        if ('jpg' !== substr($src, -3) &&
            'jpeg' !== substr($src, -4)) {
            return null;
        }

        $path = $this->download($src);

        $media = new Media();
        $media->temp = false;
        $media->poster = $poster;
        $media->name = basename($path);
        $media->version = 'md';
        $media->path = $this->relativize($path);
        $media->type = 'image/jpeg';
        $media->size = @filesize($path);
        $media->width = $node->attr('width');
        $media->height = $node->attr('height');
        $media->uploaderIp = '127.0.0.1';

        return $media;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapVideos(Crawler $crawler, Ad $data)
    {
        $data->videos = [];
        $data->numVideos = 0;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTags(Crawler $crawler, Ad $data)
    {
        $data->tags = [];

        foreach (preg_split('/\s*,\s*/', trim($crawler->filter('.last .first .info-block .info-line:first-child .info-value')->text())) as $tag) {
            $data->tags[] = $tag;
        }

        foreach (preg_split('/\s*,\s*/', trim($crawler->filter('.last .first .info-block .info-line:last-child .info-value')->text())) as $tag) {
            $data->tags[] = $tag;
        }
    }


    protected function relativize(string $path): string
    {
        return str_replace($this->getConfig()->layout->mediaDir, '/', $path);
    }

}