<?php

namespace App\Scrap\Scrapper;

use App\Model\Ad;
use App\Model\Media;
use App\Model\Province;
use App\Scrap\ScrappingTarget;
use App\Scrap\ScrapUtil;

use Symfony\Component\DomCrawler\Crawler;

class ForosXScrapper extends AdScrapper
{

    protected $dayNameToDayNumber = [
        'lunes'     => 1,
        'martes'    => 2,
        'miercoles' => 3,
        'miércoles' => 3,
        'jueves'    => 4,
        'viernes'   => 5,
        'sabado'    => 6,
        'domingo'   => 0,
    ];

    /**
     * @inheritdoc
     */
    protected $baseUrl = 'http://www.forosx.com';

    /**
     * @inheritdoc
     */
    protected function createAd(ScrappingTarget $target): Ad
    {
        $data = parent::createAd($target);
        $data->provider = 'forosx';
        $data->postalCode = $target->getMeta()->provinceCode;

        return $data;
    }

    /**
     * @inheritdoc
     */
    protected function normalize(Ad $ad)
    {
        if (empty($ad->city)) {
            $ad->city = Province::name($ad->postalCode);
        }

        parent::normalize($ad);
    }

    /**
     * @inheritdoc
     */
    protected function doSchedule()
    {
        $this->doScheduleByProvince();
    }

    /**
     * @inheritdoc
     */
    protected function doGenerateProvincePath($provinceCode, string $slug): string
    {
        return sprintf('/escorts/in/%s', $slug);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapProvinceTargets($provinceCode, Crawler $crawler)
    {
        $crawler->filter('#content-area .view-content .views-row a.imagecache')->each(function(Crawler $node) use ($provinceCode) {
            $targetUrl = $this->baseUrl . $node->attr('href');

            $this->session->add($targetUrl, [
                'promoted' => false,
                'provinceCode' => $provinceCode,
            ]);
        });
    }


    /**
     * @inheritdoc
     */
    protected function doScrapCategory(Crawler $crawler, Ad $data)
    {
        $data->category = 'escort';
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTitleAndText(Crawler $crawler, Ad $data)
    {
        $data->title = ScrapUtil::cleanText($crawler->filter('h1')->text());
        $data->text = ScrapUtil::cleanText($crawler->filter('.content .container-fluid .well')->text());
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPhone(Crawler $crawler, Ad $data)
    {
        $data->phone = preg_replace('/\s+/', '', $crawler->filter('#telfcomplet a')->text());
        $data->twitter = null;
        $data->whatsapp = true;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapPersonalData(Crawler $crawler, Ad $data)
    {
        $data->category = 'escort';
        $data->name = ScrapUtil::cleanText($crawler->filter('.content .container-fluid h2.titol')->text());
        $data->numHits = (int) trim($crawler->filterXPath('//*[@id="content-area"]/div/div[1]/div[2]/div[1]/div')->text());
        $data->ethnicity = null;
        $data->origin = null;
        $data->occupation = null;
        $data->schooling = null;
        $data->languages = null;
        $data->hairColor = null;
        $data->bsize = null;
        $data->height = null;
        $data->weight = null;

        $crawler->filter('.content h2.titol a')->each(function(Crawler $node) use ($data) {
            $data->name = str_replace(trim($node->text()), '', $data->name);
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAgency(Crawler $crawler, Ad $data)
    {
    }

    /**
     * @inheritdoc
     */
    protected function doScrapLocation(Crawler $crawler, Ad $data)
    {
        $data->city = '';
        $data->zone = '';
    }

    /**
     * @inheritdoc
     */
    protected function doScrapFees(Crawler $crawler, Ad $data)
    {
        $data->fee = floatval($crawler->filter('.tabletar .tags_escort')->text());
        $data->fees = [];

        $crawler->filter('.tabletar tr')->each(function(Crawler $row) use ($data) {
            $fee = new \stdClass();
            $fee->desc = trim($row->filter('td:first-child')->text());
            $fee->fee = floatval(trim($row->filter('td:last-child')->text()));

            $data->fees[] = $fee;
        });
    }

    /**
     * @inheritdoc
     */
    protected function doScrapAvailability(Crawler $crawler, Ad $data)
    {
        $data->availability = [];

        $node = $crawler->filterXPath('//*[@id="content-area"]/div/div[1]/div[2]/div[9]');

        if (! $node->count()) {
            return;
        }

        foreach (preg_split('/<br\/?><br\/?>/', $node->html()) as $pair) {
            list ($day, $timerange) = preg_split('/<br\/?>/', $pair, 2);

            $parts = preg_split('/ a /', $timerange, 2);

            if (count($parts) < 2) {
                continue;
            }

            list ($from, $to) = $parts;

            $from = $this->parseHour(str_replace('de ', '', strtolower($from)));
            $to = $this->parseHour($to);

            switch (strtolower(trim($day))) {
                case 'laborables':
                    for ($dayNo = 1; $dayNo <= 5; $dayNo ++) {
                        $entry = new \stdClass();
                        $entry->day = $dayNo;
                        $entry->from = $from;
                        $entry->to = $to;
                    }
                    break;
                case 'fines de semana':
                    $entry = new \stdClass();
                    $entry->day = 6;
                    $entry->from = $from;
                    $entry->to = $to;

                    $entry = new \stdClass();
                    $entry->day = 0;
                    $entry->from = $from;
                    $entry->to = $to;

                    break;
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImages(Crawler $crawler, Ad $data)
    {
        $data->images = [];
        $data->numImages = 0;

        $crawler->filter('.content .img-responsive')->each(function($node, $offset) use ($data) {
            $image = $this->doScrapImage($node, 0 === $offset);

            if (null !== $image) {
                $data->images[] = $image;
            }
        });

        $data->numImages = count($data->images);
    }

    /**
     * @inheritdoc
     */
    protected function doScrapImage(Crawler $node, bool $poster = false)
    {
        $src = $node->attr('src');

        if ('jpg' !== substr($src, -3) &&
            'jpeg' !== substr($src, -4)) {
            return null;
        }

        $path = $this->download($src);

        $media = new Media();
        $media->temp = false;
        $media->poster = $poster;
        $media->name = basename($path);
        $media->version = 'md';
        $media->path = $this->relativize($path);
        $media->type = 'image/jpeg';
        $media->size = @filesize($path);
        $media->width = $node->attr('width');
        $media->height = $node->attr('height');
        $media->uploaderIp = '127.0.0.1';

        return $media;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapVideos(Crawler $crawler, Ad $data)
    {
        $data->videos = [];
        $data->numVideos = 0;
    }

    /**
     * @inheritdoc
     */
    protected function doScrapTags(Crawler $crawler, Ad $data)
    {
        $crawler->filter('.content .container-fluid .tags_escort')->each(function(Crawler $node) use ($data) {
           $href = $node->attr('href');
           $text = $node->text();

           if ('efectivo' === trim(strtolower($text))) {
               $data->cash = true;

           } else if ('tarjeta de ' === trim(strtolower(substr($text, 0, 11)))) {
               $data->card = true;

           } else if (preg_match('/\/agencia\/([a-z0-9-]+)$/', $href)) {
               $data->agency = $text;
               $data->independent = false;

           } else if (preg_match('/^\s*(\d{2,})\s+a(ñ|n)os\s*$/', $text) && empty($this->age) && ((int) $text) > 0 && ((int) $text) < 50) {
               $data->age = (int) $text;

           } else if (preg_match('/\/all\/(\d{2,})$/', $href) && empty($this->age) && ((int) $text) > 0 && ((int) $text) < 50) {
               $data->age = (int) $text;

           } else if (preg_match('/\/all\/all\/(\w+)$/', $href) && ! $data->origin) {
               $data->origin = $text;
           }
        });
    }


    protected function parseHour($hour)
    {
        if (preg_match('/(\d+)\s*h/', $hour, $match)) {
            return $match[1] .':00';
        } else {
            return intval($hour).':00';
        }
    }


    protected function relativize(string $path): string
    {
        return str_replace($this->getConfig()->layout->mediaDir, '/', $path);
    }

}