<?php

namespace App\Scrap;

final class ScrappingTargetStatus
{
    const NONE = 0;
    const SCRAP_FAILED = 1;
    const SCRAP_INVALID_DATA = 2;
    const SCRAP_VALID_DATA = 4;
    const PERSIST_ALREADY_EXIST = 8;
    const PERSIST_FAILED = 16;
    const PERSISTED = 32;
    const PUBLISH_FAILED = 64;
    const PUBLISHED = 128;
}