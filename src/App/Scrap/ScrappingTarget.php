<?php

namespace App\Scrap;

use stdClass;

class ScrappingTarget
{

    /**
     * @var string
     */
    protected $url;

    /**
     * @internal
     *
     * @var mixed
     */
    protected $meta;

    /**
     * @internal
     *
     * @var mixed
     */
    protected $data;

    /**
     * @internal
     *
     * @var bool
     */
    protected $scrapped;

    /**
     * @internal
     *
     * @var bool
     */
    protected $fromCache;

    /**
     * @internal
     *
     * @var int
     */
    protected $status;

    /**
     * @internal
     *
     * @var ScrappingDownload[]
     */
    protected $downloads;

    /**
     * @internal
     *
     * @var bool
     */
    protected $ocr;


    public function __construct(string $url)
    {
        $this->url = $url;
        $this->meta = new stdClass();
        $this->data = new stdClass();
        $this->scrapped = false;
        $this->fromCache = false;
        $this->status = ScrappingTargetStatus::NONE;
        $this->downloads = [];
        $this->ocr = false;
    }

    public function __toString()
    {
        return $this->url;
    }


    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMeta()
    {
        return $this->meta;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setMeta($key, $value = null)
    {
        if (null === $value && $key instanceof stdClass) {
            $this->meta = $key;
        } else {
            $this->meta->$key = $value;
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function getInvalidFields()
    {
        $messages = [];

        if (empty($this->data->title)) {
            $messages[] = 'Empty title';
        } else if (strlen($this->data->title) < 3) {
            $messages[] = 'Title too short';
        }

        if (empty($this->data->name)) {
            $messages[] = 'Empty name';
        } else if (strlen($this->data->name) < 3) {
            $messages[] = 'Name too short';
        }

        if (empty($this->data->phone)) {
            $messages[] = 'Empty phone';
        } else if (strlen($this->data->phone) < 9) {
            $messages[] = 'Phone too short';
        } else if ('000000000' === $this->data->phone) {
            $messages[] = 'Phone is 000000000';
        }

        if (empty($this->data->postalCode)) {
            $messages[] = 'Empty postal code';
        } else if (strlen($this->data->postalCode) < 2) {
            $messages[] = 'Postal code too short';
        }

        return implode(', ', $messages);
    }

    public function getDownloads(): array
    {
        return $this->downloads;
    }

    public function setData($data, bool $fromCache = false)
    {
        $this->data = $data;
        $this->fromCache = $fromCache;
        $this->scrapped = true;

        if (empty($data->title) || strlen($data->title) < 3 ||
            empty($data->name) || strlen($data->name) < 3 ||
            empty($data->phone) || $data->phone === '000000000' ||
            empty($data->postalCode) || strlen($data->postalCode) < 2) {
            $this->addStatus(ScrappingTargetStatus::SCRAP_INVALID_DATA);
        } else {
            $this->addStatus(ScrappingTargetStatus::SCRAP_VALID_DATA);
        }
    }

    public function setOcr(bool $ocr)
    {
        $this->ocr = $ocr;
    }

    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    public function addDownload(string $src, string $path, int $status)
    {
        $this->downloads[] = new ScrappingDownload($src, $path, $status);
    }

    public function addStatus(int $status)
    {
        $this->status |= $status;
    }

    public function isWorthProcessing(): bool
    {
        return $this->isScrapValidData();
    }

    public function isScrapped(): bool
    {
        return $this->scrapped;
    }

    public function isFromCache(): bool
    {
        return $this->fromCache;
    }

    public function isScrapFailed(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::SCRAP_FAILED);
    }

    public function isScrapInvalidData(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::SCRAP_INVALID_DATA);
    }

    public function isScrapValidData(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::SCRAP_VALID_DATA);
    }

    public function isPersistFailed(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::PERSIST_FAILED);
    }

    public function isPersistAlreadyExists(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::PERSIST_ALREADY_EXIST);
    }

    public function isPersisted(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::PERSISTED);
    }

    public function isPublishFailed(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::PUBLISH_FAILED);
    }

    public function isPublished(): bool
    {
        return $this->testStatus(ScrappingTargetStatus::PUBLISHED);
    }

    public function isOcr(): bool
    {
        return $this->ocr;
    }


    public function toObject(): stdClass
    {
        $obj = new stdClass();
        $obj->url = $this->url;
        $obj->meta = $this->meta;
        $obj->data = $this->data;
        $obj->scrapped = $this->scrapped;
        $obj->status = $this->status;

        // Phalcon won't let this to be copied
        $obj->experiences = $this->data->experiences;
        $obj->images = $this->data->images;
        $obj->videos = $this->data->videos;
        $obj->tags = $this->data->tags;

        return $obj;
    }


    protected function testStatus(int $status): bool
    {
        return $status === ($status & $this->status);
    }
}
