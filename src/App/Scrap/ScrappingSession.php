<?php

namespace App\Scrap;

class ScrappingSession implements \Countable
{

    /**
     * @var ScrappingTarget[]
     */
    protected $targets;


    public function __construct()
    {
        $this->targets = [];
    }


    /**
     * @param string $url
     * @param array $meta
     *
     * @return ScrappingSession
     */
    public function add(string $url, array $meta = []): ScrappingSession
    {
        $target = new ScrappingTarget($url);

        foreach ($meta as $key => $value) {
            $target->setMeta($key, $value);
        }

        $this->targets[] = $target;

        return $this;
    }

    /**
     * @return ScrappingTarget[]
     */
    public function getTargets(): array
    {
        return $this->targets;
    }


    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->targets);
    }

}