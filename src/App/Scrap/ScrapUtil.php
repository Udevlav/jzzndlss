<?php

namespace App\Scrap;

final class ScrapUtil
{

    static public function cleanAscii7b($input): string
    {
        return empty($input) ? '' : preg_replace('/[\x00-\x1F\x7F-\xFF]/', '',(string) $input);
    }

    static public function cleanAscii8b($input): string
    {
        return empty($input) ? '' : preg_replace('/[\x00-\x1F\x7F]/', '', (string) $input);
    }

    static public function cleanUtf8($input): string
    {
        if (empty($input)) {
            return '';
        }

        if (strlen($input) != strlen(utf8_decode($input))) {
            $replacementsDone = [];

            // Strip overly long 2 byte sequences, as well as characters
            // above U+10000 and replace with $replace_text
            $input = preg_replace(
                '/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
                '|[\x00-\x7F][\x80-\xBF]+' .
                '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
                '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
                '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
                '',
                $input,
                -1, $replacementsDone[]
            );

            // Strip overly long 3 byte sequences and UTF-16 surrogates
            // and replace with $replace_text
            $input = preg_replace(
                '/\xE0[\x80-\x9F][\x80-\xBF]' .
                '|\xED[\xA0-\xBF][\x80-\xBF]/S',
                '',
                $input,
                -1,
                $replacementsDone[]
            );

            if (array_sum($replacementsDone) > 0) {
                $message = 'Input contains unsupported characters';
            }
        }

        return $input;
    }

    static public function cleanText($input): string
    {
        $input = trim(strval($input));

        if (empty($input)) {
            return '';
        }

        $input = str_replace("\r\n", '', $input);
        $input = preg_replace('/(\r|\n|\t)/', '', $input);
        $input = preg_replace('/\s+/', ' ', $input);
        $input = preg_replace('/([\.]{3,})/', '...', $input);

        return $input;
    }

    static public function capitalize(string $input): string
    {
        return ucfirst(strtolower($input));
    }

    static public function phraseize(string $input): string
    {
        $parts = [];

        foreach (preg_split('/\s*\.\s*/', $input) as $part) {
            $parts[] = ucfirst(strtolower(static::cleanUtf8(trim($part))));
        }

        return implode('. ', $parts);
    }
}