<?php

namespace App\Scrap;

class ScrappingDownload
{

    /**
     * @var string
     */
    private $src;

    /**
     * @var string
     */
    private $path;

    /**
     * @var int
     */
    private $status;


    /**
     * ScrappingDownload constructor.
     *
     * @param string $src
     * @param string $path
     * @param int    $status
     */
    public function __construct(string $src, string $path, int $status)
    {
        $this->src = $src;
        $this->path = $path;
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

}
