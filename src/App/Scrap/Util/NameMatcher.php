<?php

namespace App\Scrap\Util;

use InvalidArgumentException;

/**
 * Tries to match a valid name from text.
 *
 * This implementation combines some heuristic with two name lists: a whitelist
 * and a blacklist.
 */
class NameMatcher
{

    /**
     * @var array
     */
    static protected $commonStartings = [
        '/so(?:y|i)\s+([^\s]+)/i',
        '/llam(?:a|ar|o)\s+([^\s]+)/i',
        '/mi\s+nombre\s+es\s+([^\s]+)/i',
        '/([^\s]+)\s+es\s+una\s+/i',
        '/pregunta\s+por\s+([^\s]+)/i',
        '/novedad\s+([^\s]+)/i',
    ];

    /**
     * @var array
     */
    static protected $filteredNames = [
        'a',
        'ante',
        'cabe',
        'con',
        'de',
        'desde',
        'en',
        'el',
        'els',
        'ellos',
        'es',
        'la',
        'las',
        'luego',
        'mia',
        'mias',
        'mio',
        'mios',
        'por',
        'un',
        'una',
        'uno',
        'unas',
        'unos',
    ];

    /**
     * @var array
     */
    static protected $nonNames = [
        'belleza',
        'bellezon',
        'bellezón',
        'caribeña',
        'japonesa',
        'jubilada',
        'negra',
        'negrita',
        'madura',
        'madurita',
        'masaje',
        'morena',
        'morenita',
        'morenaza',
        'mulata',
        'mulatita',
        'rubia',
        'rubita',
    ];

    /**
     * Returns the absolute path for the default names file (a CSV file
     * containing the list of what is considered a "valid" name).
     *
     * This can be extended to allow other locales...
     *
     * @return string
     */
    static public function defaultNamesFile(): string
    {
        return dirname(dirname(dirname(dirname(__DIR__)))) .'/var/txt/names.csv';
    }

    /**
     * The list of whitelisted names.
     *
     * @var string[]
     */
    private $whitelist;

    /**
     * The list of blacklisted names.
     *
     * @var string[]
     */
    private $blacklist;

    /**
     * NameMatcher constructor.
     *
     * @param string[] | string | null $whitelist
     *   the list of names, a CSV file which contains the name list, or null
     *   to use the default name list
     *
     * @param string[] | string | null $blacklist
     *   the list of names, a CSV file which contains the name list, or null
     *   to use the default name list
     *
     * @throws InvalidArgumentException
     */
    public function __construct($whitelist = null, $blacklist = null)
    {
        if (null === $whitelist) {
            $whitelist = static::defaultNamesFile();
        }

        if (null === $blacklist) {
            $blacklist = static::$filteredNames;
        }

        if (is_array($whitelist) && $whitelist) {
            $this->whitelist = $whitelist;
        } else if (is_string($whitelist)) {
            $this->whitelist = $this->load($whitelist);
        } else {
            throw new InvalidArgumentException(sprintf('The whitelist should be a non-empty array of names, a CSV file path or null (%s given)', gettype($whitelist)));
        }

        $this->whitelist = array_merge($this->whitelist, static::$nonNames);

        if (is_array($blacklist) && $blacklist) {
            $this->blacklist = $blacklist;
        } else if (is_string($blacklist)) {
            $this->blacklist = $this->load($blacklist);
        } else {
            throw new InvalidArgumentException(sprintf('The blacklist should be a non-empty array of names, a CSV file path or null (%s given)', gettype($blacklist)));
        }
    }


    /**
     * Tests whether the given name is valid.
     *
     * In the default implementation, a name is considered valid iff it
     * matches a name in the whitelist and does not match a list from the
     * blacklist.
     *
     * @param string $name
     *
     * @return bool
     */
    public function valid(string $name): bool
    {
        return (
              in_array(strtolower($name), $this->whitelist, true) &&
            ! in_array(strtolower($name), $this->blacklist, true)
        );
    }

    /**
     * Tries to match a valid name.
     *
     * The default implementation first normalizes the text to process. Common
     * starting patterns are checked first, then an exhaustive list search is
     * performed.
     *
     * @param string | null $text
     *
     * @return mixed | string | null
     */
    public function match(string $text = null)
    {
        $text = $this->normalize($text);

        if (empty($text)) {
            return null;
        }

        if (null !== $name = $this->matchCommonStartings($text)) {
            return $this->format($name);
        }

        if (null !== $name = $this->matchList($text)) {
            return $this->format($name);
        }

        return null;
    }


    /**
     * Tries to find a name by matching common starting patterns.
     *
     * @param string $text the normalized, non-empty text
     *
     * @return string | null
     */
    protected function matchCommonStartings(string $text)
    {
        // Note: first match wins
        foreach (static::$commonStartings as $pattern) {
            if (preg_match($pattern, $text, $match) && $this->valid($match[1])) {
                return $match[1];
            }
        }

        return null;
    }

    /**
     * Tries to find a name match in the given text by performing an exhaustive
     * search. The basic idea is:
     *
     * 1. Let candidates be an empty list
     * 2. For each name in the whitelist try to find a match in the text; if the
     *    match is found, then add it to the list of candidates
     * 3. Reduce the list of candidates, filtering out any blacklisted name
     *
     * (4. Apply some heuristic to decide which candidate on the list to choose)
     *
     * @param string $text the normalized, non-empty text
     *
     * @return string | null
     */
    protected function matchList(string $text)
    {
        $candidates = [];

        foreach ($this->whitelist as $name) {
            if (preg_match('/(\s+|\.|,|^)'.$name.'(\s+|\.|,|$)/i', $text)) {
                $candidates[] = $name;
            }
        }

        $candidates = array_filter($candidates, [$this, 'valid']);

        if (! $candidates) {
            return null;
        }

        return $this->choose($candidates);
    }


    /**
     * Concrete sub-classes may override this method in order to change the
     * default candidate selection choice. By default, this implementation
     * just chooses the first match.

     * @param array $candidates
     *   the list of candidate names; this list is guaranteed to be non-empty
     *   and contains only valid names
     *
     * @return string | null
     */
    protected function choose(array $candidates)
    {
        return $candidates[0] ?? null;
    }


    /**
     * @param string $name
     *
     * @return string
     */
    private function format(string $name): string
    {
        $parts = [];

        foreach (preg_split('/\s+/', trim($name)) as $part) {
            $parts[] = ucfirst($part);
        }

        return implode(' ', $parts);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function normalize(string $name): string
    {
        $parts = [];

        foreach (preg_split('/\s+/', trim($name)) as $part) {
            $parts[] = strtolower($part);
        }

        return implode(' ', $parts);
    }

    /**
     * @param string $csvFile
     *
     * @return string[]
     *
     * @throws InvalidArgumentException
     */
    private function load(string $csvFile): array
    {
        if (! file_exists($csvFile)) {
            throw new InvalidArgumentException(sprintf('Names file "%s" does not exists.', $csvFile));
        }

        if (! is_readable($csvFile)) {
            throw new InvalidArgumentException(sprintf('Names file "%s" is not readable.', $csvFile));
        }

        $list = [];

        if (false === ($fd = fopen($csvFile, 'r'))) {
            throw new InvalidArgumentException(sprintf('Failed to open whitelist file "%s".', $csvFile));
        }

        $heading = true;

        while (false !== ($data = fgetcsv($fd, 1024))) {
            if (! $heading) {
                foreach (explode(' ', $this->normalize($data[0])) as $name) {
                    $list[] = $name;
                }
            } else {
                $heading = false;
            }
        }

        return array_unique($list);
    }

}