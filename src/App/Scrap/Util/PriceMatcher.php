<?php

namespace App\Scrap\Util;

class PriceMatcher
{

    const SEARCH = ['EUjjRO', 'euro', 'euros', 'hora', 'horas', 'E'];
    const PRICES = [40, 50, 60, 70, 75, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 200, 210, 220, 230, 240, 250];
    const PRICES2 = [20, 30];

    protected $preuminim;

    public function __construct()
    {
        $this->preuminim = null;
    }

    public function match(string $cadena)
    {
        $cadena = str_replace('€', 'EUjjRO', $cadena);

        foreach (self::SEARCH as $valor) {
            $fpos = stripos($cadena, $valor);

            if ($fpos != false) {
                preg_match_all('!\d+!', $cadena, $matches);

                $matches = array_map(function ($value) {
                    return intval($value);
                }, $matches[0]);

                foreach (self::PRICES as $valor) {
                    if (in_array($valor, $matches) && ($valor < $this->preuminim || $this->preuminim == null)) {
                        $this->preuminim = $valor;
                    }
                }

                if ($this->preuminim == null) {
                    foreach (self::PRICES2 as $valor) {
                        if (in_array($valor, $matches) && ($valor < $this->preuminim || $this->preuminim == null)) {
                            $this->preuminim = $valor;
                        }
                    }
                }

                break;
            }
        }

        return $this->preuminim;
    }
}
