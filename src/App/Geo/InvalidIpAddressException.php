<?php

namespace App\Geo;

class InvalidIpAddressException extends \InvalidArgumentException
{


    /**
     * @param mixed $source
     *
     * @return \App\Geo\InvalidIpAddressException
     */
    static public function from($source): InvalidIpAddressException
    {
        return new static(sprintf('Invalid IP address "%s".', $source));
    }
}