<?php

namespace App\Geo;

use App\Model\Province;

class GeoLocationToProvince
{

    static public function getProvince(GeoLocation $geoLocation)
    {
        $postalCode = static::getPostalCode($geoLocation);

        if (null !== $postalCode && is_numeric($postalCode)) {
            return Province::slug($postalCode);
        }

        return null;
    }

    static public function getPostalCode(GeoLocation $geoLocation)
    {
        if (null !== ($postalCode = static::tryFrom($geoLocation->getPostalCode()))) {
            return $postalCode;
        }

        if (null !== ($postalCode = static::tryFrom($geoLocation->getRegion()))) {
            return $postalCode;
        }

        if (null !== ($postalCode = static::tryFrom($geoLocation->getCity()))) {
            return $postalCode;
        }

        return null;
    }

    static private function tryFrom($that)
    {
        if (! empty($that) && null !== ($postalCode = Province::postalCode($that))) {
            return $postalCode;
        }

        return null;
    }
}