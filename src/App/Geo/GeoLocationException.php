<?php

namespace App\Geo;

class GeoLocationException extends \RuntimeException
{


    /**
     * @param \App\Geo\GeoLocationException[] $chain
     *
     * @return \App\Geo\GeoLocationException
     */
    static public function chain(array $chain): GeoLocationException
    {
        $e = null;

        while (count($chain)) {
            $cause = array_shift($chain);

            if (null === $e) {
                $e = $cause;
            } else {
                $e = new static($cause->getMessage(), $cause->getCode(), $e);
            }
        }

        return $e;
    }
}