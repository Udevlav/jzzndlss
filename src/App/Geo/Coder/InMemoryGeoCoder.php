<?php

namespace App\Geo\Coder;

use App\Geo\GeoCoder;
use App\Geo\GeoCoderException;
use App\Geo\LatLng;

/**
 * A geocoder implementation which uses an in-memory array to resolve
 * requests.
 */
class InMemoryGeoCoder implements GeoCoder
{

    /**
     * @var array<string, LatLng>
     */
    private $geoCodes;

    /**
     * InMemoryGeoCoder constructor.
     *
     * @param array $geoCodes
     */
    public function __construct(array $geoCodes)
    {
        $this->geoCodes = $geoCodes;
    }

    /**
     * @inheritdoc
     */
    public function lookup(string $address): LatLng
    {
        $postalCode = $this->getPostalCode($address);

        if (isset($this->geoCodes[$postalCode])) {
            return $this->geoCodes[$postalCode];
        }

        $provinceCode = substr($postalCode, 0, 2);

        if (isset($this->geoCodes[$provinceCode])) {
            return $this->geoCodes[$provinceCode];
        }

        throw new GeoCoderException(sprintf('Could not resolve LatLng for address "%s"', $address));
    }

    /**
     * @param string $address
     *
     * @return string
     */
    protected function getPostalCode(string $address): string
    {
        // TODO
    }
}
