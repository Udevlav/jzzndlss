<?php

namespace App\Geo\Coder;

use App\Geo\GeoCoder;
use App\Geo\GeoCoderException;
use App\Geo\LatLng;

use InvalidArgumentException;
use RuntimeException;
use Throwable;

/**
 * @see http://freegeoip.net/
 */
class GoogleGeoCoder implements GeoCoder
{

    /**
     * @var string
     */
    const BASEURL = 'https://maps.googleapis.com/maps/api/geocode/json';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * GoogleGeoCoder constructor.
     *
     * @param string $apiKey
     *
     * @throws InvalidArgumentException on invalid API key
     */
    public function __construct(string $apiKey)
    {
        if (empty($apiKey)) {
            throw new InvalidArgumentException('The Google GeoCoder API key should not be empty.');
        }

        if (! ini_get('allow_url_fopen')) {
            throw new RuntimeException('The GoogleGeoCoder requires the "allow_url_fopen" PHP directive to be set.');
        }

        $this->apiKey = $apiKey;
    }

    /**
     * @inheritdoc
     */
    public function lookup(string $address): LatLng
    {
        $url = $this->getUrl($address);

        if (false === ($response = file_get_contents($url))) {
            throw new GeoCoderException(sprintf('Failed to fetch service response.'));
        }

        try {
            $data = json_decode($response, true);
        } catch (Throwable $t) {
            throw new GeoCoderException(sprintf('Failed to decode service response.'));
        }

        switch ($data['status']) {
            case 'OK':
                // Success !
                break;
            case 'ZERO_RESULTS':
                throw new GeoCoderException(
                    'The geocoding operation was successful but returned '.
                    'no results. This may occur if the geocoder was passed a '.
                    'non-existent address'
                );
            case 'OVER_DAILY_LIMIT':
                throw new GeoCoderException(
                    'Either the API key is missing or invalid or a self-'.
                    'imposed usage cap has been exceeded.'
                );
            case 'OVER_QUERY_LIMIT':
                throw new GeoCoderException('Quota exceeded.');
            case 'REQUEST_DENIED':
                throw new GeoCoderException('Request denied.');
            case 'INVALID_REQUEST':
                throw new GeoCoderException('Invalid geocoding request.');
            case 'UNKNOWN_ERROR':
                throw new GeoCoderException(
                    'The request could not be processed fue to a server'.
                    'error. The request may succeed if you try again.'
                );
        }

        try {
            $lat = $data['results'][0]['geometry']['location']['lat'];
            $lng = $data['results'][0]['geometry']['location']['lng'];

        } catch (Throwable $t) {
            throw new GeoCoderException(
                'Failed to retrieve geocoding response data, after the '.
                'response has been evaluated as valid.'
            );
        }

        return new LatLng($lat, $lng);
    }

    /**
     * @param string $address
     *
     * @return string
     */
    protected function getUrl(string $address): string
    {
        return sprintf('%s?address=%s&key=%s', self::BASEURL, urlencode($address), $this->apiKey);
    }
}