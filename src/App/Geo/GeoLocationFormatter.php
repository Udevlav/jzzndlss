<?php

namespace App\Geo;

use InvalidArgumentException;

class GeoLocationFormatter
{

    /**
     * Shortcut to "%i / %z %h, %r (%R), %c (%C)".
     *
     * @var string
     */
    const FULL = 'f';

    /**
     * @var string
     */
    const IP = 'i';

    /**
     * @var string
     */
    const COUNTRY = 'c';

    /**
     * @var string
     */
    const COUNTRY_CODE = 'C';

    /**
     * @var string
     */
    const REGION = 'r';

    /**
     * @var string
     */
    const REGION_CODE = 'R';

    /**
     * @var string
     */
    const CITY = 'h';

    /**
     * @var string
     */
    const POSTAL_CODE = 'z';

    /**
     * @var string
     */
    const LAT = 'a';

    /**
     * @var string
     */
    const LNG = 'o';

    /**
     * @var string
     */
    const TIMEZONE = 't';


    /**
     * @var GeoLocation
     */
    private $geolocation;

    /**
     * @param GeoLocation $geolocation
     */
    public function __construct(GeoLocation $geolocation)
    {
        $this->geolocation = $geolocation;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function format(string $format): string
    {
        $output = '';

        $offset = 0;
        $length = strlen($format);

        while ($offset < $length) {
            $char = $format[$offset ++];

            if ('%' === $char) {
                $char = $format[$offset ++];

                switch ($char) {
                    case self::FULL:
                        $output .= $this->format('%i, %z %h, %r (%R), %c (%C)');
                        break;

                    case self::IP:
                        $output .= $this->geolocation->getIp() ?? '';
                        break;

                    case self::COUNTRY_CODE:
                        $output .= $this->geolocation->getCountryCode() ?? '';
                        break;

                    case self::COUNTRY:
                        $output .= $this->geolocation->getCountry() ?? '';
                        break;

                    case self::REGION_CODE:
                        $output .= $this->geolocation->getRegionCode() ?? '';
                        break;

                    case self::REGION:
                        $output .= $this->geolocation->getRegion() ?? '';
                        break;

                    case self::CITY:
                        $output .= $this->geolocation->getCity() ?? '';
                        break;

                    case self::POSTAL_CODE:
                        $output .= $this->geolocation->getPostalCode() ?? '';
                        break;

                    case self::LAT:
                        $output .= $this->geolocation->getLat() ?
                            number_format($this->geolocation->getLat()) : '';
                        break;

                    case self::LNG:
                        $output .= $this->geolocation->getLng() ?
                            number_format($this->geolocation->getLng()) : '';
                        break;

                    default:
                        throw new InvalidArgumentException(sprintf('Invalid geolocation format "%s": invalid placeholder %%%s.', $format, $char));
                }
            } else {
                $output .= $char;
            }
        }

        return $output;
    }
}