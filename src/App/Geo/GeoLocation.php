<?php

namespace App\Geo;

use App\Model\Province;
use InvalidArgumentException;

class GeoLocation
{

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string | null
     */
    private $countryCode;

    /**
     * @var string | null
     */
    private $country;

    /**
     * @var string | null
     */
    private $regionCode;

    /**
     * @var string | null
     */
    private $region;

    /**
     * @var string | null
     */
    private $city;

    /**
     * @var string | null
     */
    private $postalCode;

    /**
     * @var float | null
     */
    private $lat;

    /**
     * @var float | null
     */
    private $lng;

    /**
     * @var string | null
     */
    private $timeZone;


    /**
     * GeoLocation constructor.
     *
     * @param string $ip
     * @param string | null $countryCode
     * @param string | null $country
     * @param string | null $regionCode
     * @param string | null $region
     * @param string | null $city
     * @param string | null $postalCode
     * @param float | null $lat
     * @param float | null $lng
     * @param string | null $timeZone
     */
    public function __construct(string $ip, string $countryCode = null, string $country = null, string $regionCode = null, string $region = null, string $city = null, string $postalCode = null, float $lat = null, float $lng = null, string $timeZone = null)
    {
        $this->ip = $ip;
        $this->countryCode = $countryCode;
        $this->country = $country;
        $this->regionCode = $regionCode;
        $this->region = $region;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->lat = $lat;
        $this->lng = $lng;
        $this->timeZone = $timeZone;
}

    public function __toString()
    {
        return $this->format('%f');
    }


    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @return string | null
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @return string | null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string | null
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * @return string | null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return string | null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string | null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return float | null
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @return float | null
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @return string | null
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param string $ip
     *
     * @return GeoLocation
     */
    public function setIp(string $ip): GeoLocation
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @param null|string $countryCode
     *
     * @return GeoLocation
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @param null|string $country
     *
     * @return GeoLocation
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param null|string $regionCode
     *
     * @return GeoLocation
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;

        return $this;
    }

    /**
     * @param null|string $region
     *
     * @return GeoLocation
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @param null|string $city
     *
     * @return GeoLocation
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param null|string $postalCode
     *
     * @return GeoLocation
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @param float|null $lat
     *
     * @return GeoLocation
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @param float|null $lng
     *
     * @return GeoLocation
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @param null|string $timeZone
     *
     * @return GeoLocation
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;

        return $this;
    }


    /**
     * @param string $format
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function format(string $format): string
    {
        return (new GeoLocationFormatter($this))->format($format);
    }
}