<?php

namespace App\Geo;

interface GeoCoder
{

    /**
     * @param string $address the address to lookup
     *
     * @return LatLng the geocoded address
     *
     * @throws GeoCoderException on error
     */
    public function lookup(string $address): LatLng;
}