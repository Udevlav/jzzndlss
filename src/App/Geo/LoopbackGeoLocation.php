<?php

namespace App\Geo;

final class LoopbackGeoLocation extends GeoLocation
{

    public function __construct()
    {
        parent::__construct('127.0.0.1');
    }

    public function __toString()
    {
        return '127.0.0.1 (loopback)';
    }

    public function getCountryCode()
    {
        return '00';
    }

    public function getCountry()
    {
        return 'Loopback';
    }

    public function getRegionCode()
    {
        return '00';
    }

    public function getRegion()
    {
        return 'Loopback';
    }

    public function getCity()
    {
        return 'Loopback';
    }

    public function getPostalCode()
    {
        return '00000';
    }

    public function getLat()
    {
        return 0.0;
    }

    public function getLng()
    {
        return 0.0;
    }

    public function getTimeZone()
    {
        return 'UTC';
    }
}