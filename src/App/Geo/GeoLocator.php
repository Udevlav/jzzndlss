<?php

namespace App\Geo;

interface GeoLocator
{

    /**
     * @param string $ip the IP address to lookup
     *
     * @return \App\Geo\GeoLocation the lookup result
     *
     * @throws \App\Geo\InvalidIpAddressException on invalid IP address
     * @throws \App\Geo\GeoLocationException on error
     */
    public function lookup(string $ip): GeoLocation;
}