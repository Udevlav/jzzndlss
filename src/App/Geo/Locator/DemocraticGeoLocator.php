<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocationException;
use App\Geo\LoopbackGeoLocation;

class DemocraticGeoLocator extends DelegatingGeoLocator
{

    public function lookup(string $ip): GeoLocation
    {
        $this->addDefaults();
        $this->assertValidIp($ip);

        if ($this->isLoopback($ip)) {
            return new LoopbackGeoLocation();
        }

        $results = [];
        $chain = [];

        foreach ($this->delegates as $delegate) {
            try {
                if (null !== ($geoLocation = $delegate->lookup($ip))) {
                    $results[get_class($delegate)] = $geoLocation;
                }
            } catch (GeoLocationException $e) {
                $chain[] = $e;
            }
        }

        if (count($results)) {
            return $this->combine($ip, $results);
        }

        throw GeoLocationException::chain($chain);
    }


    /**
     * @param string $ip
     * @param GeoLocation[] $results
     *
     * @return GeoLocation
     */
    protected function combine(string $ip, array $results): GeoLocation
    {
        $geolocation = new GeoLocation($ip);

        foreach ($results as $result) {
            if (null === $geolocation->getCountryCode()) {
                if (null !== ($countryCode = $result->getCountryCode())) {
                    $geolocation->setCountryCode($countryCode);
                }
            }

            if (null === $geolocation->getCountry()) {
                if (null !== ($country = $result->getCountry())) {
                    $geolocation->setCountry($country);
                }
            }

            if (null === $geolocation->getRegionCode()) {
                if (null !== ($regionCode = $result->getRegionCode())) {
                    $geolocation->setRegionCode($regionCode);
                }
            }

            if (null === $geolocation->getRegion()) {
                if (null !== ($region = $result->getRegion())) {
                    $geolocation->setRegion($region);
                }
            }

            if (null === $geolocation->getCity()) {
                if (null !== ($city = $result->getCity())) {
                    $geolocation->setCity($city);
                }
            }

            if (null === $geolocation->getPostalCode()) {
                if (null !== ($postalCode = $result->getPostalCode())) {
                    $geolocation->setPostalCode($postalCode);
                }
            }

            if (null === $geolocation->getLat()) {
                if (null !== ($lat = $result->getLat())) {
                    $geolocation->setLat($lat);
                }
            }

            if (null === $geolocation->getLng()) {
                if (null !== ($lng = $result->getLng())) {
                    $geolocation->setLng($lng);
                }
            }

            if (null === $geolocation->getTimeZone()) {
                if (null !== ($tz = $result->getTimeZone())) {
                    $geolocation->setTimeZone($tz);
                }
            }
        }

        return $geolocation;
    }
}