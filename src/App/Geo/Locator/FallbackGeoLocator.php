<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocationException;
use App\Geo\LoopbackGeoLocation;
use App\Geo\UnknownGeoLocation;
use App\Model\Province;

class FallbackGeoLocator extends DelegatingGeoLocator
{

    public function lookup(string $ip): GeoLocation
    {
        $this->addDefaults();
        $this->assertValidIp($ip);

        if ($this->isLoopback($ip)) {
            return new LoopbackGeoLocation();
        }

        foreach ($this->delegates as $delegate) {
            $geoLocation = null;

            try {
                $geoLocation = $delegate->lookup($ip);
            } catch (GeoLocationException $e) {
                continue;
            }

            if ($this->isWorthReturning($geoLocation)) {
                return $geoLocation;
            }
        }

        return new UnknownGeoLocation($ip);
    }


    /**
     * @param GeoLocation | null $geoLocation
     *
     * @return bool
     */
    protected function isWorthReturning(GeoLocation $geoLocation = null): bool
    {
        if (null === $geoLocation) {
            return false;
        }

        if ($geoLocation instanceof UnknownGeoLocation) {
            return false;
        }

        $this->resolve($geoLocation);

        return (
            ! empty($geoLocation->getCountryCode()) &&
            ! empty($geoLocation->getPostalCode())
        );
    }

    protected function resolve(GeoLocation $geoLocation)
    {
        if ('ES' !== strtoupper($geoLocation->getCountryCode())) {
            return;
        }

        if (empty($geoLocation->getPostalCode()) && ! empty($geoLocation->getCity()) && ! empty($postalCode = Province::postalCode($geoLocation->getCity()))) {
            $geoLocation->setPostalCode($postalCode);
        }

        if (empty($geoLocation->getPostalCode()) && ! empty($geoLocation->getRegion()) && ! empty($postalCode = Province::postalCodeByRegion($geoLocation->getRegion()))) {
            $geoLocation->setPostalCode($postalCode);
        }

        if (empty($geoLocation->getPostalCode()) && ! empty($geoLocation->getRegionCode()) && ! empty($postalCode = Province::postalCodeByRegionCode($geoLocation->getRegionCode()))) {
            $geoLocation->setPostalCode($postalCode);
        }
    }
}