<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocator;
use App\Geo\InvalidIpAddressException;
use App\Geo\LoopbackGeoLocation;
use App\Geo\UnknownGeoLocation;

class LoopbackLocator implements GeoLocator
{
    public function lookup(string $ip): GeoLocation
    {
        $ip = trim(strtolower($ip));

        if ('localhost' === $ip) {
            return new LoopbackGeoLocation();
        }

        if (! filter_var($ip, \FILTER_VALIDATE_IP)) {
            throw InvalidIpAddressException::from($ip);
        }

        $bytes = $this->pton($ip);

        if (4 === count($bytes)) {
            if (/* 127 */ 0x7f === $bytes[0]) {
                return new LoopbackGeoLocation();
            }

        } else if (16 === count($bytes)) {
            $test = 0x00;

            for ($offset = 0; $offset < 15; $offset ++) {
                $test |= $bytes[$offset];
            }

            if ((0x00 === $test) && (0x01 === $bytes[$offset])) {
                return new LoopbackGeoLocation();
            }
        }

        return new UnknownGeoLocation($ip);
    }

    private function pton(string $p): array
    {
        if (false === ($n = @inet_pton($p))) {
            throw InvalidIpAddressException::from($p);
        }

        return array_slice(unpack('C*', $n), 0);
    }
}