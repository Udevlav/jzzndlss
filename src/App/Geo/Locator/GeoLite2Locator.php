<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocationException;
use App\Geo\GeoLocator;
use App\Geo\InvalidIpAddressException;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

use MaxMind\Db\Reader\InvalidDatabaseException;

class GeoLite2Locator implements GeoLocator
{

    /**
     * @var \GeoIp2\Database\Reader
     */
    private $reader;

    /**
     * GeoIpLocator constructor.
     *
     * @param string $db the GeoIP database path
     * @param string[] $locales the locales
     *
     * @throws \InvalidArgumentException on invalid database
     */
    public function __construct(string $db = '/usr/local/share/GeoIP/GeoIP2-City.mmdb', array $locales = ['en'])
    {
        if (! file_exists($db)) {
            throw new \InvalidArgumentException(sprintf('The GeoIP database "%s" does not exist.', $db));
        }

        if (! is_readable($db)) {
            throw new \InvalidArgumentException(sprintf('The GeoIP database "%s" is not readable.', $db));
        }

        try {
            $this->reader = new Reader($db, $locales);
        } catch (InvalidDatabaseException $e) {
            throw new \InvalidArgumentException(sprintf('Invalid Maxming GeoIP2 database "%s".', $db));
        }
    }

    public function lookup(string $ip): GeoLocation
    {
        if (! filter_var($ip, \FILTER_VALIDATE_IP)) {
            throw InvalidIpAddressException::from($ip);
        }

        try {
            $record = $this->reader->city($ip);
        } catch (AddressNotFoundException $e) {
            throw new GeoLocationException(sprintf('Service GeoIP2 failed to lookup IP address %s.', $ip));
        }

        return new GeoLocation(
            $ip,
            $record->country->isoCode,
            $record->country->names['es'],
            $record->mostSpecificSubdivision->isoCode,
            $record->mostSpecificSubdivision->name,
            $record->city->name,
            $record->postal->code,
            $record->location->latitude,
            $record->location->longitude
        );
    }
}