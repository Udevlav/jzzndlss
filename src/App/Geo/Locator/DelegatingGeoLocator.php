<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocationException;
use App\Geo\GeoLocator;
use App\Geo\InvalidIpAddressException;
use App\Geo\LoopbackGeoLocation;

use Phalcon\Mvc\User\Component;

class DelegatingGeoLocator extends Component implements GeoLocator
{

    /**
     * @var \App\Geo\GeoLocator[]
     */
    protected $delegates;

    public function lookup(string $ip): GeoLocation
    {
        $this->addDefaults();
        $this->assertValidIp($ip);

        if ($this->isLoopback($ip)) {
            return new LoopbackGeoLocation();
        }

        $chain = [];

        foreach ($this->delegates as $delegate) {
            try {
                if (null !== ($geoLocation = $delegate->lookup($ip))) {
                    return $geoLocation;
                }
            } catch (GeoLocationException $e) {
                $chain[] = $e;
            }
        }

        throw GeoLocationException::chain($chain);
    }

    protected function addDefaults()
    {
        if (null === $this->delegates) {
            $this->delegates = [];

            $config = $this->di->get('config');

            try {
                $this->delegates[] = new GeoLite2Locator($config->geo->geoip2->db);
            } catch (\InvalidArgumentException $e) {}

            try {
                $this->delegates[] = new FreeGeoIpLocator($config->geo->freegeoip->apiKey);
            } catch (\InvalidArgumentException $e) {}

            try {
                $this->delegates[] = new IpInfoLocator();
            } catch (\InvalidArgumentException $e) {}

            try {
                $this->delegates[] = new LoopbackLocator();
            } catch (\InvalidArgumentException $e) {}

            if (0 === count($this->delegates)) {
                throw new \RuntimeException('No GeoLocator instance could be created.');
            }
        }
    }

    protected function assertValidIp(string $ip)
    {
        if (! filter_var($ip, \FILTER_VALIDATE_IP)) {
            throw InvalidIpAddressException::from($ip);
        }
    }

    protected function isLoopback(string $ip): bool
    {
        return ('127.0.0.1' === $ip);
    }
}