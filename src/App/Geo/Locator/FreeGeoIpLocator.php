<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocationException;
use App\Geo\GeoLocator;
use App\Geo\InvalidIpAddressException;

use RuntimeException;

/**
 * @see http://freegeoip.net/
 */
class FreeGeoIpLocator implements GeoLocator
{

    /**
     * @var string
     */
    private $apiKey;

    /**
     * FreeGeoIpLocator constructor.
     *
     * @param string $apiKey
     *
     * @throws RuntimeException
     *    if the "allow_url_fopen" directive is not set
     */
    public function __construct(string $apiKey)
    {
        if (! ini_get('allow_url_fopen')) {
            throw new RuntimeException('The FreeGeoIpLocator requires the "allow_url_fopen" PHP directive set.');
        }

        $this->apiKey = $apiKey;
    }

    /**
     * @inheritdoc
     */
    public function lookup(string $ip): GeoLocation
    {
        if (! filter_var($ip, \FILTER_VALIDATE_IP)) {
            throw InvalidIpAddressException::from($ip);
        }

        $url = sprintf('http://api.ipstack.com/%s?access_key=%s', $ip, $this->apiKey);

        if (false === ($response = @file_get_contents($url))) {
            throw new GeoLocationException(sprintf('Failed to fetch FreeGeoIP service response from "%s".', $url));
        }

        if (null === ($data = @json_decode($response))) {
            throw new GeoLocationException(sprintf('Failed to decode FreeGeoIP service response "%s".', $response));
        }

        return new GeoLocation(
            $data->ip,
            $data->country_code,
            $data->country_name,
            $data->region_code,
            $data->region_name,
            $data->city,
            $data->zip,
            $data->latitude,
            $data->longitude
        );
    }
}