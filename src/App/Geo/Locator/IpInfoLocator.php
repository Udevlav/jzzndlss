<?php

namespace App\Geo\Locator;

use App\Geo\GeoLocation;
use App\Geo\GeoLocationException;
use App\Geo\GeoLocator;
use App\Geo\InvalidIpAddressException;

/**
 * @see http://ipinfo.io/
 */
class IpInfoLocator implements GeoLocator
{

    /**
     * FreeGeoIpLocator constructor.
     *
     * @throws \InvalidArgumentException
     *    if the "allow_url_fopen" directive is not set
     */
    public function __construct()
    {
        if (! ini_get('allow_url_fopen')) {
            throw new \RuntimeException('The IpInfoLocator requires the "allow_url_fopen" PHP directive set.');
        }
    }

    /**
     * @inheritdoc
     */
    public function lookup(string $ip): GeoLocation
    {
        if (! filter_var($ip, \FILTER_VALIDATE_IP)) {
            throw InvalidIpAddressException::from($ip);
        }

        $url = sprintf('http://ipinfo.io/%s/json', $ip);

        if (false === ($response = @file_get_contents($url))) {
            throw new GeoLocationException(sprintf('Failed to fetch IpInfo service response from "%s".', $url));
        }

        if (null === ($data = @json_decode($response))) {
            throw new GeoLocationException(sprintf('Failed to decode IpInfo service response "%s".', $response));
        }

        if (empty($data->loc)) {
            throw new GeoLocationException(sprintf('Service IpInfo failed to lookup address %s.', $ip));
        }

        list ($lat, $lng) = explode(',', $data->loc);

        return new GeoLocation(
            $data->ip,
            $data->country,
            null,
            null,
            $data->region,
            $data->city,
            null,
            floatval($lat),
            floatval($lng)
        );
    }
}