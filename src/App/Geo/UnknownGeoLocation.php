<?php

namespace App\Geo;

final class UnknownGeoLocation extends GeoLocation
{

    public function __construct(string $ip)
    {
        parent::__construct($ip);
    }

    public function getCountryCode()
    {
        return '00';
    }

    public function getCountry()
    {
        return 'Unknown';
    }

    public function getRegionCode()
    {
        return '00';
    }

    public function getRegion()
    {
        return 'Unknown';
    }

    public function getCity()
    {
        return 'Unknown';
    }

    public function getPostalCode()
    {
        return '00000';
    }

    public function getLat()
    {
        return 0.0;
    }

    public function getLng()
    {
        return 0.0;
    }

    public function getTimeZone()
    {
        return 'UTC';
    }
}