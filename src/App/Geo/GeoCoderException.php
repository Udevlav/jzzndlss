<?php

namespace App\Geo;

use RuntimeException;

class GeoCoderException extends RuntimeException
{
}