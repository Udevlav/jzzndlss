<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Payment;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class PaymentRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Payment::class, $db, $manager, $config, $logger);
    }


    public function findOneByOrderNo(string $orderNo)
    {
        return $this->getQueryBuilder()
            ->from(['g' => Payment::class])
            ->where('g.active = 1');
    }
}