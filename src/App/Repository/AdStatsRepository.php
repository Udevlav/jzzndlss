<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\AdStats;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class AdStatsRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(AdStats::class, $db, $manager, $config, $logger);
    }

    public function findLastByAd(Ad $entity, int $days = 30)
    {
        $this->assertHasId($entity);

        $collection = $this->findBy([
            'adId='. $entity->id,
            'order' => 'measuredAt DESC',
            'limit' => 512,
        ]);

        /** @var AdStats[] $data */
        $data = [];

        foreach ($collection as $record) {
            /** @var AdStats $record */
            $key = $record->measuredAt;

            if (! isset($data[$key])) {
                $data[$key] = new AdStats();
            }

            $stats = $data[$key];
            $stats->adId = $record->adId;
            $stats->numPromotions += $record->numPromotions;
            $stats->numDaysOnTop += $record->numDaysOnTop;
            $stats->numImpressions += $record->numImpressions;
            $stats->numPhoneClicks += $record->numPhoneClicks;
            $stats->numVisits += $record->numVisits;
            $stats->measuredAt = $key;
        }

        return $data;
    }


    protected function assertHasId($model)
    {
        if (empty($model->id)) {
            throw new \InvalidArgumentException('The model ID should not be empty.');
        }
    }

}