<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\ChatMessage;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

use DateTime;
use Phalcon\Mvc\Model\Resultset;

class ChatMessageRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(ChatMessage::class, $db, $manager, $config, $logger);
    }

    public function findByAdvertiserSession(string $advertiserSessionId)
    {
        return $this->getQueryBuilder()
            ->from(['chat' => ChatMessage::class])
            ->where('chat.advertiserSid = "'. $advertiserSessionId .'"')
            ->orderBy('chat.sentAt ASC')
            ->limit(100)
            ->getQuery()
            ->execute();
    }

    public function findByChatSession(string $peerSessionId, string $advertiserSessionId)
    {
        return $this->getQueryBuilder()
            ->from(['chat' => ChatMessage::class])
            ->where(sprintf('(chat.peerSid = "%s" AND chat.advertiserSid = "%s") OR (chat.peerSid = "%s" AND chat.advertiserSid = "%s")',
                $peerSessionId,
                $advertiserSessionId,
                $advertiserSessionId,
                $peerSessionId
            ))
            ->orderBy('chat.sentAt ASC')
            ->limit(100)
            ->getQuery()
            ->execute();
    }

    public function findStartingAt(DateTime $startingAt, string $sessionId): Resultset
    {
        return $this->getQueryBuilder()
            ->from([
                'chat' => ChatMessage::class,
            ])
            ->andWhere(sprintf('chat.peerSid = "%s" OR chat.advertiserSid = "%s"', $sessionId, $sessionId))
            ->andWhere(sprintf('chat.sentAt >= "%s"', $startingAt->format('Y-m-d H:i:s')))
            ->orderBy('chat.sentAt ASC')
            ->limit(100)
            ->getQuery()
            ->execute()
            ;
    }

}