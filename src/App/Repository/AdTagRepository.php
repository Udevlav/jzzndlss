<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\AdTag;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class AdTagRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(AdTag::class, $db, $manager, $config, $logger);
    }

    public function findByAd(Ad $entity)
    {
        $this->assertHasId($entity);

        return $this->findBy('adId = '. $entity->id);
    }


    protected function assertHasId($model)
    {
        if (empty($model->id)) {
            throw new \InvalidArgumentException('The model ID should not be empty.');
        }
    }

}