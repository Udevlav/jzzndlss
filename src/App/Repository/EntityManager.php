<?php

namespace App\Repository;

use App\Config\Config;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\Model\Manager;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

class EntityManager extends Component
{

    protected $pool;

    /**
     * @var bool
     */
    protected $usePersistentDb = false;

    /**
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
        $this->pool = [];
    }

    public function isUsePersistentDb(): bool
    {
        return $this->usePersistentDb;
    }

    public function setUsePersistentDb(bool $enable)
    {
        $this->usePersistentDb = $enable;
    }

    public function getRepository(string $fullyQualifiedClassName): Repository
    {
        if (! isset($this->pool[$fullyQualifiedClassName])) {
            $db = $this->getDb();
            $manager = $this->getModelManager();
            $config = $this->getConfig();
            $logger = $this->getLogger();

            try {
                $reflector = new ReflectionClass($fullyQualifiedClassName);
                $repoClassName = sprintf('\\App\\Repository\\%sRepository', $reflector->getShortName());

                if (class_exists($repoClassName)) {
                    $this->pool[$fullyQualifiedClassName] = new $repoClassName($db, $manager, $config, $logger);
                } else {
                    $this->pool[$fullyQualifiedClassName] = new Repository($fullyQualifiedClassName, $db, $manager, $config, $logger);
                }
            } catch (ReflectionException $re) {
                throw new RuntimeException(sprintf('Failed to create reflector for %s: %s.', $fullyQualifiedClassName, $re->getMessage()), $re->getCode(), $re);
            }
        }

        return $this->pool[$fullyQualifiedClassName];
    }


    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    protected function getDb(): AdapterInterface
    {
        return $this->di->get('db');
    }

    protected function getLogger(): FileLogger
    {
        return $this->di->get('logger');
    }

    protected function getModelManager(): Manager
    {
        return $this->di->get('modelManager');
    }
}
