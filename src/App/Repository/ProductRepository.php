<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Product;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class ProductRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Product::class, $db, $manager, $config, $logger);

        // We can cache tags forever (or a long cache time, as one
        // month) as they're not expected to change ever
        $this
            ->setCacheEnabled(true)
            ->setCacheLifetime(30 * 24 * 3600);
    }


    public function findAllActive()
    {
        return $this->getQueryBuilder()
            ->from(['g' => Product::class])
            ->where('g.active = 1');
    }
}