<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\Media;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;
use Phalcon\Mvc\Model\Resultset\Simple;

class MediaRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Media::class, $db, $manager, $config, $logger);
    }


    public function findPoster(Ad $ad)
    {
        $this->assertHasId($ad);

        $result = $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('masterId IS NULL')
            ->andWhere('poster = 1')
            ->andWhere('type LIKE "image%"')
            ->getQuery()
            ->execute();

        if ($result instanceof Simple) {
            return $result->getFirst();
        }

        return null;
    }

    public function findMasterImages(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('masterId IS NULL')
            ->andWhere('type LIKE "image%"')
            ->getQuery()
            ->execute();
    }

    public function findMasterVideos(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('masterId IS NULL')
            ->andWhere('type LIKE "video%"')
            ->getQuery()
            ->execute();
    }

    public function findThumbnails(Media $media)
    {
        $this->assertHasId($media);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('masterId = '. $media->id)
            ->getQuery()
            ->execute();
    }

    public function findVersionsFor(Ad $ad, string $type, string $version = null)
    {
        $this->assertHasId($ad);

        $qb = $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('type LIKE "'. $type .'%"');

        if (null !== $version) {
            $qb->andWhere('version = "'.$version.'"');
        }

        return $qb
            ->getQuery()
            ->execute();
    }

    public function findByAd(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->getQuery()
            ->execute();
    }

    public function findMastersByAd(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('masterId IS NULL')
            ->getQuery()
            ->execute();
    }

    public function findImagesFor(Ad $ad, string $version = null)
    {
        return $this->findVersionsFor($ad, 'image', $version);
    }

    public function findVideosFor(Ad $ad, string $version = null)
    {
        return $this->findVersionsFor($ad, 'video', $version);
    }


    public function findOneByPath(string $path)
    {
        $result = $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('path = "'. $path .'"')
            ->limit(1)
            ->getQuery()
            ->execute();

        return count($result) ? $result->getFirst() : null;
    }

    public function findOneByMasterAndVersion(Media $master, string $version)
    {
        $this->assertHasId($master);

        $result = $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('masterId = '. $master->id)
            ->andWhere('version = "'. $version .'"')
            ->limit(1)
            ->getQuery()
            ->execute();

        return count($result) ? $result->getFirst() : null;
    }

    public function findTempByAd(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('temp = 1')
            ->getQuery()
            ->execute();
    }

    public function findTempMastersByAd(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('adId = '. $ad->id)
            ->andWhere('masterId IS NULL')
            ->andWhere('temp = 1')
            ->getQuery()
            ->execute();
    }

    public function updateAdMedia(Ad $ad, array $ids)
    {
        $this->assertHasId($ad);
        $this->assertNonEmptyIds($ids);

        return $this->phql(sprintf('UPDATE %s SET adId = %d WHERE id IN (%s)',
            Media::class,
            $ad->id,
            implode(',', $ids)
        ));
    }


    protected function assertHasId($model)
    {
        if (empty($model->id)) {
            throw new \InvalidArgumentException('The model ID should not be empty.');
        }
    }

    protected function assertNonEmptyIds(array $array)
    {
        if (0 === count($array)) {
            throw new \InvalidArgumentException('The list of IDs should not be empty.');
        }
    }

}