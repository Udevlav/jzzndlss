<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Promotion;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class PromotionRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Promotion::class, $db, $manager, $config, $logger);
    }

    public function findCurrentByAd(Ad $ad)
    {
        return $this->getQueryBuilder()
            ->from(['promo' => Promotion::class])
            ->distinct(true)
            ->andWhere('promo.adId = "'. $ad->id .'"')
            ->andWhere('promo.validTo >= NOW()')
            ->andWhere('promo.validFrom < NOW()')
            ->getQuery()
            ->execute();
    }

    public function findAutoByAd(Ad $ad)
    {
        return $this->getQueryBuilder()
            ->from(['promo' => Promotion::class])
            ->distinct(true)
            ->andWhere('promo.adId = "'. $ad->id .'"')
            ->andWhere('(promo.validTo < NOW()) OR (promo.validFrom >= NOW())')
            ->getQuery()
            ->execute();
    }

    public function findByAdvertiser(Advertiser $advertiser)
    {
        return $this->getQueryBuilder()
            ->from(['promo' => Promotion::class])
            ->distinct(true)
            ->leftJoin(Ad::class, null, 'ad')
            ->andWhere('ad.advertiserId = "'. $advertiser->id .'"')
            ->andWhere('(promo.validTo < NOW()) OR (promo.validFrom >= NOW())')
            ->getQuery()
            ->execute();
    }

}