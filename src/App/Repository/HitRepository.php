<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Hit;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class HitRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Hit::class, $db, $manager, $config, $logger);
    }

    public function countByAd($adId, int $from, int $to)
    {
        return $this->getQueryBuilder()
            ->from(['hit' => Hit::class])
            ->where('hit.adId = "'. $adId .'"')
            ->andWhere(sprintf('(hit.hitAt >= %d AND hit.hitAt <= %d)', $from, $to))
            ->limit(100)
            ->getQuery()
            ->execute()
            ->count();
    }

}