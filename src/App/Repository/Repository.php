<?php

namespace App\Repository;

use App\Config\Config;
use App\Util\StringUtil;

use Phalcon\Db;
use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Manager;

use InvalidArgumentException;

class Repository
{

    protected $className;
    protected $db;
    protected $manager;
    protected $config;
    protected $logger;

    protected $hydration;
    protected $cacheEnabled;
    protected $cacheKey;
    protected $cacheLifetime;

    /**
     * @param string $className
     * @param AdapterInterface $db
     * @param Manager $manager
     * @param Config $config
     * @param FileLogger $logger
     *
     * @throws InvalidArgumentException
     */
    public function __construct(string $className, AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        if (! is_subclass_of($className , Model::class)) {
            throw new InvalidArgumentException(sprintf('The class should extends the %s class.', Model::class));
        }

        $this->className = $className;
        $this->db = $db;
        $this->manager = $manager;
        $this->config = $config;
        $this->logger = $logger;

        $this
            ->setHydration($config->db->hydration)
            ->setCacheKey($this->generateKey())
            ->setCacheLifetime($config->db->cache->lifetime)
            ->setCacheEnabled($config->db->cache->enabled);
    }


    public function getClassName(): string
    {
        return $this->className;
    }

    public function getShortClassName(): string
    {
        return str_replace('\\App\\Model\\', '', $this->getClassName());
    }

    public function getManager(): Manager
    {
        return $this->manager;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }


    public function getHydration(): int
    {
        return $this->hydration;
    }

    public function setHydration(int $hydration): Repository
    {
        $this->hydration = $hydration;

        return $this;
    }

    public function hydrateArray(): Repository
    {
        return $this->setHydration(Resultset::HYDRATE_ARRAYS);
    }

    public function hydrateRecords(): Repository
    {
        return $this->setHydration(Resultset::HYDRATE_RECORDS);
    }

    public function hydrateObjects(): Repository
    {
        return $this->setHydration(Resultset::HYDRATE_OBJECTS);
    }

    public function isCacheEnabled(): bool
    {
        return $this->cacheEnabled;
    }

    public function setCacheEnabled(bool $enable): Repository
    {
        /*
        if ($enable) {
            $this->logger->debug(sprintf('%s repository cache enabled (key "%s", expiry %d s)',
                $this->getShortClassName(),
                $this->getCacheKey(),
                $this->getCacheLifetime()
            ));
        } else {
            $this->logger->debug(sprintf('%s repository cache disabled.',
                $this->getShortClassName()
            ));
        }
         */

        $this->cacheEnabled = $enable;

        return $this;
    }

    public function disableCache(): Repository
    {
        return $this->setCacheEnabled(false);
    }

    public function enableCache(): Repository
    {
        return $this->setCacheEnabled(true);
    }

    public function getCacheKey(): string
    {
        return $this->cacheKey;
    }

    public function setCacheKey(string $cacheKey): Repository
    {
        $this->cacheKey = $cacheKey;

        return $this;
    }

    public function getCacheLifetime(): int
    {
        return $this->cacheLifetime;
    }

    public function setCacheLifetime(int $lifetime): Repository
    {
        $this->cacheLifetime = $lifetime;

        return $this;
    }


    public function getQueryBuilder(): Builder
    {
        return new Builder();
    }

    public function sql(string $query, bool $read = true)
    {
        $resultSet = $this->db->query($query);

        if ($read) {
            $resultSet->setFetchMode(Db::FETCH_ASSOC);
            $resultSet = $resultSet->fetchAll();
        }

        return $resultSet;
    }

    public function phql(string $query)
    {
        return $this->manager->createQuery($query)->execute();
    }


    public function find(string $id)
    {
        if (empty($id)) {
            throw new InvalidArgumentException();
        }

        return call_user_func([$this->className, 'findFirst'],
            $this->buildOptions(sprintf('id = %s', $id))
        );
    }

    public function findOneBy($criteria)
    {
        return call_user_func([$this->className, 'findFirst'],
            $this->buildOptions($criteria)
        );
    }

    public function findAll($criteria = null)
    {
        return call_user_func([$this->className, 'find'],
            $this->buildOptions($criteria)
        );
    }

    public function findIn(array $ids)
    {
        if (0 === count($ids)) {
            throw new InvalidArgumentException('The array of IDs to find in should not be empty.');
        }

        return call_user_func([$this->className, 'find'],
            $this->buildOptions(sprintf('id IN (%s)', implode(',', $ids)))
        );
    }

    public function findBy($criteria)
    {
        return call_user_func([$this->className, 'find'],
            $this->buildOptions($criteria)
        );
    }


    public function remove(string $id)
    {
        // TODO
    }

    public function removeIn(array $ids)
    {
        if (0 === count($ids)) {
            return;
        }

        // TODO
    }


    protected function buildOptions($options = null): array
    {
        if (null === $options) {
            $options = [];
        }

        if (! is_array($options)) {
            $options = [$options];
        }

        $defaultOptions = [];
        $defaultOptions['hydration'] = $this->hydration;

        if ($this->cacheEnabled) {
            $defaultOptions['cache'] = [
                'key'      => $this->cacheKey,
                'lifetime' => $this->cacheLifetime
            ];
        } else {
            $defaultOptions['cache'] = [
                'key' => uniqid(),
                'lifetime' => 0,
            ];
        }

        return array_merge($defaultOptions, $options);
    }

    protected function generateKey(): string
    {
        return StringUtil::underscore(str_replace('\\', '', $this->className));
    }
}