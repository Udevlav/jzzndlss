<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\Experience;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class ExperienceRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Experience::class, $db, $manager, $config, $logger);
    }

    public function findByAd(Ad $ad, int $limit = 10, int $offset = 0)
    {
        return $this->getQueryBuilder()
            ->from(['experience' => Experience::class])
            ->andWhere('experience.adId = '. $ad->id)
            ->orderBy('experience.rating DESC')
            ->orderBy('experience.createdAt DESC')
            ->limit($limit, $offset)
            ->getQuery()
            ->execute();
    }

}
