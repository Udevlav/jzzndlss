<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\AdTag;
use App\Model\Tag;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class TagRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Tag::class, $db, $manager, $config, $logger);

        // We can cache tags forever (or a long cache time, as one
        // month) as they're not expected to change ever
        $this
            ->setCacheEnabled(true)
            ->setCacheLifetime(30 * 24 * 3600);
    }


    public function groupByGid()
    {
        static $gids;

        if (null === $gids) {
            $gids = [];

            foreach ($this->findAll() as $tag) {
                $gid = $tag->gid;

                if (! isset($gids[$gid])) {
                    $gids[$gid] = [];
                }

                $gids[$gid][] = $tag;
            }
        }

        return $gids;
    }

    public function groupBySlug()
    {
        static $tags;

        if (null == $tags) {
            $tags = [];

            foreach ($this->findAll() as $tag) {
                $tags[$tag->slug] = $tag;
            }
        }

        return $tags;
    }

    public function findByGid(int $gid)
    {
        return $this->groupByGid()[$gid] ?? [];
    }

    public function optionsByGid(int $gid)
    {
        $options = [];

        foreach ($this->findByGid($gid) as $tag) {
            $options[$tag->id] = $tag->name;
        }

        return $options;
    }

    public function findByAd(Ad $ad)
    {
        $this->assertHasId($ad);

        return $this->getQueryBuilder()
            ->from(['tag' => Tag::class])
            ->leftJoin(AdTag::class, null, 'rel')
            ->where('rel.adId = '. $ad->id)
            ->getQuery()
            ->execute();
    }

    public function idsByAd(Ad $ad)
    {
        $ids = [];

        foreach ($this->findByAd($ad) as $tag) {
            $ids[] = $tag->id;
        }

        return $ids;
    }

    public function groupByAd(Ad $ad)
    {
        $gids = [];

        foreach ($this->findByAd($ad) as $tag) {
            $gid = $tag->gid;

            if (! isset($gids[$gid])) {
                $gids[$gid] = [];
            }

            $gids[$gid][] = $tag;
        }

        return $gids;
    }


    protected function assertHasId($model)
    {
        if (empty($model->id)) {
            throw new \InvalidArgumentException('The model ID should not be empty.');
        }
    }
}