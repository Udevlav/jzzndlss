<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\ChatMessage;
use App\Model\OnlineAdvertiser;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;

class OnlineAdvertiserRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(ChatMessage::class, $db, $manager, $config, $logger);
    }

    public function findByAd(Ad $ad)
    {
        return $this->getQueryBuilder()
            ->from(['online' => OnlineAdvertiser::class])
            ->andWhere('online.advertiserId = '. $ad->advertiserId)
            ->orderBy('online.lastActivityAt DESC')
            ->limit(1)
            ->getQuery()
            ->execute();
    }

    public function findBySessionId(string $sessionId)
    {
        return $this->getQueryBuilder()
            ->from(['online' => OnlineAdvertiser::class])
            ->andWhere('online.advertiserSid = "'. $sessionId .'"')
            ->orderBy('online.lastActivityAt DESC')
            ->limit(1)
            ->getQuery()
            ->execute();
    }

}