<?php

namespace App\Repository;

class EntityNotFoundException extends \RuntimeException
{

    static public function entity(string $className, string $cause = null): EntityNotFoundException
    {
        $message = sprintf('Cannot found %s entity', $className);

        if (null !== $cause) {
            $message .= $cause;
        }

        $message .= '.';

        return new static($message);
    }

    static public function id(string $className, string $id): EntityNotFoundException
    {
        return static::entity($className, sprintf(' with id %s', $id));
    }

    static public function token(string $className, string $token): EntityNotFoundException
    {
        return static::entity($className, sprintf(' with token %s', $token));
    }
}