<?php

namespace App\Repository;

use App\Config\Config;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Promotion;

use Phalcon\Db\AdapterInterface;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Model\Manager;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Resultset;

class AdRepository extends Repository
{

    public function __construct(AdapterInterface $db, Manager $manager, Config $config, FileLogger $logger)
    {
        parent::__construct(Ad::class, $db, $manager, $config, $logger);
    }

    public function getPublishedQuery(): Builder
    {
        return $this->getQueryBuilder()
            ->from([
                'ad' => Ad::class,
            ])
            ->distinct(true)
            ->leftJoin(Promotion::class, null, 'promo')
            ->andWhere('ad.publishedAt <= NOW()')
        ;
    }

    public function getPromotedQuery(): Builder
    {
        return $this->getPublishedQuery()
            ->andWhere('(promo.validFrom <= NOW()) AND (promo.validTo > NOW())')
            ->orderBy('ad.promotedAt DESC, ad.numHits DESC, ad.publishedAt DESC')
        ;
    }

    public function getWithPromotionQuery(): Builder
    {
        return $this->getQueryBuilder()
            ->columns('ad.*, promo.*')
            ->addFrom(Ad::class, 'ad')
            ->distinct(true)
            ->leftJoin(Promotion::class, null, 'promo')
            ->andWhere('ad.publishedAt <= NOW()')
            ->andWhere('promo.adId IS NOT NULL')
            ->andWhere('promo.validFrom <= NOW()')
            ->andWhere('promo.validTo > NOW()')
            ->orderBy('ad.score DESC, ad.publishedAt DESC')
        ;
    }

    public function getRegularQuery(Resultset $promoted): Builder
    {
        $qb = $this->getPublishedQuery()
            ->andWhere('(promo.adId IS NULL) OR (promo.validFrom >= NOW()) OR (promo.validTo <= NOW())');

        if ($promoted->count()) {
            $qb->andWhere('(ad.id NOT IN ('.$this->toIds($promoted).'))');
        }

        return $qb->orderBy('ad.score DESC, ad.publishedAt DESC');
    }

    public function getSortedByPublicationDateQuery(): Builder
    {
        return $this->getQueryBuilder()
            ->from([ 'ad' => Ad::class ])
            ->distinct(true)
            ->andWhere('ad.publishedAt <= NOW()')
            ->orderBy('ad.publishedAt DESC, ad.numHits DESC, ad.score DESC')
        ;
    }


    public function findOneByTitle(string $title)
    {
        if (empty($title)) {
            throw new \InvalidArgumentException();
        }

        return $this->findOneBy(sprintf('title = "%s"', $title));
    }

    public function findOneBySlug(string $slug)
    {
        if (empty($slug)) {
            throw new \InvalidArgumentException();
        }

        return $this->findOneBy(sprintf('slug = "%s"', $slug));
    }

    public function findOneByHash(string $hash)
    {
        if (empty($hash)) {
            throw new \InvalidArgumentException();
        }

        return $this->findOneBy(sprintf('hash = "%s"', $hash));
    }

    public function findOneByHashWithToken(string $hash)
    {
        if (empty($hash)) {
            throw new \InvalidArgumentException();
        }

        return $this->getQueryBuilder()
            ->columns([
                'ad.*',
                'advertiser.token AS token',
            ])
            ->from([
                'ad' => Ad::class,
            ])
            ->leftJoin(Advertiser::class, null, 'advertiser')
            ->andWhere('ad.hash = "'. $hash .'"')
            ->limit(1)
            ->getQuery()
            ->execute();
    }

    public function findByAdvertiser(Advertiser $advertiser)
    {
        return $this->findBy(sprintf('advertiserId = "%s"', $advertiser->id));
    }

    public function getPage(int $page, int $limit = 20)
    {
        return $this->findBy([
            'limit' => [
                'number' => $limit,
                'offset' => $page * $limit,
            ]
        ]);
    }


    protected function toIds(Resultset $promoted)
    {
        $ids = [];

        foreach ($promoted as $record) {
            $ids[] = $record->id;
        }

        return implode(',', $ids);
    }

}
