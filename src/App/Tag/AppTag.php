<?php

namespace App\Tag;

use App\Config\Config;
use App\I18n\Translator;

use App\Model\Ad;
use App\Model\Category;
use App\Model\Province;
use App\Util\StringUtil;

use Phalcon\Mvc\Router\Annotations;
use Phalcon\Tag;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

use DateTime;
use Throwable;

class AppTag extends Tag
{

    private $tabIndex = 0;

    public function nextTabIndex()
    {
        return $this->tabIndex ++;
    }

    public function isArray($object)
    {
        return is_array($object);
    }

    public function get($object, $key)
    {
        return $object->$key ?? null;
    }

    public function set($object, $key, $val)
    {
        $object->$key = $val;
    }

    public function inArray($needle, $haystack)
    {
        return in_array($needle, $haystack);
    }

    public function formatNumber($n, $decimals = 0, $decPoint = ',', $thSep = '.')
    {
        return number_format($n, $decimals, $decPoint, $thSep);
    }

    public function date($format, $date = null)
    {
        if ($date === null) {
            return date($format);
        }

        if (! ($date instanceof DateTime)) {
            try {
                $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
            } catch (Throwable $t) {
                $date = new DateTime();
            }
        }

        return $date->format($format);
    }

    public function datetime($timestamp, $format)
    {
        $date = new \DateTime();
        $date->setTimestamp(strtotime($timestamp));

        return $date->format($format);
    }

    public function timerange($timerange)
    {
        return is_array($timerange) ? implode(' - ', $timerange) : $timerange;
    }

    public function dump($object)
    {
        return '<pre>'.print_r($object).'</pre>';
    }

    public function cn($object)
    {
        return $object ? get_class($object) : null;
    }

    public function fallback($data, $fallback = '')
    {
        return $data ?? $fallback;
    }

    public function asset(string $path)
    {
        return '/asset/'. str_replace($this->getConfig()->layout->publicDir, '', $path);
    }

    public function media(string $path)
    {
        return '/media/'. str_replace($this->getConfig()->layout->mediaDir, '', $path);
    }

    public function intval($that)
    {
        return intval($that);
    }

    public function floatval($that)
    {
        return floatval($that);
    }

    public function adPoster(Ad $ad)
    {
        if (null !== $ad->publishedAt) {
            return $ad->poster;
        }

        $path = explode('.', basename($ad->poster));
        array_pop($path);

        return sprintf('/tmp/%s.tmp', implode('.', $path));
    }

    public function poster(Ad $ad, $size = null)
    {
        $poster = $this->adPoster($ad);

        if (null !== $size) {
            $poster = explode('.', $poster);
            $count = count($poster);
            $poster[$count - 2] .= '-'. $size;
            $poster = implode('.', $poster);
        }

        return $poster;
    }

    public function flavour(string $path, string $version)
    {
        $path = explode('.', $path);
        $ext = array_pop($path);

        return implode('.', $path) .'-'. $version .'.'. $ext;
    }

    public function trans(... $parameters)
    {
        if (null === ($token = array_shift($parameters))) {
            throw new \BadFunctionCallException('Bad translation call: no token specified.');
        }

        return $this->getTranslator()->translate($token, ... $parameters);
    }

    public function link(... $parameters)
    {
        if (null === ($token = array_shift($parameters))) {
            throw new \BadFunctionCallException('Bad route link call: no route name specified.');
        }

        if ('@' !== $token[0]) {
            throw new \BadFunctionCallException('Bad route link call: route name should start with "@".');
        }

        if (false === ($route = $this->getRouter()->getRouteByName($token))) {
            throw new \RuntimeException(sprintf('Route "%s" does not exist.', $token));
        }

        $segments = explode('/', $route->getPattern());

        foreach ($segments as & $segment) {
            if (false !== strpos($segment, ':')) {
                if (count($parameters) > 0) {
                    $segment = array_shift($parameters) ?? '?';
                } else {
                    throw new \RuntimeException(sprintf('Route "%s" requires parameter "%s".', $token, $segment));
                }
            }
        }

        return implode('/', $segments);
    }

    public function abslink(... $parameters)
    {
        $config = $this->getConfig();

        return rtrim($config->baseUrl(), '/') . $this->link(... $parameters);
    }

    public function phone($number)
    {
        return sprintf('tel:+34%s', $number);
    }

    public function formatPhone($number, $separator = ' ')
    {
        if (empty($number)) {
            return '';
        }

        if ('/' === $number[0]) {
            return sprintf('<img src="https://www.nuevoloquo.com/%s" />', $number);
        }

        $phoneUtil = PhoneNumberUtil::getInstance();

        try {
            $phone = $phoneUtil->parse($number);
        } catch (NumberParseException $e) {
            return $number;
        }

        return $phoneUtil->format($phone, PhoneNumberFormat::E164) ?? $number;
    }

    public function dayname($dayno, bool $long = false)
    {
        if (-1 === (int) $dayno) {
            return 'day.all';
        }

        if ($long) {
            $labels = [
                'day.sun',
                'day.mon',
                'day.tue',
                'day.wed',
                'day.thu',
                'day.fri',
                'day.sat',
            ];
        } else {
            $labels = [
                'day.su',
                'day.mo',
                'day.tu',
                'day.we',
                'day.th',
                'day.fr',
                'day.sa',
            ];
        }

        return $labels[$dayno] ?? '';
    }

    public function slug($token)
    {
        return StringUtil::sluggify($token);
    }

    public function category($cat)
    {
        return $this->trans(Category::constants()[$cat] ?? $cat);
    }

    public function superCategory($cat)
    {
        return $this->trans(Category::superCategory(intval($cat)));
    }

    public function provinceCode($name)
    {
        return Province::postalCode($name);
    }

    public function provinceCodeBySlug($slug)
    {
        return Province::postalCodeBySlug($slug);
    }

    public function provinceName($provinceCode)
    {
        return Province::name($provinceCode);
    }

    public function provinceSlug($provinceCode)
    {
        return 'provincia-'. Province::slug($provinceCode);
    }

    public function seoTitle(Ad $ad, Config $config)
    {
        $name = $ad->name;
        $category = $this->superCategory($ad->category);
        $appName = $config->app->name;

        // Google will truncate this to 70 chars
        // Google will truncate desriptions to 156 chars
        return sprintf('%s | %s | %s', $name, $category, $appName);
    }

    public function ucfirst($token)
    {
        return ucfirst($token);
    }

    public function substr($text, $length = 20)
    {
        return strlen($text) <= $length ? $text : substr($text, 0, $length) .'...';
    }

    public function localDate($date)
    {
        return strftime('%a %e %b %H:%M', DateTime::createFromFormat('Y-m-d H:i:s', $date)->getTimestamp());
    }

    public function timeago($date)
    {
        $second = 1;
        $minute = 60;
        $hour = 60 * $minute;
        $day = 24 * $hour;
        $month = 30 * $day;

        $delta = time() - strtotime($date);

        if ($delta < 10 * $second) {
            return $this->trans('timeago.now');
        }

        if ($delta < 60 * $second) {
            return $this->trans('timeago.secondsAgo', $delta);
        }

        if ($delta < 2 * $minute) {
            return $this->trans('timeago.oneMinuteAgo');
        }

        if ($delta < 45 * $minute) {
            return $this->trans('timeago.minutesAgo', floor($delta / $minute));
        }

        if ($delta < 90 * $minute) {
            return $this->trans('timeago.oneHourAgo');
        }

        if ($delta < 24 * $hour) {
            return $this->trans('timeago.hoursAgo', floor($delta / $hour));
        }

        if ($delta < 48 * $hour) {
            return $this->trans('timeago.yesterday');
        }

        if ($delta < 30 * $day) {
            return $this->trans('timeago.daysAgo', floor($delta / $day));
        }

        if ($delta < 12 * $month) {
            $months = floor($delta / $day / 30);

            if ($months <= 1) {
                return $this->trans('timeago.lastMonth');
            }

            return $this->trans('timeago.monthsAgo', $months);
        }

        $years = floor($delta / $day / 365);

        if ($years <= 1) {
            return $this->trans('timeago.lastYear');
        }

        return $this->trans('timeago.yearsAgo', $years);
    }

    public function mapCircle($entity, $apiKey)
    {
        //maps.googleapis.com/maps/api/staticmap?center={{ entity.lat }},{{ entity.lng }}&amp;zoom=13&amp;size=1200x300&amp;maptype=roadmap&#10;&amp;markers=color:red%7Clabel:C%7C{{ entity.lat }},{{ entity.lng }}&amp;key={{ apiKey }}

        $mapCentLat = $entity->lat + 0.002;
        $mapCentLng = $entity->lng - 0.011;
        $mapW =       1200;
        $mapH =       300;
        $zoom =       14;

        $circRadius =       0.75;         //Radius in km
        $circRadiusThick =  1;
        $circFill =         '00BFFF';
        $circFillOpacity =  '60';
        $circBorder =       'red';

        $encString = '3Aad_yHofn%60%40JyFh%40sF%60AcFxAmElBqD~BoCnCiBtC_AzCUzCTvC~%40lChB~BnCnBpDxAlE%60AbFf%40rFLxFMxFg%40rFaAbFyAlEoBpD_CnCmChBwC~%40%7BCT%7BCUuC_AoCiB_CoCmBqDyAmEaAcFi%40sFKyF%3F%3F';//GMapCircle($lat,$lng,$circRadius); //Encoded polyline string

        $src =  '//maps.googleapis.com/maps/api/staticmap?';
        $src .= 'center=' .$entity->lat. ',' .$entity->lng. '&';
        $src .= 'zoom=' .$zoom. '&';
        $src .= 'size=' .$mapW. 'x' .$mapH.'&';
        $src .= 'maptype=roadmap&';
        $src .= 'style=feature:water|element:geometry.fill|color:0x9bd3ff&';
        $src .= 'path=';
        $src .= 'color:0x' .$circBorder. '00|';
        $src .= 'fillcolor:0x' .$circFill.$circFillOpacity. '|';
        $src .= 'weight:' .$circRadiusThick. '|';
        $src .= 'enc:' .$encString;

        return $src .'&key='. $apiKey;
    }


    protected function getConfig(): Config
    {
        return $this->getDI()->get('config');
    }

    protected function getRouter(): Annotations
    {
        return $this->getDI()->get('router');
    }

    protected function getTranslator(): Translator
    {
        return $this->getDI()->get('translator');
    }
}
