<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Annotations;

// See http://stackoverflow.com/questions/26181633/

$router = new Annotations(false);
$router->removeExtraSlashes(true);
$router->setDefaultNamespace('App\\Controller\\');
$router->setDefaultController('index');
$router->setDefaultAction('index');
$router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);

define('ROUTE_ALPHA_TOKEN', '[a-z]{1}[a-z-]+');
define('ROUTE_ALPHANUM_TOKEN', '[a-z]{1}[a-z0-9-]+');
define('ROUTE_NUM_TOKEN', '[0-9]+');

$config = $this->get('config');


/**
 * GET|POST (Default route)
 */
$route = $router->add('/', []);
$route->via(['GET', 'POST']);
$route->setName('@index');

/**
 * GET /cookies (static)
 */
$route = $router->add('/cookies/{name:[a-f]+}', [
    'controller' => 'cookies',
    'action'     => 'index',
]);
$route->via(['GET']);
$route->setName('@cookies');

/**
 * GET /privacidad (static)
 */
$route = $router->add('/privacidad', [
    'controller' => 'static',
    'action'     => 'privacy',
]);
$route->via(['GET']);
$route->setName('@privacy');

/**
 * GET /test/:what (REMOVE ME)
 */
$route = $router->add('/test/{what:.*}', [
    'controller' => 'test',
    'action'     => 'index',
]);
$route->via(['GET']);
$route->setName('@test');

/**
 * GET /terminos-uso (static)
 */
$route = $router->add('/terminos-uso', [
    'controller' => 'static',
    'action'     => 'tos',
]);
$route->via(['GET']);
$route->setName('@tos');

/**
 * GET /quienes-somos (static)
 */
$route = $router->add('/quienes-somos', [
    'controller' => 'static',
    'action'     => 'whoweare',
]);
$route->via(['GET']);
$route->setName('@whoweare');

/**
 * GET|POST /contacto
 */
$route = $router->add('/contacto', [
    'controller' => 'contact',
    'action'     => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@contact');

/**
 * POST /desbloquear
 */
$route = $router->add('/desbloquear', [
    'controller' => 'contact',
    'action'     => 'index',
]);
$route->via(['POST']);
$route->setName('@unblock');

/**
 * POST|PUT /ingestar
 */
$route = $router->add('/ingestar', [
    'controller' => 'ingest',
    'action' => 'index',
]);
$route->via(['POST', 'PUT']);
$route->setName('@ingest');

/**
 * DELETE /descartar
 */
$route = $router->add('/descartar/{id:[\d]+}', [
    'controller' => 'ingest',
    'action' => 'delete',
]);
$route->via(['DELETE']);
$route->setName('@unlink');

/**
 * GET /preview?url=:url
 */
$route = $router->add('/previsualizar', [
    'controller' => 'ingest',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@preview');

/**
 * POST /pago/:token/:slug
 */
$route = $router->add('/pago/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'payment',
    'action' => 'index',
]);
$route->via(['POST']);
$route->setName('@payment');

/**
 * GET|POST /verificar-pago/:orderNo
 */
$route = $router->add('/verificar-pago/{orderNo:[0-9a-zA-Z]+}', [
    'controller' => 'payment',
    'action' => 'verify',
]);
$route->via(['GET', 'POST']);
$route->setName('@verify_payment');

/**
 * GET|POST /pago-confirmado/:orderNo
 */
$route = $router->add('/pago-confirmado/{orderNo:[0-9a-zA-Z]+}', [
    'controller' => 'payment',
    'action' => 'succeed',
]);
$route->via(['GET', 'POST']);
$route->setName('@payment_succeed');

/**
 * GET|POST /error-pago/:orderNo
 */
$route = $router->add('/error-pago/{orderNo:[0-9a-zA-Z]+}', [
    'controller' => 'payment',
    'action' => 'verify',
]);
$route->via(['GET', 'POST']);
$route->setName('@payment_failed');

/**
 * GET /subir/:token/:slug
 */
$route = $router->add('/subir/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'promote',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@promote');

/**
 * GET /subir-ahora/:token/:slug
 * /
$route = $router->add('/subir-ahora/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'promote',
    'action' => 'now',
]);
$route->via(['GET']);
$route->setName('@promote_now');
 * */

/**
 * Sustituye a la anterior.
 *
 * POST /subir-ahora/:token/:slug
 */
$route = $router->add('/subir-ahora/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'promote',
    'action' => 'now',
]);
$route->via(['POST']);
$route->setName('@promote_now');

/**
 * GET|POST /publicar
 */
$route = $router->add('/publicar', [
    'controller' => 'create',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@publish');

/**
 * GET|POST /publicar/:token/:slug
 */
$route = $router->add('/publicar/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'publish',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@confirm_publication');

/**
 * GET|POST /anunciante/:provider
 */
$route = $router->add('/anunciante/generico', [
    'controller' => 'scrap',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@manage_scrapped');

/**
 * GET|POST /anunciante/:token
 */
$route = $router->add('/anunciante/{token:[0-9a-f]+}', [
    'controller' => 'manage',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@manage');

/**
 * GET|POST|PUT /editar/:token/:slug
 */
$route = $router->add('/editar/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'edit',
    'action' => 'index',
]);
$route->via(['GET', 'POST', 'PUT']);
$route->setName('@edit');

/**
 * DELETE /borrar/:token/:slug
 */
$route = $router->add('/borrar/{token:[0-9a-f]+}/{slug:[0-9a-z-]*}', [
    'controller' => 'delete',
    'action' => 'index',
]);
$route->via(['GET', 'DELETE']);
$route->setName('@delete');

/**
 * GET|POST /denunciar/:slug
 */
$route = $router->add('/denunciar/{slug:[0-9a-z-]+}', [
    'controller' => 'complaint',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@complaint');

/**
 * GET|POST /buscar
 */
$route = $router->add('/buscar', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@search');

/**
 * GET /unsubscribe
 */
$route = $router->add('/unsubscribe', [
    'controller' => 'unsubscribe',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@unsubscribe');

/**
 * GET /chat
 */
$route = $router->add('/chat', [
    'controller' => 'chat',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@chat_read');

/**
 * POST /chat/post
 */
$route = $router->add('/chat/post', [
    'controller' => 'chat',
    'action' => 'post',
]);
$route->via(['POST']);
$route->setName('@chat_post');

/**
 * GET|POST /:category
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category');

/**
 * GET|POST /:category/:place
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}/{place:[0-9a-z-]+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category_and_location');

/**
 * GET|POST /:category/:place/:tag1
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}/{place:[0-9a-z-]+}/{tag1:[0-9a-z-]+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category_and_location_and_tags_1');

/**
 * GET|POST /:category/:place/:tag1/:tag2
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}/{place:[0-9a-z-]+}/{tag1:[0-9a-z-]+}/{tag2:[0-9a-z-]+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category_and_location_and_tags_2');

/**
 * GET|POST /:category/:place/:tag1/:tag2/:tag3
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}/{place:[0-9a-z-]+}/{tag1:[0-9a-z-]+}/{tag2:[0-9a-z-]+}/{tag3:[0-9a-z-]+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category_and_location_and_tags_3');

/**
 * GET|POST /:category/:place/:tag1/:tag2/:tag3/:tag4
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}/{place:[0-9a-z-]+}/{tag1:[0-9a-z-]+}/{tag2:[0-9a-z-]+}/{tag3:[0-9a-z-]+}/{tag4:[0-9a-z-]+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category_and_location_and_tags_4');

/**
 * GET|POST /:category/:place/:tag1/:tag2/:tag3/:tag4/:tag5
 */
$route = $router->add('/{category:(escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales)+}/{place:[0-9a-z-]+}/{tag1:[0-9a-z-]+}/{tag2:[0-9a-z-]+}/{tag3:[0-9a-z-]+}/{tag4:[0-9a-z-]+}/{tag5:[0-9a-z-]+}', [
    'controller' => 'index',
    'action' => 'index',
]);
$route->via(['GET', 'POST']);
$route->setName('@filter_by_category_and_location_and_tags_5');

/**
 * GET /:slug/hit
 */
//$route = $router->add('/{slug:[a-z0-9-]{16,}}/hit', [
$route = $router->add('/{hash:[a-z0-9-]{3,}-[0-9]{6,9}-[0-9a-f]{8}}/hit', [
    'controller' => 'hit',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@hit');

/**
 * GET /:slug/stats
 */
//$route = $router->add('/{slug:[a-z0-9-]{16,}}/hit', [
$route = $router->add('/{hash:[a-z0-9-]{3,}-[0-9]{6,9}-[0-9a-f]{8}}/stats', [
    'controller' => 'stats',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@stats');

/**
 * GET /:slug/chat
 */
//$route = $router->add('/{slug:[a-z0-9-]{16,}}/chat', [
$route = $router->add('/{hash:[a-z0-9-]{3,}-[0-9]{6,9}-[0-9a-f]{8}}/chat', [
    'controller' => 'online',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@chat_available');

/**
 * POST /:slug/peer
 */
$route = $router->add('/{slug:[a-z0-9-]{16,}}/peer', [
    'controller' => 'online',
    'action' => 'peer',
]);
$route->via(['POST']);
$route->setName('@chat_peer');

/**
 * POST /:slug/online
 */
$route = $router->add('/{slug:[a-z0-9-]{16,}}/online', [
    'controller' => 'online',
    'action' => 'online',
]);
$route->via(['POST']);
$route->setName('@chat_on');

/**
 * POST /:slug/offline
 */
$route = $router->add('/{slug:[a-z0-9-]{16,}}/offline', [
    'controller' => 'online',
    'action' => 'offline',
]);
$route->via(['POST', 'DELETE']);
$route->setName('@chat_off');

/**
 * GET|POST /:slug/experiencias
 */
$route = $router->add('/{slug:[a-z0-9-]{16,}}/experiencias', [
    'controller' => 'experience',
    'action' => 'index',
]);
$route->via(['GET','POST']);
$route->setName('@experiences');

/**
 * GET|POST /:slug/enviar-enlace
 */
$route = $router->add('/{slug:[a-z0-9-]{16,}}/enviar-enlace', [
    'controller' => 'manage',
    'action' => 'requestToken',
]);
$route->via(['GET']);
$route->setName('@send_link');

/**
 * GET /:hash
 */
$route = $router->add('/{hash:[a-z0-9-]{3,}-[0-9]{6,9}-[0-9a-f]{8}}', [
    'controller' => 'detail',
    'action' => 'seo',
]);
$route->via(['GET']);
$route->setName('@ad_seo');

/**
 * GET /:slug
 */
$route = $router->add('/anuncio/{slug:[a-z0-9-]{16,}}', [
    'controller' => 'detail',
    'action' => 'index',
]);
$route->via(['GET']);
$route->setName('@ad');


$router->removeExtraSlashes(true);

$router->setDefaults([
    'controller' => 'index',
    'action'     => 'index',
]);

// Set 404 paths
$router->notFound([
    'controller' => 'error',
    'action'     => 'error404',
]);

return $router;
