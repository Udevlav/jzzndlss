<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

$config = $this->get('config');

$view = new View();
$view->setDI($this);
$view->setViewsDir($config->layout->viewsDir);

$view->registerEngines([
    '.phtml' => PhpEngine::class,
    '.volt' => function ($view) use ($config) {
        $volt = new VoltEngine($view, $this);
        $volt->setOptions([
            'compiledPath' => $config->layout->tplCacheDir,
            'compiledSeparator' => '_',
        ]);

        // Recompile always on development environments
        if ($config->app->env === 'dev') {
            $volt->setOptions([
                'stat' => true,
                'compileAlways' => true
            ]);
        }

        return $volt;
    },
]);

return $view;