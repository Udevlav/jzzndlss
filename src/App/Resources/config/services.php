<?php

use App\Captcha\ReCaptchaVerifier;
use App\Command\Check\CheckConfigCommand;
use App\Command\Check\CheckEnvCommand;
use App\Command\Check\CheckMailerCommand;
use App\Command\Check\CheckSmsCommand;
use App\Command\Clean\CleanPromotionsCommand;
use App\Command\Cron\EditCronCommand;
use App\Command\Cron\ListCronCommand;
use App\Command\Cron\ScheduleJobCommand;
use App\Command\Db\CreateSchemaCommand;
use App\Command\Db\DropSchemaCommand;
use App\Command\Db\LoadFixturesCommand;
use App\Command\Db\UpdateAdsCommand;
use App\Command\Db\UpdateStatsCommand;
use App\Command\Deploy\GenerateMakefileCommand;
use App\Command\Deploy\GenerateManifestCommand;
use App\Command\Deploy\GeneratePackageJsonCommand;
use App\Command\Deploy\GenerateRobotsCommand;
use App\Command\Deploy\GenerateSelfSignedCertCommand;
use App\Command\Deploy\GenerateSitemapCommand;
use App\Command\Deploy\GenerateVhostCommand;
use App\Command\Gc\CleanAdsCommand;
use App\Command\Gc\CleanCacheCommand;
use App\Command\Gc\CleanMediaCommand;
use App\Command\Gc\CleanPaymentsCommand;
use App\Command\Publish\ExtractMetadataCommand;
use App\Command\Publish\GenerateFramesCommand;
use App\Command\Publish\GenerateThumbnailCommand;
use App\Command\Publish\GenerateThumbnailsCommand;
use App\Command\Publish\GenerateWatermarksCommand;
use App\Command\Publish\MoveMediaCommand;
use App\Command\Publish\ImageOptimizeCommand;
use App\Command\Publish\ImageWatermarkCommand;
use App\Command\Publish\PublishAdCommand;
use App\Command\Publish\TranscodeMediaCommand;
use App\Command\Rank\RankCommand;
use App\Command\Rank\UpdateRankCommand;
use App\Command\Scrap\ScrapCommand;
use App\FFMpeg\FormatFactory;
use App\FFMpeg\QtFaststart;
use App\Finder\FallbackLocationFinder;
use App\Geo\Locator\FallbackGeoLocator;
use App\Geo\Coder\GoogleGeoCoder;
use App\I18n\Loader\DbTranslationLoader;
use App\I18n\Loader\JsonTranslationLoader;
use App\I18n\Loader\IniTranslationLoader;
use App\I18n\Loader\PhpTranslationLoader;
use App\I18n\Translator;
use App\IO\HexTreePathGenerator;
use App\Locale\Guesser\DelegatingLocaleGuesser;
use App\Mailer\Mailer;
use App\Payment\Broker;
use App\Promotion\Promoter;
use App\Rank\RedditRanker;
use App\Repository\EntityManager;
use App\Runner\Runner;
use App\Session\Flash;
use App\Sex\Hasher;
use App\Sms\InstasentSms;
use App\Sms\MensatekSms;
use App\Token\TokenGenerator;
use App\View\Templating;

use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;

use Imagine\Gd\Imagine as GdAdapter;
use Imagine\Gmagick\Imagine as GmagickAdapter;
use Imagine\Imagick\Imagine as ImagickAdapter;

use ImageOptimizer\OptimizerFactory;

use Phalcon\Cache\Frontend\Data as FrontendData;
use Phalcon\Cache\Backend\Apc as ApcBackend;
use Phalcon\Cache\Backend\Libmemcached as LibmemcachedBackend;
use Phalcon\Cache\Backend\File as FileBackend;
use Phalcon\Cache\Backend\Memcache as MemcacheBackend;
use Phalcon\Crypt;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Events\Event;
use Phalcon\Ext\Mailer\Manager as MailerManager;
use Phalcon\Ext\Mailer\Message as MailerMessage;
use Phalcon\Http\Response\Cookies;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Manager as ModelManager;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return require(APP_PATH . '/Resources/config/config.php');
});

/**
 * Logging
 */
$di->setShared('logger', function () {
    $config = $this->get('config');

    $logger = new FileLogger(sprintf('%s%s', $config->layout->logDir, $config->logging->logName));
    $logger->setLogLevel($config->logging->logLevel);

    return $logger;
});

/**
 * Events Manager
 */
$di->setShared('eventsManager', function () {
    return new EventsManager();
});

/**
 * Register a custom router.
 *
 * @see http://stackoverflow.com/questions/26181633/
 */
$di->setShared('router', function () {
    return require(APP_PATH . '/Resources/config/service/routing.php');
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->get('config');

    $url = new UrlResolver();
    $url->setBaseUri(sprintf('%s://%s%s',
        $config->server->protocol,
        $config->server->authority,
        $config->server->port ? ':'. $config->server->port : ''
    ));

    return $url;
});

/**
 * Translator service
 */
$di->setShared('translator', function() {
    $config = $this->get('config');

    switch ($config->i18n->loader) {
        case 'db':
            $loader = new DbTranslationLoader($this->getShared('db'));
            break;

        case 'ini':
            $loader = new IniTranslationLoader($config->i18n->dictionary);
            break;

        case 'php':
            $loader = new PhpTranslationLoader($config->i18n->dictionary);
            break;

        case 'json':
            $loader = new JsonTranslationLoader($config->i18n->dictionary);
            break;

        default:
            throw new \InvalidArgumentException(sprintf('No such translation loader "%s".', $config->i18n->loader));
    }

    $translator = new Translator($loader);
    $translator->setDI($this);
    $translator->setLocale($config->app->locale);

    return $translator;
});

/**
 * @see \App\Locale\LocaleGuesser
 */
$di->setShared('localeGuesser', function () {
    $guesser = new DelegatingLocaleGuesser();
    $guesser->setDI($this);

    return $guesser;
});

/**
 * @see \App\IO\HexTreePathGenerator
 */
$di->setShared('pathGenerator', function () {
    $pathGenerator = new HexTreePathGenerator();
    $pathGenerator->setDI($this);

    return $pathGenerator;
});


/**
 * Setting up the view component
 */
$di->setShared('helper', function () {
    return new \App\Tag\AppTag();
});


/**
 * Wiring up the dispatcher with the events manager
 */
$di->setShared('dispatcher', function () {
    $logger = $this->get('logger');

    $eventsManager = new EventsManager();

    $eventsManager->attach('dispatch:beforeException', function (Event $event, Dispatcher $dispatcher, Exception $exception) use ($logger) {
        /** @var FileLogger $logger */
        $logger->log('----- Request could not be dispatched -----', Logger::DEBUG);

        switch ($exception->getCode()) {
            // Handle 404
            case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
            case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                $dispatcher->setParam('throwable', $exception);
                $dispatcher->forward([
                    'controller' => 'error',
                    'action'     => 'error404',
                ]);

                return false;
        }
    });

    $eventsManager->attach('dispatch:beforeExecuteRoute', function(Event $event, Dispatcher $dispatcher) use ($logger) {
        /** @var FileLogger $logger */
        $logger->log(sprintf('----- Dispatching request to %s:%s -----',
            $dispatcher->getControllerName(),
            $dispatcher->getActionName()
        ), Logger::DEBUG);
    });

    $eventsManager->attach('dispatch:beforeForward', function(Event $event, Dispatcher $dispatcher) use ($logger) {
        /** @var FileLogger $logger */
        $logger->log(sprintf('----- Forwarding request to %s:%s -----',
            $dispatcher->getControllerName(),
            $dispatcher->getActionName()
        ), Logger::DEBUG);
    });

    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});


/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->get('config');

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->layout->viewsDir);

    $view->registerEngines([
        '.phtml' => PhpEngine::class,
        '.volt' => function ($view) use ($config) {
            $volt = new VoltEngine($view, $this);
            $volt->setOptions([
                'compiledPath' => $config->layout->tplCacheDir,
                'compiledSeparator' => '_',
            ]);

            if ($config->app->env === 'dev') {
                $volt->setOptions([
                    'stat' => true,
                    'compileAlways' => true,
                    'compiledPath' => $config->layout->tplCacheDir,
                    'compiledSeparator' => '_',
                ]);
            }

            return $volt;
        },
    ]);

    return $view;
});

/**
 * Used to render individual templates.
 */
$di->setShared('templating', function () {
    $config = $this->get('config');

    return new Templating($config->layout->cacheDir, $config->layout->viewsDir);
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->get('config');

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->db->adapter;
    $params = [
        'host'     => $config->db->host,
        'username' => $config->db->username,
        'password' => $config->db->password,
        'dbname'   => $config->db->dbname,
        'charset'  => $config->db->charset,
    ];

    if ($config->db->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    // Log database queries ??
    if ($config->db->log) {
        $eventsManager = $this->get('eventsManager');

        $logger = new FileLogger(sprintf('%s%s', $config->layout->logDir, $config->db->logName));
        $logger->setLogLevel($config->db->logLevel);

        $eventsManager->attach('db', function($event, $connection) use ($logger) {
            if ($event->getType() === 'beforeQuery') {
                $sqlStatement = $connection->getSqlStatement();
                $sqlVariables = $connection->getSQLVariables();

                if (is_array($sqlVariables) && count($sqlVariables)) {

                    // Replaces :p1, .. :p11 .. and defined binds with binded values
                    foreach ($connection->getSQLVariables() as $key => $val) {
                        $sqlStatement = preg_replace('/\?/', json_encode($val), $sqlStatement, 1);
                        $sqlStatement = preg_replace(sprintf('/:%s/', $key), json_encode($val), $sqlStatement, 1);
                    }
                }

                $logger->log($sqlStatement, Logger::INFO);
            }
        });

        // Assign the eventsManager to the db adapter instance
        $connection->setEventsManager($eventsManager);
    }

    return $connection;
});

/**
 * A persistent database connection used for scrappers
 * (otherwise scrap DB gets stuck)
 */
$di->setShared('pdb', function () {
    $config = $this->get('config');

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->db->adapter;
    $params = [
        'host'     => $config->db->host,
        'username' => $config->db->username,
        'password' => $config->db->password,
        'dbname'   => $config->db->dbname,
        'charset'  => $config->db->charset,
        'persistent' => true,
    ];

    return new $class($params);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelManager', function() {
    $manager = new ModelManager();
    $manager->setDI($this);
    $manager->setEventsManager($this->get('eventsManager'));

    return $manager;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * @see https://docs.phalconphp.com/en/latest/reference/models-cache.html
 */
$di->setShared('modelsCache', function () {
    $config = $this->get('config');

    // Cache data for one day by default
    $frontCache = new FrontendData([
        "lifetime" => $config->cache->lifetime,
    ]);

    // Memcached connection settings
    switch ($config->cache->backend) {
        case 'apc':
            $cache = new ApcBackend($frontCache, [
                "prefix" => $config->cache->prefix,
            ]);
            break;

        case 'file':
            $cache = new FileBackend($frontCache, [
                "cacheDir" => $config->cache->cacheDir,
            ]);
            break;

        case 'libmemcached':
            $cache = new LibmemcachedBackend($frontCache, [
                "host" => $config->cache->host,
                "port" => $config->cache->port,
                "weight" => $config->cache->weight,
            ]);
            break;

        case 'memcache':
            $cache = new MemcacheBackend($frontCache, [
                "host" => $config->cache->host,
                "port" => $config->cache->port,
            ]);
            break;

        default:
            throw new \RuntimeException(sprintf('Unsupported cache backend "%s".', $config->cache->backend));
    }

    return $cache;
});

/**
 * Register the entity manager
 */
$di->set('em', function () {
    $em = new EntityManager();
    $em->setDI($this);

    return $em;
});

/**
 * The ad finder service.
 */
$di->setShared('finder', function () {
    $em = new FallbackLocationFinder();
    $em->setDI($this);

    return $em;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash([
        'error'   => 'alert alert-error',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning',
    ]);
});


/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Default mailer service (which uses the default mailbox).
 *
 * @see https://github.com/phalcon-ext/mailer
 */
$di->setShared('mailer', function () {
    $config = $this->get('config');

    $manager = new MailerManager([
        'driver'     => $config->mailer->driver,
        'host'       => $config->mailer->host,
        'port'       => $config->mailer->port,
        'encryption' => $config->mailer->encryption,
        'username'   => $config->mailer->username,
        'password'   => $config->mailer->password,
    ]);

    if ($config->mailer->log) {
        $eventsManager = $this->get('eventsManager');

        $logger = new FileLogger(sprintf('%s%s', $config->layout->logDir, $config->mailer->logName));
        $logger->setLogLevel($config->mailer->logLevel);

        $eventsManager->attach('mailer:beforeSend', function(Event $event, MailerMessage $message) use ($config, $logger) {
            $logger->log(sprintf('About to send %s email [From: %s; To=%s; Subject=%s]',
                $message->getContentType(),
                implode(', ', array_keys($message->getFrom())),
                implode(', ', array_keys($message->getTo())),
                $message->getSubject()
            ), Logger::INFO);
        });

        $eventsManager->attach('mailer:afterSend', function(Event $event, MailerMessage $message, $data) use ($config, $logger) {
            $recipients = $data[0] ?? 0;
            $failedRecipients = $data[1] ?? [];

            $logger->log(sprintf('Sent %s email to %d recipients (%d failed recipients)',
                $message->getContentType(),
                $recipients,
                count($failedRecipients)
            ), Logger::INFO);
        });

        $manager->setEventsManager($eventsManager);
    }

    return new Mailer($manager);
});

/**
 * Crypt service (required in cookies).
 */
$di->set('crypt', function () {
    $config = $this->get('config');

    $crypt = new Crypt();
    $crypt->setKey($config->sex->key);

    return $crypt;
});

/**
 * Cookies service.
 *
 * @see https://docs.phalconphp.com/en/3.0.2/reference/cookies.html
 */
$di->setShared('cookies', function () {
    $cookies = new Cookies();
    $cookies->useEncryption(true);

    return $cookies;
});

/**
 * @see \App\Payment\Broker
 */
$di->setShared('broker', function () {
    $broker = new Broker();
    $broker->setDI($this);
    $broker->setEventsManager($this->get('eventsManager'));

    return $broker;
});

/**
 * @see \App\Sex\Hasher
 */
$di->setShared('hasher', function () {
    $config = $this->get('config');

    return new Hasher($config->sex->hashingAlgorithm);
});

/**
 * @see \App\Token\TokenGenerator
 */
$di->setShared('tokenGenerator', function () {
    $generator = new TokenGenerator();
    $generator->setDI($this);

    return $generator;
});

/**
 * @see \App\Promotion\Promoter
 */
$di->setShared('promoter', function () {
    $config = $this->get('config');

    $promoter = new Promoter(
        $config->promotion->creditsPerHour ?? 1.0,
        $config->promotion->rankUpdatePeriod ?? Promoter::MIN_UPDATE_PERIOD
    );
    $promoter->setDI($this);

    return $promoter;
});

/**
 * @see \App\Rank\Ranker
 */
$di->setShared('ranker', function () {
    $ranker = new RedditRanker();
    $ranker->setDI($this);

    return $ranker;
});

/**
 * @see \App\Captcha\ReCaptchaVerifier
 */
$di->setShared('recaptcha', function () {
    $config = $this->get('config');

    $verifier = new ReCaptchaVerifier($config->recaptcha->secretKey);
    $verifier->setDI($this);

    return $verifier;
});

/**
 * @see \App\Geo\GeoLocator
 */
$di->setShared('geolocator', function () {
    //$geolocator = new DemocraticGeoLocator();
    $geolocator = new FallbackGeoLocator();
    $geolocator->setDI($this);

    return $geolocator;
});

/**
 * @see \App\Geo\GeoCoder
 */
$di->setShared('geocoder', function () {
    $config = $this->get('config');
    $geocoder = new GoogleGeoCoder($config->geocoder->apiKey);

    return $geocoder;
});

/**
 * @ see \App\RabbitMQ\Queue
$di->setShared('queue', function () {
    $config = $this->get('config');

    $queue = new Queue();
    $queue->connect(
        $config->rabbitmq->host,
        $config->rabbitmq->port,
        $config->rabbitmq->username,
        $config->rabbitmq->password
    );

    foreach ($config->rabbitmq->queues as $name => $attr) {
        $queue->declareQueue(
            $name,
            $attr->passive,
            $attr->durable,
            $attr->exclussive,
            $attr->autoDelete,
            $attr->nowait
        );
    }

    return $queue;
});
 */

/**
 * @see \Imagine\Image\ImagineInterface
 */
$di->setShared('imagine', function () {
    $config = $this->get('config');

    switch ($config->media->adapter) {
        case 'gd':
            $adapter = new GdAdapter();
            break;

        case 'gmagick':
            $adapter = new GmagickAdapter();
            break;

        case 'imagick':
            $adapter = new ImagickAdapter();
            break;

        default:
            throw new \RuntimeException(sprintf('No such image adapter "%s".', $config->media->adapter));
    }

    return $adapter;
});

/**
 * @see \ImageOptimizer\Optimizer
 */
$di->setShared('optimizer', function () {
    $config = $this->get('config');
    $factory = new OptimizerFactory($config->media->optimizer->toArray());

    return $factory->get();
});

/**
 * @see \App\FFMpeg\FormatFactory
 */
$di->setShared('formatFactory', function () {
    $factory = new FormatFactory();
    $factory->setDI($this);

    return $factory;
});

/**
 * @see \FFMpeg\FFMpeg
 */
$di->setShared('ffmpeg', function () {
    $config = $this->get('config');

    return FFMpeg::create([
        'ffprobe.binaries' => $config->ffprobe->binaries,
        'ffprobe.timeout' => $config->ffprobe->timeout,
        'ffmpeg.binaries' => $config->ffmpeg->binaries,
        'ffmpeg.threads' => $config->ffmpeg->threads,
        'ffmpeg.timeout' => $config->ffmpeg->timeout,
    ]);
});

/**
 * @see \FFMpeg\FFProbe
 */
$di->setShared('ffprobe', function () {
    $config = $this->get('config');

    return FFProbe::create([
        'ffprobe.binaries' => $config->ffprobe->binaries,
        'ffprobe.timeout' => $config->ffprobe->timeout,
    ]);
});

/**
 * @see \App\FFMpeg\QtFaststart
 */
$di->setShared('qtfaststart', function () {
    $config = $this->get('config');

    return QtFaststart::create([
        'qtfaststart.binaries' => $config->qtfaststart->binaries,
    ]);
});

/**
 * Command runner.
 */
$di->setShared('runner', function () {
    $config = $this->get('config');

    $runner = new Runner($config->layout->binDir . 'cli');
    $runner->setDI($this);

    return $runner;
});

/**
 * @see \App\Sms\Sms
 * @see https://api.mensatek.com/apiMensatekv5.0.pdf
 * @see https://docs.instasent.com/1.0/sdks/php-sdk
 */
$di->setShared('sms', function () {
    $config = $this->get('config');

    switch (strtolower($config->sms->provider)) {
        case 'mensatek':
            $sms = new MensatekSms(
                $config->sms->email,
                $config->sms->password,
                $config->sms->version,
                $config->sms->https
            );
            break;

        case 'instasent':
            $sms = new InstasentSms();
            break;

        default:
            throw new \InvalidArgumentException();
    }

    $sms->setDI($this);
    $sms->setPlacebo($config->sms->placebo);
    $sms->setLenient($config->sms->lenient);
    $sms->setTimeout($config->sms->timeout);

    if ($config->sms->log) {
        $eventsManager = $this->get('eventsManager');

        $logger = new FileLogger(sprintf('%s%s', $config->layout->logDir, $config->sms->logName));
        $logger->setLogLevel($config->sms->logLevel);

        $eventsManager->attach('sms', function(/** @var \Phalcon\Events\Event $event */ $event, $manager) use ($logger) {
            $recipient = $event->getData();

            if ($event->getType() === 'beforeSend') {
                $logger->log(sprintf('Sending SMS to %s ...', $recipient), Logger::INFO);

            } else if ($event->getType() === 'afterSend') {
                $logger->log(sprintf('Sent SMS to %s ...', $recipient), Logger::INFO);
            }
        });

        $sms->setEventsManager($eventsManager);
    }

    return $sms;
});

/**
 * @see https://github.com/PayTpv/PAYTPV-XML-Bankstore
 * @see http://developers.paytpv.com/es/documentacion/bankstore
$di->setShared('tpv', function () {
    $config = $this->get('config');

    return new Tpv(
        $config->tpv->merchantCode,
        $config->tpv->password,
        $config->tpv->terminal,
        $config->tpv->jetId
    );
});
 */

/**
 * Default commands
 */
$di->setShared('cmd.check.env', function () {
    return new CheckEnvCommand();
});

$di->setShared('cmd.check.config', function () {
    return new CheckConfigCommand();
});

$di->setShared('cmd.check.mailer', function () {
    return new CheckMailerCommand();
});

$di->setShared('cmd.check.sms', function () {
    return new CheckSmsCommand();
});

$di->setShared('cmd.cron.edit', function () {
    return new EditCronCommand();
});

$di->setShared('cmd.cron.list', function () {
    return new ListCronCommand();
});

$di->setShared('cmd.cron.schedule', function () {
    return new ScheduleJobCommand();
});

$di->setShared('cmd.deploy.makefile', function () {
    return new GenerateMakefileCommand();
});

$di->setShared('cmd.deploy.manifest', function () {
    return new GenerateManifestCommand();
});

$di->setShared('cmd.deploy.package', function () {
    return new GeneratePackageJsonCommand();
});

$di->setShared('cmd.deploy.robots', function () {
    return new GenerateRobotsCommand();
});

$di->setShared('cmd.deploy.sitemap', function () {
    return new GenerateSitemapCommand();
});

$di->setShared('cmd.deploy.selfsignedcert', function () {
    return new GenerateSelfSignedCertCommand();
});

$di->setShared('cmd.deploy.vhost', function () {
    return new GenerateVhostCommand();
});
/*

$di->setShared('cmd.db.media.clean-orphan', function () {
    return new CleanOrphanMediaCommand();
});

$di->setShared('cmd.db.hits.truncate', function () {
    return new TruncateHitsCommand();
});

$di->setShared('cmd.db.score.compute', function () {
    return new ComputeScoreCommand();
});
*/

$di->setShared('cmd.db.schema.create', function () {
    return new CreateSchemaCommand();
});

$di->setShared('cmd.db.schema.drop', function () {
    return new DropSchemaCommand();
});

$di->setShared('cmd.db.fixtures.load', function () {
    return new LoadFixturesCommand();
});

$di->setShared('cmd.db.update.ad', function () {
    return new UpdateAdsCommand();
});

$di->setShared('cmd.db.update.stats', function () {
    return new UpdateStatsCommand();
});

$di->setShared('cmd.gc.ad', function () {
    return new CleanAdsCommand();
});

$di->setShared('cmd.gc.cache', function () {
    return new CleanCacheCommand();
});

$di->setShared('cmd.gc.media', function () {
    return new CleanMediaCommand();
});

$di->setShared('cmd.gc.payments', function () {
    return new CleanPaymentsCommand();
});

$di->setShared('cmd.gc.promotions', function () {
    return new CleanPromotionsCommand();
});

$di->setShared('cmd.publish.ad', function () {
    return new PublishAdCommand();
});

$di->setShared('cmd.publish.frames', function () {
    return new GenerateFramesCommand();
});

$di->setShared('cmd.publish.metadata', function () {
    return new ExtractMetadataCommand();
});

$di->setShared('cmd.publish.move', function () {
    return new MoveMediaCommand();
});

$di->setShared('cmd.publish.thumbnail', function () {
    return new GenerateThumbnailCommand();
});

$di->setShared('cmd.publish.thumbnails', function () {
    return new GenerateThumbnailsCommand();
});

$di->setShared('cmd.publish.transcode', function () {
    return new TranscodeMediaCommand();
});

$di->setShared('cmd.publish.optimize', function () {
    return new ImageOptimizeCommand();
});

$di->setShared('cmd.publish.watermark', function () {
    return new ImageWatermarkCommand();
});

$di->setShared('cmd.publish.watermarks', function () {
    return new GenerateWatermarksCommand();
});

$di->setShared('cmd.rank.ad', function () {
    return new RankCommand();
});

$di->setShared('cmd.rank.update', function () {
    return new UpdateRankCommand();
});

$di->setShared('cmd.scrap', function () {
    return new ScrapCommand();
});
