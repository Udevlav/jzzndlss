<?php

use Phalcon\Loader;

// See https://forum.phalconphp.com/discussion/2016/composer-and-autoloading

$loader = new Loader();
$config = $di->get('config');

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs([
    $config->layout->controllersDir,
    $config->layout->modelsDir,
    $config->layout->formsDir,
    $config->layout->libraryDir,
]);

$loader->registerNamespaces([
    'App\Captcha'           => $config->layout->appDir .'Captcha',
    'App\Cli'               => $config->layout->appDir .'Cli',
    'App\Command'           => $config->layout->appDir .'Command',
    'App\Config'            => $config->layout->appDir .'Config',
    'App\Controller'        => $config->layout->appDir .'Controller',
    'App\Cron'              => $config->layout->appDir .'Cron',
    'App\FFMpeg'            => $config->layout->appDir .'FFMpeg',
    'App\Form'              => $config->layout->appDir .'Form',
    'App\Filter'            => $config->layout->appDir .'Filter',
    'App\Finder'            => $config->layout->appDir .'Finder',
    'App\Session'           => $config->layout->appDir .'Session',
    'App\Geo'               => $config->layout->appDir .'Geo',
    'App\Hash'              => $config->layout->appDir .'Hash',
    'App\I18n'              => $config->layout->appDir .'I18n',
    'App\IO'                => $config->layout->appDir .'IO',
    'App\Locale'            => $config->layout->appDir .'Locale',
    'App\Mailer'            => $config->layout->appDir .'Mailer',
    'App\Manager'           => $config->layout->appDir .'Manager',
    'App\Model'             => $config->layout->appDir .'Model',
    'App\Payment'           => $config->layout->appDir .'Payment',
    'App\Promotion'         => $config->layout->appDir .'Promotion',
    'App\RabbitMQ'          => $config->layout->appDir .'RabbitMQ',
    'App\Repository'        => $config->layout->appDir .'Repository',
    'App\Rank'              => $config->layout->appDir .'Rank',
    'App\Runner'            => $config->layout->appDir .'Runner',
    'App\Scrap'             => $config->layout->appDir .'Scrap',
    'App\Script'            => $config->layout->appDir .'Script',
    'App\Sex'               => $config->layout->appDir .'Sex',
    'App\Sitemap'           => $config->layout->appDir .'Sitemap',
    'App\Sms'               => $config->layout->appDir .'Sms',
    'App\Tag'               => $config->layout->appDir .'Tag',
    'App\Token'             => $config->layout->appDir .'Token',
    'App\Util'              => $config->layout->appDir .'Util',
    'App\View'              => $config->layout->appDir .'View',
    'App\Validation'        => $config->layout->appDir .'Validation',

    'Alchemy'               => $config->layout->vendorDir .'alchemy/binary-driver/src/Alchemy',
    'Doctrine'              => $config->layout->vendorDir .'doctrine/cache/lib/Doctrine',
    'Doctrine\Common\Lexer' => $config->layout->vendorDir .'doctrine/lexer/lib/Doctrine/Common/Lexer',
    'Egulias\EmailValidator'
                            => $config->layout->vendorDir .'egulias/email-validator/EmailValidator',
    'Evenement'             => $config->layout->vendorDir .'evenement/evenement/src/Evenement',
    'Faker'                 => $config->layout->vendorDir .'fzaninotto/faker/src/Faker',
    'FFMpeg'                => $config->layout->vendorDir .'php-ffmpeg/php-ffmpeg/src/FFMpeg',
    'GeoIp2'                => $config->layout->vendorDir .'geoip2/geoip2/src',
    'Imagine'               => $config->layout->vendorDir .'imagine/imagine/lib/Imagine',
    'ImageOptimizer'        => $config->layout->vendorDir .'ps/image-optimizer/src',
    'Instasent'             => $config->layout->vendorDir .'instasent/instasent-php-lib/src/Instasent',
    'MaxMind'               => $config->layout->vendorDir .'maxmind-db/reader/src/MaxMind',
    'Monolog'               => $config->layout->vendorDir .'monolog/monolog/src/Monolog',
    'PhpAmqpLib'            => $config->layout->vendorDir .'php-amqplib/php-amqplib/PhpAmqpLib',
    'Neutron\TemporaryFilesystem'
                            => $config->layout->vendorDir .'neutron/temporary-filesystem/src/Neutron/TemporaryFilesystem',
    'Phalcon\Ext\Mailer'    => $config->layout->vendorDir .'phalcon-ext/mailer/src',
    'Psr'                   => $config->layout->vendorDir .'psr/log/Psr',
    'Symfony\Component\Process'
                            => $config->layout->vendorDir .'symfony/process',
    'Symfony\Component\Filesystem'
                            => $config->layout->vendorDir .'symfony/filesystem',
    'libphonenumber'        => $config->layout->vendorDir .'giggsey/libphonenumber-for-php/src',
    'thiagoalessio'         => $config->layout->vendorDir .'thiagoalessio/tesseract_ocr/src',
]);

$loader->register();

// Swift uses its own loader
require_once BASE_PATH .'/vendor/swiftmailer/swiftmailer/lib/swift_required.php';