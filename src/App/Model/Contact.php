<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class Contact extends Model implements Entity
{

    public $id;

    public $email;
    public $text;

    public $createdAt;
    public $updatedAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'email' => 'email',
            'text' => 'text',
            'created_at' => 'createdAt',
            'updated_at' => 'updatedAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_contact');

        $this->useDynamicUpdate(true);
    }
}