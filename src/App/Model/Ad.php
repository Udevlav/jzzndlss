<?php

namespace App\Model;

use App\Util\StringUtil;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;

class Ad extends Model implements Entity
{

    const POSTER_FALLBACK = '';

    public $id;

    public $advertiserId;
    public $experiences = [];
    public $images = [];
    public $videos = [];
    public $tags = [];
    public $promos = [];
    public $promoted = false;

    public $hash;
    public $slug;
    public $score;
    public $rating;
    public $provider;
    public $category;
    public $poster;
    public $layout;
    public $title;
    public $text;

    public $name;
    public $age;
    public $email;
    public $phone;
    public $twitter;
    public $whatsapp;
    public $agency;
    public $independent;

    public $lat;
    public $lng;
    public $country;
    public $postalCode;
    public $city;
    public $zone;

    public $fee;
    public $fees;
    public $cash;
    public $card;
    public $availability;
    public $ethnicity;
    public $origin;
    public $occupation;
    public $schooling;
    public $languages;
    public $hairColor;
    public $bsize;
    public $height;
    public $weight;
    public $numExperiences = 0;
    public $numImages = 0;
    public $numVideos = 0;
    public $numHits = 0;
    public $allTags;

    public $createdAt;
    public $createdFrom;
    public $updatedAt;
    public $updatedFrom;
    public $publishedAt;
    public $promotedAt;
    public $lastHitAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'advertiser_id' => 'advertiserId',
            'hash' => 'hash',
            'slug' => 'slug',
            'provider' => 'provider',
            'category' => 'category',
            'poster' => 'poster',
            'layout' => 'layout',
            'title' => 'title',
            'text' => 'text',
            'name' => 'name',
            'age' => 'age',
            'email' => 'email',
            'phone' => 'phone',
            'twitter' => 'twitter',
            'whatsapp' => 'whatsapp',
            'agency' => 'agency',
            'independent' => 'independent',
            'lat' => 'lat',
            'lng' => 'lng',
            'country' => 'country',
            'postal_code' => 'postalCode',
            'city' => 'city',
            'zone' => 'zone',
            'fee' => 'fee',
            'fees' => 'fees',
            'cash' => 'cash',
            'card' => 'card',
            'availability' => 'availability',
            'ethnicity' => 'ethnicity',
            'origin' => 'origin',
            'occupation' => 'occupation',
            'schooling' => 'schooling',
            'languages' => 'languages',
            'hair_color' => 'hairColor',
            'bsize' => 'bsize',
            'height' => 'heigth',
            'weight' => 'weight',
            'score' => 'score',
            'rating' => 'rating',
            'num_hits' => 'numHits',
            'num_images' => 'numImages',
            'num_videos' => 'numVideos',
            'num_experiences' => 'numExperiences',
            'all_tags' => 'allTags',
            'created_at' => 'createdAt',
            'created_from' => 'createdFrom',
            'updated_at' => 'updatedAt',
            'updated_from' => 'updatedFrom',
            'promoted_at' => 'promotedAt',
            'published_at' => 'publishedAt',
            'last_hit_at' => 'lastHitAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_ad');

        $this->useDynamicUpdate(true);

        $this->belongsTo('advertiserId', Advertiser::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);

        $this->hasMany('id', AdTag::class, 'adId', [
            'alias' => 'tags',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);

        $this->hasMany('id', Experience::class, 'adId', [
            'alias' => 'experiences',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);

        $this->hasMany('id', Hit::class, 'adId', [
            'alias' => 'hits',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);

        $this->hasMany('id', Media::class, 'adId', [
            'alias' => 'medias',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);

        $this->hasMany('id', Promotion::class, 'adId', [
            'alias' => 'promotions',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);
    }

    public function beforeSave() {
        $this->generateSlug();
        $this->generateHash();
        $this->generateWhatsapp();
        $this->generateIndependent();

        $this->fixAnyTypeUnawareFrameworkOrmBehaviour();
        $this->serializeColumns();
    }

    public function afterFetch() {
        $this->unserializeColumns();
    }

    public function afterSave() {
        $this->unserializeColumns();
    }

    public function validation()
    {
        $validator = new Validation();
        $validator
            ->add('slug', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('slug', new Uniqueness([
                'message' => 'validation.ad_slug_uniqueness',
            ]))
            ->add('category', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('provider', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('title', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('text', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('name', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('age', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('email', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('phone', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('lat', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('lng', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('country', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('postalCode', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('city', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('zone', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('fee', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('availability', new PresenceOf([
                'message' => 'validation.required',
            ]));

        return $this->validate($validator);
    }


    /**
     * Return the related promotions
     *
     * @param mixed $parameters
     *
     * @return ResultsetInterface | Promotion[]
     */
    public function getPromotions($parameters = null)
    {
        return $this->getRelated('promotions', $parameters);
    }

    /**
     * Return the related experiences
     *
     * @param mixed $parameters
     *
     * @return ResultsetInterface | Promotion[]
     */
    public function getExperiences($parameters = null)
    {
        return $this->getRelated('experiences', $parameters);
    }


    protected function generateSlug()
    {
        if (null !== $this->title) {
            $this->slug = StringUtil::sluggify($this->title);
        }
    }

    public function generateHash()
    {
        if (null !== $this->name && null !== $this->phone) {
            $phone = $this->phone;

            if (! empty($phone) && '/' === $phone[0]) {
                $phone = '000000000';
            }

            $this->hash = sprintf('%s-%s-%s',
                StringUtil::sluggify($this->name),
                str_replace('/([^0-9]+)/', '', $phone),
                substr(md5($this->name . $this->phone), -8)
            );
        }
    }

    protected function generateWhatsapp()
    {
        $this->whatsapp = (int) $this->whatsapp;
    }

    protected function generateIndependent()
    {
        $this->independent = (int) $this->independent;
    }

    protected function generateFee()
    {
        if (is_array($this->fees)) {
            $this->fee = \PHP_INT_MAX;

            foreach ($this->fees as $fee) {
                if (! isset($fee->$fee)) {
                    continue;
                }

                if ($fee->fee < $this->fee) {
                    $this->fee = $fee->fee;
                }
            }

            if (\PHP_INT_MAX === $this->fee) {
                $this->fee = 0.0;
            }
        }
    }

    public function fixAnyTypeUnawareFrameworkOrmBehaviour()
    {
        if (null === $this->cash) {
            $this->cash = (bool) $this->cash;
        }
        if (null === $this->card) {
            $this->card = (bool) $this->card;
        }
    }

    public function serializeColumns()
    {
        if (null !== $this->availability && ! is_string($this->availability)) {
            $this->availability = json_encode($this->availability);
        }

        if (null !== $this->fee && ! is_string($this->fees)) {
            $this->fees = json_encode($this->fees);
        }

        if (null !== $this->allTags && ! is_string($this->allTags)) {
            $this->allTags = json_encode($this->allTags);
        }
    }

    public function unserializeColumns()
    {
        if (is_string($this->availability)) {
            $this->unserializeAvailability();
        }

        if (is_string($this->fees)) {
            $this->unserializeFees();
        }

        if (is_string($this->allTags)) {
            $this->unserializeAllTags();
        }
    }

    protected function unserializeAvailability()
    {
        $availability = json_decode($this->availability);

        if (! is_array($availability)) {
            $availability = [];
        }

        $this->availability = [];

        foreach ($availability as $item) {
            $this->availability[] = new Availability($item);
        }
    }

    protected function unserializeFees()
    {
        $fees = json_decode($this->fees);

        if (! is_array($fees)) {
            $fees = [];
        }

        $this->fees = [];

        foreach ($fees as $fee) {
            $this->fees[] = new Fee($fee);
        }
    }

    protected function unserializeAllTags()
    {
        $tags = json_decode($this->allTags);

        if (! is_array($tags)) {
            $tags = [];
        }

        $this->allTags = [];

        foreach ($tags as $item) {
            $tag = new Tag();
            $tag->id = $item->id;
            $tag->gid = $item->gid;
            $tag->slug = $item->slug;
            $tag->name = $item->name;

            $this->allTags[] = $tag;
        }
    }

    public function getCaption(bool $includePhone = true)
    {
        $caption= strval($this->name);

        if ($this->age > 0) {
            $caption .= sprintf(', %d años', $this->age);
        }

        if ($includePhone && $this->phone && $this->phone !== '000000000') {
            $caption .= '. '. $this->phone;
        }

        return $caption;
    }

    public function getSeoLabel()
    {
        $label = strval($this->name);

        if (! empty($this->category)) {
            $label .= ' | '. ucfirst(Category::superName(intval($this->category)));
        }

        if (! empty($this->city)) {
            $label .= ' | '. strval($this->city);
        }

        if (! empty($this->phone) && $this->phone !== '000000000' && '/' !== $this->phone[0]) {
            $label .= ' | '. strval($this->phone);
        }

        return $label;
    }

    public function isPublished(): bool
    {
        if (empty($this->publishedAt)) {
            return false;
        }

        return $this->publishedAt >= time();
    }

    public function isPortrait(): bool
    {
        return Layout::PORTRAIT === $this->layout;
    }

    public function isLandscape(): bool
    {
        return Layout::LANDSCAPE === $this->layout;
    }
}
