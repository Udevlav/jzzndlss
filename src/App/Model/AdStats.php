<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class AdStats extends Model implements Entity
{

    public $id;

    public $adId;
    public $measuredAt;
    public $numDaysOnTop;
    public $numImpressions;
    public $numPromotions;
    public $numPhoneClicks;
    public $numVisits;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'measured_at' => 'measuredAt',
            'num_days_on_top' => 'numDaysOnTop',
            'num_impressions' => 'numImpressions',
            'num_promotions' => 'numPromotions',
            'num_phone_clicks' => 'numPhoneClicks',
            'num_visits' => 'numVisits',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_ad_stats');
        $this->useDynamicUpdate(true);
        $this->belongsTo('adId', Ad::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }
}