<?php

namespace App\Model;

final class ChatMessageDirection implements Enumeration
{

    const PEER_TO_ADVERTISER = 0;
    const ADVERTISER_TO_PEER = 1;
}