<?php

namespace App\Model;

use App\Util\StringUtil;

final class Province
{

    static public $postalCodeToName = [
        '15' => 'A Coruña',
        '01' => 'Álava',
        '02' => 'Albacete',
        '03' => 'Alicante',
        '04' => 'Almería',
        '33' => 'Asturias',
        '05' => 'Ávila',
        '06' => 'Badajoz',
        '07' => 'Baleares',
        '08' => 'Barcelona',
        '48' => 'Bizkaia',
        '09' => 'Burgos',
        '10' => 'Cáceres',
        '11' => 'Cádiz',
        '39' => 'Cantabria',
        '12' => 'Castellón',
        '51' => 'Ceuta',
        '13' => 'Ciudad Real',
        '14' => 'Córdoba',
        '16' => 'Cuenca',
        '17' => 'Girona',
        '18' => 'Granada',
        '19' => 'Guadalajara',
        '20' => 'Guipuzkoa',
        '21' => 'Huelva',
        '22' => 'Huesca',
        '23' => 'Jaén',
        '26' => 'La Rioja',
        '35' => 'Las Palmas',
        '24' => 'León',
        '25' => 'Lleida',
        '27' => 'Lugo',
        '28' => 'Madrid',
        '29' => 'Málaga',
        '52' => 'Melilla',
        '30' => 'Murcia',
        '31' => 'Navarre',
        '32' => 'Ourense',
        '34' => 'Palencia',
        '36' => 'Pontevedra',
        '37' => 'Salamanca',
        '38' => 'Santa Cruz de Tenerife',
        '40' => 'Segovia',
        '41' => 'Sevilla',
        '42' => 'Soria',
        '43' => 'Tarragona',
        '44' => 'Teruel',
        '45' => 'Toledo',
        '46' => 'Valencia',
        '47' => 'Valladolid',
        '49' => 'Zamora',
        '50' => 'Zaragoza',
    ];

    static public $postalCodeToSlug = [
        '01' => 'alava',
        '02' => 'albacete',
        '03' => 'alicante',
        '04' => 'almeria',
        '05' => 'avila',
        '06' => 'badajoz',
        '07' => 'baleares',
        '08' => 'barcelona',
        '09' => 'burgos',
        '10' => 'caceres',
        '11' => 'cadiz',
        '12' => 'castellon',
        '13' => 'ciudad-real',
        '14' => 'cordoba',
        '15' => 'a-coruna',
        '16' => 'cuenca',
        '17' => 'girona',
        '18' => 'granada',
        '19' => 'guadalajara',
        '20' => 'guipuzkoa',
        '21' => 'huelva',
        '22' => 'huesca',
        '23' => 'jaen',
        '24' => 'leon',
        '25' => 'lleida',
        '26' => 'la-rioja',
        '27' => 'lugo',
        '28' => 'madrid',
        '29' => 'malaga',
        '30' => 'murcia',
        '31' => 'navarre',
        '32' => 'ourense',
        '33' => 'asturias',
        '34' => 'palencia',
        '35' => 'las-palmas',
        '36' => 'pontevedra',
        '37' => 'salamanca',
        '38' => 'santa-cruz-de-tenerife',
        '39' => 'cantabria',
        '40' => 'segovia',
        '41' => 'sevilla',
        '42' => 'soria',
        '43' => 'tarragona',
        '44' => 'teruel',
        '45' => 'toledo',
        '46' => 'valencia',
        '47' => 'valladolid',
        '48' => 'bizkaia',
        '49' => 'zamora',
        '50' => 'zaragoza',
        '51' => 'ceuta',
        '52' => 'melilla',
    ];

    static public $regionToPostalCode = [
        'madrid' => '28',
        'catalunya' => '08',
        'catalonia' => '08',
        'cataluña' => '08',
    ];

    static public $regionCodeToPostalCode = [
        'an' => '41',
        'ar' => '50',
        'as' => '33',
        'cn' => '38',
        'cb' => '39',
        'cm' => '02',
        'cl' => '45',
        'ct' => '08',
        'ex' => '06',
        'ga' => '15',
        'ib' => '07',
        'ri' => '26',
        'md' => '28',
        'mc' => '30',
        'nc' => '31',
        'pv' => '20',
        'vc' => '46',
    ];

    static private function nameToPostalCode()
    {
        static $nameToPostalCode;

        if (null === $nameToPostalCode) {
            $nameToPostalCode = array_flip(static::$postalCodeToName);
        }

        return $nameToPostalCode;
    }

    static private function slugToPostalCode()
    {
        static $slugToPostalCode;

        if (null === $slugToPostalCode) {
            $slugToPostalCode = array_flip(static::$postalCodeToSlug);
        }

        return $slugToPostalCode;
    }


    /**
     * @param string $name
     *
     * @return string | null
     */
    static public function postalCode(string $name)
    {
        $map = static::nameToPostalCode();

        if (isset($map[$name])) {
            return $map[$name];
        }

        return self::postalCodeBySlug(StringUtil::sluggify($name));
    }

    /**
     * @param string $slug
     *
     * @return string | null
     */
    static public function postalCodeBySlug(string $slug)
    {
        $map = static::slugToPostalCode();

        if (isset($map[$slug])) {
            return $map[$slug];
        }

        return null;
    }

    /**
     * @param string $region
     *
     * @return string | null
     */
    static public function postalCodeByRegion(string $region)
    {
        $map = static::$regionToPostalCode;

        if (isset($map[strtolower($region)])) {
            return $map[strtolower($region)];
        }

        return null;
    }

    /**
     * @param string $regionCode
     *
     * @return string | null
     */
    static public function postalCodeByRegionCode(string $regionCode)
    {
        $map = static::$regionCodeToPostalCode;

        if (isset($map[strtolower($regionCode)])) {
            return $map[strtolower($regionCode)];
        }

        return null;
    }

    /**
     * @param string $postalCode
     *
     * @return string | null
     */
    static public function name(string $postalCode)
    {
        $provinceCode = substr($postalCode, 0, 2);

        return isset(static::$postalCodeToName[$provinceCode]) ? static::$postalCodeToName[$provinceCode] : null;
    }

    /**
     * @param string $postalCode
     *
     * @return string | null
     */
    static public function slug(string $postalCode)
    {
        if (strlen($postalCode) > 2) {
            $postalCode = substr($postalCode, 0, 2);
        }

        return static::$postalCodeToSlug[$postalCode] ?? null;
    }
}
