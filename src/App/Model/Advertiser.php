<?php

namespace App\Model;

use App\Token\Token;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * This should be is a private table, as so it should be managed internally.
 */
class Advertiser extends Model implements Entity
{

    public $id;

    public $ads;
    public $payments;
    public $chatMessages;

    public $name;
    public $age;
    public $email;
    public $phone;
    public $fingerprints;

    public $credits = 0;
    public $numAds = 0;
    public $token;
    public $tokenValidFrom;
    public $tokenValidTo;

    public $createdAt;
    public $updatedAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'name' => 'name',
            'age' => 'age',
            'email' => 'email',
            'phone' => 'phone',
            'fingerprints' => 'fingerprints',
            'credits' => 'credits',
            'num_ads' => 'numAds',
            'token' => 'token',
            'token_valid_from' => 'tokenValidFrom',
            'token_valid_to' => 'tokenValidTo',
            'created_at' => 'createdAt',
            'updated_at' => 'updatedAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_advertiser');

        $this->hasMany('id', Ad::class, 'advertiserId', [
            'alias' => 'ads',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ],
        ]);

        $this->hasMany('id', Payment::class, 'paymentId', [
            'alias' => 'payments',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ],
        ]);

        $this->hasMany('id', ChatMessage::class, 'advertiserId', [
            'alias' => 'chatMessages',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ],
        ]);
    }

    public function beforeSave() {
        $this->serializeFingerprints();
    }

    public function afterFetch() {
        $this->unserializeFingerprints();
    }

    public function afterSave() {
        $this->unserializeFingerprints();
    }


    public function isTokenValid(): bool
    {
        $token = Token::of($this->token, $this->tokenValidFrom, $this->tokenValidTo);

        return $token->isValid();
    }

    public function hasCredits(): bool
    {
        return $this->credits > 0;
    }

    public function serializeFingerprints()
    {
        if (is_array($this->fingerprints)) {
            $this->fingerprints = implode(',', $this->fingerprints);
        }
    }

    public function unserializeFingerprints()
    {
        if (is_string($this->fingerprints)) {
            $this->fingerprints = preg_split('/\s*,\s*/', $this->fingerprints);
        }
    }
}