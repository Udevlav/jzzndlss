<?php

namespace App\Model;

/**
 * An enumeration of complaint causes.
 */
final class ComplaintCause implements Enumeration
{

    /**
     * @var string
     */
    const OTHER = 0;

    /**
     * Fake Ad content (e.g., images.)
     *
     * @var string
     */
    const FAKE_CONTENT = 1;

    /**
     * Illegal/Inappropriate Ad content.
     *
     * @var string
     */
    const INAPPROPRIATE_CONTENT = 2;

    /**
     * Invalid or errata in Ad's contact data.
     *
     * @var string
     */
    const INVALID_CONTACT_DATA = 3;

    /**
     * Ad is not properly classified.
     *
     * @var string
     */
    const BAD_CLASSIFICATION = 3;


    static public function constants(): array
    {
        return [
            self::OTHER                 => 'complaint_cause.other',
            self::FAKE_CONTENT          => 'complaint_cause.fake_content',
            self::INAPPROPRIATE_CONTENT => 'complaint_cause.inappropriate_content',
            self::INVALID_CONTACT_DATA  => 'complaint_cause.invalid_contact_data',
            self::BAD_CLASSIFICATION    => 'complaint_cause.bad_classification',
        ];
    }
}