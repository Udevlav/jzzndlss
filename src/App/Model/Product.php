<?php

namespace App\Model;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

class Product extends Model implements Entity
{

    public $id;

    public $label;
    public $description;
    public $savings;
    public $creditsGranted;
    public $promotions;
    public $dayspan;
    public $price;
    public $active = true;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'label' => 'label',
            'description' => 'description',
            'savings' => 'savings',
            'credits_granted' => 'creditsGranted',
            'promotions' => 'promotions',
            'dayspan' => 'dayspan',
            'price' => 'price',
            'active' => 'active',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_product');

        $this->useDynamicUpdate(true);

        $this->hasMany('id', Payment::class, 'productId', [
            'alias' => 'payments',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);
    }
}
