<?php

namespace App\Model;

final class French implements Enumeration
{
    // chica|chico|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales
    const CAT_1 = 1;
    const CAT_2 = 2;
    const CAT_3 = 3;

    static public function constants(): array
    {
        return [
            'Francés completo',
            'Francés con',
            'Francés natural',
        ];
    }
}