<?php

namespace App\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

use DateTime;
use Throwable;

class OnlineAdvertiser extends Model implements Entity
{

    public $id;
    public $advertiserId;
    public $advertiserSid;
    public $lastActivityAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'advertiser_id' => 'advertiserId',
            'advertiser_sid' => 'advertiserSid',
            'last_activity_at' => 'lastActivityAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_online_advertiser');

        $this->useDynamicUpdate(true);

        $this->belongsTo('advertiserId', Advertiser::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }

    public function validation()
    {
        $validator = new Validation();
        $validator
            ->add('advertiserId', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('advertiserSid', new PresenceOf([
                'message' => 'validation.required',
            ]))
        ;

        return $this->validate($validator);
    }

    public function getLastActivityAt(): DateTime
    {
        if (empty($this->lastActivityAt)) {
            return new DateTime();
        }

        try {
            return DateTime::createFromFormat('Y-m-d H:i:s', $this->lastActivityAt);
        } catch (Throwable $t) {
            return new DateTime();
        }
    }
}