<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class Complaint extends Model implements Entity
{

    public $id;

    public $adId;

    public $email;
    public $cause;
    public $text;

    public $createdAt;
    public $updatedAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'email' => 'email',
            'cause' => 'cause',
            'text' => 'text',
            'created_at' => 'createdAt',
            'updated_at' => 'updatedAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_complaint');

        $this->useDynamicUpdate(true);

        $this->belongsTo('adId', Ad::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }
}