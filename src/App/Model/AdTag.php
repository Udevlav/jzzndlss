<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class AdTag extends Model implements Entity
{

    public $id;

    public $adId;
    public $tagId;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'tag_id' => 'tagId',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_ad_tag');

        $this->useDynamicUpdate(true);

        $this->belongsTo('adId', Ad::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);

        $this->belongsTo('tagId', Tag::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }
}