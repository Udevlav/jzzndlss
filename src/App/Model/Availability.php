<?php

namespace App\Model;

class Availability
{
    public $day = -1;
    public $from;
    public $to;
    public $timerange;

    public function __construct($obj)
    {
        if ('object' === gettype($obj)) {
            if (isset($obj->day)) {
                $this->setDay($obj->day);
            }
            if (isset($obj->from) && isset($obj->to)) {
                $this->setTimeRange($obj->from, $obj->to);
            }
        }
    }

    public function setDay($day)
    {
        if (is_numeric($day)) {
            $this->day = (int) $day;
        } else {
            $this->day = null;
        }
    }

    public function setTimeRange($from, $to)
    {
        $this->from = $this->parseTime($from);
        $this->to = $this->parseTime($to);

        if ($this->from && $this->to) {
            $this->timerange = $this->from .' - '. $this->to;

        } else if ($this->from) {
            $this->timerange = 'Desde '. $this->from;

        } else if ($this->to) {
            $this->timerange = 'Hasta '. $this->from;

        } else {
            $this->timerange = 'Cualquier hora';
        }
    }

    protected function parseTime($time)
    {
        if (! preg_match('/^(\d{1,2}):(\d{2})$/', $time, $match)) {
            return null;
        }

        return sprintf('%02d:%02d', $match[1], $match[2]);
    }
}