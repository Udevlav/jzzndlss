<?php

namespace App\Model;

final class MediaSize implements Enumeration
{

    /**
     * @var string
     */
    const EXTRA_SMALL = 'xs';

    /**
     * @var string
     */
    const SMALL = 'sm';

    /**
     * @var string
     */
    const MEDIUM = 'md';

    /**
     * @var string
     */
    const LARGE = 'lg';

    /**
     * @var string
     */
    const EXTRA_LARGE = 'xl';


    private $size;


    public function __construct(string $size)
    {
        $this->size = strtolower($size);
    }

    public function __toString()
    {
        return $this->size;
    }


    public function isStandard(): bool
    {
        return (
            $this->isExtraSmall() ||
            $this->isSmall() ||
            $this->isMedium() ||
            $this->isLarge() ||
            $this->isExtraLarge()
        );
    }

    public function isCustom(): bool
    {
        return ! $this->isStandard();
    }

    public function isExtraSmall(): bool
    {
        return self::EXTRA_SMALL === $this->size;
    }

    public function isSmall(): bool
    {
        return self::SMALL === $this->size;
    }

    public function isMedium(): bool
    {
        return self::MEDIUM === $this->size;
    }

    public function isLarge(): bool
    {
        return self::LARGE === $this->size;
    }

    public function isExtraLarge(): bool
    {
        return self::EXTRA_LARGE === $this->size;
    }


    public function equals($that): bool
    {
        if (! ($that instanceof static)) {
            return false;
        }

        return $this->size === $that->size;
    }

    public function compareTo($that): int
    {
        if (! ($that instanceof static)) {
            throw new \InvalidArgumentException();
        }

        return $this->size <=> $that->size;
    }


    public function isLessThan($that): int
    {
        return -1 === $this->compareTo($that);
    }

    public function isLessThanOrEqualTo($that): int
    {
        return 0 >= $this->compareTo($that);
    }

    public function isEqualTo($that): int
    {
        return 0 === $this->compareTo($that);
    }

    public function isGreaterThanOrEqualTo($that): int
    {
        return 0 <= $this->compareTo($that);
    }

    public function isGreaterThan($that): int
    {
        return 1 === $this->compareTo($that);
    }
}