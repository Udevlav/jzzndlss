<?php

namespace App\Model;

use Phalcon\Mvc\Model;

/**
 * This should be is a private table, as inserting records in this table will
 * definitely promote an ad. Thus, promotions should be managed internally.
 */
class Promotion extends Model implements Entity
{

    public $id;

    public $adId;
    public $productId;
    public $paymentId;
    public $validFrom;
    public $validTo;
    public $bonus;
    public $credits;
    public $timestamps;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'product_id' => 'productId',
            'payment_id' => 'paymentId',
            'valid_from' => 'validFrom',
            'valid_to' => 'validTo',
            'bonus' => 'bonus',
            'credits' => 'credits',
            'timestamps' => 'timestamps',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_promotion');

        $this->useDynamicUpdate(true);

        $this->belongsTo('adId', Ad::class, 'id', [
            'reusable' => true,
        ]);
    }

    public function isActive(): bool
    {
        $now = time();
        $start = strtotime($this->validFrom);
        $end   = strtotime($this->validTo);

        return $now >= $start && $now <= $end;
    }

    public function beforeSave() {
        $this->serializeColumns();
    }

    public function afterFetch() {
        $this->unserializeColumns();
    }

    public function afterSave() {
        $this->unserializeColumns();
    }

    public function serializeColumns()
    {
        if (! empty($this->timestamps) && is_array($this->timestamps)) {
            $this->timestamps = implode(',', $this->timestamps);
        }
    }

    public function unserializeColumns()
    {
        if (! empty($this->timestamps) && ! is_array($this->timestamps)) {
            $this->timestamps = explode(',', $this->timestamps);
        }
    }

}
