<?php

namespace App\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class Experience extends Model implements Entity
{

    public $id;
    public $adId;
    public $text;
    public $author;
    public $rating;
    public $createdAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'text' => 'text',
            'author' => 'author',
            'rating' => 'rating',
            'created_at' => 'createdAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_experience');

        $this->useDynamicUpdate(true);

        $this->belongsTo('adId', Ad::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }

    public function validation()
    {
        $validator = new Validation();
        $validator
            ->add('text', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('author', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('rating', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ;

        return $this->validate($validator);
    }
}
