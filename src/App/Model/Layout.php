<?php

namespace App\Model;

final class Layout implements Enumeration
{
    const LANDSCAPE = 'LANDSCAPE';
    const PORTRAIT = 'PORTRAIT';
    const PORTRAIT_LORES = 'PORTRAIT_LORES';

    static public function constants(): array
    {
        return [
            self::LANDSCAPE => 'layout.landscape',
            self::PORTRAIT => 'layout.portrait',
            self::PORTRAIT_LORES => 'layout.portrait_lores',
        ];
    }
}