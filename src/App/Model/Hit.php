<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class Hit extends Model implements Entity
{

    public $id;

    public $adId;

    public $hitAt;
    public $fingerprint;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'hit_at' => 'hitAt',
            'fingerprint' => 'fingerprint',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_hit');

        $this->useDynamicUpdate(true);

        $this->belongsTo('adId', Ad::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }
}