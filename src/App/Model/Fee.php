<?php

namespace App\Model;

class Fee
{
    public $fee = 0.0;
    public $desc;

    public function __construct($obj)
    {
        if ('object' === gettype($obj)) {
            if (isset($obj->fee)) {
                $this->fee = $obj->fee;
            }
            if (isset($obj->desc)) {
                $this->desc = $obj->desc;
            }
        }
    }
}