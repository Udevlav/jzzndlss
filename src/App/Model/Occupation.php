<?php

namespace App\Model;

final class Occupation implements Enumeration
{

    static public function constants(): array
    {
        return [
            '',
            'Actrices porno',
            'Amas de casa',
            'Bailarinas',
            'Estudiantes',
            'Modelos',
            'Strippers',
        ];
    }
}