<?php

namespace App\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class ChatMessage extends Model implements Entity
{

    public $id;
    public $advertiserId;
    public $advertiserSid;
    public $peerSid;
    public $direction;
    public $text;
    public $sentAt;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'advertiser_id' => 'advertiserId',
            'advertiser_sid' => 'advertiserSid',
            'peer_sid' => 'peerSid',
            'direction' => 'direction',
            'text' => 'text',
            'sent_at' => 'sentAt',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_chat_message');

        $this->useDynamicUpdate(true);

        $this->belongsTo('advertiserId', Advertiser::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }

    public function validation()
    {
        $validator = new Validation();
        $validator
            ->add('advertiserId', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('advertiserSid', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('peerSid', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('direction', new PresenceOf([
                'message' => 'validation.required',
            ]))
            ->add('text', new PresenceOf([
                'message' => 'validation.required',
            ]))
        ;

        return $this->validate($validator);
    }

    public function toArray($columns = null)
    {
        return [
            'advertiserSid' => $this->advertiserSid,
            'peerSid' => $this->peerSid,
            'sentAt' => $this->sentAt,
            'text' => $this->text,
        ];
    }
}