<?php

namespace App\Model;

final class Gid implements Enumeration
{
    /**
     * @var int
     */
    const SERVICE_1 = 1;

    /**
     * @var int
     */
    const SERVICE_2 = 2;

    /**
     * @var int
     */
    const SERVICE_3 = 3;

    /**
     * @var int
     */
    const SERVICE_4 = 4;

    /**
     * @var int
     */
    const SERVICE_5 = 5;

    static public function constants(): array
    {
        return [
            self::SERVICE_1,
            self::SERVICE_2,
            self::SERVICE_3,
            self::SERVICE_4,
            self::SERVICE_5,
        ];
    }
}