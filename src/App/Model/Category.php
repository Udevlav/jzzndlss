<?php

namespace App\Model;

final class Category implements Enumeration
{
    // escort|gigolo|travesti|castings-y-plazas|alquiler-habitaciones|fotografos-profesionales
    const CAT_1 = 1;
    const CAT_2 = 2;
    const CAT_3 = 3;
    const CAT_4 = 4;
    const CAT_5 = 5;
    const CAT_6 = 6;

    static public function available(): array
    {
        return [
            self::CAT_1 => 'category.cat1',
            self::CAT_2 => 'category.cat2',
            self::CAT_3 => 'category.cat3',
        ];
    }

    static public function constants(): array
    {
        return [
            0           => 'category.cat1',
            self::CAT_1 => 'category.cat1',
            self::CAT_2 => 'category.cat2',
            self::CAT_3 => 'category.cat3',
            self::CAT_4 => 'category.cat4',
            self::CAT_5 => 'category.cat5',
            self::CAT_6 => 'category.cat6',
        ];
    }

    static public function superCategory($value)
    {
        switch (strval($value)) {
            case self::CAT_4:
                return 'category.super2';
            case self::CAT_5:
                return 'category.super3';
            case self::CAT_6:
                return 'category.super4';
            default:
                return 'category.super1';
        }
    }

    static public function name(int $value)
    {
        static $map;

        if (null === $map) {
            $map = [
                'escort', // 0 (unknown, defaults to chica)
                'escort',
                'gigoló',
                'travesti',
                'castings-y-plazas',
                'alquiler-habitaciones',
                'fotografos-profesionales',
            ];
        }

        return $map[$value] ?? $value;
    }

    static public function superName(int $value)
    {
        switch ($value) {
            case self::CAT_4:
                return 'castings';
            case self::CAT_5:
                return 'alquiler';
            case self::CAT_6:
                return 'fotógrafos';
            default:
                return 'escort';
        }
    }

    static public function fromSlug(string $slug)
    {
        static $map;

        if (null === $map) {
            $map = [
                'chica' => 1,
                'chico' => 2,
                'travesti' => 3,
                'castings-y-plazas' => 4,
                'alquiler-habitaciones' => 5,
                'fotografos-profesionales' => 6,
            ];
        }

        if (is_numeric($slug)) {
            return intval($slug);
        }

        return $map[$slug] ?? null;
    }
}