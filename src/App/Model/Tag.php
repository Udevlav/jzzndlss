<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class Tag extends Model implements Entity
{

    public $id;

    public $gid;
    public $name;
    public $slug;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'gid' => 'gid',
            'name' => 'name',
            'slug' => 'slug',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_tag');

        $this->useDynamicUpdate(true);

        $this->hasMany('id', AdTag::class, 'tagId');
    }
}