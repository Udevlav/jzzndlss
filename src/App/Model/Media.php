<?php

namespace App\Model;

use App\Config\Config;
use App\IO\PathGenerator;
use App\Util\Bytes;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

use stdClass;


class Media extends Model implements Entity
{

    public $id;

    public $adId;
    public $masterId;

    public $temp;
    public $poster;

    public $name;
    public $version;
    public $path;

    public $type;
    public $size;
    public $width;
    public $height;
    public $duration;
    public $timeCode;
    public $containerType;
    public $audioDesc;
    public $audioCodec;
    public $audioProfile;
    public $audioLevel;
    public $audioChannels;
    public $audioSampleRate;
    public $audioBitrate;
    public $videoDesc;
    public $videoCodec;
    public $videoProfile;
    public $videoLevel;
    public $videoBitrate;
    public $videoDar;
    public $videoPar;
    public $videoChromaSubsampling;

    public $createdAt;
    public $updatedAt;
    public $uploaderIp;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'ad_id' => 'adId',
            'master_id' => 'masterId',
            'temp' => 'temp',
            'poster' => 'poster',
            'name' => 'name',
            'version' => 'version',
            'path' => 'path',
            'type' => 'type',
            'size' => 'size',
            'width' => 'width',
            'height' => 'height',
            'duration' => 'duration',
            'time_code' => 'timeCode',
            'container_type' => 'containerType',
            'audio_desc' => 'audioDesc',
            'audio_codec' => 'audioCodec',
            'audio_profile' => 'audioProfile',
            'audio_level' => 'audioLevel',
            'audio_channels' => 'audioChannels',
            'audio_sample_rate' => 'audioSampleRate',
            'audio_bitrate' => 'audioBitrate',
            'video_desc' => 'videoDesc',
            'video_codec' => 'videoCodec',
            'video_profile' => 'videoProfile',
            'video_level' => 'videoLevel',
            'video_bitrate' => 'videoBitrate',
            'video_dar' => 'videoDar',
            'video_par' => 'videoPar',
            'video_cs' => 'videoChromaSubsampling',
            'created_at' => 'createdAt',
            'updated_at' => 'updatedAt',
            'uploader_ip' => 'uploaderIp',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_media');

        $this->useDynamicUpdate(true);

        $this->belongsTo('adId', Ad::class, 'id', [
            'reusable' => true,
        ]);

        $this->belongsTo('masterId', Media::class, 'id', [
            'reusable' => true,
        ]);

        $this->hasMany('versions', Media::class, 'id', [
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Cannot delete media, as it\'s used in a child media.',
            ],
        ]);
    }

    public function afterDelete()
    {
        if (empty($this->path)) {
            return;
        }

        if (file_exists($this->path)) {
            @unlink($this->path);

        } else {
            $config = $this->getDI()->get('config');

            if (file_exists(sprintf('%s%s', $config->layout->uploadDir, rtrim($this->path, '/')))) {
                @unlink($this->path);
            }
        }
    }

    public function isImage()
    {
        return null === $this->type ? false : (bool) preg_match('/^image\//', $this->type);
    }

    public function isStream(): bool
    {
        return null === $this->type ? false : (bool) preg_match('/^(audio|video)\//', $this->type);
    }

    public function isVideo(): bool
    {
        return null === $this->type ? false : (bool) preg_match('/^video\//', $this->type);
    }

    public function isFrame()
    {
        return null !== $this->version && (bool) preg_match('/frame-\d+/', $this->version);
    }

    public function isLandscape()
    {
        return is_numeric($this->width) && is_numeric($this->height) && intval($this->width) > intval($this->height);
    }

    public function isPortrait()
    {
        // Note this includes squares !
        return is_numeric($this->width) && is_numeric($this->height) && intval($this->width) <= intval($this->height);
    }

    public function isPortraitLoRes()
    {
        // Note this includes squares !
        return $this->isPortrait() && intval($this->width) < 1024;
    }

    public function getBytes()
    {
        return Bytes::format($this->size ?? 0);
    }
}