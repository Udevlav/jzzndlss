<?php

namespace App\Model;

final class Provider implements Enumeration
{
    const USER = 'user';
    const SCRAP = 'scrap';

    static public function constants(): array
    {
        return [
            self::USER,
            self::SCRAP,
        ];
    }
}