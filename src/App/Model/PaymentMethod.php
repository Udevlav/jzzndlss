<?php

namespace App\Model;

final class PaymentMethod implements Enumeration
{

    /**
     * @var string
     */
    const PAYPAL = 'paypal';

    /**
     * @var string
     */
    const CARD = 'card';
}