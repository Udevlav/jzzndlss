<?php

namespace App\Model;

use App\Promotion\Request;
use Phalcon\Mvc\Model;

use Throwable;
use stdClass;

class Payment extends Model implements Entity
{

    public $id;

    public $productId;
    public $advertiserId;
    public $adId;
    public $method;
    public $amount;
    public $promoCode;
    public $creditsGranted;
    public $createdAt;
    public $committedAt;
    public $request;
    public $result;
    public $data;

    public function columnMap()
    {
        // table_column_name => class_property_name
        return [
            'id' => 'id',
            'product_id' => 'productId',
            'advertiser_id' => 'advertiserId',
            'ad_id' => 'adId',
            'method' => 'method',
            'amount' => 'amount',
            'promo_code' => 'promoCode',
            'credits_granted' => 'creditsGranted',
            'created_at' => 'createdAt',
            'committed_at' => 'committedAt',
            'request' => 'request',
            'result' => 'result',
            'data' => 'data',
        ];
    }

    public function initialize()
    {
        $this->setSource('jz_payment');

        $this->useDynamicUpdate(true);

        $this->belongsTo('productId', Product::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);

        $this->belongsTo('advertiserId', Advertiser::class, 'id', [
            'foreignKey' => true,
            'reusable' => true,
        ]);
    }

}