<?php

namespace App\Model;

final class Ethnicity implements Enumeration
{

    static public function constants(): array
    {
        return [
            '',
            'African@',
            'Anglosajon@',
            'Árabe',
            'Asiátic@',
            'Europe@',
            'Latin@',
            'Mediterráne@',
            'Mulat@',
            'Negr@',
            'Nórdic@',
            'Rus@/Eslav@',
        ];
    }
}