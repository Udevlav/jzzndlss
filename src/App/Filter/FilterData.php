<?php

namespace App\Filter;

class FilterData
{

    public $type = Filters::TYPE_BOTH;
    public $limit = Filters::DEFAULT_LIMIT;
    public $offset = Filters::DEFAULT_OFFSET;
    public $q;
    public $lat;
    public $lng;
    public $category;
    public $postalCode;
    public $province;
    public $city;
    public $ageFrom;
    public $ageTo;
    public $feeFrom;
    public $feeTo;
    public $tags = [];
}
