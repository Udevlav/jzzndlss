<?php

namespace App\Filter;

use App\Form\FilterForm;
use App\Geo\GeoLocation;
use App\Model\AdTag;
use App\Model\Category;
use App\Model\Province;
use App\Model\Tag;
use App\Repository\EntityManager;
use App\Session\User;

use Phalcon\DispatcherInterface;
use Phalcon\Http\Request;
use Phalcon\Http\Response\CookiesInterface;
use Phalcon\Mvc\Model\Query\Builder;

class Filters
{

    const MAX_TAGS = 5;
    const DEFAULT_OFFSET = 0;
    const DEFAULT_LIMIT = 32;
    const TYPE_BOTH = '';
    const TYPE_REGULAR = 'r';
    const TYPE_PROMOTED = 'p';
    const ANY_PROVINCE = 'any';

    protected $form;
    protected $data;
    protected $tagsById;
    protected $tagsByGid;
    protected $tagsBySlug;
    protected $categories;
    protected $exclusive;

    public function __construct(EntityManager $em)
    {
        $this->loadCollections($em);

        $this->setDefaultData();
        $this->setExclusive();
    }

    public function __toString()
    {
        $buffer = [];

        $buffer[] = sprintf('type %s', $this->isMixed() ? 'mixed' : ($this->isPromotedOnly() ? 'promoted': 'regular'));
        $buffer[] = sprintf('offsets %d-%d', $this->data->offset, $this->data->limit);

        if (! empty($this->data->q)) {
            $buffer[] = sprintf('query "%s"', $this->data->q);
        }

        if (! empty($this->data->postalCode) ||
            ! empty($this->data->province) ||
            ! empty($this->data->city)) {
            if (! empty($this->data->postalCode)) {
                $buffer[] = sprintf('postalCode %s', $this->data->postalCode);
            } else if (! empty($this->data->province)) {
                $buffer[] = sprintf('province %d', $this->data->province);
            } else {
                $buffer[] = sprintf('city %s', $this->data->city);
            }
        } else {
            $buffer[] = 'any location';
        }

        if (! empty($this->data->category)) {
            $buffer[] = sprintf('category %s (%s)', is_numeric($this->data->category) ?
                Category::name($this->data->category) : $this->data->category,
                $this->data->category
            );
        } else {
            $buffer[] = 'any category';
        }

        if (! empty($this->data->tags)) {
            $buffer[] = sprintf('tags %s', implode($this->data->tags));
        } else {
            $buffer[] = 'any tag';
        }

        if (! empty($this->data->ageFrom) ||
            ! empty($this->data->ageTo)) {
            if (null === $this->data->ageFrom) {
                $buffer[] = sprintf('age to %d', $this->data->ageTo);
            } else if (null === $this->data->ageTo) {
                $buffer[] = sprintf('age from %f', $this->data->ageFrom);
            } else {
                $buffer[] = sprintf('age in %f-%f', $this->data->ageFrom, $this->data->ageTo);
            }
        }

        if (! empty($this->data->feeFrom) ||
            ! empty($this->data->feeTo)) {
            if (null === $this->data->feeFrom) {
                $buffer[] = sprintf('fee to %d', $this->data->feeTo);
            } else if (null === $this->data->feeTo) {
                $buffer[] = sprintf('fee from %f', $this->data->feeFrom);
            } else {
                $buffer[] = sprintf('fee in %f-%f', $this->data->feeFrom, $this->data->feeTo);
            }
        }

        return implode (', ', $buffer);
    }


    public function applyTo(Builder $qb)
    {
        // Category should always be AND'ed to the rest of criteria
        // (in the exclusive case, we join criteria using AND, in the
        // exclusive criteria, which applies to related ads, we join
        // using OR).
        //
        // Unfortunately, Phalcon does not provide a way to build
        // such complex queries programmatically. So when criteria
        // is inclusive, use a dedicated method -- the only found
        // use case is DeailedController::getRelated() method
        if (! $this->exclusive) {
            $this->applyToInclusive($qb);

            return;
        }

        if (! empty($token = $this->data->category)) {
            if (is_numeric($token)) {
                $token = Category::name($token);
            }

            $this->addToQuery($qb, 'ad.category = "'. $token .'"');
        } else {
            $this->data->category = 'escort';
            $this->addToQuery($qb, 'ad.category = "escort"');
        }


        if (! empty($token = $this->data->postalCode)) {
            $token = str_pad(strval($token), 2, '0', STR_PAD_LEFT);

            $this->addToQuery($qb, 'ad.postalCode LIKE "'. substr($token, 0, 2) .'%"');

        } else if (! empty($token = $this->data->province)) {
            // Skip on ANY keyword
            if (self::ANY_PROVINCE !== $token) {
                if (null !== ($token = Province::postalCodeBySlug($token))) {
                    $token = str_pad(strval($token), 2, '0', STR_PAD_LEFT);

                    $this->data->postalCode = substr($token, 0, 2);
                    $this->addToQuery($qb, 'ad.postalCode LIKE "'. $this->data->postalCode .'%"');
                }
            }

        } else if (! empty($token = $this->data->city)) {
            // Skip on ANY keyword
            if (self::ANY_PROVINCE !== $token) {
                if (null !== ($token = Province::postalCodeBySlug($token))) {
                    $token = str_pad(strval($token), 2, '0', STR_PAD_LEFT);

                    $this->data->postalCode = substr($token, 0, 2);
                    $this->addToQuery($qb, 'ad.postalCode LIKE "'. $this->data->postalCode .'%"');
                }
            }
        }

        $ageFrom = $this->data->ageFrom;
        $ageTo   = $this->data->ageTo;

        if (! empty($ageFrom) && ! empty($ageTo)) {
            $this->addToQuery($qb, sprintf('ad.age BETWEEN %d AND %d', min($ageFrom, $ageTo), max($ageFrom, $ageTo)));

        } else if (! empty($ageFrom)) {
            $this->addToQuery($qb, 'ad.age > '. $ageFrom);

        } else if (! empty($ageTo)) {
            $this->addToQuery($qb, 'ad.age < '. $ageTo);
        }

        $feeFrom = $this->data->feeFrom;
        $feeTo   = $this->data->feeTo;

        if (! empty($feeFrom) && ! empty($feeTo)) {
            $this->addToQuery($qb, sprintf('ad.fee BETWEEN %d AND %d', min($feeFrom, $feeTo), max($feeFrom, $feeTo)));

        } else if (! empty($feeFrom)) {
            $this->addToQuery($qb, 'ad.fee > '.$feeFrom);

        } else if (! empty($feeTo)) {
            $this->addToQuery($qb, 'ad.fee < '.$feeTo);
        }

        $tags = $this->data->tags ?? [];

        if (is_array($tags) && count($tags)) {
            $qb
                ->join(AdTag::class, null, 'tag')
                ->andWhere('tag.tagId IN ('.implode(',', array_slice($tags, 0, self::MAX_TAGS)).')');
        }

        if (! empty($this->data->q)) {
            $qb
                ->distinct('ad.id')
                ->andWhere('((LOWER(ad.slug) LIKE "%'. $this->data->q .'%") OR (LOWER(ad.category) LIKE "%'. $this->data->q .'%") OR (LOWER(ad.name) LIKE "%'. $this->data->q .'%") OR (LOWER(ad.phone) LIKE "%'. $this->data->q .'%"))');
        }

        $qb->limit($this->data->limit, $this->data->offset);
    }

    protected function applyToInclusive(Builder $qb)
    {
        // See note on applyTo() method
        if (! empty($token = $this->data->category)) {
            if (is_numeric($token)) {
                $token = Category::name($token);
            }

            $qb->andWhere('ad.category = "'. $token .'"');
        } else {
            $this->data->category = 'escort';
            $this->addToQuery($qb, 'ad.category = "escort"');
        }

        // The rest of criteria is AND'ed
        $criteria = [];

        if (! empty($token = $this->data->postalCode)) {
            $token = str_pad(strval($token), 2, '0', STR_PAD_LEFT);

            $criteria[] = sprintf('(ad.postalCode LIKE "%s%%")', substr($token, 0, 2));

        } else if (! empty($token = $this->data->province)) {
            // Skip on ANY keyword
            if (self::ANY_PROVINCE !== $token) {
                if (null !== ($token = Province::postalCodeBySlug($token))) {
                    $token = str_pad(strval($token), 2, '0', STR_PAD_LEFT);

                    $criteria[] = sprintf('(ad.postalCode LIKE "%s%%")', substr($token, 0, 2));
                }
            }

        } else if (! empty($token = $this->data->city)) {
            // Skip on ANY keyword
            if (self::ANY_PROVINCE !== $token) {
                if (null !== ($token = Province::postalCodeBySlug($token))) {
                    $token = str_pad(strval($token), 2, '0', STR_PAD_LEFT);

                    $criteria[] = sprintf('(ad.postalCode LIKE "%s%%")', substr($token, 0, 2));
                }
            }
        }

        $ageFrom = $this->data->ageFrom;
        $ageTo   = $this->data->ageTo;

        if (! empty($ageFrom) && ! empty($ageTo)) {
            $criteria[] = sprintf('(ad.age BETWEEN %d AND %d)', min($ageFrom, $ageTo), max($ageFrom, $ageTo));

        } else if (! empty($ageFrom)) {
            $criteria[] = sprintf('(ad.age > %d)', min($ageFrom, $ageTo));

        } else if (! empty($ageTo)) {
            $criteria[] = sprintf('(ad.age < %d)', max($ageFrom, $ageTo));
        }

        $feeFrom = $this->data->feeFrom;
        $feeTo   = $this->data->feeTo;

        if (! empty($feeFrom) && ! empty($feeTo)) {
            $criteria[] = sprintf('(ad.fee BETWEEN %d AND %d)', min($feeFrom, $feeTo), max($feeFrom, $feeTo));

        } else if (! empty($feeFrom)) {
            $criteria[] = sprintf('(ad.fee > %d)', min($feeFrom, $feeTo));

        } else if (! empty($feeTo)) {
            $criteria[] = sprintf('(ad.fee < %d)', max($feeFrom, $feeTo));
        }

        $tags = $this->data->tags ?? [];

        if (count($tags)) {
            $qb->join(AdTag::class, null, 'tag');

            $criteria[] = sprintf('(tag.tagId IN (%s))', implode(',', array_slice($tags, 0, self::MAX_TAGS)));
        }

        $qb->andWhere(implode(' OR ', $criteria));

        if (! empty($this->data->q)) {
            $q = strtolower($this->data->q);
            $qb
                ->distinct('ad.id')
                ->andWhere('('. implode(' OR ', [
                    '(LOWER(ad.slug) LIKE "%'. $q .'%")',
                    '(LOWER(ad.category) LIKE "%'. $q .'%")',
                    '(LOWER(ad.name) LIKE "%'. $q .'%")',
                ]).')')
            ;
        }

        $qb->limit($this->data->limit, $this->data->offset);
    }

    public function getLimit(): int
    {
        return $this->data->limit;
    }

    public function isMixed(): bool
    {
        return self::TYPE_BOTH === $this->data->type;
    }

    public function isPromotedOnly(): bool
    {
        return self::TYPE_PROMOTED === $this->data->type;
    }

    public function isRegularOnly(): bool
    {
        return self::TYPE_REGULAR === $this->data->type;
    }

    public function isNullCriteria(): bool
    {
        return null === $this->data->q &&
            null === $this->data->lat &&
            null === $this->data->lng &&
            null === $this->data->category &&
            null === $this->data->postalCode &&
            null === $this->data->province &&
            null === $this->data->city &&
            null === $this->data->ageFrom &&
            null === $this->data->ageTo &&
            null === $this->data->feeFrom &&
            null === $this->data->feeTo;
    }


    public function generateRedirectUrl(): string
    {
        $segments = [];

        if (null !== $this->data->category) {
            $segments[] = $this->data->category;
        }

        if (! empty($this->data->province) && strlen($this->data->province) > 2) {
            $segments[] = 'provincia-'. $this->data->province;
        } else if (! empty ($this->data->city)) {
            $segments[] = $this->data->city;
        }

        if ($this->data->tags) {
            foreach ($this->data->tags as $tag) {
                $tag = $this->tagsById[$tag] ?? null;

                if (null !== $tag) {
                    $segments[] = $tag->slug;
                }
            }
        }

        $query = [];

        if ($this->data->q) {
            $query['q'] = $this->data->q;
        }

        if ($this->data->ageFrom) {
            $query['ageFrom'] = $this->data->ageFrom;
        }

        if ($this->data->ageTo) {
            $query['ageTo'] = $this->data->ageTo;
        }

        if ($this->data->feeFrom) {
            $query['feeFrom'] = $this->data->feeFrom;
        }

        if ($this->data->feeTo) {
            $query['feeTo'] = $this->data->feeTo;
        }

        if ($this->data->postalCode === '00') {
            // Handle any location
            $query['al'] = '';
        }

        $path = '/' . implode('/', $segments);

        if ($query) {
            $path .= '?'. http_build_query($query);
        }

        return $path;
    }


    public function generateKey(string $base): string
    {
        $key = [$base];

        if (null !== ($token = $this->data->type)) {
            $key[] = $token;
        }

        if (null !== ($token = $this->data->limit)) {
            $key[] = $token;
        }

        if (null !== ($token = $this->data->offset)) {
            $key[] = $token;
        }

        if (null !== ($token = $this->data->q)) {
            $key[] = $token;
        }

        if (null !== ($token = $this->data->category)) {
            $key[] = $token;
        }

        if (null !== ($token = $this->data->postalCode)) {
            $key[] = $token;
        }

        if (is_array($this->data->tags)) {
            $key = array_merge($key, $this->data->tags);
        }

        return implode('_', $key);
    }


    public function getForm(): FilterForm
    {
        return $this->form;
    }

    public function getData(): FilterData
    {
        return $this->data;
    }

    public function getProvinces(): array
    {
        $arr = Province::$postalCodeToName;
        ksort($arr);

        return $arr;
    }

    public function getTags($gid): array
    {
        return $this->tagsByGid[$gid] ?? [];
    }

    public function hasTag($id): bool
    {
        return is_array($this->data->tags) && in_array($id, $this->data->tags);
    }


    public function setExclusive()
    {
        $this->exclusive = true;
    }

    public function setInclusive()
    {
        $this->exclusive = false;
    }

    public function set(string $key, $value)
    {
        $this->data->$key = $value;
    }

    public function setData(FilterData $data)
    {
        $this->data = $data;
    }

    public function setDefaultData()
    {
        $this->data = new FilterData();
    }

    public function setUserData(User $user)
    {

    }

    public function setCookieData(CookiesInterface $cookies)
    {
        $location = $cookies->get('location')->getValue();

        if ($location && 2 === strlen($location) && is_numeric($location)) {
            $this->data->postalCode = $location;
        }
    }

    public function setGeoData(GeoLocation $geoLocation)
    {
        $postalCode = $geoLocation->getPostalCode();

        if ($postalCode && strlen($postalCode) > 1) {
            $this->data->postalCode = substr($postalCode, 0, 2);
        }
    }

    public function setRequestData(Request $request = null)
    {
        if (null === $request) {
            return;
        }

        $this->form = new FilterForm();

        if (! $this->form->isValid($request->getPost(), $this->data)) {
            // Reset to defaults if the form is not valid
            $this->setDefaultData();
        }

        if (! empty(($token = $request->getQuery('type'))) ||
            ! empty(($token = $request->getPost('type')))) {
            $this->data->type = (string) $token;
        }

        if (! empty(($token = $request->getQuery('limit'))) ||
            ! empty(($token = $request->getPost('limit')))) {
            $this->data->limit = (int) $token;
        }

        if (! empty(($token = $request->getQuery('offset'))) ||
            ! empty(($token = $request->getPost('offset')))) {
            $this->data->offset = (int) $token;

            if ($this->data->offset > 0) {
                $this->data->offset = $this->data->offset * $this->data->limit;
            }
        }

        if (! empty(($token = $request->getQuery('q'))) ||
            ! empty(($token = $request->getPost('q')))) {
            $this->data->q = (string) $token;
        }

        if (! empty(($token = $request->getQuery('province'))) ||
            ! empty(($token = $request->getPost('province')))) {
            $this->data->province = (string) $token;
        }

        if (! empty(($token = $request->getQuery('city'))) ||
            ! empty(($token = $request->getPost('city')))) {
            $this->data->city = (string) $token;
        }

        if (! empty(($token = $request->getQuery('postalCode'))) ||
            ! empty(($token = $request->getPost('postalCode')))) {
            if ('any' === $token) {
                // Clear if any is received
                $this->data->city = null;
                $this->data->province = null;
                $this->data->postalCode = $request->isPost() ? '00' : null;
            } else {
                $this->data->postalCode = (string) $token;
            }
        }

        if ($request->isGet() && null !== ($token = $request->getQuery('al'))) {
            // This is a search request, redirected from a post loooking at any place
            // (al stands for "any location")
            $this->data->city = null;
            $this->data->province = null;
            $this->data->postalCode = null;
        }

        if (! empty(($token = $request->getQuery('tags'))) ||
            ! empty(($token = $request->getPost('tags')))) {
            $this->data->tags = array_filter(explode(',', $token), function($tag) {
                return ! empty($tag);
            });
        }

        if (! empty(($token = $request->getQuery('ageFrom'))) ||
            ! empty(($token = $request->getPost('ageFrom')))) {
            $this->data->ageFrom = (int) $token;
        }

        if (! empty(($token = $request->getQuery('ageTo'))) ||
            ! empty(($token = $request->getPost('ageTo')))) {
            $this->data->ageTo = (int) $token;
        }

        if (! empty(($token = $request->getQuery('feeFrom'))) ||
            ! empty(($token = $request->getPost('feeFrom')))) {
            $this->data->feeFrom = (int) $token;
        }

        if (! empty(($token = $request->getQuery('feeTo'))) ||
            ! empty(($token = $request->getPost('feeTo')))) {
            $this->data->feeTo = (int) $token;
        }
    }

    public function setDispatcherData(DispatcherInterface $dispatcher = null)
    {
        if (null === $dispatcher) {
            return;
        }

        if ($dispatcher->hasParam('category')) {
            $this->data->category = $dispatcher->getParam('category');
        }

        if ($dispatcher->hasParam('place')) {
            $place = $dispatcher->getParam('place');

            if (0 === strpos($place, 'provincia-')) {
                $provinceSlug = str_replace('provincia-', '', $place);

                $this->data->province = $provinceSlug;
                $this->data->postalCode = Province::postalCodeBySlug($provinceSlug);
            } else {
                $this->data->city = $place;
            }
        }

        if (! isset($this->data->tags)) {
            $this->data->tags = [];
        }

        if ($dispatcher->hasParam('tag1')) {
            $this->addTag($dispatcher->getParam('tag1'));
        }

        if ($dispatcher->hasParam('tag2')) {
            $this->addTag($dispatcher->getParam('tag2'));
        }

        if ($dispatcher->hasParam('tag3')) {
            $this->addTag($dispatcher->getParam('tag3'));
        }

        if ($dispatcher->hasParam('tag4')) {
            $this->addTag($dispatcher->getParam('tag4'));
        }

        if ($dispatcher->hasParam('tag5')) {
            $this->addTag($dispatcher->getParam('tag5'));
        }

        if ($dispatcher->hasParam('ageFrom')) {
            $this->data->ageFrom = (int) $dispatcher->getParam('ageFrom');
        }

        if ($dispatcher->hasParam('ageTo')) {
            $this->data->ageTo = (int) $dispatcher->getParam('ageTo');
        }

        if ($dispatcher->hasParam('feeFrom')) {
            $this->data->feeFrom = (int) $dispatcher->getParam('feeFrom');
        }

        if ($dispatcher->hasParam('feeTo')) {
            $this->data->feeTo = (int) $dispatcher->getParam('feeTo');
        }
    }


    protected function loadCollections(EntityManager $em)
    {
        /** @var \App\Repository\TagRepository $repository */
        $repository = $em->getRepository(Tag::class);

        $this->tagsById = [];
        $this->tagsBySlug = [];
        $this->tagsByGid = $repository->groupByGid();
        $this->categories = $this->tagsByGid[0] ?? [];

        unset($this->tagsByGid[0]);

        foreach ($this->tagsByGid as $gid => $tags) {
            foreach ($tags as $tag) {
                /** @var \App\Model\Tag $tag */
                $this->tagsById[$tag->id] = $tag;
                $this->tagsBySlug[$tag->slug] = $tag;
            }
        }
    }

    protected function addTag(string $slug)
    {
        $tag = $this->tagsBySlug[$slug] ?? null;

        if (null !== $tag) {
            $this->data->tags[] = $tag->id;
        }
    }

    protected function addToQuery(Builder $qb, string $criteria)
    {
        if ($this->exclusive) {
            $qb->andWhere($criteria);
        } else {
            $qb->orWhere($criteria);
        }
    }
}
