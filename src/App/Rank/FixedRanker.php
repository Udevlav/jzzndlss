<?php

namespace App\Rank;

use App\Model\Ad;

/**
 * Fixed ranking strategy.
 */
class FixedRanker implements Ranker
{

    /**
     * @var int
     */
    private $rank;

    /**
     * FixedRanker constructor.
     *
     * @param int $rank
     */
    public function __construct(int $rank)
    {
        $this->rank = $rank;
    }

    /**
     * @inheritdoc
     */
    public function rank(Ad $ad): int
    {
        return $this->rank;
    }
}