<?php

namespace App\Rank;

use RuntimeException;

class RankException extends RuntimeException
{
}