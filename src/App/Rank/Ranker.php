<?php

namespace App\Rank;

use App\Model\Ad;

/**
 * Abstraction of an ad ranking strategy.
 */
interface Ranker
{

    /**
     * Computes the rank for a given ad using this ranking strategy.
     *
     * @param Ad $ad
     *
     * @return int the ad rank (score)
     */
    public function rank(Ad $ad): int;
}