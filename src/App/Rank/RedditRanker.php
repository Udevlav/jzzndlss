<?php

namespace App\Rank;

use App\Model\Ad;

use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\User\Component;

use DateTime;
use Throwable;

/**
 * Reddit ranking strategy:
 *
 * 1. Given the time the entry was posted A and the reference time A, compute
 *    their difference in seconds:
 *
 *       ts = A - B
 *
 * 2. Let x be the difference between the number of up votes A and the number
 *    of down votes D
 *
 *       x = U - D
 *
 * 3. Let y be
 *
 *       y =  1, if x > 0
 *       y =  0, if x = 0
 *       y = -1, if x < 0
 *
 * 4. Let z be the maximal value of the absolute value of x and 1,
 *
 *       z = max(1, abs(x))
 *
 * 5. Then the entry rank is defined as
 *
 *       f(ts, y, z) = log10(z) + (y * ts) / 45000
 *
 * Notes on the algorithm:
 *
 *  - Publication time has a great impact on the ranking score, and the
 *    algorithm will rank newer stories higher than older
 *
 *  - The score won't decrease as time goes by
 *
 *  - The use of a logarithm weights the first votes higher than later ones
 *    For example, the first 10 upvotes have the same weight as the next
 *    100 upvotes, which, in turn, have the same weight as the next 1000
 *    upvotes, and so on ...
 *
 *  - Controversial stories that get approximately the same number of
 *    upvotes and downvotes will get a low ranking compared to stories that
 *    mainly get upvotes
 *
 * @see https://medium.com/hacking-and-gonzo/how-reddit-ranking-algorithms-work-ef111e33d0d9
 */
class RedditRanker extends Component implements Ranker
{

    /**
     * @var string
     */
    const DATEFORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    const REFDATE = '2018-01-01 00:00:00';

    /**
     * @var int
     */
    private $b;

    /**
     * @var int
     */
    protected $logger;

    /**
     * RedditRanker constructor.
     *
     * @param string $refDate the reference date
     *
     * @throws RankException
     */
    public function __construct(string $refDate = self::REFDATE)
    {
        try {
            $this->b = DateTime::createFromFormat(self::DATEFORMAT, $refDate)->getTimestamp();
        } catch (Throwable $t) {
            throw new RankException(sprintf('Invalid reference datetime %s (expected format %s).', $refDate, self::DATEFORMAT));
        }
    }

    /**
     * @inheritdoc
     */
    public function rank(Ad $ad): int
    {
        $ts = $this->computeTs($ad);
        $x = $this->computeX($ad);
        $y = $this->computeY($x);
        $z = $this->computeZ($x);

        $rank = (int) (log($z, 10) + $y * $ts / 4500);

        $this->getLogger()->debug(sprintf('Ranked ad #%d with %s score.', $ad->id, $ad->score));

        return $rank;
    }

    /**
     * Returns the difference in seconds between current time and the ad
     * publication time, in seconds.
     *
     * @param Ad $ad
     *
     * @return int
     */
    private function computeTs(Ad $ad): int
    {
        if (! empty($ad->publishedAt)) {
            $a = DateTime::createFromFormat(self::DATEFORMAT, $ad->publishedAt);
        } else {
            $a = new DateTime();
        }

        return abs($a->getTimestamp() - $this->b);
    }

    /**
     * Returns the difference between up votes and down votes.
     *
     * @param Ad $ad
     *
     * @return int
     */
    private function computeX(Ad $ad): int
    {
        return $ad->numHits /* + $ad->numExperiences * $ad->rating */;
    }

    /**
     * @param int $x
     *
     * @return int
     */
    private function computeY(int $x): int
    {
        return $x <=> 0;
    }

    /**
     * @param int $x
     *
     * @return int
     */
    private function computeZ(int $x): int
    {
        return max(1, abs($x));
    }

    /**
     * @return FileLogger
     */
    private function getLogger(): FileLogger
    {
        return $this->di->get('logger');
    }
}