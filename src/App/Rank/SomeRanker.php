<?php

namespace App\Rank;

use App\Model\Ad;

/**
 * Some ranking strategy.
 */
class SomeRanker implements Ranker
{

    /**
     * @inheritdoc
     */
    public function rank(Ad $ad): int
    {
        return 100;
    }
}