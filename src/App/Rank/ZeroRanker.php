<?php

namespace App\Rank;

use App\Model\Ad;

/**
 * A zero (no-op) ranking strategy.
 */
class ZeroRanker implements Ranker
{

    /**
     * @inheritdoc
     */
    public function rank(Ad $ad): int
    {
        return 0;
    }
}