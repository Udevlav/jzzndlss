<?php

namespace App\Finder;

use App\Filter\Filters;

use Phalcon\Mvc\Model\Resultset;

class YetAnotherFinder extends Finder
{

    /**
     * Finds ads matching the given filters.
     *
     * @param Filters $filters
     *
     * @return Results
     */
    public function find(Filters $filters): Results
    {
        $this->getLogger()->debug(sprintf('Finding by %s ...', $filters));

        // No promoted results, all are regular
        $results = new Results();
        $results->promoted = null;
        $results->regular = $this->findAny($filters);

        $this->getLogger()->debug(sprintf('Found %d results',
            $results->count()
        ));

        return $results;
    }


    protected function findAny(Filters $filters): Resultset
    {
        $qb = $this->getAdRepository($filters->generateKey('ad_promoted'))
            ->getSortedByPublicationDateQuery();

        $filters->applyTo($qb);

        return $qb
            ->getQuery()
            ->execute();
    }

}
