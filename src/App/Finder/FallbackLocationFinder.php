<?php

namespace App\Finder;

use App\Filter\Filters;

/**
 * A finder which tries a less restrictive location filter if the previous
 * filter criteria yielded no results.
 */
class FallbackLocationFinder extends Finder
//class FallbackLocationFinder extends YetAnotherFinder
{

    /**
     * @inheritdoc
     */
    public function find(Filters $filters): Results
    {
        $results = parent::find($filters);

        if (0 === $results->count() && $this->isWorthRelaxing($filters)) {
            if (null !== ($relaxedFilters = $this->relax($filters))) {
                $this->getLogger()->debug('No results with current filtering criteria, relax location criteria ...');

                return $this->find($relaxedFilters);
            }
        }

        return $results;
    }


    protected function isWorthRelaxing(Filters $filters)
    {
        $data = $filters->getData();

        return (
            $data->ageFrom > 0 ||
            $data->ageTo > 0 ||
            $data->feeFrom > 0 ||
            $data->feeTo > 0 ||
            count($data->tags) > 0
        );
    }

    protected function relax(Filters $filters)
    {
        $clone = clone $filters;
        $data = $clone->getData();

        if (null === $data->postalCode && null === $data->province) {
            // We weren't filtering by location; no results with the
            // current criteria and cannot relax location criteria
            // any further -> no further queries
            return null;
        }

        if (null !== $data->postalCode) {
            // No results for current postal code, fallback is to use
            // the province code
            $data->province = substr($data->postalCode, 0, 2);
            $data->postalCode = null;

        } else {
            // No results for current province, fallback is to return
            // all results
            $data->province = null;
            $data->postalCode = null;
        }

        return $clone;
    }
}
