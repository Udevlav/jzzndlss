<?php

namespace App\Finder;

use Phalcon\Mvc\Model\Resultset;

class Results
{

    /**
     * @var Resultset
     */
    public $promoted;

    /**
     * @var Resultset
     */
    public $regular;


    public function get(int $n): array
    {
        $results = [];

        if ($this->countPromoted()) {
            foreach ($this->promoted as $record) {
                if (count($results) >= $n) {
                    break;
                }

                if ($record->publishedAt) {
                    $results[] = $record;
                }
            }
        }

        if ($this->countRegular()) {
            foreach ($this->regular as $record) {
                if (count($results) >= $n) {
                    break;
                }

                if ($record->publishedAt) {
                    $results[] = $record;
                }
            }
        }

        return $results;
    }


    public function isEmpty(): bool
    {
        return 0 === $this->count();
    }

    public function hasPromoted(): bool
    {
        return (bool) $this->countPromoted();
    }

    public function hasRegular(): bool
    {
        return (bool) $this->countRegular();
    }


    public function countPromoted(): int
    {
        if (null === $this->promoted) {
            return 0;
        }

        return $this->promoted->count();
    }

    public function countRegular(): int
    {
        if (null === $this->regular) {
            return 0;
        }

        return $this->regular->count();
    }

    public function count(): int
    {
        return $this->countPromoted() + $this->countRegular();
    }

}
