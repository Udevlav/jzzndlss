<?php

namespace App\Finder;

use App\Config\Config;
use App\Filter\Filters;
use App\Model\Ad;
use App\Repository\AdRepository;
use App\Repository\EntityManager;

use Phalcon\Logger\Adapter;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\User\Component;

class Finder extends Component
{

    /**
     * Finds ads matching the given filters.
     *
     * @param Filters $filters
     *
     * @return Results
     */
    public function find(Filters $filters): Results
    {
        $this->getLogger()->debug(sprintf('Finding by %s ...', $filters));

        $results = new Results();
        $results->promoted = $this->findPromoted($filters);

        if ($results->count() < $filters->getLimit()) {
            $results->regular = $this->findRegular($filters, $results->promoted);
        }

        $this->getLogger()->debug(sprintf('Found %d results (%d promoted, %d regular)',
            $results->count(),
            $results->countPromoted(),
            $results->countRegular()
        ));

        return $results;
    }


    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    protected function getLogger(): Adapter
    {
        return $this->di->get('logger');
    }

    protected function getEntityManager(): EntityManager
    {
        return $this->di->get('em');
    }

    protected function getAdRepository(string $cacheKey): AdRepository
    {
        $config = $this->getConfig();

        /** @var \App\Repository\AdRepository $repository */
        $repository = $this->getEntityManager()->getRepository(Ad::class);
        $repository
            ->setCacheKey($cacheKey)
            ->setCacheLifetime($config->publication->indexLifetime);

        return $repository;
    }

    protected function findPromoted(Filters $filters): Resultset
    {
        $qb = $this->getAdRepository($filters->generateKey('ad_promoted'))
            ->getPromotedQuery();

        $filters->applyTo($qb);

        return $qb
            ->getQuery()
            ->execute();
    }

    protected function findRegular(Filters $filters, Resultset $promoted): Resultset
    {
        $qb = $this->getAdRepository($filters->generateKey('ad_regular'))
            ->getRegularQuery($promoted);

        $filters->applyTo($qb);

        return $qb
            ->getQuery()
            ->execute();
    }
}
