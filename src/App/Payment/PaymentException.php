<?php

namespace App\Payment;

use RuntimeException;

class PaymentException extends RuntimeException
{
}