<?php

namespace App\Payment;

use App\Model\Payment;
use App\Model\PaymentMethod;
use App\Payment\PayPal\PayPalGateway;
use App\Payment\Redsys\RedsysGateway;

use Phalcon\Mvc\User\Component;

class Broker extends Component
{

    /**
     * @param Payment $payment
     *
     * @return PaymentGateway
     */
    public function createGateway(Payment $payment): PaymentGateway
    {
        switch ($payment->method) {
            case PaymentMethod::PAYPAL:
                return new PayPalGateway();

            case PaymentMethod::CARD:
                return new RedsysGateway();
        }

        throw new PaymentException(sprintf('Invalid payment method %s.', $payment->method));
    }
}