<?php

namespace App\Payment;

use App\Config\Config;

abstract class PaymentResult
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var bool
     */
    protected $success;

    /**
     * @var mixed
     */
    protected $payload;

    /**
     * @var int
     */
    protected $errorCode;

    /**
     * @var int
     */
    protected $errorMessage;

    /**
     * PaymentResult constructor.
     *
     * @param Config $config
     * @param array $data
     */
    public function __construct(Config $config, array $data)
    {
        $this->config = $config;
        $this->data = $data;
        $this->success = false;

        $this->verify();
    }

    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return int | null
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string | null
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Verifies the payment data.
     *
     * @return void
     */
    abstract protected function verify();

    /**
     * Sets the current error
     *
     * @param int    $errorCode
     * @param string $errorMessage
     *
     * @return void
     */
    protected function setError(int $errorCode, string $errorMessage)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->success = false;
    }
}