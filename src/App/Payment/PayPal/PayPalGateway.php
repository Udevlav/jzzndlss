<?php

namespace App\Payment\PayPal;

use App\Config\Config;
use App\Model\Payment;
use App\Payment\PaymentException;
use App\Payment\PaymentGateway;

use PayPal\Api\Amount;
use PayPal\Api\Payment as PaypalPayment;
use PayPal\Api\Payer;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConfigurationException;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

use Phalcon\Mvc\User\Component;

class PayPalGateway extends Component implements PaymentGateway
{

    /**
     * @var ApiContext
     */
    protected $apiContext;

    public function commit(Payment $payment)
    {
        $apiContext = new ApiContext($this->getCredentials());

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setTotal($payment->amount);

        $transaction = new Transaction();
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setCancelUrl($this->getCancelUrl());
        $redirectUrls->setReturnUrl($this->getReturnUrl());

        $payment = new PaypalPayment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setTransactions([$transaction]);
        $payment->setRedirectUrls($redirectUrls);

        try {
            $payment->create($apiContext);

        } catch (PayPalConfigurationException $e) {
            throw new PaymentException($e->getMessage(), $e->getCode(), $e);

        } catch (PayPalConnectionException $f) {
            throw new PaymentException($f->getMessage(), $f->getCode(), $f);
        }
    }


    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    protected function getCredentials(): OAuthTokenCredential
    {
        $config = $this->getConfig();

        return new OAuthTokenCredential(
            $config->paypal->clientId,
            $config->paypal->clientSecret
        );
    }

    protected function getCancelUrl(): OAuthTokenCredential
    {
        return sprintf('%spago-cancelado', $this->getConfig()->baseUrl());
    }

    protected function getReturnUrl(): OAuthTokenCredential
    {
        return sprintf('%spago-confirmado', $this->getConfig()->baseUrl());
    }
}