<?php

namespace App\Payment;

use App\Model\Payment;

use App\Model\Product;
use Phalcon\Forms\Form;

interface PaymentGateway
{
    public function createPaymentForm(Payment $payment, Product $product): Form;
    public function verify(Payment $payment, array $responseData): bool;
}