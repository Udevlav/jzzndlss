<?php

namespace App\Payment\Redsys;

use App\Payment\PaymentResult;

use Throwable;

class RedsysPaymentResult extends PaymentResult
{

    /**
     * @var string[]
     */
    static private $signatureFields = [
        'Ds_Amount',
        'Ds_Order',
        'Ds_MerchantCode',
        'Ds_Currency',
        'Ds_Response',
    ];

    /**
     * @inheritdoc
     */
    protected function verify()
    {
        if (! $this->verifyResponseData()) {
            return;
        }

        $params = $this->decodeTransactionParams();

        if (empty($params)) {
            return;
        }

        if (! $this->verifyErrorCode($params)) {
            return;
        }

        if (! $this->verifyResponseCode($params)) {
            return;
        }

        if (! $this->verifySignature($params)) {
            return;
        }

        $this->success = true;
        $this->data = array_merge($this->data, array_map('urldecode', $params));
    }


    private function decodeTransactionParams(): array
    {
        try {
            return json_decode(base64_decode(strtr($this->data['Ds_MerchantParameters'], '-_', '+/')), true);
        } catch (Throwable $t) {
            $this->setError(0, sprintf('Payment data not decodable: %s.', $t->getMessage()));

            return null;
        }
    }


    private function verifyResponseData(): bool
    {
        if (empty($this->data) ||
            ! isset($this->data['Ds_MerchantParameters']) ||
            ! isset($this->data['Ds_Signature']) ||
            ! isset($this->data['Ds_SignatureVersion'])) {

            $this->setError(0, 'Empty payment data.');

            return false;
        }

        return true;
    }

    private function verifyErrorCode(array $params): bool
    {
        if (! isset($params['Ds_ErrorCode'])) {
            return true;
        }

        $errorCode = (int) $params['Ds_ErrorCode'];
        $this->setError($errorCode, RedsysError::message($errorCode));

        return false;
    }

    private function verifyResponseCode(array $params): bool
    {
        $responseCode = $params['Ds_Response'] ?? null;

        if (empty($responseCode)) {
            $this->setError(0, 'Empty payment response code.');

            return false;
        }

        $responseCode = (int) $responseCode;

        if (0 > $responseCode || ($responseCode > 99 && $responseCode !== 900)) {
            $this->setError($responseCode, RedsysError::message($responseCode));

            return false;
        }

        return true;
    }

    private function verifyResponseField(string $name, array $params): bool
    {
        if (! isset($params['Ds_'. $name])) {
            $this->setError(0, sprintf('Missing required response parameter %s.', $name));

            return false;
        }

        return true;
    }

    private function verifySignature(array $params): bool
    {
        foreach (static::$signatureFields as $field) {
            if (! $this->verifyResponseField($field, $params)) {
                return false;
            }
        }

        $actualSignature = strtr($params['Ds_Signature'], '-_', '+/');

        $expectedSignature = RedsysUtil::crypt($this->config->redsys->key, $params['Ds_Order']);
        $expectedSignature = RedsysUtil::mac($expectedSignature, base64_encode(json_encode($params)));

        if ($expectedSignature !== $actualSignature) {
            $this->setError(0, sprintf('Invalid signature: got %s, expected %s.', $actualSignature, $expectedSignature));

            return false;
        }

        return true;
    }
}