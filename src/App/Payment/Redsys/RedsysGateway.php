<?php

namespace App\Payment\Redsys;

use App\Config\Config;
use App\Form\RedsysPaymentForm;
use App\Model\Payment;
use App\Model\Product;
use App\Payment\PaymentGateway;
use App\Util\RouteUtil;

use Phalcon\Forms\Form;
use Phalcon\Logger;
use Phalcon\Logger\AdapterInterface;
use Phalcon\Mvc\Router\Annotations;
use Phalcon\Mvc\User\Component;

use BadFunctionCallException;
use RuntimeException;

class RedsysGateway extends Component implements PaymentGateway
{

    /**
     * @var string
     */
    const URL_TEST = 'https://sis-t.redsys.es:25443/sis';

    /**
     * @var string
     */
    //const URL_PROD = 'https://sis.redsys.es/sis';
    // @sgonzalez PROD is currently staging
    const URL_PROD = 'https://sis-t.redsys.es:25443/sis';


    /**
     * @inheritdoc
     */
    public function createPaymentForm(Payment $payment, Product $product): Form
    {
        $paymentData = $this->createPaymentData($payment, $product);

        $form = new RedsysPaymentForm($paymentData->createPayment($this->getMerchantKey()));
        $form->setAction($this->getPaymentUrl());

        $this->getLogger()->debug(sprintf('Payment data being sent: %s', json_encode($paymentData->toArray(), JSON_PRETTY_PRINT)));

        return $form;
    }

    /**
     * @inheritdoc
     */
    public function verify(Payment $payment, array $responseData): bool
    {
        $redsysPayment = RedsysPayment::fromArray($responseData);
        $paymentData = RedsysData::fromPayment($redsysPayment);

        $actualSignature = $redsysPayment->signature;
        $expectedSignature = $paymentData->getVerificationSignature($this->getMerchantKey(), $redsysPayment->merchantParameters);

        $this->getLogger()->log(sprintf('Redsys ayment verification (actual/expected): %s', $actualSignature, $expectedSignature), Logger::DEBUG);

        $payment->data = $paymentData->toJson();
        $payment->committedAt = date('Y-m-d H:i:s');
        $payment->result = ($actualSignature === $expectedSignature);

        return $payment->result;
    }


    /**
     * @param Payment $payment
     * @param Product $product
     *
     * @return RedsysData
     */
    protected function createPaymentData(Payment $payment, Product $product): RedsysData
    {
        $config = $this->getConfig();

        $paymentData = new RedsysData();
        $paymentData
            ->set('DS_MERCHANT_AMOUNT', RedsysUtil::amount($payment->amount))
            ->set('DS_MERCHANT_ORDER', RedsysUtil::orderNo($payment->id))
            ->set('DS_MERCHANT_MERCHANTCODE', $config->redsys->merchantCode)
            ->set('DS_MERCHANT_CURRENCY', $config->redsys->currency ?? RedsysData::DEFAULT_CURRENCY)
            ->set('DS_MERCHANT_TRANSACTIONTYPE', '0')
            ->set('DS_MERCHANT_TERMINAL', $config->redsys->terminal)
            ->set('DS_MERCHANT_MERCHANTURL', $this->getRouteUrl('@verify_payment', $payment->id))
            ->set('DS_MERCHANT_URLOK', $this->getRouteUrl('@payment_succeed', $payment->id))
            ->set('DS_MERCHANT_URLKO', $this->getRouteUrl('@payment_failed', $payment->id))
            //->set('DS_MERCHANT_MERCHANTDATA', $product->label)
        ;

        return $paymentData;
    }

    /**
     * @return string
     */
    protected function getPaymentUrl(): string
    {
        $config = $this->getConfig();

        return ($config->app->debug ? self::URL_TEST : self::URL_PROD) .'/realizarPago';
    }

    /**
     * @return string
     */
    protected function getMerchantKey(): string
    {
        $config = $this->getConfig();

        return $config->redsys->key;
    }

    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @param string $name
     * @param array ...$parameters
     *
     * @return string
     */
    protected function getRouteUrl(string $name, ... $parameters): string
    {
        if ('@' !== $name[0]) {
            throw new BadFunctionCallException('Bad route link call: route name should start with "@".');
        }

        if (false === ($route = $this->getRouter()->getRouteByName($name))) {
            throw new RuntimeException(sprintf('Route "%s" does not exist.', $name));
        }

        return rtrim($this->getConfig()->baseUrl(), '/') . RouteUtil::generate($route, ... $parameters);
    }

    /**
     * @return AdapterInterface
     */
    protected function getLogger(): AdapterInterface
    {
        return $this->di->get('logger');
    }

    /**
     * @return Annotations
     */
    protected function getRouter(): Annotations
    {
        return $this->di->get('router');
    }
}
