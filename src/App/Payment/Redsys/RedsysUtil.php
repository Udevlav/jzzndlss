<?php

namespace App\Payment\Redsys;

use App\Payment\PaymentException;

use InvalidArgumentException;
use Throwable;

final class RedsysUtil
{

    /**
     * @param mixed $amount
     *
     * @return string
     */
    static public function amount($amount): string
    {
        return round(100 * floatval($amount));
    }

    /**
     * @param string $order
     *
     * @return string
     */
    static public function orderNo(string $order): string
    {
        if (ctype_digit($order)) {
            $order = sprintf('%012s', $order);
        }

        $len = strlen($order);

        if (4 > $len || $len > 12) {
            throw new InvalidArgumentException();
        }

        if (! preg_match('/^[0-9]{4}[0-9a-z]{0,8}$/i', $order)) {
            throw new InvalidArgumentException();
        }

        return $order;
    }

    /**
     * @param string $input
     *
     * @return string
     */
    static public function decodeBase64(string $input): string
    {
        return base64_decode($input);
    }

    /**
     * @param string $input
     *
     * @return string
     */
    static public function encodeBase64(string $input): string
    {
        return base64_encode($input);
    }

    /**
     * @param string $input
     *
     * @return string
     */
    static public function base64UrlDecode(string $input = null): string
    {
        return empty($input) ? '' : base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * @param string $input
     *
     * @return string
     */
    static public function base64UrlEncode(string $input): string
    {
        return strtr(base64_encode($input), '+/', '-_');
    }

    /**
     * @param string $key
     * @param string $data
     *
     * @return string
     */
    static public function mac(string $key, string $data): string
    {
        return hash_hmac('sha256', $data, $key, true);
    }

    /**
     * @param string $key       the key
     * @param string $plaintext the plain text
     *
     * @return string the ciphertext
     */
    static public function crypt(string $key, string $plaintext): string
    {
        if (function_exists('openssl_encrypt')) {
            $len = ceil(strlen($plaintext) / 8) * 8;
            $plaintext = $plaintext . str_repeat("\0", $len - strlen($plaintext));

            return substr(openssl_encrypt($plaintext, 'des-ede3-cbc', $key, OPENSSL_RAW_DATA, "\0\0\0\0\0\0\0\0"), 0, $len);
        } else {
            // MCrypt is not deprecated
            $iv = implode(array_map('chr', array(0, 0, 0, 0, 0, 0, 0, 0)));

            return mcrypt_encrypt(MCRYPT_3DES, $key, $plaintext, MCRYPT_MODE_CBC, $iv);
        }
    }

    /**
     * @param string $data
     *
     * @return string
     */
    static public function readOrder(string $data): string
    {
        $len = strlen("<Ds_Order>");
        $offsetStart = strrpos($data, "<Ds_Order>");
        $offsetEnd = strrpos($data, "</Ds_Order>");
        
        return substr($data, $offsetStart + $len, $offsetEnd - ($offsetStart + $len));
    }

    /**
     * @param string $data
     *
     * @return string
     */
    static public function readRequest(string $data): string
    {
        $len = strlen("</Request>");
        $offsetStart = strrpos($data, "<Request");
        $offsetEnd = strrpos($data, "</Request>");

        return substr($data, $offsetStart, ($offsetEnd + $len) - $offsetStart);
    }

    /**
     * @param string $data
     *
     * @return string
     */
    static public function readResponse(string $data): string
    {
        $len = strlen("</Response>");
        $offsetStart = strrpos($data, "<Response");
        $offsetEnd = strrpos($data, "</Response>");

        return substr($data, $offsetStart, ($offsetEnd + $len) - $offsetStart);
    }
}
