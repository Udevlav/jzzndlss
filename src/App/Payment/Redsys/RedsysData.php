<?php

namespace App\Payment\Redsys;

use InvalidArgumentException;

class RedsysData
{

    /**
     * @param RedsysPayment $payment
     *
     * @return RedsysData
     */
    static public function fromPayment(RedsysPayment $payment): RedsysData
    {
        $instance = new static();

        foreach (json_decode(RedsysUtil::base64UrlDecode($payment->merchantParameters), true) as $key => $val) {
            $instance->set($key, $val);
        }

        return $instance;
    }


    /**
     * @var string
     */
    const DEFAULT_CURRENCY = '978';


    /**
     * @var array
     */
    private $data = [];

    /**
     * @param string $key
     * @param string $value
     *
     * @return RedsysData
     */
    public function set(string $key, string $value): RedsysData
    {
        $this->data[strtoupper($key)] = $value;

        return $this;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return isset($this->data[strtoupper($key)]);
    }

    /**
     * @param string $key
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function get(string $key): string
    {
        if (! $this->has($key)) {
            throw new InvalidArgumentException();
        }

        return $this->data[strtoupper($key)];
    }


    /**
     * @param string $key
     *
     * @return RedsysPayment
     */
    public function createPayment(string $key): RedsysPayment
    {
        $payment = new RedsysPayment();

        $payment->signatureVersion = RedsysPayment::SIGNATURE_VERSION;
        $payment->merchantParameters = $this->toBase64();
        $payment->signature = $this->getSignature($key);

        return $payment;
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return string
     */
    public function toBase64(): string
    {
        return RedsysUtil::encodeBase64($this->toJson());
    }


    /**
     * Used to generate the payment form.
     *
     * @param string $key
     *
     * @return string
     */
    public function getSignature(string $key): string
    {
        // Generate Ds_MerchantParams
        $data = $this->toBase64();

        // Diversify key using the (unique) order number
        $key = RedsysUtil::decodeBase64($key);
        $key = RedsysUtil::crypt($key, $this->get('DS_MERCHANT_ORDER'));

        // Compute MAC
        $mac = RedsysUtil::mac($key, $data);

        return RedsysUtil::encodeBase64($mac);
    }

    /**
     * Used to generate the payment form.
     *
     * @param string $key
     * @param string $data
     *
     * @return string
     */
    public function getVerificationSignature(string $key, string $data): string
    {
        // Diversify key using the (unique) order number
        $key = RedsysUtil::decodeBase64($key);
        $key = RedsysUtil::crypt($key, $this->get('DS_ORDER'));

        // Compute MAC
        $mac = RedsysUtil::mac($key, $data);

        return RedsysUtil::base64UrlEncode($mac);
    }
}