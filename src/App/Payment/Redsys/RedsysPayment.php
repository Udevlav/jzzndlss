<?php

namespace App\Payment\Redsys;

class RedsysPayment
{

    /**
     * @var string
     */
    const SIGNATURE_VERSION = 'HMAC_SHA256_V1';


    /**
     * Factory method.
     *
     * @param array $data
     *
     * @return RedsysPayment
     */
    static public function fromArray(array $data): RedsysPayment
    {
        $instance = new static();
        $instance->signatureVersion = $data['Ds_SignatureVersion'];
        $instance->merchantParameters = $data['Ds_MerchantParameters'];
        $instance->signature = $data['Ds_Signature'];

        return $instance;
    }


    public $signatureVersion = self::SIGNATURE_VERSION;
    public $merchantParameters;
    public $signature;

    /**
     * Validates the payment.
     *
     * @return bool
     */
    public function isValid(): bool
    {

    }
}