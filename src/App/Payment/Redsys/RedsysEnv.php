<?php

namespace App\Payment\Redsys;

final class RedsysEnv
{

    /**
     * @var string
     */
    const TEST = 'test';

    /**
     * @var string
     */
    const PRODUCTION = 'real';
}