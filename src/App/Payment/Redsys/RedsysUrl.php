<?php

namespace App\Payment\Redsys;

final class RedsysUrl
{

    /**
     * @var string
     */
    const TEST = 'https://sis-t.redsys.es:25443/sis';

    /**
     * @var string
     */
    const PRODUCTION = 'https://sis.redsys.es/sis';
}