<?php

namespace App\Command\Cron;

use App\Cron\Crontab;
use App\Cron\Job;

use Cron\CronExpression;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseCronCommand extends Command
{

    static protected $cronExprToComponents = [
        '@yearly' => '0 0 1 1 *',
        '@annually' => '0 0 1 1 *',
        '@monthly' => '0 0 1 * *',
        '@weekly' => '0 0 * * 0',
        '@daily' => '0 0 * * *',
        '@hourly' => '0 * * * *',
    ];

    static protected $jobStatusToConsoleTag = [
        'unknown' => 'info',
        'success' => 'success',
        'error'   => 'error',
    ];

    static protected function jobToExpr(Job $job): CronExpression
    {
        return CronExpression::factory(implode(' ', [
            $job->getMinute(),
            $job->getHour(),
            $job->getDayOfMonth(),
            $job->getMonth(),
            $job->getDayOfWeek(),
        ]));
    }


    protected function getCrontab(InputInterface $input): Crontab
    {
        $crontab = new Crontab(false);
        $user = $input->getOption('user');

        if (null !== $user && get_current_user() !== $user) {
            $crontab->setUser($user);
        }

        $crontab->parseExistingCrontab();

        return $crontab;
    }

    protected function formatQuestion(string $message, string $hint = null, string $default = null)
    {
        if (null !== $hint) {
            $message .= sprintf(' (%s)', $hint);
        }

        if (null !== $default) {
            $message .= sprintf(' [%s]', $default);
        }

        return sprintf('<question> %s </question> > ', $message);
    }

    protected function writeCrontab(OutputInterface $output, Crontab $crontab)
    {
        $user = $crontab->getUser() ?? get_current_user();

        $output->writeln(sprintf('Crontab file for user <info>%s</info> (%d entries):', $user, count($crontab->getJobs())));
        $output->writeln('');
    }

    protected function writeJob(OutputInterface $output, Job $job)
    {
        $tag = static::$jobStatusToConsoleTag[$job->getStatus()];

        $output->writeln(sprintf('<%s>●</%s> Job #%s', $tag, $tag, $job->getHash()));
        $output->writeln(sprintf('       Entry: <comment>%s</comment>', $job->render()));

        if (! empty($job->getComments())) {
            $output->writeln(sprintf('    Comments: <comment>%s</comment>', $job->getComments()));
        }

        $output->writeln(sprintf('         Job: <comment>%s</comment>', $job->getCommand()));

        if (! empty($job->getLogFile())) {
            $output->writeln(sprintf('    Log file: <comment>%s</comment>', $job->getLogFile()));
        }

        if (! empty($job->getErrorFile())) {
            $output->writeln(sprintf('  Error file: <comment>%s</comment>', $job->getErrorFile()));
        }

        $scheduleShortcut = '';
        $schedule = implode(' ', [
            strval($job->getMinute()),
            strval($job->getHour()),
            strval($job->getDayOfMonth()),
            strval($job->getMonth()),
            strval($job->getDayOfWeek()),
        ]);

        if (in_array($schedule, array_values(static::$cronExprToComponents))) {
            $scheduleShortcut = sprintf(' (%s)', array_search($schedule, static::$cronExprToComponents));
        }

        $output->writeln(sprintf('    Schedule: <comment>%s</comment>%s',
            $schedule,
            $scheduleShortcut
        ));

        $output->writeln(sprintf('            ├─ Minute: <comment>%s</comment>', $job->getMinute()));
        $output->writeln(sprintf('            ├─ Hour: <comment>%s</comment>', $job->getHour()));
        $output->writeln(sprintf('            ├─ Day of month: <comment>%s</comment>', $job->getDayOfMonth()));
        $output->writeln(sprintf('            ├─ Month: <comment>%s</comment>', $job->getMonth()));
        $output->writeln(sprintf('            └─ Day of week: <comment>%s</comment>', $job->getDayOfWeek()));

        if (! empty($job->getLastRunTime())) {
            $output->writeln(sprintf('    Last run: <%s>%s (status=%s)</%s>',
                $tag,
                $job->getLastRunTime()->format('r'),
                $job->getStatus(),
                $tag
            ));
        } else {
            $output->writeln('    Last run: <comment>(no executions yet)</comment>');
        }

        $output->writeln('   Next runs:');
        $expr = static::jobToExpr($job);

        foreach ($expr->getMultipleRunDates(10) as $offset => $date) {
            $tree = ($offset === 9) ? '└─' : '├─';
            $output->writeln(sprintf('            %s %s', $tree, $date->format('r')));
        }

        $output->writeln('');
    }
}