<?php

namespace App\Command\Cron;

use App\Cron\Crontab;
use App\Cron\Job;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class EditCronCommand extends ListCronCommand
{

    private $jobsToRemove = [];

    protected function configure()
    {
        $this
            ->setName('cron:edit')
            ->setDescription('Edits cron jobs for an user.')
            ->addOption('user', 'u', InputOption::VALUE_OPTIONAL, 'Crontab user (requires sudo privileges)')
        ;
    }

    public function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('');
        $helper = $this->getHelper('question');

        if (null === $input->getOption('user')) {
            $output->writeln('Note that accessing other user\'s crontab requires sudo privileges');
            $output->writeln('');
            $question = new Question($this->formatQuestion('The user you want to schedule the job for', 'press ENTER for the current user', get_current_user()));
            $user = $helper->ask($input, $output, $question);
            $input->setOption('user', $user);
            $output->writeln('');
        }
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $crontab = $this->getCrontab($input);

        $this->writeCrontab($output, $crontab);

        foreach ($crontab->getJobs() as $job) {
            $this->editJob($input, $output, $crontab, $job);
        }

        if (count($this->jobsToRemove)) {
            foreach ($this->jobsToRemove as $job) {
                $tag = static::$jobStatusToConsoleTag[$job->getStatus()];

                $output->writeln(sprintf('<%s>●</%s> Job <info>#%s</info> will be removed !',
                    $tag,
                    $tag,
                    $job->getHash()
                ));
            }

            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion($this->formatQuestion('Proceed?', 'y/n', 'n'), false);
            $output->writeln('');

            if ($helper->ask($input, $output, $question)) {
                $crontab->write();
                $output->writeln('');
                $output->writeln(sprintf('<info>Done</info> (deleted %d jobs)', count($this->jobsToRemove)));
                $output->writeln('');
            } else {
                $output->writeln('');
                $output->writeln('<info>Aborted by the user</info>');
                $output->writeln('');
            }

        } else {
            $output->writeln('<info>Nothing to do</info>');
            $output->writeln('');
        }
    }


    protected function editJob(InputInterface $input, OutputInterface $output, Crontab $crontab, Job $job)
    {
        $this->writeJob($output, $job);

        if ($input->isInteractive()) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion($this->formatQuestion('Do you want to remove this job?', 'y/n', 'n'), false);

            if ($helper->ask($input, $output, $question)) {
                $crontab->removeJob($job);
                $this->jobsToRemove[] = $job;
            }

            $output->writeln('');
        }
    }
}