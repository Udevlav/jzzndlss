<?php

namespace App\Command\Cron;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ListCronCommand extends BaseCronCommand
{

    protected function configure()
    {
        $this
            ->setName('cron:list')
            ->setDescription('Lists cron jobs for the current user.')
            ->addOption('user', 'u', InputOption::VALUE_OPTIONAL, 'Crontab user (requires sudo privileges)')
        ;
    }

    public function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('');
        $helper = $this->getHelper('question');

        if (null === $input->getOption('user')) {
            $output->writeln('Note that accessing other user\'s crontab requires sudo privileges');
            $output->writeln('');
            $question = new Question($this->formatQuestion('The user you want to schedule the job for', 'press ENTER for the current user', get_current_user()));
            $user = $helper->ask($input, $output, $question);
            $input->setOption('user', $user);
            $output->writeln('');
        }
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $crontab = $this->getCrontab($input);

        $this->writeCrontab($output, $crontab);

        foreach ($crontab->getJobs() as $job) {
            $this->writeJob($output, $job);
        }

        $output->writeln(sprintf('<info>Done</info> (found %d jobs)', count($crontab->getJobs())));
        $output->writeln('');
    }
}