<?php

namespace App\Command\Cron;

use App\Cron\Job;

use Cron\CronExpression;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

use RuntimeException;

class ScheduleJobCommand extends BaseCronCommand
{

    protected function configure()
    {
        $this
            ->setName('cron:schedule')
            ->setDescription('Schedules a cron job.')
            ->addOption('job', 'j', InputOption::VALUE_OPTIONAL, 'Job to schedule')
            ->addOption('schedule', 's', InputOption::VALUE_OPTIONAL, 'Job schedule')
            ->addOption('user', 'u', InputOption::VALUE_OPTIONAL, 'Crontab user (requires sudo privileges)')
        ;
    }

    public function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('');
        $helper = $this->getHelper('question');

        if (null === $input->getOption('user')) {
            $output->writeln('Note that accessing other user\'s crontab requires sudo privileges');
            $output->writeln('');

            $question = new Question($this->formatQuestion('The user you want to schedule the job for', 'press ENTER for the current user', get_current_user()));
            $user = $helper->ask($input, $output, $question);

            $input->setOption('user', $user);
        }

        if (null === $input->getOption('job')) {
            $question = new Question($this->formatQuestion('The job you want to schedule'));
            $question->setValidator(function($answer) {
                if ( empty(trim($answer))) {
                    throw new RuntimeException('The command should not be empty.');
                }

                return $answer;
            });

            $input->setOption('job', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('schedule')) {
            $didSchedule = false;

            while (! $didSchedule) {
                $question = new Question($this->formatQuestion('The job schedule'));
                $question->setValidator(function($answer) {
                    if (! CronExpression::isValidExpression($answer)) {
                        throw new RuntimeException(sprintf('Invalid cron tab expression "%s".', $answer));
                    }

                    return $answer;
                });

                $schedule = $helper->ask($input, $output, $question);
                $input->setOption('schedule', $schedule);
                $expr = CronExpression::factory($schedule);

                $output->writeln('');
                $output->writeln('Next 10 job execution dates:');

                foreach ($expr->getMultipleRunDates(10) as $offset => $date) {
                    $tree = ($offset === 9) ? '└─' : '├─';
                    $output->writeln(sprintf(' %s <comment>%s</comment>', $tree, $date->format('r')));
                }

                $output->writeln('');
                $confirmation = new ConfirmationQuestion($this->formatQuestion('Is that OK?', 'y/n', 'n'), false);
                $didSchedule = $helper->ask($input, $output, $confirmation);
                $output->writeln('');
            }
        }
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $crontab = $this->getCrontab($input);

        $newJob = $this->createNewJob($input, $output);
        $newJobHash = $newJob->getHash();

        foreach ($crontab->getJobs() as $job) {
            if ($job->getHash() === $newJobHash) {
                $output->writeln(sprintf('<error>Warning: the crontab already contains the specified job with the given schedule. Aborting</error>.', $newJob->getCommand()));
                $output->writeln('');

                return 0;
            }

            if ($job->getCommand() === $newJob->getCommand()) {
                $output->writeln(sprintf('<error>Warning: the crontab already contains a "%s" job</error>', $newJob->getCommand()));

                if ($input->isInteractive()) {
                    $helper = $this->getHelper('question');
                    $question = new ConfirmationQuestion($this->formatQuestion('Add the new job anyway?', 'y/n', 'n'), false, '/^(y|s)/');
                    $output->writeln('');

                    if (! $helper->ask($input, $output, $question)) {
                        $output->writeln('');
                        $output->writeln('<error>Aborted by the user</error>.');
                        $output->writeln('');

                        return 0;
                    }
                }
            }
        }

        $crontab->addJob($newJob);
        $crontab->write();

        $this->writeCrontab($output, $crontab);

        foreach ($crontab->getJobs() as $job) {
            $this->writeJob($output, $job);
        }

        $output->writeln('<info>Done</info>');
        $output->writeln('');
    }

    protected function createNewJob(InputInterface $input, OutputInterface $output): Job
    {
        $schedule = $input->getOption('schedule');

        if (isset (static::$cronExprToComponents[$schedule])) {
            $schedule = static::$cronExprToComponents[$schedule];
        }

        $job = Job::parse(sprintf('%s %s', $schedule, $input->getOption('job')));
        $job->setComments(sprintf('Added to crontab by cron:schedule on %s', date('r')));

        return $job;
    }
}