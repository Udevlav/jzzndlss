<?php

namespace App\Command\Clean;

use App\Mailer\Mailer;
use App\Model\Promotion;

use App\Util\Bytes;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DateTimeImmutable;

class CleanPromotionsCommand extends CleanCommand
{

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('gc:promotions')
            ->setDescription('Cleans old promotions.')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'Dry-run (DO NOT delete enything)')
            ->addOption('sendmail', 'm', InputOption::VALUE_NONE, 'Whether to enable notification by email to the admaster')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dryRun = (bool) $input->getOption('dry-run');
        $sendmail = (bool) $input->getOption('sendmail');

        $total = 0;
        $errors = 0;
        $failedIds = [];

        $output->writeln('Cleaning old promotions ...');
        $offset = 0;

        $startedAt = new DateTimeImmutable();
        $microtime = microtime(true);

        while (true) {
            $records = Promotion::find([
                sprintf('validTo < "%s"', date('Y-m-d H:i:s')),
                'limit' => 100,
                'offset' => $offset,
            ]);

            if (0 === count($records)) {
                $output->write("\n");
                break;
            }

            $offset += count($records);

            foreach ($records as $record) {
                if (! $dryRun && false === $record->delete()) {
                    $failedIds[] = $record->id;
                    $errors ++;
                    $output->write('<error>F</error>');
                } else {
                    $output->write('.');
                }

                $total ++;

                if (0 === $total % 80) {
                    $output->write(sprintf('  (%03d processed)', $total));
                    $output->write("\n");
                }
            }
        }

        $dir = $this->getConfig()->layout->rootDir;
        $endedAt = new DateTimeImmutable();
        $diskSpace = Bytes::format(disk_free_space('/'));
        $relSpace = 100 * disk_free_space($dir) / disk_total_space($dir);
        $peakMemory = Bytes::format(memory_get_peak_usage(true));
        $diffMemory = Bytes::format(memory_get_usage(true));
        $microtime = microtime(true) - $microtime;

        if ($sendmail) {
            $mailer = $this->getMailer();
            $mailer->useMailbox('admaster');
            $mailer->send($mailer->getMailbox()->from, 'Clean process report', '../mail/cleanedPromotions.volt', [
                'total'      => $total,
                'errors'     => $errors,
                'failedIds'  => $failedIds,
                'startedAt'  => $startedAt->format('H:i:s'),
                'endedAt'    => $endedAt->format('H:i:s'),
                'peakMemory' => $peakMemory,
                'diffMemory' => $diffMemory,
                'millitime'  => number_format(1e3 * $microtime, 3),
                'diskSpace'  => $diskSpace,
                'relSpace'   => number_format($relSpace, 2),
            ]);
        }

        $output->writeln('');
        $output->writeln(sprintf('Done <info>%d</info> records cleaned.', $total));

        if ($errors) {
            $output->writeln(sprintf('The following records could not be deleted: <warning>%s</warning>.', implode(',', $failedIds)));
        }
    }

    private function getMailer(): Mailer
    {
        return $this->getDI()->get('mailer');
    }
}
