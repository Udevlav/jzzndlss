<?php

namespace App\Command\Clean;

use App\Model\ChatMessage;

use Phalcon\Mvc\Model\ResultsetInterface;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DateInterval;
use DateTime;
use RuntimeException;

class CleanChatMessagesCommand extends CleanCommand
{

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('clean:chat')
            ->setDescription('Cleans old chat messages.')
            ->addOption('days', 'd', InputOption::VALUE_OPTIONAL, 'The number of days to keep.', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $days = $input->getOption('days');
        $date = $this->getCutoffDate($days);
        $total = 0;

        $output->writeln(sprintf('Cleaning chat messages older than %d days ...', $days));

        while (true) {
            $records = $this->moreRecords($date);

            if (! $records || 0 === count($records)) {
                // No more records
                break;
            }

            if (false === $records->delete()) {
                throw new RuntimeException('Failed to remove records.');
            }

            $total += count($records);
        }

        $output->writeln(sprintf('Done <info>%d</info> records cleaned.', $total));
    }

    private function getCutoffDate(int $days): DateTime
    {
        $date = new DateTime();
        $date->sub(new DateInterval(sprintf('PD%d', $days)));
        $date->setTime(23, 59, 59);

        return $date;
    }

    private function moreRecords(DateTime $cutoff): ResultsetInterface
    {
        return ChatMessage::find([
            sprintf('sentAt < "%s"', $cutoff->format('Y-m-d H:i:s')),
            'limit' => 100,
        ]);
    }
}