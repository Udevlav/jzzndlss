<?php

namespace App\Command\Clean;

use App\Command\InjectionAwareCommand;

use Phalcon\Db\Adapter\Pdo;

abstract class CleanCommand extends InjectionAwareCommand
{


    /**
     * @return \Phalcon\Db\Adapter\Pdo
     */
    protected function getDb(): Pdo
    {
        return $this->getDI()->get('db');
    }

}