<?php

namespace App\Command;

use App\Config\Config;
use Phalcon\DiInterface;
use Phalcon\Di\InjectionAwareInterface;

use Symfony\Component\Console\Command\Command;

class InjectionAwareCommand extends Command implements InjectionAwareInterface
{

    /**
     * @var DiInterface
     */
    protected $di;

    /**
     * @return DiInterface
     */
    public function getDI(): DiInterface
    {
        return $this->di;
    }

    /**
     * @param DiInterface $di
     */
    public function setDI(DiInterface $di)
    {
        $this->di = $di;
    }


    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    protected function getPhpCommand(string $job): string
    {
        return sprintf('php %s %s %s',
            $this->getXDebugFlags(),
            $this->getCliPath(),
            $job
        );
    }

    protected function getCliPath(): string
    {
        return $this->getConfig()->layout->binDir .'cli';
    }

    protected function getXDebugFlags(): string
    {
        if (! $this->getConfig()->app->debug) {
            return '';
        }

        return implode(' ', [
            '-dxdebug.idekey=PHPSTORM',
            '-dxdebug.remote_autostart=1',
            '-dxdebug.remote_enable=1',
            '-dxdebug.remote_host=127.0.0.1',
            '-dxdebug.remote_port=9009',
            '-dxdebug.remote_mode=req',
        ]);
    }

}