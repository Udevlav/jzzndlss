<?php

namespace App\Command\Util;

use App\Model\Ad;
use Symfony\Component\Console\Output\OutputInterface;

final class Dumper
{
    
    protected $output;
    
    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function dump(Ad $ad)
    {
        $this
            ->newLine()
            ->line()
            ->head($ad)
            ->line()
            ->fields([
                'Publish' => $ad->publishedAt ?? '(Not published)',
            ])
            ->fields([
                'Created' => $ad->createdAt,
                'IP' => $ad->createdFrom,
            ])
            ->fields([
                'Created' => $ad->updatedAt,
                'IP' => $ad->updatedFrom,
            ])
            ->fields([
                'Images' => $ad->numImages,
                'Videos' => $ad->numVideos,
            ])
            ->fields([
                'Exps' => $ad->numExperiences,
                'Hits' => $ad->numHits,
            ])
            ->line()
            ->fields([
                'Lat' => $ad->lat,
                'ZIP' => $ad->postalCode,
            ])
            ->fields([
                'Lng' => $ad->lng,
                'City' => $ad->city,
            ])
            ->fields([
                'Zone' => $ad->zone,
            ])
            ->line()
            ->fields([
                'Name' => $ad->name,
                'Age'  => $ad->age,
            ])
            ->fields([
                'Phone' => $ad->phone,
                'Email'  => $ad->email,
            ])
            ->fields([
                'Twitter' => $ad->twitter,
                'Whatsapp' => $ad->whatsapp,
            ])
            ->fields([
                'Ethn' => $ad->ethnicity,
                'Origin' => $ad->origin,
            ])
            ->fields([
                'Occup' => $ad->occupation,
                'School' => $ad->schooling,
            ])
            ->fields([
                'Hair' => $ad->hairColor,
                'BSize' => $ad->bsize,
            ])
            ->line()
            ->fields([
                'Title' => $ad->title,
                'Text' => $ad->text,
            ])
            ->line()
            ->fields([
                'Fee' => $ad->fee,
            ])
            ->fields([
                'Cash' => $ad->cash,
                'Card' => $ad->card,
            ])
            ->line()
            ->newLine()
        ;
    }

    protected function newLine(): Dumper
    {
        $this->output->writeln('');
    }

    protected function line(): Dumper
    {
        $this->output->writeln(str_repeat('-', 80));

        return $this;
    }

    protected function head(Ad $ad): Dumper
    {
        if (strlen($ad->title) > 64) {
            $title = substr($ad->title, 0, 61) .'...';
        } else {
            $title = $ad->title;
        }

        $this->output->writeln(sprintf('| %-10d  %-64s |', $ad->id, $title));
        
        return $this;
    }

    protected function fields(array $fields): Dumper
    {
        $cols = count($fields);
        $colWidth = floor(72 / $cols);
        $valWidth = $colWidth - (4 + 3 * ($cols - 1));
        $format = sprintf('%%-5s : %%-%ds', $valWidth);

        $this->output->write('| ');

        foreach ($fields as $key => $val) {
            $this->output->write(sprintf($format, substr($key, 0, 5), substr($val, 0, $valWidth)));
            $this->output->write(' | ');
        }

        $this->output->write("\n");
        
        return $this;
    }
}