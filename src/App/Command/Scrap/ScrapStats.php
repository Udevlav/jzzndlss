<?php

namespace App\Command\Scrap;

use App\Scrap\ScrappingTarget;

use Throwable;

class ScrapStats
{

    /**
     * @var int
     */
    const FAILED = 1;

    /**
     * @var int
     */
    const SKIPPED = 2;

    /**
     * @var int
     */
    const SCRAPPED = 4;

    /**
     * @var int
     */
    const PERSISTENCE_FAILED = 8;

    /**
     * @var int
     */
    const PERSISTED = 16;

    /**
     * @var int
     */
    const FIXTURE_FAILED = 32;

    /**
     * @var int
     */
    const FIXTURED = 64;

    /**
     * @var int
     */
    const PUBLICATION_FAILED = 128;

    /**
     * @var int
     */
    const PUBLISHED = 256;


    /**
     * @var int
     */
    private $startTime;

    /**
     * @var int
     */
    private $expected;

    /**
     * @var array
     */
    private $targets = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var int
     */
    private $failed = 0;

    /**
     * @var int
     */
    private $skipped = 0;

    /**
     * @var int
     */
    private $scrapped = 0;

    /**
     * @var int
     */
    private $downloaded = 0;

    /**
     * @var int
     */
    private $persisted = 0;

    /**
     * @var int
     */
    private $persistFailed = 0;

    /**
     * @var int
     */
    private $fixtured = 0;

    /**
     * @var int
     */
    private $fixtureFailed = 0;

    /**
     * @var int
     */
    private $published = 0;

    /**
     * @var int
     */
    private $publicationFailed = 0;

    /**
     * @var int
     */
    private $timeToComplete = 0;

    /**
     * @param int $expected
     *
     * @return void
     */
    public function __construct(int $expected)
    {
        $this->expected = $expected;
    }


    public function tic()
    {
        $this->startTime = microtime(true);
    }

    public function tac(): float
    {
        return microtime(true) - $this->startTime;
    }


    public function getStartTime()
    {
        return $this->startTime;
    }

    public function getStartTimeHumanReadable()
    {
        return date('r', $this->startTime);
    }

    public function getTimeToComplete()
    {
        return $this->timeToComplete;
    }

    public function getTimeToCompleteMins()
    {
        return ceil($this->timeToComplete / 1e6 / 60);
    }

    public function setStartTime(int $time)
    {
        $this->startTime = $time;
    }

    public function setEndTime(int $time)
    {
        $this->timeToComplete = $time - $this->startTime;
    }


    public function scrapped(ScrappingTarget $target)
    {
        $this->targets[$this->key($target)] = static::SCRAPPED;
        $this->scrapped ++;
    }

    public function skipped(ScrappingTarget $target)
    {
        $this->targets[$this->key($target)] = static::SKIPPED;
        $this->skipped ++;
    }

    public function failed(ScrappingTarget $target, Throwable $t = null)
    {
        $this->targets[$this->key($target)] = static::FAILED;
        $this->errors[$this->key($target)] = $t;
        $this->failed ++;
    }

    public function persisted(ScrappingTarget $target)
    {
        $this->targets[$this->key($target)] |= static::PERSISTED;
        $this->persisted ++;
    }

    public function fixtured(ScrappingTarget $target)
    {
        $this->targets[$this->key($target)] |= static::FIXTURED;
        $this->fixtured ++;
    }

    public function published(ScrappingTarget $target)
    {
        $this->targets[$this->key($target)] |= static::PUBLISHED;
        $this->published ++;
    }

    public function persistFailed(ScrappingTarget $target, Throwable $t = null)
    {
        $this->targets[$this->key($target)] |= static::PERSISTENCE_FAILED;
        $this->errors[$this->key($target)] = $t;
        $this->persistFailed ++;
    }

    public function fixtureFailed(ScrappingTarget $target, Throwable $t = null)
    {
        $this->targets[$this->key($target)] |= static::FIXTURE_FAILED;
        $this->errors[$this->key($target)] = $t;
        $this->fixtureFailed ++;
    }

    public function publishFailed(ScrappingTarget $target, Throwable $t = null)
    {
        $this->targets[$this->key($target)] |= static::PUBLICATION_FAILED;
        $this->errors[$this->key($target)] = $t;
        $this->publicationFailed ++;
    }


    public function isComplete(): bool
    {
        return $this->expected === $this->getTotal();
    }

    public function getTargets(): array
    {
        return $this->targets;
    }

    public function getExpectedTotal(): int
    {
        return $this->expected;
    }

    public function getTotal(): int
    {
        return count($this->targets);
    }

    public function getFailed(): int
    {
        return $this->failed;
    }

    public function getFailedPct(): float
    {
        return $this->pct($this->failed, $this->getTotal());
    }

    public function getSkipped(): int
    {
        return $this->skipped;
    }

    public function getSkippedPct(): float
    {
        return $this->pct($this->skipped, $this->getTotal());
    }

    public function getScrapped(): int
    {
        return $this->scrapped;
    }

    public function getScrappedPct(): float
    {
        return $this->pct($this->scrapped, $this->getTotal());
    }

    public function getPersisted(): int
    {
        return $this->persisted;
    }

    public function getPersistedPct(): float
    {
        return $this->pct($this->persisted, $this->scrapped);
    }

    public function getPersistFailed(): int
    {
        return $this->persistFailed;
    }

    public function getPersistFailedPct(): float
    {
        return $this->pct($this->persistFailed, $this->scrapped);
    }

    public function getFixtured(): int
    {
        return $this->fixtured;
    }

    public function getFixturedPct(): float
    {
        return $this->pct($this->fixtured, $this->scrapped);
    }

    public function getFixtureFailed(): int
    {
        return $this->fixtureFailed;
    }

    public function getFixtureFailedPct(): float
    {
        return $this->pct($this->fixtureFailed, $this->scrapped);
    }

    public function getPublished(): int
    {
        return $this->published;
    }

    public function getPublishedPct(): float
    {
        return $this->pct($this->published, $this->scrapped);
    }

    public function getPublicationFailed(): int
    {
        return $this->publicationFailed;
    }

    public function getPublicationFailedPct(): float
    {
        return $this->pct($this->publicationFailed, $this->scrapped);
    }


    protected function key(ScrappingTarget $target): string
    {
        return md5($target->getUrl());
    }

    protected function pct(int $n, $t): float
    {
        if (0 >= $t) {
            return 0;
        }

        return 100 * round($n / $t, 4);
    }
}