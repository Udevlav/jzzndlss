<?php

namespace App\Command\Scrap;

use App\Command\InjectionAwareCommand;
use App\Command\Util\Dumper;
use App\Manager\ModelManager;
use App\Manager\ScrapModelManager;
use App\Model\Ad;
use App\Model\Media;
use App\Repository\EntityManager;
use App\Scrap\NotFoundException;
use App\Scrap\Scrapper;
use App\Scrap\Scrapper\ForosXScrapper;
use App\Scrap\Scrapper\NLoquoScrapper;
use App\Scrap\Scrapper\PasionScrapper;
use App\Scrap\Scrapper\SlumiScrapper;
use App\Scrap\Scrapper\SustitutasScrapper;
use App\Scrap\ScrappingDownload;
use App\Scrap\ScrappingException;
use App\Scrap\ScrappingTarget;
use App\Scrap\ScrappingTargetStatus;

use Phalcon\Mvc\Model\Resultset;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use LogicException;
use OutOfRangeException;
use OutOfBoundsException;
use RuntimeException;
use Throwable;

class ScrapCommand extends InjectionAwareCommand
{

    /**
     * @const int
     */
    const MAX_ALLOWED_PUBLISH_FAILS = 15;

    /**
     * @const int
     */
    const MAX_ALLOWED_PERSIST_FAILS = 15;

    /**
     * @const int
     */
    const MAX_ALLOWED_SUCCESS = 30;


    /**
     * Available scrapping targets.
     *
     * @var string[]
     */
    static protected $sources = [
        'forosx',
        'nloquo',
        'pasion',
        'slumi',
        'sustitutas',
    ];

    /**
     * @var Dumper
     */
    protected $dumper;

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('scrap:run')
            ->setDescription('Gathers ad data via scrapping.')
            ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'The scrapping source.')
            ->addOption('target', 't', InputOption::VALUE_OPTIONAL, 'A single scrapping target.')
            ->addOption('cache', 'c', InputOption::VALUE_OPTIONAL, 'Whether to use cache.')
            ->addOption('lenient', 'l', InputOption::VALUE_OPTIONAL, 'Whether to be lenient on individual scrapping errors.')
            ->addOption('download', 'd', InputOption::VALUE_OPTIONAL, 'Whether to download files.')
            ->addOption('persist', 'p', InputOption::VALUE_OPTIONAL, 'Whether to persist entities.')
            ->addOption('fixture', 'f', InputOption::VALUE_OPTIONAL, 'Whether to generate fixtures.')
            ->addOption('publish', 'a', InputOption::VALUE_OPTIONAL, 'Whether to auto-publish entities.')
            ->addOption('max', 'm', InputOption::VALUE_OPTIONAL, 'The max number of ads to scrap.')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $config = $this->di->get('config');
        $helper = $this->getHelper('question');

        if (null === $input->getOption('source')) {
            $output->writeln([
                '',
                'This is the website you\'re going to scrap.',
                '',
            ]);
            $question = new ChoiceQuestion($this->formatQuestion('Select the scrapping source (required)'), static::$sources);

            $input->setOption('source', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('target')) {
            $output->writeln([
                '',
                'A typical scrapping session begins collecting a set of links which ',
                'are later scrapped on a one-by-one basis. You can choose to scrap ',
                'a single target specifying the following option, or to process all ',
                'the collected links leaving it blank.',
                '',
            ]);
            $question = new Question($this->formatQuestion('Select a single scrapping target (optional), or press ENTER to skip'));
            $question->setValidator(function($answer) {
                if (empty($answer)) {
                    return null;
                }

                return $answer;
            });

            $input->setOption('target', $helper->ask($input, $output, $question));

            if (null === $input->getOption('target')) {
                $output->writeln(['', 'You have chosen to scrapp all links.', '',]);
            } else {
                $output->writeln(['', 'You have chosen to scrapp a single target.', '',]);
            }
        }

        if (null === $input->getOption('max')) {
            $output->writeln([
                '',
                'The scrapper will look for all ads, but you can limit the maximum',
                'number of records to look for. Use 0 for no limit.',
                '',
            ]);
            $question = new Question($this->formatQuestion('Max records', 0), 0);

            $input->setOption('max', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('cache')) {
            $output->writeln([
                '',
                'As scrapping can be an intensive operation, which requires a lot of',
                'requests. You can cache the scrapping session, so you can resume ',
                'the session later. The cache files are stored in a temporary folder, ',
                'so it can be deleted if the machine is rebooted, or even if the user ',
                'session is closed.',
                '',
            ]);
            $question = new ConfirmationQuestion($this->formatConfirmation('Use cache?', true), true, '/^(y|s)/');

            $input->setOption('cache', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('lenient')) {
            $output->writeln([
                '',
                'Pages change frequently, so its generally not safe to run a ',
                'scrapper. You can choose between fail and abort the current ',
                'scrapping session on the first error, or to be lenient.',
                '',
            ]);
            $question = new ConfirmationQuestion($this->formatConfirmation('Be lenient?', true), true, '/^(y|s)/');

            $input->setOption('lenient', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('fixture')) {
            $output->writeln([
                '',
                'You can generate SQL fixtures on a scrapping session and save them',
                'for later, or just to test. Fixtures are placed at:',
                '<info>'. $this->formatPath($config->layout->fixturesDir) .'</info>',
                '',
            ]);
            $question = new ConfirmationQuestion($this->formatConfirmation('Generate fixtures?', true), true, '/^(y|s)/');

            $input->setOption('fixture', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('download')) {
            $output->writeln([
                '',
                'Downloaded files are prefixed with the scrapper name, and stored in:',
                '<info>'. $this->formatPath($config->layout->uploadDir) .'</info>',
                '',
            ]);
            $question = new ConfirmationQuestion($this->formatConfirmation('Download files?', true), true, '/^(y|s)/');

            $input->setOption('download', $helper->ask($input, $output, $question));
        }

        if (null === $input->getOption('persist')) {
            $output->writeln([
                '',
                'You can choose whether to save results to database or not. Scrapped',
                'data will be published using the scrapper name (the name you selected ',
                'in first place) as advertiser and provider, and <info>'. $config->mailbox->scrap->from . '</info>',
                'as email address. To manage these ads, please access the URL',
                '<info>/anunciante/:provider</info>',
                '',
            ]);
            $question = new ConfirmationQuestion($this->formatConfirmation('Persist data?', true), true, '/^(y|s)/');

            $input->setOption('persist', $helper->ask($input, $output, $question));
        }

        if ($input->getOption('persist')) {
            $output->writeln([
                '',
                'You can choose whether to automatically publish scrapped data or not.',
                'Remember you can always publish the scrapped ads manually at the URL',
                '<info>/anunciante/:provider</info>',
                '',
            ]);
            $question = new ConfirmationQuestion($this->formatConfirmation('Publish data?', true), true, '/^(y|s)/');

            $input->setOption('publish', $helper->ask($input, $output, $question));
        } else {
            $input->setOption('publish', false);
        }

        $output->writeln('');
    }

    /**
     * @inheritdoc
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lenient = $input->getOption('lenient');
        $source = $input->getOption('source');
        $target = $input->getOption('target');
        $verbose = $input->getOption('verbose');
        $useCache = (bool) $input->getOption('cache');
        $download = (bool) $input->getOption('download');
        $publish = $input->getOption('publish');
        $persist = $input->getOption('persist');
        $fixtures = $input->getOption('fixture');
        $max = $input->getOption('max');

        // Create scrapper
        $scrapper = $this->createScrapper($source, $useCache, $download);
        $cacheDir = $scrapper->cachePath();

        if ($useCache) {
            $output->writeln(sprintf('Using cache dir <comment>%s</comment>.', $cacheDir));
        }

        // Check for target
        if (null !== $target) {
            $scrapper->getSession()->add($target);
            $total = 1;

        } else {
            $output->writeln('Collecting links (this may take a while) ...');

            $scrapper->schedule();
            $total = count($scrapper->getSession());

            $output->writeln(sprintf('Collected <comment>%s targets</comment>%s.', number_format($total, 0, ',', '.'), $scrapper->isFromCache() ? ' (from cache)' : ' (fresh)'));
        }

        if ($input->isInteractive()) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion($this->formatConfirmation(sprintf('You\'re about to scrap %s target(s). Proceed?', number_format($total, 0, ',', '.')), true), true, '/^(y|s)/');
            $output->writeln('');

            if (! $helper->ask($input, $output, $question)) {
                $output->writeln('Operation aborted by the user.');

                return 0;
            }
        }

        $stats = new ScrapStats($total);
        $stats->setStartTime(microtime(true));

        foreach ($scrapper->getSession()->getTargets() as $offset => $target) {
            $output->writeln('');

            if ($max > 0 && $stats->getScrapped() >= $max) {
                break;
            }

            $this->writeTargetHeader($output, $scrapper, $target, $offset, $total);
            $stats->tic();

            try {
                $scrapper->scrap($target);
            } catch (NotFoundException $e) {
                $this->writeTargetNotFound($output, $e);

                continue;
            } catch (ScrappingException $f) {
                $this->writeTargetError($output, $f);

                continue;
            }

            if ($target->isOcr()) {
                $this->writeTargetPhoneOcr($output, $target);
            }

            foreach ($target->getDownloads() as $download) {
                $this->writeTargetDownload($output, $download);
            }

            // Check if the target is marked as scrapped
            if (! $target->isScrapped()) {
                $stats->skipped($target);
                $target->addStatus(ScrappingTargetStatus::SCRAP_INVALID_DATA);

                $this->writeTargetNotScrapped($output);

                continue;
            }

            // If the target is not worth processing (e.g., incomplete
            // or malformed ad data), then skip now
            if (! $target->isWorthProcessing()) {
                $stats->skipped($target);

                $this->writeTargetNotWorthProcessing($output);

                continue;
            }

            // Skip if target already exists
            if (null !== $ad = $this->find($target)) {
                /** @var Ad $ad */
                $stats->skipped($target);
                $this->writeTargetAlreadyExists($output, $target);

                continue;
            }

            // Skip if ad has no images
            $ad = $target->getData();

            if (null !== $ad && 0 === count($ad->images)) {
                /** @var Ad $ad */
                $stats->skipped($target);
                $this->writeTargetHasNoImages($output, $target);

                continue;
            }

            $stats->scrapped($target);

            if ($fixtures) {
                try {
                    $this->fixture($target);

                    $stats->fixtured($target);
                    $this->writeTargetFixtureSuccess($output);
                } catch (LogicException $e) {
                    $stats->fixtureFailed($target, $e);
                    $this->writeTargetFixtureFailed($output, $stats, $e);

                    continue;
                }
            }

            if ($persist) {
                try {
                    $this->persist($target);

                    $stats->persisted($target);
                    $this->writeTargetPersistSuccess($output, $target);
                } catch (OutOfBoundsException $e) {
                    $stats->persistFailed($target, $e);
                    $this->writeTargetPersistFailed($output, $stats, $e);

                    continue;
                }
            }

            if ($publish) {
                try {
                    $this->publish($output, $scrapper, $target);

                    $stats->published($target);
                    $this->writeTargetPublishSuccess($output, $target);
                } catch (OutOfRangeException $e) {
                    $stats->publishFailed($target, $e);
                    $this->writeTargetPublishFailed($output, $stats, $e);

                    continue;
                }
            }

            if ($verbose) {
                $this->debug($output, $target, $cacheDir);
            }

            $this->writeTargetDone($output, $stats);

            if ($stats->getPersistFailed() > self::MAX_ALLOWED_PERSIST_FAILS) {
                // DO NOT keep trying; somehting bad may be happening
                // which prevents resources to be persisted
                $this->writeNewLine($output);
                $this->writeTooMuchPersistenceErrors($output, $stats);

                break;
            }

            if ($stats->getPublicationFailed() > self::MAX_ALLOWED_PUBLISH_FAILS) {
                // DO NOT keep trying; somehting bad may be happening
                // which prevents resources to be published
                $this->writeNewLine($output);
                $this->writeTooMuchPublicationErrors($output, $stats);

                break;
            }

            if ($stats->getPublished() > self::MAX_ALLOWED_SUCCESS) {
                // Don't be greedy. MAX_ALLOWED_SUCCESS is anough so far...
                $this->writeNewLine($output);
                $this->writeSoFarSoGood($output, $stats);

                // Be greedy.
                //break;
            }
        }

        $this->removeDir($cacheDir);
        $this->writeNewLine($output);
        $this->writeCacheCleaned($output, $cacheDir);

        $stats->setEndTime(microtime(true));
        $this->notify($scrapper, $stats);
        $this->writeNotified($output, 'devtahoro@gmail.com');

        return 0;
    }


    protected function notify(Scrapper $scrapper, ScrapStats $stats)
    {
        $mailer = $this->di->get('mailer');
        $mailer->send('devtahoro@gmail.com', sprintf('%s Scrapping Session Report', ucfirst($scrapper->getId())), '../mail/scrap.volt', [
            'scrapper' => $scrapper,
            'stats'    => $stats,
        ]);
    }

    protected function fixture(ScrappingTarget $target): string
    {
        $config = $this->di->get('config');
        $fixturesDir = $config->layout->fixturesDir;

        // Noop

        return $fixturesDir;
    }

    protected function persist(ScrappingTarget $target)
    {
        sleep(1);

        /** @var Ad $data */
        $data = $target->getData();

        $manager = new ScrapModelManager($data);
        $manager->setDI($this->di);
        $manager->setNotifyByEmail(false);

        $posterFound = false;

        foreach ($data->images as $media) {
            if ($media->poster) {
                $posterFound = true;
                break;
            }
        }

        if (! $posterFound && count($data->images)) {
            $data->images[0]->poster = true;
        }

        try {
            foreach ($data->images as $media) {
                if ($media->poster) {
                    $manager->setPosterId($media->id);
                }

                if ($this->existsMedia($media)) {
                    $manager->addMedia($media->id);

                    continue;
                }

                $mediaManager = new ModelManager($media);
                $mediaManager->setDI($this->di);
                $mediaManager->save();

                $manager->addMedia($media->id);

                sleep(1);
            }
        } catch (Throwable $t) {
            $target->addStatus(ScrappingTargetStatus::PERSIST_FAILED);

            throw new OutOfBoundsException(sprintf('Failed to persist images: %s', $t->getMessage()), $t->getCode(), $t);
        }

        try {
            foreach ($data->videos as $media) {
                if ($this->existsMedia($media)) {
                    $manager->addMedia($media->id);

                    continue;
                }

                $mediaManager = new ModelManager($media);
                $mediaManager->setDI($this->di);
                $mediaManager->save();

                $manager->addMedia($media->id, true);

                sleep(1);
            }
        } catch (Throwable $t) {
            $target->addStatus(ScrappingTargetStatus::PERSIST_FAILED);

            throw new OutOfBoundsException(sprintf('Failed to persist videos: %s', $t->getMessage()), $t->getCode(), $t);
        }

        try {
            $manager->save();

            $target->addStatus(ScrappingTargetStatus::PERSISTED);
        } catch (Throwable $t) {
            $target->addStatus(ScrappingTargetStatus::PERSIST_FAILED);

            throw new OutOfBoundsException(sprintf('Failed to persist ad: %s', $t->getMessage()), $t->getCode(), $t);
        }

        /** @var Resultset $result */
        $result = $this
            ->getEM()
            ->getRepository(Ad::class)
            ->disableCache()
            ->find($data->id);

        if (empty($result)) {
            throw new OutOfBoundsException('Cannot found persisted ad.');
        }

        return $result;
    }

    protected function publish(OutputInterface $output, Scrapper $scrapper, ScrappingTarget $target)
    {
        /** @var Ad $data */
        $ad = $target->getData();

        if (! $ad->isPublished()) {
            $log = sprintf('/tmp/.scraplog.%s.%s', $scrapper->getId(), $ad->id);
            $cmd = $this->getPhpCommand(sprintf('publish:ad -i %s > %s 2>&1', $ad->id, $log));

            $this->writeTargetCmd($output, $cmd);

            try {
                $process = new Process($cmd);
                $process->setTimeout(600);
                $process->run();

                if (! $process->isSuccessful()) {
                    throw new ProcessFailedException($process);
                }

                $target->addStatus(ScrappingTargetStatus::PUBLISHED);

                $this->writeOK($output);
                $this->writeNewLine($output);

            } catch (Throwable $t) {
                $target->addStatus(ScrappingTargetStatus::PUBLISH_FAILED);

                $this->writeKO($output);
                $this->writeNewLine($output);

                throw new OutOfRangeException($t->getMessage(), $t->getCode(), $t);
            } finally {
                unlink($log);
            }
        }
    }

    protected function debug(OutputInterface $output, ScrappingTarget $target, string $cachePath)
    {
        $data = $target->getData();

        if (empty($data)) {
            return;
        }

        $this->debugLine($output, ['URL', '/'. strval($data->hash), 12]);
        $this->debugLine($output, ['Cache', $cachePath, 8],
                                  ['Cat', strval($data->category), 4]);
        $this->debugLine($output, ['Title', strval($data->title), 12]);
        $this->debugLine($output, ['Phone', strval($data->phone), 3],
                                  ['Name', strval($data->name), 3],
                                  ['Age', strval($data->age), 3]);
        $this->debugLine($output, ['ZIP', strval($data->postalCode), 3],
                                  ['City', strval($data->city), 6]);
        $this->debugLine($output, ['Lat', sprintf('%0.3f', $data->lat), 3],
                                  ['Lng', sprintf('%0.3f', $data->lng), 3],
                                  ['Zone', strval($data->zone), 6]);
        $this->debugLine($output, ['Fee', strval($data->fee), 3],
                                  ['Cash', $data->cash ? 'T':'F', 3],
                                  ['Card', $data->card ? 'T':'F', 3],
                                  ['Layout', strval($data->layout), 3]);
        $this->debugLine($output, ['Images', $data->numImages, 3],
                                  ['Videos', $data->numVideos, 3]);

        foreach ($data->images as $offset => $media) {
            $this->debugFile($output, $offset, strval($media->path), strval($media->type), strval($media->size));
        }

        foreach ($data->videos as $offset => $media) {
            $this->debugFile($output, $offset, strval($media->path), strval($media->type), strval($media->size));
        }

        $output->writeln('');
    }

    protected function debugFile(OutputInterface $output, int $offset, string $file, string $type, string $size)
    {
        $this->debugPad($output);
        $output->write(sprintf('File %d: <info>%s</info> (%s, %s bytes)', $offset, basename($file), $type, $size));
        $this->writeNewLine($output);
    }

    protected function debugLine(OutputInterface $output, ...$cols)
    {
        $this->debugPad($output);

        foreach ($cols as $col) {
            $this->debugCol($output, ...$col);
        }

        $this->writeNewLine($output);
    }

    protected function debugPad(OutputInterface $output)
    {
        $output->write(str_repeat(' ', 7));
    }

    protected function debugCol(OutputInterface $output, string $key, string $val, int $size)
    {
        $maxLen = 10 * $size;
        $len = 2 + strlen($key) + strlen($val);

        if ($len > $maxLen) {
            $val = substr($val, 0, $maxLen - strlen($key) - 5) .'...';
        }

        $format = sprintf('%%-%ds  ', $maxLen);
        $string = sprintf('%s: <info>%s</info>', $key, $val);

        $output->write(sprintf($format, $string));
    }


    protected function find(ScrappingTarget $target)
    {
        $data = $target->getData();

        try {
            /** @var Resultset $result */
            $result = $this
                ->getEM()
                ->getRepository(Ad::class)
                ->disableCache()
                ->findOneByTitle($data->title);

            $exists = $result && $result->count();

            if ($exists) {
                $data->id = $result->id;
                $target->addStatus(ScrappingTargetStatus::PERSIST_ALREADY_EXIST);

                return $result;
            }
        } catch (Throwable $t) {
        }

        return null;
    }

    protected function existsMedia(Media $media): bool
    {
        try {
            /** @var Resultset $result */
            $result = $this
                ->getEM()
                ->getRepository(Media::class)
                ->disableCache()
                ->findOneByPath($media->path);

            $exists = null !== $result;

            if ($exists) {
                $media->id = $result->id;
            }

            return $exists;
        } catch (Throwable $t) {
            return false;
        }
    }

    private function createScrapper(string $source, bool $useCache = true, bool $download = true): Scrapper
    {
        switch ($source) {
            case 'forosx':
                return new ForosXScrapper($useCache, $download);
            case 'nloquo':
                return new NLoquoScrapper($useCache, $download);
            case 'pasion':
                return new PasionScrapper($useCache, $download);
            case 'slumi':
                return new SlumiScrapper($useCache, $download);
            case 'sustitutas':
                return new SustitutasScrapper($useCache, $download);
        }

        throw new RuntimeException(sprintf('Unknown scrap source %s', $source));
    }

    private function removeDir(string $dir)
    {
        if (is_dir($dir)) {
            $handle = opendir($dir);

            while ($file = readdir($handle)) {
                if ('.' === $file || '..' === $file) {
                    continue;
                }

                $path = $dir .'/'. $file;

                if (is_file($file)) {
                    unlink($path);
                } else {
                    $this->removeDir($path);
                }
            }

            closedir($handle);
            rmdir($dir);

        } else {
            unlink($dir);
        }
    }


    private function getEM(): EntityManager
    {
        /** @var EntityManager $em */
        $em = $this->di->get('em');
        $em->setUsePersistentDb(true);

        return $em;
    }

    private function formatPath(string $path): string
    {
        return str_replace(BASE_PATH .'/', '', $path);
    }

    private function formatUrl(string $base, string $url): string
    {
        $url = str_replace($base, '', $url);

        if (strlen($url) > 64) {
            $url = substr($url, 0, 61) .'...';
        }

        return $url;
    }

    private function formatConfirmation(string $message, bool $default = false): string
    {
        return $this->formatQuestion($message, 'y/n', $default ? 'y' : 'n');
    }

    private function formatQuestion(string $message, string $hint = null, string $default = null): string
    {
        if (null !== $hint) {
            $message .= sprintf(' (%s)', $hint);
        }

        if (null !== $default) {
            $message .= sprintf(' [%s]', $default);
        }

        return sprintf('<question> %s </question> > ', $message);
    }


    private function writeTargetCmd(OutputInterface $output, string $cmd)
    {
        $output->write(sprintf('   <info>✔</info> Running <comment>%s</comment> ...', $cmd));
    }

    private function writeOK(OutputInterface $output)
    {
        $output->write(' [<info>OK</info>]');
    }

    private function writeKO(OutputInterface $output)
    {
        $output->write(' [<error>KO</error>]');
    }

    private function writeNewLine(OutputInterface $output)
    {
        $output->write("\n");
    }


    private function writeTargetHeader(OutputInterface $output, Scrapper $scrapper, ScrappingTarget $target, int $offset, int $total)
    {
        $output->writeln(sprintf(' - Scrapping target %03d/%03d (%02d %%) <info>%s</info> ...',
            $offset,
            $total,
            round(100 * $offset / $total),
            $this->formatUrl($scrapper->getBaseUrl(), $target->getUrl())
        ));
    }

    private function writeTargetPhoneOcr(OutputInterface $output, ScrappingTarget $target)
    {
        $output->writeln(sprintf('   <info>✔</info> Phone extracted via OCR: %s', $target->getData()->phone));
    }

    private function writeTargetDownload(OutputInterface $output, ScrappingDownload $download)
    {
        if (ScrappingTargetStatus::PERSISTED === $download->getStatus()) {
            $output->writeln(sprintf('   <info>✔</info> Download: %s', $download->getSrc()));
        } else {
            $output->writeln(sprintf('   <error>✘ Download failed</error>: %s', $download->getSrc()));
        }
    }

    private function writeTargetError(OutputInterface $output, ScrappingException $e)
    {
        $output->writeln(sprintf('   <error>✘ Scrapping Error</error>: %s', $e->getMessage()));
    }

    private function writeTargetNotFound(OutputInterface $output, NotFoundException $e)
    {
        $output->writeln(sprintf('   <error>✘ Not Found</error>: %s', $e->getMessage()));
    }

    private function writeTargetNotScrapped(OutputInterface $output)
    {
        $output->writeln(sprintf('   <error>✘ Skipped: Could not be scrapped</error> (incomplete, unparseable data)'));
    }

    private function writeTargetNotWorthProcessing(OutputInterface $output)
    {
        $output->writeln(sprintf('   <error>✘ Skipped: Not worth procesing</error>'));
    }

    private function writeTargetAlreadyExists(OutputInterface $output, ScrappingTarget $target)
    {
        /** @var Ad $ad */
        $ad = $target->getData();

        $output->writeln(sprintf('   <error>✘ Skipped: Already exists</error> (id = %d) [%s]', $ad->id, $ad->hash));
    }

    private function writeTargetHasNoImages(OutputInterface $output, ScrappingTarget $target)
    {
        /** @var Ad $ad */
        $ad = $target->getData();

        $output->writeln(sprintf('   <error>✘ Skipped: No images</error>'));
    }

    private function writeTargetFixtureFailed(OutputInterface $output, ScrapStats $stats, LogicException $e)
    {
        $output->writeln(sprintf('   <error>✘ Failed to generate fixture (%d)</error>: %s',
            $stats->getFixtureFailed(),
            $e->getMessage()
        ));
    }

    private function writeTargetPersistFailed(OutputInterface $output, ScrapStats $stats, OutOfBoundsException $e)
    {
        $output->writeln(sprintf('   <error>✘ Failed to perstist (%d/%d)</error>: %s',
            $stats->getPersistFailed(),
            self::MAX_ALLOWED_PERSIST_FAILS,
            $e->getMessage()
        ));
    }

    private function writeTargetPublishFailed(OutputInterface $output, ScrapStats $stats, OutOfRangeException $e)
    {
        $output->writeln(sprintf('   <error>✘ Failed to publish (%d/%d)</error>: %s',
            $stats->getPublicationFailed(),
            self::MAX_ALLOWED_PUBLISH_FAILS,
            $e->getMessage()
        ));
    }

    private function writeTargetFixtureSuccess(OutputInterface $output)
    {
        $output->writeln('   <info>✔</info> Fixture successfully created');
    }

    private function writeTargetPersistSuccess(OutputInterface $output, ScrappingTarget $target)
    {
        /** @var Ad $ad */
        $ad = $target->getData();

        $output->writeln(sprintf('   <info>✔</info> Persisted successfully (id = %d)', $ad->id));
    }

    private function writeTargetPublishSuccess(OutputInterface $output, ScrappingTarget $target)
    {
        /** @var Ad $ad */
        $ad = $target->getData();

        $output->writeln(sprintf('   <info>✔</info> Published successfully (%s)', $ad->hash));
    }

    private function writeTargetDone(OutputInterface $output, ScrapStats $stats)
    {
        $output->writeln(sprintf('   <info>✔</info> Done in %0.3fs', $stats->tac()));
    }


    private function writeCacheCleaned(OutputInterface $output, string $dir)
    {
        $output->writeln(sprintf('   <info>✔</info> Cache dir cleaned (%s)', $dir));
    }

    private function writeNotified(OutputInterface $output, string $recipients)
    {
        $output->writeln(sprintf('   <info>✔</info> Report sent to %s', $recipients));
    }


    private function writeTooMuchPersistenceErrors(OutputInterface $output, ScrapStats $stats)
    {
        $output->writeln(sprintf('   <error>✘ Too much persistence errors (%d/%d)</error>. Abort', $stats->getPersistFailed(), self::MAX_ALLOWED_PERSIST_FAILS));
    }

    private function writeTooMuchPublicationErrors(OutputInterface $output, ScrapStats $stats)
    {
        $output->writeln(sprintf('   <error>✘ Too much publication errors (%d/%d)</error>. Abort', $stats->getPublicationFailed(), self::MAX_ALLOWED_PUBLISH_FAILS));
    }

    private function writeSoFarSoGood(OutputInterface $output, ScrapStats $stats)
    {
        $output->writeln(sprintf('   <info>✘ So far, so good  (%d/%d)</info>. Abort', $stats->getPublished(), self::MAX_ALLOWED_SUCCESS));
    }

}
