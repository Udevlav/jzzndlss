<?php

namespace App\Command\Check;

use App\Script\AppRequirements;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckEnvCommand extends Command
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('check:env')
            ->setDescription('Checks the app environment.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Checking app environment ...');

        $appRequirements = new AppRequirements(BASE_PATH);

        $majorProblems = $appRequirements->getFailedRequirements();
        $minorProblems = $appRequirements->getFailedRecommendations();

        $hasMajorProblems = (bool) count($majorProblems);
        $hasMinorProblems = (bool) count($minorProblems);

        if ($hasMajorProblems) {
            $output->writeln(sprintf('Found %d major problems', count($majorProblems)));

            foreach ($majorProblems as /** @var \Symfony\Requirements\Requirement */ $problem) {
                $output->writeln(sprintf(' - <info>%s</info>: %s',
                    $problem->getTestMessage(),
                    $problem->getHelpText()
                ));
            }
        }

        if ($hasMinorProblems) {
            $output->writeln(sprintf('Found %d minor problems', count($minorProblems)));

            foreach ($minorProblems as /** @var \Symfony\Requirements\Requirement */ $problem) {
                $output->writeln(sprintf(' - <info>%s</info>: %s',
                    $problem->getTestMessage(),
                    $problem->getHelpText()
                ));
            }
        }

        if ($appRequirements->hasPhpConfigIssue()) {
            if ($appRequirements->getPhpIniPath()){
                $output->writeln(sprintf('Changes to the <info>php.ini</info> file must be done in <comment>%s</comment>', $appRequirements->getPhpIniPath()));
            } else {
                $output->writeln('To change settings, create a "<comment>php.ini</comment>"');
            }
        }

        if (! $hasMajorProblems && ! $hasMinorProblems) {
            $output->writeln('All checks passed successfully.');
        }
    }

}