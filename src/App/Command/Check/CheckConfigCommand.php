<?php

namespace App\Command\Check;

use App\Command\InjectionAwareCommand;
use App\Script\AppConfigRequirements;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckConfigCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('check:config')
            ->setDescription('Checks the app config.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Checking app config ...');

        $appConfigRequirements = new AppConfigRequirements($this->di->get('config'));

        $majorProblems = $appConfigRequirements->getFailedRequirements();
        $minorProblems = $appConfigRequirements->getFailedRecommendations();

        $hasMajorProblems = (bool) count($majorProblems);
        $hasMinorProblems = (bool) count($minorProblems);

        if ($hasMajorProblems) {
            $output->writeln(sprintf('Found %d major problems', count($majorProblems)));

            foreach ($majorProblems as /** @var \Symfony\Requirements\Requirement */ $problem) {
                $output->writeln(sprintf(' - <info>%s</info>: %s',
                    $problem->getTestMessage(),
                    $problem->getHelpText()
                ));
            }
        }

        if ($hasMinorProblems) {
            $output->writeln(sprintf('Found %d minor problems', count($minorProblems)));

            foreach ($minorProblems as /** @var \Symfony\Requirements\Requirement */ $problem) {
                $output->writeln(sprintf(' - <info>%s</info>: %s',
                    $problem->getTestMessage(),
                    $problem->getHelpText()
                ));
            }
        }

        if ($appConfigRequirements->hasPhpConfigIssue()) {
            if ($appConfigRequirements->getPhpIniPath()){
                $output->writeln(sprintf('Changes to the <info>php.ini</info> file must be done in <comment>%s</comment>', $appConfigRequirements->getPhpIniPath()));
            } else {
                $output->writeln('To change settings, create a "<comment>php.ini</comment>"');
            }
        }

        if (! $hasMajorProblems && ! $hasMinorProblems) {
            $output->writeln('<info>All checks passed successfully.</info>');
        }
    }

}