<?php

namespace App\Command\Check;

use App\Command\InjectionAwareCommand;
use App\Config\Config;
use App\Sms\Sms;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckSmsCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('check:sms')
            ->setDescription('Checks the SMS Service API.')
            ->addOption('phone', 'p', InputOption::VALUE_REQUIRED, 'The phone number to test with')
            ->addOption('dump', 'd', InputOption::VALUE_NONE, 'Whether to dump the mailer configuration');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Checking SMS Service API ...');

        if ($input->getOption('dump')) {
            $this->dumpConfig($output);
        }

        try {
            $credits = $this->checkCredits();
            $output->writeln(sprintf('<info>Successfully checked credits: %f credits remaining</info>.', $credits));

            if ($credits < 0) {
                $output->writeln('<error>Skip further tests: no credits left</error>.');

                return 0;
            }

        } catch (\Throwable $t) {
            $output->writeln(sprintf('<error>Failed to send plain message</error>: %s.', $t->getMessage()));
        }

        $phone = $input->getOption('phone');

        if (empty($phone)) {
            throw new \InvalidArgumentException('The phone is required');
        }

        try {
            $this->checkPlainMessage($phone);
            $output->writeln('<info>Successfully sent plain message</info>.');

        } catch (\Throwable $t) {
            $output->writeln(sprintf('<error>Failed to send plain message</error>: %s.', $t->getMessage()));
        }

        try {
            $this->checkViewMessage($phone);
            $output->writeln('<info>Successfully sent a message created from a view</info>.');

        } catch (\Throwable $t) {
            $output->writeln(sprintf('<error>Failed to send a message rendered from a view</error>: %s.', $t->getMessage()));
        }

        $output->writeln('Done. All checks passed.');
    }


    protected function getConfig(): Config
    {
        return $this->getDI()->get('config');
    }

    protected function getSms(): Sms
    {
        return $this->getDI()->get('sms');
    }

    protected function dumpConfig(OutputInterface $output)
    {
        $config = $this->getConfig();

        $output->writeln([
            sprintf('%-10s  %s', str_repeat('-', 10), str_repeat('-', 20)),
            sprintf('%-10s  <comment>%s</comment>', 'provider', $config->sms->provider),
            sprintf('%-10s  <comment>%s</comment>', 'email',    $config->sms->email),
            sprintf('%-10s  <comment>%s</comment>', 'password', $config->sms->password),
            sprintf('%-10s  <comment>%s</comment>', 'version',  $config->sms->version),
            sprintf('%-10s  <comment>%s</comment>', 'timeout',  $config->sms->timeout),
            sprintf('%-10s  <comment>%d</comment>', 'https',    $config->sms->https),
            sprintf('%-10s  <comment>%d</comment>', 'lenient',  $config->sms->lenient),
            sprintf('%-10s  <comment>%d</comment>', 'placebo',  $config->sms->placebo),
            sprintf('%-10s  %s', str_repeat('-', 10), str_repeat('-', 20)),
        ]);
    }

    protected function checkCredits(): float
    {
        return $this->getSms()->checkCredits();
    }

    protected function checkPlainMessage(string $phone): int
    {
        return $this->getSms()->send($phone, 'Foo bar');
    }

    protected function checkViewMessage(string $phone): int
    {
        return $this->getSms()->send($phone, 'sms/test', [
            'foobar' => 'foobar',
        ]);
    }

}