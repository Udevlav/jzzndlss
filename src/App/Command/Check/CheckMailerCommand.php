<?php

namespace App\Command\Check;

use App\Command\InjectionAwareCommand;

use App\Config\Config;
use App\Mailer\Mailer;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckMailerCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('check:mailer')
            ->setDescription('Checks the Mailer service.')
            ->addOption('dump', 'd', InputOption::VALUE_NONE, 'Whether to dump the mailer configuration')
            ->addOption('recipient', 'r', InputOption::VALUE_OPTIONAL, 'The target recipient');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $recipient = $input->getOption('recipient');

        if (null === $recipient) {
            $recipient = $this->di->get('config')->mailbox->default->from;
        }

        $output->writeln('Checking Mailer service...');

        if ($input->getOption('dump')) {
            $this->dumpConfig($output);
        }

        try {
            $this->checkPlainMessage($recipient);
            $output->writeln(sprintf('Successfully sent plain message to <info>%s</info>.', $recipient));

        } catch (\Throwable $t) {
            $output->writeln(sprintf('<error>Failed to send plain message</error>: %s.', $t->getMessage()));

            if ($input->getOption('verbose')) {
                $output->writeln($t->getTraceAsString());
            }
        }

        try {
            $this->checkViewMessage($recipient);
            $output->writeln(sprintf('Successfully sent a message created from a view to <info>%s</info>.', $recipient));

        } catch (\Throwable $t) {
            $output->writeln(sprintf('<error>Failed to send a message rendered from a view</error>: %s.', $t->getMessage()));

            if ($input->getOption('verbose')) {
                $output->writeln($t->getTraceAsString());
            }
        }

        $output->writeln('Done. All checks passed.');
    }


    protected function getConfig(): Config
    {
        return $this->getDI()->get('config');
    }

    protected function getMailer(): Mailer
    {
        return $this->getDI()->get('mailer');
    }

    protected function dumpConfig(OutputInterface $output)
    {
        $config = $this->getConfig();

        $output->writeln([
            '',
            sprintf(' ---  %-10s  %s', str_repeat('-', 10), str_repeat('-', 40)),
            sprintf('      %-10s  %s', 'option', 'value'),
            sprintf(' ---  %-10s  %s', str_repeat('-', 10), str_repeat('-', 40)),
            sprintf('      %-10s  <comment>%s</comment>', 'driver',     $config->mailer->driver),
            sprintf('      %-10s  <comment>%s</comment>', 'host',       $config->mailer->host),
            sprintf('      %-10s  <comment>%s</comment>', 'port',       $config->mailer->port),
            sprintf('      %-10s  <comment>%s</comment>', 'encryption', $config->mailer->encryption),
            sprintf('      %-10s  <comment>%s</comment>', 'username',   $config->mailer->username),
            sprintf('      %-10s  <comment>%s</comment>', 'password',   $config->mailer->password),
            sprintf('      %-10s  <comment>%s</comment>', 'from',       json_encode($config->mailer->from)),
            sprintf('      %-10s  <comment>%s</comment>', 'cc',         json_encode($config->mailer->cc)),
            sprintf('      %-10s  <comment>%s</comment>', 'bcc',        json_encode($config->mailer->bcc)),
            sprintf(' ---  %-10s  %s', str_repeat('-', 10), str_repeat('-', 40)),
            '',
        ]);
    }

    protected function checkPlainMessage(string $recipient)
    {
        $this->getMailer()->send($recipient, 'Test plain', 'Foo bar');
    }

    protected function checkViewMessage(string $recipient)
    {
        $this->getMailer()->send($recipient, 'Test view', '../mail/test.volt', [
            'foobar' => 'foobar',
        ]);
    }

}