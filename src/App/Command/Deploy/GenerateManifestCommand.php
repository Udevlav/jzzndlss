<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;
use Throwable;

class GenerateManifestCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:manifest')
            ->setDescription('Generates and deploys the app manifest.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getDI()->get('config');

        $path = sprintf('%smanifest.json', $config->layout->publicDir);
        $uri = sprintf('%s://%s%s/',
            $config->server->protocol,
            $config->server->authority,
            $config->server->port ? ':'. $config->server->port : ''
        );

        $output->writeln('Generating app manifest ...');

        $manifest = new \stdClass();
        $manifest->manifest_version = '2';
        $manifest->name = $config->app->name;
        $manifest->short_name = $config->app->name;
        $manifest->description = $config->app->name;
        $manifest->version = $config->app->version;
        $manifest->default_locale = $config->app->locale;
        $manifest->lang = $config->app->locale;
        $manifest->dir = $config->app->dir;
        $manifest->display = $config->app->display;
        $manifest->theme_color = $config->app->themeColor;
        $manifest->background_color = $config->app->bgColor;
        $manifest->scope = '/';
        $manifest->start_url = '/';
        $manifest->app = new \stdClass();
        $manifest->app->urls = [$uri];
        $manifest->app->launch = new \stdClass();
        $manifest->app->launch->web_url = $uri;
        $manifest->prefer_related_applications = false;
        $manifest->icons = $this->generateIcons($uri);

        if (file_exists($path)) {
            @unlink($path);
        }

        try {
            $json = @json_encode($manifest, \JSON_PRETTY_PRINT);
        } catch (Throwable $t) {
            throw new RuntimeException(sprintf('Failed to encode manifest data to JSON: %s.', $e->getMessage()), $e->getCode(), $e);
        }

        if (false === @file_put_contents($path, $json)) {
            throw new RuntimeException(sprintf('Failed to write manifest file "%s".', $path));
        }

        $output->writeln(sprintf('Done. Manifest file is located at <info>%s</info>.', $path));
    }


    private function generateIcons(string $uri): array
    {
        $icons = [];

        // Std
        $icons[] = $this->generateIcon($uri, 16);
        $icons[] = $this->generateIcon($uri, 32);
        $icons[] = $this->generateIcon($uri, 96);

        // iOS
        $icons[] = $this->generateIcon($uri, 57, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 60, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 72, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 76, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 114, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 120, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 144, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 152, 'apple-icon');
        $icons[] = $this->generateIcon($uri, 180, 'apple-icon');

        // Android
        $icons[] = $this->generateIcon($uri, 36, 'android-icon');
        $icons[] = $this->generateIcon($uri, 48, 'android-icon');
        $icons[] = $this->generateIcon($uri, 72, 'android-icon');
        $icons[] = $this->generateIcon($uri, 96, 'android-icon');
        $icons[] = $this->generateIcon($uri, 144, 'android-icon');
        $icons[] = $this->generateIcon($uri, 192, 'android-icon');

        return $icons;
    }

    private function generateIcon(string $uri, int $size, string $prefix = 'favicon'): \stdClass
    {
        $icon = new \stdClass();
        $icon->type = 'image/png';
        $icon->src = sprintf('%s/asset/favicon/%s-%dx%d.png', $uri, $prefix, $size, $size);
        $icon->sizes = sprintf('%dx%d', $size, $size);
        $icon->density = $this->getIconDensity($size);

        return $icon;
    }

    private function getIconDensity(int $size): float
    {
        if ($size < 32) {
            return 0.5;
        }

        if ($size < 96) {
            return 0.75;
        }

        if ($size < 128) {
            return 1;
        }

        if ($size < 256) {
            return 1.5;
        }

        return 1.75;
    }

}