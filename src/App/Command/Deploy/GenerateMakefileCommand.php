<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateMakefileCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:makefile')
            ->setDescription('Generates and deploys the app Makefile.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->di->get('config');
        $templating = $this->di->get('templating');

        $template = '../deploy/make/Makefile.volt';
        $path = sprintf('%sMakefile', $config->layout->baseDir);

        $output->writeln(sprintf('Generating app Makefile from template <info>%s</info> ...', $template));

        if (file_exists($path)) {
            @unlink($path);
        }

        $data = $templating->render($template, [
            'config' => $config,
        ]);

        if (false === @file_put_contents($path, $data)) {
            throw new \RuntimeException(sprintf('Failed to write app Makefile "%s".', $path));
        }

        $output->writeln(sprintf('Done. App Makefile is located at <info>%s</info>.', $path));
    }

}