<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;
use App\Config\Config;
use App\Model\Province;
use App\Sitemap\Sitemap;
use App\Sitemap\SitemapBuilder;
use App\View\Templating;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;

class GenerateSitemapCommand extends InjectionAwareCommand
{

    const DEFAULT_TEMPLATE = '../deploy/sitemap/sitemap';


    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Templating
     */
    protected $templating;


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:sitemap')
            ->setDescription('Generates and deploys the app sitemap file.')
            ->addOption('template', 't', InputOption::VALUE_OPTIONAL, sprintf('The template (defaults to "%s")', self::DEFAULT_TEMPLATE))
            ->addOption('hierarchical', 'c', InputOption::VALUE_NONE, 'Whether to generate a hierarchical sitemap');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->config = $this->di->get('config');
        $this->templating = $this->di->get('templating');

        if (null === $input->getOption('template')) {
            $input->setOption('template', self::DEFAULT_TEMPLATE);
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating app sitemap. See <info>https://www.sitemaps.org/</info> for details ...');

        $sitemap = $this->generateSitemap($input, $output);

        $path = sprintf('%ssitemap.xml', $this->config->layout->publicDir);
        $this->deploySitemap($input, $output, $path, $sitemap);

        if ($sitemap->isIndex()) {
            foreach ($sitemap->getSitemapIndex()->getSitemaps() as $child) {
                $path = sprintf('%s%s', $this->config->layout->publicDir, $this->path($child->getLoc()));
                $this->deploySitemap($input, $output, $path, $child);
            }
        }

        $output->writeln('<info>Done</info>.');
    }


    private function generateSitemap(InputInterface $input, OutputInterface $output): Sitemap
    {
        $builder = new SitemapBuilder(true);

        // Generate categories
        $output->writeln(' > Adding sitemap by category ...');
        $builder
            ->addSitemap($this->loc('sitemap_categories.xml'), date('c'))
                ->addUrl($this->loc(''), null, null, 1.0)
                ->addUrl($this->loc('chica'), null, null, 0.8)
                ->addUrl($this->loc('chico'), null, null, 0.8)
                ->addUrl($this->loc('travesti'), null, null, 0.8)
                ->addUrl($this->loc('castings-y-plazas'), null, null, 0.8)
                ->addUrl($this->loc('alquiler-habitaciones'), null, null, 0.8)
                ->addUrl($this->loc('fotografos-profesionales'), null, null, 0.8)
            ->endSitemap();

        // Generate provinces
        $output->writeln(' > Adding sitemap by province ...');

        $builder
            ->addSitemap($this->loc('sitemap_provinces.xml'), date('c'))
                ->addUrl($this->loc(''), null, null, 1.0);

        foreach (array_values(Province::$slugs) as $slug) {
            $builder->addUrl($this->loc($slug), null, null, 0.8);
        }

        $builder->endSitemap();

        // Generate general

        return $builder->build();
    }

    private function deploySitemap(InputInterface $input, OutputInterface $output, string $path, Sitemap $sitemap)
    {
        if (file_exists($path)) {
            @unlink($path);
        }

        $data = $this->templating->render($input->getOption('template'), [
            'config'  => $this->config,
            'sitemap' => $sitemap,
        ]);

        if (false === @file_put_contents($path, $data)) {
            throw new RuntimeException(sprintf('Failed to write sitemap file "%s".', $path));
        }

        if ($sitemap->isIndex()) {
            $output->writeln(sprintf(' > Deployed main sitemap at <comment>%s</comment>.', $path));
        } else {
            $output->writeln(sprintf(' > Deployed secondary sitemap at <comment>%s</comment>.', $path));
        }
    }

    private function path(string $loc): string
    {
        return str_replace($this->config->baseUrl(), '', $loc);
    }

    private function loc(string $path): string
    {
        return sprintf('%s/%s', rtrim('/', $this->config->baseUrl()), $path);
    }

}