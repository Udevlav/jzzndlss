<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;

class GenerateVhostCommand extends InjectionAwareCommand
{

    /**
     * @var string
     */
    const TEMPLATE_APACHE = '../deploy/vhost/vhost.volt';

    /**
     * @var string
     */
    const TEMPLATE_HOSTS = '../deploy/vhost/hosts.volt';

    /**
     * @var string
     */
    const TEMPLATE_NGINX = '../deploy/vhost/assets.volt';

    /**
     * @var string
     */
    const SERVER_APACHE = 'apache';

    /**
     * @var string
     */
    const SERVER_NGINX = 'nginx';

    protected $config;
    protected $templating;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:vhost')
            ->setDescription('Generates and deploys the app virtual host.')
            ->addOption('server', 's', InputOption::VALUE_OPTIONAL, 'Target server')
            ->addOption('https', null, InputOption::VALUE_OPTIONAL, 'Whether to include HTTPS config');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->config = $this->di->get('config');
        $this->templating = $this->di->get('templating');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating app virtual host ...');
        $output->writeln('');

        $path = sprintf('%sdeploy/%s.conf',
            $this->config->layout->varDir,
            $this->config->server->authority
        );
        $this->deployVhost($input, $output, self::TEMPLATE_APACHE, $path);
        $output->writeln(sprintf(' > www virtual host file is located at <info>%s</info>.', $path));

        if ($this->config->cdn->enabled) {
            $path = sprintf('%sdeploy/%s.conf',
                $this->config->layout->varDir,
                $this->config->cdn->authority
            );
            $this->deployVhost($input, $output, self::TEMPLATE_NGINX, $path);
            $output->writeln(sprintf(' > cdn virtual host file is located at <info>%s</info>.', $path));
        }

        $output->writeln([
            '',
            'You should copy this file to the appropriate location and reload ',
            'the server. Be sure to define the appropriate settings in yout ',
            'hosts file (/etc/hosts):',
            '',
            sprintf('<info>%s</info>', $this->templating->render(self::TEMPLATE_HOSTS, [
                'config' => $this->config,
            ])),
            '',
        ]);
    }


    private function deployVhost(InputInterface $input, OutputInterface $output, string $template, string $path)
    {
        if (file_exists($path)) {
            @unlink($path);
        }

        $data = $this->templating->render($template, [
            'config' => $this->config,
            'https' => (bool) $input->getOption('https'),
        ]);

        if (false === @file_put_contents($path, $data)) {
            throw new RuntimeException(sprintf('<error>Failed to write virtual host file "%s"</error>.', $path));
        }

        if ($input->getOption('verbose')) {
            $output->writeln($data);
        }
    }
}