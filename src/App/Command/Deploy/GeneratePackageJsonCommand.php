<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePackageJsonCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:package')
            ->setDescription('Generates and deploys the app package.json file.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating app package.json ...');

        $config = $this->di->get('config');
        $templating = $this->di->get('templating');

        $template = 'deploy/package/package.json';
        $path = sprintf('%sjs/%s', $config->layout->assetDir, $template);

        if (file_exists($path)) {
            @unlink($path);
        }

        $data = $templating->render($template, [
            'config' => $config,
        ]);

        if (false === @file_put_contents($path, $data)) {
            throw new \RuntimeException(sprintf('Failed to write package.json file "%s".', $path));
        }

        $output->writeln(sprintf('Done. package.json file is located at <info>%s</info>.', $path));
    }

}