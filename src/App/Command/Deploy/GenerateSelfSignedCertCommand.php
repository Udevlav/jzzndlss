<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GenerateSelfSignedCertCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:selfsignedcert')
            ->addOption('keyname', 'k', InputOption::VALUE_OPTIONAL, 'The private key name.')
            ->addOption('certname', 'c', InputOption::VALUE_OPTIONAL, 'The certificate name.')
            ->addOption('passphrase', 'p', InputOption::VALUE_OPTIONAL, 'The certificate passphrase.', false)
            ->setDescription('Generates a self-signed certificate.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $cmd = $this->buildCommand($input);

        $output->writeln('Generating SSL self-signed certificate ...');
        $output->writeln(sprintf('Running <info>%s</info>', $cmd));

        $process = new Process($cmd);
        $process->setTimeout(600);
        $process->run();

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output->writeln($process->getOutput());
        $output->writeln(sprintf('Done. Certificate files are located in <info>%s</info>', $this->getCertDir()));

        return $process->getExitCode();
    }

    protected function buildCommand(InputInterface $input): string
    {
        $config = $this->di->get('config');

        $command = sprintf('openssl req -x509 -newkey rsa:4096 -keyout %s%s -out %s%s -days 365',
            $config->layout->certDir,
            $input->getOption('keyname') ?? $config->server->sslKey,
            $config->layout->certDir,
            $input->getOption('certname') ?? $config->server->sslCert
        );

        if (! $input->isInteractive() || ! $input->getOption('passphrase')) {
            $command .= ' -nodes';
        }

        $command .= sprintf(' -subj "/C=ES/ST=Catalonia/L=Barcelona/O=%s/OU=Org/CN=%s"',
            $config->app->name,
            $config->server->authority
        );

        return $command;
    }

    protected function getCertDir(): string
    {
        return $this->di->get('config')->layout->certDir;
    }

}