<?php

namespace App\Command\Deploy;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;

class GenerateRobotsCommand extends InjectionAwareCommand
{

    const DEFAULT_TEMPLATE = '../deploy/robots/robots.volt';


    protected $config;
    protected $templating;


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('deploy:robots')
            ->setDescription('Generates and deploys the app robots.txt file.')
            ->addOption('template', 't', InputOption::VALUE_OPTIONAL, sprintf('The template (defaults to "%s")', self::DEFAULT_TEMPLATE))
            ->addOption('disallow_assets', null, InputOption::VALUE_NONE, 'Whether to disallow assets');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->config = $this->di->get('config');
        $this->templating = $this->di->get('templating');

        if (null === $input->getOption('template')) {
            $input->setOption('template', self::DEFAULT_TEMPLATE);
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating app robots. See <info>http://www.robotstxt.org/</info> for details ...');

        $path = sprintf('%srobots.txt', $this->config->layout->publicDir);
        $this->deployRobots($input, $output, $path);

        $output->writeln(sprintf('Done. Robots file is located at <info>%s</info>.', $path));
    }


    private function deployRobots(InputInterface $input, OutputInterface $output, string $path)
    {
        if (file_exists($path)) {
            @unlink($path);
        }

        $data = $this->templating->render($input->getOption('template'), [
            'config'  => $this->config,
        ]);

        if (false === @file_put_contents($path, $data)) {
            throw new RuntimeException(sprintf('Failed to write robots file "%s".', $path));
        }

        if ($input->getOption('verbose')) {
            $output->writeln(sprintf('Deployed robots.txt at <comment>%s</comment>.', $path));
        }
    }

}