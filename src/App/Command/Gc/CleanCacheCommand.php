<?php

namespace App\Command\Gc;

use App\Command\InjectionAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use DirectoryIterator;

/**
 * Clears the cache.
 *
 * @package App\Command\Gc
 */
class CleanCacheCommand extends InjectionAwareCommand
{

    /**
     * @const string[]
     */
    const KEEP_FILE = [
        '.gitkeep',
    ];


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('gc:cache')
            ->setDescription('Cache garbage collection.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $style = new SymfonyStyle($input, $output);
        $style->writeln(sprintf('<info>Done. %d files deleted</info>', $this->clearCache($style)));

        return 0;
    }

    /**
     * @param SymfonyStyle $style
     *
     * @return int
     */
    protected function clearCache(SymfonyStyle $style): int
    {
        $iterator = new DirectoryIterator($this->getCacheDir());

        $count = 0;

        foreach ($iterator as $fileInfo) {
            if (! $fileInfo->isFile()) {
                // Skip dots, dirs, links, etc.
                continue;
            }

            $basename = $fileInfo->getBasename();

            if (in_array($basename, self::KEEP_FILE)) {
                // Skip files explicitly marked to be ignored
                continue;
            }

            if (unlink($fileInfo->getPathname())) {
                $count ++;
                $style->writeln(sprintf('<info>✔</info> deleted <comment>%s</comment>', $fileInfo->getBasename()));
            } else {
                $style->writeln(sprintf('<fg=red>✘</> failed to delete <comment>%s</comment>', $fileInfo->getBasename()));
            }
        }

        return $count;
    }

    /**
     * @return string
     */
    protected function getCacheDir(): string
    {
        return $this->getConfig()->layout->cacheDir;
    }

}
