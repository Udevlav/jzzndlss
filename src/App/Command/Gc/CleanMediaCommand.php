<?php

namespace App\Command\Gc;

use App\Model\Media;
use App\Repository\MediaRepository;

use Phalcon\Mvc\Model\Query\Builder;

use DateTime;

/**
 * Cleans temporary (unpublished or corrupt) media.
 *
 * @package App\Command\Gc
 */
class CleanMediaCommand extends GcCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('gc:media')
            ->setDescription('Media garbage collection.');
    }

    /**
     * @inheritdoc
     */
    protected function drop($model)
    {
        $this->deleteFile($model);

        parent::drop($model);
    }

    /**
     * @inheritdoc
     */
    protected function analyze($model): int
    {
        /** @var Media $model */
        if (null === $model->adId) {
            return self::CAUSE_EXPIRED;
        }

        return self::CAUSE_RECORD_IS_FINE;
    }

    /**
     * @inheritdoc
     */
    protected function getFindQuery(DateTime $date): Builder
    {
        $qb = $this->getMediaRepository()
            ->getQueryBuilder()
            ->from(['media' => Media::class])
            ->where('(media.adId IS NULL OR media.temp = 1)')
            ->andWhere(sprintf('media.createdAt <= "%s"', $date->format('Y-m-d H:i:s')));

        return $qb;
    }


    private function getMediaRepository(): MediaRepository
    {
        /** @var MediaRepository $repository */
        $repository = $this->getRepository(Media::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }

    private function deleteFile($model)
    {
        if (empty($model->path)) {
            return;
        }

        if (file_exists($model->path)) {
            @unlink($model->path);

        } else {
            $config = $this->getConfig();
            $dirs = [
                $config->layout->publicDir,
                $config->layout->mediaDir,
                $config->layout->uploadDir,
            ];

            foreach ($dirs as $dir) {
                $path = $dir . ltrim($model->path, '/');

                if (file_exists($path)) {
                    @unlink($path);
                }
            }
        }
    }

}