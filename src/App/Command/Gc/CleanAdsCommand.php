<?php

namespace App\Command\Gc;

use App\Model\Ad;
use App\Repository\AdRepository;

use Phalcon\Mvc\Model\Query\Builder;

use DateTime;

/**
 * Cleans temporary (unpublished or corrupt) ads.
 *
 * @package App\Command\Gc
 */
class CleanAdsCommand extends GcCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('gc:ad')
            ->setDescription('Ad garbage collection.');
    }

    /**
     * @inheritdoc
     */
    protected function getFindQuery(DateTime $date): Builder
    {
        $qb = $this->getAdRepository()
            ->getQueryBuilder()
            ->from(['ad' => Ad::class])
            ->where('ad.publishedAt IS NULL')
            ->andWhere(sprintf('ad.createdAt < "%s"', $date->format('Y-m-d H:i:s')));

        return $qb;
    }


    private function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }

}