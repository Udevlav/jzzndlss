<?php

namespace App\Command\Gc;

use App\Model\Payment;
use App\Repository\PaymentRepository;

use Phalcon\Mvc\Model\Query\Builder;

use DateTime;

/**
 * Cleans temporary (uncommited) payments.
 *
 * @package App\Command\Gc
 */
class CleanPaymentsCommand extends GcCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('gc:payments')
            ->setDescription('Payments garbage collection.');
    }

    /**
     * @inheritdoc
     */
    protected function getFindQuery(DateTime $date = null): Builder
    {
        return $this->getPaymentRepository()
            ->getQueryBuilder()
            ->from(['payment' => Payment::class])
            ->where('payment.committedAt IS NULL')
            ->andWhere(sprintf('payment.createdAt <= "%s"', $date->format('Y-m-d H:i:s')));
    }


    private function getPaymentRepository(): PaymentRepository
    {
        /** @var PaymentRepository $repository */
        $repository = $this->getRepository(Payment::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }
}