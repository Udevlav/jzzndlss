<?php

namespace App\Command\Gc;

use App\Command\InjectionAwareCommand;
use App\Config\Config;
use App\IO\PathUtil;
use App\Manager\ModelManager;
use App\Repository\EntityManager;
use App\Repository\Repository;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Query\Builder;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DateInterval;
use DateTime;
use Exception;
use InvalidArgumentException;

/**
 * Base class for garbage-collecting commands.
 *
 * @package App\Command\Gc
 */
abstract class GcCommand extends InjectionAwareCommand
{

    /**
     * @var int
     */
    const MAX_RESULTS = 1e3;

    /**
     * @var int
     */
    const MAX_IDLE_TIME = 24 * 3600;


    /**
     * Data is fine.
     *
     * @var int
     */
    const CAUSE_RECORD_IS_FINE = 0;

    /**
     * Data is incomplete and/or corrupt.
     *
     * @var int
     */
    const CAUSE_CORRUPT = 1;

    /**
     * Data was created long time ago, but has never been published, used
     * or consumed (in practice, the difference between the current date
     * time and the ad creation date time exceeds the max idle time.)
     *
     * @var int
     */
    const CAUSE_EXPIRED = 2;


    /**
     * @var Resultset
     */
    protected $collection;


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->addOption('placebo', 'p', InputOption::VALUE_OPTIONAL, 'Placebo', false)
            ->addOption('since', 's', InputOption::VALUE_OPTIONAL, 'An ISO8601 duration (e.g. "P1D"). Determines the records to process: date = now - since', 'P1D');
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $placebo = (bool) $input->getOption('placebo');
        $since = $input->getOption('since');

        // Determine date
        try {
            $interval = new DateInterval($since);
        } catch (Exception $e) {
            $interval = new DateInterval('P1D');
        }

        $date = new DateTime();
        $date->sub($interval);

        $output->writeln(sprintf('Garbage Collector for records created before <info>%s</info>', $date->format('Y-m-d H:i:s')));

        // Select records
        $records = $this->find($date);
        $count = $records->count();

        if (0 === $count) {
            $output->writeln('Nothing to do');

            return 0;
        }

        $output->writeln(sprintf('Found <info>%d</info> potential records to remove', $count));
        $output->writeln('');

        // Analyze each record; remove on positive
        $dropped = 0;

        foreach ($records as $model) {
            $result = $this->analyze($model);

            $this->writeResult($output, $model, $result);

            if (self::CAUSE_RECORD_IS_FINE !== $result) {
                if (! $placebo) {
                    $this->drop($model);
                }

                $dropped ++;
            }
        }

        $output->writeln('');
        $output->writeln(sprintf('<info>Done</info>. %d analyzed, %d deleted (%0.2f %%)', $count, $dropped, 100 * $dropped / $count));
    }

    /**
     * @param mixed $model
     *
     * @return void
     */
    protected function drop($model)
    {
        $manager = new ModelManager($model);
        $manager->delete();
    }

    /**
     * @param int $result
     *
     * @return string
     */
    protected function cause(int $result): string
    {
        if (self::CAUSE_CORRUPT === $result) {
            return 'CORRUPT';
        }

        if (self::CAUSE_EXPIRED === $result) {
            return 'EXPIRED';
        }

        return 'FINE';
    }

    /**
     * @return bool
     */
    protected function confirm(): bool
    {
        return false;
    }

    /**
     * @param DateTime $date
     *
     * @return Builder
     *
     * @throws Exception
     */
    abstract protected function getFindQuery(DateTime $date): Builder;

    /**
     * @param DateTime $date
     *
     * @return Resultset
     *
     * @throws Exception
     */
    protected function find(DateTime $date): Resultset
    {
        $qb = $this->getFindQuery($date);
        $qb->limit(self::MAX_RESULTS);

        return $qb->getQuery()->execute();
    }

    /**
     * @param mixed $model
     *
     * @return int
     *    one of the cause constants on this class, indicating whether the
     *    model was fine, or should be dropped (garbage collected), either
     *    because of corrupted/incomplete data or expired data
     */
    protected function analyze($model): int
    {
        return self::CAUSE_EXPIRED;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->di->getShared('em');
    }

    /**
     * @param string $fullyQualifiedClassName
     *
     * @return Repository
     *
     * @throws InvalidArgumentException
     */
    protected function getRepository(string $fullyQualifiedClassName): Repository
    {
        return $this->getEntityManager()->getRepository($fullyQualifiedClassName);
    }

    /**
     * @param OutputInterface $output
     * @param Model $model
     * @param int $result
     *
     * @return void
     */
    protected function writeResult(OutputInterface $output, Model $model, int $result)
    {
        if (isset($model->path)) {
            $path = PathUtil::format($model->path);
        } else {
            $path = sprintf('%s.%s', $model->getSource(), $model->id);
        }

        if (self::CAUSE_RECORD_IS_FINE === $result) {
            $type = 'info';
            $action = 'KEEP';
        } else {
            $type = 'comment';
            $action = 'DROP';
        }

        $output->writeln(sprintf('<%s>●</%s> %s %s (%s)',
            $type,
            $type,
            $action,
            $path,
            $this->cause($result)
        ));
    }

}
