<?php

namespace App\Command\Gc;

use App\Model\Ad;
use App\Repository\AdRepository;

use Phalcon\Mvc\Model\Query\Builder;

use DateTime;

/**
 * Cleans old stats.
 *
 * @package App\Command\Gc
 */
class CleanStatsCommand extends GcCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('gc:stats')
            ->setDescription('Ad stats garbage collection.');
    }

    /**
     * @inheritdoc
     */
    protected function getFindQuery(DateTime $date): Builder
    {
        // TODO
        $qb = $this->getAdRepository()
            ->getQueryBuilder()
            ->from(['ad' => Ad::class])
            ->where('ad.publishedAt IS NULL')
            ->andWhere(sprintf('ad.createdAt < "%s"', $date->format('Y-m-d H:i:s')));

        return $qb;
    }


    private function getAdRepository(): AdRepository
    {
        // TODO
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }

}