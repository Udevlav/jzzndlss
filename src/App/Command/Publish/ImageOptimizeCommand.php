<?php

namespace App\Command\Publish;

use App\Model\Media;

use ImageOptimizer\Optimizer;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;
use RuntimeException;

class ImageOptimizeCommand extends PublishMediaCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:optimize')
            ->setDescription('Optimizes images for streaming.');
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        if (! $media->isImage()) {
            throw new RuntimeException(sprintf('Only image resources can be optimized, "%s" given.', $media->getType()));
        }

        try  {
            $this->getOptimizer()->optimize($this->fs($media));
        } catch (\Exception $e) {
            // Noop
        }
    }

    private function getOptimizer(): Optimizer
    {
        return $this->di->get('optimizer');
    }

}