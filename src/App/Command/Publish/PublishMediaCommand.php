<?php

namespace App\Command\Publish;

use App\Command\InjectionAwareCommand;
use App\Config\Config;
use App\FFMpeg\FormatFactory;
use App\FFMpeg\QtFaststart;
use App\IO\PathGenerator;
use App\IO\PathUtil;
use App\Model\Media;
use App\Repository\EntityManager;
use App\Repository\MediaRepository;

use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\FFProbe\DataMapping\Stream;

use Imagine\Image\ImagineInterface;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;
use RuntimeException;

abstract class PublishMediaCommand extends InjectionAwareCommand
{

    /**
     * @var array
     */
    static protected $ext = [
        'image/gif'   => 'gif',
        'image/jpeg'  => 'jpg',
        'image/png'   => 'png',
        'image/webp'  => 'webp',
        'video/mp4'   => 'mp4',
    ];


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this->addOption('id', 'i', InputOption::VALUE_REQUIRED, 'The input media ID');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (null === ($mediaId = $input->getOption('id'))) {
            throw new RuntimeException('The input media identifier is required.');
        }

        if (null === ($media = $this->findMedia($mediaId))) {
            throw new RuntimeException(sprintf('Cannot found media resource %s.', $mediaId));
        }

        $this->process($input, $output, $media);
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return PathGenerator
     */
    protected function getPathGenerator(): PathGenerator
    {
        return $this->di->get('pathGenerator');
    }

    /**
     * @return ImagineInterface
     */
    protected function getImagine(): ImagineInterface
    {
        return $this->di->get('imagine');
    }

    /**
     * @return FFMpeg
     */
    protected function getFFMpeg(): FFMpeg
    {
        return $this->di->get('ffmpeg');
    }

    /**
     * @return FFProbe
     */
    protected function getFFProbe(): FFProbe
    {
        return $this->di->get('ffprobe');
    }

    /**
     * @return QtFaststart
     */
    protected function getQtFaststart(): QtFaststart
    {
        return $this->di->get('qtfaststart');
    }

    /**
     * @return FormatFactory
     */
    protected function getFormatFactory(): FormatFactory
    {
        return $this->di->get('formatFactory');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->di->get('em');
    }

    /**
     * @return MediaRepository
     */
    protected function getMediaRepository(): MediaRepository
    {
        /** @var MediaRepository $repository */
        $repository = $this->getEntityManager()
            ->getRepository(Media::class)
            ->hydrateRecords()
            ->disableCache();

        return $repository;
    }

    /**
     * @param string $id
     *
     * @return Media | null
     */
    protected function findMedia($id): Media
    {
        return $this->getMediaRepository()->find($id);
    }

    /**
     * @param Media $media
     * @param bool $checkExists
     *
     * @return string
     */
    protected function fs(Media $media, bool $checkExists = true): string
    {
        $path = $media->path;

        if ('/' === $path[0] && (! $checkExists || file_exists($path))) {
            return $path;
        }

        return $this->getConfig()->layout->publicDir . ltrim($path, '/');
    }

    /**
     * @param Media $media
     * @param int $mode
     *
     * @return string
     */
    protected function chmod(Media $media, int $mode = 0777): string
    {
        $path = $this->fs($media);

        if (false === @chmod($path, $mode)) {
            throw new InvalidArgumentException(sprintf('Failed to chmod %s to %s', PathUtil::format($path), $mode));
        }
    }

    /**
     * @param Stream $stream
     * @param string $property
     * @param mixed $default
     *
     * @return string
     */
    protected function prop(Stream $stream, string $property, $default = null)
    {
        if (null === $default) {
            $default = sprintf('unknown %s', $property);
        }

        return $stream->has($property) ? $stream->get($property) : $default;
    }


    protected function formatTargetPath(string $path): string
    {
        $config = $this->getConfig();

        return str_replace($config->layout->mediaDir, '$MEDIA_DIR/', $path);
    }

    protected function formatTempPath(string $path): string
    {
        $config = $this->getConfig();

        return str_replace($config->layout->uploadDir, '$UPLOAD_DIR/', $path);
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param Media $media
     *
     * @return void
     *
     * @throws RuntimeException
     */
    abstract protected function process(InputInterface $input, OutputInterface $output, Media $media);
}