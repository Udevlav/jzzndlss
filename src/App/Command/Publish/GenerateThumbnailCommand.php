<?php

namespace App\Command\Publish;

use App\IO\PathUtil;
use App\Model\Media;
use App\Repository\PersistenceException;

use Imagine\Image\Box;

use Imagine\Image\ImageInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;
use RuntimeException;
use Throwable;

class GenerateThumbnailCommand extends PublishMediaCommand
{

    /**
     * @var string
     */
    const DEFAULT_TYPE = 'image/jpeg';

    /**
     * @var int
     */
    const RES_LO = 36;

    /**
     * @var int
     */
    const RES_MID = 72;

    /**
     * @var int
     */
    const RES_HI = 96;


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:thumbnail')
            ->setDescription('Generates thumbnails from the input image resource.')
            ->addOption('type', 't', InputOption::VALUE_OPTIONAL, 'Output media type')
            ->addOption('size', 's', InputOption::VALUE_OPTIONAL, 'Thumbnail size')
            ->addOption('width', 'sw', InputOption::VALUE_OPTIONAL, 'Thumbnail width')
            ->addOption('height', 'sh', InputOption::VALUE_OPTIONAL, 'Thumbnail height')
            ->addOption('filter', 'f', InputOption::VALUE_OPTIONAL, 'Resampling filter (default lanczos -- ignored if adapter uses GD extension)')
            ->addOption('xres', 'x', InputOption::VALUE_OPTIONAL, 'X-axis resolution, either a number or one of "lo", "mid", "hi", "auto"  (default 72 ppi)')
            ->addOption('yres', 'y', InputOption::VALUE_OPTIONAL, 'Y-axis resolution, either a number or one of "lo", "mid", "hi", "auto" (default 72 ppi)')
            ->addOption('units', 'u', InputOption::VALUE_OPTIONAL, 'Resolution units (default ppi)')
            ->addOption('compression', 'c', InputOption::VALUE_OPTIONAL, 'PNG compression (0 - 9; default 9)')
            ->addOption('quality', null, InputOption::VALUE_OPTIONAL, 'JPEG quality (0 - 100; default 75)');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        if (null === $input->getOption('size')) {
            if (null === $input->getOption('width') ||
                null === $input->getOption('height')) {
                throw new InvalidArgumentException('Either the size option or the width and height options should be specified.');
            }
        }

        if (null === $input->getOption('filter')) {
            // GD only supports ImageInterface::FILTER_UNDEFINED filter
            if (strtolower($this->getConfig()->media->adapter) === 'gd') {
                $input->setOption('filter', ImageInterface::FILTER_UNDEFINED);
            } else {
                $input->setOption('filter', ImageInterface::FILTER_LANCZOS);
            }
        }

        if (null === $input->getOption('xres')) {
            $input->setOption('xres', self::RES_MID);
        }

        if (null === $input->getOption('yres')) {
            $input->setOption('yres', self::RES_MID);
        }

        if (null === $input->getOption('units')) {
            $input->setOption('units', ImageInterface::RESOLUTION_PIXELSPERINCH);
        }

        if (null === $input->getOption('compression')) {
            $input->setOption('compression', 9);
        }

        if (null === $input->getOption('quality')) {
            $input->setOption('quality', 75);
        }
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        if (! $media->isImage()) {
            throw new RuntimeException(sprintf('<error>Only image resources can be processed, "%s" given</error>.', $media->type));
        }

        $path = $this->fs($media);

        if (! is_readable($path)) {
            throw new RuntimeException(sprintf('<error>Cannot find "%s"</error>.', $path));
        }

        $image = $this->getImagine()->open($path);

        list ($width, $height, $outputSize) = $this->computeSize($input, $image);

        if (in_array($outputSize, ['lg', 'xl']) && $width > $image->getSize()->getWidth()) {
            return 0;
        }

        $outputType    = $this->getOutputType($input);
        $outputFlags   = $this->getOutputFlags($input);
        $outputOptions = $this->getOutputOptions($input, $width, $outputType);
        $outputExt     = $this->getOutputExtension($outputType);

        $thumbnail = $this->getThumbnail($media, $width, $height, $outputSize, $outputType, $outputExt);

        $output->writeln(sprintf('│  └─ <info>●</info> THUMBNAIL %s %d:%d <comment>%s</comment>',
            strtoupper($thumbnail->version),
            $thumbnail->width,
            $thumbnail->height,
            PathUtil::format($this->formatTempPath($thumbnail->path))
        ));

        try {
            $image
                ->resize(new Box($width, $height), $outputFlags)
                ->save($this->fs($thumbnail, false), $outputOptions);

        } catch (Throwable $t) {
            throw new RuntimeException(sprintf('<error>Failed to generate thumbnail %s at %s</error>: %s.', $outputSize, PathUtil::format($thumbnail->path), $t->getMessage()), $t->getCode(), $t);
        }

        $this->setThumbnailMeta($thumbnail);

        if (false === $thumbnail->save()) {
            throw PersistenceException::saving($thumbnail);
        }
    }


    private function getOutputOptions(InputInterface $input, int $width, string $outputType): array
    {
        $options = [
            'resampling-filter' => $input->getOption('filter'),
            'resolution-units' => $input->getOption('units'),
            'resolution-x' => $this->getResolution($input, 'xres', $width),
            'resolution-y' => $this->getResolution($input, 'yres', $width),
        ];

        if ('image/jpeg' === $outputType) {
            $options['jpeg_quality'] = $input->getOption('quality');
        }

        if ('image/png' === $outputType) {
            $options['png_compression'] = $input->getOption('compression');
        }

        return $options;
    }

    private function getOutputFlags(InputInterface $input)
    {
        return $input->getOption('filter');
    }

    private function getOutputExtension(string $type): string
    {
        if (! isset(static::$ext[$type])) {
            throw new InvalidArgumentException(sprintf('Unsupported output frame type "%s".', $type));
        }

        return static::$ext[$type];
    }

    private function getOutputType(InputInterface $input): string
    {
        if (null === ($type = $input->getOption('type'))) {
            if (null === ($type = $this->getConfig()->media->image->defaultType)) {
                $type = self::DEFAULT_TYPE;
            }
        }

        return $type;
    }

    private function getOutputPath(Media $media, int $width, int $height, string $size, string $ext): string
    {
        $path = $media->path;
        $path = explode('.', $path);

        array_pop($path);

        if ($size === 'custom') {
            $size = sprintf('%dx%d', $width, $height);
        }

        return sprintf('%s-%s.%s', implode('.', $path), $size, $ext);
    }


    private function computeSize(InputInterface $input, ImageInterface $image): array
    {
        $dar = $image->getSize()->getWidth() / $image->getSize()->getHeight();

        if (null !== ($size = $input->getOption('size'))) {
            $width = $this->getConfig()->media->thumbnail->$size ?? 0;
            $height = round($width / $dar);

        } else {
            $width = $input->getOption('width');
            $height = $input->getOption('height');
            $size = 'custom';
        }

        return [$width, $height, $size];
    }


    private function getThumbnail(Media $media, int $width, int $height, string $outputSize, string $outputType, string $outputExt): Media
    {
        if (null !== ($thumbnail = $this->getMediaRepository()->findOneByMasterAndVersion($media, $outputSize))) {
            if (false === $thumbnail->delete()) {
                throw PersistenceException::removing($thumbnail);
            }
        }

        return $this->createThumbnail($media, $width, $height, $outputSize, $outputType, $outputExt);
    }

    private function createThumbnail(Media $media, int $width, int $height, string $outputSize, string $outputType, string $outputExt): Media
    {
        $thumbnail = new Media();
        $thumbnail->setDI($this->di);
        
        $outputPath = $this->getOutputPath($media, $width, $height, $outputSize, $outputExt);

        if (file_exists($outputPath)) {
            @unlink($outputPath);
        }

        $thumbnail->adId = $media->adId;
        $thumbnail->masterId = $media->id;
        $thumbnail->temp = $media->temp;
        $thumbnail->poster = $media->poster;
        $thumbnail->uploaderIp = $media->uploaderIp;
        $thumbnail->path = $outputPath;
        $thumbnail->type = $outputType;
        $thumbnail->name = $media->name;
        $thumbnail->version = $outputSize;
        $thumbnail->width = $width;
        $thumbnail->height = $height;

        return $thumbnail;
    }

    private function setThumbnailMeta(Media $thumbnail)
    {
        $thumbnail->size = @filesize($this->fs($thumbnail));
    }

    private function getResolution(InputInterface $input, string $name, int $width): int
    {
        $res = $input->getOption($name);

        if ('lo' === $res) {
            return self::RES_LO;
        }

        if ('mid' === $res) {
            return self::RES_MID;
        }

        if ('hi' === $res) {
            return self::RES_HI;
        }

        $config = $this->getConfig()->media->thumbnail;

        if ($width < $config->md) {
            return self::RES_LO;
        }

        if ($width > $config->lg) {
            return self::RES_HI;
        }

        return self::RES_MID;
    }

}
