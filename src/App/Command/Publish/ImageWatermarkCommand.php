<?php

namespace App\Command\Publish;

use App\Model\Media;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;
use RuntimeException;

class ImageWatermarkCommand extends PublishMediaCommand
{

    const POS_BOTTOM_LEFT  = 'bottom-left';
    const POS_BOTTOM_RIGHT = 'bottom-right';
    const POS_CENTER       = 'center';
    const POS_TOP_LEFT     = 'top-left';
    const POS_TOP_RIGHT    = 'top-right';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:watermark')
            ->setDescription('Adds a watermark to the input image resource.')
            ->addOption('watermark', null, InputOption::VALUE_OPTIONAL, 'The watermark image path')
            ->addOption('position', null, InputOption::VALUE_OPTIONAL, 'The watermark position')
            ->addOption('width', null, InputOption::VALUE_OPTIONAL, 'The watermark width')
            ->addOption('height', null, InputOption::VALUE_OPTIONAL, 'The watermark height')
            ->addOption('hpad', null, InputOption::VALUE_OPTIONAL, 'The horizontal padding')
            ->addOption('vpad', null, InputOption::VALUE_OPTIONAL, 'The vertical padding');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getConfig();

        if (null === $input->getOption('watermark')) {
            $input->setOption('watermark', $config->media->watermark->path);
        }

        if (null === $input->getOption('position')) {
            $input->setOption('position', $config->media->watermark->position);
        }

        if (null === $input->getOption('width')) {
            $input->setOption('width', $config->media->watermark->width);
        }

        if (null === $input->getOption('height')) {
            $input->setOption('height', $config->media->watermark->height);
        }

        if (null === $input->getOption('hpad')) {
            $input->setOption('hpad', $config->media->watermark->padX);
        }

        if (null === $input->getOption('vpad')) {
            $input->setOption('vpad', $config->media->watermark->padY);
        }
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        if (! $media->isImage()) {
            throw new RuntimeException(sprintf('Only image resources can be watermarked, "%s" given.', $media->getType()));
        }

        $path = $this->fs($media);

        $output->writeln(sprintf(' - Using watermark <comment>%s</comment> at %s (with target size %s:%s, pad %s:%s)',
            $input->getOption('watermark'),
            $input->getOption('position'),
            $input->getOption('width'),
            $input->getOption('height'),
            $input->getOption('hpad'),
            $input->getOption('vpad')
        ));

        $image = $this->getImagine()->open($path);
        $watermark = $this->getImagine()->open($input->getOption('watermark'));

        list ($position, $size) = $this->getWatermarkPosition($input, $output, $image, $watermark);

        $watermark->resize($size);

        $image
            ->paste($watermark, $position)
            ->save(str_replace('.tmp', '.jpg', $path));
    }

    private function getInt(InputInterface $input, string $name, int $max): int
    {
        $value = $input->getOption($name);

        if (null === $value) {
            return 0;
        }

        if (is_string($value) && '%' === substr($value, -1)) {
            return intval($max * floatval($value) / 100);
        }

        return intval($value);
    }

    private function getWatermarkPosition(InputInterface $input, OutputInterface $output, ImageInterface $image, ImageInterface $watermark): array
    {
        // Image dimensions
        $imageWidth  = $image->getSize()->getWidth();
        $imageHeight = $image->getSize()->getHeight();

        // Watermark dimensions
        $wmarkWidth  = $watermark->getSize()->getWidth();
        $wmarkHeight = $watermark->getSize()->getHeight();
        $wmarkRatio  = $wmarkWidth / $wmarkHeight;

        // Watermark size is adjusted to span 75% of the image
        // horizontally, as long as the adjusted watermark width
        // does not exceed it's real size
        $adjWidth  = min($wmarkWidth, round(0.75 * $imageWidth));
        $adjHeight = round($adjWidth / $wmarkRatio);

        // Paddings for BOTTOM_LEFT, BOTTOM_RIGHT, TOP_LEFT and
        // TOP_RIGHT positions
        $hpad = $this->getInt($input, 'hpad', $imageWidth);
        $vpad = $this->getInt($input, 'vpad', $imageHeight);

        $pos = $input->getOption('position');

        switch ($pos) {
            case self::POS_BOTTOM_LEFT:
                return $this->createPoint($output, $imageWidth, $imageHeight, $adjWidth, $adjHeight, $hpad, $imageHeight - $vpad - $adjHeight, $pos);

            case self::POS_BOTTOM_RIGHT:
                return $this->createPoint($output, $imageWidth, $imageHeight, $adjWidth, $adjHeight, $imageWidth - $hpad - $adjWidth, $imageHeight - $vpad - $adjHeight, $pos);

            case self::POS_CENTER:
                return $this->createPoint($output, $imageWidth, $imageHeight, $adjWidth, $adjHeight, round(($imageWidth - $adjWidth) / 2), round(($imageHeight - $adjHeight) / 2), $pos);

            case self::POS_TOP_LEFT:
                return $this->createPoint($output, $imageWidth, $imageHeight, $adjWidth, $adjHeight, $hpad, $vpad, $pos);

            case self::POS_TOP_RIGHT:
                return $this->createPoint($output, $imageWidth, $imageHeight, $adjWidth, $adjHeight, $imageHeight - $hpad - $adjWidth, $vpad, $pos);
        }

        throw new InvalidArgumentException(sprintf('Cannot determine position "%s".', $pos));
    }

    private function createPoint(OutputInterface $output, int $imageWidth, int $imageHeight, int $wmarkWidth, int $wmarkHeight, int $x, int $y, string $position): array
    {
        if (0 > $x || 0 > $y) {
            throw new RuntimeException(sprintf('Image size is too small (%dx%d) to be watermarked', $imageWidth, $imageHeight));
        }

        $output->writeln(sprintf(' - Pasting %d:%d watermark at %s (%d, %d) inside %d:%d',
            $wmarkWidth,
            $wmarkHeight,
            $position,
            $x,
            $y,
            $imageWidth,
            $imageHeight
        ));

        return [
            new Point($x, $y),
            new Box($wmarkWidth, $wmarkHeight),
        ];
    }

}
