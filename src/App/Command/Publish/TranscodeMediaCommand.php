<?php

namespace App\Command\Publish;

use Alchemy\BinaryDriver\Exception\ExecutionFailureException;

use App\IO\PathUtil;
use App\Model\Media;
use App\Repository\PersistenceException;

use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\Format\VideoInterface;
use FFMpeg\Media\Video;

use Phalcon\Config;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use InvalidArgumentException;
use RuntimeException;

class TranscodeMediaCommand extends PublishMediaCommand
{

    const POS_BOTTOM_LEFT  = 'bottom-left';
    const POS_BOTTOM_RIGHT = 'bottom-right';
    const POS_CENTER       = 'center';
    const POS_TOP_LEFT     = 'top-left';
    const POS_TOP_RIGHT    = 'top-right';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:transcode')
            ->setDescription('Transcodes the input media resource.');
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        if (! $media->isStream()) {
            throw new RuntimeException(sprintf('Only stream resources can be processed, "%s" given.', $media->type));
        }
        
        $path = $this->fs($media);

        $this->writeLine($output);
        $this->writeHeader($output);
        $this->writeLine($output);

        $output->writeln(sprintf('<info>%-10s</info>  %-20s  %-20s  <comment>%s...</comment>',
            'master',
            $media->videoCodec,
            $media->audioCodec,
            PathUtil::format($media->path)
        ));

        $stream = $this->getFFMpeg()->open($path);
        /*
        $stream
            ->filters()
            ->watermark($this->getWatermarkPath(), $this->getWatermarkOptions());
         */

        foreach ($this->getOutputFormats() as $outputFormat => $outputOptions) {
            $this->transcode($output, $stream, $media, $outputFormat, $outputOptions);
        }

        $this->writeLine($output);
    }


    private function transcode(OutputInterface $output, Video $stream, Media $media, string $outputFormat, Config $outputOptions): Media
    {
        $rendition = $this->getRendition($media, $outputFormat, $outputOptions);

        $format = $this->getFormat($outputFormat, $outputOptions);

        try {
            //$stream->save($format, $rendition->getFspath());
            $this->save($this->fs($media), $this->fs($rendition), $outputOptions);
        } catch (ExecutionFailureException $e) {
            throw new RuntimeException(sprintf('Failed to save rendition %s: %s.', $outputFormat, $e->getMessage()), $e->getCode(), $e);
        }

        $this->setRenditionMeta($rendition);

        if (false === $rendition->save()) {
            throw PersistenceException::saving($rendition);
        }

        $this->writeRendition($output, $rendition, $format);

        return $rendition;
    }

    private function getWatermarkPath(): string
    {
        return $this->getConfig()->media->watermark->path;
    }

    private function getWatermarkOptions(): array
    {
        $watermark = $this->getConfig()->media->watermark;

        $options = [
            'position' => 'relative',
        ];

        switch ($watermark->position) {
            case self::POS_BOTTOM_LEFT:
                $options['bottom'] = $watermark->padY;
                $options['left'] = $watermark->padX;
                break;

            case self::POS_BOTTOM_RIGHT:
                $options['bottom'] = $watermark->padY;
                $options['right'] = $watermark->padX;
                break;

            case self::POS_CENTER:
                $options['top'] = '(main_h-overlay_h)/2';
                $options['left'] = '(main_w-overlay_w)/2';
                break;

            case self::POS_TOP_LEFT:
                $options['top'] = $watermark->padY;
                $options['right'] = $watermark->padX;
                break;

            case self::POS_TOP_RIGHT:
                $options['top'] = $watermark->padY;
                $options['right'] = $watermark->padX;
                break;

            default:
                throw new InvalidArgumentException(sprintf('Invalid position "%s".', $watermark->position));
        }

        return $options;
    }

    private function getRendition(Media $media, string $outputFormat, Config $outputOptions): Media
    {
        if (null !== ($rendition = $this->getMediaRepository()->findOneByMasterAndVersion($media, $outputFormat))) {
            if (false === $rendition->delete()) {
                throw PersistenceException::removing($rendition);
            }
        }

        return $this->createRendition($media, $outputFormat, $outputOptions);
    }

    private function createRendition(Media $media, string $outputFormat, Config $outputOptions): Media
    {
        $rendition = new Media();
        $rendition->setDI($this->di);

        $outputPath = $this->getOutputPath($media, $outputFormat, $outputOptions);

        $rendition->adId = $media->adId;
        $rendition->masterId = $media->id;
        $rendition->temp = $media->temp;
        $rendition->poster = false;
        $rendition->name = $media->name;
        $rendition->version = $outputFormat;
        $rendition->path = $outputPath;
        $rendition->type = $outputOptions->type;
        $rendition->containerType = $outputOptions->container;
        $rendition->uploaderIp = $media->uploaderIp;

        return $rendition;
    }

    private function setRenditionMeta(Media $rendition)
    {
        /** @var StreamCollection $streams */
        $streams = $this->getFFProbe()->streams($this->fs($rendition));

        /** @var Stream $audio */
        $audio = $streams->audios()->first();

        $rendition->audioDesc = sprintf('%s - %s %s (%s)',
            $this->prop($audio, 'codec_tag_string'),
            $this->prop($audio, 'codec_long_name'),
            $this->prop($audio, 'profile'),
            $this->prop($audio, 'sample_fmt')
        );
        $rendition->audioCodec = $this->prop($audio, 'codec_name');
        $rendition->audioProfile = $this->prop($audio, 'profile');
        $rendition->audioChannels = $this->prop($audio, 'channel_layout');
        $rendition->audioSampleRate = $this->prop($audio, 'sample_rate');
        $rendition->audioBitrate = $this->prop($audio, 'bit_rate');

        /** @var Stream $video */
        $video = $streams->videos()->first();

        $rendition->videoDesc  = sprintf('%s - %s %s %s (%s)',
            $this->prop($video, 'codec_tag_string'),
            $this->prop($video, 'codec_long_name'),
            $this->prop($video, 'profile'),
            $this->prop($video, 'level'),
            $this->prop($video, 'pix_fmt')
        );
        $rendition->videoCodec = $this->prop($video, 'codec_name');
        $rendition->videoProfile = $this->prop($video, 'profile');
        $rendition->videoLevel = $this->prop($video, 'level');
        $rendition->videoDar = $this->prop($video, 'display_aspect_ratio');
        $rendition->videoPar = $this->prop($video, 'sample_aspect_ratio');
        $rendition->videoChromaSubsampling = $this->prop($video, 'pix_fmt');
        $rendition->duration = $this->prop($video, 'duration');
        $rendition->width = $this->prop($video, 'width');
        $rendition->height = $this->prop($video, 'height');
        $rendition->size = @filesize($this->fs($rendition));
    }


    private function getOutputPath(Media $media, string $outputFormat, Config $outputOptions): string
    {
        $path = $media->path;
        $path = explode('.', $path);

        array_pop($path);

        return sprintf('%s-%s.%s', implode('.', $path), str_replace('_', '-', $outputFormat), $outputOptions->ext);
    }

    private function getOutputFormats(): array
    {
        $outputFormats = [];

        foreach ($this->getConfig()->media->transcoding as $outputFormat => $outputOptions) {
            $outputFormats[$outputFormat] = $outputOptions;
        }

        return $outputFormats;
    }

    private function getFormat(string $format, Config $options): VideoInterface
    {
        return $this->getFormatFactory()->create($format, $options);
    }


    private function getVideoDetails(VideoInterface $format): string
    {
        return sprintf('%s %skbps',
            $format->getVideoCodec(),
            $format->getKiloBitrate()
        );
    }

    private function getAudioDetails(VideoInterface $format): string
    {
        return sprintf('%s %skbps',
            $format->getAudioCodec(),
            $format->getAudioKiloBitrate()
        );
    }

    private function writeHeader(OutputInterface $output)
    {
        $output->writeln(sprintf('%-10s  %-20s  %-20s  %-20s', 'Format', 'Video', 'Audio', 'Hash'));
    }

    private function writeLine(OutputInterface $output)
    {
        $output->writeln(sprintf('%s  %s  %s  %s',
            str_repeat('-', 10),
            str_repeat('-', 20),
            str_repeat('-', 20),
            str_repeat('-', 20)
        ));
    }

    private function writeRendition(OutputInterface $output, Media $rendition, VideoInterface $format)
    {
        $output->writeln(sprintf('<info>%-10s</info>  %-20s  %-20s  <comment>%-17s...</comment>',
            $rendition->version,
            $this->getVideoDetails($format),
            $this->getAudioDetails($format),
            substr($rendition->path, 0, 17)
        ));
    }


    private function save(string $inputPath, string $outputPath, Config $outputOptions)
    {
        $config = $this->getConfig();
        $watermark = $config->media->watermark;
        $position = $this->getWatermarkOptions();

        $command = [];
        $command[] = $config->ffmpeg->binaries;

        // Input
        $command[] = '-y';
        $command[] = sprintf('-i %s', $inputPath);

        // Filters (watermark)
        $command[] = sprintf('-i %s', $watermark->path);
        //$command[] = sprintf('-filter_complex "overlay=x=%s:y=%s"', $position['left'], $position['top']);

        // Threads
        $command[] = sprintf('-threads %d', $config->ffmpeg->threads);

        // Video
        $command[] = sprintf('-codec:v %s', $outputOptions->videoCodec);
        $command[] = sprintf('-profile:v %s', $outputOptions->profile);
        $command[] = sprintf('-level:v %s', $outputOptions->level);
        $command[] = sprintf('-b:v %dk', $outputOptions->videoBitrate);
        $command[] = sprintf('-filter_complex "[0:v]scale=%d:-2[scaled];[scaled][1:v]overlay=x=%s:y=%s[out]" -map "[out]" -map 0:a', $outputOptions->size, $position['left'], $position['top']);
        $command[] = '-pix_fmt yuv420p';
        $command[] = '-movflags +faststart';

        // Audio
        $command[] = sprintf('-codec:a %s', $outputOptions->audioCodec);
        $command[] = sprintf('-b:a %dk', $outputOptions->audioBitrate);

        // Output
        $command[] = $outputPath;

        $ps = new Process(implode(' ', $command));
        $ps->run();

        if (! $ps->isSuccessful()) {
            throw new ProcessFailedException($ps);
        }
    }

}