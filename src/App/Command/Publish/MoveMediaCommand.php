<?php

namespace App\Command\Publish;

use App\Command\InjectionAwareCommand;
use App\Config\Config;
use App\IO\PathGenerator;
use App\IO\PathUtil;
use App\Manager\ModelManager;
use App\Model\Ad;
use App\Model\Media;
use App\Repository\AdRepository;
use App\Repository\EntityManager;

use App\Repository\MediaRepository;
use Phalcon\Mvc\Model\Resultset;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;

class MoveMediaCommand extends InjectionAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:move')
            ->setDescription('Moves ad media to the appropriate public folder.')
            ->addOption('id', 'i', InputOption::VALUE_REQUIRED, 'The ad ID');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (null === ($adId = $input->getOption('id'))) {
            throw new RuntimeException('The ad identifier is required.');
        }

        if (null === ($ad = $this->findAd($adId))) {
            throw new RuntimeException(sprintf('Cannot found ad resource %s.', $adId));
        }

        $output->writeln('');
        $output->writeln(sprintf('Moving <info>ad %s</info> media ...', $ad->id));

        foreach ($this->findAdMedia($ad->id) as $media) {
            $this->move($output, $ad, $media);
        }

        $output->writeln('');
        $output->writeln('<info>Done</info>');
    }


    /**
     * @param OutputInterface $output
     * @param Ad $ad
     * @param Media $media
     *
     * @return void
     */
    protected function move(OutputInterface $output, Ad $ad, Media $media)
    {
        $src = $this->getSourcePath($media);
        $tgt = $this->getPathGenerator()->generatePublicPath($ad, $media);

        $rsrc = $media->path;
        $rtgt = str_replace($this->getPublicDir(), '/', $tgt);

        if (! file_exists($src)) {
            throw new RuntimeException(sprintf('Source file %s does not exist.', PathUtil::format($rsrc)));
        }

        if ($src === $tgt) {
            $output->writeln(sprintf(' - <info>Media %d</info> is already at target path <comment>%s</comment>', $media->id, PathUtil::format($rtgt)));
        } else {
            if (file_exists($tgt)) {
                @unlink($tgt);
            }

            if (false === @rename($src, $tgt)) {
                throw new RuntimeException(sprintf('Failed to move file from %s to %s.', PathUtil::format($rsrc), PathUtil::format($rtgt)));
            }

            $output->writeln(sprintf(' - <info>Media %d</info> moved to target path <comment>%s</comment>', $media->id, PathUtil::format($rtgt)));

            $media->path = $rtgt;
            $media->temp = false;

            if (false === $media->save()) {
                throw new RuntimeException(sprintf('Failed to update media path: %s.', ModelManager::errorsAsString($media)));
            }
        }
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return PathGenerator
     */
    protected function getPathGenerator(): PathGenerator
    {
        return $this->di->get('pathGenerator');
    }

    /**
     * @return string
     */
    protected function getPublicDir(): string
    {
        return $this->getConfig()->layout->publicDir;
    }

    /**
     * @param Media $media
     *
     * @return string
     */
    protected function getSourcePath(Media $media): string
    {
        $path = $media->path;

        if ('/' === $path[0] && file_exists($path)) {
            return $path;
        }

        return $this->getPublicDir() . ltrim($media->path, '/');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->di->get('em');
    }

    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getEntityManager()
            ->getRepository(Ad::class)
            ->hydrateRecords()
            ->disableCache();

        return $repository;
    }

    /**
     * @return MediaRepository
     */
    protected function getMediaRepository(): MediaRepository
    {
        /** @var MediaRepository $repository */
        $repository = $this->getEntityManager()
            ->getRepository(Media::class)
            ->hydrateRecords()
            ->disableCache();

        return $repository;
    }

    /**
     * @param string $id
     *
     * @return Ad | null
     */
    protected function findAd($id): Ad
    {
        return $this->getAdRepository()->find($id);
    }

    /**
     * @param string $id
     *
     * @return Resultset
     */
    protected function findAdMedia($id)
    {
        return $this->getMediaRepository()->findBy('adId = '. $id);
    }
}