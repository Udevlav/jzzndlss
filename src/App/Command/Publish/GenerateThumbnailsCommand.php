<?php

namespace App\Command\Publish;

use App\Model\Media;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Throwable;

class GenerateThumbnailsCommand extends PublishMediaCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:thumbnails')
            ->setDescription('Generates thumbnails from the input image resource.');
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        foreach ($this->getConfig()->media->thumbnail->toArray() as $size => $width) {
            $this->generateThumbnail($output, $media, $size);
        }
    }

    /**
     * @inheritdoc
     */
    protected function generateThumbnail(OutputInterface $output, Media $media, string $size)
    {
        $commandInput = new ArrayInput([
            'command' => 'publish:thumbnail',
            '--id'    => $media->id,
            '--size'  => $size,
        ]);

        try {
            /** @var Command $command */
            $command = $this->getApplication()->find('publish:thumbnail');
            $returnCode = $command->run($commandInput, $output);
        } catch (Throwable $t) {
            return 1;
        }

        return $returnCode;
    }

}
