<?php

namespace App\Command\Publish;

use App\Model\Media;
use App\Manager\PersistenceException;

use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\FFProbe\DataMapping\StreamCollection;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExtractMetadataCommand extends PublishMediaCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:metadata')
            ->setDescription('Extracts metadata for the input media resource.');
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        $path = $this->fs($media);

        $media->size = @filesize($path);

        if ($media->isImage()) {
            // For image media types, extract the dimensions (width, height)
            $size = $this->getImagine()->open($path)->getSize();

            $media->width = $size->getWidth();
            $media->height = $size->getHeight();

            $output->writeln(sprintf(' - size: <info>%s x %s px</info>',
                $media->width,
                $media->height
            ));
        }

        if ($media->isStream()) {
            // For streamable media types, extract the duration, frame
            // dimensions (width, height) and codec type

            /** @var StreamCollection $streams */
            $streams = $this->getFFProbe()->streams($path);

            /** @var Stream $audio */
            $audio = $streams->audios()->first();

            $media->audioDesc = sprintf('%s - %s %s (%s)',
                $this->prop($audio, 'codec_tag_string'),
                $this->prop($audio, 'codec_long_name'),
                $this->prop($audio, 'profile'),
                $this->prop($audio, 'sample_fmt')
            );
            $media->audioCodec = $this->prop($audio, 'codec_name');
            $media->audioProfile = $this->prop($audio, 'profile');
            $media->audioChannels = $this->prop($audio, 'channel_layout');
            $media->audioSampleRate = $this->prop($audio, 'sample_rate');
            $media->audioBitrate = $this->prop($audio, 'bit_rate');

            /** @var Stream $video */
            $video = $streams->videos()->first();

            $media->videoDesc  = sprintf('%s - %s %s %s (%s)',
                $this->prop($video, 'codec_tag_string'),
                $this->prop($video, 'codec_long_name'),
                $this->prop($video, 'profile'),
                $this->prop($video, 'level'),
                $this->prop($video, 'pix_fmt')
            );
            $media->videoCodec = $this->prop($video, 'codec_name');
            $media->videoProfile = $this->prop($video, 'profile');
            $media->videoLevel = $this->prop($video, 'level');
            $media->videoDar = $this->prop($video, 'display_aspect_ratio');
            $media->videoPar = $this->prop($video, 'sample_aspect_ratio');
            $media->videoChromaSubsampling = $this->prop($video, 'pix_fmt');
            $media->width      = $video->getDimensions()->getWidth();
            $media->height     = $video->getDimensions()->getHeight();
            $media->duration   = $this->prop($video, 'duration');

            $output->writeln([
                sprintf(' - audio codec: <info>%s</info>', $media->audioCodec),
                sprintf(' - video codec: <info>%s</info>', $media->videoCodec),
                sprintf(' - duration:    <info>%s</info>', $media->duration),
                sprintf(' - frame size:  <info>%s x %s px</info>',
                    $media->width,
                    $media->height
                ),
            ]);
        }

        if (false === $media->save()) {
            throw PersistenceException::saving($media);
        }
    }

}