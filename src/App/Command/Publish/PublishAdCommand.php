<?php

namespace App\Command\Publish;

use App\Command\Db\DbAwareCommand;
use App\Geo\UnknownGeoLocation;
use App\IO\PathGenerator;
use App\IO\PathUtil;
use App\Manager\PublishAdModelManager;
use App\Model\Ad;
use App\Model\Media;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use RuntimeException;

class PublishAdCommand extends DbAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:ad')
            ->setDescription('Publishes an ad (this may involve running other tasks).')
            ->addOption('id', 'i', InputOption::VALUE_OPTIONAL, 'Ad ID')
            ->addOption('no-watermark', 'w', InputOption::VALUE_OPTIONAL, 'Wheter to skip image watermarking (defaults to false)', false)
            ->addOption('keep-master', 'm', InputOption::VALUE_OPTIONAL, 'Wheter to keep masters (defaults to false)', false)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getConfig();

        if (null === ($adId = $input->getOption('id'))) {
            throw new RuntimeException('The input ad identifier is required.');
        }

        if (! ($ad = $this->findAd($adId))) {
            throw new RuntimeException(sprintf('Cannot found ad resource %s.', $adId));
        }

        $noWatermark = $input->getOption('no-watermark');
        $keepMasters = $input->getOption('keep-master');

        $output->writeln(['', sprintf('<info>●</info> Publishing ad <comment>%s</comment>', $ad->id)]);

        // Create target directory
        $path = $this->getPathGenerator()->generateAdPath($ad);

        if (file_exists($path)) {
            if (! is_dir($path)) {
                throw new RuntimeException(sprintf('Path %s exists, but it\'s not a directory.', PathUtil::format($path)));
            }

            if (! is_readable($path)) {
                throw new RuntimeException(sprintf('Path %s exists, but it\'s not readable.', PathUtil::format($path)));
            }

            if (! is_writable($path)) {
                throw new RuntimeException(sprintf('Path %s exists, but it\'s not writable.', PathUtil::format($path)));
            }

            $output->writeln(sprintf('├─ Notice: target dir <info>%s</info> already exists', PathUtil::format($path)));

        } else {
            if (! mkdir($path, 0777, true)) {
                throw new RuntimeException(sprintf('Path %s exists, but it\'s not a directory.', PathUtil::format($path)));
            }

            $output->writeln(sprintf('├─ Notice: created target dir <info>%s</info>', PathUtil::format($path)));
        }

        // Schedule jobs by media
        $jobs = [];
        $numJobs = 1;
        $medias = $this->findTempMasterMedia($ad);

        if (0 === count($medias)) {
            $output->writeln('├─ <error>Warning: No temporary master media found. Nothing to do</error>.');
        }

        foreach ($medias as $media) {
            /** @var Media $media */
            $queue = [];
            $queue[] = sprintf('publish:metadata -v -i %s', $media->id);

            if ($media->isImage()) {
                $queue[] = sprintf('publish:thumbnails -v -i %s', $media->id);

                if (! $noWatermark) {
                    $queue[] = sprintf('publish:watermarks -v -i %s', $media->id);
                }

                $queue[] = sprintf('publish:optimize -v -i %s', $media->id);

            } else if ($media->isVideo()) {
                $queue[] = sprintf('publish:frames -v -i %s -f %d', $media->id, $config->media->video->frames);
                $queue[] = sprintf('publish:transcode -v -i %s', $media->id);
            }

            $jobs[$media->id] = $queue;
            $numJobs += count($queue);
        }

        $output->writeln(sprintf('├─ Queued <info>%d jobs</info> for <info>%s medias</info>', $numJobs, count($medias)));

        $offset = 0;

        foreach ($jobs as $id => $queue) {
            $this->writeQueue($output, $id, $offset ++ === count($jobs) - 1);

            foreach ($queue as $job) {
                $exitCode = -1;
                $execTime = -1;
                $throwable = null;

                try {
                    list ($exitCode, $execTime) = $this->runJob($job);
                } catch (ProcessFailedException $e) {
                    $throwable = $e;
                } finally {
                    $this->writeJob($output, $job, $exitCode, $execTime);

                    if (null !== $throwable) {
                        throw $throwable;
                    }
                }
            }
        }

        $this->runMove($output, $ad);
        $this->publish($output, $ad, $medias, $keepMasters);

        $output->writeln('');
        $output->writeln('<info>Done</info>');
    }

    protected function runMove(OutputInterface $output, Ad $ad)
    {
        $job = sprintf('publish:move -v -i %s', $ad->id);

        $exitCode = -1;
        $execTime = -1;
        $throwable = null;

        try {
            list ($exitCode, $execTime) = $this->runJob($job);
        } catch (ProcessFailedException $e) {
            $throwable = $e;
        } finally {
            $this->writeJob($output, $job, $exitCode, $execTime, true);

            if (null !== $throwable) {
                throw $throwable;
            }
        }
    }

    protected function publish(OutputInterface $output, Ad $ad, $medias, $keepMasters)
    {
        $execTime = microtime(true);

        $manager = new PublishAdModelManager($ad);
        $manager->setDI($this->di);
        $manager->setGeoLocation(new UnknownGeoLocation('0.0.0.0'));
        $manager->setNotifyBySms(false);

        $manager->save();

        if (! $keepMasters) {
            foreach ($medias as $media) {
                @unlink($media->path);
            }
        }

        $this->writeJob($output, 'publish and rank ad', 0, microtime(true) - $execTime, true);
    }

    protected function runJob($job): array
    {
        $tic = microtime(true);
        $cmd = $this->getPhpCommand($job);

        $process = new Process($cmd);
        $process->setTimeout(600);
        $process->run();

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return [
            $process->getExitCode(),
            microtime(true) - $tic,
        ];
    }


    protected function getPathGenerator(): PathGenerator
    {
        return $this->di->get('pathGenerator');
    }

    protected function findAd($id)
    {
        return $this->di->get('em')
            ->getRepository(Ad::class)
            ->disableCache()
            ->find($id);
    }

    protected function findTempMasterMedia(Ad $ad)
    {
        return $this->di->get('em')
            ->getRepository(Media::class)
            ->hydrateRecords()
            ->disableCache()
            ->findMastersByAd($ad);
    }


    private function writeQueue(OutputInterface $output, $id, bool $final = false)
    {
        $tree = $final ? '└─' : '├─';
        $output->writeln(sprintf('%s <info>●</info> %s', $tree, sprintf('Queue for media <info>%s</info>', $id)));
    }

    private function writeJob(OutputInterface $output, $job, $exitCode, $execTime, bool $final = false)
    {
        $pad = $final ? '' : '│  ';
        $tree = $final ? '└─' : '├─';
        $tag = $exitCode === 0 ? 'info' : 'error';

        $output->write(sprintf('%s%s <%s>●</> Job <comment>%s</comment> (%0.3fs)', $pad, $tree, $tag, $job, $execTime));
        $output->write("\n");
    }

}