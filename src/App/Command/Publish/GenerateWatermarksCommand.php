<?php

namespace App\Command\Publish;

use App\IO\PathUtil;
use App\Model\Media;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;
use Throwable;

class GenerateWatermarksCommand extends PublishMediaCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:watermarks')
            ->setDescription('Generates watermarks for the input image resource\'s thumbnails.');
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        $output->writeln(sprintf('<info>%-10s</info>  %-5s  %-5s  %0.2f  <comment>%s</comment>',
            'master',
            $media->width,
            $media->height,
            $media->width / $media->height,
            PathUtil::format($media->path)
        ));

        foreach ($this->getThumbnails($media) as $thumb) {
            $this->generateWatermark($output, $thumb);
        }
    }

    protected function generateWatermark(OutputInterface $output, Media $media)
    {
        $commandInput = new ArrayInput([
            'command' => 'publish:watermark',
            '--id'    => $media->id,
        ]);

        try {
            /** @var Command $command */
            $command = $this->getApplication()->find('publish:watermark');
            $returnCode = $command->run($commandInput, $output);
        } catch (Throwable $t) {
            $returnCode = 1;
        }

        if (0 !== $returnCode) {
            throw new RuntimeException(sprintf('<error>Command publish:watermark returned code %d</error>.', $returnCode));
        }
    }

    protected function getThumbnails(Media $media)
    {
        return $this->getMediaRepository()->findThumbnails($media);
    }

}
