<?php

namespace App\Command\Publish;

use Alchemy\BinaryDriver\Exception\ExecutionFailureException;

use App\IO\PathUtil;
use App\Model\Media;
use App\Repository\PersistenceException;

use FFMpeg\Coordinate\TimeCode;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;
use RuntimeException;

class GenerateFramesCommand extends PublishMediaCommand
{

    const DEFAULT_TYPE = 'image/jpeg';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('publish:frames')
            ->setDescription('Extracts frames from the input video resource.')
            ->addOption('timecode', 't', InputOption::VALUE_OPTIONAL, 'The frame timecode')
            ->addOption('frames', 'f', InputOption::VALUE_OPTIONAL, 'The number of frames to extract')
            ->addOption('delta', 'd', InputOption::VALUE_OPTIONAL, 'The time delta (seconds) between frames')
            ->addOption('type', 'o', InputOption::VALUE_OPTIONAL, 'The frame media type (defaults to image/jpeg)');
    }

    /**
     * @inheritdoc
     */
    protected function process(InputInterface $input, OutputInterface $output, Media $media)
    {
        if (! $media->isVideo()) {
            throw new RuntimeException(sprintf('Only video resources can be processed, "%s" given.', $media->type));
        }

        $path = $this->fs($media);

        $timeCodes = $this->getTimeCodes($input, $media, $path);
        $outputType = $this->getOutputType($input);
        $outputExt = $this->getOutputExtension($outputType);

        $resource = $this->getFFMpeg()->open($path);

        $this->writeLine($output);
        $this->writeHeader($output);
        $this->writeLine($output);

        $output->writeln(sprintf('<info>%-15s</info>  %-15s  <comment>%s</comment>',
            'master',
            '-',
            PathUtil::format($media->path)
        ));

        $lastOffset = count($timeCodes) - 1;

        foreach ($timeCodes as $offset => $timeCode) {
            $frame = $this->getMediaFrame($media, $timeCode, $outputType, $outputExt);

            $output->writeln(sprintf('<info>%-15s</info>  %-15s  <comment>%s</comment>',
                $frame->version,
                $timeCode,
                PathUtil::format($frame->path)
            ));

            try {
                $resource
                    ->frame(TimeCode::fromSeconds($timeCode))
                    ->save($this->fs($frame));

            } catch (ExecutionFailureException $e) {
                throw new RuntimeException(sprintf('<error>Failed to extract frame at %s to %s</error>: %s', $timeCode, PathUtil::format($media->path), $e->getMessage()), $e->getCode(), $e);
            }

            try {
                $this->setMediaFrameMeta($frame);
            } catch (InvalidArgumentException $f) {
                if ($offset === $lastOffset && ! file_exists($this->fs($frame))) {
                    // Maybe the resource duration is 59.999 seconds, and we're
                    // requesting a frame at 60s ...
                    break;
                }

                throw new RuntimeException(sprintf('<error>Failed to extract frame at %s to %s</error>: %s', $timeCode, PathUtil::format($media->path), $e->getMessage()), $e->getCode(), $e);
            }

            if (false === $frame->save()) {
                throw PersistenceException::saving($frame);
            }
        }

        $this->writeLine($output);
    }


    private function getTimeCodes(InputInterface $input, Media $media, string $path): array
    {
        $timeCodes = [];

        if (null !== ($timeCode = $input->getOption('timecode'))) {
            $timeCodes[] = $timeCode;

        } else {
            $stream = $this->getFFProbe()->streams($path)->videos()->first();
            $duration = $stream->get('duration');

            if (null === ($delta = $input->getOption('delta'))) {
                if (null === ($frames = $input->getOption('frames'))) {
                    throw new RuntimeException(sprintf('One of the input options (timecode, frames, or delta) should be specified.'));
                }

                $delta = floor($duration / $frames);
            }

            for ($timeCode = 0; $timeCode < $duration; $timeCode += $delta) {
                $timeCodes[] = $timeCode;
            }
        }

        return $timeCodes;
    }

    private function getOutputType(InputInterface $input): string
    {
        if (null === ($type = $input->getOption('type'))) {
            if (null === ($type = $this->getConfig()->media->image->defaultType)) {
                $type = self::DEFAULT_TYPE;
            }
        }

        return $type;
    }

    private function getOutputExtension(string $type): string
    {
        if (! isset(static::$ext[$type])) {
            throw new InvalidArgumentException(sprintf('Unsupported output frame type "%s".', $type));
        }

        return static::$ext[$type];
    }

    private function getOutputPath(Media $media, float $timeCode, string $ext): string
    {
        $path = $media->path;
        $path = explode('.', $path);

        array_pop($path);

        return sprintf('%s-%s.%s', implode('.', $path), $timeCode, $ext);
    }

    private function getMediaFrame(Media $media, float $timeCode, string $outputType, string $outputExt): Media
    {
        if (null !== ($thumbnail = $this->getMediaRepository()->findOneByMasterAndVersion($media, 'frame-'. $timeCode))) {
            if (false === $thumbnail->delete()) {
                throw PersistenceException::removing($thumbnail);
            }
        }

        return $this->createMediaFrame($media, $timeCode, $outputType, $outputExt);
    }

    private function createMediaFrame(Media $media, float $timeCode, string $outputType, string $outputExt): Media
    {
        $output = new Media();
        $output->setDI($this->di);
        
        $outputPath = $this->getOutputPath($media, $timeCode, $outputExt);

        $output->adId = $media->adId;
        $output->masterId = $media->id;
        $output->poster = $media->poster;
        $output->temp = $media->temp;
        $output->path = $outputPath;
        $output->type = $outputType;
        $output->name = $media->name;
        $output->name = $media->name;
        $output->version = 'frame-'. $timeCode;
        $output->timeCode = $timeCode;
        $output->uploaderIp = $media->uploaderIp;

        return $output;
    }

    private function setMediaFrameMeta(Media $frame)
    {
        $path = $this->fs($frame);

        $image = $this->getImagine()->open($path);

        $frame->size = @filesize($path);
        $frame->width = $image->getSize()->getWidth();
        $frame->height = $image->getSize()->getHeight();
    }


    private function writeHeader(OutputInterface $output)
    {
        $output->writeln(sprintf('%-15s  %-15s  %-32s', 'Version', 'Time Code', 'Path'));
    }

    private function writeLine(OutputInterface $output)
    {
        $output->writeln(sprintf('%s  %s  %s',
            str_repeat('-', 15),
            str_repeat('-', 15),
            str_repeat('-', 32)
        ));
    }

}