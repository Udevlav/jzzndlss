<?php

namespace App\Command\Rank;

use App\Command\InjectionAwareCommand;
use App\Manager\ModelManager;
use App\Model\Ad;
use App\Rank\Ranker;
use App\Repository\AdRepository;
use App\Repository\EntityManager;

use Phalcon\Mvc\Model\Resultset;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DateTime;
use InvalidArgumentException;
use RuntimeException;

/**
 * @package App\Command\Gc
 */
class RankCommand extends InjectionAwareCommand
{

    /**
     * @var int
     */
    const MAX_RESULTS = 1e3;

    /**
     * @var array
     */
    static $timeAgo = [
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    ];


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('rank:ad')
            ->setDescription('Updates ad scores.')
            ->addOption('placebo', 'p', InputOption::VALUE_OPTIONAL, 'Placebo', false);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $placebo = (bool) $input->getOption('placebo');
        $verbose = (bool) $input->getOption('verbose');

        $limit = self::MAX_RESULTS;
        $offset = 0;
        $processed = 0;

        $output->writeln('Ranking ads ...');

        // Select records
        for (;;) {
            $records = $this->find($limit, $offset);
            $count = $records->count();

            if (0 === $count) {
                $output->writeln(sprintf('<info>Done</info>. %d records processed', $processed));

                return 0;
            }

            $ranker = $this->getRanker();

            foreach ($records as $ad) {
                $ad->score = $ranker->rank($ad);

                if (! $placebo) {
                    if (false === $ad->save()) {
                        throw new RuntimeException(sprintf('Failed to update ad score: %s.', ModelManager::errorsAsString($ad)));
                    }
                }

                if ($verbose) {
                    $output->writeln(sprintf('<comment>%4d</comment>: <info>%6d</info> (%4d hits, %s)',
                        $ad->id,
                        $ad->score,
                        $ad->numHits,
                        $this->elapsed($ad->publishedAt)
                    ));
                }

                $processed ++;
            }

            $offset += $limit + 1;
        }

        return 0;
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return Resultset
     */
    protected function find(int $limit, int $offset): Resultset
    {
        return $this->getAdRepository()
            ->getPublishedQuery()
            ->limit($limit)
            ->offset($offset)
            ->getQuery()
            ->execute();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->di->get('em');
    }

    /**
     * @return Ranker
     */
    protected function getRanker(): Ranker
    {
        return $this->di->get('ranker');
    }

    /**
     * @return AdRepository
     *
     * @throws InvalidArgumentException
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getEntityManager()
            ->getRepository(Ad::class)
            ->disableCache();

        return $repository;
    }

    protected function elapsed(string $datetime, bool $full = false) {
        $now = new DateTime();
        $ago = new DateTime($datetime);

        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $timeAgo = array_slice(static::$timeAgo, 0);

        foreach ($timeAgo as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($timeAgo[$k]);
            }
        }

        if (! $full) {
            $timeAgo = array_slice($timeAgo, 0, 1);
        }

        return $timeAgo ? implode(', ', $timeAgo) . ' ago' : 'just now';
    }
}