<?php

namespace App\Command\Rank;

use App\Command\InjectionAwareCommand;
use App\Manager\ModelManager;
use App\Model\Ad;
use App\Model\Promotion;
use App\Repository\AdRepository;
use App\Repository\EntityManager;

use Phalcon\Mvc\Model\Resultset;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DateTime;
use InvalidArgumentException;
use RuntimeException;

class UpdateRankCommand extends InjectionAwareCommand
{

    /**
     * @var string
     */
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var int
     */
    const MAX_RESULTS = 1e3;

    /**
     * @var array
     */
    static $timeAgo = [
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    ];


    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('rank:update')
            ->setDescription('Updates ad scores.')
            ->addOption('dryrun', 'd', InputOption::VALUE_OPTIONAL, 'Dry-run (do not actually perform any query)', false);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dryRun = (bool) $input->getOption('dryrun');
        $verbose = (bool) $input->getOption('verbose');

        $limit = self::MAX_RESULTS;
        $offset = 0;
        $processed = 0;

        $output->writeln(sprintf('Rank Updater - job run at %s', date('r')));

        // Select records; process results in batches with
        // size up to MAX_RESULTS records
        for (;;) {
            $records = $this->find($limit, $offset);
            $count = $records->count();

            if (0 === $count) {
                // No more records
                $output->writeln(sprintf('<info>Done</info>. %d records processed', $processed));

                return 0;
            }

            $output->writeln(sprintf('Found <info>%d</info> promoted records to process (%d already processed)',
                $count,
                $processed
            ));

            foreach ($records as $record) {
                /** @var Ad $ad */
                $ad = $record['ad'];
                /** @var Promotion $ad */
                $promotion = $record['promo'];

                if (! $dryRun) {
                    if (! count($promotion->timestamps)) {
                        // This SHOULD not happen
                        continue;
                    }

                    // Compute new publication time
                    $newPromotedAt = $this->computePromotionTime($promotion);

                    if ($newPromotedAt === $ad->promotedAt) {
                        if ($verbose) {
                            $output->writeln(sprintf('<comment>%4d</comment>: <info>%-20s</info>: Promotion time unaltered <info>%s</info> (%4d hits, %0.3f score)',
                                $ad->id,
                                substr($ad->slug, 0, 20),
                                $ad->promotedAt,
                                $ad->numHits,
                                $ad->score
                            ));
                        }

                        // No queries
                        continue;
                    }

                    $ad->promotedAt = $newPromotedAt;

                    if (false === $ad->save()) {
                        throw new RuntimeException(sprintf('Failed to update ad score: %s.', ModelManager::errorsAsString($ad)));
                    }
                }

                if ($verbose) {
                    $output->writeln(sprintf('<comment>%4d</comment>: <info>%-20s</info>: New promotion time <info>%s</info> (%4d hits, %0.3f score)',
                        $ad->id,
                        substr($ad->slug, 0, 20),
                        $ad->promotedAt,
                        $ad->numHits,
                        $ad->score
                    ));
                }

                $processed ++;
            }

            $offset += $limit + 1;
        }

        return 0;
    }

    /**
     * @param Promotion $promotion
     *
     * @return string
     */
    protected function computePromotionTime(Promotion $promotion): string
    {
        $now = time();

        // Ensure promotions are ordered in ascendent order
        sort($promotion->timestamps);
        $candidate = null;

        foreach ($promotion->timestamps as $timestamp) {
            if ((int) $timestamp > $now) {
                if (null !== $candidate) {
                    return date(self::DATE_FORMAT, $candidate);
                }

                break;
            }

            if ($timestamp < $now) {
                $candidate = $timestamp;
            }
        }

        // Timestamp is guaranteed to be not null, as
        // timestamps is a non-empty array
        /** @noinspection PhpUndefinedVariableInspection */
        return date(self::DATE_FORMAT, $timestamp);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return Resultset
     */
    protected function find(int $limit, int $offset): Resultset
    {
        return $this->getAdRepository()
            ->getWithPromotionQuery()
            ->limit($limit)
            ->offset($offset)
            ->getQuery()
            ->execute();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->di->get('em');
    }

    /**
     * @return AdRepository
     *
     * @throws InvalidArgumentException
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getEntityManager()
            ->getRepository(Ad::class)
            ->disableCache();

        return $repository;
    }

    protected function elapsed(string $datetime, bool $full = false) {
        $now = new DateTime();
        $ago = new DateTime($datetime);

        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $timeAgo = array_slice(static::$timeAgo, 0);

        foreach ($timeAgo as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($timeAgo[$k]);
            }
        }

        if (! $full) {
            $timeAgo = array_slice($timeAgo, 0, 1);
        }

        return $timeAgo ? implode(', ', $timeAgo) . ' ago' : 'just now';
    }
}
