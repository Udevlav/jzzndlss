<?php

namespace App\Command\Db;

use App\Manager\ModelManager;
use App\Model\AdStats;
use App\Model\Hit;
use App\Repository\HitRepository;

use Google_Client;
use Google_Exception;
use Google_Service_Analytics;
use Google_Service_Analytics_GaData;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use RuntimeException;

class UpdateStatsCommand extends DbAwareCommand
{

    const ACTION_IMPRESSION = 'impression';
    const ACTION_PHONE_CLICK = 'phoneClick';
    const ACTION_TOP = 'top';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('db:update:stats')
            ->setDescription('Updates ad stats from Google Analytics.')
            ->addOption('date', 'd', InputOption::VALUE_OPTIONAL, 'The date (YYYY-MM-DD)')
        ;
    }

    /**
     * @inheritdoc
     *
     * @throws Google_Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = $input->getOption('date');

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $from = date('Y-m-d', strtotime($date) - 24 * 60 * 60);
        $to = $date;

        $client = new Google_Client();
        $client->setApplicationName('Wallalumi');
        $client->setAuthConfig($this->getAuthConfig());
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);

        $analytics = new Google_Service_Analytics($client);
        $profileId = $this->getFirstProfileId($analytics);

        if (empty($profileId)) {
            $output->writeln('<error>Failed to retrieve Google Analytics profile ID</error>');
            return;
        }

        $output->writeln(sprintf('Google Analytics profile ID <comment>%s</comment>', $profileId));

        $events = $this->getEvents($profileId, $analytics, $from, $to);

        $stats = [];
        $rows = $events->getRows();

        if (! is_array($rows)) {
            $output->writeln('<error>No data</error>');
            return;
        }

        $output->writeln(sprintf('<comment>%d records</comment> from Google Analytics (date is <info>%s</info>)',
            count($rows),
            $date
        ));

        foreach ($rows as $row) {
            $eventCategory = $row[0];
            $eventAction = $row[1];
            $eventValue = (int) $row[3];

            if ($eventCategory !== 'ad') {
                continue;
            }

            if (! isset($stats[$eventValue])) {
                $stats[$eventValue] = [];
            }

            if (! isset($stats[$eventValue][$eventAction])) {
                $stats[$eventValue][$eventAction] = 0;
            }

            $stats[$eventValue][$eventAction] ++;
        }

        foreach ($stats as $adId => $adStats) {
            if (! $adId) {
                continue;
            }

            $numHits = $this->getNumHits($adId, $from, $to);

            $record = new AdStats();
            $record->adId = $adId;
            $record->measuredAt = $date;
            $record->numDaysOnTop = $adStats[self::ACTION_TOP] ?? 0;
            $record->numImpressions = $adStats[self::ACTION_IMPRESSION] ?? 0;
            $record->numPhoneClicks = $adStats[self::ACTION_PHONE_CLICK] ?? 0;
            $record->numVisits = $numHits;

            if (false === $record->save()) {
                //throw new RuntimeException(sprintf('Failed to save ad stats: %s.', ModelManager::errorsAsString($record)));
                $output->writeln(sprintf('<error>Failed to save ad stats: %s</error>.', ModelManager::errorsAsString($record)), E_USER_WARNING);
            }
        }
    }


    private function getAuthConfig(): string
    {
        return dirname(dirname(dirname(dirname(__DIR__)))).'/var/credentials/Wallalumi-7e5998ad7d98.json';
    }

    private function getNumHits(int $adId, $from, $to): int
    {
        /** @var HitRepository $repository */
        $repository = $this->getRepository(Hit::class);

        return $repository->countByAd($adId, strtotime($from), strtotime($to));
    }

    private function getEvents($profileId, Google_Service_Analytics $analytics, $from, $to): Google_Service_Analytics_GaData
    {
        return $analytics->data_ga->get('ga:'.$profileId, $from, $to, 'ga:totalEvents', [
            'dimensions' => 'ga:eventCategory,ga:eventAction,ga:eventLabel',
        ]);
    }

    // Get the user's first view (profile) ID.
    private function getFirstProfileId(Google_Service_Analytics $analytics)
    {
        // Get the list of accounts for the authorized user.
        $accounts = $analytics->management_accounts->listManagementAccounts();

        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                // Get the list of views (profiles) for the authorized user.
                $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();

                    // Return the first view (profile) ID.
                    return $items[0]->getId();
                } else {
                    throw new RuntimeException('No views (profiles) found for this user.');
                }
            } else {
                throw new RuntimeException('No properties found for this user.');
            }
        } else {
            throw new RuntimeException('No accounts found for this user.');
        }
    }
}