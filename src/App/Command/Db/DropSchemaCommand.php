<?php

namespace App\Command\Db;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DropSchemaCommand extends DbAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('db:schema:drop')
            ->setDescription('Drops the DB schema.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->di->get('config');
        $path =

        $output->writeln('Dropping DB schema ...');
        $output->writeln('');

        $process = new Process(sprintf('mysql -v -h %s -P %d -u %s --password=%s %s --execute=%s',
            $config->db->host,
            $config->db->port,
            $config->db->username,
            $config->db->password,
            $config->db->dbname,
            escapeshellarg($this->getCommand($config->db->dbname))
        ));

        $process->run();

        if ($input->getOption('verbose')) {
            $output->writeln($process->getOutput());
        }

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output->writeln(sprintf('<info>Done</info>. Process returned code %d.', $process->getExitCode()));
        $output->writeln('');
    }


    private function getCommand(string $dbname): string
    {
        return <<<EOC
SET FOREIGN_KEY_CHECKS = 0; 
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables 
  WHERE table_schema = '$dbname';

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1; 
EOC;
    }

}