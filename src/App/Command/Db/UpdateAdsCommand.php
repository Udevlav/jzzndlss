<?php

namespace App\Command\Db;

use App\Model\Ad;
use App\Model\Tag;
use App\Repository\AdRepository;

use App\Repository\TagRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAdsCommand extends DbAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('db:update:ads')
            ->setDescription('Ads hash update.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $verbose = $input->getOption('verbose');
        $repository = $this->getAdRepository();
        $updated = 0;
        $page = 0;

        while (true) {
            $results = $repository->getPage($page ++, 100);

            if (! $results->count()) {
                break;
            }

            foreach ($results as $result) {
                /** @var Ad $result */
                $tags = [];

                foreach ($this->getAdTags($result) as $tag) {
                    $tags[] = $tag->toArray();
                }

                $result->allTags = $tags;
                $result->generateHash();

                if (false === $result->save()) {
                    $errors = [];

                    foreach ($result->getMessages() as $message) {
                        $errors[] = sprintf('%s: %s', $message->getField(), $message->getMessage());
                    }

                    $output->writeln(sprintf('<error>Failed to update ad: %s.</error>', $errors));

                } else {
                    if ($verbose) {
                        $output->writeln(sprintf(' %s Updated ad hash <comment>%s</comment>', "\xe2\x9c\x94", $result->hash));
                    }

                    $updated ++;
                }
            }
        }

        $output->writeln(sprintf('<info>Done</info>. %d ads updated.', $updated));

        return 0;
    }


    private function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->di->get('em')->getRepository(Ad::class);
        $repository->disableCache();

        return $repository;
    }

    private function getAdTags(Ad $ad)
    {
        /** @var TagRepository $repository */
        $repository = $this->di->get('em')->getRepository(Tag::class);

        return $repository->findByAd($ad);
    }

}