<?php

namespace App\Command\Db;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class LoadFixturesCommand extends DbAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('db:fixtures:load')
            ->setDescription('Loads the data fixtures.')
            ->addOption('fixtures', 'f', InputOption::VALUE_OPTIONAL, 'A comma-separated list of fixtures to load');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->di->get('config');

        if (null !== ($fixtures = $input->getOption('fixtures'))) {
            $fixtures = preg_split('/\s*,\s*/', $fixtures);
            $fixtures = array_map(function($path) use ($config) {
                return sprintf('%ssql/fixtures/%s', $config->layout->varDir, $path);
            }, $fixtures);
        } else {
            $fixtures = $this->getFixtures($config->layout->varDir .'sql/fixtures');
        }

        $output->writeln(sprintf('Loading %d data fixtures ...', count($fixtures)));

        foreach ($fixtures as $fixture) {
            $output->write(sprintf('Loading <comment>%s</comment> ... ', $fixture));

            $process = new Process(sprintf('mysql -v -h %s -P %d -u %s --password=%s %s < %s',
                $config->db->host,
                $config->db->port,
                $config->db->username,
                $config->db->password,
                $config->db->dbname,
                $fixture
            ));

            $process->run();

            if ($input->getOption('verbose')) {
                $output->writeln($process->getOutput());
            }

            if (! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $output->write("OK\n");
        }

        $output->writeln(sprintf('<info>Done</info>. Process returned code %d.', $process->getExitCode()));
    }


    private function getFixtures(string $path): array
    {
        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        $fixtures = [];

        foreach ($iterator as $path => $file) {
            if (preg_match('/^.+\.sql$/i', $path)) {
                $fixtures[] = $path;
            }
        }

        return $fixtures;
    }

}