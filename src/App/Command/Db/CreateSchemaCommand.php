<?php

namespace App\Command\Db;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CreateSchemaCommand extends DbAwareCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('db:schema:create')
            ->setDescription('Creates the DB schema.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->di->get('config');
        $path = $config->layout->varDir .'sql/schema.sql';

        $output->writeln(sprintf('Creating DB schema <info>%s</info> ...', $path));

        if (is_readable($path)) {
            $process = new Process(sprintf('mysql -v -h %s -P %d -u %s --password=%s %s < %s',
                $config->db->host,
                $config->db->port,
                $config->db->username,
                $config->db->password,
                $config->db->dbname,
                $path
            ));

            $process->run();

            if ($input->getOption('verbose')) {
                $output->writeln($process->getOutput());
            }

            if (! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $output->writeln(sprintf('<info>Done</info>. Process returned code %d.', $process->getExitCode()));
        } else {
            $output->writeln(sprintf('<warning>Skipped</warning>. File %s does not exists', $path));
        }

        $output->writeln('');
    }

}