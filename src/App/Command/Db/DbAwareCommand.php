<?php

namespace App\Command\Db;

use App\Command\InjectionAwareCommand;
use App\Repository\EntityManager;
use App\Repository\Repository;

use Phalcon\Db\Adapter\Pdo;

use InvalidArgumentException;

class DbAwareCommand extends InjectionAwareCommand
{


    /**
     * @return Pdo
     */
    protected function getDb(): Pdo
    {
        return $this->getDI()->get('db');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->getDI()->getShared('em');
    }

    /**
     * @param string $fullyQualifiedClassName
     *
     * @return Repository
     *
     * @throws InvalidArgumentException
     */
    protected function getRepository(string $fullyQualifiedClassName): Repository
    {
        return $this->getEntityManager()->getRepository($fullyQualifiedClassName);
    }

}
