<?php

namespace App\FFMpeg;

use FFMpeg\Format\VideoInterface;

use Phalcon\Config;
use Phalcon\Di\InjectionAwareInterface;
use Phalcon\DiInterface;

class FormatFactory implements InjectionAwareInterface
{

    protected $di;
    protected $pool;

    public function getDI()
    {
        return $this->di;
    }

    public function setDI(DiInterface $di)
    {
        $this->di = $di;

        return $this;
    }

    public function create($format, Config $options = null): VideoInterface
    {
        $this->lazyLoad();

        if (! isset($this->pool[$format])) {
            throw NoSuchFormatException::name($format);
        }

        $className = $this->pool[$format];

        return new $className();
    }


    private function lazyLoad()
    {
        if (null === $this->pool) {
            $this->pool = [];

            foreach ($this->di->get('config')->media->transcoding as $format => $options) {
                $this->pool[$format] = $options['className'];
            }
        }
    }
}