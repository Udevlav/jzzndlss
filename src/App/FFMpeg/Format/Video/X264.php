<?php

namespace App\FFMpeg\Format\Video;

use FFMpeg\Format\Video\X264 as BaseX264;

class X264 extends BaseX264
{

    public function __construct($videoCodec = 'libx264')
    {
        parent::__construct('libmp3lame', $videoCodec);
    }

    public function getAvailableAudioCodecs()
    {
        return [
            'aac',
            'libvo_aacenc',
            'libmp3lame',
        ];
    }
}
