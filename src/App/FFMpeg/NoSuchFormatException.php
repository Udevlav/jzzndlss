<?php

namespace App\FFMpeg;

class NoSuchFormatException extends \InvalidArgumentException
{

    static public function name(string $format): NoSuchFormatException
    {
        return new static(sprintf('No such transcoding format "%s".', $format));
    }
}