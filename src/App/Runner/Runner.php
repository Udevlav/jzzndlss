<?php

namespace App\Runner;

use App\Config\Config;
use App\Model\Ad;

use Phalcon\Logger\Adapter;
use Phalcon\Mvc\User\Component;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

use RuntimeException;

class Runner extends Component
{

    /**
     * @var string
     */
    protected $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }


    public function getTempPath(Ad $ad): string
    {
        $authority = explode('.', $this->getConfig()->server->authority);
        $authority = implode('.', array_reverse($authority));

        return sprintf('%s.%s.%s.log', $this->getTempDir(), $authority, $ad->id);
    }

    public function getLockPath(Ad $ad): string
    {
        $authority = explode('.', $this->getConfig()->server->authority);
        $authority = implode('.', array_reverse($authority));

        return sprintf('%s.%s.%s.lock', $this->getTempDir(), $authority, $ad->id);
    }

    public function run(Ad $ad)
    {
        $this->writeLock($ad);
        $this->getLogger()->debug(sprintf('Wrote lockfile %s', $this->getLockPath($ad)));

        $cmd = sprintf('php %s publish:ad -i %s > %s 2>&1 &', $this->path, $ad->id, $this->getTempPath($ad));

        $this->getLogger()->debug(sprintf('Running "%s" ...', $cmd));

        $process = new Process($cmd);
        $process->setTimeout(600);
        $process->run();

        //$this->removeLock($ad);
        //$this->getLogger()->debug(sprintf('Deleted lockfile %s', $this->getLockPath($ad)));

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }


    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    protected function getLogger(): Adapter
    {
        return $this->di->get('logger');
    }

    protected function getTempDir(): string
    {
        return sys_get_temp_dir() .'/';
        //return $this->getConfig()->layout->logDir;
    }

    protected function writeLock(Ad $ad)
    {
        $path = $this->getLockPath($ad);

        if (false === @touch($path)) {
            throw new RuntimeException(sprintf('Failed to create lock file %s.', $path));
        }
    }

    protected function removeLock(Ad $ad)
    {
        $path = $this->getLockPath($ad);

        if (false === @unlink($path)) {
            throw new RuntimeException(sprintf('Failed to remove lock file %s.', $path));
        }
    }
}