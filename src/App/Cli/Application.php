<?php

namespace App\Cli;

use Phalcon\Di\InjectionAwareInterface;
use Phalcon\Mvc\Application as App;
use Phalcon\Version;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Debug\Exception\FatalThrowableError;

use Throwable;

class Application extends BaseApplication
{
    private $app;
    private $commandsRegistered = false;
    private $registrationErrors = [];

    /**
     * @param \Phalcon\Mvc\Application $app
     */
    public function __construct(App $app)
    {
        $config = $app->getDI()->get('config');

        parent::__construct($config->app->name, $config->app->version);

        $this->app = $app;

        $this->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The environment name', $config->app->env));
        $this->getDefinition()->addOption(new InputOption('--no-debug', null, InputOption::VALUE_NONE, 'Switches off debug mode'));
    }

    /**
     * @return \Phalcon\Mvc\Application
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Runs the current application.
     *
     * @param InputInterface  $input  An Input instance
     * @param OutputInterface $output An Output instance
     *
     * @return int 0 if everything went fine, or an error code
     */
    public function doRun(InputInterface $input, OutputInterface $output)
    {
        if ($this->registrationErrors) {
            $this->renderRegistrationErrors($input, $output);
        }

        return parent::doRun($input, $output);
    }

    /**
     * {@inheritdoc}
     */
    protected function doRunCommand(Command $command, InputInterface $input, OutputInterface $output)
    {
        if ($this->registrationErrors) {
            $this->renderRegistrationErrors($input, $output);
        }

        return parent::doRunCommand($command, $input, $output);
    }

    /**
     * {@inheritdoc}
     */
    public function find($name)
    {
        $this->registerCommands();

        return parent::find($name);
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        $this->registerCommands();

        $command = parent::get($name);

        if ($command instanceof InjectionAwareInterface) {
            $command->setDI($this->app->getDI());
        }

        return $command;
    }

    /**
     * {@inheritdoc}
     */
    public function all($namespace = null)
    {
        $this->registerCommands();

        return parent::all($namespace);
    }

    /**
     * {@inheritdoc}
     */
    public function getLongVersion()
    {
        $config = $this->app->getDI()->get('config');

        return parent::getLongVersion().sprintf(' (PHP: %s, phalcon: <comment>%s</comment>, env: <comment>%s</>, debug: <comment>%s</>)',
            \PHP_VERSION,
            Version::get(),
            $config->app->env,
            $config->app->debug ? 'true' : 'false'
        );
    }

    public function add(Command $command)
    {
        $this->registerCommands();

        return parent::add($command);
    }

    protected function registerCommands()
    {
        if ($this->commandsRegistered) {
            return;
        }

        $this->commandsRegistered = true;

        $di = $this->app->getDI();

        foreach ($di->get('config')->cli->commands as $id) {
            try {
                $this->add($di->get($id));
            } catch (\Exception $e) {
                $this->registrationErrors[] = $e;
            } catch (\Throwable $e) {
                $this->registrationErrors[] = new FatalThrowableError($e);
            }
        }
    }

    protected function renderRegistrationErrors(InputInterface $input, OutputInterface $output)
    {
        if ($output instanceof ConsoleOutputInterface) {
            $output = $output->getErrorOutput();
        }

        (new SymfonyStyle($input, $output))->warning('Some commands could not be registered.');

        foreach ($this->registrationErrors as $error) {
            $this->doRenderError($error, $output, 'RegistrationError');
        }

        $this->registrationErrors = array();
    }

    protected function doRenderError(Throwable $t, OutputInterface $output, string $type = null)
    {
        $output->writeln([
            '',
            sprintf('<error>[%s]</error>: %s', null === $type ? get_class($t) : $type, $t->getMessage()),
            $t->getTraceAsString(),
            '',
        ]);

        if (null !== ($p = $t->getPrevious())) {
            $this->doRenderError($p, $output);
        }
    }
}