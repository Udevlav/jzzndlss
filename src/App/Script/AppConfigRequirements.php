<?php

namespace App\Script;

use App\Config\Config;

use App\Util\Bytes;
use Symfony\Requirements\RequirementCollection;

/**
 * This class specifies all requirements and optional recommendations that
 * are necessary in the app config file.
 */
class AppConfigRequirements extends RequirementCollection
{

    public function __construct(Config $config)
    {
        /* mandatory requirements follow */

        foreach ($config->layout->toArray() as $key => $dir) {
            $rdir = str_replace($config->layout->rootDir, '/', $dir);
            $this->addRequirement(is_dir($dir), sprintf('layout directory .%s does not exist', $rdir), sprintf('create directory .%s', $rdir));
        }

        $this->addPhpConfigRequirement('allow_url_fopen', true);
        $this->addPhpConfigRequirement('date.timezone', function($value) {
            return $value === 'UTC';
        }, false, 'date.timezone should be UTC in php.ini', 'Set date.timezone to UTC in php.ini*');

        $this->addPhpConfigRequirement('default_charset', 'UTF-8');
        $this->addPhpConfigRequirement('display_errors', false);
        $this->addPhpConfigRequirement('display_startup_errors', false);
        $this->addPhpConfigRequirement('file_uploads', true);
        $this->addPhpConfigRequirement('log_errors', true);
        $this->addPhpConfigRequirement('max_execution_time', function($value) {
            return $value <= 30;
        }, false, sprintf('max_execution_time is too high (%ds) in php.ini', ini_get('max_execution_time')), 'Set max_execution_time to 30 in php.ini*');

        $this->addPhpConfigRequirement('memory_limit', function($value) use ($config) {
            return $this->parseSize($value) >= 128 * 1024 * 1024;
        }, false, 'memory_limit should be greater than or equal to 128M in php.ini', 'Set memory_limit to 128M in php.ini*');

        $this->addPhpConfigRequirement('short_open_tag', false);
        $this->addPhpConfigRequirement('upload_max_filesize', function($value) use ($config) {
            return $this->parseSize($value) >= $config->media->image->maxSize;
        }, false, sprintf('upload_max_filesize should be greater than %s bytes to support image uploads in php.ini', Bytes::format($config->media->image->maxSize)), sprintf('Set upload_max_filesize to %s in php.ini*', Bytes::format($config->media->image->maxSize)));

        $this->addPhpConfigRequirement('upload_max_filesize', function($value) use ($config) {
            return $this->parseSize($value) >= $config->media->video->maxSize;
        }, false, sprintf('upload_max_filesize should be greater than %s bytes to support video uploads in php.ini', Bytes::format($config->media->video->maxSize)), sprintf('Set upload_max_filesize to %s in php.ini*', Bytes::format($config->media->video->maxSize)));

        /* optional recommendations follow */
        $this->addPhpConfigRecommendation('expose_php', false);
    }


    protected function parseSize($size): int
    {
        // Remove the non-unit characters from the size
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);

        // Remove the non-numeric characters from the size
        $size = preg_replace('/[^0-9\.]/', '', $size);

        if ($unit) {
            // Find the position of the unit in the ordered string which
            // is the power of magnitude to multiply a kilobyte by
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }

        return round($size);
    }
}
