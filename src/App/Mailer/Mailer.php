<?php

namespace App\Mailer;

use Phalcon\Ext\Mailer\Manager;
use Phalcon\Ext\Mailer\Message;
use Phalcon\Mvc\User\Component;

class Mailer extends Component
{

    protected $manager;
    protected $mailbox;


    public function __construct(Manager $manager, string $mailbox = 'default')
    {
        $this->manager = $manager;
        $this->useMailbox($mailbox);
    }


    public function getManager(): Manager
    {
        return $this->manager;
    }

    public function getMailbox()
    {
        // Note that mailbox is lazy-loaded
        $config = $this->di->get('config');

        if (! isset($config->mailbox->{$this->mailbox})) {
            throw new \InvalidArgumentException(sprintf('Undefined mailbox "%s".', $this->mailbox));
        }

        return $config->mailbox->{$this->mailbox};
    }


    public function useMailbox($mailbox)
    {
        $this->mailbox = $mailbox;

        return $this;
    }

    public function useDefaultMailbox()
    {
        return $this->useMailbox('default');
    }


    public function createMessage(string $to, string $subject, string $content): Message
    {
        $message = $this->manager->createMessage()
            ->subject($subject)
            ->content($content)
            ->to($to);

        return $message;
    }

    public function createMessageFromView(string $to, string $subject, string $template, array $data = []): Message
    {
        $message = $this->manager->createMessageFromView($template, $data)
            ->subject($subject)
            ->to($to);

        return $message;
    }

    public function send(string $to, string $subject, ... $args)
    {
        $mailbox = $this->getMailbox();

        switch (count($args)) {
            case 1:
                $message = $this->createMessage($to, $subject, $args[0]);
                break;

            case 2:
                $message = $this->createMessageFromView($to, $subject, ... $args);
                break;

            default:
                throw new \BadMethodCallException();
        }

        $message->from($mailbox->from);

        foreach ($mailbox->cc as $cc) {
            $message->cc($cc);
        }

        foreach ($mailbox->bcc as $bcc) {
            $message->bcc($bcc);
        }

        if (1 !== $message->send()) {
            throw new MailerException(sprintf('Failed to send message "%s" to "%s" (using mailbox %s).', $subject, $to, $this->mailbox));
        }
    }
}