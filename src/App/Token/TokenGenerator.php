<?php

namespace App\Token;

use App\Config\Config;

use Phalcon\Mvc\User\Component;

class TokenGenerator extends Component
{

    /**
     * Generates a token.
     *
     * @param array ... $args
     *
     * @return \App\Token\Token
     */
    public function generateToken(... $args): Token
    {
        $hash = hash($this->getHashingAlgorithm(), implode('-', $args));

        return new Token($hash, $this->getTokenLifetime());
    }



    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    protected function getHashingAlgorithm(): string
    {
        return $this->getConfig()->sex->hashingAlgorithm;
    }

    protected function getTokenLifetime(): int
    {
        return $this->getConfig()->publication->tokenLifetime;
    }
}