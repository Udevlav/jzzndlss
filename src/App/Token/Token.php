<?php

namespace App\Token;

class Token
{

    /**
     * Factory method.
     *
     * @param string $hash
     * @param string $from
     * @param string $to
     *
     * @return Token
     */
    static public function of(string $hash, string $from, string $to): Token
    {
        if (empty($hash)) {
            throw new \InvalidArgumentException('The token hash should not be empty');
        }

        $token = new static($hash, 0);

        if (false === ($token->validFrom = \DateTime::createFromFormat('Y-m-d H:i:s', $from))) {
            throw new \InvalidArgumentException('Invalid token validity start date');
        }

        if (false === ($token->validTo = \DateTime::createFromFormat('Y-m-d H:i:s', $to))) {
            throw new \InvalidArgumentException('Invalid token validity expiration date');
        }

        if ($token->validFrom > $token->validTo) {
            throw new \InvalidArgumentException('Token validity start should be lower than expiration date');
        }

        return $token;
    }


    private $hash;
    private $validFrom;
    private $validTo;

    public function __construct(string $hash, int $lifetime)
    {
        $this->hash = $hash;
        $this->validFrom = new \DateTime();
        $this->validTo = new \DateTime();
        $this->validTo->add(new \DateInterval(sprintf('PT%dS', $lifetime)));
    }

    public function __toString()
    {
        return $this->getHash();
    }


    public function getHash(): string
    {
        return $this->hash;
    }

    public function getValidFrom(): \DateTime
    {
        return $this->validFrom;
    }

    public function getValidTo(): \DateTime
    {
        return $this->validTo;
    }

    public function isValid(): bool
    {
        $now = new \DateTime();

        return (null !== $this->hash) && ($now > $this->validFrom) && ($now < $this->validTo);
    }
}