<?php

namespace App\Sms;

class SmsServiceUnreachableException extends SmsException
{

    static public function service(string $name, int $errno = null, string $errstr = null): SmsServiceUnreachableException
    {
        $message = sprintf('Unreachable SMS service API "%s"', $name);

        if (null !== $errno) {
            $message .= sprintf(' (%d)', $errno);
        }

        if (null !== $errstr) {
            $message .= sprintf(': %s', $errstr);
        }

        $message .= '.';

        return new static($message);
    }
}