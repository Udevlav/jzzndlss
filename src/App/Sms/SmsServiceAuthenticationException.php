<?php

namespace App\Sms;

class SmsServiceAuthenticationException extends SmsException
{

    static public function service(string $name): SmsServiceAuthenticationException
    {
        return new static(sprintf('Failed to authenticate with SMS service API "%s"', $name));
    }
}