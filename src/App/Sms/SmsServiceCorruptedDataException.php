<?php

namespace App\Sms;

class SmsServiceCorruptedDataException extends SmsException
{

    static public function response($data): SmsServiceCorruptedDataException
    {
        return new static(sprintf('Corrupted SMS service API response: "%s".', $data));
    }
}