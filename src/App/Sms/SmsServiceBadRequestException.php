<?php

namespace App\Sms;

class SmsServiceBadRequestException extends SmsException
{

    static public function request(string $data): SmsServiceBadRequestException
    {
        return new static(sprintf('Bad SMS service API request (maybe missing required params): "%s".', $data));
    }
}