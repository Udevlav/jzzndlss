<?php

namespace App\Sms;

class SmsServiceNoCreditsException extends SmsException
{

    static public function service(string $name): SmsServiceNoCreditsException
    {
        return new static(sprintf('No credits left for SMS service API "%s"', $name));
    }
}