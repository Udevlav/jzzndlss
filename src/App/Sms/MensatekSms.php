<?php

namespace App\Sms;

class MensatekSms extends Sms
{

    const API_NAME      = 'mensatek';
    const API_AUTHORITY = 'api.mensatek.com';

    const ENDPOINT_SEND    = 'enviar.php';
    const ENDPOINT_CREDITS = 'creditos.php';

    const PARAM_EMAIL         = 'Correo';
    const PARAM_PASSWORD      = 'Passwd';
    const PARAM_RECIPIENTS    = 'Destinatarios';
    const PARAM_MESSAGE       = 'Mensaje';
    const PARAM_SENDER        = 'Remitente';
    const PARAM_DATE          = 'Fecha';
    const PARAM_REPORT        = 'Report';
    const PARAM_DISCOUNT      = 'Descuento';
    const PARAM_EMAIL_REPORT  = 'EmailReport';
    const PARAM_RESPONSE_TYPE = 'Resp';
    const PARAM_REF           = 'Referencia';


    static public function supported(): bool
    {
        if (function_exists('curl_init')) {
            return true;
        }

        if (function_exists('fsockopen')) {
            return true;
        }

        if (@ini_get('allow_url_fopen')) {
            return true;
        }

        return false;
    }


    private $email;
    private $password;
    private $version = 'v5';
    private $https;

    public function __construct(string $email, string $password, string $version = 'v5', bool $https = true)
    {
        if (! static::supported()) {
            throw new \RuntimeException(sprintf('Cannot use %s API.', self::API_NAME));
        }

        $this->email = $email;
        $this->password = $password;
        $this->version = $version;
        $this->https = $https;
    }


    protected function doCheckCredits(): float
    {
        try {
            $data = $this->request(self::ENDPOINT_CREDITS, $this->buildCreditsParameters());
        } catch (SmsException $e) {
            if ($this->isLenient()) {
                return 0.0;
            }
            throw $e;
        }

        return $data->Cred;
    }

    protected function doSend($recipients, string $message): int
    {
        try {
            $data = $this->request(self::ENDPOINT_SEND, $this->buildSendParameters($recipients, $message));
        } catch (SmsException $e) {
            if ($this->isLenient()) {
                return 0;
            }
            throw $e;
        }

        return $data->Res;
    }


    protected function request(string $path, array $parameters)
    {
        $qualifiedPath = sprintf('/%s/%s', $this->version, $path);

        $this->info(sprintf('Sending SMS request to API path %s: %s', $path, http_build_query($parameters)));

        if (function_exists('curl_init')) {
            $ch = curl_init();

            curl_setopt($ch, \CURLOPT_URL, $this->getApiUrl($qualifiedPath));
            curl_setopt($ch, \CURLOPT_VERBOSE, 1);
            curl_setopt($ch, \CURLOPT_HEADER, 1);
            curl_setopt($ch, \CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
            curl_setopt($ch, \CURLOPT_POST, 1);
            curl_setopt($ch, \CURLOPT_FOLLOWLOCATION,1);
            curl_setopt($ch, \CURLOPT_POSTFIELDS, http_build_query($parameters));
            curl_setopt($ch, \CURLOPT_PORT, $this->https ? 443 : 80);
            curl_setopt($ch, \CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, \CURLOPT_TIMEOUT, 10);

            $response = @curl_exec($ch);

            @curl_close($ch);

            if (false === $response) {
                throw SmsServiceUnreachableException::service(self::API_NAME);
            }

        } else if (@ini_get('allow_url_fopen')) {
            $options = [
                'http' => [
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($parameters, '', '&'),
                ],
            ];

            if (false === ($context = @stream_context_create($options))) {
                throw SmsServiceUnreachableException::service(self::API_NAME);
            }

            if (false === ($response = @file_get_contents($this->getApiUrl($path), false, $options))) {
                throw SmsServiceUnreachableException::service(self::API_NAME);
            }

        } else if (function_exists('fsockopen')) {
            $fp = @fsockopen (self::API_AUTHORITY, $this->https ? 443 : 80, $errno, $errstr, 10);

            if (false === $fp) {
                throw SmsServiceUnreachableException::service(self::API_NAME, $errno, $errstr);
            }

            $data = http_build_query($parameters, '', '&');
            $length = strlen($data);

            fputs($fp, sprintf("POST %s HTTP/1.1\r\n", $qualifiedPath));
            fputs($fp, sprintf("Host: %s\r\n", self::API_AUTHORITY));
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-Length: $length\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $data);

            $response = '';

            while (! feof($fp)) {
                $response .= fgets($fp, 1024);
            }

            @fclose($fp);

        } else {
            throw new \LogicException();
        }

        $response = explode("\r\n", $response);
        $response = array_pop($response);

        try {
            $data = @json_decode($response);
        } catch (\Throwable $t) {
            $this->error(sprintf('Failed to fetch SMS service response: %s', $t->getMessage()));

            throw SmsServiceCorruptedDataException::response($response);
        }

        if (isset($data->Res)) {
            switch ($data->Res) {
                case -1:
                    $this->error('Failed to authenticate against SMS Service API');

                    throw SmsServiceAuthenticationException::service(self::API_NAME);

                case -2:
                    $this->error('No SMS Service credits left');

                    throw SmsServiceNoCreditsException::service(self::API_NAME);

                case -3:
                    $this->error('Bad SMS Service request');

                    // Do not leak auth info
                    unset($parameters[self::PARAM_EMAIL]);
                    unset($parameters[self::PARAM_PASSWORD]);

                    throw SmsServiceBadRequestException::request(self::API_NAME);
            }
        }

        $this->info(sprintf('Got SMS service response "%s"', $response));

        if (isset($data->Cred)) {
            $this->info(sprintf('%0.2f credits remaining', $data->Cred));
        }

        return $data;
    }


    private function getApiUrl(string $path): string
    {
        return sprintf('%s://%s/%s', $this->https ? 'https' : 'http', self::API_AUTHORITY, ltrim($path, '/'));
    }


    private function buildCreditsParameters(): array
    {
        return [
            self::PARAM_EMAIL         => $this->email,
            self::PARAM_PASSWORD      => $this->password,
            self::PARAM_RESPONSE_TYPE => 'JSON',
        ];
    }

    private function buildSendParameters($recipients, string $message): array
    {
        $config = $this->getDI()->get('config');

        if (! is_array($recipients)) {
            $recipients = [$recipients];
        }

        foreach ($recipients as $offset => $recipient) {
            $recipients[$offset] = $this->formatPhone($recipient);
        }

        return [
            self::PARAM_EMAIL         => $this->email,
            self::PARAM_PASSWORD      => $this->password,
            self::PARAM_RECIPIENTS    => implode(';', $recipients),
            self::PARAM_MESSAGE       => $message,
            self::PARAM_SENDER        => $this->formatSender($config->app->shortName),
            self::PARAM_DATE          => '', // Send now
            self::PARAM_REPORT        => 1,
            self::PARAM_DISCOUNT      => 0,
            self::PARAM_REF           => 'report',
            self::PARAM_RESPONSE_TYPE => 'JSON',
        ];
    }

    private function formatPhone(string $phone): string
    {
        return '34'.$phone;
    }

    private function formatSender(string $sender): string
    {
        return urlencode(strlen($sender) > 11 ? substr($sender, 0, 11) : $sender);
    }
}