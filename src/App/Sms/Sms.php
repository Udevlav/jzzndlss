<?php

namespace App\Sms;

use Phalcon\Logger;
use Phalcon\Logger\AdapterInterface;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

abstract class Sms extends Component
{

    protected $placebo = false;
    protected $lenient = true;
    protected $timeout = 10;


    public function checkCredits(): float
    {
        return $this->doCheckCredits();
    }

    public function send($recipients, ... $args): int
    {
        $eventManager = $this->getEventsManager();

        if (null !== $eventManager) {
            $eventManager->fire('sms:beforeSend', $recipients);
        }

        if ($this->placebo) {
            return 0;
        }

        try {
            if (count($args) === 2) {
                return $this->doSend($recipients, $this->renderTemplate($args[0], $args[1]));
            }

            if (count($args) === 1) {
                return $this->doSend($recipients, $args[0]);
            }
        } finally {
            if (null !== $eventManager) {
                $eventManager->fire('sms:afterSend', $recipients);
            }
        }

        throw new \BadMethodCallException();
    }


    abstract protected function doCheckCredits(): float;
    abstract protected function doSend($recipients, string $message): int;


    public function isPlacebo()
    {
        return $this->placebo;
    }

    public function setPlacebo(bool $placebo = false)
    {
        $this->placebo = $placebo;
    }

    public function isLenient()
    {
        return $this->lenient;
    }

    public function setLenient(bool $lenient = true)
    {
        $this->lenient = $lenient;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout = 10)
    {
        $this->timeout = $timeout;
    }


    protected function renderTemplate(string $template, array $parameters = []): string
    {
        $view = $this->getView();

        list ($dir, $path) = explode('/', $template);

        try {
            $view->start();
            $view->render($dir, $path, $parameters);
            $view->finish();
        } catch (\Throwable $t) {
            throw new \RuntimeException(sprintf('Failed to render template "%s": %s.', $template, $t->getMessage()), $t->getCode(), $t);
        }

        return $view->getContent();
    }


    protected function getView(): View
    {
        return $this->getDI()->get('view');
    }

    protected function getLogger(): AdapterInterface
    {
        return $this->getDI()->get('logger');
    }

    protected function log(string $message, int $level): Sms
    {
        $this->getLogger()->log($message, $level);

        return $this;
    }

    protected function debug(string $message): Sms
    {
        return $this->log($message, Logger::DEBUG);
    }

    protected function info(string $message): Sms
    {
        return $this->log($message, Logger::INFO);
    }

    protected function error(string $message): Sms
    {
        return $this->log($message, Logger::ERROR);
    }
}