<?php

namespace App\Captcha;

use App\Config\Config;

use Phalcon\Logger\Adapter;
use Phalcon\Mvc\User\Component;

/**
 * @see https://developers.google.com/recaptcha/docs/verify
 */
class ReCaptchaVerifier extends Component implements CaptchaVerifier
{

    /**
     * @var string
     */
    const BASE_URL = 'https://www.google.com/recaptcha/api/siteverify';


    /**
     * @var string
     */
    protected $secret;

    /**
     * @var ReCaptchaResponse | null
     */
    protected $lastResponse;


    /**
     * ReCaptchaVerifier constructor.
     *
     * @param string $secret the shared key between your site and reCAPTCHA
     */
    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }


    /**
     * @return ReCaptchaResponse | null
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Adapter
     */
    public function getLogger(): Adapter
    {
        return $this->di->get('logger');
    }


    /**
     * @inheritdoc
     */
    public function verify(string $response = null, string $remoteIp = null): bool
    {
        if (! $this->getConfig()->recaptcha->enabled) {

            // Disabled by configuration
            $this->lastResponse = ReCaptchaResponse::success();
            $this->getLogger()->debug('Validation: Note that reCAPTCHA verification is disabled by configuration.');

        } else if (empty($response)) {

            // Don't waste time with an responses
            $this->lastResponse = ReCaptchaResponse::failed();
            $this->getLogger()->debug('Validation: Skipped reCAPTCHA verification for empty response.');

        } else {

            // Query API
            $data = [
                'secret' => $this->secret,
                'response' => $response
            ];

            if (null !== $remoteIp) {
                $data['remoteip'] = $remoteIp;
            }

            $this->lastResponse = $this->post($data);
            $this->getLogger()->debug(sprintf('Validation: Verified reCAPTCHA response: %s.', json_encode($this->lastResponse, \JSON_PRETTY_PRINT)));
        }

        return $this->lastResponse->success;
    }


    /**
     * POSTs data to the reCAPTCHA API.
     *
     * The response is a JSON object, for reCAPTCHA V2 and invisible reCAPTCHA:
     *
     *    {
     *       "success": true|false,
     *       "challenge_ts": timestamp,  // timestamp of the challenge load
     *                                   // (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
     *       "hostname": string,         // the hostname of the site where the
     *                                   // reCAPTCHA was solved
     *       "error-codes": [...]        // optional
     *    }
     *
     * @param array $data
     *
     * @return ReCaptchaResponse
     *
     * @throws CaptchaException
     */
    protected function post(array $data = []): ReCaptchaResponse
    {
        // Use key 'http' even if you send the request to https
        $options = [
            'http' => [
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            ],
        ];

        if (false === ($context = stream_context_create($options))) {
            throw new CaptchaException('Failed to create streaming context.');
        }

        if (false === ($result = @file_get_contents(self::BASE_URL, false, $context))) {
            throw new CaptchaException('Failed to fetch response.');
        }

        if (empty($result)) {
            return ReCaptchaResponse::success();
        }

        try {
            $result = json_decode($result);
        } catch (\Throwable $t) {
            throw new CaptchaException('Failed to decode JSON response.');
        }

        return new ReCaptchaResponse(
            $result->success,
            $result->challenge_ts ?? null,
            $result->hostname ?? null,
            $result->errorCodes ?? []
        );
    }
}