<?php

namespace App\Captcha;

/**
 * @see https://developers.google.com/recaptcha/docs/verify
 */
final class ReCaptchaError
{

    /**
     * Empty response.
     *
     * @var string
     */
    const EMPTY_RESPONSE = 'empty-response';

    /**
     * The secret parameter is missing.
     *
     * @var string
     */
    const MISSING_INPUT_SECRET = 'missing-input-secret';

    /**
     * The secret parameter is invalid or malformed..
     *
     * @var string
     */
    const INVALID_INPUT_SECRET = 'invalid-input-secret';

    /**
     * The response parameter is missing.
     *
     * @var string
     */
    const MISSING_INPUT_RESPONSE = 'missing-input-response';

    /**
     * The response parameter is invalid or malformed.
     *
     * @var string
     */
    const INVALID_INPUT_RESPONSE = 'invalid-input-response';

    /**
     * The request is invalid or malformed.
     *
     * @var string
     */
    const BAD_REQUEST = 'bad-request';
}