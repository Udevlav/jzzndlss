<?php

namespace App\Captcha;

interface CaptchaVerifier
{

    /**
     * Verifies a user response to a captcha's challenge.
     *
     * @param string $response the user response token to the captcha challenge
     * @param string | null $remoteIp the user's IP address
     *
     * @return bool
     */
    public function verify(string $response, string $remoteIp = null): bool;
}