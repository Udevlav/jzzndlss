<?php

namespace App\Captcha;

class ReCaptchaException extends \RuntimeException
{

    /**
     * @var ReCaptchaResponse
     */
    protected $response;

    public function __construct(ReCaptchaResponse $response)
    {
        parent::__construct(sprintf('Failed to verify reCAPTCHA response: %s.', implode(', ', $response->getCauses())));

        $this->response = $response;
    }
}