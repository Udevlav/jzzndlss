<?php

namespace App\Captcha;

/**
 * @see https://developers.google.com/recaptcha/docs/verify
 */
class ReCaptchaResponse
{

    /**
     * Factory method.
     *
     * @return ReCaptchaResponse
     */
    static public function success(): ReCaptchaResponse
    {
        return new static(true, null, null, []);
    }

    /**
     * Factory method.
     *
     * @return ReCaptchaResponse
     */
    static public function failed(): ReCaptchaResponse
    {
        $instance = new static(false, null, null, [ReCaptchaError::EMPTY_RESPONSE]);
        $instance->isEmpty = true;

        return $instance;
    }


    /**
     * @var bool
     */
    public $success;

    /**
     * @var string
     */
    public $challengeTs;

    /**
     * @var string
     */
    public $hostname;

    /**
     * @var string[]
     */
    public $errorCodes;

    /**
     * @var bool
     */
    public $isEmpty = false;


    public function __construct(bool $success, string $challengeTs = null, string $hostname = null, array $errorCodes = [])
    {
        $this->success = $success;
        $this->challengeTs = $challengeTs;
        $this->hostname = $hostname;
        $this->errorCodes = $errorCodes;
    }

    public function getCauses(): array
    {
        $causes = [];

        if (in_array(ReCaptchaError::MISSING_INPUT_SECRET, $this->errorCodes)) {
            $causes[] = 'missing input secret';
        }

        if (in_array(ReCaptchaError::INVALID_INPUT_SECRET, $this->errorCodes)) {
            $causes[] = 'invalid input secret';
        }

        if (in_array(ReCaptchaError::MISSING_INPUT_RESPONSE, $this->errorCodes)) {
            $causes[] = 'missing input response';
        }

        if (in_array(ReCaptchaError::INVALID_INPUT_RESPONSE, $this->errorCodes)) {
            $causes[] = 'invalid input response';
        }

        if (in_array(ReCaptchaError::BAD_REQUEST, $this->errorCodes)) {
            $causes[] = 'bad request';
        }

        return $causes;
    }
}