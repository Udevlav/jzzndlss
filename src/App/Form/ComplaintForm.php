<?php

namespace App\Form;

use App\Model\ComplaintCause;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ComplaintForm extends CaptchaForm
{

    protected $tags;

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(new Hidden('id'))
            ->add(new Hidden('adId'))
            ->add(
                (new Text('email', [
                    'minLength' => 6,
                    'maxLength' => 127,
                    'placeholder' => 'contact.placeholder.private_email',
                    'required' => true,
                    'icon' => 'mail_outline',
                ]))
                    ->setLabel('contact.label.email')
                    ->setFilters([
                        'string',
                        'trim',
                        'email',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 6,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            )
            ->add(
                (new Select('cause', ComplaintCause::constants(), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('complaint.label.cause')
            )
            ->add(
                (new TextArea('text', [
                    'minLength' => 250,
                    'maxLength' => 4096,
                    'placeholder' => 'contact.placeholder.text',
                    'required' => true,
                    'rows' => 6,
                ]))
                    ->setLabel('contact.label.text')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 250,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            );
    }

}