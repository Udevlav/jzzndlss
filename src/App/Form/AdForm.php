<?php

namespace App\Form;

use App\Model\Category;
use App\Model\Ethnicity;
use App\Model\Gid;
use App\Model\Occupation;
use App\Model\Origin;
use App\Model\Schooling;
use App\Model\Tag;
use App\Repository\TagRepository;
use App\Validation\ArrayLengthValidator;
use App\Validation\CallbackValidator;
use App\Validation\PosterValidator;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class AdForm extends CaptchaForm
{

    protected $tags;

    public function initialize($entity = null, $options = [])
    {
        $config = $this->di->get('config');
        $repository = $this->getTagRepository();

        parent::initialize();

        $this
            ->add(new Hidden('id'))
            ->add(new Hidden('advertiserId'))
            ->add(
                (new Select('category', Category::available(), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => true,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.category')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Text('title', [
                    'placeholder' => 'ad.placeholder.title',
                    'minLength' => 30,
                    'maxLength' => 127,
                    'required' => true,
                ]))
                    ->setLabel('ad.label.title')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.requered',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 30,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            )
            ->add(
                (new TextArea('text', [
                    'minLength' => 100,
                    'maxLength' => 4096,
                    'placeholder' => 'ad.placeholder.text',
                    'required' => true,
                    'rows' => 6,
                ]))
                    ->setLabel('ad.label.text')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 100,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            )
            ->add(
                (new Text('name', [
                    'minLength' => 3,
                    'maxLength' => 127,
                    'placeholder' => 'ad.placeholder.name',
                    'required' => true,
                ]))
                    ->setLabel('ad.label.name')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->setFilters([
                        'string',
                        'trim',
                    ])
                    ->addValidator(new StringLength([
                        'min' => 3,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            )
            ->add(
                (new Text('age', [
                    'placeholder' => 'ad.placeholder.age',
                    'required' => true,
                ]))
                    ->setLabel('ad.label.age')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Text('email', [
                    'minLength' => 10,
                    'maxLength' => 127,
                    'placeholder' => 'ad.placeholder.private_email',
                    'required' => true,
                ]))
                    ->setLabel('ad.label.email')
                    ->setFilters([
                        'string',
                        'trim',
                        'email',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 10,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            )
            ->add(
                (new Text('phone', [
                    'placeholder' => 'ad.placeholder.phone',
                    'minLength' => 9,
                    'maxLength' => 9,
                    'required' => true,
                ]))
                    ->setLabel('ad.label.phone')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 9,
                        'messageMinimum' => 'validation.too_short',
                    ]))
                    ->addValidator(new StringLength([
                        'max' => 15,
                        'messageMaximum' => 'validation.too_long',
                    ]))
                    ->addValidator(new CallbackValidator([
                        'callback' => function($phone) {
                            $phoneUtil = PhoneNumberUtil::getInstance();

                            try {
                                $normalized = $phoneUtil->parse($phone, "ES");
                            } catch (NumberParseException $e) {
                                return false;
                            }

                            return $phoneUtil->isValidNumber($normalized);
                        }
                    ]))
            )
            ->add(
                (new Text('twitter', [
                    'placeholder' => 'app.placeholder.optional',
                    'maxLength' => 127,
                    'required' => false,
                ]))
                    ->setLabel('ad.label.twitter')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Check('whatsapp', [
                    'placeholder' => 'ad.placeholder.whatsapp',
                    'required' => false,
                    'filled' => true,
                ]))
                    ->setLabel('ad.label.whatsapp')
            )
            ->add(
                (new Check('independent', [
                    'placeholder' => 'ad.placeholder.independent',
                    'required' => false,
                    'filled' => true,
                ]))
                    ->setLabel('ad.label.independent')
            )
            ->add(
                (new Text('agency', [
                    'placeholder' => 'app.placeholder.optional',
                    'maxLength' => 127,
                    'required' => false,
                ]))
                    ->setLabel('ad.label.agency')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Hidden('lat'))
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Hidden('lng'))
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Hidden('country'))
                    ->setLabel('ad.label.country')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Hidden('postalCode'))
                    ->setLabel('ad.label.postalCode')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Hidden('city'))
                    ->setLabel('ad.label.city')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Hidden('zone'))
                    ->setLabel('ad.label.zone')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Hidden('fee'))
                    ->setLabel('ad.label.fee')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Text('fees', [
                    'descriptionLabel' => 'ad.description.fees',
                    'help' => '',
                    'oneHourLabel' => 'ad.description.one_hour',
                    'priceLabel' => 'ad.placeholder.price_currency',
                    'required' => false,
                ]))
                    ->setLabel('ad.label.fees')
            )
            ->add(
                (new Check('cash', [
                    'placeholder' => 'ad.placeholder.cash',
                    'required' => false,
                    'filled' => true,
                ]))
                    ->setLabel('ad.label.cash')
            )
            ->add(
                (new Check('card', [
                    'placeholder' => 'ad.placeholder.card',
                    'required' => false,
                    'filled' => true,
                ]))
                    ->setLabel('ad.label.card')
            )
            ->add(
                (new Text('availability', [
                    'help' => '',
                    'required' => false,
                    'fromLabel' => 'app.label.from',
                    'toLabel' => 'app.label.to',
                ]))
                    ->setLabel('ad.label.availability')
            )
            ->add(
                (new Select('origin', Origin::constants(), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.origin')
            )
            ->add(
                (new Select('ethnicity', Ethnicity::constants(), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => true,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.ethnicity')
            )
            ->add(
                (new Select('occupation', Occupation::constants(), [
                    'placeholder' => 'app.placeholder.optional',
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.occupation')
            )
            ->add(
                (new Select('schooling', Schooling::constants(), [
                    'placeholder' => 'app.placeholder.optional',
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.schooling')
            )
            ->add(
                (new Check('sms', [
                    'placeholder' => 'ad.placeholder.sms',
                    'required' => false,
                    'filled' => true,
                ]))
                    ->setLabel('ad.label.sms')
            )
            ->add(
                (new File('images', [
                    'extensionWhitelist' => $config->media->image->allowedExts,
                    'description' => 'ad.desc.images',
                    'help'        => 'ad.help.images',
                    'inputName'   => 'images',
                    'labelAdd'    => 'ad.link.add',
                    'labelMinItems' => 'ad.error.min_images',
                    'labelSelectableRequired' => 'ad.error.poster_required',
                    'maxFileSize' => $config->media->image->maxSize,
                    'maxFiles'    => $config->publication->maxImagesPerAd,
                    'minFileSize' => $config->media->image->minSize,
                    'minFiles'    => 1,
                    'mediaTypeWhitelist' => $config->media->image->allowedTypes,
                    'multiple'    => true,
                    'required'    => false,
                    'selectable'  => true,
                ]))
                    ->setLabel('ad.label.images')
                    ->addValidator(new ArrayLengthValidator([
                        'min' => $config->publication->minImagesPerAd,
                        'max' => $config->publication->maxImagesPerAd,
                    ]))
            )
            ->add(
                (new Hidden('poster'))
                    ->addValidator(new PosterValidator([
                        'message' => 'validation.poster_required',
                    ]))
            )
            ->add(
                (new File('videos', [
                    'extensionWhitelist' => $config->media->video->allowedExts,
                    'description' => 'ad.desc.videos',
                    'help' => 'ad.help.videos',
                    'inputName' => 'videos',
                    'labelAdd' => 'ad.link.add',
                    'labelMinItems' => 'ad.error.min_videos',
                    'maxFileSize' => $config->media->video->maxSize,
                    'maxFiles' => $config->publication->maxVideosPerAd,
                    'minFileSize' => $config->media->video->minSize,
                    'minFiles' => 0,
                    'mediaTypeWhitelist' => $config->media->video->allowedTypes,
                    'multiple' => true,
                    'required' => false,
                    'selectable' => false,
                ]))
                    ->setLabel('ad.label.videos')
                    ->addValidator(new ArrayLengthValidator([
                        'min' => 0,
                        'max' => $config->publication->maxVideosPerAd,
                    ]))
            )
            ->add(
                (new Select('tags1', $repository->optionsByGid(Gid::SERVICE_1), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.tags')
            )
            ->add(
                (new Select('tags2', $repository->optionsByGid(Gid::SERVICE_2), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.tags')
            )
            ->add(
                (new Select('tags3', $repository->optionsByGid(Gid::SERVICE_3), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.tags')
            )
            ->add(
                (new Select('tags4', $repository->optionsByGid(Gid::SERVICE_4), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.tags')
            )
            ->add(
                (new Select('tags5', $repository->optionsByGid(Gid::SERVICE_5), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => false,
                    'useEmpty' => true,
                ]))
                    ->setLabel('ad.label.tags')
            );
    }


    protected function getTagRepository(): TagRepository
    {
        return $this->di->get('em')->getRepository(Tag::class);
    }


    public function notifySms()
    {
        return (bool) $this->get('sms')->getValue();
    }

    public function getPosterId()
    {
        return $this->get('poster')->getValue();
    }

    public function getImageIds(): array
    {
        return array_filter($this->get('images')->getValue() ?? [], function($imageId) {
            return ! empty($imageId);
        });
    }

    public function getVideoIds(): array
    {
        return array_filter($this->get('videos')->getValue() ?? [], function($videoId) {
            return ! empty($videoId);
        });
    }

    public function getTagIds(): array
    {
        return array_filter(array_unique(array_merge(
            $this->get('tags1')->getValue() ?? [],
            $this->get('tags2')->getValue() ?? [],
            $this->get('tags3')->getValue() ?? [],
            $this->get('tags4')->getValue() ?? [],
            $this->get('tags5')->getValue() ?? []
        )), function($tagId) {
            return ! empty($tagId);
        });
    }


    public function getNumImages(): int
    {
        return count($this->getImageIds());
    }

    public function getNumVideos(): int
    {
        return count($this->getVideoIds());
    }

}
