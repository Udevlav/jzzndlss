<?php

namespace App\Form;

use Phalcon\Forms\Form;
use Phalcon\Validation\Message;

final class FormUtil
{

    static public function errors(Form $form): array
    {
        $messages = [];

        foreach ($form->getMessages() as $message) {
            /** @var Message $message */
            $messages[] = sprintf('%s %s (%s): %s',
                $message->getType(),
                $message->getField(),
                $message->getCode(),
                $message->getMessage()
            );
        }

        return $messages;
    }

    static public function errorsAsString(Form $form): string
    {
        return implode(', ', static::errors($form));
    }

}