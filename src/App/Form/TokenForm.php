<?php

namespace App\Form;

use Phalcon\Forms\Element\Check;

class TokenForm extends CaptchaForm
{

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this->add(
            (new Check('notifyBySms', ['required' => false ]))
                ->setLabel('advertiser.label.notify_by_sms')
        );
    }

    public function notifyBySms()
    {
        return (bool) $this->get('notifyBySms')->getValue();
    }
}