<?php

namespace App\Form;

use Phalcon\Forms\Element\Text;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class OnlineAdvertiserForm extends CaptchaForm
{

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(
                (new Text('email', [
                    'minLength' => 10,
                    'maxLength' => 127,
                    'placeholder' => 'online.placeholder.email',
                    'required' => true,
                ]))
                    ->setLabel('online.label.email')
                    ->setFilters([
                        'string',
                        'trim',
                        'email',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 5,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            );
    }

}