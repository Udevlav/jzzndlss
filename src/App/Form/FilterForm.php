<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;

class FilterForm extends CsrfForm
{

    protected $tags;

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add((new Hidden('l',           [ 'required' => false, 'label' => 'filter.label.l' ])))
            ->add((new Hidden('q',           [ 'required' => false, 'label' => 'filter.label.q' ])))
            ->add((new Hidden('limit',       [ 'required' => false, 'label' => 'filter.label.limit' ])))
            ->add((new Hidden('offset',      [ 'required' => false, 'label' => 'filter.label.offset' ])))
            ->add((new Hidden('category',    [ 'required' => false, 'label' => 'filter.label.category' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('lat',         [ 'required' => false, 'label' => 'filter.label.lat' ])))
            ->add((new Hidden('lng',         [ 'required' => false, 'label' => 'filter.label.lng' ])))
            ->add((new Hidden('city',        [ 'required' => false, 'label' => 'filter.label.city' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('province',    [ 'required' => false, 'label' => 'filter.label.province' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('anyProvince', [ 'required' => false, 'label' => 'filter.label.any_province' ])))
            ->add((new Hidden('postalCode',  [ 'required' => false, 'label' => 'filter.label.postal_code' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('zip',         [ 'required' => false, 'label' => 'filter.label.zip' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('ageFrom',     [ 'required' => false, 'label' => 'filter.label.age_from' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('ageTo',       [ 'required' => false, 'label' => 'filter.label.age_to' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('feeFrom',     [ 'required' => false, 'label' => 'filter.label.fee_from' ]))->setFilters(['string', 'trim']))
            ->add((new Hidden('feeTo',       [ 'required' => false, 'label' => 'filter.label.fee_to' ]))->setFilters(['string', 'trim']))
            ->add((new Select('tags',        [ 'required' => false, 'label' => 'filter.label.tags', 'useEmpty' => true ])))
        ;
    }

}