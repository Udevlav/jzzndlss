<?php

namespace App\Form;

use App\Model\Product;
use App\Promotion\Request;
use App\Repository\ProductRepository;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;

class PromoteForm extends CsrfForm
{

    protected $tags;

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(new Hidden('id'))
            ->add(
                (new Select('productId', $this->getActiveProductOptions(), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => true,
                    'useEmpty' => true,
                ]))
                    ->setLabel('promote.label.product')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Select('when', $this->getWhenOptions(), [
                    'emptyText' => '',
                    'emptyValue' => '',
                    'required' => true,
                    'useEmpty' => true,
                ]))
                    ->setLabel('payment.label.method')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Text('startDate', [
                    'minLength' => 0,
                    'maxLength' => 13,
                    'placeholder' => 'promote.placeholder.start_date',
                    'required' => true,
                ]))
                    ->setLabel('promote.label.start_date')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Text('startTime', [
                    'minLength' => 5,
                    'maxLength' => 5,
                    'placeholder' => 'promote.placeholder.start_time',
                    'required' => true,
                ]))
                    ->setLabel('promote.label.start_time')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Text('endTime', [
                    'minLength' => 5,
                    'maxLength' => 5,
                    'placeholder' => 'promote.placeholder.end_time',
                    'required' => true,
                ]))
                    ->setLabel('promote.label.end_time')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
            )
            ->add(
                (new Check('fullDay', [
                    'placeholder' => 'promote.placeholder.full_day',
                    'required' => false,
                ]))
                    ->setLabel('promote.label.full_day')
            );
    }

    public function createRequest(): Request
    {
        $request = new Request();
        $request->when      = strval($this->get('when')->getValue());
        $request->startDate = strval($this->get('startDate')->getValue());
        $request->startTime = strval($this->get('startTime')->getValue());
        $request->endTime   = strval($this->get('endTime')->getValue());
        $request->fullDay   = (bool) $this->get('fullDay')->getValue();
        $request->productId =        $this->get('productId')->getValue();

        return $request;
    }

    protected function getProductRepository(): ProductRepository
    {
        // Note the repository does use cache
        return $this->di->get('em')->getRepository(Product::class);
    }

    protected function getActiveProductOptions()
    {
        $products = [];

        foreach ($this->getProductRepository()->findAllActive() as $product) {
            $products[$product->id] = $product->label;
        }

        return $products;
    }

    protected function getWhenOptions(): array
    {
        return [
            'now',
            'auto',
        ];
    }
}