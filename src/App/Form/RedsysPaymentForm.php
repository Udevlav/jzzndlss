<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Form;

class RedsysPaymentForm extends Form
{

    public function initialize($entity = null, $options = [])
    {
        $this
            ->add(new Hidden('signatureVersion'))
            ->add(new Hidden('merchantParameters'))
            ->add(new Hidden('signature'))
        ;
    }
}
