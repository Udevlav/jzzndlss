<?php

namespace App\Form;

use Phalcon\Forms\Element\Text;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class OnlinePeerForm extends CaptchaForm
{

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(
                (new Text('displayName', [
                    'minLength' => 3,
                    'maxLength' => 127,
                    'placeholder' => 'online.placeholder.displayName',
                    'required' => true,
                ]))
                    ->setLabel('online.label.displayName')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 3,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            );
    }

}