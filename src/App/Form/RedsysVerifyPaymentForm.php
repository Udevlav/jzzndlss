<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Form;

class RedsysVerifyPaymentForm extends Form
{

    public function initialize($entity = null, $options = [])
    {
        $this
            ->add(new Hidden('amount'))
            ->add(new Hidden('currency'))
            ->add(new Hidden('environment'))
            //->add(new Hidden('identifier'))
            ->add(new Hidden('key'))
            ->add(new Hidden('merchantCode'))
            ->add(new Hidden('merchantData'))
            ->add(new Hidden('merchantName'))
            ->add(new Hidden('merchantURL'))
            ->add(new Hidden('order'))
            ->add(new Hidden('signature'))
            ->add(new Hidden('signatureVersion'))
            ->add(new Hidden('terminal'))
            ->add(new Hidden('titular'))
            ->add(new Hidden('transactionType'))
            ->add(new Hidden('urlKO'))
            ->add(new Hidden('urlOK'))
        ;
    }
}
