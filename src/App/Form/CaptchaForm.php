<?php

namespace App\Form;

use App\Validation\ReCaptchaValidator;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\ElementInterface;

abstract class CaptchaForm extends CsrfForm
{

    /**
     * @param string $entity
     * @param array $options
     *
     * @return void
     */
    public function initialize($entity = null, $options = [])
    {
        parent::initialize($entity, $options);

        $this->add(
            (new Hidden('g-recaptcha-response'))
                ->addValidator(new ReCaptchaValidator())
        );
    }

    /**
     * @return ElementInterface
     */
    public function getCaptchaField(): ElementInterface
    {
        return $this->get('g-recaptcha-response');
    }

    /**
     * @return string
     */
    public function getCaptchaResponse(): string
    {
        return $this->getCaptchaField()->getValue();
    }

}