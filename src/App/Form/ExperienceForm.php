<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ExperienceForm extends CaptchaForm
{

    protected $tags;

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(new Hidden('id'))
            ->add(new Hidden('adId'))
            ->add(new Hidden('rating'))
            ->add(
                (new Text('author', [
                    'minLength' => 3,
                    'maxLength' => 32,
                    'placeholder' => 'experience.placeholder.author',
                    'required' => true,
                ]))
                    ->setLabel('experience.label.author')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 3,
                        'max' => 32,
                        'messageMinimum' => 'validation.too_short',
                        'messageMaximum' => 'validation.too_long',
                    ]))
            )
            ->add(
                (new TextArea('text', [
                    'minLength' => 20,
                    'maxLength' => 1024,
                    'placeholder' => 'experience.placeholder.text',
                    'required' => true,
                    'rows' => 6,
                ]))
                    ->setLabel('experience.label.text')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 20,
                        'max' => 1024,
                        'messageMinimum' => 'validation.too_short',
                        'messageMaximum' => 'validation.too_long',
                    ]))
            );
    }

}
