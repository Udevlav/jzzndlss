<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Form;

abstract class CsrfForm extends Form
{
    /**
     * @param string $entity
     * @param array $options
     *
     * @return void
     */
    public function initialize($entity = null, $options = [])
    {
        $this->add(new Hidden('_csrf'));
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->_elements;
    }

    /**
     * @return string
     */
    public function getCsrfToken(): string
    {
        return $this->security->getToken();
    }
}