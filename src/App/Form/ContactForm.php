<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ContactForm extends CaptchaForm
{

    protected $tags;

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(new Hidden('id'))
            ->add(
                (new Text('email', [
                    'minLength' => 10,
                    'maxLength' => 127,
                    'placeholder' => 'contact.placeholder.private_email',
                    'required' => true,
                    'icon' => 'mail_outline',
                ]))
                    ->setLabel('contact.label.email')
                    ->setFilters([
                        'string',
                        'trim',
                        'email',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 8,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            )
            ->add(
                (new TextArea('text', [
                    'minLength' => 25,
                    'maxLength' => 4096,
                    'placeholder' => 'contact.placeholder.text',
                    'required' => true,
                    'icon' => 'mode_edit',
                    'rows' => 6,
                ]))
                    ->setLabel('contact.label.text')
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 25,
                        'messageMinimum' => 'validation.too_short',
                    ]))
            );
    }

}