<?php

namespace App\Form;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Text;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ChatForm extends CsrfForm
{

    public function initialize($entity = null, $options = [])
    {
        parent::initialize();

        $this
            ->add(
                (new Hidden('peerSid'))
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Hidden('advertiserSid'))
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
            )
            ->add(
                (new Text('text', [
                    'minLength' => 1,
                    'maxLength' => 127,
                    'placeholder' => 'chat.placeholder.text',
                    'required' => true,
                ]))
                    ->setLabel('chat.label.text')
                    ->setFilters([
                        'string',
                        'trim',
                    ])
                    ->addValidator(new PresenceOf([
                        'message' => 'validation.required',
                    ]))
                    ->addValidator(new StringLength([
                        'min' => 1,
                        'max' => 127,
                        'messageMaximum' => 'validation.too_long',
                        'messageMinimum' => 'validation.too_long',
                    ]))
            );
    }

}