<?php

namespace App\I18n;

class CorruptDictionaryException extends \RuntimeException
{

    static public function path(string $path, \Throwable $t = null): CorruptDictionaryException
    {
        return new static(sprintf('Translation dictionary "%s" is corrupt: %s.', $path, $t->getMessage()), $t->getCode(), $t);
    }
}