<?php

namespace App\I18n\Loader;

use App\I18n\CorruptDictionaryException;

class JsonTranslationLoader extends FileTranslationLoader
{
    public function doLoad(string $path): array
    {
        try {
            return @json_decode(@file_get_contents($path), \JSON_OBJECT_AS_ARRAY);
        } catch (\Throwable $t) {
            throw CorruptDictionaryException::path($path, $t);
        }
    }
}