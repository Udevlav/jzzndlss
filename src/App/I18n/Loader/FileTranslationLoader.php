<?php

namespace App\I18n\Loader;

use App\I18n\DictionaryNotFoundException;
use App\I18n\TranslationLoader;

abstract class FileTranslationLoader implements TranslationLoader
{
    private $path;
    private $fallbackLocale;

    public function __construct(string $path, string $fallbackLocale = 'es')
    {
        $this->path = $path;
        $this->fallbackLocale = $this->lang($fallbackLocale);
    }

    public function load(string $locale): array
    {
        $path = sprintf($this->path, $this->lang($locale));

        if (! is_file($path)) {
            $path = sprintf($this->path, $this->fallbackLocale);
        }

        if (! is_file($path)) {
            throw DictionaryNotFoundException::path($path);
        }

        if (! is_readable($path)) {
            throw DictionaryNotFoundException::notReadable($path);
        }

        return $this->doLoad($path);
    }

    abstract protected function doLoad(string $path): array;

    protected function lang(string $locale): string
    {
        if (empty($locale)) {
            return $locale;
        }

        $parts = explode('_', strtolower($locale));

        return $parts[0];
    }
}