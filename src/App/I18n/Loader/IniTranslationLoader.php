<?php

namespace App\I18n\Loader;

use App\I18n\CorruptDictionaryException;

class IniTranslationLoader extends FileTranslationLoader
{
    public function doLoad(string $path): array
    {
        try {
            return @parse_ini_file($path, false);
        } catch (\Throwable $t) {
            throw CorruptDictionaryException::path($path);
        }
    }
}