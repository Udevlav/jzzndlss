<?php

namespace App\I18n\Loader;

use App\I18n\TranslationLoader;

use Phalcon\Db\AdapterInterface;

class DbTranslationLoader implements TranslationLoader
{
    private $db;

    public function __construct(AdapterInterface $db)
    {
        $this->db = $db;
    }

    public function load(string $locale): array
    {
    }
}