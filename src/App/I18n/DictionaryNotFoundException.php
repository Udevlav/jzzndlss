<?php

namespace App\I18n;

class DictionaryNotFoundException extends \RuntimeException
{

    static public function path(string $path): DictionaryNotFoundException
    {
        return new static(sprintf('Translation dictionary "%s" not found.', $path));
    }

    static public function notReadable(string $path): DictionaryNotFoundException
    {
        return new static(sprintf('Translation dictionary "%s" is not readable.', $path));
    }
}