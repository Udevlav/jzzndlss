<?php

namespace App\I18n;

use Phalcon\Mvc\User\Component;

class Translator extends Component
{

    private $loader;
    private $locale;
    private $dictionary;

    public function __construct(TranslationLoader $loader)
    {
        $this->loader = $loader;
    }


    /**
     * @return string the locale
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale the locale
     *
     * @return self chainable
     */
    public function setLocale(string $locale): Translator
    {
        $this->locale = $locale;
        $this->dictionary = $this->loader->load($locale);

        return $this;
    }

    /**
     * @param string $token the token to translate
     * @param mixed ... $arguments the token arguments
     *
     * @return string
     */
    public function translate($token, ... $arguments): string
    {
        if (! is_string($token) || empty($token)) {
            return '';
        }

        if (! isset($this->dictionary[$token])) {
            return $token;
        }

        return sprintf($this->dictionary[$token], ... $arguments);
    }
}