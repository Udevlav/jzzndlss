<?php

namespace App\I18n;

interface TranslationLoader
{

    /**
     * @param string $locale the locale
     *
     * @return array the dictionary
     */
    public function load(string $locale): array;
}