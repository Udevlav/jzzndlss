<?php

namespace App\Config;

use Phalcon\Config as BaseConfig;

class Config extends BaseConfig
{

    public $app;
    public $analytics;
    public $db;
    public $cache;
    public $cdn;
    public $chat;
    public $cli;
    public $developer;
    public $ffmpeg;
    public $ffprobe;
    public $geo;
    public $geocoder;
    public $i18n;
    public $layout;
    public $locale;
    public $logging;
    public $mailbox;
    public $mailer;
    public $map;
    public $media;
    public $paypal;
    public $pos;
    public $publication;
    public $promotion;
    public $qtfaststart;
    public $rabbitmq;
    public $recaptcha;
    public $redsys;
    public $robots;
    public $server;
    public $sex;
    public $sms;


    public function __construct(array $arrayConfig = null)
    {
        parent::__construct($arrayConfig);
    }

    /**
     * @return string
     */
    public function getEnv(): string
    {
        return $this->app->env;
    }

    /**
     * @return bool
     */
    public function isDev(): bool
    {
        return 'dev' === strtolower($this->app->env);
    }

    /**
     * @return string
     */
    public function baseUrl(): string
    {
        return sprintf('%s://%s%s/',
            $this->server->protocol,
            $this->server->authority,
            $this->server->port ? ':'. $this->server->port : ''
        );
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function asset(string $path = ''): string
    {
        return '/asset/'. str_replace($this->layout->publicDir, '', $path);
    }
}
