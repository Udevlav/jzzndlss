<?php

namespace App\Util;

use Phalcon\Mvc\Router\RouteInterface;

class RouteUtil
{
    static public function generate(RouteInterface $route, ... $parameters): string
    {
        $segments = explode('/', $route->getPattern());

        foreach ($segments as & $segment) {
            if (false !== strpos($segment, ':')) {
                if (count($parameters) > 0) {
                    $segment = array_shift($parameters) ?? '?';
                } else {
                    throw new \RuntimeException(sprintf('Route "%s" requires parameter "%s".', $token, $segment));
                }
            }
        }

        return implode('/', $segments);
    }
}