<?php

namespace App\Util;

class StringUtil
{

    static public function underscore(string $token): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $token));
    }

    // Vintage implementation from Symfony 1's Jobeet tutorial
    static public function sluggify(string $token): string
    {
        $slug = str_replace(['á', 'é', 'í', 'ó', 'ú'], ['a', 'e', 'i', 'o', 'u'], strtolower($token));

        // replace non letter or digits by -
        $slug = preg_replace('~[^\pL\d]+~u', '-', $slug);

        // transliterate
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);

        // remove unwanted characters
        $slug = preg_replace('~[^-\w]+~', '', $slug);

        // trim
        $slug = trim($slug, '-');

        // remove duplicate -
        $slug = preg_replace('~-+~', '-', $slug);

        // lowercase
        $slug = strtolower($slug);

        return empty($slug) ? false : $slug;
    }
}