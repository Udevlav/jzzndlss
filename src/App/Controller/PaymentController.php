<?php

namespace App\Controller;

use App\Form\FormUtil;
use App\Form\PromoteForm;
use App\Manager\PaymentModelManager;
use App\Manager\PaymentSuccessModelManager;
use App\Manager\PromotionModelManager;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Payment;
use App\Model\PaymentMethod;
use App\Model\Product;
use App\Model\Promotion;
use App\Payment\PaymentGateway;
use App\Promotion\Request;
use App\Repository\Repository;

use Phalcon\Validation\Message;
use Phalcon\Validation;

use Throwable;
use LogicException;

/**
 * A controller for all payments (rule them all?).
 */
class PaymentController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if (null === ($token = $this->dispatcher->getParam('token')) ||
            null === ($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            return $this->notFound();
        }

        if (! $advertiser->isTokenValid()) {
            $this->flash->error($this->translate('ad.label.token_expired'));

            // Forward to @manage
            $this->dispatcher->setParam('advertiser', $advertiser);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action' => 'index',
            ]);
        }

        if (empty($ad = $this->findAdBySlug($slug))) {
            return $this->notFound();
        }

        if ($this->request->isPost()) {
            return $this->check($advertiser, $ad);
        }

        throw new LogicException('L00');
    }

    // The following controller methods are called-back by the
    // payment gateway

    public function verifyAction()
    {
        return $this->verify(null);
    }

    public function failedAction()
    {
        return $this->verify(false);
    }

    public function succeedAction()
    {
        return $this->verify(true);
    }


    // POST
    protected function check(Advertiser $advertiser, Ad $ad)
    {
        $request = new Request();
        $form = new PromoteForm();

        if (! $form->isValid($this->request->getPost(), $request)) {
            $this->error(sprintf('Failed to validate promotion request data: %s.', FormUtil::errorsAsString($form)));
            $this->addValidationErrorsToFlash($form, 'payment.label.');

            $this->retry($advertiser, $ad, $form, $request);

        } else if (! ($product = $this->getProductRepository()->find($request->productId))) {
            /** @var Validation $validation */
            $validation = $form->getValidation();
            $validation->appendMessage(new Message('validation.invalid'));
            $this->addValidationErrorsToFlash($form, 'payment.label.');

            $this->retry($advertiser, $ad, $form, $request);

        } else {
            $this->pay($advertiser, $ad, $request, $product);
        }
    }

    // POST
    protected function retry(Advertiser $advertiser, Ad $ad, PromoteForm $form, Request $request)
    {
        $this->setDefaultViewVars();

        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('ad', $ad);
        $this->view->setVar('products', $this->getProducts());
        $this->view->setVar('request', $request);
        $this->view->setVar('form', $form);
        $this->view->render('promote', 'index');
    }

    // POST
    protected function pay(Advertiser $advertiser, Ad $ad, Request $request, Product $product)
    {
        $payment = new Payment();

        $this->info(sprintf('Commiting payment for advertiser %s, ad %s', $advertiser->id, $ad->id));

        $payment->adId = $ad->id;
        $payment->advertiserId = $advertiser->id;
        $payment->productId = $product->id;
        $payment->amount = $product->price;
        $payment->creditsGranted = $product->creditsGranted;
        $payment->request = json_encode($request);
        $payment->method = PaymentMethod::CARD;

        $this->debug(sprintf('Payment data is %s', json_encode($payment, \JSON_PRETTY_PRINT)));

        try {
            // 1. Create a new payment entity
            $this->save($payment);

            // 2. Commit transaction, returning an auto-send POST form
            $gateway = $this->getGateway($payment);

            $this->info('Saved payment, redirecting to payment gateway via POST form...');

            $this->setDefaultViewVars();
            $this->view->setVar('form', $gateway->createPaymentForm($payment, $product));
            $this->view->pick('payment/pay');

        } catch (Throwable $t) {
            $this->error(sprintf('Failed to commit payment: %s', $t->getMessage()));
            $this->dispatcher->setParam('throwable', $t);

            $this->internalServerError($t);
        }
    }

    protected function verify(bool $success = null)
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if (null === ($token = $this->dispatcher->getParam('orderNo'))) {
            return $this->badRequest();
        }

        if (empty($payment = $this->getPaymentRepository()->find($token))) {
            return $this->notFound();
        }

        if (null !== $payment->result) {
            return $this->alreadyCommitted($payment);
        }

        if (empty($advertiser = $this->getAdvertiserRepository()->find($payment->advertiserId))) {
            return $this->notFound();
        }

        if (empty($ad = $this->getAdRepository()->find($payment->adId))) {
            return $this->notFound();
        }

        if (empty($product = $this->getProductRepository()->find($payment->productId))) {
            return $this->notFound();
        }

        if ($this->request->isPost()) {
            $responseData = $this->request->getPost() ?? [];
        } else {
            $responseData = $this->request->getQuery() ?? [];
        }

        if (null === $success || ! $responseData) {
            $this->info(sprintf('Payment failed or no data to verify for %s', $advertiser->email));

            $this->setDefaultViewVars();

            $this->view->setVar('payment', $payment);
            $this->view->setVar('advertiser', $advertiser);
            $this->view->pick('payment/failed');

            return null;
        }

        $this->info(sprintf('Verifying payment for %s', $advertiser->email));

        $gateway = $this->getGateway($payment);

        // What if the payment gateway does not send it ??
        // Looks like in sandbox mode it does not
        if (isset($responseData['Ds_SignatureVersion']) &&
            isset($responseData['Ds_MerchantParameters']) &&
            isset($responseData['Ds_Signature'])) {
            try {
                $gateway->verify($payment, $responseData);
            } catch (Throwable $t) {
                $this->error(sprintf('Failed to verify payment: %s', $t->getMessage()));
                $this->dispatcher->setParam('throwable', $t);

                return $this->internalServerError();
            }
        }

        if (! $success) {
            $payment->creditsGranted = 0;
        }

        $this->info(sprintf('Updating payment data and adding advertiser credits (+%d)', $payment->creditsGranted));

        try {
            $this->update($payment, $advertiser);

        } catch (Throwable $t) {
            $this->error(sprintf('Failed to update payment: %s', $t->getMessage()));
            $this->dispatcher->setParam('throwable', $t);

            $this->internalServerError($t);
        }

        $request = json_decode($payment->request);
        $promotions = $this->getPromotions($request, $product);

        $this->info(sprintf('Promoting ad %d (%d promotions) ...', $ad->id, count($promotions)));
        $this->debug(sprintf('%s', json_encode($request, JSON_PRETTY_PRINT)));

        try {
            $this->promote($advertiser, $ad, $promotions, $product, $payment);
        } catch (Throwable $t) {
            $this->error(sprintf('Failed to promote ad: %s', $t->getMessage()));

            return $this->internalServerError($t);
        }

        $this->info(sprintf('Succeed, %d credits left.', $advertiser->credits));

        $this->setDefaultViewVars();

        $this->view->setVar('payment', $payment);
        $this->view->setVar('advertiser', $advertiser);

        if ($success) {
            $this->view->pick('payment/success');
        } else {
            $this->view->pick('payment/failed');
        }
    }

    protected function alreadyCommitted(Payment $payment)
    {
        $this->setDefaultViewVars();

        $this->view->setVar('payment', $payment);
        $this->view->pick('payment/alreadyCommitted');
    }

    protected function save(Payment $payment)
    {
        // Save fresh payment entity
        $manager = new PaymentModelManager($payment);
        $manager->save();
    }

    protected function update(Payment $payment, Advertiser $advertiser)
    {
        // Grant credits and save payment entity
        $manager = new PaymentSuccessModelManager($payment);
        $manager->setAdvertiser($advertiser);
        $manager->save();
    }

    protected function promote(Advertiser $advertiser, Ad $ad, array $promotions, Product $product, Payment $payment)
    {
        foreach ($promotions as $promotion) {
            /** @var Promotion $promotion */
            $promotion->productId = $product->id;
            $promotion->paymentId = $payment->id;

            $this->getPromotionModelManager($advertiser, $ad, $promotion)->save();
            $this->debug(sprintf(' %s %s - %s', "\xe2\x9c\x94", $promotion->validFrom, $promotion->validTo));
        }
    }


    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getPaymentRepository(): Repository
    {
        return $this->getRepository(Payment::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getProductRepository(): Repository
    {
        return $this->getRepository(Product::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getPromotionModelManager(Advertiser $advertiser, Ad $ad, Promotion $promotion): PromotionModelManager
    {
        $manager = new PromotionModelManager($promotion);
        $manager->setDI($this->di);
        $manager->setAdvertiser($advertiser);
        $manager->setAd($ad);

        return $manager;
    }

    protected function getPromotions($request, Product $product): array
    {
        return $this->getPromoter()->computePromotions($request, $product);
    }

    protected function getGateway(Payment $payment): PaymentGateway
    {
        $gateway = $this->getBroker()->createGateway($payment);
        $gateway->setDI($this->di);

        return $gateway;
    }

    protected function getProducts()
    {
        // Note we go through cache
        return $this->getRepository(Product::class)->findAll('active = 1');
    }

    protected function findAdBySlug(string $slug)
    {
        return $this->getAdRepository()->findOneBy(sprintf('slug = "%s"', $slug));
    }

    protected function findAdvertiserByToken(string $token)
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('token = "%s"', $token));
    }
}
