<?php

namespace App\Controller;

use App\Manager\MediaModelManager;
use App\Manager\ModelManager;
use App\Manager\ValidationError;
use App\Model\Media;

use App\Repository\MediaRepository;
use Phalcon\Http\Request\File;
use Phalcon\Mvc\ModelInterface;

use Throwable;

/**
 * All ingestion tasks should be delegated to this controller. This just
 * validates the uploaded file and moves it to an appropriate, temporary
 * place.
 */
class IngestController extends BaseController
{

    /**
     * Media[]
     */
    protected $newMedia = [];

    /**
     * ValidationError[]
     */
    protected $validationErrors = [];


    public function indexAction()
    {
        $this->checkMediaDir();

        if (! $this->request->isAjax()) {
            return $this->json(400);
        }

        if (! $this->request->hasFiles()) {
            // Maybe the client just set the "Content-Type: multipart/form-data"
            // header, but the data is not actually valid multipart data; or
            // the form has not the enctype attribute, ...
            return $this->json(400);
        }

        if ($this->request->isPost() || $this->request->isPut()) {
            return $this->ingest();
        }

        return $this->json(405);
    }

    public function deleteAction()
    {
        if (! $this->request->isAjax()) {
            return $this->json(400);
        }

        if (! $this->request->isDelete()) {
            return $this->json(405);
        }

        if (null === ($id = $this->dispatcher->getParam('id'))) {
            return $this->json(400);
        }

        if (null === ($entity = $this->getMediaRepository()->find($id))) {
            return $this->json(404);
        }

        $this->delete($entity);

        return $this->json(204);
    }


    protected function checkMediaDir()
    {
        $path = $this->getConfig()->layout->mediaDir;

        if (! is_writable($path)) {
            $this->critical(sprintf('Media dir "%s" is not writable.', $path));
        }
    }

    protected function getModelManager(ModelInterface $model): ModelManager
    {
        $manager = new MediaModelManager($model);
        $manager->setDI($this->di);
        $manager->setGeoLocation($this->getGeoLocation());

        return $manager;
    }


    protected function ingest()
    {
        foreach ($this->request->getUploadedFiles() as $file) {
            /** @var File $file */
            if (UPLOAD_ERR_OK !== $file->getError()) {
                return $this->json(400, [
                    'code' => $file->getError(),
                    'message' => $this->getUploadErrorMessage($file),
                ]);
            }

            if ($this->isValid($file)) {
                try {
                    $this->persist($file);
                } catch (Throwable $t) {
                    $this->error(sprintf('Failed to create new media: %s', $t->getMessage()));

                    return $this->json(500, [
                        'message' => $t->getMessage(),
                    ]);
                }

                $this->info(sprintf('Successfully ingested "%s" to "%s"', $file->getName(), $this->newMedia[count($this->newMedia) - 1]->path));

            } else {
                $this->error('Bad request contains invalid media');
            }
        }

        if (count($this->validationErrors)) {
            return $this->json(400, [
                'errors' => $this->validationErrors,
            ]);
        }

        $this->info('Success');

        return $this->json(201, [
            'media' => $this->newMedia,
        ]);
    }

    protected function persist(File $file)
    {
        /** @var MediaModelManager $manager */
        $manager = $this->getModelManager(new Media());
        $manager->setFile($file);

        try {
            $manager->save();
        } catch (Throwable $t) {
            $this->error(sprintf('Persisting media: %s', $t->getMessage()));
            $this->json(500, [
                'message' => $t->getMessage(),
            ]);
        }

        $this->newMedia[] = $manager->getModel();
    }

    protected function delete(Media $entity)
    {
        $manager = $this->getModelManager($entity);

        try {
            $manager->delete();
        } catch (Throwable $t) {
            $this->error(sprintf('Deleting media %d: %s.', $entity->id, $t->getMessage()));
            $this->json(500, [
                'message' => $t->getMessage(),
            ]);
        }
    }


    protected function getUploadErrorMessage(File $file): string
    {
        switch ($file->getError()) {
            case UPLOAD_ERR_INI_SIZE:
                return sprintf('Size is too large (max. is %s)', ini_get('upload_max_filesize'));

            case UPLOAD_ERR_FORM_SIZE:
                return 'Size is too large';

            case UPLOAD_ERR_PARTIAL:
                return 'File was only partially uploaded';

            case UPLOAD_ERR_NO_FILE:
                return 'No file';

            case UPLOAD_ERR_NO_TMP_DIR:
                return 'No temp dir';

            case UPLOAD_ERR_CANT_WRITE:
                return 'Can\'t write upload dir';

            case UPLOAD_ERR_EXTENSION:
                return 'Invalid file extension';
        }

        return 'Unknown error';
    }

    protected function getMediaRepository(): MediaRepository
    {
        /** @var MediaRepository $repository */
        $repository = $this
            ->getRepository(Media::class)
            ->disableCache();

        return $repository;
    }

    protected function getMediaType(File $file)
    {
        static $ext;

        if (null === $ext) {
            $ext = [
                'gif' => 'image/gif',
                'jpg' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'mp4' => 'video/mp4',
                'png' => 'image/png',
            ];
        }

        $type = $file->getRealType() ?? $file->getType();

        if (empty($type)) {
            if (isset($ext[$file->getExtension()])) {
                $type = $ext[$file->getExtension()];
            } else {
                $type = 'application/octet-stream';
            }
        }

        return $type;
    }

    protected function isValid(File $file)
    {
        $config = $this->getConfig();

        $type = explode('/', $this->getMediaType($file), 2);
        $type = strtolower(array_shift($type));

        switch ($type) {
            case 'image':
                return $this->validateConstraints(
                    $file,
                    $config->media->image->minSize,
                    $config->media->image->maxSize,
                    $config->media->image->allowedTypes->toArray()
                );

            case 'video':
                return $this->validateConstraints(
                    $file,
                    $config->media->video->minSize,
                    $config->media->video->maxSize,
                    $config->media->video->allowedTypes->toArray()
                );
        }

        return false;
    }

    protected function validateConstraints(File $file, int $minSize, int $maxSize, array $allowedTypes)
    {
        $validationErrors = count($this->validationErrors);

        if ($file->getSize() < $minSize) {
            $this->addValidationError($file, 'validation.too_small', sprintf('The file size should be greater than %d.', $minSize));
        }

        if ($file->getSize() > $maxSize) {
            $this->addValidationError($file, 'validation.too_big', sprintf('The file size should be less than %d.', $maxSize));
        }

        $type = $file->getRealType() ?? $file->getType();

        if (! in_array($type, $allowedTypes)) {
            $this->addValidationError($file, 'validation.invalid_type', sprintf('Invalid resource type. Only %s types are allowed.', implode(', ', $allowedTypes)));
        }

        return count($this->validationErrors) === $validationErrors;
    }

    protected function addValidationError(File $file, string $error, string $message)
    {
        $validationError = new ValidationError();

        $validationError->path = $file->getName();
        $validationError->error = $error;
        $validationError->message = $message;

        $this->validationErrors[] = $validationError;
    }
}
