<?php

namespace App\Controller;

use App\Form\ChatForm;
use App\Form\FormUtil;
use App\Manager\ModelManager;
use App\Model\ChatMessage;
use App\Model\ChatMessageDirection;
use App\Model\OnlineAdvertiser;
use App\Repository\ChatMessageRepository;

use App\Repository\OnlineAdvertiserRepository;
use Phalcon\Mvc\Model\Resultset;

use DateInterval;
use DateTime;

/**
 * A controller to manage chatting.
 */
class ChatController extends BaseController
{
    const TS_PARAM = 'ts';

    public function indexAction()
    {
        if (! $this->request->isAjax() || ! $this->request->isGet()) {
            return $this->badRequest();
        }

        $session = $this->getSession();

        if (! $session->isStarted() || ! $session->getId()) {
            return $this->serviceUnavailable();
        }

        return $this->get($session->getId());
    }

    public function postAction()
    {
        if (! $this->request->isAjax() || ! $this->request->isPost()) {
            return $this->badRequest();
        }

        $session = $this->getSession();

        if (! $session->isStarted() || ! $session->getId()) {
            return $this->serviceUnavailable();
        }

        return $this->post($session->getId());
    }


    protected function get(string $sessionId)
    {
        $startingAt = new DateTime('now');

        if (null !== ($time = $this->request->getQuery('ts'))) {
            $startingAt->setTimestamp((int) $time);
        } else {
            $startingAt->sub(new DateInterval('PT1H'));
        }

        /** @var Resultset $messages */
        $messages = $this->getChatMessageRepository()->findStartingAt($startingAt, $sessionId);

        $this->info(sprintf('Found %s messages for sessionId = %s (starting at time %s).', $messages->count(), $sessionId, $startingAt->format('H:i:s')));

        if (0 === $messages->count()) {
            return $this->json(204, [
                'chats' => [],
            ]);
        }

        return $this->json(200, [
            'messages' => $messages->toArray(),
        ]);
    }

    protected function post(string $sessionId)
    {
        $entity = new ChatMessage();
        $form = new ChatForm();

        if (! $form->isValid($this->request->getPost(), $entity)) {
            $this->error(sprintf('Failed to validate chat message: %s.', FormUtil::errorsAsString($form)));

            $this->json(400, [
                'posted' => false,
                'reason' => 'invalid',
            ]);
        }

        $peerSessionId = null;

        if ($sessionId === $entity->peerSid) { // peer > advertiser
            $peerSessionId = $entity->advertiserSid;
            $entity->direction = ChatMessageDirection::PEER_TO_ADVERTISER;

        } else if ($sessionId === $entity->advertiserSid) { // advertiser > peer
            $peerSessionId = $entity->peerSid;
            $entity->direction = ChatMessageDirection::ADVERTISER_TO_PEER;

        } else {
            $this->error(sprintf('The current user sessionId (%s) does not match neither the peer (%s) nor the advertiser (%s) sessionId.',
                $sessionId,
                $entity->peerSid,
                $entity->advertiserSid
            ));

            return $this->json(400, [
                'posted' => false,
                'reason' => 'session_mismatch',
            ]);
        }

        /** @var OnlineAdvertiser $onlineAdvertiser */
        if (null === ($onlineAdvertiser = $this->findOnlineAdvertiser($entity->advertiserSid))) {
            $this->error(sprintf('Advertiser with sessionId %s is no longer online.', $entity->advertiserSid));

            return $this->json(412, [
                'posted' => false,
                'reason' => 'peer_disconnected',
            ]);
        }

        if (ChatMessageDirection::PEER_TO_ADVERTISER === $entity->direction) {
            $onlineAdvertiser->lastActivityAt = date('Y-m-d H:i:s');

            if (false === $onlineAdvertiser->save()) {
                $this->error(sprintf('Failed to update online advertiser: %s.', ModelManager::errorsAsString($onlineAdvertiser)));

                return $this->json(500, [
                    'posted' => false,
                    'reason' => 'update_error',
                ]);
            }
        }

        $entity->advertiserId = $onlineAdvertiser->advertiserId;
        $entity->text = $this->clean($entity->text);

        if (false === $entity->save()) {
            $this->error(sprintf('Failed to post chat message: %s.', ModelManager::errorsAsString($entity)));

            return $this->json(500, [
                'posted' => false,
                'reason' => 'write_error',
            ]);
        }

        $this->info(sprintf('Successfully posted chat message from %s to %s.', $sessionId, $peerSessionId));

        return $this->json(201, $entity->toArray());
    }


    protected function getChatMessageRepository(): ChatMessageRepository
    {
        /** @var ChatMessageRepository $repository */
        $repository = $this->getRepository(ChatMessage::class);

        return $repository;
    }

    protected function getOnlineAdvertiserRepository(): OnlineAdvertiserRepository
    {
        /** @var OnlineAdvertiserRepository $repository */
        $repository = $this->getRepository(OnlineAdvertiser::class);

        return $repository;
    }

    protected function findOnlineAdvertiser(string $sessionId)
    {
        /** @var Resultset $online */
        $online = $this->getOnlineAdvertiserRepository()->findBySessionId($sessionId);

        if (0 === $online->count()) {
            return null;
        }

        return $online->getFirst();
    }


    private function clean(string $text): string
    {
        return substr($text, 0, 127);
    }
}