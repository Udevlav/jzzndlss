<?php

namespace App\Controller;

use App\Finder\Results;
use App\Form\ChatForm;
use App\Form\ExperienceForm;
use App\Form\OnlineAdvertiserForm;
use App\Form\OnlinePeerForm;
use App\Model\Ad;
use App\Model\Experience;
use App\Model\Gid;
use App\Model\Media;
use App\Model\Tag;
use App\Repository\AdRepository;
use App\Repository\ExperienceRepository;
use App\Repository\MediaRepository;
use App\Repository\TagRepository;

use Phalcon\Mvc\Model\ResultsetInterface;

/**
 * A GET for a simple ad.
 */
class DetailController extends BaseController
{

    public function indexAction()
    {
        if (! $this->request->isGet()) {
            return $this->methodNotAllowed();
        }

        if (empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }

        return $this->display($entity);
    }

    public function seoAction()
    {
        if (! $this->request->isGet()) {
            return $this->methodNotAllowed();
        }

        if (empty($hash = $this->dispatcher->getParam('hash'))) {
            return $this->badRequest();
        }

        if (empty($result = $this->getAdRepository()->findOneByHashWithToken($hash))) {
            return $this->notFound();
        }

        try {
            $entity = $result[0]->ad;
            $token = $result[0]->token;
        } catch (\Throwable $e) {
            // The index does not exist in the cursor
            return $this->notFound($e);
        }

        return $this->display($entity, $token);
    }

    protected function display(Ad $entity, string $token = null)
    {
        $this->setDefaultViewVars();

        if (null !== $token) {
            $this->view->setVar('token', $token);
        } else {
            $this->view->setVar('token', sha1(''));
        }

        $this->view->setVar('entity', $entity);
        $this->view->setVar('images', $this->getImages($entity));
        $this->view->setVar('videos', $this->getVideos($entity));
        $this->view->setVar('experiences', $this->getExperiences($entity));

        $tags = $this->getTags($entity);

        $this->view->setVar('tags1', $tags[Gid::SERVICE_1] ?? []);
        $this->view->setVar('tags2', $tags[Gid::SERVICE_2] ?? []);
        $this->view->setVar('tags3', $tags[Gid::SERVICE_3] ?? []);
        $this->view->setVar('tags4', $tags[Gid::SERVICE_4] ?? []);
        $this->view->setVar('tags5', $tags[Gid::SERVICE_5] ?? []);

        $this->view->setVar('filters', $this->getFilters());
        $this->view->setVar('related', $this->getRelated($entity));

        $this->view->setVar('chatForm', new ChatForm());
        $this->view->setVar('experienceForm', new ExperienceForm());
        $this->view->setVar('onlinePeerForm', new OnlinePeerForm());
        $this->view->setVar('onlineAdvertiserForm', new OnlineAdvertiserForm());

        if ($entity->isPortrait()) {
            $this->view->pick('detail/portrait');
        }

        $this->view->pick('detail/index');
    }


    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)
            ->disableCache();

        return $repository;
    }

    /**
     * @return ExperienceRepository
     */
    protected function getExperienceRepository(): ExperienceRepository
    {
        /** @var ExperienceRepository $repository */
        $repository = $this->getRepository(Experience::class)
            ->disableCache();

        return $repository;
    }

    /**
     * @return MediaRepository
     */
    protected function getMediaRepository(): MediaRepository
    {
        /** @var MediaRepository $repository */
        $repository = $this->getRepository(Media::class)
            ->disableCache();

        return $repository;
    }

    /**
     * @return TagRepository
     */
    protected function getTagRepository(): TagRepository
    {
        /** @var TagRepository $repository */
        $repository = $this->getRepository(Tag::class);

        return $repository;
    }

    /**
     * @param Ad $entity
     *
     * @return ResultsetInterface | Media[]
     */
    protected function getImages(Ad $entity): ResultsetInterface
    {
        $x =$this->getMediaRepository()->findImagesFor($entity);

        return $x;
    }

    /**
     * @param Ad $entity
     *
     * @return ResultsetInterface | Media[]
     */
    protected function getVideos(Ad $entity): ResultsetInterface
    {
        return $this->getMediaRepository()->findVideosFor($entity);
    }

    /**
     * @param Ad $entity
     *
     * @return ResultsetInterface | Experience[]
     */
    protected function getExperiences(Ad $entity): ResultsetInterface
    {
        return $this->getExperienceRepository()->findByAd($entity, 10);
    }

    /**
     * @param Ad $entity
     *
     * @return ResultsetInterface | Tag[]
     */
    protected function getTags(Ad $entity)
    {
        return $this->getTagRepository()->groupByAd($entity);
    }

    /**
     * @param Ad $entity
     *
     * @return Results
     */
    protected function getRelated(Ad $entity): Results
    {
        $filters = $this->getFilters();
        $filters->setInclusive();
        $filters->set('city', $entity->city);
        $filters->set('postalCode', substr($entity->postalCode, 0, 2));
        $filters->set('category', $entity->category);

        return $this->getFinder()->find($filters);
    }
}
