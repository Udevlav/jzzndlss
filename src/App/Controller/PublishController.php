<?php

namespace App\Controller;

use App\Model\Ad;
use App\Model\Advertiser;
use App\Repository\Repository;

use Throwable;

/**
 * A controller to publish an ad.
 */
class PublishController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (null === ($token = $this->dispatcher->getParam('token')) ||
            null === ($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            return $this->notFound();
        }

        if (! $advertiser->isTokenValid()) {
            $this->flash->error('ad.label.token_expired');

            // Forward to manage
            $this->dispatcher->setParam('advertiser', $advertiser);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action' => 'index',
            ]);
        }

        if (empty($ad = $this->findAdBySlug($slug))) {
            return $this->notFound();
        }

        if ($this->request->isGet()) {
            return $this->publish($ad);
        }

        return $this->methodNotAllowed();
    }

    public function alreadyPublishedAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('entity', $this->dispatcher->getParam('entity'));
    }

    public function alreadyPublishingAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('entity', $this->dispatcher->getParam('entity'));
    }


    protected function publish(Ad $entity)
    {
        if (null !== $entity->publishedAt) {
            $this->warning(sprintf('Ad %d is already published. Abort.', $entity->id));

            $this->dispatcher->setParam('entity', $entity);
            $this->dispatcher->forward([
                'controller' => 'publish',
                'action' => 'alreadyPublished',
            ]);

        } else {
            $runner = $this->getRunner();
    
            if (file_exists($runner->getLockPath($entity))) {
                $this->warning(sprintf('Ad %d is already being published. Abort.', $entity->id));

                $this->dispatcher->setParam('entity', $entity);
                $this->dispatcher->forward([
                    'controller' => 'publish',
                    'action' => 'alreadyPublishing',
                ]);
            }

            $this->debug(sprintf('Spawning publish job for ad %d ...', $entity->id));

            try {
                $runner->run($entity);
            } catch (Throwable $t) {
                $this->error(sprintf('Failed to spawn publish job: %s', $t->getMessage()));

                return $this->internalServerError($t);
            }

            $this->setDefaultViewVars();

            $this->view->setVar('entity', $this->dispatcher->getParam('entity'));
        }
    }


    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function findAdBySlug(string $slug)
    {
        return $this->getAdRepository()->findOneBy(sprintf('slug = "%s"', $slug));
    }

    protected function findAdvertiserByToken(string $token)
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('token = "%s"', $token));
    }
}