<?php

namespace App\Controller;

use App\Form\ExperienceForm;
use App\Form\FormUtil;
use App\Manager\ExperienceModelManager;
use App\Model\Ad;
use App\Model\Experience;
use App\Repository\AdRepository;
use App\Repository\ExperienceRepository;

use DateTime;
use LogicException;
use Throwable;

/**
 * A controller to create and retrieve experiences.
 */
class ExperienceController extends BaseController
{

    const DEFAULT_LIMIT = 10;

    public function indexAction()
    {
        if (! $this->request->isAjax()|| empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }

        if ($this->request->isGet()) {
            return $this->getExperiences($entity);
        }

        if ($this->request->isPost()) {
            return $this->createExperience($entity);
        }

        throw new LogicException('L00');
    }


    protected function getExperiences(Ad $ad)
    {
        $limit = (int) ($this->request->getQuery('limit') ?? self::DEFAULT_LIMIT);
        $offset = (int) ($this->request->getQuery('offset') ?? 0);
        
        $experiences = $this->getExperienceRepository()->findByAd($ad, $limit, $offset);
        
        return $this->json(200, [
            'limit' => $limit,
            'offset' => $offset,
            'experiences' => $this->toArray($experiences),
        ]);
    }

    protected function createExperience(Ad $ad)
    {
        $entity = new Experience();
        $form = new ExperienceForm();

        if (!$form->isValid($this->request->getPost(), $entity)) {
            $this->error(sprintf('Failed to validate experience data: %s', FormUtil::errorsAsString($form)));

            return $this->json(400, [
                'errors' => $form->getMessages(),
            ]);
        }

        try {
            $manager = new ExperienceModelManager($entity);
            $manager->setAd($ad);
            $manager->save();

        } catch (Throwable $t) {
            $this->error(sprintf('Failed to create experience: %s', $t->getMessage()));
            $this->error(sprintf('%s', $t->getTraceAsString()));


            return $this->json(500);
        }

        $this->info('Experience created successfully');

        return $this->json(201, [
            'text' => $entity->text,
            'author' => $entity->author,
            'rating' => $entity->rating,
            'createdAt' => new DateTime(),
        ]);
    }


    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)->disableCache();

        return $repository;
    }

    /**
     * @return ExperienceRepository
     */
    protected function getExperienceRepository(): ExperienceRepository
    {
        /** @var ExperienceRepository $repository */
        $repository = $this->getRepository(Ad::class)->disableCache();

        return $repository;
    }
    
    protected function toArray($experiences): array
    {
        $results = [];
        
        foreach ($experiences as $experience) {
            $results[] = [
                'text' => $experience->text,
                'author' => $experience->author,
                'rating' => $experience->rating,
                'createdAt' => $experience->createdAt,
            ];
        }
        
        return $results;
    }
}
