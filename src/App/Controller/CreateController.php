<?php

namespace App\Controller;

use App\Form\AdForm;
use App\Form\FormUtil;
use App\Manager\AdModelManager;
use App\Manager\ModelManager;
use App\Manager\PersistenceException;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Media;

use Phalcon\Mvc\ModelInterface;

use LogicException;
use Throwable;

/**
 * All ad creation tasks should be delegated to this controller.
 */
class CreateController extends BaseController
{

    /**
     *
     */
    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if ($this->request->isGet()) {
            return $this->newAd();
        }

        if ($this->request->isPost()) {
            return $this->createAd();
        }

        throw new LogicException('L00');
    }


    protected function newAd()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('form', new AdForm());
        $this->view->setVar('entity', new Ad());
        $this->view->setVar('poster', []);
        $this->view->setVar('images', []);
        $this->view->setVar('videos', []);
        $this->view->setVar('tags', []);
    }


    protected function createAd()
    {
        $entity = new Ad();
        $form = new AdForm();

        if ($form->isValid($this->request->getPost(), $entity)) {
            try {
                $this->create($entity, $form);

            } catch (Throwable $t) {
                if ($t instanceof PersistenceException && false !== strpos($t->getMessage(), 'uniqueness')) {
                    return $this->validationError($entity, $form);
                }

                $this->error(sprintf('Failed to create ad: %s', $t->getMessage()));

                return $this->internalServerError($t);
            }

            $this->debug('Created ad '. json_encode($entity->toArray(), \JSON_PRETTY_PRINT));
            $this->info('Succeed, forward to @payment');

            $this->response->setStatusCode(201, 'Created');
            $this->flash->success($this->translate('ad.label.success'));
            $this->dispatcher->setParam('entity', $entity);
            $this->dispatcher->setParam('slug', $entity->slug);
            $this->dispatcher->setParam('token', $this->getAdvertiser($entity)->token);
            $this->dispatcher->setParam('dispatched', true);
            $this->dispatcher->forward([
                'controller' => 'promote',
                'action'     => 'index',
            ]);

        } else {
            return $this->validationError($entity, $form);
        }
    }

    /**
     * @param Ad $entity
     * @param AdForm $form
     */
    protected function validationError(Ad $entity, AdForm $form)
    {
        if (count(FormUtil::errors($form))) {
            $this->error(sprintf('Failed to validate ad data: %s', FormUtil::errorsAsString($form)));
            $this->setDefaultViewVars();
            $this->addValidationErrorsToFlash($form, 'ad.label.');
        } else if (count(ModelManager::errors($entity))) {
            $this->error(sprintf('Failed to persist ad data: %s', ModelManager::errorsAsString($entity)));
            $this->setDefaultViewVars();
            $this->addModelErrorsToFlash($entity, 'ad.label.');
        }

        $this->response->setStatusCode(400, 'Bad Request');

        $entity->unserializeColumns();

        $this->view->setVar('form', $form);
        $this->view->setVar('entity', $entity);
        $this->view->setVar('poster', $form->getPosterId());
        $this->view->setVar('images', $this->getMedia($form->getImageIds()));
        $this->view->setVar('videos', $this->getMedia($form->getVideoIds()));
        $this->view->setVar('tags', $form->getTagIds());
    }

    /**
     * @param Ad $entity
     * @param AdForm $form
     */
    protected function create(Ad $entity, AdForm $form)
    {
        $this->info(sprintf('Adding tags [%s]', implode(', ', $form->getTagIds())));
        $this->info(sprintf('Adding images [%s]', implode(', ', $form->getImageIds())));
        $this->info(sprintf('Adding videos [%s]', implode(', ', $form->getVideoIds())));

        /** @var AdModelManager $manager */
        $manager = $this->getModelManager($entity);
        $manager->setGeoLocation($this->getGeoLocation());
        $manager->setPosterId($form->getPosterId());
        $manager->setNotifyBySms($form->notifySms());

        foreach ($form->getTagIds() as $id) {
            $manager->addTag($id);
        }

        foreach ($form->getImageIds() as $id) {
            $manager->addMedia($id);
        }

        foreach ($form->getVideoIds() as $id) {
            $manager->addMedia($id, true);
        }

        $manager->save();
    }

    /**
     * @inheritdoc
     */
    protected function getModelManager(ModelInterface $model): ModelManager
    {
        $manager = new AdModelManager($model);
        $manager->setDI($this->di);

        return $manager;
    }

    /**
     * @param array $ids
     *
     * @return Media[]
     */
    protected function getMedia(array $ids)
    {
        $medias = [];

        if (count($ids)) {
            foreach ($this->getRepository(Media::class)->findIn($ids) as $media) {
                $medias[] = $media;
            }
        }

        return $medias;
    }

    /**
     * @param Ad $ad
     *
     * @return Advertiser
     */
    protected function getAdvertiser(Ad $ad)
    {
        return $this->getRepository(Advertiser::class)->find($ad->advertiserId);
    }

}
