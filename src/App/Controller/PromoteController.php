<?php

namespace App\Controller;

use App\Form\PromoteForm;
use App\Manager\AssignModelManager;
use App\Manager\PromotionModelManager;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Product;
use App\Model\Promotion;
use App\Promotion\Request;
use App\Repository\PromotionRepository;
use App\Repository\Repository;

use Phalcon\Mvc\Model\Resultset\Simple;
use Throwable;

/**
 * Spends a credit promoting an ad.
 */
class PromoteController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->dispatcher->getParam('dispatched')) {
            return $this->methodNotAllowed();
        }

        if (null === ($token = $this->dispatcher->getParam('token')) ||
            null === ($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            // FIXME El token puede haber caducado
            return $this->notFound();
        }

        /*
         * DO NOT require the user to re-generate its token,
         * as this action is free and everybody can call it
         *
        if (! $advertiser->isTokenValid()) {
            $this->flash->error($this->translate('ad.label.token_expired'));

            // Forward to @manage
            $this->dispatcher->setParam('advertiser', $advertiser);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action' => 'index',
            ]);
        }
        */

        if (empty($ad = $this->findAdBySlug($slug))) {
            return $this->notFound();
        }

        return $this->display($advertiser, $ad);
    }

    public function nowAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        /*
        if (! $this->request->isGet()) {
            return $this->methodNotAllowed();
        }
        */

        if (! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if (null === ($token = $this->dispatcher->getParam('token')) ||
            null === ($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            return $this->notFound();
        }

        /*
         * DO NOT require the user to re-generate its token,
         * as this action is free and everybody can call it
         *
        if (! $advertiser->isTokenValid()) {
            $this->flash->error($this->translate('ad.label.token_expired'));

            // Forward to @manage
            $this->dispatcher->setParam('advertiser', $advertiser);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action' => 'index',
            ]);
        }
        */

        if (empty($ad = $this->findAdBySlug($slug))) {
            return $this->notFound();
        }

        // Validate the provided user email is allowed to promote
        // the ad. This includes:
        //
        // - the legitimate owner
        // - the mailbox configured in "config.publication.allowPublishFrom"
        // - any email, if the current advertiser is the generic scrapper
        //   (identified by its email, "config.mailbox.scrap.from")
        $email = $_POST['author_email'];

        if (! $this->isAllowedPublicationEmail($email, $advertiser)) {
            return $this->invalidEmail($advertiser, $ad);
        }

        if ($this->isAllegedAdvertiser($email, $advertiser)) {
            if (false === $this->assign($ad, $email)) {
                return $this->internalServerError($t);
            }
        }

        /** @var Simple $promotion */
        $promotion = $this->findCurrentPromotion($ad);

        if ($promotion->count()) {
            /** @noinspection PhpParamsInspection */
            return $this->alreadyPromoted($advertiser, $ad, $promotion[0]);
        }

        return $this->promote($advertiser, $ad);
    }


    protected function invalidEmail(Advertiser $advertiser, Ad $ad)
    {
        $this->info(sprintf(' Tried to freely promote ad #%d with invalid email %s (expected %s)',
            $ad->id,
            $_POST['author_email'],
            $advertiser->email
        ));

        $this->setDefaultViewVars();

        $this->view->setVar('ad', $ad);
        $this->view->setVar('advertiser', $advertiser);
        $this->view->pick('promote/invalidEmail');
    }

    protected function alreadyPromoted(Advertiser $advertiser, Ad $ad, Promotion $promotion)
    {
        $this->debug(sprintf(' %s Ad is already promoted (from %s to %s)',
            "\xe2\x9d\x8c",
            $promotion->validFrom,
            $promotion->validTo
        ));

        $this->setDefaultViewVars();

        $this->view->setVar('ad', $ad);
        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('promotion', $promotion);
        $this->view->pick('promote/alreadyPromoted');
    }

    protected function display(Advertiser $advertiser, Ad $ad)
    {
        $this->setDefaultViewVars();

        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('ad', $ad);
        $this->view->setVar('products', $this->getProducts());
        $this->view->setVar('request', new Request());
        $this->view->setVar('form', new PromoteForm());
    }

    protected function assign(Ad $ad, string $email): bool
    {
        try {
            $this->getAssignModelManager($ad, $email)->save();
            $this->debug(sprintf(' %s Ad assigned to %s', "\xe2\x9c\x94", $email));
        } catch (Throwable $t) {
            $this->error(sprintf('Failed to assign ad to %s: %s', $email, $t->getMessage()));

            return false;
        }

        return true;
    }

    protected function promote(Advertiser $advertiser, Ad $ad)
    {
        $this->info(sprintf('Promoting ad %d (Freemium Promotion) ...', $ad->id));

        // Should we get a promoter
        $promotion = new Promotion();
        $promotion->validFrom = time();
        $promotion->validTo = $promotion->validFrom + (24 * 3600);
        $promotion->credits = 0;

        try {
            $this->getPromotionModelManager($advertiser, $ad, $promotion)->save();
            $this->debug(sprintf(' %s Promoted from %s to %s',
                "\xe2\x9c\x94",
                $promotion->validFrom,
                $promotion->validTo
            ));
        } catch (Throwable $t) {
            $this->error(sprintf('Failed to promote ad: %s', $t->getMessage()));

            return $this->internalServerError($t);
        }

        $this->setDefaultViewVars();

        $this->response->setStatusCode(202, 'Accepted');
        $this->flash->success($this->translate('advertiser.label.promotion_success'));
        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('ad', $ad);

        // Clear cache

    }


    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAssignModelManager(Ad $ad, string $email): AssignModelManager
    {
        $manager = new AssignModelManager($ad);
        $manager->setDI($this->di);
        $manager->setEmail($email);

        return $manager;
    }

    protected function getPromotionModelManager(Advertiser $advertiser, Ad $ad, Promotion $promotion): PromotionModelManager
    {
        $manager = new PromotionModelManager($promotion);
        $manager->setDI($this->di);
        $manager->setAdvertiser($advertiser);
        $manager->setAd($ad);

        return $manager;
    }

    protected function getProductRepository(): Repository
    {
        return $this->getRepository(Product::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getPromotionRepository(): PromotionRepository
    {
        /** @var PromotionRepository $repository */
        $repository = $this->getRepository(Promotion::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }


    protected function findAdBySlug(string $slug)
    {
        return $this->getAdRepository()->findOneBy(sprintf('slug = "%s"', $slug));
    }

    protected function findAdvertiserByToken(string $token)
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('token = "%s"', $token));
    }

    protected function findAutoPromotionsByAd(Ad $ad)
    {
        return $this->getPromotionRepository()->findAutoByAd($ad);
    }

    protected function findCurrentPromotion(Ad $ad)
    {
        return $this->getPromotionRepository()->findCurrentByAd($ad);
    }

    protected function getProducts()
    {
        // Note we go through cache
        return $this->getRepository(Product::class)->findAll('active = 1');
    }

    protected function isAllegedAdvertiser(string $email, Advertiser $advertiser): bool
    {
        return $advertiser->email === $this->getConfig()->mailbox->scrap->from
            && $advertiser->email !== $email
        ;
    }

    protected function isAllowedPublicationEmail(string $email, Advertiser $advertiser): bool
    {
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Invalid email
            return false;
        }

        $config = $this->getConfig();

        if ($email === $advertiser->email && $email !== $config->mailbox->scrap->from) {
            // Legitimate owner (advertiser) is always allowed
            return true;
        }

        if ($email === $config->publication->allowPublishFrom) {
            // Always allow the email address configured in
            // publication.allowPublishFrom email
            return true;
        }

        if ($advertiser->email === $config->mailbox->scrap->from) {
            // Assume the provided email is the legitimate
            // advertiser (this may be too heavy ;)
            return true;
        }

        return false;
    }

}
