<?php

namespace App\Controller;

use App\Form\AdForm;
use App\Form\FormUtil;
use App\Manager\AdModelManager;
use App\Manager\ModelManager;
use App\Manager\PersistenceException;
use App\Model\Ad;

use App\Model\Advertiser;
use App\Model\Media;
use App\Repository\Repository;
use Phalcon\Forms\Form;
use Phalcon\Mvc\ModelInterface;

/**
 * All ad deletion tasks should be delegated to this controller.
 */
class DeleteController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isDelete()) {
            return $this->methodNotAllowed();
        }

        if (null === ($token = $this->dispatcher->getParam('token')) ||
            null === ($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            return $this->notFound();
        }

        if (! $advertiser->isTokenValid()) {
            $this->invalidToken($advertiser);
        }

        if (empty($ad = $this->findAdBySlug($slug))) {
            return $this->notFound();
        }

        return $this->deleteAd($ad);
    }


    protected function invalidToken(Advertiser $advertiser)
    {
        $this->flash->error($this->translate('ad.label.invalid_token'));
        $this->dispatcher->setParam('advertiser', $advertiser);
        $this->dispatcher->forward([
            'controller' => 'manage',
            'action' => 'requestToken',
        ]);
    }

    protected function deleteAd(Ad $entity)
    {
        /** @var AdModelManager $manager */
        $manager = $this->getModelManager($entity);
        $manager->delete();

        $this->setDefaultViewVars();
    }

    /**
     * @inheritdoc
     */
    protected function getModelManager(ModelInterface $model): ModelManager
    {
        $manager = new AdModelManager($model);
        $manager->setDI($this->di);

        return $manager;
    }

    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }


    protected function findAdBySlug(string $slug)
    {
        return $this->getAdRepository()->findOneBy(sprintf('slug = "%s"', $slug));
    }

    protected function findAdvertiserByToken(string $token)
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('token = "%s"', $token));
    }

}
