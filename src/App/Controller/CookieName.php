<?php

namespace App\Controller;

final class CookieName
{
    /**
     * @var string
     */
    const USE_OF_COOKIES = 'UOC';
}