<?php

namespace App\Controller;

use App\Form\ContactForm;
use App\Form\FormUtil;
use App\Manager\PersistenceException;
use App\Model\Contact;

/**
 * A controller to create contact requests. A mail notification will be sent
 * to both the requester and the mail configured in the configuration property
 * "mailbox.complaints".
 */
class ContactController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if ($this->request->isGet()) {
            return $this->newContact();
        }

        if ($this->request->isPost()) {
            return $this->createContact();
        }

        throw new \LogicException('L00');
    }

    public function successAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('entity', $this->dispatcher->getParam('entity'));
    }


    protected function newContact()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('form', new ContactForm());
        $this->view->setVar('entity', new Contact());
    }

    protected function createContact()
    {
        $entity = new Contact();
        $form = new ContactForm();

        if ($form->isValid($this->request->getPost(), $entity)) {
            try {
                $this->contact($entity);
            } catch (\Throwable $t) {
                $this->error(sprintf('Failed to create contact request: %s', $t->getMessage()));

                return $this->internalServerError($t);
            }

            $this->info('Succeed, forward to @contact_success');

            $this->flash->success('contact.label.success');
            $this->response->setStatusCode(201, 'Created');
            $this->dispatcher->setParam('entity', $entity);
            $this->dispatcher->forward([
                'controller' => 'contact',
                'action'     => 'success',
            ]);

        } else {
            $this->error(sprintf('Failed to validate contact data: %s', FormUtil::errorsAsString($form)));
            $this->addValidationErrorsToFlash($form, 'contact.label.');
            $this->setDefaultViewVars();

            $this->view->setVar('form', $form);
            $this->view->setVar('entity', $entity);
        }
    }

    protected function contact(Contact $contact)
    {
        $manager = $this->getModelManager($contact);
        $manager->save();

        $this->notify($contact);
    }

    protected function notify(Contact $contact)
    {
        $subject = $this->getTranslator()->translate('mail.subject.contact_request');

        $this->mail(null, $contact->email, $subject, '../mail/contact', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->getGeoLocation(),
            'subject'     => $subject,
            'entity'      => $contact,
        ]);

        $mailbox = $this->getConfig()->mailbox;

        if (isset ($mailbox->contact)) {
            $this->mail('contact', $mailbox->contact->from, $subject, '../mail/contactRequest', [
                'config'      => $this->getConfig(),
                'geolocation' => $this->getGeoLocation(),
                'subject'     => $subject,
                'entity'      => $contact,
                'date'        => new \DateTime(),
            ]);
        }
    }
}
