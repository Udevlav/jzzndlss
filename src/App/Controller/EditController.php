<?php

namespace App\Controller;

use App\Form\AdForm;
use App\Form\FormUtil;
use App\Manager\AdModelManager;
use App\Manager\ModelManager;
use App\Manager\PersistenceException;
use App\Model\Ad;

use App\Model\Advertiser;
use App\Model\Media;
use App\Model\Tag;
use App\Repository\MediaRepository;
use App\Repository\Repository;
use App\Repository\TagRepository;
use Phalcon\Mvc\ModelInterface;

/**
 * All ad edition tasks should be delegated to this controller.
 */
class EditController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isPost() && ! $this->request->isPut()) {
            return $this->methodNotAllowed();
        }

        if (null === ($token = $this->dispatcher->getParam('token')) ||
            null === ($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            return $this->notFound();
        }

        if (! $advertiser->isTokenValid()) {
            $this->invalidToken($advertiser);
        }

        if (empty($ad = $this->findAdBySlug($slug))) {
            return $this->notFound();
        }

        if ($this->request->isGet()) {
            return $this->editAd($advertiser, $ad);
        }

        return $this->updateAd($ad);
    }


    protected function editAd(Advertiser $advertiser, Ad $entity)
    {
        $form = new AdForm();
        $form->setEntity($entity);

        $this->setDefaultViewVars();

        $this->view->setVar('form', $form);
        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('entity', $entity);
        $this->view->setVar('poster', []);
        $this->view->setVar('images', $this->findImages($entity));
        $this->view->setVar('videos', $this->findVideos($entity));
        $this->view->setVar('tags', $this->findTags($entity));
    }


    protected function updateAd(Ad $entity)
    {
        $form = new AdForm();

        if ($form->isValid($this->request->getPost(), $entity)) {
            try {
                $this->update($entity, $form);

            } catch (\Throwable $t) {
                if ($t instanceof PersistenceException && false !== strpos($t->getMessage(), 'uniqueness')) {
                    return $this->validationError($entity, $form);
                }

                $this->error(sprintf('Failed to update ad: %s', $t->getMessage()));

                return $this->internalServerError($t);
            }

            $this->debug('Updated ad '. json_encode($entity->toArray(), \JSON_PRETTY_PRINT));
            $this->info('Succeed, forward to @payment');

            $this->response->setStatusCode(202, 'Accepted');
            $this->flash->success($this->translate('ad.label.edit_success'));
            $this->dispatcher->setParam('entity', $entity);
            $this->dispatcher->setParam('dispatched', true);
            $this->dispatcher->forward([
                'controller' => 'payment',
                'action'     => 'index',
            ]);

        } else {
            return $this->validationError($entity, $form);
        }
    }

    protected function invalidToken(Advertiser $advertiser)
    {
        $this->flash->error($this->translate('ad.label.invalid_token'));
        $this->dispatcher->setParam('advertiser', $advertiser);
        $this->dispatcher->forward([
            'controller' => 'manage',
            'action' => 'requestToken',
        ]);
    }

    protected function notEnoughCredits(Advertiser $advertiser)
    {
        $this->flash->error($this->translate('advertiser.label.not_enough_credits'));
        $this->dispatcher->setParam('advertiser', $advertiser);
        $this->dispatcher->forward([
            'controller' => 'payment',
            'action' => 'index',
        ]);
    }

    /**
     * @param Ad $entity
     * @param AdForm $form
     */
    protected function validationError(Ad $entity, AdForm $form)
    {
        $this->error(sprintf('Failed to validate ad data: %s', FormUtil::errorsAsString($form)));
        $this->setDefaultViewVars();
        $this->addValidationErrorsToFlash($form, 'ad.label.');

        $this->response->setStatusCode(400, 'Bad Request');

        $this->view->setVar('form', $form);
        $this->view->setVar('entity', $entity);
        $this->view->setVar('poster', $form->getPosterId());
        $this->view->setVar('images', $this->findMediaIn($form->getImageIds()));
        $this->view->setVar('videos', $this->findMediaIn($form->getVideoIds()));
        $this->view->setVar('tags', $form->getTagIds());
    }

    /**
     * @param Ad $entity
     * @param AdForm $form
     */
    protected function update(Ad $entity, AdForm $form)
    {
        $this->debug(sprintf('Tags [%s]', implode(', ', $form->getTagIds())));
        $this->debug(sprintf('Images [%s]', implode(', ', $form->getImageIds())));
        $this->debug(sprintf('Videos [%s]', implode(', ', $form->getVideoIds())));

        /** @var AdModelManager $manager */
        $manager = $this->getModelManager($entity);
        $manager->setGeoLocation($this->getGeoLocation());
        $manager->setPosterId($form->getPosterId());
        $manager->setNotifyBySms($form->notifySms());

        foreach ($form->getTagIds() as $id) {
            $manager->addTag($id);
        }

        foreach ($form->getImageIds() as $id) {
            $manager->addMedia($id);
        }

        foreach ($form->getVideoIds() as $id) {
            $manager->addMedia($id, true);
        }

        $manager->save();
    }

    /**
     * @inheritdoc
     */
    protected function getModelManager(ModelInterface $model): ModelManager
    {
        $manager = new AdModelManager($model);
        $manager->setDI($this->di);

        return $manager;
    }

    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getMediaRepository(): Repository
    {
        return $this->getRepository(Media::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getTagRepository(): Repository
    {
        return $this->getRepository(Tag::class)
            ->disableCache()
            ->hydrateRecords();
    }


    protected function findAdBySlug(string $slug)
    {
        return $this->getAdRepository()->findOneBy(sprintf('slug = "%s"', $slug));
    }

    protected function findAdvertiserByToken(string $token)
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('token = "%s"', $token));
    }

    protected function findTags(Ad $ad)
    {
        /** @var TagRepository $repository */
        $repository = $this->getTagRepository();

        return $repository->idsByAd($ad);
    }

    protected function findImages(Ad $ad)
    {
        /** @var MediaRepository $repository */
        $repository = $this->getMediaRepository();

        return $repository->findMasterImages($ad);
    }

    protected function findVideos(Ad $ad)
    {
        /** @var MediaRepository $repository */
        $repository = $this->getMediaRepository();

        return $repository->findMasterVideos($ad);
    }

    protected function findMediaIn(array $ids)
    {
        $medias = [];

        if (count($ids)) {
            foreach ($this->getRepository(Media::class)->findIn($ids) as $media) {
                $medias[] = $media;
            }
        }

        return $medias;
    }

}
