<?php

namespace App\Controller;

use App\Form\FormUtil;
use App\Form\OnlineAdvertiserForm;
use App\Form\OnlinePeerForm;
use App\Manager\ModelManager;
use App\Model\Ad;
use App\Model\ChatMessage;
use App\Model\OnlineAdvertiser;
use App\Repository\AdRepository;
use App\Repository\ChatMessageRepository;
use App\Repository\OnlineAdvertiserRepository;

use Phalcon\Mvc\Model\Resultset;

use DateTime;

/**
 * A controller to manage online users.
 */
class OnlineController extends BaseController
{

    public function indexAction()
    {
        /*
        if (! $this->request->isAjax() || ! $this->request->isGet() || empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }
        */
        if (! $this->request->isAjax() || ! $this->request->isGet() || empty($hash = $this->dispatcher->getParam('hash'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneByHash($hash))) {
            return $this->notFound();
        }

        return $this->check($entity);
    }

    public function peerAction()
    {
        if (! $this->request->isAjax() || ! $this->request->isPost() || empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }

        return $this->peer($entity);
    }

    public function onlineAction()
    {
        if (! $this->request->isAjax() || ! $this->request->isPost() || empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }

        return $this->on($entity);
    }

    public function offlineAction()
    {
        if (! $this->request->isAjax() || ! ($this->request->isPost() || $this->request->isDelete()) || empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }

        return $this->off($entity);
    }


    protected function check(Ad $ad)
    {
        /** @var Resultset $online */
        $online = $this->getOnlineAdvertiserRepository()->findByAd($ad);

        if (! $online->getFirst()) {
            $this->info(sprintf('Advertiser %s is NOT online.', $ad->email));

            return $this->json(204, [
                'online' => false,
            ]);
        }

        // Remove old sessions
        $this->debug(sprintf('Removing %d old sessions for advertiser %s ...', count($online) - 1, $ad->email));

        /** @var OnlineAdvertiser $onlineAdvertiser */
        $onlineAdvertiser = $online->getFirst();

        for ($offset = $online->count() - 1; $offset > 0; $offset --) {
            if (! $online->offsetGet($offset)->delete()) {
                $this->error(sprintf('Failed to remove old online user entry (%d/%d): %s.',
                    ModelManager::errorsAsString($online->offsetGet($offset)),
                    $online->count() - $offset,
                    $online->count()
                ));

                return $this->json(500);
            }
        }

        if (! $this->isLastActivityInRange($onlineAdvertiser)) {
            $this->debug(sprintf('Session is too old (%s), remove entry ...', $onlineAdvertiser->getLastActivityAt()->format('d/m/Y H:i:s')));

            if (! $online->offsetGet($offset)->delete()) {
                $this->error(sprintf('Failed to remove old online user entry: %s.', ModelManager::errorsAsString($online)));

                return $this->json(500);
            }

            $this->info(sprintf('Advertiser %s is NOT online.', $ad->email));

            return $this->json(204, [
                'online' => false,
            ]);
        }

        $this->info(sprintf('Advertiser %s is online (sessionId = %s).', $ad->email, $onlineAdvertiser->advertiserSid));

        return $this->json(200, [
            'online' => true,
            'peer' => [
                'age' => $ad->age,
                'name' => $ad->name,
                'phone' => $ad->phone,
                'sessionId' => $onlineAdvertiser->advertiserSid,
            ],
        ]);
    }

    protected function peer(Ad $ad)
    {
        $form = new OnlinePeerForm();

        if (! $form->isValid($this->request->getPost())) {
            $this->error(sprintf('Invalid online peer request: %s.', FormUtil::errorsAsString($form)));

            return $this->json(400);
        }

        /** @var Resultset $online */
        $online = $this->getOnlineAdvertiserRepository()->findByAd($ad);

        if (0 === $online->count()) {
            $this->error('Invalid online peer request: advertiser is no longer online.');

            return $this->json(404);
        }

        /** @var OnlineAdvertiser $onlineAdvertiser */
        $onlineAdvertiser = $online->getFirst();

        if ($onlineAdvertiser->advertiserSid === $this->getSession()->getId()) {
            $this->error('Invalid online peer request: you cannot talk with yourself !');

            return $this->json(412);
        }

        /** @var Resultset $messages */
        $messages = $this->getChatMessageRepository()->findByChatSession(
            $this->getSession()->getId(),
            $onlineAdvertiser->advertiserSid
        );

        $this->info(sprintf('Peer is NOW online (sessionId = %s, %d messages).',
            $this->getSession()->getId(),
            count($messages)
        ));

        return $this->json(201, [
            'online' => true,
            'messages' => $messages->toArray(),
            'user' => [
                'name' => $form->get('displayName')->getValue(),
                'sessionId' => $this->getSession()->getId(),
            ],
            'peer' => [
                'age' => $ad->age,
                'name' => $ad->name,
                'phone' => $ad->phone,
                'sessionId' => $onlineAdvertiser->advertiserSid,
            ],
        ]);
    }

    protected function on(Ad $ad)
    {
        $online = new OnlineAdvertiser();
        $form = new OnlineAdvertiserForm();

        if (! $form->isValid($this->request->getPost(), $online)) {
            $this->error(sprintf('Invalid online advertiser: %s.', FormUtil::errorsAsString($form)));

            return $this->json(400);
        }

        if ($ad->email !== $form->get('email')->getValue()) {
            $this->error(sprintf('Invalid online advertiser: provided email (%s) does not match expected (%s).', $form->get('email')->getValue(), $ad->email));

            return $this->json(400);
        }

        $online->advertiserId = $ad->advertiserId;
        $online->advertiserSid = $this->getSession()->getId();

        if (! $online->save()) {
            $this->error(sprintf('Failed to create online advertiser: %s.', ModelManager::errorsAsString($online)));

            return $this->json(500);
        }

        /** @var Resultset $messages */
        $messages = $this->getChatMessageRepository()->findByAdvertiserSession($this->getSession()->getId());

        $this->info(sprintf('Advertiser %s is NOW online (sessionId = %s).', $ad->email, $online->advertiserSid));

        return $this->json(201, [
            'online' => true,
            'messages' => $messages->toArray(),
            'user' => [
                'age' => $ad->age,
                'name' => $ad->name,
                'phone' => $ad->phone,
                'sessionId' => $online->advertiserSid,
            ],
        ]);
    }

    protected function off(Ad $ad)
    {
        /** @var Resultset $online */
        $online = $this->getOnlineAdvertiserRepository()->findByAd($ad);

        // Remove all sessions
        for ($offset = 0; $offset < $online->count(); $offset ++) {
            if (! $online->offsetGet($offset)->delete()) {
                return $this->json(500);
            }
        }

        $this->info(sprintf('Advertiser %s is NOW offline.', $ad->email));

        return $this->json(202, [
            'online' => false,
            'user' => [
                'age' => $ad->age,
                'name' => $ad->name,
                'phone' => $ad->phone,
                'sessionId' => $this->getSession()->getId(),
            ],
        ]);
    }


    protected function isLastActivityInRange(OnlineAdvertiser $onlineAdvertiser): bool
    {
        $diff = (new DateTime())->getTimestamp() - $onlineAdvertiser->getLastActivityAt()->getTimestamp();
        $threshold = $this->getConfig()->chat->idleSessionTimeout;

        return $diff <= $threshold;
    }

    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)
            ->disableCache();

        return $repository;
    }

    /**
     * @return ChatMessageRepository
     */
    protected function getChatMessageRepository(): ChatMessageRepository
    {
        /** @var ChatMessageRepository $repository */
        $repository = $this->getRepository(ChatMessage::class)
            ->disableCache();

        return $repository;
    }

    /**
     * @return OnlineAdvertiserRepository
     */
    protected function getOnlineAdvertiserRepository(): OnlineAdvertiserRepository
    {
        /** @var OnlineAdvertiserRepository $repository */
        $repository = $this->getRepository(OnlineAdvertiser::class)
            ->disableCache();

        return $repository;
    }
}