<?php

namespace App\Controller;

use App\Model\Ad;
use App\Model\AdStats;
use App\Repository\AdRepository;
use App\Repository\AdStatsRepository;

class StatsController extends BaseController
{

    static public function aaa($id)
    {
        $data = [];

        for ($i = 0; $i < 30; $i ++) {
            $stats = new AdStats();
            $stats->adId = $id;
            $stats->numVisits = (int) rand(0, 500000);
            $stats->numPhoneClicks = 0;
            $stats->numImpressions = (int) rand(0, 500000);
            $stats->numDaysOnTop = (int) rand(0, 5000);
            $stats->numPromotions = (int) rand(0, 2);

            $data[] = $stats;
        }

        return $data;
    }

    public function indexAction()
    {
        if (! $this->request->isGet()) {
            return $this->methodNotAllowed();
        }

        if (! $this->request->isAjax()) {
            return $this->badRequest();
        }

        if (empty($hash = $this->dispatcher->getParam('hash'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneByHash($hash))) {
            return $this->notFound();
        }

        $stats = $this->getAdStatsRepository()->findLastByAd($entity);
        //$stats = static::aaa($entity->id);
        ksort($stats);

        return $this->json(200, $stats);
    }


    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)->disableCache();

        return $repository;
    }

    /**
     * @return AdStatsRepository
     */
    protected function getAdStatsRepository(): AdStatsRepository
    {
        /** @var AdStatsRepository $repository */
        $repository = $this->getRepository(AdStats::class)->disableCache();

        return $repository;
    }

}
