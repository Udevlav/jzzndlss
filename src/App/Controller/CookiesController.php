<?php

namespace App\Controller;

class CookiesController extends BaseController
{

    public function indexAction(string $name)
    {
        $this->view->setVar('nombre', $name);
    }
}