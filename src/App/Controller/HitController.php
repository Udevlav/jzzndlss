<?php

namespace App\Controller;

use App\Manager\HitModelManager;
use App\Manager\ModelManager;
use App\Manager\PersistenceException;
use App\Model\Ad;
use App\Model\Hit;
use App\Repository\AdRepository;

use Phalcon\Mvc\ModelInterface;

/**
 * Handles ad hits
 */
class HitController extends BaseController
{

    public function indexAction()
    {
        if (! $this->request->isGet()) {
            return $this->methodNotAllowed();
        }

        if (! $this->request->isAjax()) {
            return $this->badRequest();
        }

        /*
        if (empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }
         */

        if (empty($hash = $this->dispatcher->getParam('hash'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneByHash($hash))) {
            return $this->notFound();
        }

        try {
            /** @var HitModelManager $manager */
            $manager = $this->getModelManager(new Hit());
            $manager->setGeoLocation($this->getGeoLocation());
            $manager->setRequest($this->request);
            $manager->setAd($entity);
            $manager->save();

        } catch (PersistenceException $e) {
            $this->error(sprintf('Failed to persist entity hit: %s.', $e->getMessage()));

            return $this->json(500, [
                'error' => $e->getMessage(),
            ]);
        }

        $this->info(sprintf('Hit %s (%d total, score = %s)', $entity->slug, $entity->numHits, $entity->score));
        $this->info(sprintf('Advertiser %s', $entity->email));

        return $this->json(200, [
            'hits' => $entity->numHits,
            'score' => $entity->score,
        ]);
    }


    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)->disableCache();

        return $repository;
    }

    /**
     * @inheritdoc
     */
    protected function getModelManager(ModelInterface $model): ModelManager
    {
        $manager = new HitModelManager($model);
        $manager->setDI($this->di);

        return $manager;
    }
}