<?php

namespace App\Controller;

use App\Filter\Filters;

use App\Finder\Results;
use App\Geo\GeoLocationToProvince;
use Phalcon\Http\Response;
use Phalcon\Mvc\Model\Resultset;

/**
 * All GET-like requests (indexes, filters, queries, ...) are directed to
 * this front-end. This controller merely constructs a {@link Filters}
 * instance and it will pass the request, along the filters, to the
 * appropriate "@app.ad.finder" service.
 */
class IndexController extends BaseController
{


    public function indexAction()
    {
        if (! $this->request->isGet() && ! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        // Build filters from the request
        $filters = $this->getFilters();

        if ($this->request->isPost()) {
            $response = new Response();
            $response->redirect($filters->generateRedirectUrl());

            return $response->send();
        }

        // Find results matching filters
        try {
            $results = $this->find($filters);
        } catch (\Throwable $t) {
            return $this->internalServerError($t);
        }

        if ($this->request->isAjax()) {
            return $this->json(200, $results);
        }

        // Store user preferences in the cookie
        if ($postalCode = $filters->getData()->postalCode) {
            $this->cookies->set('location', $postalCode);
        }

        // HTML response
        $this->setDefaultViewVars();

        $this->view->setVar('filters', $filters);
        $this->view->setVar('results', $results);
    }


    protected function find(Filters $filters): Results
    {
        $geoLocation = $this->getGeoLocation();

        if ($filters->isNullCriteria()) {
            if (null !== ($province = GeoLocationToProvince::getProvince($geoLocation))) {
                echo $province;
                $filters->getData()->province = $province;
            }
        }

        return $this->getFinder()->find($filters);
    }
}
