<?php

namespace App\Controller;

/**
 * A controller for all static pages.
 */
class StaticController extends BaseController
{

    public function privacyAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('filters', $this->getFilters());
    }

    public function tosAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('filters', $this->getFilters());
    }

    public function whoweareAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('filters', $this->getFilters());
    }
}