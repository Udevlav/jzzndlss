<?php

namespace App\Controller;

use App\Form\FormUtil;
use App\Form\PromoteForm;
use App\Form\TokenForm;
use App\Manager\AdvertiserModelManager;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Promotion;
use App\Repository\Repository;

/**
 * Manages scrapped targets.
 */
class ScrapController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (null === ($token = $this->request->getQuery('provider'))) {
            return $this->badRequest();
        }

        return $this->manage($token);
    }


    protected function manage($token)
    {
        $advertiser = $this->findAdvertiser();
        $ads = $this->findAdsByProvider($token);

        $this->info(sprintf('Managing generic advertiser %s <%s> scrapped ads', $advertiser->id, $advertiser->email));
        $promotions = $this->findPromotionsByAdvertiser($advertiser);

        $this->setDefaultViewVars();
        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('ads', $ads);
        $this->view->setVar('promotions', $promotions);
        $this->view->setVar('form', new PromoteForm());
    }


    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getPromotionRepository(): Repository
    {
        return $this->getRepository(Promotion::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function findAdsByProvider($provider)
    {
        return $this->getAdRepository()->findBy([
            'conditions' => sprintf('provider = "%s"', $provider),
            'order' => 'createdAt DESC',
        ]);
    }

    protected function findAdvertiser()
    {
        $email = $this->getConfig()->mailbox->scrap->from;

        return $this->getAdvertiserRepository()->findOneBy(sprintf('email = "%s"', $email));
    }

    protected function findPromotionsByAdvertiser(Advertiser $advertiser)
    {
        return $this->getPromotionRepository()->findByAdvertiser($advertiser);
    }
}