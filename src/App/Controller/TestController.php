<?php

namespace App\Controller;

/**
 * Test controller
 */
class TestController extends BaseController
{

    public function indexAction($what)
    {
        $this->setDefaultViewVars();

        if (! empty($what)) {
            $this->view->pick($what);
        }
    }
}