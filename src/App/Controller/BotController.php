<?php

namespace App\Controller;

/**
 * A controller to manage bot requests.
 */
class BotController extends BaseController
{

    public function rateLimitExceededAction()
    {
        $this->setDefaultViewVars();
    }

    public function ipBlockedAction()
    {
        $this->setDefaultViewVars();
    }

    public function captchaVerificationFailedAction()
    {
        $this->setDefaultViewVars();
    }
}