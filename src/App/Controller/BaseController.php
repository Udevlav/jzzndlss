<?php

namespace App\Controller;

use App\Captcha\CaptchaException;
use App\Captcha\CaptchaVerifier;
use App\Config\Config;
use App\Filter\Filters;
use App\Finder\Finder;
use App\Geo\GeoLocation;
use App\Geo\GeoLocator;
use App\I18n\Translator;
use App\Locale\Locale;
use App\Locale\LocaleGuesser;
use App\Mailer\Mailer;
use App\Manager\ModelManager;
use App\Payment\Broker;
use App\Promotion\Promoter;
use App\Rank\Ranker;
use App\Repository\EntityManager;
use App\Repository\Repository;
use App\Runner\Runner;
use App\Session\User;
use App\Sms\Sms;
use App\Token\TokenGenerator;

use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;

use Imagine\Image\ImagineInterface;

use Phalcon\Dispatcher;
use Phalcon\Forms\Form;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Logger;
use Phalcon\Logger\AdapterInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\DispatcherInterface;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Router\Annotations;
use Phalcon\Session\Adapter\Files;
use Phalcon\Validation\Message;

class BaseController extends Controller
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var Locale
     */
    private $locale;

    /**
     * @var GeoLocation
     */
    private $geoLocation;

    /**
     * @var Filters
     */
    private $filters;


    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Executed before every found action
    }


    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Executed after every found action
    }


    /**
     * @return void
     */
    protected function setDefaultViewVars()
    {
        $this->view->setVar('config', $this->getConfig());
        $this->view->setVar('user', $this->getUser());
        $this->view->setVar('locale', $this->getLocale());
        $this->view->setVar('geolocation', $this->getGeoLocation());
        $this->view->setVar('route', $this->getRouter()->getMatchedRoute());
        $this->view->setVar('title', $this->getTitle());
    }

    /**
     * @return void
     */
    protected function setCookies()
    {
        $this->cookies->set(CookieName::USE_OF_COOKIES, time(), time() + 365 * 86400);
    }


    /**
     * @return Request
     */
    protected function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return DispatcherInterface
     */
    protected function getDispatcher(): DispatcherInterface
    {
        return $this->dispatcher;
    }

    /**
     * @param string $fullyQualifiedClassName
     *
     * @return Repository
     *
     * @throws \InvalidArgumentException
     */
    protected function getRepository(string $fullyQualifiedClassName): Repository
    {
        return $this->getEntityManager()->getRepository($fullyQualifiedClassName);
    }

    /**
     * @param ModelInterface $model
     *
     * @return ModelManager
     */
    protected function getModelManager(ModelInterface $model): ModelManager
    {
        $manager = new ModelManager($model);
        $manager->setDI($this->di);

        return $manager;
    }


    /**
     * @return string
     */
    protected function getTitle(): string
    {
        return $this->getConfig()->app->name;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Annotations
     */
    protected function getRouter(): Annotations
    {
        return $this->di->getShared('router');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->di->getShared('em');
    }

    /**
     * @return Finder
     */
    protected function getFinder(): Finder
    {
        return $this->di->get('finder');
    }

    /**
     * @return Runner
     */
    protected function getRunner(): Runner
    {
        return $this->di->get('runner');
    }

    /**
     * @return Broker
     */
    protected function getBroker(): Broker
    {
        return $this->di->get('broker');
    }

    /**
     * @return Files
     */
    protected function getSession(): Files
    {
        return $this->di->get('session');
    }

    /**
     * @return CaptchaVerifier
     */
    protected function getCaptchaVerifier(): CaptchaVerifier
    {
        return $this->di->getShared('recaptcha');
    }

    /**
     * @return TokenGenerator
     */
    protected function getTokenGenerator(): TokenGenerator
    {
        return $this->di->getShared('tokenGenerator');
    }

    /**
     * @return Translator
     */
    protected function getTranslator(): Translator
    {
        return $this->di->getShared('translator');
    }

    /**
     * @return Mailer
     */
    protected function getMailer(): Mailer
    {
        return $this->di->getShared('mailer');
    }

    /**
     * @return Sms
     */
    protected function getSms(): Sms
    {
        return $this->di->getShared('sms');
    }

    /**
     * @return FFMpeg
     */
    protected function getFFMpeg(): FFMpeg
    {
        return $this->di->getShared('ffmpeg');
    }

    /**
     * @return FFProbe
     */
    protected function getFFProbe(): FFProbe
    {
        return $this->di->getShared('ffprobe');
    }

    /**
     * @return ImagineInterface
     */
    protected function getImagine(): ImagineInterface
    {
        return $this->di->getShared('imagine');
    }

    /**
     * @return GeoLocator
     */
    protected function getGeoLocator(): GeoLocator
    {
        return $this->di->getShared('geolocator');
    }

    /**
     * @return \App\Locale\LocaleGuesser
     */
    protected function getLocaleGuesser(): LocaleGuesser
    {
        return $this->di->getShared('localeGuesser');
    }

    /**
     * @return AdapterInterface
     */
    protected function getLogger(): AdapterInterface
    {
        return $this->di->getShared('logger');
    }

    /**
     * @return Promoter
     */
    protected function getPromoter(): Promoter
    {
        return $this->di->getShared('promoter');
    }

    /**
     * @return Ranker
     */
    protected function getRanker(): Ranker
    {
        return $this->di->getShared('ranker');
    }


    /**
     * @return string
     */
    protected function getClientAddress(): string
    {
        if (null !== ($fakeIp = $this->getConfig()->geo->fakeIp)) {
            return $fakeIp;
        }

        return $this->request->getClientAddress();
    }

    /**
     * @return GeoLocation
     */
    protected function getGeoLocation(): GeoLocation
    {
        if (null === $this->geoLocation) {
            $this->geoLocation = $this->getGeoLocator()->lookup($this->getClientAddress());

            $this->debug(sprintf('Resolved geolocation %s', $this->geoLocation));
        }

        return $this->geoLocation;
    }

    /**
     * @return string
     */
    protected function getLocale(): string
    {
        if (null === $this->locale) {
            //$this->locale = $this->getLocaleGuesser()->getLocale($this->request);
            $this->locale = 'es';

            setlocale(\LC_ALL, $this->locale);

            $this->getTranslator()->setLocale($this->locale);
            $this->debug(sprintf('Guessed locale %s', $this->locale));
        }

        return $this->locale;
    }

    /**
     * @return User
     */
    protected function getUser(): User
    {
        if (null === $this->user) {
            $this->user = new User();
            $this->user->setGeoLocation($this->getGeoLocation());
        }

        return $this->user;
    }

    /**
     * @return Filters
     */
    protected function getFilters(): Filters
    {
        if (null === $this->filters) {
            $this->filters = new Filters($this->getEntityManager());

            $this->filters->setUserData($this->getUser());
            $this->filters->setGeoData($this->getGeoLocation());
            $this->filters->setCookieData($this->cookies);
            $this->filters->setRequestData($this->request);
            $this->filters->setDispatcherData($this->dispatcher);
        }

        return $this->filters;
    }


    /* reCAPTCHA verification */


    /**
     * @param string $response
     * @param string | null $remoteIp
     *
     * @return bool
     */
    public function verifyCaptcha(string $response, string $remoteIp = null): bool
    {
        try {
            return $this->getCaptchaVerifier()->verify($response, $remoteIp);
        } catch (CaptchaException $e) {
            $this->warning(sprintf('Failed to verify reCAPTCHA response: %s.', $e->getMessage()));
        }

        return true;
    }

    public function verifyCaptchaOrForwardToBot(string $response)
    {
        if (! $this->verifyCaptcha($response, $this->getGeoLocation()->getIp())) {
            $this->dispatcher->forward([
                'controller' => 'bot',
                'action' => 'captchaVerificationFailed',
            ]);
        }
    }


    /* Sending mails and SMS notifications */

    /**
     * @param string $mailbox
     * @param string $to
     * @param string $subject
     * @param string $template
     * @param array $data
     *
     * @return void
     */
    protected function mail(string $mailbox = null, string $to, string $subject, string $template, array $data = [])
    {
        if (null === $mailbox) {
            $mailbox = 'default';
        }

        $this->getMailer()->useMailbox($mailbox)->send($to, $subject, $template, $data);

        $this->info(sprintf('Sent %s mail to %s (using mailbox %s, template %s) ...', $subject, $to, $mailbox, $template));
    }

    /**
     * @param string $phone
     * @param string $template
     * @param array $data
     *
     * @return void
     */
    protected function sms(string $phone, string $template, array $data = [])
    {
        $this->getSms()->send($phone, $template, $data);

        $this->info(sprintf('Sent SMS to %s (using template %s)', $phone, $template));
    }

    /**
     * @param string $token
     * @param mixed ... $args
     *
     * @return string
     */
    protected function translate(string $token, ... $args)
    {
        return $this->getTranslator()->translate($token, ... $args);
    }

    /**
     * @param Form $form
     * @param string $prefix
     *
     * @return void
     */
    protected function addValidationErrorsToFlash(Form $form, string $prefix)
    {
        foreach ($form->getMessages() as $message) {
            /** @var Message $message */
            $this->flash->error(sprintf('%s: %s',
                $this->translate($prefix . $message->getField()),
                $this->translate($message->getMessage())
            ));
        }
    }

    /**
     * @param Model  $model
     * @param string $prefix
     *
     * @return void
     */
    protected function addModelErrorsToFlash(Model $model, string $prefix)
    {
        foreach ($model->getMessages() as $message) {
            /** @var Message $message */
            $this->flash->error(sprintf('%s: %s',
                $this->translate($prefix . $message->getField()),
                $this->translate($message->getMessage())
            ));
        }
    }


    /* Logging */

    /**
     * @param string $message
     * @param int $level
     *
     * @return BaseController
     */
    protected function log(string $message, $level = Logger::INFO): BaseController
    {
        $route = $this->getRouter()->getMatchedRoute();

        $line = sprintf('%s %s - %s',
            $this->request->getMethod(),
            $route ? $route->getName() : '<no route>',
            $message
        );

        $this->getLogger()->log($line, $level);

        return $this;
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function special(string $message): BaseController
    {
        return $this->log($message, Logger::SPECIAL);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function custom(string $message): BaseController
    {
        return $this->log($message, Logger::CUSTOM);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function debug(string $message): BaseController
    {
        return $this->log($message, Logger::DEBUG);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function info(string $message): BaseController
    {
        return $this->log($message, Logger::INFO);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function notice(string $message): BaseController
    {
        return $this->log($message, Logger::NOTICE);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function warning(string $message): BaseController
    {
        return $this->log($message, Logger::WARNING);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function error(string $message): BaseController
    {
        return $this->log($message, Logger::ERROR);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function alert(string $message): BaseController
    {
        return $this->log($message, Logger::ALERT);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function critical(string $message): BaseController
    {
        return $this->log($message, Logger::CRITICAL);
    }

    /**
     * @param string $message
     *
     * @return BaseController
     */
    protected function emergency(string $message): BaseController
    {
        return $this->log($message, Logger::EMERGENCY);
    }


    /**
     * Checks the rate limit for the current session / IP, and returns an
     * 428 FLOOD response if the limit is exceeded.
     *
     * @return void
     */
    protected function checkRateLimit()
    {
        // TODO
    }


    /* Responses */

    /**
     * Phalcon automatically calls the view on each request (from the manual:
     * "Phalcon automatically passes the execution to the view component as
     * soon as a particular controller has completed its cycle. The view
     * component will look in the views folder for a folder named as the same
     * name of the last controller executed and then for a file named as the
     * last action executed.
     *
     * So, if you want a different template, specify it here
     *
     * @param string $dir
     * @param string $path
     * @param bool $setDefaultViewVars
     */
    protected function render(string $dir, string $path, bool $setDefaultViewVars = false)
    {
        if ($setDefaultViewVars) {
            $this->setDefaultViewVars();
        }

        $this->view->render($dir, $path);
    }

    /**
     * @param int $status
     * @param mixed $data
     * @param array $headers
     *
     * @return Response
     */
    protected function json(int $status, $data = null, array $headers = []): Response
    {
        if (null !== $data) {
            try {
                $data = @json_encode($data);
            } catch (\Throwable $t) {
                $this->internalServerError($t);
            }
        } else {
            $data = '';
        }

        $response = new Response();
        $response->setStatusCode($status);
        $response->setHeader('Content-Type', 'application/json');
        $response->setHeader('Content-Length', strlen($data));

        foreach ($headers as $name => $header) {
            $response->setHeader($name, $header);
        }

        $response->setContent($data);

        return $response;
    }


    /* Error forwarding */


    /**
     * @param \Throwable $t
     */
    protected function badRequest(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error400',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function unauthorized(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error401',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function forbidden(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error403',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function notFound(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error404',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function methodNotAllowed(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error405',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function gone(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error410',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function internalServerError(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error500',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function notImplemented(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error501',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function serviceUnavailable(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error503',
        ]);
    }

    /**
     * @param \Throwable $t
     */
    protected function gatewayTimeout(\Throwable $t = null)
    {
        $this->dispatcher->setParam('throwable', $t);
        $this->dispatcher->forward([
            'controller' => 'error',
            'action' => 'error504',
        ]);
    }
}
