<?php

namespace App\Controller;

class ErrorController extends BaseController
{

    public function indexAction()
    {
        $this->setDefaultViewVars();
    }

    public function error400Action()
    {
        return $this->handle(400, 'Bad Request');
    }

    public function error401Action()
    {
        return $this->handle(401, 'Unauthorized');
    }

    public function error403Action()
    {
        return $this->handle(403, 'Forbidden');
    }

    public function error404Action()
    {
        return $this->handle(404, 'Not Found');
    }

    public function error405Action()
    {
        return $this->handle(405, 'Method Not Allowed');
    }

    public function error410Action()
    {
        return $this->handle(410, 'Gone');
    }

    public function error412Action()
    {
        return $this->handle(412, 'Precondition Failed');
    }

    public function error500Action()
    {
        return $this->handle(500, 'Oh My God');
    }

    public function error501Action()
    {
        return $this->handle(501, 'Not Implemented');
    }

    public function error503Action()
    {
        return $this->handle(503, 'Service Unavailable');
    }

    public function error504Action()
    {
        return $this->handle(504, 'Gateway Timeout');
    }


    protected function handle(int $errorCode, string $errorMessage)
    {
        $this->response->setStatusCode($errorCode, $errorMessage);

        $this->setDefaultViewVars();

        $this->view->setVar('code', $errorCode);
        $this->view->setVar('message', $errorMessage);

        $message = sprintf('Error %d %s (%s %s)',
            $errorCode,
            $errorMessage,
            $this->request->getMethod(),
            $this->request->getURI()
        );

        if (null !== ($throwable = $this->dispatcher->getParam('throwable'))) {
            $this->view->setVar('throwable', $throwable);

            $message .= sprintf("\n\nCaused by %s (%s)\n%s\n\n",
                $throwable->getMessage(),
                $throwable->getCode(),
                $throwable->getTraceAsString()
            );
        }

        $this->error($message);
        $this->view->render('error', 'index');
        $this->view->disable();
    }
}