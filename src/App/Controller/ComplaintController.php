<?php

namespace App\Controller;

use App\Form\ComplaintForm;
use App\Form\FormUtil;
use App\Model\Ad;
use App\Model\Complaint;
use App\Repository\AdRepository;

use LogicException;

/**
 * A controller to create complaints. A mail notification will be sent to both
 * the requester and the mail configured in the configuration property
 * "mailbox.complaints".
 */
class ComplaintController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (! $this->request->isGet() && ! $this->request->isPost()) {
            return $this->methodNotAllowed();
        }

        if (empty($slug = $this->dispatcher->getParam('slug'))) {
            return $this->badRequest();
        }

        if (empty($entity = $this->getAdRepository()->findOneBySlug($slug))) {
            return $this->notFound();
        }

        if ($this->request->isGet()) {
            return $this->newComplaint($entity);
        }

        if ($this->request->isPost()) {
            return $this->createComplaint($entity);
        }

        throw new LogicException('L00');
    }

    public function successAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('entity', $this->dispatcher->getParam('entity'));
    }


    protected function newComplaint(Ad $ad)
    {
        $this->setDefaultViewVars();

        $this->view->setVar('ad', $ad);
        $this->view->setVar('form', new ComplaintForm());
        $this->view->setVar('entity', new Complaint());
    }

    protected function createComplaint(Ad $ad)
    {
        $entity = new Complaint();
        $form = new ComplaintForm();

        if ($form->isValid($this->request->getPost(), $entity)) {
            $entity->adId = $ad->id;

            try {
                $this->complaint($entity);
            } catch (\Throwable $t) {
                $this->error(sprintf('Failed to create complaint: %s.', $t->getMessage()));

                return $this->internalServerError($t);
            }

            $this->info('Succeed, forward to @complaint_success ...');

            $this->flash->success('complaint.label.sent_success');
            $this->response->setStatusCode(201, 'Created');
            $this->dispatcher->setParam('entity', $entity);
            $this->dispatcher->forward([
                'controller' => 'complaint',
                'action' => 'success',
            ]);

        } else {
            $this->error(sprintf('Failed to validate complaint data: %s.', FormUtil::errorsAsString($form)));
            $this->addValidationErrorsToFlash($form, 'complaint.label.');
            $this->setDefaultViewVars();

            $this->view->setVar('ad', $ad);
            $this->view->setVar('form', $form);
            $this->view->setVar('entity', $entity);
        }
    }

    protected function complaint(Complaint $complaint)
    {
        $manager = $this->getModelManager($complaint);
        $manager->save();

        $this->notify($complaint);
    }

    protected function notify(Complaint $complaint)
    {
        $subject = $this->getTranslator()->translate('mail.subject.complaint_request');

        $this->mail(null, $complaint->email, $subject, '../mail/complaint', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->getGeoLocation(),
            'subject'     => $subject,
            'entity'      => $complaint,
        ]);

        $mailbox = $this->getConfig()->mailbox;

        if (isset ($mailbox->complaint)) {
            $this->mail('complaint', $mailbox->complaint->from, $subject, '../mail/complaint', [
                'config'      => $this->getConfig(),
                'geolocation' => $this->getGeoLocation(),
                'subject'     => $subject,
                'entity'      => $complaint,
                'date'        => new \DateTime(),
            ]);
        }
    }


    /**
     * @return AdRepository
     */
    protected function getAdRepository(): AdRepository
    {
        /** @var AdRepository $repository */
        $repository = $this->getRepository(Ad::class)
            ->disableCache();

        return $repository;
    }
}