<?php

namespace App\Controller;

use App\Form\FormUtil;
use App\Form\PromoteForm;
use App\Form\TokenForm;
use App\Manager\AdvertiserModelManager;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Promotion;
use App\Repository\Repository;

/**
 * All ad and advertiser management tasks should be delegated to this
 * controller. By management task we assume both generating the advertiser
 * token required to manage the advertiser profile and to grant access to the
 * advertiser management page. This includes ad edition and deletion.
 *
 * Promotions and payments are handled in its own controller.
 */
class ManageController extends BaseController
{

    public function indexAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (null === ($token = $this->dispatcher->getParam('token'))) {
            return $this->badRequest();
        }

        if (empty($advertiser = $this->findAdvertiserByToken($token))) {
            return $this->notFound();
        }

        if (! $advertiser->isTokenValid()) {
            $this->invalidToken($advertiser);
        }

        if ($this->request->isPost()) {
            return $this->generateToken($advertiser);
        }

        if ($this->request->isGet()) {
            return $this->manage($advertiser);
        }

        return $this->methodNotAllowed();
    }

    public function requestTokenAction()
    {
        if ($this->request->isAjax()) {
            return $this->badRequest();
        }

        if (null === ($advertiser = $this->dispatcher->getParam('advertiser'))) {
            if (null === ($slug = $this->dispatcher->getParam('slug'))) {
                return $this->badRequest();
            }

            if (null === ($ad = $this->findAdBySlug($slug))) {
                return $this->notFound();
            }

            if (null === ($advertiser = $this->getAdvertiserRepository()->find($ad->advertiserId))) {
                return $this->notFound();
            }
        }

        $this->setDefaultViewVars();

        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('form', new TokenForm());
    }

    public function tokenGeneratedAction()
    {
        $this->setDefaultViewVars();

        $this->view->setVar('advertiser', $this->dispatcher->getParam('advertiser'));
    }


    protected function invalidToken(Advertiser $advertiser)
    {
        if ($this->request->isPost()) {

            $this->dispatcher->setParam('advertiser', $advertiser);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action' => 'tokenGenerated',
            ]);

        } else {

            $this->flash->error($this->translate('ad.label.invalid_token'));
            $this->dispatcher->setParam('advertiser', $advertiser);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action' => 'requestToken',
            ]);
        }
    }

    protected function manage(Advertiser $advertiser)
    {
        $this->info(sprintf('Managing advertiser %s <%s> ads', $advertiser->id, $advertiser->email));

        $ads = $this->findAdsByAdvertiser($advertiser);
        $promotions = $this->findPromotionsByAdvertiser($advertiser);

        $this->setDefaultViewVars();
        $this->view->setVar('advertiser', $advertiser);
        $this->view->setVar('ads', $this->assignPromotions($ads, $promotions));
        $this->view->setVar('promotions', $promotions);
        $this->view->setVar('form', new PromoteForm());
    }

    protected function generateToken(Advertiser $entity)
    {
        $form = new TokenForm();

        if ($form->isValid($this->request->getPost())) {
            $manager = new AdvertiserModelManager($entity);
            $manager->setDI($this->di);

            $this->info('Generated token link is /anunciante/'. $entity->token);

            try {
                $manager->save();
            } catch (\Throwable $t) {
                $this->error(sprintf('Failed to generate token: %s', $t->getMessage()));

                return $this->internalServerError($t);
            }

            $this->info('Succeed, forward to @token_success');

            if ($form->notifyBySms()) {
                $this->flash->success($this->translate('manage.label.token_notified_by_sms'));
            } else {
                $this->flash->success($this->translate('manage.label.token_notified'));
            }

            $this->dispatcher->setParam('entity', $entity);
            $this->dispatcher->forward([
                'controller' => 'manage',
                'action'     => 'tokenGenerated',
            ]);

        } else {
            $this->error(sprintf('Failed to validate token form: %s', FormUtil::errorsAsString($form)));
            $this->addValidationErrorsToFlash($form, 'manage.label.');
            $this->setDefaultViewVars();

            $this->view->setVar('form', $form);
            $this->view->setVar('entity', $entity);
        }
    }


    protected function getAdRepository(): Repository
    {
        return $this->getRepository(Ad::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getPromotionRepository(): Repository
    {
        return $this->getRepository(Promotion::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function findAdBySlug(string $slug)
    {
        return $this->getAdRepository()->findOneBy(sprintf('slug = "%s"', $slug));
    }

    protected function findAdsByAdvertiser(Advertiser $advertiser)
    {
        return $this->getAdRepository()->findBy([
            sprintf('advertiserId = "%s"', $advertiser->id),
            'order' => 'createdAt DESC',
        ]);
    }

    protected function findPromotionsByAdvertiser(Advertiser $advertiser)
    {
        return $this->getPromotionRepository()->findByAdvertiser($advertiser);
    }

    protected function findAdvertiserByToken(string $token)
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('token = "%s"', $token));
    }

    protected function assignPromotions($ads, $promotions)
    {
        $adIndex = [];

        foreach ($ads as $ad) {
            $adIndex[$ad->id] = clone $ad;
        }

        foreach ($promotions as /** @var Promotion $promotion */ $promotion) {
            $adIndex[$promotion->adId]->promos[] = $promotion;

            if ($promotion->isActive()) {
                $adIndex[$promotion->adId]->promoted = true;
            }
        }

        return array_values($adIndex);
    }
}
