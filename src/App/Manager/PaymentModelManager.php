<?php

namespace App\Manager;

use App\Model\Payment;

class PaymentModelManager extends ModelManager
{

    /**
     * @var Payment
     */
    protected $model;

    protected function doSave()
    {
        $this->doSaveModel($this->model);
    }
}