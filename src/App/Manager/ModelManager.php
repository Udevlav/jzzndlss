<?php

namespace App\Manager;

use App\Repository\EntityManager;

use Phalcon\Db\AdapterInterface;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\MessageInterface;
use Phalcon\Mvc\User\Component;

class ModelManager extends Component
{

    /**
     * @param ModelInterface $model
     *
     * @return array[]
     */
    static public function errors(ModelInterface $model): array
    {
        $messages = [];

        foreach ($model->getMessages() as $message) {
            /** @var MessageInterface $message */
            $messages[] = sprintf('%s %s: %s',
                $message->getType(),
                $message->getField(),
                $message->getMessage()
            );
        }

        return $messages;
    }

    /**
     * @param ModelInterface $model
     *
     * @return string
     */
    static public function errorsAsString(ModelInterface $model): string
    {
        return implode(', ', static::errors($model));
    }


    /**
     * @var bool
     */
    protected $new;

    /**
     * @var ModelInterface
     */
    protected $model;


    /**
     * @param ModelInterface $model
     */
    public function __construct(ModelInterface $model)
    {
        $this->model = $model;
    }


    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return empty($this->model->id);
    }


    /**
     * @return ModelInterface
     */
    public function getModel(): ModelInterface
    {
        return $this->model;
    }

    /**
     * @return void
     *
     * @throws PersistenceException
     */
    public function save()
    {
        $db = $this->getDb();
        $db->begin();

        try {
            $this->doSave();

            $result = $db->commit();
        } catch (PersistenceException $e) {
            $db->rollback();

            throw $e;
        }

        if (! $result) {
            throw new PersistenceException(sprintf('Failed to persist %s: %s.',
                is_object($this->getModel()) ? get_class($this->getModel()) : gettype($this->getModel()),
                static::errorsAsString($this->getModel())
            ));
        }
    }

    /**
     * @return void
     *
     * @throws PersistenceException
     */
    public function delete()
    {
        $db = $this->getDb();
        $db->begin();

        try {
            $this->doDelete();

            $db->commit();

        } catch (PersistenceException $e) {
            $db->rollback();

        } catch (\Throwable $t) {
            // This should not happend if the model is well-defined
            // but may happen, e.g., if some field is unserialized
            // and serialized on read and write operations
            throw new PersistenceException(sprintf('Low-level persistence error. Check your %s model (hint: %s)', get_class($this->model), $t->getMessage()), $t->getCode(), $t);
        }
    }


    /**
     * @return AdapterInterface
     */
    protected function getDb(): AdapterInterface
    {
        return $this->di->get('db');
    }

    /**
     * @return EntityManager
     */
    protected function getEm(): EntityManager
    {
        return $this->di->get('em');
    }

    /**
     * This is a template method intended to be overwriten in concrete
     * sub-classes.
     *
     * @return void
     *
     * @throws PersistenceException
     */
    protected function doSave()
    {
        $this->doSaveModel($this->model);
    }

    /**
     * This is a template method intended to be overwriten in concrete
     * sub-classes.
     *
     * @return void
     *
     * @throws PersistenceException
     */
    protected function doDelete()
    {
        $this->doDeleteModel($this->model);
    }

    /**
     * @param ModelInterface $model
     *
     * @return void
     *
     * @throws PersistenceException
     */
    final protected function doSaveModel(ModelInterface $model)
    {
        if (false === $model->save()) {
            throw PersistenceException::saving($model);
        }
    }

    /**
     * @param ModelInterface $model
     *
     * @return void
     *
     * @throws PersistenceException
     */
    final protected function doDeleteModel(ModelInterface $model)
    {
        if (false === $model->delete()) {
            throw PersistenceException::removing($model);
        }
    }
}