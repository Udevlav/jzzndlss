<?php

namespace App\Manager;

use App\Config\Config;
use App\Geo\GeoLocation;
use App\I18n\Translator;
use App\IO\PathGenerator;
use App\Mailer\Mailer;
use App\Model\Ad;
use App\Model\AdTag;
use App\Model\Advertiser;
use App\Model\Category;
use App\Model\Layout;
use App\Model\Media;
use App\Model\Provider;
use App\Repository\AdTagRepository;
use App\Repository\MediaRepository;
use App\Repository\Repository;
use App\Sms\Sms;
use App\Token\TokenGenerator;
use App\Util\StringUtil;

use Imagine\Image\ImagineInterface;

use DateTime;
use LogicException;

class AdModelManager extends ModelManager
{

    /**
     * @var Ad
     */
    protected $model;

    /**
     * @var Advertiser
     */
    protected $advertiser;

    /**
     * @var GeoLocation
     */
    protected $geolocation;

    /**
     * @var array
     */
    protected $newTags = [];

    /**
     * @var array
     */
    protected $newMedias = [];

    /**
     * @var mixed
     */
    protected $posterId;

    /**
     * @var bool
     */
    protected $notifyByEmail = true;

    /**
     * @var bool
     */
    protected $notifyBySms = false;

    /**
     * @var int
     */
    protected $newImages = 0;

    /**
     * @var int
     */
    protected $newVideos = 0;


    /**
     * @param mixed $id
     *
     * @return void
     */
    public function addTag($id)
    {
        if (! empty($id) && ! in_array($id, $this->newTags)) {
            $this->newTags[] = $id;
        }
    }

    /**
     * @param mixed $id
     * @param bool $video
     *
     * @return void
     */
    public function addMedia($id, bool $video = false)
    {
        if (! empty($id) && ! in_array($id, $this->newMedias)) {
            $this->newMedias[] = $id;

            if ($video) {
                $this->newVideos ++;
            } else {
                $this->newImages ++;
            }
        }
    }

    /**
     * @param string $posterId
     *
     * @return void
     */
    public function setPosterId($posterId)
    {
        $this->posterId = $posterId;
    }

    /**
     * @param bool $enable
     *
     * @return void
     */
    public function setNotifyByEmail(bool $enable)
    {
        $this->notifyByEmail = $enable;
    }

    /**
     * @param bool $enable
     *
     * @return void
     */
    public function setNotifyBySms(bool $enable)
    {
        $this->notifyBySms = $enable;
    }

    /**
     * @param GeoLocation $geolocation
     *
     * @return void
     */
    public function setGeoLocation(GeoLocation $geolocation)
    {
        $this->geolocation = $geolocation;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Mailer
     */
    protected function getMailer(): Mailer
    {
        return $this->di->get('mailer');
    }

    /**
     * @return Sms
     */
    protected function getSms(): Sms
    {
        return $this->di->get('sms');
    }

    /**
     * @return TokenGenerator
     */
    protected function getTokenGenerator(): TokenGenerator
    {
        return $this->di->get('tokenGenerator');
    }

    /**
     * @return Translator
     */
    protected function getTranslator(): Translator
    {
        return $this->di->get('translator');
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $new = $this->isNew();

        // Generate all private (internal) fields
        $this->generatePrivateFields();

        // Save all
        $this->saveAdvertiser();
        $this->saveAd();
        $this->saveTags($new);
        $this->saveMedias($new);

        // Notify
        if ($this->notifyByEmail) {
            $this->notify();
        }

        if ($this->notifyBySms) {
            $this->notifySms();
        }
    }


    protected function getAdTagRepository(): AdTagRepository
    {
        /** @var AdTagRepository $repository */
        $repository = $this->getEm()
            ->getRepository(AdTag::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getEm()
            ->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getMediaRepository(): Repository
    {
        return $this->getEm()
            ->getRepository(Media::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getPathGenerator(): PathGenerator
    {
        return $this->di->getShared('pathGenerator');
    }

    protected function getImagine(): ImagineInterface
    {
        return $this->di->getShared('imagine');
    }

    protected function getCurrentTagIds(): array
    {
        $ids = [];

        foreach ($this->getAdTagRepository()->findByAd($this->model) as $tag) {
            $ids[] = $tag->tagId;
        }

        return $ids;
    }

    protected function getCurrentMediaIds(): array
    {
        $ids = [];

        /** @var MediaRepository $repository */
        $repository = $this->getMediaRepository();

        foreach ($repository->findMastersByAd($this->model) as $media) {
            $ids[] = $media->id;
        }

        return $ids;
    }

    protected function getAdvertiserByEmail()
    {
        return $this->getAdvertiserRepository()
            ->findOneBy(sprintf('email = "%s"', $this->model->email)) ?? null;
    }

    protected function getPoster()
    {
        $posterId = $this->posterId;

        if (empty($posterId) && $this->newImages > 0) {
            // Select first one
            $posterId = $this->newMedias[0];
        }

        $media = $this->getMediaRepository()->find($posterId);

        // In the worst case, we won't know the model id, which may be
        // required by the path generator to set the target path, so the
        // best we can do for now is to use the temp path -- the poster
        // path will be generated on publication
        return str_replace($this->getPublicDir(), '/', $media->path);
    }

    protected function getLayout()
    {
        $path = sprintf('%s%s', $this->getConfig()->layout->publicDir, ltrim($this->model->poster, '/'));

        if (! file_exists($path)) {
            $path = $this->getPathGenerator()->generateTempPath($this->model->poster);
        }

        $size = $this->getImagine()->open($path)->getSize();

        return $size->getHeight() > $size->getWidth() ? Layout::PORTRAIT : Layout::LANDSCAPE;
    }

    protected function getPublicDir()
    {
        return $this->getConfig()->layout->publicDir;
    }

    protected function getUploadDir()
    {
        return $this->getConfig()->layout->uploadDir;
    }


    /**
     * @return void
     */
    protected function notify()
    {
        $subject = $this->getTranslator()->translate('mail.subject.ad_created');

        $this->getMailer()->send($this->model->email, $subject, '../mail/created.volt', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->geolocation,
            'subject'     => $subject,
            'entity'      => $this->model,
            'advertiser'  => $this->advertiser,
        ]);
    }

    /**
     * @return void
     */
    protected function notifySms()
    {
        $this->getSms()->send($this->model->phone, '../sms/created.volt', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->geolocation,
            'entity'      => $this->model,
            'advertiser'  => $this->advertiser,
        ]);
    }


    protected function generatePrivateFields()
    {
        // Categories were specified as integer in previous versions
        if (is_numeric($this->model->category)) {
            $this->model->category = Category::name($this->model->category);
        }

        $this->model->poster = $this->getPoster();
        $this->model->layout = $this->getLayout();
        $this->model->slug = StringUtil::sluggify($this->model->title);
        $this->model->provider = Provider::USER;
        $this->model->numExperiences = 0;
        $this->model->numImages = $this->newImages;
        $this->model->numVideos = $this->newVideos;
        $this->model->updatedFrom = $this->geolocation->getIp();
        $this->model->generateHash();

        if ($this->isNew()) {
            $this->model->createdFrom = $this->geolocation->getIp();
            $this->model->publishedAt = null;
            $this->model->lastHitAt = null;
        }
    }

    protected function generateTargetPath(Media $media)
    {
        return $this->getPathGenerator()->generatePublicPath($this->model, $media);
    }

    protected function isTokenExpired(string $validFrom, string $validTo)
    {
        $now = new DateTime();

        if ($now < DateTime::createFromFormat('Y-m-d H:i:s', $validFrom)) {
            return true;
        }

        if ($now > DateTime::createFromFormat('Y-m-d H:i:s', $validTo)) {
            return true;
        }

        return false;
    }

    protected function createAdvertiser(): Advertiser
    {
        $advertiser = new Advertiser();

        $advertiser->name = $this->model->name;
        $advertiser->age = $this->model->age;
        $advertiser->email = $this->model->email;
        $advertiser->phone = $this->model->phone;

        return $advertiser;
    }

    protected function saveAd()
    {
        $this->doSaveModel($this->model);
    }

    protected function saveAdvertiser()
    {
        // Either fetch or create the advertiser
        $advertiser = $this->getAdvertiserByEmail();

        if (! $advertiser) {
            if (! $this->isNew()) {
                // This should not happen (editing a model without an advertiser ???)
                // such model could not be created
                throw new LogicException('L001');
            }

            // Create a new advertiser
            $advertiser = $this->createAdvertiser();
        }

        // Generate advertiser token if necessary
        if (empty($advertiser->token) || $this->isTokenExpired($advertiser->tokenValidFrom, $advertiser->tokenValidTo)) {
            $token = $this->getTokenGenerator()->generateToken(time() . uniqid());

            $advertiser->token = $token->getHash();
            $advertiser->tokenValidFrom = $token->getValidFrom()->format('Y-m-d H:i:s');
            $advertiser->tokenValidTo = $token->getValidTo()->format('Y-m-d H:i:s');
        }

        // Increment number of ads for that advertiser
        $advertiser->numAds ++;

        // Save advertiser and update ad's advertiser ID
        $this->doSaveModel($advertiser);

        $this->model->advertiserId = $advertiser->id;
        $this->advertiser = $advertiser;
    }

    protected function saveTags(bool $new = false)
    {
        if ($new) {

            // Simplest case: just save all new tags
            foreach ($this->newTags as $id) {
                $tag = new AdTag();

                $tag->adId = $this->model->id;
                $tag->tagId = $id;

                $this->doSaveModel($tag);
            }

        } else {

            // Hardest case: check for existing tags
            $currentTags = $this->getCurrentTagIds();

            // Add new tags
            foreach (array_values(array_diff($this->newTags, $currentTags)) as $id) {
                $tag = new AdTag();

                $tag->adId = $this->model->id;
                $tag->tagId = $id;

                $this->doSaveModel($tag);
            }

            // Remove unused tags
            $this
                ->getEm()
                ->getRepository(AdTag::class)
                ->disableCache()
                ->removeIn(array_values(array_diff($currentTags, $this->newTags)));
        }
    }

    protected function saveMedias(bool $new = false)
    {
        if ($new) {

            // Simplest case: just update all new media
            $newMedias = $this
                ->getEm()
                ->getRepository(Media::class)
                ->disableCache()
                ->findIn($this->newMedias);

            foreach ($newMedias as $media) {
                $media->adId = $this->model->id;
                $media->temp = false;

                if ($this->posterId && $media->id === $this->posterId) {
                    $media->poster = true;
                } else {
                    $media->poster = false;
                }

                $this->doSaveModel($media);
            }

        } else {

            // Hardest case: check for existing media
            $currentMedias = $this->getCurrentMediaIds();

            // Add new media
            $ids = array_values(array_diff($this->newMedias, $currentMedias));

            if (count($ids)) {
                $newMedias = $this
                    ->getEm()
                    ->getRepository(Media::class)
                    ->disableCache()
                    ->findIn($ids);

                foreach ($newMedias as $media) {
                    $media->adId = $this->model->id;
                    $media->temp = false;

                    if ($this->posterId && $media->id === $this->posterId) {
                        $media->poster = true;
                    } else {
                        $media->poster = false;
                    }

                    $this->doSaveModel($media);
                }
            }

            // Remove unused media
            $ids = array_values(array_diff($currentMedias, $this->newMedias));

            if (count($ids)) {
                $this
                    ->getEm()
                    ->getRepository(Media::class)
                    ->disableCache()
                    ->removeIn($ids);
            }
        }
    }
}