<?php

namespace App\Manager;

use Phalcon\Mvc\ModelInterface;

class PersistenceException extends \RuntimeException
{

    static public function removing(ModelInterface $model): PersistenceException
    {
        return new static(static::generateMessage('save', $model));
    }

    static public function saving(ModelInterface $model): PersistenceException
    {
        return new static(static::generateMessage('save', $model));
    }


    static private function generateMessage(string $action, ModelInterface $model): string
    {
        return sprintf('Failed to %s %s entity: %s.', $action, get_class($model), ModelManager::errorsAsString($model));
    }

}