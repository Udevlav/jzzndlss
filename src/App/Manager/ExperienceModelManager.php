<?php

namespace App\Manager;

use App\Model\Ad;
use App\Model\Experience;

use LogicException;

class ExperienceModelManager extends ModelManager
{

    /**
     * @var Experience
     */
    protected $model;

    /**
     * @var Ad
     */
    protected $ad;

    public function setAd(Ad $ad)
    {
        $this->ad = $ad;
    }

    protected function doSave()
    {
        if (null === $this->ad) {
            throw new LogicException('You should set an ad first.');
        }

        $this->saveExperience();
        $this->saveAd();
    }

    protected function saveExperience()
    {
        $this->model->adId = $this->ad->id;
        $this->model->rating = intval(max(0, min(5, $this->model->rating)));

        $this->doSaveModel($this->model);
    }

    protected function saveAd()
    {
        if (empty($this->ad->numExperiences)) {
            $this->ad->numExperiences = 1;
        } else {
            $this->ad->numExperiences = 1 + intval($this->ad->numExperiences);
        }

        if (empty($this->ad->rating)) {
            $this->ad->rating = $this->model->rating;
        } else {
            $this->ad->rating = ($this->ad->rating + $this->model->rating) / 2;
        }

        $this->doSaveModel($this->ad);
    }
}