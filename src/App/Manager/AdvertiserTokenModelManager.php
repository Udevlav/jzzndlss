<?php

namespace App\Manager;

use App\Config\Config;
use App\I18n\Translator;
use App\Mailer\Mailer;
use App\Model\Advertiser;
use App\Sms\Sms;
use App\Token\TokenGenerator;

class AdvertiserTokenModelManager extends ModelManager
{

    /**
     * @var Advertiser
     */
    protected $model;

    /**
     * @var bool
     */
    protected $notifyBySms = false;

    /**
     * @param bool $enable
     *
     * @return void
     */
    public function setNotifyBySms(bool $enable)
    {
        $this->notifyBySms = $enable;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Mailer
     */
    protected function getMailer(): Mailer
    {
        return $this->di->get('mailer');
    }

    /**
     * @return Sms
     *
     */
    protected function getSms(): Sms
    {
        return $this->di->get('sms');
    }

    /**
     * @return TokenGenerator
     */
    protected function getTokenGenerator(): TokenGenerator
    {
        return $this->di->get('tokenGenerator');
    }

    /**
     * @return Translator
     */
    protected function getTranslator(): Translator
    {
        return $this->di->get('translator');
    }

    /**
     * @inheritdoc
     */
    protected function doDelete()
    {
        throw new \LogicException();
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        // Generate a new token
        $this->generateToken();

        // Save advertiser
        $this->saveAdvertiser();

        // Notify
        $this->notify();

        if ($this->notifyBySms) {
            $this->notifySms();
        }
    }

    protected function notify()
    {
        $subject = $this->getTranslator()->translate('mail.subject.token_generated');

        $this->getMailer()->send($this->model->email, $subject, '../mail/manage.volt', [
            'config'      => $this->getConfig(),
            'subject'     => $subject,
            'entity'      => $this->model,
        ]);
    }

    protected function notifySms()
    {
        $this->getSms()->send($this->model->phone, '../sms/manage.volt', [
            'config'      => $this->getConfig(),
            'entity'      => $this->model,
        ]);
    }

    protected function generateToken()
    {
        $token = $this->getTokenGenerator()->generateToken(time() . uniqid());

        $this->model->token = $token->getHash();
        $this->model->tokenValidFrom = $token->getValidFrom()->format('Y-m-d H:i:s');
        $this->model->tokenValidTo = $token->getValidTo()->format('Y-m-d H:i:s');
    }

    protected function saveAdvertiser()
    {
        $this->doSaveModel($this->model);
    }
}