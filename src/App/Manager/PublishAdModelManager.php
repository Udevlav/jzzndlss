<?php

namespace App\Manager;

use App\Config\Config;
use App\Geo\GeoLocation;
use App\I18n\Translator;
use App\IO\PathGenerator;
use App\Mailer\Mailer;
use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Layout;
use App\Model\Media;
use App\Rank\Ranker;
use App\Repository\MediaRepository;
use App\Repository\Repository;
use App\Sms\Sms;

use DateTime;

class PublishAdModelManager extends ModelManager
{

    /**
     * @var Ad
     */
    protected $model;

    /**
     * @var Advertiser
     */
    protected $advertiser;

    /**
     * @var GeoLocation
     */
    protected $geolocation;

    /**
     * @var bool
     */
    protected $notifyBySms = false;

    /**
     * @param bool $enable
     *
     * @return void
     */
    public function setNotifyBySms(bool $enable)
    {
        $this->notifyBySms = $enable;
    }

    /**
     * @param GeoLocation $geolocation
     *
     * @return void
     */
    public function setGeoLocation(GeoLocation $geolocation)
    {
        $this->geolocation = $geolocation;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Mailer
     */
    protected function getMailer(): Mailer
    {
        return $this->di->get('mailer');
    }

    /**
     * @return PathGenerator
     */
    protected function getPathGenerator(): PathGenerator
    {
        return $this->di->get('pathGenerator');
    }

    /**
     * @return Sms
     */
    protected function getSms(): Sms
    {
        return $this->di->get('sms');
    }

    /**
     * @return Ranker
     */
    protected function getRanker(): Ranker
    {
        return $this->di->get('ranker');
    }

    /**
     * @return Translator
     */
    protected function getTranslator(): Translator
    {
        return $this->di->get('translator');
    }

    /**
     * @return string
     */
    protected function getPublicDir(): string
    {
        return $this->getConfig()->layout->publicDir;
    }


    /**
     * @inheritdoc
     */
    protected function doDelete()
    {
        parent::doDelete();
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        // Save ad & notify
        $this->saveAd();

        $this->notify();

        if ($this->notifyBySms) {
            $this->notifySms();
        }
    }


    protected function getAdvertiserRepository(): Repository
    {
        return $this->getEm()
            ->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getMediaRepository(): MediaRepository
    {
        /** @var MediaRepository $repository */
        $repository = $this->getEm()
            ->getRepository(Media::class)
            ->disableCache()
            ->hydrateRecords();

        return $repository;
    }

    protected function getAdPoster()
    {
        return $this->getMediaRepository()->findPoster($this->model);
    }

    protected function getAdvertiserByEmail()
    {
        return $this->getAdvertiserRepository()->findOneBy(sprintf('email = "%s"', $this->model->email));
    }


    protected function saveAd()
    {
        $path = $this->getPathGenerator()->generatePublicPath($this->model, $this->getAdPoster());

        $this->model->numHits = 1;
        $this->model->score = $this->getRanker()->rank($this->model);
        $this->model->publishedAt = (new DateTime())->format('Y-m-d H:i:s');
        $this->model->poster = $this->fixPosterPath($path);
        $this->model->layout = $this->fixLayout();
        $this->model->generateHash();
        $this->model->serializeColumns();

        $this->doSaveModel($this->model);
    }

    protected function notify()
    {
        $subject = $this->getTranslator()->translate('mail.subject.ad_published');

        $this->getMailer()->send($this->model->email, $subject, '../mail/published.volt', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->geolocation,
            'subject'     => $subject,
            'entity'      => $this->model,
            'advertiser'  => $this->getAdvertiserByEmail(),
        ]);
    }

    protected function notifySms()
    {
        $this->getSms()->send($this->model->phone, '../sms/created.volt', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->geolocation,
            'entity'      => $this->model,
            'advertiser'  => $this->getAdvertiserByEmail(),
        ]);
    }

    protected function fixLayout(): string
    {
        $poster = $this->getAdPoster();

        if ($poster instanceof Media) {
            if ($poster->isPortraitLoRes()) {
                return Layout::PORTRAIT_LORES;
            }

            if ($poster->isPortrait()) {
                return Layout::PORTRAIT;
            }
        }

        return Layout::LANDSCAPE;
    }

    protected function fixPosterPath(string $path): string
    {
        $config = $this->getConfig();
        $pattern = sprintf('/\.(%s)$/', pathinfo($path, PATHINFO_EXTENSION));

        return preg_replace($pattern, '.jpg', str_replace($config->layout->mediaDir, '/media/', $path));
    }
}