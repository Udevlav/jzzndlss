<?php

namespace App\Manager;

use App\Geo\GeoLocation;

use App\Model\Ad;
use App\Model\Hit;

use App\Rank\Ranker;
use Phalcon\Http\Request;

class HitModelManager extends ModelManager
{

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * @var Hit
     */
    protected $model;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var GeoLocation
     */
    protected $geolocation;


    /**
     * @param Ad $ad
     *
     * @return void
     */
    public function setAd(Ad $ad)
    {
        $this->ad = $ad;
    }

    /**
     * @param Request $request
     *
     * @return void
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param GeoLocation $geolocation
     *
     * @return void
     */
    public function setGeoLocation(GeoLocation $geolocation)
    {
        $this->geolocation = $geolocation;
    }


    /**
     * @return Ranker
     */
    protected function getRanker(): Ranker
    {
        return $this->di->get('ranker');
    }


    /**
     * @inheritdoc
     */
    protected function doDelete()
    {
        parent::doDelete();
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $this->generateFields();
        $this->doSaveModel($this->model);
        $this->updateAd();
    }


    protected function generateFields()
    {
        $this->model->adId = $this->ad->id;
        $this->model->hitAt = (new \DateTime())->format('Y-m-d H:i:s');
        $this->model->fingerprint = $this->generateFingerprint();
    }

    protected function generateFingerprint()
    {
        return $this->geolocation->getIp();
    }

    protected function updateAd()
    {
        $this->ad->numHits ++;
        $this->ad->lastHitAt = (new \DateTime())->format('Y-m-d H:i:s');
        //$this->ad->score = $this->getRanker()->rank($this->ad);
        $this->ad->score ++;

        $this->doSaveModel($this->ad);
    }
}
