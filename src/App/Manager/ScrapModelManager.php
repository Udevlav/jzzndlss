<?php

namespace App\Manager;

use App\Model\Advertiser;
use App\Model\Provider;
use App\Repository\MediaRepository;
use App\Util\StringUtil;

class ScrapModelManager extends AdModelManager
{

    protected function createAdvertiser(): Advertiser
    {
        $advertiser = parent::createAdvertiser();
        $advertiser->name = $this->model->provider;

        return $advertiser;
    }

    protected function generatePrivateFields()
    {
        $this->model->poster = $this->getPoster();
        $this->model->slug = StringUtil::sluggify($this->model->title);
        $this->model->numImages = $this->newImages;
        $this->model->numVideos = $this->newVideos;
        $this->model->createdFrom = '127.0.0.1';
        $this->model->updatedFrom = '127.0.0.1';
        $this->model->generateHash();

        if (! $this->model->provider) {
            $this->model->provider = Provider::SCRAP;
        }
    }

    protected function saveMedias(bool $new = false)
    {
        /** @var MediaRepository $repository */
        $repository = $this->getMediaRepository();
        $repository->updateAdMedia($this->model, $this->newMedias);
    }
}