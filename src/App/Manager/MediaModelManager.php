<?php

namespace App\Manager;

use App\Config\Config;
use App\Geo\GeoLocation;
use App\Model\Media;

use App\Sex\Hasher;
use Phalcon\Http\Request\FileInterface;
use Phalcon\Mvc\ModelInterface;

use LogicException;
use RuntimeException;
use Throwable;

class MediaModelManager extends ModelManager
{

    /**
     * @var Media
     */
    protected $model;

    /**
     * @var FileInterface
     */
    protected $file;

    /**
     * @var GeoLocation
     */
    protected $geolocation;


    public function __construct(ModelInterface $model)
    {
        if (! function_exists('\\finfo_open')) {
            throw new RuntimeException('Fileinfo extension not loaded');
        }

        parent::__construct($model);
    }


    /**
     * @param FileInterface $file
     *
     * @return void
     */
    public function setFile(FileInterface $file)
    {
        $this->file = $file;
    }

    /**
     * @param GeoLocation $geolocation
     *
     * @return void
     */
    public function setGeoLocation(GeoLocation $geolocation)
    {
        $this->geolocation = $geolocation;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Hasher
     */
    protected function getHasher(): Hasher
    {
        return $this->di->get('hasher');
    }

    /**
     * @return string
     */
    protected function getPublicDir(): string
    {
        return $this->getConfig()->layout->publicDir;
    }

    /**
     * @return string
     */
    protected function getUploadDir(): string
    {
        return $this->getConfig()->layout->uploadDir;
    }


    /**
     * @inheritdoc
     */
    protected function doDelete()
    {
        // The file is completely useless in the filesystem without a
        // valid, persistent, record pointing to it
        @unlink(rtrim($this->getPublicDir(), '/') . $this->model->path);

        parent::doDelete();
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $path = $this->generatePath();

        // We assume the file was completely uploaded in one request;
        // just move to target path and persist the media
        if (false === $this->file->moveTo($this->getUploadDir() . $path)) {
            throw new PersistenceException('Failed to move upload from temporary storage to upload dir.');
        }

        // Generate all media fields from the file
        $this->generateMediaFields($path);

        try {
            parent::doSave();
        } catch (Throwable $t) {
            // The file is completely useless in the filesystem without a
            // valid, persistent, record pointing to it
            unlink($this->getUploadDir() . $path);

            throw $t;
        }
    }


    private function relativizePath(string $path): string
    {
        return str_replace($this->getPublicDir(), '/', $path);
    }

    private function generatePath(): string
    {
        if (null === $this->file) {
            throw new LogicException('You should set a file before saving a media.');
        }

        return $this->getHasher()->hash(
            $this->file->getName(),
            $this->file->getTempName(),
            time()
        ) .'.tmp';
    }

    private function generateType(string $path)
    {
        $finfo = @finfo_open(FILEINFO_MIME_TYPE);

        return @finfo_file($finfo, $path);
    }

    private function generateMediaFields(string $path)
    {
        $fullpath = $this->getUploadDir() . $path;

        $this->model->temp = true;
        $this->model->poster = false;
        $this->model->name = $this->file->getName();
        $this->model->path = $this->relativizePath($fullpath);
        $this->model->version = null;
        $this->model->type = $this->generateType($fullpath);
        $this->model->size = filesize($fullpath);
        $this->model->width = null;
        $this->model->height = null;
        $this->model->duration = null;
        $this->model->timeCode = null;

        if ($this->isNew()) {
            $this->model->uploaderIp = $this->geolocation->getIp();
        }
    }
}