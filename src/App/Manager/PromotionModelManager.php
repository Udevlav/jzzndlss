<?php

namespace App\Manager;

use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Promotion;

class PromotionModelManager extends ModelManager
{

    /**
     * @var Promotion
     */
    protected $model;

    /**
     * @var Advertiser
     */
    protected $advertiser;

    /**
     * @var Ad
     */
    protected $ad;


    /**
     * @param Advertiser $advertiser
     *
     * @return void
     */
    public function setAdvertiser(Advertiser $advertiser)
    {
        $this->advertiser = $advertiser;
    }

    /**
     * @param Ad $ad
     *
     * @return void
     */
    public function setAd(Ad $ad)
    {
        $this->ad = $ad;
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $this->generateFields();
        $this->consumeCredits();
        $this->savePromotion();
    }


    protected function generateFields()
    {
        $this->model->adId = $this->ad->id;
        $this->model->validFrom = date('Y-m-d H:i:s', $this->model->validFrom);
        $this->model->validTo = date('Y-m-d H:i:s', $this->model->validTo);
    }

    protected function consumeCredits()
    {
        $this->advertiser->credits = max(0, $this->advertiser->credits - $this->model->credits);

        $this->doSaveModel($this->advertiser);
    }

    protected function savePromotion()
    {
        $this->doSaveModel($this->model);
    }
}