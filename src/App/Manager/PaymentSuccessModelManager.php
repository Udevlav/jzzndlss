<?php

namespace App\Manager;

use App\Model\Advertiser;

use LogicException;

class PaymentSuccessModelManager extends PaymentModelManager
{

    /**
     * @var Advertiser
     */
    protected $advertiser;

    public function setAdvertiser(Advertiser $advertiser)
    {
        $this->advertiser = $advertiser;
    }

    protected function doSave()
    {
        if (null === $this->advertiser) {
            throw new LogicException('You should set an advertiser first.');
        }

        $this->savePayment();
        $this->saveAdvertiser();
    }

    protected function savePayment()
    {
        $this->doSaveModel($this->model);
    }

    protected function saveAdvertiser()
    {
        if (empty($this->advertiser->credits)) {
            $this->advertiser->credits = 0;
        }

        $this->advertiser->credits += $this->model->creditsGranted;

        $this->doSaveModel($this->advertiser);
    }
}