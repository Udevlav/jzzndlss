<?php

namespace App\Manager;

use App\Model\Ad;
use App\Model\Advertiser;
use App\Model\Provider;
use App\Repository\Repository;
use App\Token\TokenGenerator;

use DateTime;

class AssignModelManager extends ModelManager
{

    /**
     * @var Ad
     */
    protected $model;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var Advertiser | null
     */
    protected $advertiser;


    /**
     * @param string $email
     *
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $advertiser = $this->getAdvertiserByEmail();

        if (! $advertiser) {
            $advertiser = new Advertiser();

            $advertiser->name = $this->model->name;
            $advertiser->age = $this->model->age;
            $advertiser->phone = $this->model->phone;
            $advertiser->numAds = 0;
            $advertiser->email = $this->email;
        }

        // Generate advertiser token if necessary
        $empty = empty($advertiser->token);

        if ($empty || $this->isTokenExpired($advertiser->tokenValidFrom, $advertiser->tokenValidTo)) {
            $token = $this->getTokenGenerator()->generateToken(time() . uniqid());

            $advertiser->token = $token->getHash();
            $advertiser->tokenValidFrom = $token->getValidFrom()->format('Y-m-d H:i:s');
            $advertiser->tokenValidTo = $token->getValidTo()->format('Y-m-d H:i:s');
        }

        // Increment number of ads for that advertiser
        $advertiser->numAds ++;

        // Save advertiser and update ad's advertiser ID
        $this->doSaveModel($advertiser);

        $this->model->advertiserId = $advertiser->id;
        $this->model->provider = Provider::USER;

        $this->doSaveModel($this->model);
    }

    protected function getAdvertiserRepository(): Repository
    {
        return $this->getEm()
            ->getRepository(Advertiser::class)
            ->disableCache()
            ->hydrateRecords();
    }

    protected function getAdvertiserByEmail()
    {
        return $this->getAdvertiserRepository()
                ->findOneBy(sprintf('email = "%s"', $this->email)) ?? null;
    }

    /**
     * @return TokenGenerator
     */
    protected function getTokenGenerator(): TokenGenerator
    {
        return $this->di->get('tokenGenerator');
    }

    protected function isTokenExpired(string $validFrom, string $validTo)
    {
        $now = new DateTime();

        if ($now < DateTime::createFromFormat('Y-m-d H:i:s', $validFrom)) {
            return true;
        }

        if ($now > DateTime::createFromFormat('Y-m-d H:i:s', $validTo)) {
            return true;
        }

        return false;
    }

}
