<?php

namespace App\Manager

class ValidationError
{

    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $error;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $template;

    /**
     * @var string[]
     */
    public $arguments;

    /**
     * @var mixed
     */
    public $data;
}