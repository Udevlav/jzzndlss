<?php

namespace App\Manager;

use App\Config\Config;
use App\Geo\GeoLocation;
use App\I18n\Translator;
use App\Mailer\Mailer;
use App\Model\Ad;
use App\Model\AdTag;
use App\Model\Advertiser;
use App\Model\Media;
use App\Model\Provider;
use App\Repository\AdTagRepository;
use App\Repository\MediaRepository;
use App\Repository\Repository;
use App\Sms\Sms;
use App\Token\TokenGenerator;
use App\Util\StringUtil;

class AdvertiserModelManager extends ModelManager
{

    /**
     * @var Advertiser
     */
    protected $model;

    /**
     * @var GeoLocation
     */
    protected $geolocation;

    /**
     * @var bool
     */
    protected $notifyBySms = false;


    /**
     * @param GeoLocation $geolocation
     *
     * @return void
     */
    public function setGeoLocation(GeoLocation $geolocation)
    {
        $this->geolocation = $geolocation;
    }

    /**
     * @param bool $enable
     *
     * @return void
     */
    public function setNotifyBySms(bool $enable)
    {
        $this->notifyBySms = $enable;
    }


    /**
     * @return Config
     */
    protected function getConfig(): Config
    {
        return $this->di->get('config');
    }

    /**
     * @return Mailer
     */
    protected function getMailer(): Mailer
    {
        return $this->di->get('mailer');
    }

    /**
     * @return Sms
     */
    protected function getSms(): Sms
    {
        return $this->di->get('sms');
    }

    /**
     * @return TokenGenerator
     */
    protected function getTokenGenerator(): TokenGenerator
    {
        return $this->di->get('tokenGenerator');
    }

    /**
     * @return Translator
     */
    protected function getTranslator(): Translator
    {
        return $this->di->get('translator');
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $this->generateToken();
        $this->saveAdvertiser();
        $this->notify();

        if ($this->notifyBySms) {
            $this->notifySms();
        }
    }


    protected function generateToken()
    {
        $token = $this->getTokenGenerator()->generateToken(time() . uniqid());

        $this->model->token = $token->getHash();
        $this->model->tokenValidFrom = $token->getValidFrom()->format('Y-m-d H:i:s');
        $this->model->tokenValidTo = $token->getValidTo()->format('Y-m-d H:i:s');
    }

    protected function saveAdvertiser()
    {
        parent::doSaveModel($this->model);
    }

    /**
     * @return void
     */
    protected function notify()
    {
        $subject = $this->getTranslator()->translate('mail.subject.token_generated');

        $this->getMailer()->send($this->model->email, $subject, '../mail/newToken.volt', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->geolocation,
            'subject'     => $subject,
            'advertiser'  => $this->model,
        ]);
    }

    /**
     * @return void
     */
    protected function notifySms()
    {
        $this->getSms()->send($this->model->phone, '../sms/newToken.volt', [
            'config'      => $this->getConfig(),
            'geolocation' => $this->geolocation,
            'advertiser'  => $this->model,
        ]);
    }
}
