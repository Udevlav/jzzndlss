<?php

namespace App\Manager;

use App\Model\Advertiser;
use App\Model\Promotion;

class RefundModelManager extends ModelManager
{

    /**
     * @var Advertiser
     */
    protected $model;

    /**
     * @var Promotion[]
     */
    protected $promotions;


    /**
     * @param Promotion[] $promotions
     *
     * @return void
     */
    public function setPromotions($promotions)
    {
        $this->promotions = $promotions;
    }

    /**
     * @inheritdoc
     */
    protected function doSave()
    {
        $this->refundCredits();
        $this->deletePromotions();
    }


    protected function refundCredits()
    {
        $credits = 0;

        foreach ($this->promotions as $promotion) {
            $credits += $promotion->credits;
        }

        $this->model->credits += $credits;

        $this->doSaveModel($this->model);
    }

    protected function deletePromotions()
    {
        $ids = [];

        foreach ($this->promotions as $promotion) {
            $ids[] = $promotion->id;
        }

        if (count($ids)) {
            $this->getDb()->delete('jz_promotion', sprintf('id IN (%s)', implode(',', $ids)));
        }
    }
}