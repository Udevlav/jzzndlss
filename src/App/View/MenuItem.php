<?php

namespace App\View;

class MenuItem
{
    public $title;
    public $href;
    public $color;
    public $icon;
}