<?php

namespace App\View;

use Phalcon\Mvc\View\Engine\Volt\Compiler;

class ArgParser
{

    private $compiler;

    public function __construct(Compiler $compiler)
    {
        $this->compiler = $compiler;
    }

    public function parse($resolvedArgs, $exprArgs): array
    {
        $args = [];
        $offset = 0;

        while (isset($exprArgs[$offset])) {
            $expr = $exprArgs[$offset];

            if (isset($expr['expr']['value'])) {
                $args[] = $expr['expr']['value'];
            } else {
                $args[] = $this->compiler->expression($expr['expr']);
            }

            $offset ++;
        }

        return $args;
    }
}