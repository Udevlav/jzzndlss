<?php

namespace App\View;

use App\Model\Ad;

class Menu extends MenuItem
{

    /**
     * Factory method.
     *
     * @param Ad $entity
     *
     * @return Menu
     */
    static public function of(Ad $entity): Menu
    {

        $menu = new Menu();

        $item = new MenuItem();
        $item->href = 'tel:'. $entity->phone;
        $item->color = 'ref';
        $item->icon = 'phone';
        $item->title = 'ad.link.call';

        $menu->items[] = $item;

        $item = new MenuItem();
        $item->href = sprintf('/%s/experiencia', $entity->slug);
        $item->color = 'yellow';
        $item->icon = 'format_quote';
        $item->title = 'ad.link.add_experience';

        $menu->items[] = $item;

        $item = new MenuItem();
        $item->href = sprintf('/%s/chat', $entity->slug);
        $item->color = 'blue';
        $item->icon = 'insert_chart';
        $item->title = 'ad.link.stats';

        $menu->items[] = $item;

        return $menu;
    }

    public $items = [];
}