<?php

namespace App\View;

use Phalcon\Mvc\View\Engine\Volt\Compiler;

class Templating
{

    private $cacheDir;
    private $templatesDir;
    private $compiler;

    public function __construct(string $cacheDir, string $templatesDir)
    {
        $this->cacheDir = $cacheDir;
        $this->templatesDir = $templatesDir;
        $this->compiler = new Compiler();
        $this->compiler->setOptions([
            'compiledPath' => $cacheDir,
        ]);
    }

    public function render(string $template, array $data): string
    {
        if ('.volt' !== substr($template, -5)) {
            $template .= '.volt';
        }

        $path = sprintf('%s%s', $this->templatesDir, $template);

        if (! file_exists($path)) {
            throw new \RuntimeException(sprintf('Template "%s" not found (base path is %s).', $template, $this->templatesDir));
        }

        if (! is_readable($path)) {
            throw new \RuntimeException(sprintf('Template "%s" is not readable.', $template, $path));
        }

        try {
            extract($data);

            $this->compiler->compile($path);

            ob_start();

            include $this->compiler->getCompiledTemplatePath();

            return ob_get_clean();

        } catch (\Throwable $t) {
            echo ob_get_clean();
            throw new \RuntimeException(sprintf('Failed to render template "%s": %s.', $template, $t->getMessage()), $t->getCode(), $t);
        }
    }
}