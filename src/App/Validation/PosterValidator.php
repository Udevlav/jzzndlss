<?php

namespace App\Validation;

class PosterValidator extends CallbackValidator
{

    public function __construct(array $options = [])
    {
        parent::__construct(array_merge($options, [
            'callback' => function($value) {
                return is_numeric($value);
            }
        ]));
    }

}