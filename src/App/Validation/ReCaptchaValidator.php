<?php

namespace App\Validation;

use App\Captcha\ReCaptchaVerifier;

use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation;

class ReCaptchaValidator extends Validator
{

    public function validate(Validation $validation, $attribute)
    {
        /** @var ReCaptchaVerifier $verifier */
        $verifier = $validation->getDI()->get('recaptcha');

        if (! $verifier->verify($validation->getValue($attribute))) {
            $message = $this->getOption('message');

            if (empty($message)) {
                $message = $verifier->getLastResponse()->isEmpty ?
                    'validation.invalid_captcha' : 'validation.empty_captcha';
            }

            $validation->appendMessage(new Message($message, $attribute));

            return false;
        }

        return true;
    }

}