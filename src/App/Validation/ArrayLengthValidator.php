<?php

namespace App\Validation;

use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation;

class ArrayLengthValidator extends Validator
{

    public function __construct(array $options = null)
    {
        parent::__construct($options);

        $this->assertNumericOption($this->getOption('min'));
        $this->assertNumericOption($this->getOption('max'));
    }

    public function validate(Validation $validation, $attribute)
    {
        $value = $validation->getValue($attribute);

        if (! is_array($value)) {
            $value = empty($value) ? [] : [$value];
        }

        $length = count($value);

        $min = $this->getOption('min');
        $max = $this->getOption('max');

        if ($length < $min || $length > $max) {
            $message = $this->getOption('message');

            if (! $message) {
                $message = 'validation.invalid_length';
            }

            $validation->appendMessage(new Message($message, $attribute));

            return false;
        }

        return true;
    }


    private function assertNumericOption($value)
    {
        if (! is_numeric($value)) {
            throw new \InvalidArgumentException('The min and max options should be integers, if specified.');
        }
    }

}