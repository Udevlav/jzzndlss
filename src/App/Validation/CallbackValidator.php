<?php

namespace App\Validation;

use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation;

class CallbackValidator extends Validator
{

    public function __construct(array $options = null)
    {
        parent::__construct($options);

        if (! $this->hasOption('callback')) {
            throw new \InvalidArgumentException('The callback option must be set.');
        }

        if (! is_callable($this->getOption('callback'))) {
            throw new \InvalidArgumentException('The callback option must be callable.');
        }
    }

    public function validate(Validation $validation, $attribute)
    {
        // $validation->getValue($attribute) returns wrong results
        $value = $validation->getEntity()->$attribute;

        $valid = (bool) call_user_func($this->getOption('callback'), $value);

        if (! $valid) {
            $message = $this->getOption('message');

            if (empty($message)) {
                $message = sprintf('validation.invalid_%s', $attribute);
            }

            $validation->appendMessage(new Message($message, $attribute, 'callback'));
        }

        return $valid;
    }

}
