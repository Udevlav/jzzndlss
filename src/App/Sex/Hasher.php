<?php

namespace App\Sex;

class Hasher
{

    /**
     * @var string
     */
    protected $algorithm;

    public function __construct(string $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    public function hash(... $args): string
    {
        return hash($this->algorithm, implode('', $args));
    }
}