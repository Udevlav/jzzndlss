<?php

namespace App\Locale;

use Phalcon\Http\Request;

interface LocaleGuesser
{

    /**
     * @param \Phalcon\Http\Request $request
     *
     * @return string | null
     */
    public function getLocale(Request $request);
}