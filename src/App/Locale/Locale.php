<?php

namespace App\Locale;

use \Locale as Loc;

class Locale
{

    static public function of(string $locale)
    {
        if (extension_loaded('intl')) {
            $parts = Loc::parseLocale($locale);
        } else {
            $parts = [];
        }

        return new static($locale,
            $parts['language'] ?? null,
            $parts['script'] ?? null,
            $parts['region'] ?? null,
            $parts['keywords'] ?? [],
            $parts['variants'] ?? []
        );
    }

    private $locale;
    private $language;
    private $script;
    private $region;
    private $keywords;
    private $variants;


    protected function __construct(
        string $locale,
        string $language = null,
        string $script = null,
        string $region = null,
        array $keywords = [],
        array $variants = []
    )
    {
        $this->locale = $locale;
        $this->language = $language;
        $this->script = $script;
        $this->region = $region;
        $this->keywords = $keywords;
        $this->variants = $variants;
    }


    public function __toString()
    {
        return $this->locale;
    }


    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }


    /**
     * @return string | null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return string | null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return string | null
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * @return string[] | null
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @return string[] | null
     */
    public function getVariants()
    {
        return $this->variants;
    }


    /**
     * @param string | null $inLocale
     *
     * @return string | null
     */
    public function getDisplayName(string $inLocale)
    {
        return Loc::getDisplayName($this->locale, $inLocale);
    }

    /**
     * @param string | null $inLocale
     *
     * @return string | null
     */
    public function getDisplayScript(string $inLocale = null)
    {
        return Loc::getDisplayScript($this->locale, $inLocale);
    }

    /**
     * @param string | null $inLocale
     *
     * @return string | null
     */
    public function getDisplayLanguage(string $inLocale)
    {
        return Loc::getDisplayLanguage($this->locale, $inLocale);
    }

    /**
     * @param string | null $inLocale
     *
     * @return string | null
     */
    public function getDisplayRegion(string $inLocale)
    {
        return Loc::getDisplayRegion($this->locale, $inLocale);
    }

    /**
     * @param string | null $inLocale
     *
     * @return string | null
     */
    public function getDisplayVariant(string $inLocale)
    {
        return Loc::getDisplayVariant($this->locale, $inLocale);
    }


    /**
     * @return \App\Locale\Locale
     */
    public function toCanonical(): Locale
    {
        return static::of(Loc::canonicalize($this->locale));
    }
}