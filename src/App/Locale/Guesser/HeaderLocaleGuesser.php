<?php

namespace App\Locale\Guesser;

use App\Locale\LocaleGuesser;

use Phalcon\Http\Request;

class HeaderLocaleGuesser implements LocaleGuesser
{
    public function getLocale(Request $request)
    {
        if (! class_exists('\\Locale')) {
            return null;
        }

        $acceptLanguage = $request->getHeader('Accept-Language');

        return $acceptLanguage ? \Locale::acceptFromHttp($acceptLanguage) : null;
    }
}