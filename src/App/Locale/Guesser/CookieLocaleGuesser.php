<?php

namespace App\Locale\Guesser;

use App\Locale\LocaleGuesser;

use Phalcon\Http\Request;
use Phalcon\Http\Response\Cookies;

class CookieLocaleGuesser implements LocaleGuesser
{

    /**
     * @var string
     */
    const DEFAULT_COOKIE_NAME = 'hl';


    /**
     * @var \Phalcon\Http\Response\Cookies
     */
    private $cookies;

    /**
     * @var string
     */
    private $cookieName;

    /**
     * CookieLocaleGuesser constructor.
     *
     * @param \Phalcon\Http\Response\Cookies $cookies
     * @param string | null $cookieName
     */
    public function __construct(Cookies $cookies, string $cookieName = null)
    {
        $this->cookies = $cookies;
        $this->cookieName = $cookieName ?? static::DEFAULT_COOKIE_NAME;
    }

    public function getLocale(Request $request)
    {
        $cookie = $this->cookies->get($this->cookieName);

        return $cookie ? $cookie->getValue() : null;
    }
}