<?php

namespace App\Locale\Guesser;

use App\Locale\LocaleGuesser;

use Phalcon\Http\Request;

class QueryLocaleGuesser implements LocaleGuesser
{

    /**
     * @var string
     */
    const DEFAULT_PARAMETER_NAME = 'hl';


    /**
     * @var string
     */
    private $parameterName;

    /**
     * CookieLocaleGuesser constructor.
     *
     * @param string | null $parameterName
     */
    public function __construct(string $parameterName = null)
    {
        $this->parameterName = $parameterName ?? static::DEFAULT_PARAMETER_NAME;
    }

    public function getLocale(Request $request)
    {
        return $request->getQuery($this->parameterName) ?? null;
    }
}