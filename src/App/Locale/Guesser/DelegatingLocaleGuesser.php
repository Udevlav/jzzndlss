<?php

namespace App\Locale\Guesser;

use App\Locale\Locale;
use App\Locale\LocaleGuesser;

use Phalcon\Http\Request;
use Phalcon\Mvc\User\Component;

class DelegatingLocaleGuesser extends Component implements LocaleGuesser
{

    /**
     * @var \App\Locale\LocaleGuesser[]
     */
    private $delegates = [];


    public function getLocale(Request $request)
    {
        $this->init();

        foreach ($this->delegates as $delegate) {
            if (null !== ($locale = $delegate->getLocale($request))) {
                return $locale;
            }
        }

        return $this->getDefaultLocale();
    }

    public function getDefaultLocale(): Locale
    {
        $locale = $this->di->get('config')->app->locale;

        return Locale::of($locale);
    }


    private function init()
    {
        $config = $this->di->get('config');

        foreach ($config->locale->guesser->toArray() as $guesser => $noop) {
            $this->add($guesser);
        }
    }

    private function add(string $guesser, array $params = [])
    {
        switch ($guesser) {
            case 'cookie':
                $delegate = new CookieLocaleGuesser($this->di->get('cookies'), $params['cookie_name'] ?? null);
                break;

            case 'header':
                $delegate = new HeaderLocaleGuesser();
                break;

            case 'query':
                $delegate = new QueryLocaleGuesser($params['parameter_name'] ?? null);
                break;

            case 'domain':
                $delegate = new SubDomainLocaleGuesser();
                break;

            case 'tld':
                $delegate = new TopLevelDomainLocaleGuesser();
                break;

            case 'path':
                $delegate = new PathLocaleGuesser();
                break;

            default:
                throw new \InvalidArgumentException(sprintf('No such locale guesser "%s".', $guesser));
        }

        $this->delegates[] = $delegate;
    }
}