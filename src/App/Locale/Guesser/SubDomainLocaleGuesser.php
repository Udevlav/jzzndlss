<?php

namespace App\Locale\Guesser;

use App\Locale\LocaleGuesser;

use Phalcon\Http\Request;

class SubDomainLocaleGuesser implements LocaleGuesser
{

    /**
     * @var array
     */
    private $map;

    /**
     * SubDomainLocaleGuesser constructor.
     *
     * @param array $map
     */
    public function __construct(array $map = null)
    {
        $this->map = $map;
    }

    public function getLocale(Request $request)
    {
        if (null === $this->map) {
            return null;
        }

        return $this->map[$this->getSubDomain($request)] ?? null;
    }


    private function getSubDomain(Request $request): string
    {
        
    }
}