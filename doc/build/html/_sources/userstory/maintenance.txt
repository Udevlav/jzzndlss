Caso de Uso: Mantenimiento
==========================

Borrado de ficheros y anuncios temporales
-----------------------------------------

Esta tarea se encarga de borrar los registros correspondientes a anuncios y ficheros temporales (media) que se suben en la publicación de anuncios y que nunca llegan a publicarse. La tarea acepta un parámetro, que permite especificar el número de días a mantener. Todos los anuncios no publicados y media temporales con fecha de creación con una antiguedad superior al número de días especificados serán eliminados. Esta operación no se puede deshacer. Idealmente,

Rank
----

Scrapping
---------