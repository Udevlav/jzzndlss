SEO
===

Índice
------

**Title**: Debería incluir INombre del anunciante, Categoría y Nombre App (separados por una barra vertical "|")

Detalle
-------

**Title**: Nombre del anunciante, Categoría y Nombre App (separados por una barra vertical "|")

**Links**

+--------------------+--------------------------------------------------------+
| Link name          | Link description                                       |
+====================+========================================================+
| canonical          | Enlace canónico (sin parámetros de i18n)               |
+--------------------+--------------------------------------------------------+
| alternate          | Enlace alternativo (uno por lenguaje)                  |
+--------------------+--------------------------------------------------------+

**Meta**

+--------------------+--------------------------------------------------------+
| Meta name          | Meta value                                             |
+====================+========================================================+
| Content-Language   | Lenguaje del contenido                                 |
+--------------------+--------------------------------------------------------+
| abstract           | Super categoría                                        |
+--------------------+--------------------------------------------------------+
| title              | Nombre del anunciante, Categoría y Nombre App          |
|                    | (separados por una barra vertical)                     |
+--------------------+--------------------------------------------------------+
| description        | Título del anuncio y call-to-action (app.cta.head)     |
+--------------------+--------------------------------------------------------+
| copyright          | Nombre de la aplicación                                |
+--------------------+--------------------------------------------------------+
| geo.country        | País                                                   |
+--------------------+--------------------------------------------------------+
| geo.regioncode     | Código de región                                       |
+--------------------+--------------------------------------------------------+
| geo.placename      | Lugar (Ciduad)                                         |
+--------------------+--------------------------------------------------------+
| geo.position       | Posición (latitud y longitud)                          |
+--------------------+--------------------------------------------------------+
| robots             | all (indexar todos los robots)                         |
+--------------------+--------------------------------------------------------+
| revisit-after      | "1 day"                                                |
+--------------------+--------------------------------------------------------+
