======
RedSys
======

Anteriormente llamada Sermepa/Servired.

Configuración
-------------

+-----------------------------+--------------------------------------------------+
| Parámetro                   | Descripción                                      |
+=============================+==================================================+
| redsys.card.number          | Número tarjeta de prueba                         |
+-----------------------------+--------------------------------------------------+
| redsys.card.validTo         | Validex tarjeta de prueba                        |
+-----------------------------+--------------------------------------------------+
| redsys.card.cvv2            | CVV2 tarjeta de prueba                           |
+-----------------------------+--------------------------------------------------+
| redsys.currency             | Moneda (978 para EURO)                           |
+-----------------------------+--------------------------------------------------+
| redsys.key                  | Clave de comercio (base64)                       |
+-----------------------------+--------------------------------------------------+
| redsys.merchantCode         | Código de comercio                               |
+-----------------------------+--------------------------------------------------+
| redsys.merchantName         | Nombre de comercio (opcional)                    |
+-----------------------------+--------------------------------------------------+
| redsys.terminal             | Número de terminal                               |
+-----------------------------+--------------------------------------------------+

Pago
----

1. Crear los datos de pago

+-----------------------------+--------------------------------------------------+
| Parámetro                   | Descripción                                      |
+=============================+==================================================+
| DS_MERCHANT_AMOUNT          | Importe del pago                                 |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_ORDER           | Número de orden de compra                        |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_MERCHANTCODE    | Código comercio (``config.redsys.merchantCode``) |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_CURRENCY        | Código de moneda (``config.redsys.currency``)    |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_TRANSACTIONTYPE | Tipo de transacción                              |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_TERMINAL        | Terminal (``config.redsys.terminal``)            |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_MERCHANTURL     | URL del comerciante                              |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_URLOK           | URL de redirección (éxito)                       |
+-----------------------------+--------------------------------------------------+
| DS_MERCHANT_URLKO           | URL de redirección (fallo)                       |
+-----------------------------+--------------------------------------------------+

2. Crear los datos de la transacción

+-----------------------------+--------------------------------------------------+
| Parámetro                   | Descripción                                      |
+=============================+==================================================+
| Ds_SignatureVersion         | Versión de la firma (``"HMAC_SHA256_V1"``)       |
+-----------------------------+--------------------------------------------------+
| Ds_MerchantParameters       | Parámetros de la transacción                     |
+-----------------------------+--------------------------------------------------+
| Ds_Signature                | Código de autenticación de la transacción (MAC)  |
+-----------------------------+--------------------------------------------------+

con

.. code-block:: bash

    Ds_MerchantParameters := base64( json( parameters ) )

(donde ``parameters`` son los parámetros del paso 1), y

.. code-block:: bash

    key := base64decode( config.redsys.key )
    key := crypt(key, DS_MERCHANT_ORDER)

    message := base64( Ds_MerchantParameters )

    Ds_Signature := base64( mac(key, message) )

3. Redirigir a pasarela de pago (https://sis-t.redsys.es:25443/sis para pruebas ó https://sis.redsys.es/sis para producción)

4. Efectuar el pago en la TPV.

Verificación
------------

1. Crear los datos de la transacción a partir de los datos GET/POST:

+-----------------------------+--------------------------------------------------+
| Parámetro                   | Descripción                                      |
+=============================+==================================================+
| Ds_SignatureVersion         | Versión de la firma (``"HMAC_SHA256_V1"``)       |
+-----------------------------+--------------------------------------------------+
| Ds_MerchantParameters       | Parámetros de la transacción                     |
+-----------------------------+--------------------------------------------------+
| Ds_Signature                | Código de autenticación de la transacción (MAC)  |
+-----------------------------+--------------------------------------------------+

2. Verificar firma

.. code-block:: bash

    key := base64decode( config.redsys.key )
    key := crypt(key, DS_ORDER)

    assert( Ds_Signature == base64UrlEncode(mac( key, Ds_MerchantParameters ))) )