Promoción de anuncios
=====================

La promoción del anuncio no afecta a la puntuación, simplemente asigna una nueva entidad *promotion* al anuncio, que hará que sea seleccionado primero en las búsquedas mientras dicha promoción es válida. Entre dos anuncios promocionados simultáneamente, la ordenación se realiza de acuerdo a la puntuación.

(**Paso 1**) El usuario accede a la ruta de promoción de un anuncio (vía un enlace en el correo electrónico, o vía la página del anunciante). Se da una de las siguientes situaciones:

(**a**) *Si ocurre un error* procesando la petición, devuelve la página de error.

.. code-block:: bash

   > GET /promocionar/:slug
   < 500 Oh My God
     Content-Type: text/html
     (Página de error)

(**b**) *Si no hay ningún anunciante con el token especificado*, o bien *no hay ningún anuncio con el slug especificado* devuelve la págian de error.

.. code-block:: bash

   > GET /publicar/:token/:slug
   < 404 Not Found
     Content-Type: text/html
     (Página de error)

(**c**) *Si el anunciante no tiene créditos* procesando la petición, se envía al usuario a la página de compra de créditos. La compra de créditos se describe en el :doc:`payment`.

.. code-block:: bash

   > GET /promocionar/:slug
     (enviar a la compra de créditos)
   < 200 OK
     Content-Type: text/html
     (Página de error)

(**d**) *En otro caso*, ejecutar la *transacción de promoción* y devolver la página de confirmación.

.. code-block:: bash

   > GET /publicar/:token/:slug
   < 200 OK
     Content-Type: text/html
     (Página de confirmación)

Transacción de promoción
------------------------

**1**. Crear una nueva entidad *promotion*, asociada al anuncio, con la validez apropiada, y persistirla.

.. note::

   La validez de la promoción depende del producto.

**2**. Consumir un crédito del anunciante y persistirlo.

**4**. Enviar un correo a la dirección del anunciante, notificando la promoción del anuncio y su período de validez.