Caso de Uso: Chat
===============

Básicamente, el proceso es el siguiente:

1. Un usuario anónimo A se identifica como anunciante, iniciando una sesión de chat para indicar que está online.
2. Un usuario anónimo B accede a un anuncio y, si hay una sesión de chat iniciada asociada al anunciante, se habilita la funcionalidad de chat para el usuario B.
3. El usuario anónimo B inicia una sesión de chat con el anunciante (usuario B).
4. Los usuarios A y B intercambian mensajes
5. Los usuarios A ó B terminan la sesión de Chat

Planteado de esta manera, el usuario A puede mantener una o más conversaciones simultáneas con otros usuarios, mientras que éstos (no-anunciantes) sólo podrán tener una conversación por anuncio.

Paso 1: Un usuario (anunciante) indica que está online
-------------------------------------------------

Un usuario anónimo (anunciante) puede indicar que está online en cualquier momento. Es requisito imprescindible que un usuario anónimo al que se puede identificar como un anunciante haya iniciado una sesión de chat antes de que otros usuarios puedan enviarle mensajes.

Si lo hace **desde su página de gestión de anuncios**, basta que el usuario utilice el botón "Estoy online". Como todos los usuarios de la aplicación son anónimos, sólo se puede garantizar que el usuario anónimo que interactúa con la aplicación tiene acceso a la sección de gestión, y esta condición se considera suficiente para iniciar la sesión de chat. El proceso de inicio de la sesión de chat es el siguiente:

1. Se realiza una petición asíncrona ``POST /chat/online``. El servidor gestionará esta petición intentando insertar un nuevo registro en la tabla de usuarios online, asociando la sesión actual del usuario anónimo a un anunciante, y registrando la fecha de inicio de sesión.

2. Si ocurre un error manejando la petición, el servidor devuelve una respuesta ``500 Internal Server Error``. Se indicará al usuario que no fue posible iniciar la sesión de chat, y el proceso se puede abortar o retormar desde el paso (1).

3. En otro caso, el servidor devuelve una respuesta ``201 Created`` en formato JSON, indicando los datos del usuario a efectos de la sessión de chat.

.. code-block:: bash

    HTTP/1.1
    Cache-Control: no-cache
    Content-Type: application/json
    Content-Length: ...
    
    {
        "online": true,
        "user": {
            "name": "Advertiser name",
            "sessionId": "79054025255fb1a26e4bc422aef54eb4"
        }
    }

Si lo hace **desde la página de detalle de un anuncio**, el usuario anónimo también dispondrá de un botón "Estoy online". Para iniciar la sesión de chat, primero debe demostrar que conoce el correo electrónico con el que se publicó el anuncio. Tener acceso al correo electrónico de publicación equivale a tener acceso a la sección de gestión. El proceso de inicio de la sesión de chat es el siguiente:

1. Se abrirá un modal con un formulario para introducir el correo electrónico con el que se publicó el anuncio. El proceso se puede abortar cerrando el modal.

2. Cuando se envía el formulario, se realiza una petición asíncrona ``POST /:slug/online``, indicando el correo electrónico en el cuerpo de la petición. El servidor gestionará esta petición validando el email del usuario y comparándolo con el email con el que se publicó el anuncio y, si tiene éxito, intentará insertar un nuevo registro en la tabla de usuarios online, asociando la sesión actual del usuario anónimo a un anunciante, y registrando la fecha de inicio de sesión.

3. Si ocurre un error manejando la petición, el servidor devuelve una respuesta ``500 Internal Server Error``. Se indicará al usuario que no fue posible iniciar la sesión de chat, y el proceso se puede abortar o retormar desde el paso (2).

4. Si la petición no es válida (p.ej, el correo electrónico NO es válido, o bien no está relacionado con el anuncio), el servidor devuelve una respuesta ``400 Bad Request``. Se indicará al usuario el mensaje de error apropiado, y el proceso se puede abortar o retormar desde el paso (2).

5. Si la petición es válida, el servidor devuelve una respuesta ``201 Created`` en formato JSON, indicando los datos del usuario a efectos de la sessión de chat, igual que la anterior. El modal se cierra.

Si el usuario anónimo (anunciante) **inicia la sesión de chat con éxito**, se habilitará un botón fijo (FAB) que permite al usuario abrir la sección de chats.

Paso 2: Un usuario (cliente) accede a un anuncio
--------------------------------------------

Cuando un usuario anónimo (cliente) accede al detalle de un anuncio, la aplicación automáticamente comprueba si el anunciante está online. El proceso es el siguiente:

1. Se realiza una petición asíncrona ``GET /:slug/chat``. El servidor gestionará esta petición intentando encontrar un registro reciente en la tabla de usuarios online correspondiente al anunciante.

2. Si ocurre un error manejando la petición, el servidor devuelve una respuesta ``500 Internal Server Error``. La respuesta se gestiona internamente, sin realizar indicación al usuario, y la funcionalidad de chat no estará disponible.

3. Si no se encuentra una entrada correspondiente al anunciante en la tabla de usuarios online, el servidor devuelve una respuesta ``412 Precondition Failed``. La respuesta se gestiona internamente, sin realizar indicación al usuario, y la funcionalidad de chat no estará disponible (el anunciante no está conectado).

4. En otro caso, si se encuentra una entrada correspondiente al anunciante en la tabla de usuarios online, el servidor devuelve una respuesta ``200 OK`` en formato JSON, indicando los datos del anunciante a efectos de la sessión de chat:

.. code-block:: bash

    HTTP/1.1
    Cache-Control: no-cache
    Content-Type: application/json
    Content-Length: ...
    
    {
        "online": true,
        "peer": {
            "name": "Advertiser name",
            "sessionId": "79054025255fb1a26e4bc422aef54eb4",
            "lastActivityAt": 110122345512344
        }
    }


Paso 3: Un usuario (cliente) inicia una sesión de chat con el anunciante
-----------------------------------------------------------------

Si al acceder a un anuncio **el anunciante está online** (paso 2), se habilitará un botón fijo (FAB) que permite al cliente iniciar una sesión de chat con el anunciante. Para iniciar la sesión:

1. Se abrirá un modal con un formulario para que el cliente introduzca su nombre (displayName). El proceso se puede abortar cerrando el modal.

2. Cuando se envía el formulario, se realiza una petición asíncrona ``POST /:slug/peer``, indicando el nombre del cliente a mostrar (displayName) en el cuerpo de la petición.

3. Si ocurre un error manejando la petición, el servidor devuelve una respuesta ``500 Internal Server Error``. Se indicará al usuario que no fue posible iniciar la sesión de chat, y el proceso se puede abortar o retormar desde el paso (2).

4. Si la petición no es válida (el nombre del usuario NO es válido), el servidor devuelve una respuesta ``400 Bad Request``. Se indicará al usuario el mensaje de error apropiado, y el proceso se puede abortar o retormar desde el paso (2).

5. Si la petición es válida, el servidor devuelve una respuesta ``201 Created`` en formato JSON, indicando los datos del usuario a efectos de la sessión de chat, igual que la anterior. El modal se cierra.

.. code-block:: bash

    HTTP/1.1
    Cache-Control: no-cache
    Content-Type: application/json
    Content-Length: ...
    
    {
        "online": true,
        "user": {
            "name": "Client display name",
            "sessionId": "9e107d9d372bb6826bd81d3542a419d6"
        },
        "peer": {
            "name": "Advertiser name",
            "sessionId": "79054025255fb1a26e4bc422aef54eb4",
            "lastActivityAt": 110122345512344
        }
    }
    
Si el usuario anónimo (cliente) **inicia la sesión de chat con éxito**, se abrirá la sección de chats, se inicializará una sala de chat con el anunciante (inicialmente vacía).

Paso 4: Los usuarios intercambian mensajes
--------------------------------------

Cuando se inicia una sesión de chat (en cualquier extremo, anunciante o cliente), se configura un intervalo de refresco para comprobar si hay mensajes nuevos en la sesión. La duración del intervalo de refresco, así como la estrategia de refresco se pueden configurar a través de los parámetros ``chat.refreshInterval`` y ``chat.refreshStrategy`` del fichero de configuración. El proceso de refresco es el siguiente:

1. Se realiza una petición asíncrona a ``GET /chat/:sessionId``, donde ``:sessionId`` es el identificador de sesión del usuario local. El servidor manejará la petición buscando todos los mensajes remitidos o enviados desde/hacia dicha sesión.

3. Si ocurre un error manejando la petición, el servidor devuelve una respuesta ``500 Internal Server Error``. Se indicará al usuario que no fue posible iniciar la sesión de chat, y el proceso de refresco termina aquí, hasta el siguiente intento de refresco.

4. Si no se encuentra ningún mensaje pendiente de entrega, el servidor devuelve una respuesta ``204 No Content``, y el proceso termina aquí, hasta el siguiente intento de refresco.

5. Si se encuentran mensajes pendientes de entrega, el servidor devuelve una respuesta ``200 OK`` en formato JSON, indicando los mensajes pendientes de presentación, agrupados por sala de chat:

