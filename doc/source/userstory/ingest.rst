Ingesta de recursos
===================

(**Paso 1**)

.. code-block:: bash

   > PUT /ingestar
     Content-Type: multipart/form-data
   < 201 Created
     Content-Type: application/json
     (Identificador del recurso temporal ingestado)

.. note::

   El tipo y los tamaños mínimo y máximo para los recursos de imagen y video se define en las directivas de configuración ``media.image.allowedTypes``, ``media.image.allowedExts``, ``media.image.minSize`` (bytes) y ``media.image.maxSize`` (bytes).

