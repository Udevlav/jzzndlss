Publicación de un anuncio
=========================

(**Paso 1**) El usuario accede a la ruta de publicación (vía un enlace en el correo electrónico, SMS o vía la página del anunciante). Se da una de las siguientes situaciones:

(**a**) *Si ocurre un error* procesando la petición, devuelve la página de error.

.. code-block:: bash

   > GET /publicar/:token/:slug
   < 500 Oh My God
     Content-Type: text/html
     (Página de error)

(**b**) *Si no hay ningún anunciante con el token especificado*, o bien *si no hay ningún anuncio con el slug especificado*; devuelve la págian de error.

.. code-block:: bash

   > GET /publicar/:token/:slug
   < 404 Not Found
     Content-Type: text/html
     (Página de error)

(**c**) *Si el token del anunciante está caducado*, enviar al usuario a la página del anunciante. La gestión de anuncios se describe en el caso de uso :doc:`manage`. (Este envío implícitamente permitirá generar un *token* nuevo y válido, y un nuevo enlace de gestión)

.. code-block:: bash

   > GET /publicar/:token/:slug
     (forward a /anunciante/:token)
   < 200 OK
     Content-Type: text/html
     (Página del anunciante)

(**d**) *Si el anuncio se está publicando actualmente*, enviar al usuario a la página de "el anuncio está siendo publicado".

.. code-block:: bash

   > GET /publicar/:token/:slug
   < 200 OK
     Content-Type: text/html
     (El anuncio está siendo publicado)

.. note::

   La página debería tener un enlace a la página de promoción.

(**e**) *En otro caso*; se ejecuta la *transacción de publicación del anuncio* y se devuelve la página de publicación en proceso.

.. code-block:: bash

   > POST /publicar
     Content-Type: application/x-www-form-urlencoded
   < 201 Created
     Content-Type: text/html
     (Página de publicación en proceso)

.. note::

   La página de publicación en proceso debería tener un enlace a la página de promoción.

Transacción de publicación del anuncio
--------------------------------------

Los siguientes pasos se ejecutan de forma atómica:

**1**. Procesar los recursos del anuncio; para cada recurso:

**1.a**. Si el recurso es una imagen:

**1.a.1**. Insertar la marca de agua al *master*.

**1.a.2**. Generar los *thumbnails* a partir del resultado del paso anterior.

.. note::

   El tipo y las características de los thumbnails se configuran mediante las directivas ``media.image.defaultType`` y ``media.thumbnails``.

**1.b**. Si el recurso es un video:

**1.b.2**. Generar las rendiciones (transcodificaciones) a partir del *master*, insertando la marca de agua.

.. note::

   El tipo y las características de las rendiciones se configuran mediante las directivas ``media.transcoding``.

**1.b.1**. Extraer los *thumbnails* del master.

.. note::

   El tipo y las características de los thumbnails se configuran mediante las directivas ``media.image.defaultType`` y ``media.thumbnails``.

**3**. Mover los recursos del anuncio al directorio definitivo.

.. note::

   El directorio para los recursos y el formato para la estructura de directorios se configura en las directivas ``media.fs.path`` y ``media.fs.layout``.

**4**. Actualizar el anuncio: establecer la fecha de publicación y la puntuación inicial.

**5**. Enviar un correo a la dirección del anunciante, notificando la publicación del anuncio e incluyendo un enlace a la página del anunciante.
