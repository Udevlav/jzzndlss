Búsqueda de anuncios
====================

El diseño de la aplicación se basa en la suposición de que una amplísima mayoría de usuarios pase la mayor parte del tiempo buscando anuncios.

La búsqueda de anuncios es accesible desde las siguientes rutas:

+------------+----------------------------------------------------+---------------------------+
| Método     | Plantilla de la ruta                               | Ruta                      |
+============+====================================================+===========================+
| GET / POST | /                                                  | ``@index``                |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category                                         | ``@category_index``       |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category/:location                               | ``@location_index``       |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category/:location/:tag1                         | ``@location_index_1tags`` |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category/:location/:tag1/:tag2                   | ``@location_index_2tags`` |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category/:location/:tag1/:tag2                   | ``@location_index_3tags`` |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category/:location/:tag1/:tag2                   | ``@location_index_4tags`` |
+------------+----------------------------------------------------+---------------------------+
| GET / POST | /:category/:location/:tag1/:tag2/:tag3/:tag4/:tag5 | ``@location_index_5tags`` |
+------------+----------------------------------------------------+---------------------------+

(**Paso 1**) El usuario solicita una búsqueda de anuncios. La aplicación ejecuta el *algoritmo de búsqueda* de anuncios y se devuelve la lista de resultados.

.. code-block:: php

   > GET /
   < 200 OK
     Content-Type: text/html o application/json
     (Colección de anuncios)

Algoritmo de búsqueda de anuncios
---------------------------------

Todas las peticiones de búsqueda se manejan de la siguiente forma:

**1**. Se construye una instancia de la clase *Filters*, que contiene los parámetros de selección de anuncios, a partir de diferentes fuentes, con el siguiente orden de prioridad (los parámetros especificados en un elemento posterior sobreescriben los valores especificados en un elemento anterior):

   - los parámetros de configuración actual;

   - los parámetros en la sesión actual del usuario; y

   - los parámetros de la petición (ruta solicitada, parámetros GET y/o POST).

**2**. Delega la operación de búsqueda al servicio *Finder*, pasando la instancia de *Filters* creada en el paso 1. El *Finder* construye una consulta de selección y aplica los criterios de filtrado a la consulta, más los criterios de ordenación apropiados, según el algoritmo de búsqueda.

**3**. Devuelve una colección de anuncios con los resultados encontrados.

Construcción de la consulta de selección
----------------------------------------

La consulta de selección se construye aplicando los parámetros de selección (indicados por el usuario) y los parámetros de ordenación (indicados por el algoritmo).

Parámetros de selección
~~~~~~~~~~~~~~~~~~~~~~~

La lista de parámetros utilizados para la búsqueda y selección de anuncios es la siguiente:

1. **Parámetros generales**: especificados vía parámetros GET o POST

+-------------+---------------+------+------+------+--------+-------------------------------+
| Nombre      | Tipo          | Ruta | GET  | POST | Sesión | Descripción                   |
+=============+===============+======+======+======+========+===============================+
| promoted    | -/0/1         | No   | Sí   | Sí   | No     | Todos/Regulares/Promocionados |
+-------------+---------------+------+------+------+--------+-------------------------------+
| query       | string(0..64) | No   | Sí   | Sí   | No     | Texto de búsqueda             |
+-------------+---------------+------+------+------+--------+-------------------------------+
| offset      | int           | No   | Sí   | Sí   | No     | Número de página              |
+-------------+---------------+------+------+------+--------+-------------------------------+
| limit       | int           | No   | Sí   | Sí   | No     | Tamaño de página              |
+-------------+---------------+------+------+------+--------+-------------------------------+

2. **Campos del anuncio**: especificados vía la ruta o como parámetros GET o POST

+-------------+--------+------+------+------+--------+-------------------------------------------------+
| Nombre      | Tipo   | Ruta | GET  | POST | Sesión | Descripción                                     |
+=============+========+======+======+======+========+=================================================+
| category    | string | Sí   | No   | Sí   | No     | Categoría                                       |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| location    | string | Sí   | Sí   | Sí   | No     | Ubicación (codigo postal, ciudad o provincia)   |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| tags[]      | string | Sí   | Sí   | Sí   | No     | Etiquetas (máximo 5)                            |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| ethnicity   | string | No   | Sí   | Sí   | No     | Etnia                                           |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| occupation  | string | No   | Sí   | Sí   | No     | Ocupación                                       |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| origin      | string | No   | Sí   | Sí   | No     | Origen                                          |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| schooling   | string | No   | Sí   | Sí   | No     | Formación                                       |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| age_from    | int    | No   | Sí   | Sí   | No     | Edad (desde)                                    |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| age_to      | int    | No   | Sí   | Sí   | No     | Edad (hasta)                                    |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| fee_from    | int    | No   | Sí   | Sí   | No     | Precio (desde)                                  |
+-------------+--------+------+------+------+--------+-------------------------------------------------+
| fee_to      | int    | No   | Sí   | Sí   | No     | Precio (hasta)                                  |
+-------------+--------+------+------+------+--------+-------------------------------------------------+

Algunos parámetros se pueden especificar sólo mediante la ruta, y otros indistintamente vía GET y POST (por ejemplo, las categorías), algunos con limitaciones (sólo 5 etiquetas vía GET).

Para construir la instancia *Filters*, se procede de la siguiente manera:

**1**. Se aplica la configuración por defecto, en su caso. La configuración por defecto incluye parámetros de selección generales para todos los anuncios (por ejemplo, una fecha de publicación superior a la actual)

**2**. Se aplica la configuración presente en la sesión del usuario, en su caso. La configuración del usuario incluye preferencias locales y regionales.

**3**. Se aplican los filtros especificados en el cuerpo de la petición POST, en su caso.

**4**. Se aplican los filtros especificados como parámetros GET.

.. note::

    Para añadir o eliminar nuevos parámetros de filtrado y selección, o para modificar las prioridades a la hora de construir la instancia de filtros, basta editar o extender la clase ``Filters``.

Parámetros de ordenación
~~~~~~~~~~~~~~~~~~~~~~~~

El segundo paso para la selección de anuncios es aplicar los criterios de selección especificados mediante la instancia de la clase ``Filters`` a la consulta de búsqueda, y ordenar los resultados de acuerdo a su puntuación. La puntuación de cada anuncio se actualiza cada vez que se accede al anuncio, cuando se promociona, y también de forma periódica (vía crontask).

.. note::

    Para modificar el algoritmo de selección de anuncios (cómo se aplican los filtros y cómo se ordenan los resultados), basta editar, extender o reemplazar el servicio ``Finder``. Para modificar el algoritmo de puntuación, véase el servicio ``Ranker``.
