Gestión de anuncios
===================

(**Paso 1**) El usuario accede a la página del anunciante (vía un enlace en el correo electrónico, SMS o vía envío desde otra acción). Se da una de las siguientes situaciones:

(**a**) *Si ocurre un error* procesando la petición, devuelve la página de error. La historia de usuario termina aquí.

.. code-block:: bash

   > GET /anunciante/:token
   < 500 Oh My God
     Content-Type: text/html
     (Página de error)

(**b**) *Si no hay ningún anunciante con el token especificado*, devuelve la págian de error. La historia de usuario termina aquí.

.. code-block:: bash

   > GET /anunciante/:token
   < 404 Not Found
     Content-Type: text/html
     (Página de error)

(**c**) *Si el token del anunciante está caducado*, devolver el formulario de generación de *token*. Cuando el usuario envia el formulario, la historia de usuario continua en el Paso 2.

.. code-block:: bash

   > GET /anunciante/:token
   < 200 OK
     Content-Type: text/html
     (Formulario de generación de token)

(**d**) *En otro caso*, se recuperan todos los anuncios para el anunciante y se devuelve la página del anunciante. La historia de usuario termina aquí.

.. code-block:: bash

   > GET /anunciante/:token
   < 200 OK
     Content-Type: text/html
     (Página del anunciante)

(**Paso 2**) El usuario envía el formulario de generación de *token*. Se da una de las siguientes situaciones:

(**a**) *Si ocurre un error* procesando la petición, devuelve la página de error. La historia de usuario termina aquí.

.. code-block:: bash

   > POST /anunciante/:token
   < 500 Oh My God
     Content-Type: text/html
     (Página de error)

(**b**) *Si no hay ningún anunciante con el token especificado*, devuelve la página de error. La historia de usuario termina aquí.

.. code-block:: bash

   > POST /anunciante/:token
   < 404 Not Found
     Content-Type: text/html
     (Página de error)

(**c**) *Si el formulario no es válido* (por ejemplo, contiene un correo electrónico no registrado o no coincide con el del anunciante esperado), devolver el formulario de generación de *token*, con los errores correspondientes.

.. code-block:: bash

   > POST /anunciante/:token
   < 400 Bad Request
     Content-Type: text/html
     (Formulario de generación de token)

(**d**) *En otro caso*, se ejecuta la *transacción de generación de token*, y se envía al usuario a la página del anunciante.

.. code-block:: bash

   > POST /anunciante/:token
   < 202 Accepted
     Content-Type: text/html
     (Página del anunciante)

