Compra de créditos
==================

Para promocionar sus anuncios, los anunciantes disponen de un saldo de créditos, que pueden utilizar para aparecer antes en los listados, independientemente de su puntuación. Los créditos se ofrecen a través de una o varias promociones (productos).

La promoción del anuncio es accesible desde la ruta genérica (``/promocionar``), o bien desde el detalle de un anuncio y la página del anunciante (ambas usan la ruta ``/promocionar/:slug``). También es accesible vía envíos de otras acciones.

(**Paso 1**) El usuario accede a la página de promoción y se devuelve el formulario de promoción. En la página de promoción, el usuario debe seleccionar una promoción, indicar su correo electrónico (si proviene de la ruta genérica) y el método de pago, y verificar la selección realizada.

.. code-block:: php

   > GET /promocionar[/:slug]
   < 200 OK
     Content-Type: text/html
     (Formulario de promoción)

(**Paso 2**) El usuario confirma los datos de la operación y envía el formulario. Se da una de las siguientes situaciones:

(**a**) *Si ocurre un error* procesando la petición, devuelve la página de error.

.. code-block:: bash

   > POST /promocionar[/:slug]
     Content-Type: application/x-www-form-urlencoded
   < 500 Oh My God
     Content-Type: text/html
     (Página de error)

(**b**) *Si el formulario no es válido*, se devuelve el formulario de pago. La historia de usuario se puede retomar desde el Paso 2.

.. code-block:: bash

   > POST /promocionar[/:slug]
     Content-Type: application/x-www-form-urlencoded
   < 400 Bad Request
     Content-Type: text/html
     (Página de error)

(**c**) *En otro caso*, se redirige al usuario a la pasarela de pago apropiada, de acuerdo al método seleccionado.

.. code-block:: bash

   > POST /promocionar[/:slug]
     (reenvío a la pasarela de pago)

(**Paso 3**) El usuario ejecuta o cancela la operación de pago. La pasarela puede realizar peticiones a la plataforma para verificar la operación:

.. code-block:: bash

   > GET /verificar-pago
   < 200 OK
     (El tipo de respuesta depende de la pasarela de pago)

Se da una de las siguientes situaciones:

(**a**) *El usuario cancela la operación*, o bien ésta falla, y la pasarela redirige al usuario a la página de error en el pago.

.. code-block:: bash

   > GET /error-pago
   < 200 OK
     Content-Type: text/html
     (Página de error en el pago)

(**b**) *El usuario ejecuta la operación*, y la pasarela redirige al usuario a la página de éxito en el pago. Se ejecuta la *transacción de verificación de pago* y ocurre un error (por ejemplo, no se puede verificar la operación, o no se pueden sumar créditos al anunciante).

.. code-block:: bash

   > GET /pago-confirmado
   < 200 OK
     Content-Type: text/html
     (Página de error en el pago)

(**c**) *El usuario ejecuta la operación*, y la pasarela redirige al usuario a la página de éxito en el pago. Se ejecuta la *transacción de verificación de pago* con éxito y se devuelve la página de confirmación de pago.

.. code-block:: bash

   > GET /pago-confirmado
   < 200 OK
     Content-Type: text/html
     (Página de confirmación de pago)

Transacción de verificación de pago
-----------------------------------

Los siguientes pasos se ejecutan de forma atómica:

**1**. Crear una nueva entidad *payment* para registrar la operación de compra, asociada al anunciante correspondiente.

**2**. Actualizar el anunciante

**3**. Enviar un correo a la dirección del anunciante, notificando los datos de la operación de compra y el nuevo saldo de créditos (factura - tienen derecho ?).