Creación de un anuncio
======================

(**Paso 1**) El usuario accede al formulario de publicación.

.. code-block:: bash

   > GET /publicar
   < 200 OK
     Content-Type: text/html
     (Formulario de publicación)

(**Paso 2**) El usuario rellena el formulario de publicación. Durante el proceso, puede ingestar uno o varios recursos, que se almacenan en el directorio temporal y, en el momento de su ingesta, no están asociados a ningún anuncio (*huérfanos*) ni se procesan de ningún modo. La ingesta de recursos se describe en el :doc:`ingest`.

.. code-block:: bash

   > PUT /ingestar
     Content-Type: multipart/form-data
   < 201 Created
     Content-Type: application/json
     (Identificador del recurso temporal ingestado)

.. note::

   El número mínimo y máximo de recursos (imágenes y vídeos) se define en las directivas de configuración ``publication.maxImagesPerAd``, ``publication.minImagesPerAd`` y ``publication.maxVideosPerAd``.

(**Paso 3**) El usuario envía el formulario (que incluye los identificadores de los recursos relacionados). El servidor procesa la petición y se da una de las siguientes situaciones:

(**a**) *Si ocurre un error* procesando el formulario, devuelve la página de error.

.. code-block:: bash

   > POST /publicar
     Content-Type: application/x-www-form-urlencoded
   < 500 Oh My God
     Content-Type: text/html
     (Página de error)

(**b**) *Si hay errores de validación* devuelve el formulario, con los errores correspondientes.

.. code-block:: bash

   > POST /publicar
     Content-Type: application/x-www-form-urlencoded
   < 400 Bad Request
     Content-Type: text/html
     (Formulario de publicación)

(**c**) *En otro caso*, se ejecuta la *transacción de creación del anuncio* y devuelve la página de promoción.

.. code-block:: bash

   > POST /publicar
     Content-Type: application/x-www-form-urlencoded
   < 201 Created
     Content-Type: text/html
     (Página de promoción)

Transacción de creación del anuncio
-----------------------------------

Los siguientes pasos se ejecutan de forma atómica:

**1**. Generar campos internos (privados) del anuncio: poster seleccionado y layout (*portrait*/*landscape*), slug, proveedor (*user*), número de imágenes y vídeos, número de reseñas y puntuación según reseñas (nulos), puntuación interna del anuncio (nula), IP de creación, fecha de última actualización (la actual UTC).

**2**. Obtener el anunciante.

**2.1**. Obtener el anunciante correspondiente al correo electrónico especificado en el anuncio e incrementar el número de anuncios. Si no existera un anunciante con tal correo electrónico, crear y persistir un nuevo anunciante a partir de los datos del anuncio, con un único anuncio y número de anuncios publicados y número de créditos nulos.

**2.2**. Si el anunciante no dispusiera de un *token* válido, generar un nuevo token para el anunciante, con la validez apropiada (el token es necesario para generar un enlace de publicación y de gestión de anuncios).

**2.3**. Persistir el anunciante. Asignar el identificador del anunciante al anuncio.

.. note::

   Cabe recordar que, sin autenticación, los anunciantes se identifican unívocamente por su *email*. El período de validez de un token se controla en la directiva de configuración ``publication.tokenLifetime`` (en segundos), por defecto 24 horas (3600 segundos).

**3**. Persistir el anuncio.

**4**. Persistir las etiquetas para el anuncio.

**5**. Actualizar los recursos correspondientes al anuncio, asignando el identificador del anuncio creado. Los recursos se mantienen en el directorio temporal.

.. note::

   Los recursos (imágenes y vídeos) especificados simplemente se asocian al anuncio recién creado, pero permanecerán en su ubicación temporal y no se procesarán de ninguna forma hasta la publicación del anuncio, en su caso. Los recursos huérfanos permanecen en la ubicación temporal, y son eliminados por una tarea de mantenimiento periódica. Consultar las directivas de configuración ``gc.orphanMediaInterval`` y ``gc.orphanMediaTtl``.

**6**. Enviar un correo a la dirección especificada en el anuncio, suministrando un enlace para publicar el anuncio y otro a la página del anunciante.

**7**. Si el usuario lo solicitó, enviar un SMS con el enlace de publicación al número de teléfono indicado en el anuncio.

El anuncio creado no está publicado (*temporal*) y ninguno de los recursos se moverá del directorio temporal ni se procesará hasta que se solicite la publicación del anuncio.

.. note::

   Los anuncios no publicados (y sus recursos temporales asociados), son eliminados por una tarea de mantenimiento periódica. Consultar las directivas de configuración ``gc.tempAdInterval`` y ``gc.tempAdTtl``.
