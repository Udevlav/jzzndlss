=======
Consola
=======

La aplicación dispone de una consola, desde la que se pueden lanzar diferentes tareas de despliegue. La consola se accede desde la raíz del proyecto:

.. code-block:: bash

   $ cd ${PATH_TO_PROJECT}
   $ php cli

Cuando se ejecuta sin argumentos, la consola simplemente devuelve una lista de comandos disponibles. Para el ejemplo anterior:

.. code-block:: bash

    Jazz Needless 0.0 (PHP: 7.0.15-0ubuntu0.16.04.4, phalcon: 3.0.4, env: dev, debug: true)

    Usage:
      command [options] [arguments]

    Options:
      -h, --help            Display this help message
      -q, --quiet           Do not output any message
      -V, --version         Display this application version
          --ansi            Force ANSI output
          --no-ansi         Disable ANSI output
      -n, --no-interaction  Do not ask any interactive question
      -e, --env=ENV         The environment name [default: "dev"]
          --no-debug        Switches off debug mode
      -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

    Available commands:
      help                   Displays help for a command
      list                   Lists commands
     check
      check:config           Checks the app config.
      check:env              Checks the app environment.
      check:mailer           Checks the Mailer service.
      check:sms              Checks the SMS Service API.
     db
      db:fixtures:load       Loads the data fixtures.
      db:schema:create       Creates the DB schema.
      db:schema:drop         Drops the DB schema.
     deploy
      deploy:makefile        Generates and deploys the app Makefile.
      deploy:manifest        Generates and deploys the app manifest.
      deploy:package         Generates and deploys the app package.json file.
      deploy:robots          Generates and deploys the app robots.txt file.
      deploy:selfsignedcert  Generates a self-signed certificate.
      deploy:sitemap         Generates and deploys the app sitemap file.
      deploy:vhost           Generates and deploys the app virtual host.
     publish
      publish:ad             Publishes an ad (this may involve running other tasks).
      publish:frames         Extracts frames from the input video resource.
      publish:metadata       Extracts metadata for the input media resource.
      publish:thumbnail      Generates thumbnails from the input image resource.
      publish:thumbnails     Generates thumbnails from the input image resource.
      publish:transcode      Transcodes the input media resource.
      publish:watermark      Adds a watermark to the input image resource.
     scrap
      scrap:ad               Populates the ad data via scrapping

