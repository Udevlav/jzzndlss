.. jazz-needless documentation master file, created by
   sphinx-quickstart on Fri Dec 15 22:05:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

jazz-needless documentation
===========================

.. toctree::
   :caption: Documentación
   :maxdepth: 1

   cli/index
   config/index
   userstory/index

Generada |today|
